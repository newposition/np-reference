@echo off
setlocal

call %~dp0setVers.cmd

if EXIST %CATALINA_HOME% goto :eof

set DOWNLOADS=%USERPROFILE%\Downloads

if EXIST %DOWNLOADS%\%TOMCAT_FILE%.zip GOTO :SKIPDL
bitsadmin /transfer mydownloadjob /download /priority normal %TOMCAT_DL% %DOWNLOADS%\%TOMCAT_FILE%.zip
if %ERRORLEVEL% neq 0 echo dl failed & exit /b 1
:SKIPDL

powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('%DOWNLOADS%\%TOMCAT_FILE%.zip', '%SUPER_ROOT_DIR%'); }"
if %ERRORLEVEL% neq 0 echo unzip failed & exit /b 1

endlocal
