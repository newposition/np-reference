@echo off

rem batch file to build\fire up a working instance
rem todo: replace with a cross platform version .gradle? 

rem targets: 
rem <empty>=build and deploy out dir
rem start=start web site in tomcat-8
rem stop=stop web site
rem clean=delete build output

cls
setlocal

call %~dp0setVers.cmd

call mav.cmd
if %ERRORLEVEL% neq 0 echo mav failed & goto :eof

set PROJECT=%ROOT_DIR%\ReferenceSourceLatest
set BUILD_TARGETS=clean verify
set WAR_FILE=%ROOT_DIR%\ReferenceSourceLatest\newposition-webapp\target\newposition-webapp-1.0-SNAPSHOT.war
set EXPLODED_WAR=%ROOT_DIR%\ReferenceSourceLatest\newposition-webapp\target\newposition-webapp-1.0-SNAPSHOT

if "%1" EQU "clean" goto :clean
if "%1" EQU "start" goto :start
if "%1" EQU "stop" goto :stop
if "%1" NEQ "" echo unknown commnd : %1 & goto :eof

call mvn.cmd -f %PROJECT%\pom.xml %BUILD_TARGETS%
if %ERRORLEVEL% neq 0 echo mvn failed & goto :eof

xcopy %BUILD_DIR%\conf %OUT_DIR%\conf\ /S /D /Y /Q
rem try and get war file to work
rem xcopy %WAR_FILE% %OUT_DIR%\ROOT\ /D /Y /Q
xcopy %EXPLODED_WAR% %OUT_DIR%\ROOT\ /S /D /Y /Q

goto :eof

:clean
echo clean
rem call mvn.cmd -f %PROJECT%\pom.xml clean
call mvn.cmd -f %PROJECT%\pom.xml clean
if %ERRORLEVEL% neq 0 echo mvn failed & goto :eof

rd /S /Q %OUT_DIR%
goto :eof

:start
echo start
call tom.cmd
if %ERRORLEVEL% neq 0 echo tom failed & goto :eof

start cmd.exe /c %CATALINA_HOME%\bin\catalina.bat run
goto :eof

:stop
echo stop
call tom.cmd
if %ERRORLEVEL% neq 0 echo tom failed & goto :eof

call %CATALINA_HOME%\bin\catalina.bat stop
goto :eof

:hbm2ddl
call mvn.cmd -f %ROOT_DIR%\ReferenceSourceLatest\newposition-core\pom.xml hibernate3:hbm2ddl
if %ERRORLEVEL% neq 0 echo mvn failed & goto :eof
md %ROOT_DIR%\ReferenceSourceLatest\Database\Schema
copy %ROOT_DIR%\ReferenceSourceLatest\newposition-core\target\hibernate3\sql\schema.sql %ROOT_DIR%\ReferenceSourceLatest\Database\Schema\Newposition-Schema.sql /Y
goto :eof
