@echo off
cls

call %~dp0setVers.cmd

set SQL="c:\Program Files\MySQL\%MYSQL_VER%\bin\mysql.exe"
set CRED=--user=%USERNAME% --password
set ADMCRED=--user=npAdmin --password=npAdmin
set OPT=--show-warnings
set NOWARNOPT=

if NOT EXIST %SQL% echo please install mysql %SQL% & goto :eof

rem check db running? issue sc start etc.

%SQL% %ADMCRED% %OPT% -e ""
if %ERRORLEVEL% equ 0 goto :adminOk

echo Enter creds for %USERNAME%@localhost for dev db setup
%SQL% %CRED% %OPT% -v < .\SetupDevDb.sql
if %ERRORLEVEL% neq 0 echo Setup failed & goto :eof

:adminOk
%SQL% %ADMCRED% %OPT% -e "status;"
if %ERRORLEVEL% neq 0 echo status failed & goto :eof

%SQL% %ADMCRED% %NOWARNOPT% --default-character-set=utf8 < ..\ReferenceSourceLatest\Database\Schema\create-db.sql
if %ERRORLEVEL% neq 0 echo schema deploy failed & goto :eof

%SQL% %ADMCRED% %NOWARNOPT% --database=npdbs --default-character-set=utf8 < ..\ReferenceSourceLatest\Database\Schema\Newposition-Schema.sql
if %ERRORLEVEL% neq 0 echo schema deploy failed & goto :eof

%SQL% %ADMCRED% %NOWARNOPT% --database=npdbs --default-character-set=utf8 < ..\ReferenceSourceLatest\Database\Schema\test-data.sql
if %ERRORLEVEL% neq 0 echo schema deploy failed & goto :eof

echo done.