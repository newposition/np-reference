@echo off
setlocal

call %~dp0setVers.cmd

set MAVEN_FILE=apache-maven-%MAVEN_VER%
set MAVEN_ZIP=%MAVEN_FILE%-bin.zip
set MIRROR=http://apache.mirror.anlx.net
set DOWNLOADS=%USERPROFILE%\Downloads

pushd %~dp0..
set BIN=%cd%\bin
popd

if not exist "%JAVA_HOME%" echo please install jdk %JAVA_HOME% & exit /b 1

call mvn.cmd --help > NUL 2> NUL
if %ERRORLEVEL% equ 0 goto :mvnFound

if EXIST %DOWNLOADS%\%MAVEN_ZIP% GOTO :SKIPDL
bitsadmin /transfer mydownloadjob /download /priority normal %MIRROR%/maven/maven-3/%MAVEN_VER%/binaries/%MAVEN_ZIP% %DOWNLOADS%\%MAVEN_ZIP%
if %ERRORLEVEL% neq 0 echo dl failed & exit /b 1
:SKIPDL

if EXIST %BIN%\%MAVEN_FILE% GOTO :SKIPUNZIP
powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('%DOWNLOADS%\%MAVEN_ZIP%', '%BIN%'); }"
if %ERRORLEVEL% neq 0 echo unzip failed & exit /b 1
:SKIPUNZIP

endlocal & set "PATH=%PATH%;%BIN%\%MAVEN_FILE%\bin" & set JAVA_HOME=%JAVA_HOME%
goto :fin

:mvnFound
endlocal

:fin
echo mvn:
where mvn.cmd
