set JAVA_MAJ=8
set JAVA_MIN=0
set JAVA_UPD=73
set JAVA_SUF=b02

set JAVA_FILE=jdk-%JAVA_MAJ%u%JAVA_UPD%-windows-x64.exe
rem set JAVA_DL=http://download.oracle.com/otn-pub/java/jdk/%JAVA_MAJ%u%JAVA_UPD%-%JAVA_SUF%/%JAVA_FILE%
set JAVA_HOME=C:\Program Files\Java\jdk1.%JAVA_MAJ%.%JAVA_MIN%_%JAVA_UPD%

set TOMCAT_VER=8.0.32
set TOMCAT_FILE=apache-tomcat-%TOMCAT_VER%
set TOMCAT_DL=http://www.mirrorservice.org/sites/ftp.apache.org/tomcat/tomcat-8/v%TOMCAT_VER%/bin/%TOMCAT_FILE%.zip

set MAVEN_VER=3.3.9

set MYSQL_VER=MySQL Server 5.7

set BUILD_DIR=%~dp0

pushd %~dp0..
set ROOT_DIR=%cd%
cd ..
set SUPER_ROOT_DIR=%cd%
popd

set OUT_DIR=%ROOT_DIR%\bin\out
if not exist %OUT_DIR% md %OUT_DIR%
if not exist %OUT_DIR%\temp md %OUT_DIR%\temp

set CATALINA_HOME=%SUPER_ROOT_DIR%\%TOMCAT_FILE%
set CATALINA_BASE=%OUT_DIR%
