package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.admin.forms.IndustryAliases;
import com.newposition.admin.service.NpAdminIndustryManagementService;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;

@Controller
@Template(title = "Industry Management", id = "newPositionModule:components/industryManagementComponent")
public class NpAdminIndustryManagementComponent
{
    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private NpAdminIndustryManagementService npAdminIndustryManagementService;

    @RequestMapping(value = "/industryManagement", method = RequestMethod.GET)
    public String getIndustryManagementPage(HttpServletRequest request, Model model)
    {
        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = request.getParameter("sortBy");
        String sectorFilter = request.getParameter("sectorFilter");
        String industryTypeFilter = request.getParameter("industryTypeFilter");
        String validFilter = request.getParameter("validFilter");

        String toolTypeFilter = request.getParameter("toolTypeFilter");
        String _sectorFilter = sectorFilter, _industryTypeFilter = industryTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(industryTypeFilter) && industryTypeFilter.equals("0"))
        {
            toolTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(sectorFilter) && sectorFilter.equals("0"))
        {
            sectorFilter = "";
        }

        List npEmployersResultSet = npAdminIndustryManagementService.getFilteredNpIndustries(sortBy, industryTypeFilter,
                validFilter, sectorFilter, pageNumber);

        List<NpEmployerImpl> npEmployers = (List<NpEmployerImpl>) npEmployersResultSet.get(0);
        int npEmployersCount = (int) npEmployersResultSet.get(1);

        model.addAttribute("industryTypeFilter", _industryTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("sectorFilter", _sectorFilter);
        model.addAttribute("sortBy", sortBy);

        double noOfPages = (double) npEmployersCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);
        model.addAttribute("currentPageNumber", pageNumber);
        model.addAttribute("npEmploymentRolesTotalPages", (int) noOfPages);

        model.addAttribute("npEmployers", npEmployers);
        model.addAttribute("npEmployersCount", npEmployersCount);

        model.addAttribute("npIndustryTypes", npAdminIndustryManagementService.getIndustryTypes());
        model.addAttribute("coSources", npAdminIndustryManagementService.getcoSources());
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("addNpIndustriesForm", new AddNpIndustriesForm());
        model.addAttribute("industryAliases", new IndustryAliases());

        return "components/industryManagementComponent.jsp";
    }

    @RequestMapping(value = "/industryManagement", method = RequestMethod.POST)
    public String getIndustryManagementPage(@ModelAttribute("addNpIndustriesForm") AddNpIndustriesForm addNpIndustriesForm,
                                            HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpIndustriesForm.getMergeToId()))
            {
                npAdminIndustryManagementService.mergeNpIndustry(addNpIndustriesForm);
                model.addAttribute("showMergeMsg", true);
            }
            else if (addNpIndustriesForm.isDeleteIndustry())
            {
                model.addAttribute("showDelMsg", true);
                npAdminIndustryManagementService.deletNpIndustries(addNpIndustriesForm.getIndustryId());
            }
            else
            {
                npAdminIndustryManagementService.saveNpIndustries(addNpIndustriesForm);
                if (addNpIndustriesForm.getIndustryId().isEmpty())
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:industryManagement";
        }
        catch (Exception e)
        {
            return "redirect:industryManagement";
        }
    }
}
