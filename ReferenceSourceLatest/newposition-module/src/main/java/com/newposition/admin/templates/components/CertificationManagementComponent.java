/**
 *
 */
package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpCertificationForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.CertificationManagementService;
import com.newposition.common.domain.NpInstituteCertificate;

/**
 * @author Yamuna Vemula
 */

@Controller
@Template(title = "CertificationManagement Form", id = "newPositionModule:components/certificationManagement")
public class CertificationManagementComponent
{
    protected static final Logger LOG = Logger.getLogger(CertificationManagementComponent.class);

    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private CertificationManagementService certificationManagementService;

    @RequestMapping(value = "/certificationManagement", method = RequestMethod.GET)
    public String getCertificationManagementPage(HttpServletRequest request, Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = request.getParameter("sortBy");
        String instituteFilter = request.getParameter("instituteFilter");
        String certificationTypeFilter = request.getParameter("certificationTypeFilter");
        String validFilter = request.getParameter("validFilter");

        String _instituteFilter = instituteFilter, _certificationTypeFilter = certificationTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(certificationTypeFilter) && certificationTypeFilter.equals("0"))
        {
            certificationTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(instituteFilter) && instituteFilter.equals("0"))
        {
            instituteFilter = "";
        }
        List npInstituteCertificatesResult = certificationManagementService.getFilteredNpInstituteCertificate(sortBy, certificationTypeFilter, validFilter, instituteFilter, pageNumber);

        List<NpInstituteCertificate> npInstituteCertificates = (List<NpInstituteCertificate>) npInstituteCertificatesResult.get(0);
        int npCertificateCount = (int) npInstituteCertificatesResult.get(1);

        model.addAttribute("certificationTypeFilter", _certificationTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("instituteFilter", _instituteFilter);
        model.addAttribute("sortBy", sortBy);

        double noOfPages = (double) npCertificateCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);
        model.addAttribute("currentPageNumber", pageNumber);
        model.addAttribute("npCertificationsTotalPages", (int) noOfPages);
        model.addAttribute("npCertificationsCount", npCertificateCount);

        model.addAttribute("npInstituteCertificates", npInstituteCertificates);
        model.addAttribute("npInstitutions", certificationManagementService.getNpInstitutions());
        model.addAttribute("npCertificationTypes", certificationManagementService.getCertificationTypes());
        model.addAttribute("coSources", certificationManagementService.getCoSources());
        model.addAttribute("sortBy", sortBy);

        model.addAttribute("addNpCertificationForm", new AddNpCertificationForm());
        model.addAttribute("certificationAliases", new CertificationAliases());

        return "components/certificationManagement.jsp";
    }

    @RequestMapping(value = "/certificationManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpCertificationForm") AddNpCertificationForm addNpCertificationForm, HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpCertificationForm.getMergeId()))
            {
                certificationManagementService.saveMergedNpInstituteCertificate(addNpCertificationForm);
                ;
                model.addAttribute("showMergeMsg", true);
            }
            else
            {
                certificationManagementService.saveNpCertifications(addNpCertificationForm);
                if (addNpCertificationForm.getNpInstituteCertificateId() == null)
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:certificationManagement";
        }
        catch (Exception e)
        {
            return "redirect:certificationManagement";
        }
    }
}
