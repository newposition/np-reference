package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.SkillAliases;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.NpSkillImpl;

/**
 * @author Trinadh
 */

@Controller
@Template(title = "Skill Management", id = "newPositionModule:components/skillManagementComponent")
public class SkillManagementComponent
{
    protected static final Logger LOG = Logger.getLogger(SkillManagementComponent.class);

    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private SkillManagementService skillManagementService;

    @RequestMapping(value = "/skillManagement", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = request.getParameter("sortBy");
        String domainFiter = request.getParameter("domainFiter");
        String skillTypeFilter = request.getParameter("skillTypeFilter");
        String validFilter = request.getParameter("validFilter");
        String _domainFiter = domainFiter, _skillTypeFilter = skillTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(skillTypeFilter) && skillTypeFilter.equals("0"))
        {
            skillTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(domainFiter) && domainFiter.equals("0"))
        {
            domainFiter = "";
        }
        List npSkillsSet = skillManagementService.getFilteredNpSkills(sortBy, skillTypeFilter, validFilter, domainFiter, pageNumber);
        List<NpSkillImpl> npSkills = (List<NpSkillImpl>) npSkillsSet.get(0);
        int npSkillsSize = (int) npSkillsSet.get(1);

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("skillTypeFilter", _skillTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("domainFiter", _domainFiter);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npSkillsSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npSkills", npSkills);
        model.addAttribute("npSkillsCount", npSkillsSize);
        model.addAttribute("npSkillsTotalPages", (int) noOfPages);
        model.addAttribute("npDomainTypes", skillManagementService.getDomainTypes());
        model.addAttribute("npSkillTypes", skillManagementService.getSkillTypes());
        model.addAttribute("coSources", skillManagementService.getcoSources());
        model.addAttribute("sortBy", sortBy);

        model.addAttribute("addNpSkillsForm", new AddNpSKillsForm());
        model.addAttribute("skillAliases", new SkillAliases());

        return "components/skillManagementComponent.jsp";
    }

    @RequestMapping(value = "/skillManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpSkillsForm") AddNpSKillsForm addNpSkillsForm, HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpSkillsForm.getMergeToId()))
            {
                skillManagementService.mergeNpSkill(addNpSkillsForm);
                model.addAttribute("showMergeMsg", true);
            }
            else if (addNpSkillsForm.isDeleteSkill())
            {
                skillManagementService.deletNpSkills(addNpSkillsForm.getSkillId());
                model.addAttribute("showDelMsg", true);
            }
            else
            {
                skillManagementService.saveNpSkills(addNpSkillsForm);
                if (addNpSkillsForm.getSkillId().isEmpty())
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:skillManagement";
        }
        catch (Exception e)
        {
            return "redirect:skillManagement";
        }
    }
}
