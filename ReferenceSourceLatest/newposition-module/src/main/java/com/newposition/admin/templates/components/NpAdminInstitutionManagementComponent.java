
package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpInstitutionForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.NpAdminInstitutionManagementService;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.NpEmploymentRoleImpl;

/**
 * @author Yamuna Vemula
 */

@Controller
@Template(title = "Institution Management Form", id = "newPositionModule:components/institutionManagement")
public class NpAdminInstitutionManagementComponent
{
    protected static final Logger LOG = Logger.getLogger(NpAdminInstitutionManagementComponent.class);

    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private NpAdminInstitutionManagementService institutionManagementService;

    @RequestMapping(value = "/institutionManagement", method = RequestMethod.GET)
    public String getQualificationManagementPage(HttpServletRequest request, Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }
        String sortBy = request.getParameter("sortBy");
        String instituteFilter = request.getParameter("instituteFilter");
        String instituteTypeFilter = request.getParameter("instituteTypeFilter");
        String validFilter = request.getParameter("validFilter");

        String _instituteFilter = instituteFilter, _instituteTypeFilter = instituteTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(instituteTypeFilter) && instituteTypeFilter.equals("0"))
        {
            instituteTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(instituteFilter) && instituteFilter.equals("0"))
        {
            instituteFilter = "";
        }

        List npInstitutionsResult = institutionManagementService.getFilteredNpInstitutes(sortBy, instituteTypeFilter, validFilter, instituteFilter, pageNumber);

        List<NpInstitutions> npInstitutions = (List<NpInstitutions>) npInstitutionsResult.get(0);
        int npInstitutionsCount = (int) npInstitutionsResult.get(1);
        double noOfPages = (double) npInstitutionsCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npInstitutions", npInstitutions);
        model.addAttribute("npInstitutionsCount", npInstitutionsCount);
        model.addAttribute("npInstitutionsTotalPages", (int) noOfPages);
        model.addAttribute("currentPageNumber", pageNumber);

        model.addAttribute("instituteTypeFilter", _instituteTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("instituteFilter", _instituteFilter);
        model.addAttribute("sortBy", sortBy);

        model.addAttribute("npInstituteNames", institutionManagementService.getInstituteNames(npInstitutions));
        model.addAttribute("npInstituteTypes", institutionManagementService.getInstituteTypes(npInstitutions));
        model.addAttribute("coSources", institutionManagementService.getCoSources());

        model.addAttribute("addNpInstitutionForm", new AddNpInstitutionForm());
        model.addAttribute("certificateAliases", new CertificationAliases());

        return "components/institutionManagement.jsp";
    }

    @RequestMapping(value = "/institutionManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpcourseForm") AddNpInstitutionForm addNpInstitutionForm, HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpInstitutionForm.getMergeId()))
            {
                institutionManagementService.saveMergedNpInstitute(addNpInstitutionForm);
                ;
                model.addAttribute("showMergeMsg", true);
            }
            else
            {
                institutionManagementService.saveNpInstitute(addNpInstitutionForm);
                if (addNpInstitutionForm.getInstituteId() == null)
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:institutionManagement";
        }
        catch (Exception e)
        {
            return "redirect:institutionManagement";
        }
    }
}

