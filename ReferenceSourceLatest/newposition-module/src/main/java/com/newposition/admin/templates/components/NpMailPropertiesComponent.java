package com.newposition.admin.templates.components;

import java.util.List;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.common.domain.NpMailConfigProp;
import com.newposition.infra.mail.NpMailService;

@Controller
@Template(title = "Male Properties", id = "newPositionModule:components/npMailPropertiesComponent")
public class NpMailPropertiesComponent
{

    @Autowired
    NpMailService npMailService;

    @RequestMapping(value = "/mailProperties", method = RequestMethod.GET)
    public String getMailProperties(HttpServletRequest request, Model model, @ModelAttribute("npMailConfigProp") NpMailConfigProp npMailConfigProp)
    {
    /*	model.addAttribute("npMailConfigProp",  new NpMailConfigProp());*/
        return "components/npMailPropertiesComponent.jsp";
    }

    /**
     * @param request
     * @param model
     * @param npMailConfigProp
     * @return
     */
    @RequestMapping(value = "/mailProperties", method = RequestMethod.POST)
    public String saveMailProperties(HttpServletRequest request, Model model, NpMailConfigProp npMailConfigProp, HttpSession session)
    {
        System.out.println("in post method");
        System.out.println("var=" + npMailConfigProp.getNpMailUserName());
        System.out.println("value=" + npMailConfigProp.getNpMailPassword());
        List<String> mailPropVariableList = npMailService.getNpMailPropVariables();
        int refCount = 0;
        String variable = npMailConfigProp.getNpMailUserName().trim();
        String value = npMailConfigProp.getNpMailPassword().trim();
        for (int i = 0; i < mailPropVariableList.size(); i++)
        {
            System.out.println("mailPropVariableList.get(i)=" + mailPropVariableList.get(i).toString());
            if (npMailConfigProp.getNpMailUserName().trim().equals(mailPropVariableList.get(i).toString().trim()))
            {
                System.out.println(" in if in for");
                refCount++;
            }
        }
        System.out.println("before if refCount=" + refCount);
        if (refCount == 0)
        {
            model.addAttribute("error1", "Variable Does Not Exits");
            session.setAttribute("error1", "Variable Does Not Exits");
            model.addAttribute("npMailConfigProp", npMailConfigProp);
            return "components/npMailPropertiesComponent.jsp";
        }
        System.out.println("refCount=" + refCount);
        try
        {
            npMailService.saveMailProp(variable, value);
            model.addAttribute("error1", "Value Successfully Updated");
            session.setAttribute("error1", "Value Successfully Updated");
        }
        catch (Exception e)
        {
            model.addAttribute("error1", "Value Not Updated");
            session.setAttribute("error1", "Value Not Updated");
        }

        return "redirect:testProperties";
    }
}
