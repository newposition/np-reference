/**
 *
 */
package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpQualificationForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.QualificationManagementService;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.common.domain.NpEmploymentRoleImpl;

/**
 * @author Yamuna Vemula
 */

@Controller
@Template(title = "Qualification Management Form", id = "newPositionModule:components/qualificationManagement")
public class QualificationManagementComponent
{
    protected static final Logger LOG = Logger
            .getLogger(QualificationManagementComponent.class);
    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private QualificationManagementService qualificationManagementService;

    @RequestMapping(value = "/qualificationManagement", method = RequestMethod.GET)
    public String getQualificationManagementPage(HttpServletRequest request,
                                                 Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = request.getParameter("sortBy");
        String instituteFilter = request.getParameter("instituteFilter");
        String courseTypeFilter = request.getParameter("courseTypeFilter");
        String validFilter = request.getParameter("validFilter");

        String _instituteFilter = instituteFilter, _courseTypeFilter = courseTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(courseTypeFilter) && courseTypeFilter.equals("0"))
        {
            courseTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(instituteFilter) && instituteFilter.equals("0"))
        {
            instituteFilter = "";
        }

        List npInstituteCoursesResult = qualificationManagementService
                .getFilteredNpInstituteCourse(sortBy, courseTypeFilter,
                        validFilter, instituteFilter, pageNumber);

        List<NpInstituteCourse> npInstituteCourses = (List<NpInstituteCourse>) npInstituteCoursesResult.get(0);
        int npInstituteCourseCount = (int) npInstituteCoursesResult.get(1);

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("courseTypeFilter", _courseTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("instituteFilter", _instituteFilter);

        double noOfPages = (double) npInstituteCourseCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("currentPageNumber", pageNumber);
        model.addAttribute("npInstituteCourseCount", npInstituteCourseCount);
        model.addAttribute("npInstituteCourseTotalPages", (int) noOfPages);

        model.addAttribute("npInstituteCourses", npInstituteCourses);
        model.addAttribute("npInstitutions",
                qualificationManagementService.getNpInstitutions());
        model.addAttribute("npCourseTypes",
                qualificationManagementService.getCourseTypes());
        model.addAttribute("coSources",
                qualificationManagementService.getCoSources());

        model.addAttribute("addNpQualificationForm",
                new AddNpQualificationForm());
        model.addAttribute("certificateAliases", new CertificationAliases());

        return "components/qualificationManagement.jsp";
    }

    @RequestMapping(value = "/qualificationManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpcourseForm") AddNpQualificationForm addNpQualificationForm,
                           HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpQualificationForm.getMergeId()))
            {
                qualificationManagementService
                        .saveMergedNpInstituteCourse(addNpQualificationForm);
                ;
                model.addAttribute("showMergeMsg", true);
                // return getQualificationManagementPage(request,model);
            }
            else
            {
                qualificationManagementService
                        .saveNpInstituteCourse(addNpQualificationForm);
                if (addNpQualificationForm.getNpInstituteCourseId() == null)
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:qualificationManagement";
        }
        catch (Exception e)
        {
            return "redirect:qualificationManagement";
        }
    }
}
