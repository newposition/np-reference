package com.newposition.admin.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.RoleAliases;
import com.newposition.admin.forms.SkillAliases;
import com.newposition.admin.service.RoleManagementService;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;

/**
 * @author Trinadh
 */

@Controller
@Template(title = "Role Management", id = "newPositionModule:components/roleManagementComponent")
public class RoleManagementComponent
{
    protected static final Logger LOG = Logger.getLogger(RoleManagementComponent.class);
    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private RoleManagementService roleManagementService;

    @RequestMapping(value = "/roleManagement", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }
        String sortBy = request.getParameter("sortBy");
        String domainFiter = request.getParameter("domainFiter");
        String roleNameFilter = request.getParameter("roleNameFilter");
        String validFilter = request.getParameter("validFilter");
        String _domainFiter = domainFiter, _roleNameFilter = roleNameFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(roleNameFilter) && roleNameFilter.equals("0"))
        {
            roleNameFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(domainFiter) && domainFiter.equals("0"))
        {
            domainFiter = "";
        }
        List npEmploymentResultSet = roleManagementService.getFilteredNpEmploymentRoles(sortBy, roleNameFilter, validFilter, domainFiter, pageNumber);
        List<NpEmploymentRoleImpl> npEmploymentRoles = (List<NpEmploymentRoleImpl>) npEmploymentResultSet.get(0);
        int npEmploymentRolesCount = (int) npEmploymentResultSet.get(1);

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("roleNameFilter", _roleNameFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("domainFiter", _domainFiter);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npEmploymentRolesCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npEmploymentRoles", npEmploymentRoles);
        model.addAttribute("npEmploymentRolesCount", npEmploymentRolesCount);
        model.addAttribute("npEmploymentRolesTotalPages", (int) noOfPages);

        model.addAttribute("npDomainTypes", roleManagementService.getDomainTypes());
        model.addAttribute("npEmploymentRolesNames", roleManagementService.getNpEmploymentRoleNames());
        model.addAttribute("coSources", roleManagementService.getcoSources());
        model.addAttribute("sortBy", sortBy);

        model.addAttribute("addNpEmploymentRoleForm", new AddNpEmploymentRoleForm());
        model.addAttribute("roleAliases", new RoleAliases());

        return "components/roleManagementComponent.jsp";
    }

    @RequestMapping(value = "/roleManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpEmploymentRoleForm") AddNpEmploymentRoleForm addNpEmploymentRoleForm, HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpEmploymentRoleForm.getMergeToId()))
            {
                roleManagementService.mergeNpEmploymentRole(addNpEmploymentRoleForm);
                model.addAttribute("showMergeMsg", true);
            }
            else if (addNpEmploymentRoleForm.isDeleteRole())
            {
                roleManagementService.deletNpEmploymentRoles(addNpEmploymentRoleForm.getRoleId());
                model.addAttribute("showDelMsg", true);
            }
            else
            {
                roleManagementService.saveNpEmploymentRoles(addNpEmploymentRoleForm);
                if (addNpEmploymentRoleForm.getRoleId().isEmpty())
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:roleManagement";
        }
        catch (Exception e)
        {
            LOG.info("Exception Occured in Role" + e.getMessage());
            return "redirect:roleManagement";
        }
    }
}
