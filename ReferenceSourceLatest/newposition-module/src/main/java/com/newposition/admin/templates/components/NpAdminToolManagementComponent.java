package com.newposition.admin.templates.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.Template;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.admin.forms.ToolAliases;
import com.newposition.admin.service.NpAdminToolManagementService;
import com.newposition.common.domain.NpToolImpl;

@Controller
@Template(title = "Tool Management", id = "newPositionModule:components/toolManagementComponent")
public class NpAdminToolManagementComponent
{

    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @Autowired
    private NpAdminToolManagementService npAdminToolManagementService;

    @RequestMapping(value = "/toolManagement", method = RequestMethod.GET)
    public String getToolManagementPage(HttpServletRequest request, Model model)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = request.getParameter("sortBy");
        String domainFiter = request.getParameter("domainFiter");
        String toolTypeFilter = request.getParameter("toolTypeFilter");
        String validFilter = request.getParameter("validFilter");
        String _domainFiter = domainFiter, _toolTypeFilter = toolTypeFilter, _validFilter = validFilter;
        if (StringUtils.isNotEmpty(toolTypeFilter) && toolTypeFilter.equals("0"))
        {
            toolTypeFilter = "";
        }
        if (StringUtils.isNotEmpty(validFilter) && validFilter.equals("0"))
        {
            validFilter = "";
        }
        if (StringUtils.isNotEmpty(domainFiter) && domainFiter.equals("0"))
        {
            domainFiter = "";
        }
        List npToolsSet = npAdminToolManagementService.getFilteredNpTools(sortBy, toolTypeFilter, validFilter, domainFiter, pageNumber);
        List<NpToolImpl> npTools = (List<NpToolImpl>) npToolsSet.get(0);
        int npToolsCount = (int) npToolsSet.get(1);

        model.addAttribute("sortBy", sortBy);
        model.addAttribute("toolTypeFilter", _toolTypeFilter);
        model.addAttribute("validFilter", _validFilter);
        model.addAttribute("domainFiter", _domainFiter);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npToolsCount / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npTools", npTools);
        model.addAttribute("npToolsCount", npToolsCount);
        model.addAttribute("npToolsTotalPages", (int) noOfPages);
        model.addAttribute("currentPageNumber", pageNumber);

        model.addAttribute("npToolsTypes", npAdminToolManagementService.getToolTypes());
        model.addAttribute("coSources", npAdminToolManagementService.getcoSources());

        model.addAttribute("addNpToolsForm", new AddNpToolsForm());
        model.addAttribute("toolAliases", new ToolAliases());

        return "components/toolManagementComponent.jsp";
    }

    @RequestMapping(value = "/toolManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("addNpToolsForm") AddNpToolsForm addNpToolsForm, HttpServletRequest request, Model model)
    {
        try
        {
            if (StringUtils.isNotBlank(addNpToolsForm.getMergeToId()))
            {
                npAdminToolManagementService.mergeNpTool(addNpToolsForm);
                model.addAttribute("showMergeMsg", true);
            }
            else if (addNpToolsForm.isDeleteTool())
            {
                npAdminToolManagementService.deletNpTools(addNpToolsForm.getToolId());
                model.addAttribute("showDelMsg", true);
            }
            else
            {
                npAdminToolManagementService.saveNpTools(addNpToolsForm);
                if (addNpToolsForm.getToolId().isEmpty())
                {
                    model.addAttribute("showAddMsg", true);
                }
                else
                {
                    model.addAttribute("showEditMsg", true);
                }
            }
            return "redirect:toolManagement";
        }
        catch (Exception e)
        {
            return "redirect:toolManagement";
        }
    }
}
