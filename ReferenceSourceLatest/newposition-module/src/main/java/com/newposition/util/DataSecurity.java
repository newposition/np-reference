package com.newposition.util;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import java.security.spec.InvalidKeySpecException;

//import javax.crypto.*;
import sun.misc.*;

public class DataSecurity
{

    private static final String ALGO = "AES";
    private static final byte[] keyValue =
            new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't',

                    'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};

    @SuppressWarnings("restriction")
    public static String encrypt(String Data) throws Exception
    {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);
        String urlEncodeddata = URLEncoder.encode(encryptedValue, "UTF-8");
        return urlEncodeddata;
    }

    @SuppressWarnings("restriction")
    public static String decrypt(String encryptedData) throws Exception
    {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        String urlDecodedData = URLDecoder.decode(encryptedData, "UTF-8");
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(urlDecodedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static Key generateKey() throws Exception
    {
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }
}
