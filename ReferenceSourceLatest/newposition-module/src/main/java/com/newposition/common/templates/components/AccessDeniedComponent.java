package com.newposition.common.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author ravi
 */
@Controller
@Template(title = "Access Denied", id = "newPositionModule:components/access_denied_component")
public class AccessDeniedComponent
{

    @RequestMapping(value = "/access_denied", method = RequestMethod.GET)
    public String renderAccessDeniedPage()
    {
        return "components/accessDeniedComponent.jsp";
    }
}
