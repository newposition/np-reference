package com.newposition.common.templates.components;

import java.util.Collection;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.common.forms.LoginForm;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "Login Form", id = "newPositionModule:components/login")
public class LoginFormComponent
{

    @Autowired
    private NpUserService npUserService;

    @Value("${email.images.context}")
    private String imageContext;

    @Value("${unlockAccountLink}")
    private String unlockAccountLink;

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String render(@ModelAttribute LoginForm loginForm, HttpServletRequest request, Model model, Authentication authentication)
    {

        // avoiding the error message in session attribute
        if (authentication != null && request.getSession().getAttribute("user") != null)
        {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

            for (GrantedAuthority grantedAuthority : authorities)
            {
                if (grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_NP_USERSUPPORT") || grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_SKILLS_ANALYST") || grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_CLIENT_LIASON"))
                {
                    return "redirect:adminClientLiason";
                }
                else if (grantedAuthority.getAuthority().equals("ROLE_CANDIDATE") || grantedAuthority.getAuthority().equals("REGISTERED_USER"))
                {
                    return "redirect:basicDashboard";
                }
                else if (grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_ADMINISTRATOR") ||
                        grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_INTERVIEWER") ||
                        grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_MANAGER") ||
                        grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_EMPLOYEE") ||
                        grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_SUPPORT"))
                {
                    return "redirect:clientDashboard";
                }
                if (grantedAuthority.getAuthority().startsWith("ROLE_NP_USERSUPPORT") || grantedAuthority.getAuthority().startsWith("ROLE_SKILLS_ANALYST") || grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_LIASON"))
                {
                    return "redirect:adminClientLiason";
                }
            }
        }
        if (request.getSession().getAttribute("check") == null)
        {
            request.getSession().setAttribute("errmsgUsn", "");
            request.getSession().setAttribute("errmsgPwd", "");
            request.getSession().setAttribute("errmsg", "");
            request.setAttribute("lock", false);
        }
        if (request.getSession().getAttribute("check") != null && !request.getSession().getAttribute("check").equals("check"))
        {
            request.getSession().setAttribute("errmsgUsn", "");
            request.getSession().setAttribute("errmsgPwd", "");
            request.getSession().setAttribute("errmsg", "");
            if (request.getSession().getAttribute("check").equals("locked"))
            {
                request.setAttribute("lock", true);
            }
            else if (request.getSession().getAttribute("check").equals("disabled"))
            {
                request.setAttribute("disabled", true);
            }
        }
        request.getSession().setAttribute("check", "");

        if (request.getParameter("recentEmail") != null)
        {
            npUserService.resendUnockAccountLink(request.getParameter("recentEmail").trim(), unlockAccountLink, imageContext);
            request.setAttribute("successmsg", "Link has been sent.");
            request.setAttribute("lock", false);
        }

        // unlock account link associated id
        String unlockId = request.getParameter("id");
        if (unlockId != null)
        {

            String key = new String(DatatypeConverter.parseBase64Binary(unlockId));

            String timeParam = new String(DatatypeConverter.parseBase64Binary(request.getParameter("dur")));
            Long sentTime = Long.parseLong(timeParam);

            boolean check = npUserService.unlockAccount(new Integer(key));
            if (npUserService.isLinkExpired(sentTime) || check)
            {
                model.addAttribute("successmsg", "Your Account has been Activated. Please login with your valid credentials");
                return "components/loginForm.jsp";
            }
            else
            {
                request.getSession().setAttribute("bodyText", "Link has expired");
                return "redirect:sucessPage";
            }
        }

        model.addAttribute("loginForm", new LoginForm());
        return "components/loginForm.jsp";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String rendering()
    {
        return "components/loginForm.jsp";
    }
}
