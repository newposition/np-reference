package com.newposition.common.templates.components;

import java.io.File;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.NpProgram;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpProgrammeService;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title = "Programme Component", id = "newPositionModule:components/npProgramme")
public class NpProgrammeComponent
{

    @Autowired
    NpProgrammeService npProgrammeService;

    @Autowired
    NpClientService npClientService;

    protected static final Logger LOG = Logger
            .getLogger(NpProgrammeComponent.class);

    @RequestMapping(value = "/programmeComponent", method = RequestMethod.GET)
    public String displayProgramme(Model model, HttpSession session, Authentication authentication)
    {
        System.out.println(" @@@@@@@@@@@@@@@@@@@@@@In Programme get method");
        NpProgram npProgramme = new NpProgram();
        model.addAttribute("npProgramme", npProgramme);

//	NpClient client = npClientService.getCompanyOfCurrentUser(authentication.getName());
//	model.addAttribute("p_videos",npProgrammeService.getAllClientProgramsWithVideo(client));

        model.addAttribute("message", getMessage((String) session.getAttribute("cmsg")));
        session.setAttribute("cmsg", null);
        return "components/npProgramme.jsp";
    }

    private String getMessage(String msg)
    {
        if (StringUtils.isNotEmpty(msg))
        {
            if (StringUtils.equals(msg, "1"))
            {
                return "Program Added Successfully";
            }
            else if (StringUtils.equals(msg, "2"))
            {
                return "Program Edited Successfully";
            }
        }

        return null;
    }

    @RequestMapping(value = "/programmeComponent", method = RequestMethod.POST)
    public String saveNewProjects(NpProgram npProgram, NpClientCostCentreImpl npClientCostCentreImpl, Model model, HttpSession session)
    {
        System.out.println("in post method");
        System.out.println("cost center id=" + npClientCostCentreImpl.getCostCentreId());
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        npProgram.setNpClientCostCentreImpl(npClientCostCentreImpl);
        npProgram.setNpClient(npClient);

        /**
         *
         * getting the video file from the Magnolia context
         *
         *
         * */
        File video = null;
        byte[] videoInByte = null;
        try
        {
            video = MgnlContext.getPostedForm().getDocument("video").getFile();
            videoInByte = npProgrammeService.getVideo(video);
            if (video != null)
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  file uploaded  "
                        + video.getName());
            }
            else
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    file not available");
            }
        }
        catch (NullPointerException npe)
        {
            System.out.println("Getting File or Video as Null");
        }

        if (npProgram.getClientProgramID() == null)
        {
            System.out.println("program Document new ");
            NpClientProgramDocument proDoc = new NpClientProgramDocument();
            proDoc.setDocument(videoInByte);

            npProgrammeService.saveProgramme(npProgram, proDoc);
            session.setAttribute("cmsg", "1");
        }
        else
        {

            NpProgram program = npProgrammeService.getNpProgrammeById(npProgram.getClientProgramID().longValue());
            if (videoInByte != null)
            {
                NpClientProgramDocument doc = program.getProgramDoc();

                doc.setDocument(videoInByte);
                program.setProgramDoc(doc);
            }
            NpProgram _program = npProgrammeService.updateWithNewValue(npProgram, program);

            npProgrammeService.saveOrUpdate(_program);
            session.setAttribute("cmsg", "2");
        }

        return "redirect:npProgramme";
    }
}
