/**
 *
 */
package com.newposition.common.templates.components;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/*
 * @author ravi
 *
 *
 */
@Controller
@Template(title = "Footer Component", id = "newPositionModule:components/footerComponent")
@TemplateDescription("")
public class FooterComponent
{

    @RequestMapping(value = "/footer", method = RequestMethod.GET)
    public String viewFooter()
    {
        return "components/footerComponent.jsp";
    }

    @RequestMapping(value = "/footer", method = RequestMethod.POST)
    public String viewPostFooter()
    {
        return "components/footerComponent.jsp";
    }
}
