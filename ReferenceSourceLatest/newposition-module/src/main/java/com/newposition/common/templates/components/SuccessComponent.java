/**
 *
 */
package com.newposition.common.templates.components;

import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * @author sachi
 *
 *
 */

@Controller
@Template(title = "Success Component", id = "newPositionModule:components/successComponent")
@TemplateDescription("")
public class SuccessComponent
{

    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String getSuccessPage(HttpServletRequest request)
    {

        return "components/successComponent.jsp";
    }
}
