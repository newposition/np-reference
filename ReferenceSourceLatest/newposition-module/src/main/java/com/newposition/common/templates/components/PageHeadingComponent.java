package com.newposition.common.templates.components;

import info.magnolia.module.blossom.annotation.TabFactory;
import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.ui.form.config.TabBuilder;
import info.magnolia.ui.framework.config.UiConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Page Heading Component.
 */
@Controller
@Template(title = "Page Heading", id = "newPositionModule:components/pageHeading")
public class PageHeadingComponent
{

    @RequestMapping(value = "/pageHeading", method = RequestMethod.GET)
    public String viewPageHeading()
    {
        return "components/pageHeadingComponent.jsp";
    }

    @RequestMapping(value = "/pageHeading", method = RequestMethod.POST)
    public String viewPageHeadingPost()
    {
        return "components/pageHeadingComponent.jsp";
    }

    @TabFactory("Content")
    public void contentTab(UiConfig cfg, TabBuilder tab)
    {
        tab.fields(
                cfg.fields.text("title").label("Title"),
                cfg.fields.text("subtitle").label("SubTitle"),
                cfg.fields.text("css").label("css")
        );
    }
}
