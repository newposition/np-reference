package com.newposition.common.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpEnquiry;
import com.newposition.common.forms.ContactUsForm;
import com.newposition.common.service.NpEnquiryReasonService;
import com.newposition.infra.mail.NpMailService;

/**
 * @author ravi
 */
@Controller
@Template(title = "Contact Form", id = "newPositionModule:components/contact_us_component")
public class ContactUsComponent
{

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpEnquiryReasonService npEnquiryReasonService;

    @RequestMapping(value = "/contact_us_form", method = RequestMethod.GET)
    public String render(@ModelAttribute("contactUs") ContactUsForm contactUs, HttpServletRequest request)
    {
        List<NpEnquiry> reasons = npEnquiryReasonService.getReasonsForEnquiry();
        request.setAttribute("reasons", reasons);
        return "components/contact_us_form.jsp";
    }

    @RequestMapping(value = "/contact_us_form", method = RequestMethod.POST)
    public String rendering(@ModelAttribute("contactUs") @Valid ContactUsForm contactUs, BindingResult result, HttpServletRequest request)
    {

        if (result.hasErrors())
        {
            List<NpEnquiry> reasons = npEnquiryReasonService.getReasonsForEnquiry();
            request.setAttribute("reasons", reasons);
            return "components/contact_us_form.jsp";
        }

        Context context = new Context();
        context.setVariable("template", "contactus-mail.html");
        context.setVariable("firstName", contactUs.getFirstName());
        context.setVariable("lastName", contactUs.getLastName());
        context.setVariable("reasonForEnquiry", contactUs.getReasonForEnquiry());
        context.setVariable("company", contactUs.getCompany());
        context.setVariable("enquiry", contactUs.getEnquiry());
        context.setVariable("emailAddress", contactUs.getPrimaryEmailAddress());
        String information = "Contact Request";

        npMailService.sendMail(information, contactUs.getPrimaryEmailAddress(), context);
        request.getSession().setAttribute("headerText", "Thank You For Contacting us, we will get back to you within 48 hours.");
        request.getSession().setAttribute("bodyText", "");

        return "redirect:sucessPage";
    }
}