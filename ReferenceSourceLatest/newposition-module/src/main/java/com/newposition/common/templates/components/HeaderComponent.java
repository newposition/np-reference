/**
 *
 */
package com.newposition.common.templates.components;

import info.magnolia.module.blossom.annotation.Template;
import info.magnolia.module.blossom.annotation.TemplateDescription;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.common.forms.LoginForm;

/*
 * @author ravi
 *
 *
 */
@Controller
@Template(title = "Header Component", id = "newPositionModule:components/headerComponent")
@TemplateDescription("")
public class HeaderComponent
{

    @RequestMapping(value = "/header", method = RequestMethod.GET)
    public String viewHeader(Model model)
    {
        model.addAttribute("loginForm", new LoginForm());
        return "components/headerComponent.jsp";
    }

    @RequestMapping(value = "/header", method = RequestMethod.POST)
    public String viewPostHeader()
    {
        return "components/headerComponent.jsp";
    }
}
