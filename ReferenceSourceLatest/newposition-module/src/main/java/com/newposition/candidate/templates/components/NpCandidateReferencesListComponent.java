package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.client.templates.components.NpClientSupportImpersonateUserComponent;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;
import com.newposition.utils.CompareByFirstName;

@Controller
@Template(title = "references list component", id = "newPositionModule:components/referencesListComponent")
public class NpCandidateReferencesListComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpClientSupportImpersonateUserComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/referencesList", method = RequestMethod.GET)
    public String render(Model model, Authentication authentication, HttpServletRequest request)
    {

        NpUser npUser = null;
        List<NpCandidateReference> npCandidateReferences = null;
        try
        {
            npUser = npUserService.findByUserName(authentication.getName());
        }
        catch (Exception exception)
        {
            LOG.error(exception);
            return "redirect:home";
        }
//    	request.getParameter("sortBy")
        npCandidateReferences = npCandidateService.getCandidateReferenceList(authentication.getName());
        LOG.info(npCandidateReferences.size());
        List<NpUser> users = new ArrayList<NpUser>();
        for (NpCandidateReference candidateReference : npCandidateReferences)
        {
            users.add(candidateReference.getNpCandidate());
        }
        if (request.getParameter("sortBy") != null)
        {
            java.util.Collections.sort(users, new CompareByFirstName());
            model.addAttribute("sortBy", request.getParameter("sortBy"));
        }
        model.addAttribute("candidateReferences", users);
        return "components/referencesListComponent.jsp";
    }

    @RequestMapping(value = "/referencesList", method = RequestMethod.POST)
    public String rendering(HttpServletRequest request, ModelMap model)
    {
        System.out.println("posting referenceList form");
        String userId = request.getParameter("userId"), id = "";
        if (userId != null || !userId.equals(""))
        {
            id = DatatypeConverter.printBase64Binary(userId.getBytes());
        }
        return "redirect:candidateReferenceForm?id=" + id;
    }
}
