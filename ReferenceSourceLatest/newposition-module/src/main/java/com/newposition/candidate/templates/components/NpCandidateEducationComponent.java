package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateEducationForm;
import com.newposition.candidate.service.NpCandidateEducationRegisterService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "educationRegisterForm", id = "newPositionModule:components/NpCandidateEducationComponent")
public class NpCandidateEducationComponent
{

    @Autowired
    private NpCandidateEducationRegisterService npCandidateEducationRegisterService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/myEducation", method = RequestMethod.GET)
    public String render(@ModelAttribute("educationForm") NpCandidateEducationForm npCandidateEducationForm, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate != null)
        {
            List<NpCandidateEducationForm> educationHistories = npCandidateEducationRegisterService.getEducationHistories(npCandidate.getId());
            request.setAttribute("educationHistories", educationHistories);

            List<NpCandidateEducationForm> candidateCertificates = npCandidateEducationRegisterService.getCandidateCertificates(npCandidate.getId());
            request.setAttribute("candidateCertificates", candidateCertificates);

            List<NpCandidateEducationForm> candidateQualifications = npCandidateEducationRegisterService.getQualifications(npCandidate.getId());
            request.setAttribute("candidateQualifications", candidateQualifications);

            List<String> candidateLanguageNames = npCandidateEducationRegisterService.getCandidateLanguageNames(npCandidate.getId());
            request.setAttribute("candidateLanguageNames", candidateLanguageNames);

            List<String> candidateInterestNames = npCandidateEducationRegisterService.getCandidateInterestNames(npCandidate.getId());
            request.setAttribute("candidateInterestNames", candidateInterestNames);
        }

        return "components/npCandidateEducationComponent.jsp";
    }

    @RequestMapping(value = "/myEducation", method = RequestMethod.POST)
    public String rendering(@ModelAttribute("educationForm") NpCandidateEducationForm npCandidateEducationForm, HttpServletRequest request, ModelMap model)
    {
        return "components/npCandidateEducationComponent.jsp";
    }
}
