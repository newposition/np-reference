package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "candidate dashboard", id = "newPositionModule:components/candidateDashboardComponent")
public class NpCandidateDashboardComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateDashboardComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/candidateDashbord", method = RequestMethod.GET)
    public String render(Model model, Authentication authentication, HttpServletRequest request)
    {
        LOG.info("getting candidate dashboard page");
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        Integer referenceNo = (Integer) request.getSession().getAttribute("refereCandidateNo");
        model.addAttribute("refereCandidateNo", referenceNo);
        request.getSession().setAttribute("refereCandidateNo", null);
        if (npCandidate != null)
        {

            if (npCandidate.getNpCandidateDocument() != null)
            {
                try
                {
                    model.addAttribute("profileImage", new String(Base64.encodeBase64(npCandidate.getNpCandidateDocument().getPhoto())));
                }
                catch (NullPointerException npe)
                {
                    LOG.error("Candidate Document is Null");
                }
            }
        }
        return "components/candidateDashboardComponent.jsp";
    }

    @RequestMapping(value = "/candidateDashbord", method = RequestMethod.POST)
    public String rendering(HttpServletRequest request, ModelMap model)
    {
        return "components/candidateDashboardComponent.jsp";
    }
}
