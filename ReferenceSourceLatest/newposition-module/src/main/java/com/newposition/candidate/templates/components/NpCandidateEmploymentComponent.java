package com.newposition.candidate.templates.components;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.Template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.service.NpCandidateEmploymentService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpTool;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "Employment Component", id = "newPositionModule:components/candidateEmployment")
public class NpCandidateEmploymentComponent
{

    @Autowired
    private NpCandidateEmploymentService npCandidateEmploymentService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/npCandidateEmployment", method = RequestMethod.GET)
    public String getEmploymentPage(HttpServletRequest request, Model model, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate != null)
        {

            request.setAttribute("npCandidate", npCandidate);

            List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npCandidate.getId());

            request.setAttribute("npCandidateEmployers", npCandidateEmployers);

            model.addAttribute("npCandidateEmployerNo", npCandidateEmployers.size());

            if (npCandidateEmployers.size() > 0)
            {

                if (npCandidate.getNpCandidateSkills().size() > 0)
                {

                    NpCandidateSkills npCandidateSkill = npCandidate.getNpCandidateSkills().iterator().next();
                    if (npCandidateSkill != null)
                    {

                        if (npCandidateSkill.getNpSkill() != null && npCandidateSkill.getNpSkill().getNpDomain() != null)
                        {

                            Integer domainId = (npCandidateSkill.getNpSkill().getNpDomain().getId());

                            List<NpSkill> listOfNpSkills = npCandidateEmploymentService.getSkills(domainId.toString());
                            List<NpEmploymentRole> listOfNpRoles = npCandidateEmploymentService.getRoles(domainId.toString());
                            List<NpTool> listOfNpTools = npCandidateEmploymentService.getTools(domainId.toString());
                            request.setAttribute("listOfNpSkills", listOfNpSkills);
                            request.setAttribute("listOfNpRoles", listOfNpRoles);
                            request.setAttribute("listOfNpTools", listOfNpTools);
                            request.setAttribute("domainId", domainId);
                        }
                    }
                }
            }
            if (npCandidateEmployers.size() <= 1)
            {
                request.setAttribute("notShowNext", true);
            }

            if (npCandidateEmployers.size() > 0)
            {
                request.setAttribute("employer", npCandidateEmployers.get(0));
            }

            request.setAttribute("notShowPrev", true);
        }

        return "components/npCandidateEmployment.jsp";
    }
}
