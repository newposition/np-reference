package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "candidate reference form", id = "newPositionModule:components/candidateReferenceFormComponent")
public class NpCandidateReferenceFormComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateReferenceFormComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/candidateReference", method = RequestMethod.GET)
    public String render(Model model, Authentication authentication, HttpServletRequest request)
    {
        LOG.info("getting candidate Reference form page");
        String decodedId = "";
        NpUser user = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = null;
        if (request.getParameter("id") != null)
        {
            String id = (String) request.getParameter("id").trim();
            decodedId = new String(DatatypeConverter.parseBase64Binary(id));
            npCandidate = npCandidateService.findByID(new Integer(decodedId));

            System.out.println(request.getParameter("id") + " = " + decodedId);
        }
        else
        {
            System.out.println("id = null");
        }
        NpCandidateReference npCandidateReference = npCandidateService.getCurrentCandidateReference(user.getPrimaryEmail(), npCandidate.getId());
        System.out.println(npCandidateReference.getNpCandidate() + "\t" + npCandidateReference.getNpCandidate().getId() + " " + npCandidateReference.getDateFrom());
        model.addAttribute("refereCandidate", npCandidate);
        model.addAttribute("id", decodedId);
        model.addAttribute("npCandidateReference", npCandidateReference);

        return "components/candidateReferenceFormComponent.jsp";
    }

    @RequestMapping(value = "/candidateReference", method = RequestMethod.POST)
    public String rendering(Authentication authentication, HttpServletRequest request, ModelMap model)
    {

        NpUser user = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = null;
        NpCandidateReference npCandidateReference = null;
        if (request.getParameter("candidateId") != null)
        {
            npCandidate = npCandidateService.findByID(new Integer(request.getParameter("candidateId").trim()));
            npCandidateReference = npCandidateService.getCurrentCandidateReference(user.getPrimaryEmail(), npCandidate.getId());
            npCandidateReference.setReferenceVerified(request.getParameter("referenceVerified").trim());
        }
        npCandidateService.varifyReference(npCandidateReference);
        return "redirect:basicDashboard";
    }
}
