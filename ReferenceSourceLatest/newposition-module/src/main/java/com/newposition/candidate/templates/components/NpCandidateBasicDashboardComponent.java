package com.newposition.candidate.templates.components;

import java.util.List;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.NpCandidateEducationForm;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.client.templates.components.NpClientSupportImpersonateUserComponent;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "candidate basic dashboard", id = "newPositionModule:components/candidateBasicDashboardComponent")
public class NpCandidateBasicDashboardComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateBasicDashboardComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/basicDashbord", method = RequestMethod.GET)
    public String render(Model model, Authentication authentication, HttpServletRequest request)
    {
        System.out.println("getting basic dashboard page");
//    	NpUser npUser = npUserService.findByUserName("ravik@jarvisbs.com");authentication
        NpUser npUser = null;
        try
        {
            npUser = npUserService.findByUserName(authentication.getName());
        }
        catch (Exception exception)
        {
            LOG.error(exception);
            return "redirect:home";
        }
        List<NpCandidateReference> npCandidateReferences = npCandidateService.getCandidateReferenceList(authentication.getName());

        model.addAttribute("refereCandidateNo", npCandidateReferences.size());
        for (NpCandidateReference candidateReference : npCandidateReferences)
        {
            System.out.println(candidateReference.getNpCandidate().getFirstName());
        }
        model.addAttribute("user", npUser);
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        if (npCandidate.getNpCandidatePreference() != null)
        {
            request.getSession().setAttribute("refereCandidateNo", npCandidateReferences.size());
            return "redirect:candidateDashboard";
        }

        return "components/candidateBasicDashboardComponent.jsp";
    }

    @RequestMapping(value = "/basicDashbord", method = RequestMethod.POST)
    public String rendering(HttpServletRequest request, ModelMap model)
    {
        return "components/candidateBasicDashboardComponent.jsp";
    }
}
