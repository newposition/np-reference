package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateReferenceForm;
import com.newposition.candidate.forms.validator.NpCandidateReferenceFormValidator;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.candidate.service.NpCandidateSponsorService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "sponcerRegisterForm", id = "newPositionModule:components/NpCandidateSponsorFormComponent")
public class NpCandidateSponsorFormComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateSponsorFormComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpCandidateSponsorService npCandidateSponsorService;

    @Autowired
    private NpCandidateReferenceFormValidator npCandidateReferenceFormValidator;

    @RequestMapping(value = "/mySponsor", method = RequestMethod.GET)
    public String render(@ModelAttribute("npCandidateReferenceForm") NpCandidateReferenceForm npCandidateReferenceModel,
                         HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<NpCandidateReferenceForm> referenceList = new ArrayList<NpCandidateReferenceForm>();

        referenceList = npCandidateSponsorService.getListOfSponsors(npCandidate);

        request.getSession().setAttribute("referenceList", referenceList);
        LOG.info("Getting Sponsor Page" + referenceList.size());
        return "components/npCandidateSponsorFormComponent.jsp";
    }

    @RequestMapping(value = "/mySponsor", method = RequestMethod.POST)
    public String rendering(@ModelAttribute("npCandidateReferenceForm") NpCandidateReferenceForm npCandidateReferenceForm, BindingResult result,
                            HttpServletRequest request, ModelMap model, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        // if user does not edit sponsor details
        if (StringUtils.isEmpty(request.getParameter("edit")))
        {
            getNpCandidateReferenceFormValidator().validate(npCandidateReferenceForm, result);
            if (result.hasErrors())
            {
                model.put("npCandidateReferenceModel", npCandidateReferenceForm);
                return "components/npCandidateSponsorFormComponent.jsp";
            }

            if (notValidDate(npCandidateReferenceForm.getDateFrom(), npCandidateReferenceForm.getDateTo()))
            {
                if (StringUtils.isEmpty(npCandidateReferenceForm.getDateTo()))
                {
                    npCandidateReferenceForm.setDateTo("Current");
                }
                request.setAttribute("errDate", "Please enter a valid Dates in From and To.From Date should not be later than To date.");
                model.put("npCandidateReferenceModel", npCandidateReferenceForm);
                return "components/npCandidateSponsorFormComponent.jsp";
            }
        }
        else
        {

            if (request.getParameter("_email") == null || !getNpCandidateReferenceFormValidator().isValidEmail(request.getParameter("_email")))
            {

                request.setAttribute("spid", request.getParameter("spid").toString());
                request.setAttribute("errEditEmail", "Please enter a valid email address");
                model.addAttribute("npCandidateReferenceForm", new NpCandidateReferenceForm());
                return "components/npCandidateSponsorFormComponent.jsp";
            }// if empty email or invalid email format is given while editing sponsor detail

            npCandidateReferenceForm.setId(Integer.parseInt(request.getParameter("spid").toString()));
            npCandidateReferenceForm.setFirstName(request.getParameter("_firstName"));
            npCandidateReferenceForm.setSurName(request.getParameter("_surName"));
            npCandidateReferenceForm.setEmail(request.getParameter("_email"));
            npCandidateReferenceForm.setCoveringNote(request.getParameter("_coveringNote"));
            npCandidateReferenceForm.setRelationship(request.getParameter("_relationship"));
            npCandidateReferenceForm.setDateFrom(request.getParameter("_dateFrom"));
            npCandidateReferenceForm.setDateTo(request.getParameter("_dateTo"));

            if (notValidDate(npCandidateReferenceForm.getDateFrom(), npCandidateReferenceForm.getDateTo()))
            {

                if (StringUtils.isEmpty(npCandidateReferenceForm.getDateTo()))
                {
                    npCandidateReferenceForm.setDateTo("Current");
                }

                request.setAttribute("spid", request.getParameter("spid").toString());
                request.setAttribute("errDateEdit", "Please enter a valid from/to date which is not today or future date and from should before To date");
                model.addAttribute("npCandidateReferenceForm", new NpCandidateReferenceForm());
                return "components/npCandidateSponsorFormComponent.jsp";
            }
        }
        npCandidateReferenceForm.setNpCandidate(npCandidate);

        if (npCandidateReferenceForm.getEmail().equalsIgnoreCase(npUser.getPrimaryEmail()))
        {
            if (StringUtils.isEmpty(request.getParameter("edit")))
            {
                request.setAttribute("errEmail", "You cannot add yourself as a sponsor");
                return "components/npCandidateSponsorFormComponent.jsp";
            }
            request.setAttribute("spid", request.getParameter("spid").toString());
            request.setAttribute("errEditEmail", "You cannot add yourself as a sponsor");
            model.put("npCandidateReferenceModel", npCandidateReferenceForm);
            return "components/npCandidateSponsorFormComponent.jsp";
        } // if candidate adding his own email address as sponsor email

        else if (npCandidateSponsorService.isSponsorEmailExist(npCandidateReferenceForm.getEmail(), npCandidate))
        {
            if (StringUtils.isEmpty(request.getParameter("edit")))
            {
                request.setAttribute("errEmail", "You have already added this sponsor. Please enter valid sponsor email");
                return "components/npCandidateSponsorFormComponent.jsp";
            }
        }  // if candidate adding adding duplicate email address as sponsor email

        if (StringUtils.isNotEmpty(request.getParameter("edit")) && npCandidateSponsorService.isEditingDuplicateSponsor(npCandidate, request.getParameter("spid").toString(), request.getParameter("_email")))
        {

            request.setAttribute("spid", request.getParameter("spid").toString());
            request.setAttribute("errEditEmail", "A sponsor having same email address exists in your list");
            model.addAttribute("npCandidateReferenceForm", new NpCandidateReferenceForm());
            return "components/npCandidateSponsorFormComponent.jsp";
        } // if existing sponsor email is given while editing another sponsor.

        if (npCandidateReferenceForm != null)
        {

            npCandidateSponsorService.saveMySponsorDetail(npCandidateReferenceForm);
        }

        model.addAttribute("npCandidateReferenceForm", new NpCandidateReferenceForm());
        return "redirect:mySponsor";
    }

    public NpCandidateReferenceFormValidator getNpCandidateReferenceFormValidator()
    {
        return npCandidateReferenceFormValidator;
    }

    public void setNpCandidateReferenceFormValidator(
            NpCandidateReferenceFormValidator npCandidateReferenceFormValidator)
    {
        this.npCandidateReferenceFormValidator = npCandidateReferenceFormValidator;
    }

    public boolean notValidDate(String fromDate, String toDate)
    {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date from = null, to = null;
        try
        {
            from = formater.parse(fromDate);

            if (from.after(new Date()))
            {

                return true;
            }
            if (StringUtils.isNotEmpty(toDate) && !"Current".equalsIgnoreCase(toDate))
            {

                to = formater.parse(toDate);

                if (to.before(from))
                {

                    return true;
                }
            }
            LOG.info("from " + from + " to - " + to);
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            LOG.error("Problem in Parsing From/To date:  " + e.getStackTrace() + toDate);

            return true;
        }

        return false;
    }
}
