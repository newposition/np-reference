package com.newposition.candidate.templates.components;

import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateFutureEmployer;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.forms.NpCandidateFutureForm;
import com.newposition.candidate.service.NpCandidateFutureService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "myfuturecomponent", id = "newPositionModule:components/myFuturePageComponent")
public class NpCandidateFutureComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateFutureComponent.class);

    @Autowired
    private NpCandidateFutureService npCandidateFutureService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @RequestMapping(value = "/myFuture", method = RequestMethod.GET)
    public String rendered(@ModelAttribute(value = "myFutureDetails") NpCandidateFutureForm form, HttpServletRequest request, Model model, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate != null)
        {
        /*
         * getting future preferred skills
         */
            List<NpCandidateSkills> futureSkills = null;
            futureSkills = npCandidateFutureService.getPreferedSkills(npCandidate);
            model.addAttribute("futureSkills", futureSkills);

        /*
         * getting future preferred tools
         */
            List<NpCandidateTools> futureTools = null;
            futureTools = npCandidateFutureService.getPreferedTools(npCandidate);
            model.addAttribute("futureTools", futureTools);
        /*
         * 
         * getting future preferred roles
         */
            List<NpCandidateEmpRoles> futureRoles = null;
            futureRoles = npCandidateFutureService.getPreferedRoles(npCandidate);
            model.addAttribute("futureRoles", futureRoles);

            List<String> preferCountries = null;
            preferCountries = npCandidateFutureService.getPreferCountries(npCandidate);
            model.addAttribute("preferCountries", preferCountries);

            Map<String, List<NpCandidateFutureEmployer>> npCandidateFutureEmployerEntry = null;
            npCandidateFutureEmployerEntry = npCandidateFutureService.getFutureEmployer(npCandidate);
            model.addAttribute("futurePreferCompany", npCandidateFutureEmployerEntry.get("futurePreferCompany"));
            model.addAttribute("futureExcludeCompany", npCandidateFutureEmployerEntry.get("futureExcludeCompany"));
        }

        LOG.info("getting MyFuturePage");
        return "components/npCandidateFuturePageComponent.jsp";
    }

    @RequestMapping(value = "/myFuture", method = RequestMethod.POST)
    public String render(@ModelAttribute(value = "myFutureDetails") NpCandidateFutureForm myFutureDetails, BindingResult result,
                         HttpServletRequest request, Authentication authentication)
    {

        if (result.hasErrors())
        {

            return "components/npCandidateFuturePageComponent.jsp";
        }

        NpUser npUser = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        LOG.info("POST method is Started ");
        // saving carrier preference data
        Map<String, Document> listOfFile = MgnlContext.getPostedForm().getDocuments();
        LOG.info("strings : " + listOfFile.keySet());

        String extension = null;
        File photo = null, cv = null;
        if (listOfFile.get("photo") != null)
        {
            photo = listOfFile.get("photo").getFile();
        }
        else
        {
            LOG.error("CV is not Entered");
        }
        if (listOfFile.get("cvFile") != null)
        {
            cv = listOfFile.get("cvFile").getFile();
        }
        else
        {
            LOG.error("CV is not Entered");
        }
        String rootPath = request.getContextPath();
//        String fileSaveAs =  serverFile+"\\"+npCandidate.getId()+"."+listOfFile.get("photo").getExtension();
        String fileNameWithExtension = null;
        String fileSavedPath = null;
        if (photo != null)
        {
            fileNameWithExtension = npCandidate.getFirstName() + npCandidate.getId() + "_photo." + listOfFile.get("photo").getExtension();
            fileSavedPath = saveFile(rootPath, photo, fileNameWithExtension);
            if (!StringUtils.isEmpty(fileSavedPath))
            {
                LOG.info("photo- " + fileSavedPath);
                try
                {
                    File file = photo;
                    //byte[] bFile = new byte[(int) file.length()];
                    FileInputStream input = new FileInputStream(file);
                    MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
                    myFutureDetails.setPhoto(multipartFile.getBytes());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (cv != null)
        {
            fileNameWithExtension = npCandidate.getFirstName() + npCandidate.getId() + "_cv." + listOfFile.get("cvFile").getExtension();
            fileSavedPath = saveFile(rootPath, cv, fileNameWithExtension);
            if (!StringUtils.isEmpty(fileSavedPath))
            {
                LOG.info("cv- " + fileSavedPath);
                myFutureDetails.setCvFile(fileSavedPath);
            }
        }

        myFutureDetails.setNpCandidate(npCandidate);
        npCandidateFutureService.saveFutureDetails(myFutureDetails);
        LOG.info("POST method is executed ");

        request.getSession().setAttribute("headerText", "Congratulation!!!!!!!!!!!");
        request.getSession().setAttribute("bodyText", "You have finished the Complete Registration process");
        return "redirect:sucessPage";
    }

    /*
     * for file upload in a folder
     * */
    @SuppressWarnings("resource")
    public String saveFile(String rootPath, File file, String fileNameWithExtension)
    {

        // Creating the directory to store file

        File dir = new File(rootPath + File.separator + "DocFiles");
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        // Create the file on server

        File serverFile = new File(dir.getAbsolutePath() + File.separator);
        LOG.info("Server File Location=" + serverFile.getAbsolutePath());

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try
        {
            inputStream = new FileInputStream(file.getAbsolutePath());
            outputStream = new FileOutputStream(serverFile + "\\" + fileNameWithExtension);

            Integer c;
            //continue reading till the end of the file
            while ((c = inputStream.read()) != -1)
            {
                //writes to the output Stream
                outputStream.write(c);
            }
            LOG.info("Photo is saved in : " + serverFile);
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            LOG.error("File Not Exist " + e);
        }
        catch (IOException e)
        {
            LOG.error("Error Occur while saving the file" + e);
        }
        LOG.info("path returned: " + serverFile.getAbsolutePath() + "\\" + fileNameWithExtension);
        return serverFile.getAbsolutePath() + "\\" + fileNameWithExtension;
    }
}

