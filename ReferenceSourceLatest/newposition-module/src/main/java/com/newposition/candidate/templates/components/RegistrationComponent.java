/**
 *
 */
package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.candidate.forms.validator.QuickRegistrationFormValidator;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpResetImpl;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;
import com.newposition.util.DataSecurity;

/**
 * @author Yamuna Vemula
 */

@Controller
@Template(title = "Registration Form", id = "newPositionModule:components/registration")
public class RegistrationComponent
{
    protected static final Logger LOG = Logger.getLogger(RegistrationComponent.class);

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpMailService npMailService;

    @Value("${loginPageLink}")
    private String loginPageLink;

    @Value("${email.images.context}")
    private String imageContext;

    @Value("${captcha.message}")
    private String captchamessage;

    @Value("${activationPageLink}")
    private String activationPageLink;

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.setValidator(new QuickRegistrationFormValidator());
    }

    @RequestMapping(value = "/candidateRegister", method = RequestMethod.GET)
    public String getRegisterPage(@ModelAttribute("quickRegistrationForm") QuickRegistrationForm quickRegistrationForm, HttpServletRequest request)
    {

        return "components/registration.jsp";
    }

    @RequestMapping(value = "/candidateRegister", method = RequestMethod.POST)
    public String candidateRegister(@ModelAttribute("quickRegistrationForm") @Valid QuickRegistrationForm quickRegistrationForm,
                                    BindingResult result, HttpServletRequest request)
    {

        // Validation errors
        if (result.hasErrors())
        {
            request.setAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/registration.jsp";
        }
        else if (!rpHash(request.getParameter("defaultReal")).equals(request.getParameter("defaultRealHash")))
        {
            request.setAttribute("cpm", captchamessage);
            return "components/registration.jsp";
        }
        UserDetails users = npUserService.loadUserByUsername(quickRegistrationForm.getCandidate().getPrimaryEmail());
        if (users == null)
        {

            NpCandidate npCandidate = npCandidateService.candidateRegister(quickRegistrationForm.getCandidate(), quickRegistrationForm.getPassword());

            // request.getSession().setAttribute("npCandidate", npCandidate);

            String encryptId = null;
            encryptId = new String(DatatypeConverter.printBase64Binary(npCandidate.getId().toString().getBytes()));

            // generate activation code and send it to the user email

            Random rand = new Random();
            Integer activationCode = rand.nextInt(1000000);

            NpReset resetCode = new NpResetImpl();
            resetCode.setEmail(quickRegistrationForm.getCandidate().getPrimaryEmail());
            resetCode.setActivation(activationCode.toString());

            npUserService.setActivationCode(resetCode);

            Long currentTime = System.currentTimeMillis();
            String currentTimeString = "";
            currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());
            String activationPageParams = activationPageLink + "?id=" + encryptId + "&page=regactivate";
            String directActivationLink = loginPageLink + "?id=" + encryptId + "&dur=" + currentTimeString;

            String[] activeLinkAndCode = {directActivationLink, activationCode.toString(), activationPageParams};

            LOG.info(directActivationLink + "  activationcode - " + activationCode.toString() + "  activation page link " + activationPageParams);

            String subject = "NewPosition Account activation";

            npCandidateService.sendRegistrationSucessMail(quickRegistrationForm, activeLinkAndCode, subject);

            request.getSession().setAttribute("bodyText",
                    "An Email link has sent to your email please click to activate account and continue registration");

            return "redirect:sucessPage";
        }

        else
        {
            Format format = new SimpleDateFormat("MMM dd, yyyy");
            String toDay = format.format(new Date());
            File headerPhoto = new File(request.getContextPath() + "./resources/templating-kit/img/sky.jpg");
            LOG.info("file path of sky: " + headerPhoto.getAbsolutePath());
            String candidateName = quickRegistrationForm.getCandidate().getFirstName() + " " + quickRegistrationForm.getCandidate().getLastName();
            String information = "NewPosition Account activation";
            Context context = new Context();
            context.setVariable("candidateName", candidateName);
            context.setVariable("loginPageLink", loginPageLink);
            context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
            context.setVariable("template", "exist-user-email.html");
            context.setVariable("date", toDay);

            npMailService.sendMail(information, quickRegistrationForm.getCandidate().getPrimaryEmail(), context);

            request.getSession().setAttribute("bodyText",
                    "An Email link has sent to your email please click to activate account and continue registration");

            return "redirect:sucessPage";
        }
    }

    private String rpHash(String value)
    {
        int hash = 5381;
        value = value.toUpperCase();
        for (int i = 0; i < value.length(); i++)
        {
            hash = ((hash << 5) + hash) + value.charAt(i);
        }
        return String.valueOf(hash);
    }
}
