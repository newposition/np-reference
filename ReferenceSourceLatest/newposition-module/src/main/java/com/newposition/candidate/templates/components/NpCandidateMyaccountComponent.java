package com.newposition.candidate.templates.components;

import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidateCompensationForm;
import com.newposition.candidate.forms.NpCandidatePreferenceForm;
import com.newposition.candidate.forms.validator.NpCandidateMyaccountPreferenceFormValidator;
import com.newposition.candidate.service.NpCandidateMyaccountService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpResetImpl;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;
import com.newposition.util.DataSecurity;

@Controller
@Template(title = "Account Profile", id = "newPositionModule:components/myaccount")
public class NpCandidateMyaccountComponent
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateMyaccountComponent.class);

    @Value("${contactDetails.Percentage}")
    private String contactDetailsPercentage;

    @Value("${personalStatement.Percentage}")
    private String personalStatementPercentage;

    @Value("${employmentHistory.Percentage}")
    private String employmentHistoryPercentage;

    @Value("${educationHistory.Percentage}")
    private String educationHistoryPercentage;

    @Value("${futureDetails.Percentage}")
    private String futureDetailsPercentage;

    @Value("${references.Percentage}")
    private String referencesPercentage;

    @Value("${cvAndPhoto.Percentage}")
    private String cvAndPhotoPercentage;

    @Value("${availabilityStatus.Percentage}")
    private String availabilityStatusPercentage;

    @Value("${preferences.Percentage}")
    private String preferencesPercentage;

    @Value("${loginPageLink}")
    private String loginPageLink;

    @Value("${activationPageLink}")
    private String activationPageLink;

    @Value("${email.images.context}")
    private String imageContext;

    @Autowired
    private NpCandidateMyaccountService npCandidateMyaccountService;

//    @Autowired
//    private NpUserDao npUserDao;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpCandidateMyaccountPreferenceFormValidator npCandidateMyaccountPreferenceFormValidator;

    public NpCandidateMyaccountPreferenceFormValidator getNpCandidateMyaccountPreferenceFormValidator()
    {
        return npCandidateMyaccountPreferenceFormValidator;
    }

    public void setNpCandidateMyaccountPreferenceFormValidator(
            NpCandidateMyaccountPreferenceFormValidator npCandidateMyaccountPreferenceFormValidator)
    {
        this.npCandidateMyaccountPreferenceFormValidator = npCandidateMyaccountPreferenceFormValidator;
    }

    @RequestMapping(value = "/myaccount", method = RequestMethod.GET)
    public String render(HttpServletRequest request, Model model, Authentication authentication)
    {

        addUserDetailsToModel(model, request, null, null, authentication);
        model.addAttribute("isMyAccount", true);
        return "components/npCandidateMyaccount.jsp";
    }

    @RequestMapping(value = "/myaccount", method = RequestMethod.POST)
    public String register(@ModelAttribute("npCandidatePreferenceForm") @Valid NpCandidatePreferenceForm npCandidatePreferenceForm, BindingResult result, HttpServletRequest request, Model model, Authentication authentication)
    {

        getNpCandidateMyaccountPreferenceFormValidator().validate(npCandidatePreferenceForm, result);

        if (result.hasErrors())
        {
            addUserDetailsToModel(model, request, npCandidatePreferenceForm, null, authentication);
            model.addAttribute("isPreference", true);
            return "components/npCandidateMyaccount.jsp";
        }

        LOG.info("*********************job" + npCandidatePreferenceForm.isJobAppearance());
        LOG.info("*********************permission" + npCandidatePreferenceForm.isPermissionShortlist());
        LOG.info("*********************summary" + npCandidatePreferenceForm.isSummaryReports());
        LOG.info("*********************reports" + npCandidatePreferenceForm.isSendReports());
        LOG.info("*********************opportunities" + npCandidatePreferenceForm.isSendOpportunities());
        LOG.info("*********************1Email" + npCandidatePreferenceForm.getPrimaryEmail());
        LOG.info("*********************2EMail" + npCandidatePreferenceForm.getSecondaryEmail());
        LOG.info("*********************3Email" + npCandidatePreferenceForm.getTertiaryEmail());
        LOG.info("*********************3Email" + npCandidatePreferenceForm.isAcceptPhoto());

        NpUser npUser = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        Document listOfFile = MgnlContext.getPostedForm().getDocument("photo");
        File photo = null;
        if (listOfFile != null)
        {
            photo = listOfFile.getFile();
            uploadPhoto(npCandidate, photo, request);
        }
        else
        {
            LOG.error("photo is not Entered");
        }

        if (!npCandidate.getPrimaryEmail().equalsIgnoreCase(npCandidatePreferenceForm.getPrimaryEmail()))
        {
            if (!npCandidateMyaccountService.checkEmailExists(npCandidatePreferenceForm.getPrimaryEmail()))
            {
                result.rejectValue("primaryEmail", "candidate.myaccount.primary.email.validation.exists");
            }
            if (result.hasErrors())
            {
                addUserDetailsToModel(model, request, npCandidatePreferenceForm, null, authentication);
                model.addAttribute("isPreference", true);
                return "components/npCandidateMyaccount.jsp";
            }
            npCandidateMyaccountService.updateCandidatePreferences(npCandidatePreferenceForm, npCandidate);
            // generate activation code and send it to the user email

            String encryptId = null;
            try
            {
                encryptId = DatatypeConverter.printBase64Binary(npCandidate.getId().toString().getBytes());
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                LOG.error(e.getStackTrace());
            }
            Random rand = new Random();
            Integer activationCode = rand.nextInt(1000000);

            NpReset resetCode = new NpResetImpl();
            resetCode.setEmail(npCandidatePreferenceForm.getPrimaryEmail());
            resetCode.setActivation(activationCode.toString());

            npUserService.setActivationCode(resetCode);

            Long currentTime = System.currentTimeMillis();
            String currentTimeString = "";
            currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());

            npCandidate.setPrimaryEmail(npCandidatePreferenceForm.getPrimaryEmail());
            String activationPageParams = activationPageLink + "?id=" + encryptId;
            String directActivationLink = loginPageLink + "?id=" + encryptId + "&page=regactivate" + "&dur=" + currentTimeString;

            String[] activeLinkAndCode = {directActivationLink, activationCode.toString(), activationPageParams};

            LOG.info(directActivationLink + "  activationcode - " + activationCode.toString() + "  activation page link " + activationPageParams);

            String subject = "NewPosition Account activation";
            sendMail(npCandidate, activeLinkAndCode, subject);

            request.getSession().setAttribute("bodyText", "You have changed Primary Email Address.An Email link has sent to your new email please click to activate account.");

            return "redirect:sucessPage";
        }

//        updateCandidateInSession(npUser.getId(),request);//TODO need to remove candidate and user objects from session
        else
        {
            npCandidateMyaccountService.updateCandidatePreferences(npCandidatePreferenceForm, npCandidate);
            return "redirect:/myaccount";
        }
    }

    private NpCandidatePreferenceForm createCandidatePreferenceForm(NpUser npUser, NpCandidatePreference npCandidatePreference)
    {

        NpCandidatePreferenceForm npCandidatePreferenceForm = new NpCandidatePreferenceForm();
        npCandidatePreferenceForm.setPrimaryEmail(npUser.getPrimaryEmail());
        npCandidatePreferenceForm.setSecondaryEmail(npUser.getSecondaryEmail());
        npCandidatePreferenceForm.setTertiaryEmail(npUser.getThirdEmail());

        if (npCandidatePreference != null)
        {

            npCandidatePreferenceForm.setJobAppearance(npCandidatePreference.isJobAppearance());
            npCandidatePreferenceForm.setPermissionShortlist(npCandidatePreference.isPermissionShortlist());
            npCandidatePreferenceForm.setSendOpportunities(npCandidatePreference.isSendOpportunities());
            npCandidatePreferenceForm.setSendReports(npCandidatePreference.isSendReports());
            npCandidatePreferenceForm.setSummaryReports(npCandidatePreference.isSummaryReports());
            npCandidatePreferenceForm.setAcceptPhoto(npCandidatePreference.isAcceptPhoto());
        }
        else
        {

            npCandidatePreferenceForm.setJobAppearance(false);
            npCandidatePreferenceForm.setPermissionShortlist(false);
            npCandidatePreferenceForm.setSendOpportunities(false);
            npCandidatePreferenceForm.setSendReports(false);
            npCandidatePreferenceForm.setSummaryReports(false);
            npCandidatePreferenceForm.setAcceptPhoto(false);
        }

        return npCandidatePreferenceForm;
    }

    private void addUserDetailsToModel(Model model, HttpServletRequest request, NpCandidatePreferenceForm npCandidatePreferenceForm, NpCandidateCompensationForm npCandidateCompensationForm, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
//    updateCandidateInSession(npUser.getId(),request);
        if (npCandidate != null)
        {
            if (npCandidate.getNpCandidateDocument() != null)
            {
                try
                {
                    model.addAttribute("profileImage", new String(Base64.encodeBase64(npCandidate.getNpCandidateDocument().getPhoto())));
                }
                catch (NullPointerException npe)
                {
                    LOG.error("Candidate Document is Null");
                }

                model.addAttribute("npCandidate", npCandidate);
            }
        }
        int candidateProfilePercentage = calculatePercentageOfCandidateProfile(npCandidate);
        model.addAttribute("candidateProfilePercentage", candidateProfilePercentage);
        NpCandidatePreference npCandidatePreference = npCandidateMyaccountService.getCandidatePreferences(npCandidate);
        model.addAttribute("npUser", npUser);
        model.addAttribute("npCompensation", createCandidateCompensationForm(npCandidate, npCandidatePreference));

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateMyaccountService.getNpCandidateEmployer(npCandidate.getId());

        if (npCandidateEmployers != null)
        {
            model.addAttribute("npCandidateEmployers", npCandidateEmployers);
        }

        if (npCandidatePreferenceForm == null)
        {
            model.addAttribute("npCandidatePreferenceForm", createCandidatePreferenceForm(npUser, npCandidatePreference));
        }
        else
        {
            model.addAttribute("npCandidatePreferenceForm", npCandidatePreferenceForm);
        }
    }

    //private void updateCandidateInSession(Integer userId , HttpServletRequest request)
//{
//    NpCandidate npCandidate=npUserDao.findByUserId(userId);
//    request.getSession().setAttribute("npCandidate", npCandidate);
//}
    private NpCandidateCompensationForm createCandidateCompensationForm(NpCandidate npCandidate, NpCandidatePreference npCandidatePreference)
    {

        NpCandidateCompensationForm npCandidateCompensationForm = new NpCandidateCompensationForm();
        if (npCandidatePreference != null)
        {
            if (npCandidatePreference.getType() != null)
            {
                npCandidateCompensationForm.setCompensationType(npCandidatePreference.getType());
            }
            if (npCandidatePreference.getCompensation() != null)
            {
                npCandidateCompensationForm.setSalary(npCandidatePreference.getCompensation());
            }

            if (npCandidatePreference.getDayRate() != null)
            {
                npCandidateCompensationForm.setDayrate(npCandidatePreference.getDayRate());
            }
            if (npCandidatePreference.getBonus() != null)
            {
                npCandidateCompensationForm.setBonus(npCandidatePreference.getBonus());
            }
        }
        return npCandidateCompensationForm;
    }

    private void uploadPhoto(NpCandidate npCandidate, File photo, HttpServletRequest request)
    {
        Map<String, Document> listOfFile = MgnlContext.getPostedForm().getDocuments();
        String rootPath = request.getContextPath();
        String fileNameWithExtension = null;
        String fileSavedPath = null;
        if (photo != null)
        {
    /*fileNameWithExtension = npCandidate.getFirstName()+npCandidate.getId()+"_photo_"+
							 listOfFile.get("photo").getFileNameWithExtension();
    fileSavedPath = saveFile (rootPath, photo, fileNameWithExtension);
    if (!StringUtils.isEmpty(fileSavedPath)){
    	 LOG.info("photo- "+fileSavedPath);
    	 NpCandidateDocument candidateDocument=npCandidate.getNpCandidateDocument();
    	 if(candidateDocument == null)
    	 {
    		 candidateDocument=new NpCandidateDocument();
    		 candidateDocument.setNpCandidate((NpCandidateImpl) npCandidate);
    	 }
    	 fileSavedPath= fileSavedPath.replace("\\", "/");
    	 LOG.info("file saved path:"+fileSavedPath);*/
            try
            {
                File file = photo;
                //byte[] bFile = new byte[(int) file.length()];
                FileInputStream input = new FileInputStream(file);
                MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));
                NpCandidateDocument candidateDocument = npCandidate.getNpCandidateDocument();
                candidateDocument.setPhoto(multipartFile.getBytes());
                npCandidateMyaccountService.updateCandidateDocument(candidateDocument);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("resource")
    public String saveFile(String rootPath, File file, String fileNameWithExtension)
    {

        // Creating the directory to store file

        File dir = new File(rootPath + File.separator + "DocFiles");
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        // Create the file on server

        File serverFile = new File(dir.getAbsolutePath() + File.separator);
        LOG.info("Server File Location=" + serverFile.getAbsolutePath());
        InputStream inputStream = null;
        OutputStream outputStream = null;

        try
        {
            inputStream = new FileInputStream(file.getAbsolutePath());
            outputStream = new FileOutputStream(serverFile + "\\" + fileNameWithExtension);

            Integer c;
            //continue reading till the end of the file
            while ((c = inputStream.read()) != -1)
            {
                //writes to the output Stream
                outputStream.write(c);
            }
            LOG.info("Photo is saved in : " + serverFile);
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            LOG.error("File Not Exist " + e);
        }
        catch (IOException e)
        {
            LOG.error("Error Occur while saving the file" + e);
        }
        LOG.info("path returned: " + serverFile.getPath() + "\\" + fileNameWithExtension);
        return serverFile.getPath() + "\\" + fileNameWithExtension;
    }

    public int calculatePercentageOfCandidateProfile(NpCandidate npCandidate)
    {
        int percentage = 0;

        LOG.info("contactDetailsPercentage :" + contactDetailsPercentage);
        percentage = percentage + Integer.parseInt(contactDetailsPercentage);

        if (!CollectionUtils.isEmpty(npCandidate.getNpCandidateEducations()) &&
                !CollectionUtils.isEmpty(npCandidate.getNpCandidateCertificates()))
        {
            percentage = percentage + Integer.parseInt(educationHistoryPercentage);
        }
        if (npCandidate.getNpCandidateDocument() != null)
        {
            percentage = percentage + Integer.parseInt(cvAndPhotoPercentage);
        }
        if (!CollectionUtils.isEmpty(npCandidate.getNpCandidateReferences()))
        {
            percentage = percentage + Integer.parseInt(referencesPercentage);
        }
        if (!CollectionUtils.isEmpty(npCandidate.getNpCandidateEmployers()))
        {
            percentage = percentage + Integer.parseInt(employmentHistoryPercentage);
        }
        if (npCandidate.getPersonalStatement() != null)
        {
            percentage = percentage + Integer.parseInt(personalStatementPercentage);
        }
        if (npCandidate.getAvailableStatus() != null)
        {
            percentage = percentage + Integer.parseInt(availabilityStatusPercentage);
        }
        if (!CollectionUtils.isEmpty(npCandidate.getNpCandidateSkills()) &&
                !CollectionUtils.isEmpty(npCandidate.getNpCandidateEmpRoles()) &&
                !CollectionUtils.isEmpty(npCandidate.getNpCandidateTools()))
        {
            percentage = percentage + Integer.parseInt(futureDetailsPercentage);
        }
        if (npCandidate.getNpCandidatePreference() != null)
        {
            percentage = percentage + Integer.parseInt(preferencesPercentage);
        }
        LOG.info("Candidate Profile Percentage :" + percentage);
        return percentage;
    }

    private void sendMail(NpCandidate npCandidate, String[] codes, String subject)
    {

        String candidateName = npCandidate.getFirstName() + " " + npCandidate.getLastName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", candidateName);
        context.setVariable("loginPageLink", codes[0]);
        context.setVariable("activationPageLink", codes[2]);
        context.setVariable("activationCode", codes[1]);
        context.setVariable("template", "registration-email.html");
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);

        npMailService.sendMail(information, npCandidate.getPrimaryEmail(), context);
    }
}
