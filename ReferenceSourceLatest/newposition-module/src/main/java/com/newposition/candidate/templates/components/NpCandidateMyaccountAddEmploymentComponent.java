package com.newposition.candidate.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.service.NpCandidateEmploymentService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "Add Myaccount Employment Component", id = "newPositionModule:components/AddEmployment")
public class NpCandidateMyaccountAddEmploymentComponent
{

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpUserService npUserService;

    @RequestMapping(value = "/addMyaccountEmployment", method = RequestMethod.GET)
    public String getEmploymentPage(HttpServletRequest request, Model model, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate != null)
        {

            request.setAttribute("npCandidate", npCandidate);
        }
        return "components/addMyaccountEmployment.jsp";
    }
}
