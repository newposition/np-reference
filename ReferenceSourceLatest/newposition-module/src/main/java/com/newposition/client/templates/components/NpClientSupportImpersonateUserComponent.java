/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.service.NpClientSupportImpersonateUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.forms.ImpersonateForm;
import com.newposition.common.service.NpUsersReasonService;

/**
 * @author Sachi N
 */

@Controller
@Template(title = "ClientSupport Impersonate User", id = "newPositionModule:components/clientSupportImpersonateUserComponent")
public class NpClientSupportImpersonateUserComponent
{
    protected static final Logger LOG = Logger
            .getLogger(NpClientSupportImpersonateUserComponent.class);

    @Autowired
    private NpClientSupportImpersonateUserService clientSupportImpersonateUserService;

    @Autowired
    private NpUsersReasonService npUsersReasonService;

    @Value("${pagination.numberOfUserReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/clientSupportImpersonateUser", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        List listOfPaginatedData = null;
        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String filterBy = request.getParameter("filterBy");
        String sortBy = request.getParameter("sortBy");

        LOG.info("Sort By" + sortBy);
        listOfPaginatedData = clientSupportImpersonateUserService
                .getSortedNpUsers(sortBy, pageNumber, filterBy);

        Integer npUsersSize = (int) listOfPaginatedData.get(1);

        double noOfPages = (double) npUsersSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("currentPageNumber", pageNumber);
        model.addAttribute("npUsersList", listOfPaginatedData.get(0));
        model.addAttribute("noOfNpUsers", npUsersSize);
        model.addAttribute("filterBy", filterBy);
        model.addAttribute("npUserNames", listOfPaginatedData.get(2));

        model.addAttribute("npUserTotalPages", (int) noOfPages);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("impersonateForm", new ImpersonateForm());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForUser());

        return "components/clientSupportImpersonateUserComponent.jsp";
    }

    @RequestMapping(value = "/clientSupportImpersonateUser", method = RequestMethod.POST)
    public String register(@ModelAttribute("impersonateForm") ImpersonateForm impersonateForm,
                           HttpServletRequest request, Model model)
    {

        LOG.info(impersonateForm.getUserId());
        LOG.info(impersonateForm.getReasonCode());
        LOG.info(impersonateForm.getReasonLines());
//		LOG.info(impersonateForm.getUserRole());
        NpUser npUser = clientSupportImpersonateUserService.getUserById(Integer.parseInt(impersonateForm.getUserId()));
        LOG.info(npUser);
        Set<NpUserRole> npUserRoleses = npUser.getNpUserRole();
        Iterator<NpUserRole> userRoleIterator = npUserRoleses.iterator();
        String userRoleName = "";
        for (; userRoleIterator.hasNext(); )
        {
            userRoleName = userRoleIterator.next().getNpRole().getRoleName();
            LOG.info(userRoleName);
        }

        boolean isClientAdminGroup = false;
        /*if ("Role_Client_Liason".equalsIgnoreCase(userRoleName)) {
			LOG.info("Role_Client_Liason");
			userRoleName="Role_Client_Liason";
		}*/
		/*else if ("ROLE_NP_USERSUPPORT".equalsIgnoreCase(userRoleName)) {
			
		}
		else if ("ROLE_SKILL ANALYST".equalsIgnoreCase(userRoleName)) {
			
		}*/
        if ("Role_Client_Admin".equalsIgnoreCase(userRoleName))
        {
            isClientAdminGroup = true;
        }
        if ("Role_Client_Administrator".equalsIgnoreCase(userRoleName))
        {
            LOG.info("Role_Client_Administrator");
//			request.getSession().setAttribute("roleName",userRoleName);
            isClientAdminGroup = true;
        }
        else if ("Role_Client_Interviewer".equalsIgnoreCase(userRoleName))
        {
            LOG.info("Role_Client_Interviewer");
//			request.getSession().setAttribute("roleName",userRoleName);
            isClientAdminGroup = true;
        }
        else if ("Role_Client_Manager".equalsIgnoreCase(userRoleName))
        {
            LOG.info("Role_Client_Manager");
//			request.getSession().setAttribute("roleName",userRoleName);
            isClientAdminGroup = true;
        }
        else if ("Role_Client_Support".equalsIgnoreCase(userRoleName))
        {
            LOG.info("Role_Client_Support");
//			request.getSession().setAttribute("roleName",userRoleName);
            isClientAdminGroup = true;
        }
        else if ("Role_Client_Employee".equalsIgnoreCase(userRoleName))
        {
            LOG.info("Role_Client_Employee");
//			request.getSession().setAttribute("roleName",userRoleName);
            isClientAdminGroup = true;
        }
        if (isClientAdminGroup)
        {
            request.getSession().setAttribute("clientName", npUser.getFirstName() + " " + npUser.getLastName());
            request.getSession().setAttribute("company", npUser.getCompany());
            if (!"Role_Client_Admin".equalsIgnoreCase(userRoleName))
            {
                request.getSession().setAttribute("roleName", userRoleName);
            }
            return "redirect:/clientDashboard";
        }
        request.getSession().setAttribute("_roleName", userRoleName);
        return "redirect:/adminClientLiason";
    }
	/*
	 * private void sendMail(RegistrationForm registrationForm, String link,
	 * String subject) {
	 * 
	 * String candidateName = registrationForm.getFirstName() + " " +
	 * registrationForm.getLastName(); String information = subject; Context
	 * context = new Context(); context.setVariable("candidateName",
	 * candidateName); context.setVariable("loginPageLink", link);
	 * context.setVariable("activationPageLink",
	 * "http://52.74.221.110:8080/newposition/activationPage");
	 * context.setVariable("template", "registration-email.html");
	 * 
	 * npMailService.sendMail(information,
	 * registrationForm.getPrimaryEmailAddress(), context); }
	 */
}
