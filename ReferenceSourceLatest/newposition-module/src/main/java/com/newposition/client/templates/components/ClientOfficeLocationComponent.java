package com.newposition.client.templates.components;

import java.util.List;

import javax.servlet.http.HttpSession;

import info.magnolia.module.blossom.annotation.Template;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.client.service.NpClientOfficeLocationService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;
import com.newposition.common.domain.NpUser;

@Controller
@Template(title = "Client Office Location Component", id = "newPositionModule:components/clientOfficeLocationComponent")
public class ClientOfficeLocationComponent
{

    @Autowired
    NpClientService npClientService;

    @Autowired
    NpClientOfficeLocationService npClientOfficeLocationService;

    @RequestMapping(value = "/clientOfficeLocationComponent", method = RequestMethod.GET)
    public String displayProjects(Model model, HttpSession session)
    {
        List<String> countryNames = npClientOfficeLocationService.getAllCountries();
        model.addAttribute("cities", countryNames);
        model.addAttribute("message", getMessage((String) session.getAttribute("cmsg")));
        session.setAttribute("cmsg", null);
        return "components/clientOfficeLocationComponent.jsp";
    }

    private String getMessage(String msg)
    {
        if (StringUtils.isNotEmpty(msg))
        {
            if (StringUtils.equals(msg, "1"))
            {
                return "OfficeLocation Added Successfully";
            }
            else if (StringUtils.equals(msg, "2"))
            {
                return "OfficeLocation Edited Successfully";
            }
        }

        return null;
    }

    @RequestMapping(value = "/clientOfficeLocationComponent", method = RequestMethod.POST)
    public String saveNewProjects(HttpSession session, NpClientOfficeLocation npClientOfficeLocation, CoLocations coLocations, CoCities coCities, CoCounty coCounty, CoLocationState coLocationState, CoZip coZip, CoCountry coCountry)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        npClientOfficeLocation.setNpClient(npClient);
        if (npClientOfficeLocation.getClientOfficeLocID() != null)
        {
            session.setAttribute("cmsg", "2");
        }
        else
        {
            session.setAttribute("cmsg", "1");
        }
        npClientOfficeLocationService.saveNpClientOfficeLocation(npClientOfficeLocation, coLocations, coCities, coCounty, coLocationState, coZip, coCountry);
        return "redirect:clientOfficeLocation";
    }
}
