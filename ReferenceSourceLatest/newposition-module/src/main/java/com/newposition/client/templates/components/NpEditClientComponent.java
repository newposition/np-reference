/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.service.NpClientService;

@Controller
@Template(title = "Edit Client Component", id = "newPositionModule:components/editClient")
public class NpEditClientComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpEditClientComponent.class);

    @Autowired
    private NpClientService clientService;

    @RequestMapping(value = "/edit-client", method = RequestMethod.GET)
    public String getAddClientLiasonPage(AddClientForm form, HttpServletRequest request, Model model, String clientId)
    {
        NpClient client;

        if (!StringUtils.isEmpty(clientId))
        {
            client = clientService.getClientById(Integer.parseInt(clientId));

            NpClientUser user = null;

            for (NpClientUser clientUser : client.getClientUsers())
            {
                user = clientUser;
                break;
            }
            form = new AddClientForm();
            form.setId(client.getId());
            form.setAddress1(client.getAddress1());
            form.setAddress2(client.getAddress2());
            form.setCompanyName(client.getCompanyName());
            form.setCountry(client.getCountry());
            form.setFirstName(client.getFirstName());
            form.setLastName(client.getLastName());
            form.setMobileNumber(client.getMobileNumber());
            form.setTeleNumber(client.getTeleNumber());
            form.setTown(client.getTown());
            form.setPostCode(client.getPostalCode());
            form.setPrincipleContactEmail(client.getPrincipleContactEmail());
            if (client.getCompanyLogo() != null)
            {
                model.addAttribute("company", new String(Base64.encodeBase64(client.getCompanyLogo())));
            }

            form.setAdminFirstName(user.getFirstName());

            form.setAdminLastName(user.getLastName());

            form.setAdminEmail(user.getPrimaryEmail());

            form.setClientStatusId(client.getClientStatus().getId());

            form.setSectorId(client.getSector().getId());

            form.setAdminId(user.getId());
        }
        else
        {
            form = new AddClientForm();
        }

        model.addAttribute("form", form);

        return "components/editClient.jsp";
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/edit-client", method = RequestMethod.POST)
    public String handleAddClient(@ModelAttribute AddClientForm form, Model model, HttpServletRequest request)
    {
        NpClient client;
        Document clientUserImage = MgnlContext.getPostedForm().getDocument("companyLogo");
        String extension = null;
        File userImage = null;
        if (clientUserImage != null)
        {
            userImage = clientUserImage.getFile();
            form.setCompanyImage(clientUserImage.getFile());
        }

        String rootPath = request.getContextPath();
//  String fileSaveAs =  serverFile+"\\"+npCandidate.getId()+"."+listOfFile.get("photo").getExtension();
        String fileNameWithExtension = null;
        String fileSavedPath = null;
        if (userImage != null)
        {
            fileNameWithExtension = form.getFirstName() + "_photo." + clientUserImage.getExtension();
            fileSavedPath = saveFile(rootPath, userImage, fileNameWithExtension);
            if (!StringUtils.isEmpty(fileSavedPath))
            {
                LOG.info("photo- " + fileSavedPath);
                form.setCompanyLogo(fileSavedPath);
            }
        }
        String message = "";
        if (StringUtils.isEmpty(form.getId()))
        {
            client = clientService.addClient(form);
            message = "A new client added successfully ";
        }
        else
        {
            if (!clientService.isAddedDuplicateEmail(form.getId(), form.getAdminId(), form.getAdminEmail()))
            {
                client = clientService.updateClient(form);
                message = "Client record edited successfully ";
            }
            else
            {
                client = clientService.getClientById(form.getId());
                try
                {
                    if (client.getCompanyLogo() != null)
                    {
                        model.addAttribute("company", new String(Base64.encodeBase64(client.getCompanyLogo())));
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                model.addAttribute("form", form);
                model.addAttribute("errorMsg", "User already exist please enter valid email address");
                return "components/editClient.jsp";
            }
        }
        model.addAttribute("showEditMsg", true);
        return "redirect:clientUserManagement";
    }

    /*
     * for file upload in a folder
     * */
    @SuppressWarnings("resource")
    public String saveFile(String rootPath, File file, String fileNameWithExtension)
    {

        // Creating the directory to store file

        File dir = new File(rootPath + File.separator + "DocFiles");
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        // Create the file on server

        File serverFile = new File(dir.getAbsolutePath() + File.separator);
        LOG.info("Server File Location=" + serverFile.getAbsolutePath());

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try
        {
            inputStream = new FileInputStream(file.getAbsolutePath());
            outputStream = new FileOutputStream(serverFile + "\\" + fileNameWithExtension);

            Integer c;
            //continue reading till the end of the file
            while ((c = inputStream.read()) != -1)
            {
                //writes to the output Stream
                outputStream.write(c);
            }
            LOG.info("Photo is saved in : " + serverFile);
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            LOG.error("File Not Exist " + e);
        }
        catch (IOException e)
        {
            LOG.error("Error Occur while saving the file" + e);
        }
        LOG.info("path returned: " + serverFile.getAbsolutePath() + "\\" + fileNameWithExtension);
        return serverFile.getAbsolutePath() + "\\" + fileNameWithExtension;
    }
}