/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientService;
import com.newposition.common.forms.ImpersonateForm;
import com.newposition.common.service.NpUsersReasonService;

/**
 * @author Sachi N
 */

@Controller
@Template(title = "Impersonate Client User Component", id = "newPositionModule:components/clientUserImpersonateUserComponent")
public class NpClientImpersonateUserComponent
{
    protected static final Logger LOG = Logger
            .getLogger(NpClientImpersonateUserComponent.class);

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpUsersReasonService npUsersReasonService;

    @Value("${pagination.numberOfUserReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/clientImpersonateUser", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model, Authentication authentication)
    {

        String authUser = "";
        try
        {
            authUser = authentication.getName();
        }
        catch (NullPointerException npe)
        {
            return "redirect:home";
        }

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = (String) request.getParameter("sortBy");
        List listOfClients = clientService.getSortedNpUserClients(sortBy, pageNumber, authUser);
        model.addAttribute("sortBy", sortBy);

        int npClientSize = (int) listOfClients.get(1);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npClientSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("noOfNpUsers", npClientSize);
        model.addAttribute("npClientTotalPages", (int) noOfPages);
        model.addAttribute("npUsersList", listOfClients.get(0));
        model.addAttribute("reasonForm", new ReasonForm());
        model.addAttribute("impersonateForm", new ImpersonateForm());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForClient());

        return "components/clientUserImpersonateUserComponent.jsp";
    }

    @RequestMapping(value = "/clientImpersonateUser", method = RequestMethod.POST)
    public String register(@ModelAttribute("impersonateForm") ImpersonateForm impersonateForm,
                           HttpServletRequest request, Model model)
    {

        return "redirect:/clientDashboard";
    }
}
