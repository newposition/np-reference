/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClient;
import com.newposition.client.service.NpClientCostCentreService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.service.NpUserService;

/*
 * @author Sachi N
 *
 *
 */
@Controller
@Template(title = "Client Dashboard", id = "newPositionModule:components/clientDashboard")
public class NpClientDashboardComponent
{

    protected static final Logger LOG = Logger.getLogger(NpClientDashboardComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpClientService npClientService;

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpClientCostCentreService npClientCostCentreService;

    @RequestMapping(value = "/clientDashboard", method = RequestMethod.GET)
    public String render(HttpServletRequest request, Model model, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());
        if (npUser != null)
        {
            Set<NpUserRole> npUserRoles = npUser.getNpUserRole();
            Iterator<NpUserRole> userRoleIterator = npUserRoles.iterator();
            String userRoleName = "";
            for (; userRoleIterator.hasNext(); )
            {
                userRoleName = userRoleIterator.next().getNpRole().getRoleName();
                LOG.info(userRoleName);
            }
            model.addAttribute("roleName", userRoleName);
        }
        try
        {
            Integer userListNo = clientService.getCountNpUserClients(authentication.getName());

            if (npUser != null)
            {
                NpClient client = npClientService.getCompanyOfCurrentUser(authentication.getName());
                model.addAttribute("companyName", client.getCompanyName());
                model.addAttribute("refereCandidateNo", npClientService.getUserReferences(authentication.getName()));
                model.addAttribute("clientUserCount", userListNo);
            }
        }
        catch (NullPointerException npe)
        {
            return "redirect:home";
        }
        return "components/clientDashboard.jsp";
    }
}
