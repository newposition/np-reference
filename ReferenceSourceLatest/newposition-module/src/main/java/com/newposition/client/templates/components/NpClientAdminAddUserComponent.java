/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.validator.ClientSupportAddEditUserFormValidator;
import com.newposition.client.service.NpClientService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.service.NpUserService;

/**
 * @author Sachi N
 */

@Controller
@Template(title = "client administrator add-user Form", id = "newPositionModule:components/clientAdminAddEditUserComponent")
public class NpClientAdminAddUserComponent
{
    protected static final Logger LOG = Logger.getLogger(NpClientAdminAddUserComponent.class);

    @Autowired
    private NpClientSupportAddEditUserService clientSupportAddEditUserService;

    @Autowired
    private ClientSupportAddEditUserFormValidator clientSupportAddEditUserFormValidator;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpClientService npClientService;

    @Value("${clientSupport.addEditUser.activeLink}")
    private String clientSupportAddEditUseractiveLink;

    @RequestMapping(value = "/clientAdminAddEditUser", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        NpClientSupportAddEditUserForm clientSupportAddEditUserForm = new NpClientSupportAddEditUserForm();
        if (request.getParameter("id") != null)
        {
            String id = (String) request.getParameter("id");
            NpUser npUser = clientSupportAddEditUserService.getNpUsersForId(id);
            clientSupportAddEditUserForm.setFirstName(npUser.getFirstName());
            clientSupportAddEditUserForm.setLastName(npUser.getLastName());
            clientSupportAddEditUserForm.setPrimaryEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setConfirmEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setMobile(npUser.getPhone());
            clientSupportAddEditUserForm.setOfficeTelePhone(npUser.getOfficeTelephone());
            clientSupportAddEditUserForm.setUserId(npUser.getId().toString());
            clientSupportAddEditUserForm.setJobTitle(npUser.getJobtitle());

            Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
            List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);

            if (!npUserRolesSet.isEmpty())
            {
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
        }
        model.addAttribute("clientSupportAddEditUserForm", clientSupportAddEditUserForm);
        return "components/clientAdminAddEditUserComponent.jsp";
    }

    @RequestMapping(value = "/clientAdminAddEditUser", method = RequestMethod.POST)
    public String register(@ModelAttribute("clientSupportAddEditUserForm") NpClientSupportAddEditUserForm clientSupportAddEditUserForm, BindingResult result, Model model, Authentication authentication)
    {

        clientSupportAddEditUserFormValidator.validate(clientSupportAddEditUserForm, result);

        if (result.hasErrors())
        {

            if (StringUtils.isNotEmpty(clientSupportAddEditUserForm.getUserId()))
            {

                NpClientUser npUser = (NpClientUser) clientSupportAddEditUserService.getNpUsersForId(clientSupportAddEditUserForm.getUserId());
                Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
                List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
            model.addAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/clientAdminAddEditUserComponent.jsp";
        }

        if (StringUtils.isEmpty(clientSupportAddEditUserForm.getUserId()))
        {
            if (npUserService.findByUserName(clientSupportAddEditUserForm.getPrimaryEmailAddress()) != null)
            {
                model.addAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
                model.addAttribute("errEmail", "User email address already exists");
                return "components/clientAdminAddEditUserComponent.jsp";
            }
        }
        else
        {
            NpClientUser user = (NpClientUser) npUserService.findByUserName(authentication.getName());
            if (!npClientService.isAddedDuplicateUserEmail(user.getOrganization().getId(), Integer.parseInt(clientSupportAddEditUserForm.getUserId()), clientSupportAddEditUserForm.getPrimaryEmailAddress()))
            {
                System.out.println(false);
                model.addAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
                model.addAttribute("errEmail", "User email address already exists");
                return "components/clientSupportAddUserComponent.jsp";
            }
        }

        clientSupportAddEditUserService.saveClientUser(authentication.getName(), clientSupportAddEditUserForm);

        if (StringUtils.isEmpty(clientSupportAddEditUserForm.getUserId()))
        {
            clientSupportAddEditUserService.sendAccountActivationMail(clientSupportAddEditUserForm, clientSupportAddEditUseractiveLink);
            model.addAttribute("showAddMsg", true);
        }
        else
        {
            model.addAttribute("showEditMsg", true);
        }

        return "redirect:user_management";
    }
}
