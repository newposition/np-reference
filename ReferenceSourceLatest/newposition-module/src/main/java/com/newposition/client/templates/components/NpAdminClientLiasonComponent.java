

/**
 *
 */
package com.newposition.client.templates.components;

import java.util.Iterator;
import java.util.Set;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.service.NpUserService;


/*
 * @author santosh
 *
 *
 */

/*
 * Component that renders the Admin User
 * 
 */
@Controller
@Template(title = "AdminClientLiason", id = "newPositionModule:components/adminClientLiason")
public class NpAdminClientLiasonComponent
{
    protected static final Logger LOG = Logger.getLogger(NpClientDashboardComponent.class);

    @Autowired
    private NpUserService npUserService;

    @Autowired
    NpCandidateService npCadnidateService;

    @RequestMapping(value = "/adminClientLiason", method = RequestMethod.GET)
    public String adminClientLiasonGet(HttpServletRequest request, Model model, Authentication authentication)
    {
        if (authentication != null)
        {
            NpUser user = npUserService.findByUserName(authentication.getName());
            if (user != null)
            {
                Integer referenceNumber = npCadnidateService.getCandidateReferenceList(authentication.getName()).size();
                model.addAttribute("refereCandidateNo", referenceNumber);
            }
        }

        return "components/adminClientLiason.jsp";
    }

    @RequestMapping(value = "/adminClientLiason", method = RequestMethod.POST)
    public String adminClientLiasonPost(HttpServletRequest request, Authentication authentication)
    {
        /*NpUser npUser = npUserService.findByUserName(authentication.getName());
          List<NpCandidate> candList=loginDaoImpl.refernceList(user.getId());
	  	request.getSession().setAttribute("candList", candList);
	*/
        return "redirect:adminClientLiason";
    }
}
