/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientService;
import com.newposition.common.service.NpUsersReasonService;

@Controller
@Template(title = "ClientUserManagement Component", id = "newPositionModule:components/clientUserManagement")
public class NpClientUserManagementComponent
{

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpUsersReasonService npUsersReasonService;

    @Value("${pagination.numberOfUserReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/clientUserManagement", method = RequestMethod.GET)
    public String getAddClientLiasonPage(AddClientForm form, HttpServletRequest request, Model model, String clientId)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = (String) request.getParameter("sortBy");
        List listOfClients = clientService.getSortedNpClients(sortBy, pageNumber);
        model.addAttribute("sortBy", sortBy);

        int npClientSize = (int) listOfClients.get(1);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npClientSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npClientCount", npClientSize);
        model.addAttribute("npClientTotalPages", (int) noOfPages);
        model.addAttribute("clients", listOfClients.get(0));
        model.addAttribute("reasonForm", new ReasonForm());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForClient());
        return "components/clientUserManagement.jsp";
    }

    @RequestMapping(value = "/clientUserManagement", method = RequestMethod.POST)
    public String handleAddClient(@ModelAttribute("reasonForm") ReasonForm reasonForm, Model model)
    {
        if (reasonForm.isSuspendOrDelete())
        {
            clientService.deleteNpClient(reasonForm);
            model.addAttribute("showDeleteMsg", true);
        }

        if (!reasonForm.isSuspendOrDelete())
        {
            clientService.updateNpClientStatus(reasonForm);
            model.addAttribute("showStatusMsg", true);
        }
        return "redirect:clientUserManagement";
    }
}