/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.validator.ClientSupportAddEditUserFormValidator;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;

/**
 * @author Trinadh
 */

@Controller
@Template(title = "ClientSupport Add User Form", id = "newPositionModule:components/clientSupportAddUserComponent")
public class NpClientSupportAddUserComponent
{
    protected static final Logger LOG = Logger.getLogger(NpClientSupportAddUserComponent.class);

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpClientSupportAddEditUserService clientSupportAddEditUserService;

    @Autowired
    private ClientSupportAddEditUserFormValidator clientSupportAddEditUserFormValidator;

    @Value("${clientSupport.addEditUser.activeLink}")
    private String clientSupportAddEditUseractiveLink;

    @Value("${clientSupport.addEditUser.activationPageLink}")
    private String clientSupportAddEditUseractivationPageLink;

    @Value("${email.images.context}")
    private String imageContext;

    @RequestMapping(value = "/clientSupportAddEditUser", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        NpClientSupportAddEditUserForm clientSupportAddEditUserForm = new NpClientSupportAddEditUserForm();
        if (request.getParameter("id") != null)
        {
            String id = (String) request.getParameter("id");
            NpUser npUser = clientSupportAddEditUserService.getNpUsersForId(id);
            clientSupportAddEditUserForm.setFirstName(npUser.getFirstName());
            clientSupportAddEditUserForm.setLastName(npUser.getLastName());
            clientSupportAddEditUserForm.setPrimaryEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setConfirmEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setMobile(npUser.getPhone());
            clientSupportAddEditUserForm.setOfficeTelePhone(npUser.getOfficeTelephone());
            clientSupportAddEditUserForm.setUserId(npUser.getId().toString());
//        	clientSupportAddEditUserForm.setCurrentRole(npUser.getNpUserRoles().getNpRoles().getRoleName());

//        	fetching NpUserRoles and set it into clientSupportAddEditUserForm
//        	NpUserRoles npUserRoles = null;
            Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
            List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);

            if (!npUserRolesSet.isEmpty())
            {
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
        }

        model.addAttribute("clientSupportAddEditUserForm", clientSupportAddEditUserForm);
        return "components/clientSupportAddUserComponent.jsp";
    }

    @RequestMapping(value = "/clientSupportAddEditUser", method = RequestMethod.POST)
    public String register(@ModelAttribute("clientSupportAddEditUserForm") @Validated NpClientSupportAddEditUserForm clientSupportAddEditUserForm, BindingResult result, HttpServletRequest request, Model model)
    {

        clientSupportAddEditUserFormValidator.validate(clientSupportAddEditUserForm, result);
        if (result.hasErrors())
        {

            if (StringUtils.isNotEmpty(clientSupportAddEditUserForm.getUserId()))
            {
                NpUser npUser = clientSupportAddEditUserService.getNpUsersForId(clientSupportAddEditUserForm.getUserId());
                Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
                List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
            request.setAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/clientSupportAddUserComponent.jsp";
        }

        if (!StringUtils.isEmpty(clientSupportAddEditUserForm.getUserId()))
        {

            clientSupportAddEditUserService.saveAndUpdateNpUser(clientSupportAddEditUserForm);

            LOG.info("Updated the user");

            model.addAttribute("successMessage", "User edited successfully");

            return "components/clientSupportAddUserComponent.jsp";
        }
        UserDetails users = npUserService.loadUserByUsername(clientSupportAddEditUserForm.getPrimaryEmailAddress());
        if (users == null)
        {
            clientSupportAddEditUserService.saveClientSupportAddEditUser(clientSupportAddEditUserForm);
        }
        else
        {
            request.setAttribute("epm", "User Already Exists");
            return "components/clientSupportAddUserComponent.jsp";
        }

        Long currentTime = System.currentTimeMillis();
        String currentTimeString = "";
        String link = null;
        try
        {
            link = DatatypeConverter.printBase64Binary(clientSupportAddEditUserForm.getPrimaryEmailAddress().getBytes());
            currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String activeLink = clientSupportAddEditUseractiveLink + "?id=" + link + "&dur=" + currentTimeString;
        String subject = "NewPosition Account activation request";
        sendMail(clientSupportAddEditUserForm, activeLink, subject, "adminAdduserEmail.html");

        model.addAttribute("showAddMsg", true);

        return "redirect:clientSupportUserManagement";
    }

    private void sendMail(NpClientSupportAddEditUserForm clientSupportAddEditUserForm, String link, String subject, String mailHtml)
    {

        String candidateName = clientSupportAddEditUserForm.getFirstName() + " " + clientSupportAddEditUserForm.getLastName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", candidateName);
        context.setVariable("loginPageLink", link);
        context.setVariable("activationPageLink", link);
        context.setVariable("template", mailHtml);
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);
        npMailService.sendMail(information, clientSupportAddEditUserForm.getPrimaryEmailAddress(), context);
    }
}
