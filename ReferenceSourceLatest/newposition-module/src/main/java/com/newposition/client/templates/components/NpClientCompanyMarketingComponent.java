package com.newposition.client.templates.components;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.ClientMarketingForm;
import com.newposition.client.service.NpClientCompanyMarketingService;
import com.newposition.client.service.NpClientService;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title = "Client Marketing", id = "newPositionModule:components/clientMarketing")
public class NpClientCompanyMarketingComponent
{

    @Autowired
    private NpClientCompanyMarketingService npClientCompanyMarketingService;

    @Autowired
    private NpClientService clientService;

    protected static final Logger LOG = Logger.getLogger(NpClientCompanyMarketingComponent.class);

    @RequestMapping(value = "/clientMarketing", method = RequestMethod.GET)
    public ModelAndView clientMarketing(HttpServletRequest request, Model model)
    {

        ClientMarketingForm clientMarketingForm = new ClientMarketingForm();

        model.addAttribute("clientMarketingForm", clientMarketingForm);

        ModelAndView view = new ModelAndView("components/clientMarketing.jsp");

        return view;
    }

    @RequestMapping(value = "/clientMarketing", method = RequestMethod.POST)
    public String clientMarketingForm(@ModelAttribute("clientMarketingForm") ClientMarketingForm clientMarketingForm,
                                      BindingResult result, HttpServletRequest request, Authentication authentication)
    {

        NpClient client = clientService.getCompanyOfCurrentUser(authentication.getName());
        client.setClientOverview(clientMarketingForm.getClientOverview());

        System.out.println(MgnlContext.getPostedForm().getDocument("video"));

        File video = MgnlContext.getPostedForm().getDocument("video").getFile();
        clientMarketingForm.setVideo(video);
        npClientCompanyMarketingService.saveClientMarketingForm(client, clientMarketingForm);

        return "components/clientMarketing.jsp";
    }
}
