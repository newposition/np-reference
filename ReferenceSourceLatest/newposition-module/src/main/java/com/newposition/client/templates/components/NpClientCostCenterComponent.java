/**
 *
 */
package com.newposition.client.templates.components;

import java.io.File;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.forms.AddClientCostCentreForm;
import com.newposition.client.service.NpClientCostCentreService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "Cost Center Component", id = "newPositionModule:components/costCenter")
public class NpClientCostCenterComponent
{

    protected static final Logger LOG = Logger
            .getLogger(NpClientCostCenterComponent.class);

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpClientCostCentreService npClientCostCentreService;

    @Value("${pagination.numberOfReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/cost-center", method = RequestMethod.GET)
    public String getCostCenterPage(HttpSession session, Model model, Authentication authentication)
    {

        LOG.info("======================== Getting Cost Center Page ====================");
        model.addAttribute("costCentreData", npClientCostCentreService.findAllCostCentreOfCurrentClientWithVideo(authentication.getName()));
        AddClientCostCentreForm costCentreForm = new AddClientCostCentreForm();
        model.addAttribute("costCentreForm", costCentreForm);
        model.addAttribute("noOfResultPerPage", numberOfReusltsPerPage);
        LOG.info("======================== Getting Cost Center Page ====================" + numberOfReusltsPerPage);
        /**
         *
         * send the message add or edit CC
         *
         */
        model.addAttribute("message", getMessage((String) session.getAttribute("cmsg")));
        session.setAttribute("cmsg", null);
        return "components/costCenter.jsp";
    }

    private String getMessage(String msg)
    {
        if (StringUtils.isNotEmpty(msg))
        {
            if (StringUtils.equals(msg, "1"))
            {
                return "CostCentre added successfully";
            }
            else if (StringUtils.equals(msg, "2"))
            {
                return "CostCentre edited successfully";
            }
        }

        return null;
    }

    @RequestMapping(value = "/cost-center", method = RequestMethod.POST)
    public String handleAddClient(
            @ModelAttribute AddClientCostCentreForm costCentreForm,
            Model model, Authentication authentication, HttpSession session)
    {

        LOG.info("====================== post method =========================");
        LOG.info("======================== costCentreCode : " + costCentreForm.getCostCentreCode());
        LOG.info("======================== descrption  ====================" + costCentreForm.getDescription());
        LOG.info("======================== Owner  ====================" + costCentreForm.getCostCentreOwner());
        LOG.info("======================== System  ====================" + costCentreForm.getCostCentreSystem());
        LOG.info("======================== Location  ====================" + costCentreForm.getLocation());
        try
        {
            File video = MgnlContext.getPostedForm().getDocument("video").getFile();
            costCentreForm.setVideo(video);
            if (video != null)
            {
                LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  file uploaded  "
                        + video.getName());
            }
            else
            {
                LOG.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    file not available");
            }
        }
        catch (NullPointerException npe)
        {
            LOG.info("Not Entered Any file");
        }

        if (costCentreForm.getCostCentreId() == null)
        {
            npClientCostCentreService.saveCostCentre(costCentreForm,
                    authentication.getName());
            session.setAttribute("cmsg", "1");
        }
        else
        {
            npClientCostCentreService.updateCostCentre(costCentreForm);
            session.setAttribute("cmsg", "2");
        }

        return "redirect:cost_centre";
    }
}