package com.newposition.client.templates.components;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClient;
import com.newposition.client.service.NpClientCompanyInformationService;

import info.magnolia.module.blossom.annotation.Template;

@Controller
@Template(title = "Client CompanyInformation", id = "newPositionModule:components/clientCompanyInformation")
public class NpClientCompanyInformationComponent
{

    protected static final Logger LOG = Logger.getLogger(NpClientCompanyInformationComponent.class);

    @Autowired
    private NpClientCompanyInformationService npClientCompanyInformationService;

    @RequestMapping(value = "/clientCompanyInformation", method = RequestMethod.GET)
    public String render(HttpServletRequest request, Model model, Authentication authentication)
    {

        NpClient clientUser = npClientCompanyInformationService.findByUserName(authentication.getName());

        model.addAttribute("form", clientUser);

        return "components/clientCompanyInformation.jsp";
    }
}
