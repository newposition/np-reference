package com.newposition.client.templates.components;

import info.magnolia.context.MgnlContext;
import info.magnolia.module.blossom.annotation.Template;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.domain.NpClientProjectDocument;
import com.newposition.client.service.NpClientService;
import com.newposition.client.service.NpProjectService;
import com.newposition.common.domain.NpUser;

@Controller
@Template(title = "Project Component", id = "newPositionModule:components/projectComponent")
public class NpProjectsComponent
{

    @Autowired
    NpClientService npClientService;

    @Autowired
    NpProjectService npProjectService;

    protected static final Logger LOG = Logger
            .getLogger(NpProjectsComponent.class);

    @RequestMapping(value = "/projectComponent", method = RequestMethod.GET)
    public String displayProjects(Model model, HttpSession session, Authentication authentication)
    {

        NpClient client = npClientService.getCompanyOfCurrentUser(authentication.getName());
//		model.addAttribute("p_videos",npProjectService.getAllClientProjectsWithVideo(client));

        model.addAttribute("message", getMessage((String) session.getAttribute("cmsg")));
        session.setAttribute("cmsg", null);
        return "components/npProjects.jsp";
    }

    private String getMessage(String msg)
    {
        if (StringUtils.isNotEmpty(msg))
        {
            if (StringUtils.equals(msg, "1"))
            {
                return "Project Added Successfully";
            }
            else if (StringUtils.equals(msg, "2"))
            {
                return "Project Edited Successfully";
            }
        }

        return null;
    }

    @RequestMapping(value = "/projectComponent", method = RequestMethod.POST)
    public String saveNewProjects(NpClientProject npProject, Model model, NpClientCostCentreImpl npClientCostCentreImpl, HttpSession session)
    {
        System.out.println("in post method");
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        npProject.setNpClientCostCentreImpl(npClientCostCentreImpl);
        npProject.setNpClient(npClient);

        /**
         *
         * getting the video file from the Magnolia context
         *
         *
         * */
        File video = null;
        byte[] videoInByte = null;
        try
        {
            video = MgnlContext.getPostedForm().getDocument("video").getFile();
            videoInByte = npProjectService.getVideo(video);
            if (video != null)
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  file uploaded  "
                        + video.getName());
            }
            else
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    file not available");
            }
        }
        catch (NullPointerException npe)
        {
            System.out.println("Getting File or Video as Null");
        }

        if (npProject.getClientProjectID() == null)
        {
            NpClientProjectDocument proDoc = new NpClientProjectDocument();
            proDoc.setDocument(videoInByte);

            npProjectService.saveProgramme(npProject, proDoc);
            session.setAttribute("cmsg", "1");
        }
        else
        {

            NpClientProject project = npProjectService.getNpProjectById(npProject.getClientProjectID().intValue());
            if (videoInByte != null)
            {
                NpClientProjectDocument doc = project.getProjectDoc();

                doc.setDocument(videoInByte);
                project.setProjectDoc(doc);
            }
            NpClientProject _project = npProjectService.updateWithNewValue(npProject, project);

            npProjectService.saveOrUpdate(_project);
            session.setAttribute("cmsg", "2");
        }
        return "redirect:projects";
    }
}
