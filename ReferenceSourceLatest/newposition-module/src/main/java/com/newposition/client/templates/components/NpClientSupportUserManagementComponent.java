/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUsersReasonService;
import com.newposition.infra.mail.NpMailService;

/**
 * @author Yamuna Vemula
 */

@Controller
@Template(title = "ClientSupport User Management", id = "newPositionModule:components/clientSupportUserManagementComponent")
public class NpClientSupportUserManagementComponent
{
    protected static final Logger LOG = Logger.getLogger(NpClientSupportUserManagementComponent.class);

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpClientSupportAddEditUserService clientSupportAddEditUserService;

    @Autowired
    private NpUsersReasonService npUsersReasonService;

    @Value("${pagination.numberOfUserReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/clientSupportUserManagement", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {
        List listOfNpUsers = null;

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = (String) request.getParameter("sortBy");

        listOfNpUsers = clientSupportAddEditUserService.getSortedNpUsers(sortBy, pageNumber);
        model.addAttribute("npUsers", listOfNpUsers);

        int npUserSize = (int) listOfNpUsers.get(1);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npUserSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npUsersCount", npUserSize);
        model.addAttribute("npUserTotalPages", (int) noOfPages);
        model.addAttribute("npUsers", listOfNpUsers.get(0));

        model.addAttribute("reasonForm", new ReasonForm());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForUser());

        return "components/clientSupportUserManagementComponent.jsp";
    }

    @RequestMapping(value = "/clientSupportUserManagement", method = RequestMethod.POST)
    public String register(@ModelAttribute("reasonForm") ReasonForm reasonForm, HttpServletRequest request, Model model)
    {

//    	String queryMsg= "";
        if (reasonForm.isSuspendOrDelete())
        {
            clientSupportAddEditUserService.deleteNpuser(reasonForm);
//    		queryMsg = "?showDeleteMsg=true";
            model.addAttribute("showDeleteMsg", true);
        }

        if (!reasonForm.isSuspendOrDelete())
        {
            clientSupportAddEditUserService.updateNpuserStatus(reasonForm);
            if (reasonForm.getStatusId() == 2)
            {
                String subject = "User Account Disabled";
                clientSupportAddEditUserService.sendAccountDisabledMail(reasonForm, subject);
            }
//    		queryMsg = "?showStatusMsg=true";
            model.addAttribute("showStatusMsg", true);
        }
        model.addAttribute("reasonForm", new ReasonForm());
        model.addAttribute("npUsers", clientSupportAddEditUserService.getNpUsers());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForUser());
//    	npUserStatus.setId(2);
        return "redirect:clientSupportUserManagement";
    }

   /* private void sendMail(RegistrationForm registrationForm, String link, String subject) {

    	String candidateName = registrationForm.getFirstName() + " " + registrationForm.getLastName();
    	String information = subject;
	    Context context = new Context();
	    context.setVariable("candidateName", candidateName);
	    context.setVariable("loginPageLink", link);
	    context.setVariable("activationPageLink", "http://52.74.221.110:8080/newposition/activationPage");
	    context.setVariable("template", "registration-email.html");
	    
	    npMailService.sendMail(information, registrationForm.getPrimaryEmailAddress(),context);
    }


    private String rpHash(String value) {
        int hash = 5381;
        value = value.toUpperCase();
        for (int i = 0; i < value.length(); i++) {
            hash = ((hash << 5) + hash) + value.charAt(i);
        }
        return String.valueOf(hash);
    }
*/
}
