package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.client.forms.validator.NpClientAddRoleGroupFormValidator;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.CoGroups;

@Controller
@Template(title = "Edit Client RoleGroup Component", id = "newPositionModule:components/editRoleGroup")
public class NpClientEditRoleGroupManagementComponent
{

    @Autowired
    private NpClientRoleManagementService npClientRoleManagementService;

    @Autowired
    private NpClientSupportAddEditUserService npClientSupportAddEditUserService;

    @Autowired
    private NpClientAddRoleGroupFormValidator npClientAddRoleGroupFormValidator;

    @RequestMapping(value = "/editClientRoleGroup", method = RequestMethod.GET)
    public String getClientEditRoleGroupManagementComponent(Model model, NpClientAddRoleGroupForm npClientAddRoleGroupForm, Authentication authentication, String id)
    {

        CoGroups coGroup = npClientRoleManagementService.getCoGroupById(Integer.parseInt(id));
        npClientAddRoleGroupForm.setGroupId("" + coGroup.getId());
        npClientAddRoleGroupForm.setGroupName(coGroup.getGroupName());
        npClientAddRoleGroupForm.setGroupDescription(coGroup.getGroupDescription());

        model.addAttribute("clientRoles", coGroup.getCoRoleGroups());

        return "components/editClientRoleGroupManagementComponent.jsp";
    }

    @RequestMapping(value = "/editClientRoleGroup", method = RequestMethod.POST)
    public String postClientEditRoleGroupManagementComponent(@ModelAttribute("npClientAddRoleGroupForm") @Validated NpClientAddRoleGroupForm npClientAddRoleGroupForm, BindingResult result, Authentication authentication, HttpServletRequest request, Model model)
    {

        npClientAddRoleGroupFormValidator.validate(npClientAddRoleGroupForm, result);
        if (result.hasErrors())
        {
            model.addAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/editClientRoleGroupManagementComponent.jsp";
        }

        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        npClientRoleManagementService.saveGroups(npClientAddRoleGroupForm, clientUser.getOrganization().getId(), clientUser.getFirstName());

        RequestContextUtils.getOutputFlashMap(request).put("sm", "Client RoleGroup Edited Sucessfully");

        //request.getSession().setAttribute("sm","ClientRoleGroupEditedSucessfully");

        return "redirect:clientRoleGroupManagement";
    }
}
