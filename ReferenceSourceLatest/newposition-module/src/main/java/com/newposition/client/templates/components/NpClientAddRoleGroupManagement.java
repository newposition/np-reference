package com.newposition.client.templates.components;

import javax.servlet.http.HttpServletRequest;

import info.magnolia.module.blossom.annotation.Template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.client.forms.validator.NpClientAddRoleGroupFormValidator;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.client.service.NpClientSupportAddEditUserService;

@Controller
@Template(title = "Add Client RoleGroup Component", id = "newPositionModule:components/addRoleGroup")
public class NpClientAddRoleGroupManagement
{

    @Autowired
    private NpClientRoleManagementService npClientRoleManagementService;

    @Autowired
    private NpClientAddRoleGroupFormValidator npClientAddRoleGroupFormValidator;

    @Autowired
    private NpClientSupportAddEditUserService npClientSupportAddEditUserService;

    @RequestMapping(value = "/add-Role", method = RequestMethod.GET)
    public String getClientAddRoleManagementComponent(Model model, NpClientAddRoleGroupForm npClientAddRoleGroupForm, Authentication authentication)
    {

        return "components/addClientRoleGroupManagementComponent.jsp";
    }

    @RequestMapping(value = "/add-Role", method = RequestMethod.POST)
    public String postClientAddRoleGroupManagement(@ModelAttribute("npClientAddRoleGroupForm") @Validated NpClientAddRoleGroupForm npClientAddRoleGroupForm, BindingResult result, Authentication authentication, HttpServletRequest request, Model model)
    {

        npClientAddRoleGroupFormValidator.validate(npClientAddRoleGroupForm, result);
        if (result.hasErrors())
        {
            model.addAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/addClientRoleGroupManagementComponent.jsp";
        }
        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        npClientRoleManagementService.saveGroups(npClientAddRoleGroupForm, clientUser.getOrganization().getId(), clientUser.getFirstName());

        RequestContextUtils.getOutputFlashMap(request).put("sm", "Client RoleGroup Added Sucessfully");
        //request.getSession().setAttribute("sm","ClientRoleGroupAddedSucessfully");

        return "redirect:clientRoleGroupManagement";
    }
}
