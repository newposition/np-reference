/**
 *
 */
package com.newposition.client.templates.components;

import java.util.List;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.service.NpUsersReasonService;

@Controller
@Template(title = "Client Admin UserManagement Component", id = "newPositionModule:components/clientAdminUserManagement")
public class NpClientAdminUserManagementComponent
{

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpUsersReasonService npUsersReasonService;

    @Autowired
    private NpClientSupportAddEditUserService clientSupportAddEditUserService;

    @Value("${pagination.numberOfUserReuslts.perPage}")
    private int numberOfReusltsPerPage;

    @RequestMapping(value = "/client_UserManagement", method = RequestMethod.GET)
    public String getAddClientLiasonPage(AddClientForm form, HttpServletRequest request, Model model, Authentication authentication)
    {

        String pageNumber = request.getParameter("pageNumber");
        if (StringUtils.isBlank(pageNumber))
        {
            pageNumber = "0";
        }

        String sortBy = (String) request.getParameter("sortBy");
        List listOfClients = clientService.getSortedNpUserClients(sortBy, pageNumber, authentication.getName());
        model.addAttribute("sortBy", sortBy);

        int npClientSize = (int) listOfClients.get(1);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("currentPageNumber", pageNumber);

        double noOfPages = (double) npClientSize / (double) numberOfReusltsPerPage;
        noOfPages = Math.ceil(noOfPages);

        model.addAttribute("npClientCount", npClientSize);
        model.addAttribute("npClientTotalPages", (int) noOfPages);
        model.addAttribute("users", listOfClients.get(0));
        model.addAttribute("reasonForm", new ReasonForm());
        model.addAttribute("npUsersReasons", npUsersReasonService.getReasonsForClient());
        return "components/clientAdminUserManagement.jsp";
    }

    @RequestMapping(value = "/client_UserManagement", method = RequestMethod.POST)
    public String handleAddClient(@ModelAttribute("reasonForm") ReasonForm reasonForm, Model model)
    {
        if (reasonForm.isSuspendOrDelete())
        {
            clientSupportAddEditUserService.deleteNpuser(reasonForm);
            model.addAttribute("showDeleteMsg", true);
        }

        if (!reasonForm.isSuspendOrDelete())
        {
            clientSupportAddEditUserService.updateNpuserStatus(reasonForm);
            if (reasonForm.getStatusId() == 2)
            {
                String subject = "User Account Disabled";
                clientSupportAddEditUserService.sendAccountDisabledMail(reasonForm, subject);
            }
            model.addAttribute("showStatusMsg", true);
        }
        return "redirect:user_management";
    }
}