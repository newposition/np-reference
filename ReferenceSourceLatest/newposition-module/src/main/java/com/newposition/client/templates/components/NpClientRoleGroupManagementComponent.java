package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.CoGroups;

@Controller
@Template(title = "RoleGroupList Component", id = "newPositionModule:components/roleGroupList")
public class NpClientRoleGroupManagementComponent
{

    @Autowired
    private NpClientRoleManagementService npClientRoleManagementService;

    @Autowired
    private NpClientSupportAddEditUserService npClientSupportAddEditUserService;

    @RequestMapping(value = "/roleList", method = RequestMethod.GET)
    public String getRoleManagementListComponent(Model model, Authentication authentication, HttpServletRequest request, String delete)
    {

        String sortBy = request.getParameter("sortBy");

        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        List<CoGroups> coGroups = npClientRoleManagementService.getListOfClientGroupsById(clientUser.getOrganization().getId(), sortBy);
        model.addAttribute("coGroups", coGroups);
        model.addAttribute("sortBy", sortBy);
        if (delete != null && delete.equals("Delete"))
        {
            model.addAttribute("sm", "RoleGroup Deleted Sucessfully");
        }
        return "components/clientRoleGroupManagementComponent.jsp";
    }
}
