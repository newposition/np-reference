
/**
 *
 */
package com.newposition.client.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.validator.ClientSupportAddEditUserFormValidator;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;

/**
 * @author Trinadh
 */

@Controller
@Template(title = "ClientSupport Edit User Form", id = "newPositionModule:components/clientSupportEditUserComponent")
public class NpClientSupportEditUserComponent
{
    protected static final Logger LOG = Logger.getLogger(NpClientSupportEditUserComponent.class);

    @Autowired
    private NpClientSupportAddEditUserService clientSupportAddEditUserService;

    @Autowired
    private ClientSupportAddEditUserFormValidator clientSupportAddEditUserFormValidator;

    @RequestMapping(value = "/clientSupportEditUser", method = RequestMethod.GET)
    public String getRegisterPage(HttpServletRequest request, Model model)
    {

        NpClientSupportAddEditUserForm clientSupportAddEditUserForm = new NpClientSupportAddEditUserForm();
        if (request.getParameter("id") != null)
        {
            String id = (String) request.getParameter("id");
            NpUser npUser = clientSupportAddEditUserService.getNpUsersForId(id);
            clientSupportAddEditUserForm.setFirstName(npUser.getFirstName());
            clientSupportAddEditUserForm.setLastName(npUser.getLastName());
            clientSupportAddEditUserForm.setPrimaryEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setConfirmEmailAddress(npUser.getPrimaryEmail());
            clientSupportAddEditUserForm.setMobile(npUser.getPhone());
            clientSupportAddEditUserForm.setOfficeTelePhone(npUser.getOfficeTelephone());
            clientSupportAddEditUserForm.setUserId(npUser.getId().toString());
//        	clientSupportAddEditUserForm.setCurrentRole(npUser.getNpUserRoles().getNpRoles().getRoleName());

//        	fetching NpUserRoles and set it into clientSupportAddEditUserForm
//        	NpUserRoles npUserRoles = null;
            Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
            List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);

            if (!npUserRolesSet.isEmpty())
            {
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
        }

        model.addAttribute("clientSupportAddEditUserForm", clientSupportAddEditUserForm);
        return "components/clientSupportEditUserComponent.jsp";
    }

    @RequestMapping(value = "/clientSupportEditUser", method = RequestMethod.POST)
    public String register(@ModelAttribute("clientSupportAddEditUserForm") @Validated NpClientSupportAddEditUserForm clientSupportAddEditUserForm, BindingResult result, HttpServletRequest request, Model model)
    {

        clientSupportAddEditUserFormValidator.validate(clientSupportAddEditUserForm, result);
        if (result.hasErrors())
        {

            if (StringUtils.isNotEmpty(clientSupportAddEditUserForm.getUserId()))
            {
                NpUser npUser = clientSupportAddEditUserService.getNpUsersForId(clientSupportAddEditUserForm.getUserId());
                Set<NpUserRole> npUserRolesSet = npUser.getNpUserRole();
                List<NpUserRole> listOfNpUserRoles = new ArrayList<NpUserRole>(npUserRolesSet);
                model.addAttribute("adminRoles", listOfNpUserRoles);
            }
            request.setAttribute("epm", "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue.");
            return "components/clientSupportEditUserComponent.jsp";
        }

        if (!StringUtils.isEmpty(clientSupportAddEditUserForm.getUserId()))
        {

            clientSupportAddEditUserService.saveAndUpdateNpUser(clientSupportAddEditUserForm);

            LOG.info("Updated the user");

//           request.getSession().setAttribute("successMessage", "User edited successfully");

        }
        model.addAttribute("showEditMsg", true);
        return "redirect:clientSupportUserManagement";
    }
}
