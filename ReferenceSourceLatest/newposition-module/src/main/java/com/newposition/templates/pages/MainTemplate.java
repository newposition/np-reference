package com.newposition.templates.pages;

import info.magnolia.module.blossom.annotation.Area;
import info.magnolia.module.blossom.annotation.AvailableComponentClasses;
import info.magnolia.module.blossom.annotation.Template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.newposition.admin.templates.components.CertificationManagementComponent;
import com.newposition.admin.templates.components.NpAdminIndustryManagementComponent;
import com.newposition.admin.templates.components.NpAdminInstitutionManagementComponent;
import com.newposition.admin.templates.components.NpAdminToolManagementComponent;
import com.newposition.admin.templates.components.NpMailPropertiesComponent;
import com.newposition.admin.templates.components.QualificationManagementComponent;
import com.newposition.admin.templates.components.RoleManagementComponent;
import com.newposition.admin.templates.components.SkillManagementComponent;
import com.newposition.candidate.templates.components.NpCandidateBasicDashboardComponent;
import com.newposition.candidate.templates.components.NpCandidateDashboardComponent;
import com.newposition.candidate.templates.components.NpCandidateEducationComponent;
import com.newposition.candidate.templates.components.NpCandidateEmploymentComponent;
import com.newposition.candidate.templates.components.NpCandidateFutureComponent;
import com.newposition.candidate.templates.components.NpCandidateMyaccountAddEmploymentComponent;
import com.newposition.candidate.templates.components.NpCandidateMyaccountComponent;
import com.newposition.candidate.templates.components.NpCandidateReferenceFormComponent;
import com.newposition.candidate.templates.components.NpCandidateReferencesListComponent;
import com.newposition.candidate.templates.components.NpCandidateSponsorFormComponent;
import com.newposition.candidate.templates.components.RegistrationComponent;
import com.newposition.client.templates.components.ClientOfficeLocationComponent;
import com.newposition.client.templates.components.NpAddClientComponent;
import com.newposition.client.templates.components.NpClientAddRoleGroupManagement;
import com.newposition.client.templates.components.NpClientAdminAddUserComponent;
import com.newposition.client.templates.components.NpClientAdminUserManagementComponent;
import com.newposition.client.templates.components.NpClientCompanyMarketingComponent;
import com.newposition.client.templates.components.NpClientCostCenterComponent;
import com.newposition.client.templates.components.NpClientDashboardComponent;
import com.newposition.client.templates.components.NpClientEditRoleGroupManagementComponent;
import com.newposition.client.templates.components.NpClientImpersonateUserComponent;
import com.newposition.client.templates.components.NpClientRoleGroupManagementComponent;
import com.newposition.client.templates.components.NpClientSupportImpersonateUserComponent;
import com.newposition.client.templates.components.NpClientSupportUserManagementComponent;
import com.newposition.client.templates.components.NpClientUserManagementComponent;
import com.newposition.client.templates.components.NpEditClientComponent;
import com.newposition.client.templates.components.NpProjectsComponent;
import com.newposition.common.templates.components.AccessDeniedComponent;
import com.newposition.common.templates.components.ContactUsComponent;
import com.newposition.common.templates.components.FooterComponent;
import com.newposition.common.templates.components.HeaderComponent;
import com.newposition.common.templates.components.LoginFormComponent;
import com.newposition.common.templates.components.NpProgrammeComponent;
import com.newposition.common.templates.components.PageHeadingComponent;
import com.newposition.common.templates.components.SuccessComponent;
import com.newposition.user.templates.components.ActivationCodeComponent;
import com.newposition.user.templates.components.ChangePasswordComponent;
import com.newposition.user.templates.components.NewPasswordComponent;
import com.newposition.user.templates.components.NpUserQuickRegistrationComponent;
import com.newposition.user.templates.components.ResetPasswordComponent;
import com.newposition.user.templates.components.UnlockAccountComponent;

@Controller
@Template(title = "Main", id = "newPositionModule:pages/main")
public class MainTemplate
{

    @Area("main")
    @Controller
    @AvailableComponentClasses({QualificationManagementComponent.class, CertificationManagementComponent.class,
            LoginFormComponent.class, HeaderComponent.class, FooterComponent.class,
            PageHeadingComponent.class, NpUserQuickRegistrationComponent.class, UnlockAccountComponent.class,
            ResetPasswordComponent.class, NewPasswordComponent.class, SuccessComponent.class, RegistrationComponent.class,
            NpCandidateEmploymentComponent.class, NpCandidateEducationComponent.class, SkillManagementComponent.class,
            NpAdminIndustryManagementComponent.class, NpAdminToolManagementComponent.class,
            RoleManagementComponent.class, NpAdminInstitutionManagementComponent.class, NpClientUserManagementComponent.class,
            NpAddClientComponent.class, NpClientSupportUserManagementComponent.class, NpClientSupportImpersonateUserComponent.class,
            NpEditClientComponent.class, NpClientDashboardComponent.class, NpCandidateMyaccountComponent.class,
            NpCandidateSponsorFormComponent.class, NpCandidateFutureComponent.class, NpCandidateMyaccountAddEmploymentComponent.class, ContactUsComponent.class,
            NpCandidateBasicDashboardComponent.class, NpCandidateReferencesListComponent.class, ActivationCodeComponent.class, ChangePasswordComponent.class,
            NpCandidateDashboardComponent.class, NpCandidateReferenceFormComponent.class, AccessDeniedComponent.class, NpMailPropertiesComponent.class,
            NpClientAdminUserManagementComponent.class, NpClientAdminAddUserComponent.class, NpProjectsComponent.class, NpClientCostCenterComponent.class, NpProgrammeComponent.class,
            NpClientRoleGroupManagementComponent.class, NpClientAddRoleGroupManagement.class, NpClientEditRoleGroupManagementComponent.class, NpClientImpersonateUserComponent.class,
            ClientOfficeLocationComponent.class, NpClientCompanyMarketingComponent.class})
    public static class MainArea
    {

        @RequestMapping("/mainTemplate/home")
        public String renderMainArea()
        {
            return "pages/mainArea.jsp";
        }
    }

    @RequestMapping("/mainTemplate")
    public String renderMainTemplate()
    {
        return "pages/main.jsp";
    }
}
