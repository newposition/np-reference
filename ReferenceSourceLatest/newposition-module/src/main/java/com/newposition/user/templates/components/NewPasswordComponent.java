package com.newposition.user.templates.components;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import info.magnolia.module.blossom.annotation.Template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.NewPaswordForm;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "New Password", id = "newPositionModule:components/newpassword")
public class NewPasswordComponent
{

    @Autowired
    private NpUserService npUserService;

    @RequestMapping(value = "/newpassword", method = RequestMethod.GET)
    public String render(@ModelAttribute NewPaswordForm newPasword, HttpServletRequest request)
    {
        if (request.getParameter("id") != null)
        {
            String timeParam = new String(DatatypeConverter.parseBase64Binary(request.getParameter("dur")));
            Long sentTime = Long.parseLong(timeParam);

            String id = (String) request.getParameter("id");
            String decoded = new String(DatatypeConverter.parseBase64Binary(id));
            NpUser user = npUserService.findByUserName(decoded);
            if (!npUserService.isLinkExpired(sentTime) || !(user.getNpUserStatus().getId() == 1))
            {
                try
                {
                    newPasword.setEmail(decoded);
                    npUserService.activateUserStatus(decoded);
                    request.setAttribute("newPasword", newPasword);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                request.getSession().setAttribute("bodyText", "Link is expired");
                return "redirect:sucessPage";
            }
        }
        else
        {
            request.setAttribute("newPasword", new NewPaswordForm());
        }
        return "components/newPasword.jsp";
    }

    @RequestMapping(value = "/newpassword", method = RequestMethod.POST)
    public String setnewpassword(@ModelAttribute("newPasword") @Valid NewPaswordForm newPasword, BindingResult result, HttpServletRequest request)
    {

        boolean check = false;

        if (result.hasErrors())
        {
            return "components/newPasword.jsp";
        }
        if (!(newPasword.getNewPassword().equals(newPasword.getConfirmPassword())))
        {
            request.setAttribute("errmsg", "Password Mismatches");
            return "components/newPasword.jsp";
        }
        npUserService.setnewpassword(newPasword);

        request.setAttribute("sucessmsg", "Your Password is changed.");
        return "components/newPasword.jsp";
    }
}
