package com.newposition.user.templates.components;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import info.magnolia.module.blossom.annotation.Template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.ResetForm;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;
import com.newposition.util.DataSecurity;

@Controller
@Template(title = "Unlock Form", id = "newPositionModule:components/unlockAccount")
public class UnlockAccountComponent
{

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpMailService npMailService;

    @Value("{captchamessage}")
    private String captchamessage;

    @Value("${unlockAccountLink}")
    private String unlockAccountLink;

    @Value("${email.images.context}")
    private String imageContext;

    @Resource(name = "bcryptEncoder")
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/unlockAccount", method = RequestMethod.GET)
    public String render(@ModelAttribute("resetForm") ResetForm resetForm, HttpServletRequest request)
    {

        return "components/unlockAccountComponent.jsp";
    }

    @RequestMapping(value = "/unlockAccount", method = RequestMethod.POST)
    public String reset(@ModelAttribute("resetForm") @Valid ResetForm resetForm, BindingResult result, HttpServletRequest request)
    {

        if (result.hasErrors())
        {
            return "components/unlockAccountComponent.jsp";
        }

//		Integer checkId = 0;

        if (rpHash(request.getParameter("defaultReal")).equals(request.getParameter("defaultRealHash")))
        {
            // Accepted

            npUserService.resendUnockAccountLink(resetForm.getResetMail(), unlockAccountLink, imageContext);
            request.setAttribute("sucessmsg", "An unlock link has been sent to you. Please check your registered email.");
            return "components/unlockAccountComponent.jsp";
        }
        request.setAttribute("errmsg", "Please Enter A Valid Captcha");
        return "components/unlockAccountComponent.jsp";
    }

    /**
     * Compute the hash value to check for "real person" submission.
     *
     * @param value the entered value
     * @return its hash value
     */
    private String rpHash(String value)
    {
        int hash = 5381;
        value = value.toUpperCase();
        for (int i = 0; i < value.length(); i++)
        {
            hash = ((hash << 5) + hash) + value.charAt(i);
        }
        return String.valueOf(hash);
    }
}
