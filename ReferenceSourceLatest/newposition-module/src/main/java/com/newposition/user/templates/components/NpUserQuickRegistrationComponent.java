package com.newposition.user.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.candidate.forms.validator.QuickRegistrationFormValidator;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;

@Controller
@Template(title = "QuickRegistration Form", id = "newPositionModule:components/quickRegistration")
public class NpUserQuickRegistrationComponent
{

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpMailService npMailService;

    @Value("${loginPageLink}")
    private String loginPageLink;

    @Value("${email.images.context}")
    private String imageContext;

//	 @Value("${captchamessage}")
//	 private String captchamessage;

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.setValidator(new QuickRegistrationFormValidator());
    }

    @RequestMapping(value = "/quickRegister", method = RequestMethod.GET)
    public String getQregisterPage(@ModelAttribute("quickRegistrationForm") QuickRegistrationForm quickRegistrationForm, HttpServletRequest request)
    {

        return "components/quickRegistration.jsp";
    }

    @RequestMapping(value = "/quickRegister", method = RequestMethod.POST)
    public String register(@ModelAttribute("quickRegistrationForm") @Valid QuickRegistrationForm quickRegistrationForm, BindingResult result, HttpServletRequest request)
    {

        // Validation errors
        if (result.hasErrors())
        {
            return "components/quickRegistration.jsp";
        }
        else if (!rpHash(request.getParameter("defaultReal")).equals(request.getParameter("defaultRealHash")))
        {
            request.setAttribute("cpm", "Please enter valid captcha code");
            return "components/quickRegistration.jsp";
        }
        UserDetails users = npUserService.loadUserByUsername(quickRegistrationForm.getCandidate().getPrimaryEmail());
        if (users == null)
        {
            npCandidateService.quickRegister(quickRegistrationForm.getCandidate(), quickRegistrationForm.getPassword());
        }
        else
        {
            Format format = new SimpleDateFormat("MMM dd, yyyy");
            String candidateName = quickRegistrationForm.getCandidate().getFirstName() + " " + quickRegistrationForm.getCandidate().getLastName();
            String toDay = format.format(new Date());
            String information = "NewPosition Account activation";
            Context context = new Context();
            context.setVariable("candidateName", candidateName);
            context.setVariable("loginPageLink", loginPageLink);
            context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
            context.setVariable("template", "exist-user-email.html");
            context.setVariable("date", toDay);

            npMailService.sendMail(information, quickRegistrationForm.getCandidate().getPrimaryEmail(), context);
        }
        request.getSession().setAttribute("bodyText", "Thank You for registering. please check your email for login");
        return "redirect:sucessPage";
    }

    private String rpHash(String value)
    {
        int hash = 5381;
        value = value.toUpperCase();
        for (int i = 0; i < value.length(); i++)
        {
            hash = ((hash << 5) + hash) + value.charAt(i);
        }
        return String.valueOf(hash);
    }
}