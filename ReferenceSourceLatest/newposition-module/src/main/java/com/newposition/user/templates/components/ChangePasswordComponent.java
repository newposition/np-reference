package com.newposition.user.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.ChangePaswordForm;
import com.newposition.common.forms.validator.ChangePasswordFormValidator;
import com.newposition.common.service.NpUserService;

@Controller
@Template(title = "Change Password", id = "newPositionModule:components/changePasword")
public class ChangePasswordComponent
{

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private ChangePasswordFormValidator changePasswordFormValidator;

    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String render(@ModelAttribute ChangePaswordForm changePasword, HttpServletRequest request)
    {

        request.setAttribute("changePasword", new ChangePaswordForm());
        return "components/changePasword.jsp";
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public String setnewpassword(@ModelAttribute("changePasword") ChangePaswordForm changePasword, BindingResult result, HttpServletRequest request, Authentication authentication)
    {

        changePasswordFormValidator.validate(changePasword, result);
//    	boolean check=false;
        NpUser user = npUserService.findByUserName(authentication.getName());
        if (user == null)
        {
            return "redirect:home";
        }
        changePasword.setUser(user);
        if (result.hasErrors())
        {
            return "components/changePasword.jsp";
        }
        /*if(!(changePasword.getNewPassword().equals(changePasword.getConfirmPassword())))
		{
    		 request.setAttribute("errmsg", "Password Mismatches");
			  return "components/changePasword.jsp";
		}*/
        if (!npUserService.changePassword(changePasword))
        {
            request.setAttribute("errmsg", "Old password Mismatches");
            return "components/changePasword.jsp";
        }

        request.setAttribute("sucessmsg", "Your Password is changed.");
        return "components/changePasword.jsp";
    }
}
