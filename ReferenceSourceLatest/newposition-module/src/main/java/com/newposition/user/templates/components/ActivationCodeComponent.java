/**
 *
 */
package com.newposition.user.templates.components;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import info.magnolia.module.blossom.annotation.Template;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpReset;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;
import com.newposition.util.DataSecurity;

/*
 * @author ravi
 *
 *
 */
@Controller
@Template(title = "Activation Form", id = "newPositionModule:components/activation")
public class ActivationCodeComponent
{

    protected static final Logger LOG = Logger.getLogger(ActivationCodeComponent.class);

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpUserService npUserService;

    @Value("${resetPwdActivationMessage}")
    private String resetPwdActivationMessage;

    @Value("${resetPasswordLink}")
    private String resetPasswordLink;

    @Value("${activationPageLink}")
    private String activationPageLink;

    @Value("${email.images.context}")
    private String imageContext;

    @RequestMapping(value = "/activation", method = RequestMethod.GET)
    public String render(HttpServletRequest request, Model model)
    {

        String page = request.getParameter("page");
        if (StringUtils.isNotEmpty(page))
        {

            model.addAttribute("fromPage", page);
            model.addAttribute("userId", request.getParameter("id").trim());
        }
        return "components/activationForm.jsp";
    }

    @RequestMapping(value = "/activation", method = RequestMethod.POST)
    public String activationPost(HttpServletRequest request, Model model)
    {

		/* request comes for new account activation */
        String page = request.getParameter("page");
        if (StringUtils.isNotEmpty(page) && request.getParameter("resend") == null)
        {

            if (request.getParameter("activationCode").trim().equals(""))
            {
                request.setAttribute("errmsg",
                        "Activation Code Should Not Be Empty");
                model.addAttribute("fromPage", page);
                model.addAttribute("userId", request.getParameter("userId")
                        .trim());
                return "components/activationForm.jsp";
            }
            String userId = request.getParameter("userId").trim();

            userId = new String(DatatypeConverter.parseBase64Binary(userId));

            LOG.info("userId:  " + request.getParameter("userId")
                    + " -" + userId + "- " + page);

            NpReset resetCode = npUserService.verifyActivationCode(request
                    .getParameter("activationCode").trim());
            if (resetCode != null)
            {

                npUserService.unlockAccount(Integer.parseInt(userId));
                request.getSession().setAttribute("successmsg", "Your Account has been Activated. Please login with your valid credentials");

                return "redirect:/home";
            }
            else
            {
                request.setAttribute("errmsg",
                        "Please Enter A Valid Activation Code");
                model.addAttribute("fromPage", page);
                model.addAttribute("userId", request.getParameter("userId")
                        .trim());
                return "components/activationForm.jsp";
            }
        }

        if (request.getParameter("resend") != null)
        {
            String email = request.getParameter("email");
            //if email field is blank
            if (StringUtil.isBlank(email))
            {
                request.setAttribute("errmsg", "Email Address can not be blank");
                return "components/activationForm.jsp";
            }

            NpReset npReset = npUserService.getActivationCode(email);
            if (npReset != null)
            {
                Calendar currentTime = Calendar.getInstance();
                Long differenceInMilli = currentTime.getTimeInMillis()
                        - npReset.getTime().getTime();
                Long differenceInHour = (differenceInMilli) / 1000L / 60L / 60L;

				/* if reset code has been sent for more than 48 hours
                 *    send new activation code
				 *    
				 *  else 
				 *    send exist activation code
				 *  
				 */

                if (differenceInHour < 48)
                {

                    String activationCode = "";
                    try
                    {
                        activationCode = DataSecurity.decrypt(npReset.getActivation());
                    }
                    catch (Exception e)
                    {
                        // TODO Auto-generated catch block
                        LOG.error("error while decrypt");
                    }
                    // sending mail
                    Format format = new SimpleDateFormat("MMM dd, yyyy");
                    String toDay = format.format(new Date());
                    Context context = new Context();

                    context.setVariable("activationCode", activationCode);
                    context.setVariable("headerImageSrc", imageContext
                            + "./resources/templating-kit/img/emaillogo_header.jpg");
                    context.setVariable("template", "resendActivationCodeEmail.html");
                    context.setVariable("date", toDay);
                    if (StringUtils.isNotEmpty(page))
                    {
                        context.setVariable("activationPageLink", activationPageLink + "?id=" + request.getParameter("userId") + "&page=" + page);
                        model.addAttribute("fromPage", page);
                        model.addAttribute("userId", request.getParameter("userId").trim());
                    }
                    else
                    {
                        context.setVariable("activationPageLink", activationPageLink);
                    }
                    npMailService.sendMail("Activate Account", email, context);
                    // sendEmail(npReset.getEmail(), npReset.getActivation());

                }
                else
                {

                    npUserService.resetPassword(email);
                }
            }
            request.setAttribute("infomsg",
                    "Activation Code has been sent to your Email");
            return "components/activationForm.jsp";
        }
        else
        {

            if (request.getParameter("activationCode").trim().equals(""))
            {
                request.setAttribute("errmsg",
                        "Activation Code Should Not Be Empty");
                return "components/activationForm.jsp";
            }
            NpReset resetCode = npUserService.verifyActivationCode(request
                    .getParameter("activationCode").trim());
            if (resetCode != null)
            {
                Calendar currentTime = Calendar.getInstance();
                Long differenceInMilli = currentTime.getTimeInMillis()
                        - resetCode.getTime().getTime();
                Long differenceInHour = (differenceInMilli) / 1000L / 60L / 60L;

                if (differenceInHour > 48)
                {
                    request.getSession()
                            .setAttribute("errmsg",
                                    "Code has expired, please click resend button to get an activation code");
                    return "redirect:activationPage";
                }
                request.getSession()
                        .setAttribute("email", resetCode.getEmail());
                request.getSession().setAttribute("errmsg", null);
                return "redirect:setnewpassword";
            }
            else
            {
                request.setAttribute("errmsg",
                        "Please Enter A Valid Activation Code");
                return "components/activationForm.jsp";
            }
        }
    }
}
