package com.newposition.user.templates.components;

import info.magnolia.module.blossom.annotation.Template;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpResetImpl;
import com.newposition.common.forms.LoginForm;
import com.newposition.common.forms.ResetForm;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;

@Controller
@Template(title = "Reset Form", id = "newPositionModule:components/resetPassword")
public class ResetPasswordComponent
{

    @Autowired
    private NpMailService npMailService;

    @Autowired
    private NpUserService npUserService;

    @Value("${resetPwdActivationMessage}")
    private String resetPwdActivationMessage;

    @Value("${email.images.context}")
    private String imageContext;

    @Value("${activationPageLink}")
    private String activationPageLink;

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String render(@ModelAttribute("resetForm") ResetForm resetForm, HttpServletRequest request)
    {

        return "components/resetForm.jsp";
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public String reset(
            @ModelAttribute("resetForm") @Valid ResetForm resetForm,
            BindingResult result, Model model)
    {

        if (result.hasErrors())
        {
            return "components/resetForm.jsp";
        }

//		if (rpHash(request.getParameter("defaultReal")).equals(
//				request.getParameter("defaultRealHash"))) {

        Random rand = new Random();
        Integer number = rand.nextInt(1000000);
        System.out.println(number);
        if (npUserService.findByUserName(resetForm.getResetMail()) != null)
        {
            Format format = new SimpleDateFormat("MMM dd, yyyy");
            String toDay = format.format(new Date());

            NpReset resetCode = new NpResetImpl();
            resetCode.setEmail(resetForm.getResetMail());
            resetCode.setActivation(number.toString());

            npUserService.setActivationCode(resetCode);
            String information = "NewPosition Account activation";
            Context context = new Context();
            context.setVariable("resetPasswordLink", activationPageLink);
            context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
            context.setVariable("activationCode", number);
            context.setVariable("template", "activationcode-email.html");
            context.setVariable("date", toDay);

            npMailService.sendMail(information, resetForm.getResetMail(), context);

//			}
            model.addAttribute("sucessmsg", "Your Activation Code is Sent to your mail");

//		} else {
//			request.setAttribute("errmsg", "Please Enter A Valid Captcha");
        }

        return "components/resetForm.jsp";
    }
}
