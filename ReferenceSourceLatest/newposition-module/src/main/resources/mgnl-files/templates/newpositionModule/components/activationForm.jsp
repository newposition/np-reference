<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>

<center><c:if test="${not empty errmsg}">
    <b class="errorMessage">
            ${errmsg}</b>
</c:if>
    <c:if test="${not empty infomsg}">
        <b class="sucessMessage">
                ${infomsg}</b>
    </c:if>
</center>
<div class="container">
    <ul class="login_box">
        <li>
            <form id="login" action="?" method="POST">
                <blossom:pecid-input/>
                <div class="form-group">
                    <input type='hidden' name="page" value="${fromPage}">
                    <input type='hidden' name="userId" value="${userId}">
                    <input type="text" name="activationCode" placeholder="ActivationCode" class="form-control"/>
                    <span id="errAct"></span>
                </div>
                <div class="form-group">
                    <button type="submit" class="lgn_btn">Verify Code</button>
                </div>
                <div class="form-group">
                    <input type="button" id="resend" class="lgn_btn" value="Resend Verification Code">
                </div>
            </form>
            <form id="resendCode" action="?" method="POST" hidden="true">
                <blossom:pecid-input/>
                <input type='hidden' name="page" value="${fromPage}">
                <input type='hidden' name="userId" value="${userId}">
                <div class="form-group">
                    <input id="email_1" type="text" name="email" placeholder="Your Email address" class="form-control"/>
                    <span id="err_email_1" class="errorMessage"></span>
                </div>
                <input type="hidden" name="resend" value="true"/>
                <div class="form-group">
                    <button id="resend_1" type="submit" class="lgn_btn" value="">Resend Verification Code</button>
                </div>
            </form>
        </li>
    </ul>
</div>

<script type="text/javascript">
    $("#login").submit(function (event) {
        var activationCode = $(this).find('input[name=activationCode]').val();
        if (activationCode == "") {
            $("#errAct").text("Please enter a activation code").attr("class", "errorMessage");
            event.preventDefault();
        }
    });
    $("#resend").click(function () {
        $("#resendCode").show();
        $("#login").hide();

    });

    $("#resendCode").submit(function (event) {
        var email = $("#email_1").val();
        if (email == "") {
            $("#err_email_1").text("Email Address can not be blank").show();
            event.preventDefault();
        }
        else if (!/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(email)) {
            $("#err_email_1").text("Please enter a valid Email").show();
            event.preventDefault();
        }
    });
</script>
