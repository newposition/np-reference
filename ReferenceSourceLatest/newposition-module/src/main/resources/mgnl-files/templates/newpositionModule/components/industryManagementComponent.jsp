<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="np" uri="newpositionTags" %>

<script src="./resources/templating-kit/js/industryManagement.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select id="sectorFilter" class="form-control selectbox ">
                                        <np:listSectorOptions defaultSelect="${sectorFilter}"
                                                              defaultValue="Select Parent Industry"/>
                                        <option value="0" <c:if test="${sectorFilter eq '0'}">selected</c:if>>Show All
                                        </option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <%-- <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select  id="industryTypeFilter" class="form-control selectbox ">
                                       <option value="">Select IndustryType</option>
                                       <c:forEach items="${npIndustryTypes}" var="npIndustryType">
                                        <option value="${npIndustryType}" <c:if test="${industryTypeFilter eq npIndustryType}">selected</c:if>>${npIndustryType}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div> --%>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select id="validFilter" class="form-control selectbox ">
                                        <option value="" style="display:none;">Select Valid</option>
                                        <option value="Valid" <c:if test="${validFilter eq 'Valid'}">selected</c:if>>
                                            Valid
                                        </option>
                                        <option value="Invalid"
                                                <c:if test="${validFilter eq 'Invalid'}">selected</c:if>>Invalid
                                        </option>
                                        <option value="0" <c:if test="${validFilter eq '0'}">selected</c:if>>Show All
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="sort col-md-10">
                                <span class="result-pop"><c:if
                                        test='${param["showMergeMsg"]}'>Merged Industry Successfully<br></c:if>
                            
                                <c:if test='${param["showAddMsg"]}'>Added Industry Successfully<br></c:if>
                            
                                <c:if test='${param["showEditMsg"]}'>Edited Industry Successfully<br></c:if>
                            
                               <c:if test='${param["showDelMsg"]}'>Deleted Industry Successfully<br></c:if></span>
                                </div>
                                <%--  <c:if test="${showMergeMsg eq true }">
                                 <label><i class="fa fa-search control-label"> Industry merged Successfully</i></label>
                                 </c:if> --%>
                                <div class="col-xs-4 col-md-2 pull-right">
                                    <a href="#" class="btn btn-rd-cor" data-toggle="modal"
                                       data-target="#myModalAdd">Add</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Asp" id="qSearch" class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="sortNpIndustries">
                            <option value="">Sort by</option>
                            <option value="Name" <c:if test="${sortBy eq 'Name'}">selected</c:if>>Industry Name</option>
                            <%--                               <option value="IndustryType" <c:if test="${sortBy eq 'IndustryType'}">selected</c:if>>Industry Type</option> --%>
                            <%--                               <option value="SectorID" <c:if test="${sortBy eq 'SectorID'}">selected</c:if>>Sector Type</option> --%>
                            <option value="Valid" <c:if test="${sortBy eq 'Valid'}">selected</c:if>>Valid</option>
                        </select>
                    </div>
                    <div class="sort pull-left">
                            <span class="result-pop">
                            <c:if test="${npEmployersCount lt 2}">
                                ${npEmployersCount} Result found
                            </c:if>
                            <c:if test="${npEmployersCount gt 1}">
                                ${npEmployersCount} Results found
                            </c:if>
                            </span>
                    </div>
                </div>
            </div>

            <div class="imp-user-list">
                <div class="row no-pad">
                    <div class="tbl_panel">
                        <div class="row no-pad">
                            <div class="col-xs-4 col-md-3">
                                <div class="tbl-user">
                                    <strong>Industry Name</strong>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="tbl-name">
                                    <strong>Parent Industry</strong>
                                </div>
                            </div>
                            <div class="col-xs-2 col-md-1">
                                <!-- <div class="tbl-name">
                                    <strong>Type</strong>
                                </div> -->
                            </div>
                            <div class="col-xs-2 col-md-2">
                                <div class="tbl-name">
                                    <strong>Valid</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <c:forEach items="${npEmployers}" var="npEmployer" varStatus="index">
                    <div class="row no-pad">
                        <div class="tbl_panel">
                            <div class="row no-pad">
                                <div class="col-xs-4 col-md-3">
                                    <div class="tbl-user">
                                        <input type="hidden" id="name${index.index}" value=" ${npEmployer.name}"/>
                                            ${npEmployer.name}
                                    </div>
                                </div>
                                <div class="col-xs-4 col-md-4">
                                    <div class="tbl-name">
                                        <input type="hidden" id="sector${index.index}"
                                               value="${npEmployer.npSector.id}"/>
                                            ${npEmployer.npSector.sectorName}
                                    </div>
                                </div>
                                <div class="col-xs-2 col-md-1">
                                        <%--  <div class="tbl-name">
                                             ${npEmployer.industryType}
                                         </div> --%>
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    <div class="tbl-name">
                                        <input type="hidden" id="valid${index.index}" value="${npEmployer.valid}"/>
                                            ${npEmployer.valid}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-2">
                                    <div class="tbl-mo-button">
                                        <a class=" btn edit-icon" id="${index.index}" data-toggle="modal"
                                           data-target="#myModal${index.index}"><img
                                                src="./resources/templating-kit/img/edit-icon.png"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>

            <c:if test="${npEmploymentRolesTotalPages ge 1}">
                <div class="paging">
                    <nav role="navigation">
                        <ul class="cd-pagination">
                            <c:if test="${npEmploymentRolesTotalPages ge 1 && currentPageNumber > 0}">
                                <li><a class="fristPagination" href="?pageNumber=0"><i
                                        class="fa fa-angle-double-left"></i></a></li>
                                <li><a class="prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
                            </c:if>
                            <c:forEach begin="0" end="${npEmploymentRolesTotalPages-1}" varStatus="index">
                                <li>
                                    <c:choose>
                                        <c:when test="${currentPageNumber eq index.index}">
                                            <a href="?pageNumber=${index.index}"
                                               class="current currentPageNo">${index.index + 1}</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="?pageNumber=${index.index}"
                                               class="pagination">${index.index + 1}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </li>
                            </c:forEach>
                            <c:if test="${npEmploymentRolesTotalPages ge 1 && currentPageNumber < npEmploymentRolesTotalPages-1}">
                                <li><a class="next" href="#0"><i class="fa fa-angle-right"></i></a></li>
                                <li><a class="lastPagination" href="${npEmploymentRolesTotalPages-1}"><i
                                        class="fa fa-angle-double-right"></i></a></li>
                            </c:if>
                            <!--                             <li><a class="disabled" href="#0"><i class="fa fa-angle-double-left"></i></a></li> -->
                            <!--                             <li><a class="disabled" href="#0"><i class="fa fa-angle-left"></i></a></li> -->
                            <!--                             <li><a class="current" href="#0">1</a></li> -->
                            <!--                             <li><a href="#0">2</a></li> -->
                            <!--                             <li><a href="#0">3</a></li> -->
                            <!--                             <li><a href="#0">4</a></li> -->
                            <!--                             <li><a href="#0">5</a></li> -->
                            <!--                             <li><a href="#0">6</a></li> -->
                            <!--                             <li><a href="#0">7</a></li> -->
                            <!--                             <li><a href="#0">8</a></li> -->
                            <!--                             <li><a href="#0">9</a></li> -->
                            <!--                             <li><span>...</span></li> -->
                            <!--                             <li><a href="#0"><i class="fa fa-angle-right"></i></a></li> -->
                            <!--                             <li><a href="#0"><i class="fa fa-angle-double-right"></i></a></li> -->
                        </ul>
                    </nav>
                </div>
            </c:if>
        </div>

    </div>
</div>


<!--Bootsrap Modal
================================================== -->
<c:forEach items="${npEmployers}" var="npEmployer" varStatus="index">
    <div class="modal fade" id="myModal${index.index}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-center" id="myModalLabel">Industries Edit</h3>
                </div>
                <div class="modal-body">
                    <div class="row" style="text-align:center;">
                        <span id="errPop" class="errorMessage"></span>
                    </div>
                    <div class="container-fluid">

                        <div class="row">
                            <form:form id="addNpIndustriesForm" action="?" modelAttribute="addNpIndustriesForm"
                                       method="POST">
                                <form:input type="hidden" id="industryIds" path="industryId" value="${npEmployer.id}"/>
                                <div class="col-md-5 col-xs-12">
                                    <div class="bd-blue-panel">
                                        <div class="skl_btx">
                                            <fieldset>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form:select path="sectorId" class="form-control selectbox "
                                                                         id="Select19">
                                                                <np:listSectorOptions
                                                                        defaultSelect="${npEmployer.npSector.id}"
                                                                        defaultValue="Select Parent Industry"/>
                                                            </form:select>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <%-- <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                              <form:select path="industryType" class="form-control selectbox " id="Select20">
                                                               <option value="">Select Alias Name</option>
                                                                 <c:forEach items="${npIndustryTypes}" var="npIndustryType">
                                                                 <c:choose>
                                                                 <c:when test="${npEmployer.industryType eq npIndustryType}">
                                                                    <form:option value="${npIndustryType}" selected="selected">${npIndustryType}</form:option>
                                                                 </c:when>
                                                                 <c:otherwise>
                                                                    <form:option value="${npIndustryType}">${npIndustryType}</form:option>
                                                                 </c:otherwise>
                                                                 </c:choose>
                                                                  </c:forEach>

                                                             </form:select>
                                                            </div>
                                                        </div>
                                                    </div> --%>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form:input type="text" path="industryName"
                                                                        placeholder="Tool Name" class="pp_text"
                                                                        value="${npEmployer.name}"/>
                                                            <!--                                                             <select name="" id="Select21" class="form-control selectbox "> -->
                                                            <!--                                                                 <option value="1">Skill Name</option> -->
                                                            <!--                                                             </select> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="radio-inline f-label" for="radios-0">
                                                                <input type="radio" name="valid" id="radio1"
                                                                       value="Valid"
                                                                       <c:if test="${npEmployer.valid eq 'Valid'}">checked="checked"</c:if>>
                                                                Valid
                                                            </label>
                                                            <label class="radio-inline f-label" for="radios-1">
                                                                <input type="radio" name="valid" id="radio2"
                                                                       value="Invalid"
                                                                       <c:if test="${npEmployer.valid eq 'Invalid'}">checked="checked"</c:if>>
                                                                Invalid
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="f-label">
                                                                No. of Active Candidate
                                                                : ${fn:length(npEmployer.npCandidateEmployers)}
                                                            </label>
                                                            <label class="f-label">
                                                                No. of Active Positions : 20
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!--                                             <input type="submit" value="Add SKill" id="addSkill" class="pp_button"> -->
                                                <%--                                         </form:form> --%>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="/industryManagement/deleteAlias" id="del-url">
                                <input type="hidden" value="/industryManagement/editAlias" id="edit-url">

                                <div class="col-md-7 col-xs-12 coAliasDiv">
                                    <div class="bd-blue-panel-scroll">
                                        <div class="skl_btx">
                                            <!--                                           <input type="submit" value="Add SKill" id="addSkill" class="pp_button"> -->
                                            <!--                                         <button class="remove" type="submit"><span class="fa fa-plus"></span></button> -->
                                            <fieldset class="coAliasSection">
                                                <c:choose>
                                                    <c:when test="${empty npEmployer.coEmployerAlias}">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-4 col-xs-6">
                                                                    <input type="hidden"
                                                                           name="certificationAliases[0].aliasId"
                                                                           value=""
                                                                           class="aliasId${index.index} coAliasId"/>

                                                                    <!--                                                                <input type="text" class="pp_text" id="Text20" placeholder="Alias Skill Name"> -->
                                                                    <form:input type="text"
                                                                                path="certificationAliases[0].aliasName"
                                                                                id="Text20" placeholder="Alias Name"
                                                                                class="pp_text"/>

                                                                </div>
                                                                <div class="col-md-5">
                                                                    <form:select
                                                                            path="certificationAliases[0].aliasSource"
                                                                            class="form-control selectbox "
                                                                            id="Select22">
                                                                        <option value="" style="display:none;">Select
                                                                            Alias Source
                                                                        </option>
                                                                        <c:forEach items="${coSources}" var="coSource">
                                                                            <form:option
                                                                                    value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                                        </c:forEach>
                                                                    </form:select>
                                                                    <!--                                                             <select name="" id="Select22" class="form-control selectbox "> -->
                                                                    <!--                                                                 <option value="1">Alias Name Source</option> -->
                                                                    <!--                                                                <option value="1">Alias Name Source</option> -->
                                                                    <!--                                                             </select> -->
                                                                </div>
                                                                <div class="col-md-3 col-xs-6">
                                                                    <button id="${index.index}" name="btn-send"
                                                                            class="btn btn-edt pull-right deleteAliasRow">
                                                                        <span class="fa fa-trash"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:forEach items="${npEmployer.coEmployerAlias}"
                                                                   var="coEmployerAlias" varStatus="index">
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <input type="hidden"
                                                                               name="certificationAliases[${index.index}].aliasId"
                                                                               value="${coEmployerAlias.employerAliasID}"
                                                                               class="aliasId${index.index} coAliasId"/>
                                                                        <form:input type="text"
                                                                                    path="certificationAliases[${index.index}].aliasName"
                                                                                    id="Text20"
                                                                                    value="${coEmployerAlias.employerAliasName}"
                                                                                    placeholder="Alias Name"
                                                                                    class="aliasName${index.index} pp_text"
                                                                                    readonly="true"/>

                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <form:select
                                                                                path="certificationAliases[${index.index}].aliasSource"
                                                                                class="aliasSource${index.index} form-control selectbox "
                                                                                id="Select22" disabled="true">
                                                                            <option value="" style="display:none;">
                                                                                Select Alias Name
                                                                            </option>
                                                                            <c:forEach items="${coSources}"
                                                                                       var="coSource">
                                                                                <c:choose>
                                                                                    <c:when test="${coEmployerAlias.coSource.sourceID eq coSource.sourceID}">
                                                                                        <form:option
                                                                                                value="${coSource.sourceID}"
                                                                                                selected="selected">${coSource.sourceName}</form:option>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <form:option
                                                                                                value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:forEach>
                                                                        </form:select>
                                                                    </div>
                                                                    <div class="col-md-3 col-xs-6">
                                                                        <button id="Button1" name="btn-send"
                                                                                class="btn btn-edt editAliasRow"
                                                                                title="Click to edit alias for this Industry">
                                                                            <span class="fa fa-pencil-square-o"></span>
                                                                        </button>
                                                                        <button id="${index.index}" name="btn-send"
                                                                                class="btn btn-edt pull-right deleteAliasRow"
                                                                                title="Click to delete alias for this Industry">
                                                                            <span class="fa fa-trash"></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </c:forEach>
                                                    </c:otherwise>
                                                </c:choose>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <button id="industryFormDeleteBtn" class="btn btn-edt deleteAlias"
                                                title="Click to delete this Industry" type="button"><span
                                                class="fa fa-trash"></span></button>
                                        <button class="btn btn-edt" title="Click to save details for this Industry"
                                                type="submit"><span class="fa fa-save"></span></button>
                                        <button class="btn btn-edt editAlias"
                                                title="Click to add a new alias for this Industry"><span
                                                class="fa fa-plus"></span></button>
                                    </div>
                                </div>

                            </form:form>
                            <form:form id="addNpIndustriesFormDelete" action="?" modelAttribute="addNpIndustriesForm"
                                       method="POST">
                                <form:input type="hidden" path="industryId" value="${npEmployer.id}"/>
                                <form:input type="hidden" path="deleteIndustry" value="true"/>
                                <!-- <button class="merge" type="submit"> -->
                                <!-- </button> -->

                            </form:form>
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-md-12 quickEntryFilter">
                                <input type="text" placeholder="Search Alias Name" id="qSearch"
                                       class="form-control input-md">
                                <label for=""><i class="fa fa-search control-label"></i></label>
                            </div>
                            <div class="searchResults1">
                            </div>
                        </div>


                        <div class="row">
                            <!-- <div class="paging">
                                <nav role="navigation">
                                    <ul class="cd-pagination">
                                        <li><a class="disabled" href="#0"><i class="fa fa-angle-double-left"></i></a></li>
                                        <li><a class="disabled" href="#0"><i class="fa fa-angle-left"></i></a></li>
                                        <li><a class="current" href="#0">1</a></li>
                                        <li><a href="#0">2</a></li>
                                        <li><a href="#0">3</a></li>
                                        <li><a href="#0">4</a></li>
                                        <li><a href="#0">5</a></li>
                                        <li><a href="#0">6</a></li>
                                        <li><a href="#0">7</a></li>
                                        <li><a href="#0">8</a></li>
                                        <li><a href="#0">9</a></li>
                                        <li><span>...</span></li>
                                        <li><a href="#0"><i class="fa fa-angle-right"></i></a></li>
                                        <li><a href="#0"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </nav>
                            </div> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</c:forEach>


<!-- Bootstrap Add myModal -->

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-center" id="H1">Add Industry</h3>
            </div>
            <div class="modal-body">
                <div class="row" style="text-align:center;">
                    <span id="errPop" class="errorMessage"></span>
                </div>
                <div class="container-fluid">

                    <div class="row">
                        <form:form id="addNpIndustriesForm" action="?" modelAttribute="addNpIndustriesForm"
                                   method="POST">
                            <form:input type="hidden" path="industryId" value=""/>
                            <div class="col-md-5 col-xs-12">
                                <div class="bd-blue-panel">
                                    <div class="skl_btx">
                                        <fieldset>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form:select path="sectorId" class="form-control selectbox "
                                                                     id="Select19">
                                                            <np:listSectorOptions defaultSelect="0"
                                                                                  defaultValue="Select Parent Industry"/>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>
                                                <%-- <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                          <form:select path="industryType" class="form-control selectbox " id="Select20">
                                                           <option value="">Select IndustryType</option>
                              						       <c:forEach items="${npIndustryTypes}" var="npIndustryType">
                               						         <form:option value="${npIndustryType}">${npIndustryType}</form:option>
                               						       </c:forEach>
														 </form:select>
<!--                                                             <select name="" id="Select20" class="form-control selectbox "> -->
<!-- 															<option value="">Select SkillType</option> -->
                                          					 <c:forEach items="${npSkillTypes}" var="npSkillType">
                                          						  <option value="${npSkillType}" <c:if test="${skillTypeFilter eq npSkillType}">selected</c:if>>${npSkillType}</option>
                                          					 </c:forEach>                                                            
<!--                                           					 </select> -->
                                                        </div>
                                                    </div>
                                                </div> --%>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form:input type="text" path="industryName"
                                                                    placeholder="Indsutry Name" class="pp_text"/>
                                                        <!--                                                             <select name="" id="Select21" class="form-control selectbox "> -->
                                                        <!--                                                                 <option value="1">Skill Name</option> -->
                                                        <!--                                                             </select> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="radio-inline f-label" for="radios-0">
                                                            <input type="radio" name="valid" id="radio1" value="Valid"
                                                                   checked="checked">
                                                            Valid
                                                        </label>
                                                        <label class="radio-inline f-label" for="radios-1">
                                                            <input type="radio" name="valid" id="radio2"
                                                                   value="Invalid">
                                                            Invalid
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!--                                             <input type="submit" value="Add SKill" id="addSkill" class="pp_button"> -->
                                            <%--                                         </form:form> --%>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-7 col-xs-12 coAliasDiv">
                                <div class="bd-blue-panel-scroll">
                                    <div class="skl_btx">
                                        <!--                                           <input type="submit" value="Add SKill" id="addSkill" class="pp_button"> -->
                                        <!--                                         <button class="remove" type="submit"><span class="fa fa-plus"></span></button> -->
                                            <%--                                         <form action=""> --%>
                                        <fieldset class="coAliasSection">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-6">
                                                        <input type="hidden" name="aliasId" value=""/>

                                                        <!--                                                                <input type="text" class="pp_text" id="Text20" placeholder="Alias Skill Name"> -->
                                                        <form:input type="text" path="certificationAliases[0].aliasName"
                                                                    id="Text20" placeholder="Alias Name"
                                                                    class="pp_text"/>

                                                    </div>
                                                    <div class="col-md-5">
                                                        <form:select path="certificationAliases[0].aliasSource"
                                                                     class="form-control selectbox " id="Select22">
                                                            <option value="" style="display:none;">Alias Source</option>
                                                            <c:forEach items="${coSources}" var="coSource">
                                                                <form:option
                                                                        value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                        <!--                                                             <select name="" id="Select22" class="form-control selectbox "> -->
                                                        <!--                                                                 <option value="1">Alias Name Source</option> -->
                                                        <!--                                                                <option value="1">Alias Name Source</option> -->
                                                        <!--                                                             </select> -->
                                                    </div>
                                                    <div class="col-md-3 col-xs-6">
                                                        <!--                                                             <button id="Button37" name="btn-send" class="btn btn-edt"><span class="fa fa-pencil-square-o"></span></button> -->
                                                        <button id="${index.index}" name="btn-send"
                                                                class="btn btn-edt pull-right deleteAliasRow"
                                                                title="Click to delete alias for this Industry"><span
                                                                class="fa fa-trash"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-edt" title="Click to save details for this Industry"
                                            type="submit"><span class="fa fa-save"></span></button>
                                    <button class="btn btn-edt addAlias"
                                            title="Click to add a new alias for this Industry"><span
                                            class="fa fa-plus"></span></button>
                                </div>
                            </div>

                        </form:form>
                        <input type="hidden" id="type" value="industry"/>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function (e) {
        $(".re_check label").click(function () {
            $(this).toggleClass("re_checked");
        });


    });
</script>