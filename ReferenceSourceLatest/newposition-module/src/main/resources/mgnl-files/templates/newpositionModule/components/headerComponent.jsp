<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="userInfo" value="${user}"/>
<c:set var="isLoggedIn" value="${user eq null}"/>
<c:set var="isSwitchUser" value="${swtchUser}"></c:set>
<c:if test="${not empty isSwitchUser}">
    <c:set var="isLoggedIn" value="${true}"/>
</c:if>
<div class="headerComponent">
    <div class="top_bar">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home"> <img src="./resources/templating-kit/img/logo.png" width="158"
                                                              height="86"> </a></div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#about">How It Works</a></li>
                        <li><a href="contactUs">Contact</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right top_icons">

                        <li style="position:relative;">
                            <c:if test="${userInfo eq null}">
                                <a href="#" class="login_icon">Login <img
                                        src="./resources/templating-kit/img/user-ico.png" alt="user"
                                        style="max-width:50px;"></a> <a href="#" class="search_icon">Search <img
                                    src="./resources/templating-kit/img/search-ico.png" alt="search"
                                    style="max-width:30px;">
                            </a>
                            </c:if>
                            <c:if test="${userInfo ne null}">
                                <a href="#" class="login_icon"
                                   <c:if test="${isImpersonate}">style="color:RED;" </c:if>>${userInfo.firstName} <img
                                        src="./resources/templating-kit/img/user_ico.png" alt="user"
                                        style="max-width:50px;"></a> <a href="#" class="search_icon">Search <img
                                    src="./resources/templating-kit/img/search-ico.png" alt="search"
                                    style="max-width:30px;">
                            </a>
                            </c:if>
                            <ul id="login_drop" class="login_drop">
                                <c:if test="${user eq null}">
                                    <li class="nohover">
                                        <form id="login" action="j_spring_security_check" method="POST">
                                            <div class="form-group">
                                                <input name="j_username" id="j_username" placeholder="Username"
                                                       class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="j_password" id="j_password"
                                                       placeholder="Password" class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="lgn_btn">Login</button>
                                            </div>
                                            <div class="form-group form_group_gap_5">
                                                <a class="lgn_hrf text-center" href="registration">Register</a>
                                            </div>
                                            <div class="form-group form_group_gap_5">
                                                <a class="lgn_hrf text-center" href="forgottenPassword">Forgotten
                                                    Password</a>
                                            </div>
                                        </form>

                                    </li>
                                </c:if>
                                <c:if test="${user ne null}">
                                    <li>
                                        <div class="">
                                            <a href="#" class="lgn_hrf text-center">${user.primaryEmail}</a>

                                        </div>
                                    </li>
                                    <c:if test="${not isImpersonate}">
                                        <li>
                                            <div class="">
                                                <a href="myaccount" class="lgn_hrf text-center">Preference</a>

                                            </div>
                                        </li>
                                        <li>
                                            <div class="">
                                                <a href="changePassword" class="lgn_hrf text-center">Change password</a>
                                            </div>
                                        </li>
                                    </c:if>
                                </c:if>

                                <c:if test="${not isLoggedIn}">
                                    <li>
                                        <div class="">
                                            <a href="j_spring_security_logout" class="lgn_hrf text-center">Logout</a>
                                        </div>
                                    </li>
                                </c:if>
                                <c:if test="${not empty isSwitchUser}">
                                    <li>
                                        <div class="">
                                            <a href="j_spring_security_exit_user" class="lgn_hrf text-center">switch
                                                back</a>
                                        </div>
                                    </li>
                                </c:if>

                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.navbar-collapse -->
            </div>
        </nav>
        <div class="search_bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-sm-9">
                        <div class="search_field"><i class="fa fa-search"></i>
                            <input type="text" placeholder="Enter keywords, e.g ASP developer" id=""
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <button class="btn btn-default button" type="submit">Search</button>
                    </div>
                </div>
                <div class="advance_search text-right"><a href="#">Advanced Search</a></div>
            </div>
        </div>
    </div>
</div>