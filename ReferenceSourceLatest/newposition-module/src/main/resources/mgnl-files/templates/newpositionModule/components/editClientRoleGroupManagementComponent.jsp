<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="./resources/templating-kit/js/addRoleGroupPage.js"></script>

<div class="tlt-heading"><h2>Edit RoleGroup</h2></div>
<hr>

<div class="container addedit">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="npClientAddRoleGroupForm" action="?" commandName="npClientAddRoleGroupForm" method="POST">
                <form:input id="groupId" type="text" path="groupId" style="display:none"/>
                <div class="">
                    <b class="errorMessage">${epm}</b>
                </div>
                <fieldset>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cjtitle" class="control-label">Group Name</label>
                                <form:input id="groupname" type="text" path="groupName" placeholder="Group Name"
                                            class="form-control input-md" maxlength="128"/>
                                <form:errors path="groupName" class="errorMessage"/>
                                <span class="errorMessage">${errEmail}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cemployer" class="control-label">GroupDescription</label>
                                <form:input id="groupdescription" type="text" path="groupDescription"
                                            placeholder="Group Description" class="form-control input-md"/>
                                <form:errors path="groupDescription" class="errorMessage"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="blue-panel bp-min-height">
                            <div class="blue-panel-hpe">
                                <h3 class="blue-panel-title">Roles / Groups</h3>
                            </div>
                            <div class="blue-panel-body bp-hpe">
                                <div class="row no-pad">
                                    <div class="links_jss links_jsonlyclick links_bd">
                                        <c:if test="${not empty clientRoles}">
                                            <c:forEach items="${clientRoles}" var="clientRole">
                                                <a href="javascript:void(0)">${clientRole.npRole.roleName}<i
                                                        class="fa fa-times"></i></a>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="row no-pad">
                                    <div class="col-md-12">
                                        <input type="text" id="currentRole" class="form-control input-md"
                                               placeholder="Enter Roles / Groups">
                                        <form:hidden id="_currentRole" path="currentRole"
                                                     placeholder="Enter Roles / Groups" class="form-control input-md"/>
                                        <form:errors path="currentRole" class="errorMessage"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="button" value="Submit" id="btn-send" class="pp_button">
                                <!--                                     <button id="btn-send" name="btn-send" class="btn btn-send">Submit</button> -->
                            </div>
                        </div>
                    </div>


                </fieldset>
            </form:form>
        </div>
    </div>
</div>
