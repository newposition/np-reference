<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<header>
    <div class="wraper">
        <div class="header_logo">
            <cms:area name="logo"/>
            <div class="short_menu">
                <ul>
                    <cms:area name="navLeft"/>
                </ul>
            </div>
        </div>
    </div>
</header>


