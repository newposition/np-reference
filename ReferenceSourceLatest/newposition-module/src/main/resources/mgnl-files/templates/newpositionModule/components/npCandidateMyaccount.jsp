<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>

<script src="./resources/templating-kit/js/myEducationForm.js"></script>
<script src="./resources/templating-kit/js/employment.js"></script>
<script src="./resources/templating-kit/js/myFuturePage.js"></script>
<script src="./resources/templating-kit/js/clientMyaccount.js"></script>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="./resources/templating-kit/js/jquery1.11.2.js"></script> -->
<!-- <script src="./resources/templating-kit/js/bootstrap.js"></script> -->
<script src="./resources/templating-kit/js/bootstrap-switch.min.js"></script>
<script src="./resources/templating-kit/js/custom.js"></script>
<!--<script src="js/jquery_002.js"></script> -->
<script src="./resources/templating-kit/js/jquery-my-account.js"></script>
<!-- <script src="./resources/templating-kit/js/jquery-ui.js"></script> -->
<script src="./resources/templating-kit/js/summernote.min.js"></script>
<script type="text/javascript" src="./resources/templating-kit/js/rotator.js"></script>

<!-- Bootstrap TabCollapse-->
<!-- <script type="text/javascript" src="./resources/templating-kit/js/bootstrap-tabcollapse.js"></script> -->

<div class="container">
    <div class="row mgb20">
        <div class="col-md-6">
            <div class="media">
                <div class="media-left">
                    <div style="margin-top:0px; padding-top:0px; background:url('./resources/templating-kit/img/user_ico.png') center center no-repeat #6AB5F1;border-radius:50%; width:100px; height:100px;">
                        <canvas width="0" height="0px" style="display:block;" id="userStatus"></canvas>
                        <input style="width: 79px; height: 50px; position: absolute; vertical-align: middle; margin-top: 50px; margin-left: -114px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 30px Arial; text-align: center; color: rgb(34, 34, 34); padding: 0px; display:none;"
                               class="knob" data-width="100" data-cursor="true" data-fgcolor="#222222"
                               data-thickness=".3" value="21" id="current_value">
                    </div>
                </div>
                <div class="media-body">
                    <div class="media-body-head">${npUser.firstName} ${npUser.lastName} </div>
                    <span class="gray_text">DD/MM/YYYY</span><br/>
                    <span class="gray_text abhishek_value_fetch">${npCandidate.availableStatus}</span>
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <div class="media">
                <div class="media-left">
                    <div id="rotator" style="height:90px;width:90px"></div>
                </div>
                <div class="media-body">
                    <input type="hidden" id="profilePercentage" value="${candidateProfilePercentage}">
                    <div class="media-body-head">Profile is ${candidateProfilePercentage}% Complete</div>
                    <span class="gray_text">Make your profile more appealing for potential employers by making your profile 100% complete</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="tab_collapse_master">
        <ul id="myTab" class="nav nav-tabs">

            <li <c:if test="${isMyAccount}"> class="active" </c:if> ><a href="#csm" data-toggle="tab">Career Summary</a>
            </li>
            <li <c:if test="${isPreference}"> class="active" </c:if>><a href="#pref" data-toggle="tab">Preferences</a>
            </li>
        </ul>
        <div class="contain">
            <div class="row">
                <div class="col-md-10 centered mi_tp">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade  <c:if test="${isMyAccount}" > in active </c:if> " id="csm">
                            <div class="col-md-12 centered">

                                <div class="row no_bottom_padding">
                                    <div class="g40">
                                        <div class="blue_border bb_min">

                                            <div class="bd-rts1">
                                                <div class="pp_head_font row_padding_left_elementd edit_pencil_button">
                                                    Contact Details
                                                    <input type="button" class="edit_pencil edit-save"
                                                           data-toggle="modal" data-target="#Con_Det_Modal"
                                                           onclick="javascript:mycontactdetail(1);">
                                                </div>
                                                <div class="">
                                                    <div class="row_padding_left_elementd pharagraph_padding_right size_16">
                                                        <p><span id="dvfirstName1">${npUser.firstName} </span><span
                                                                id="dvlastName1">${npUser.lastName}</span><br>
                                                            <span id="dvemail1">${npUser.primaryEmail}</span><br>
                                                            <br>
                                                            <span id="dvtel1"> ${npUser.phone}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="g60">
                                        <div class="blue_border bb_min">
                                            <div class="pull-right gcvp">
                                                <a href="" class="pp_button pp_button_small gcvp-btn">Generate CV
                                                    PDF</a>
                                            </div>


                                            <div class="">
                                                <div class="pp_head_font row_padding_left_elementd2 edit_pencil_button">
                                                    Personal Statements
                                                    <!--<input type="button" class="edit_pencil" data-toggle="modal" data-target="#editor_Modal">-->
                                                    <input type="button" class="edit_pencil edit-save"
                                                           id="myaccount_personalStatement">
                                                </div>
                                                <div class="pharagraph_padding_right row_padding_left_elementd2 size_16">
                                                    <div class="editorr">
                                                        <textarea name="editor1" id="editor1" class="peseditor" rows="7"
                                                                  cols="40">${npCandidate.personalStatement}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row no_bottom_padding">
                                    <div class="g50">
                                        <div class="border_out_heading">
                                            <div class="col-md-12">Prefered Company</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row blue_border">

                                                <div class="links_js links_jsonlyclick link_no_padding future_comp">
                                                    <c:forEach items="${npCandidate.npCandidateFutureEmployers}"
                                                               var="npCandidateFutureEmployer">
                                                        <c:if test="${npCandidateFutureEmployer.futurePreferedCompany eq 1}"><a
                                                                href="javascript:void(0)">${npCandidateFutureEmployer.npEmployer.name}<i
                                                                class="fa fa-times"></i></a> </c:if>
                                                    </c:forEach></div>
                                                <input id="input_FC" type="text" class="pp_text" placeholder="Enter comma seperate list of Future Companies
">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g50a">
                                        <div class="border_out_heading">
                                            <div class="col-md-12">Blocked Company</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row blue_border">
                                                <div class="links_js links_jsonlyclick link_no_padding exclude_comp">
                                                    <c:forEach items="${npCandidate.npCandidateFutureEmployers}"
                                                               var="npCandidateFutureEmployer">
                                                        <c:if test="${npCandidateFutureEmployer.futurePreferedCompany eq 0}"><a
                                                                href="javascript:void(0)">${npCandidateFutureEmployer.npEmployer.name}<i
                                                                class="fa fa-times"></i></a> </c:if>
                                                    </c:forEach></div>
                                                <input id="input_ExC" type="text" class="pp_text" placeholder="Enter comma seperate list of Excluded Companies
">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="border_out_heading">Future Skills</div>
                                <div class="row blue_border">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="links_js links_jsonlyclick link_no_padding  list_skills">
                                                <c:forEach items="${npCandidate.npCandidateSkills}"
                                                           var="npCandidateSkill">
                                                    <c:if test="${npCandidateSkill.preferfutureSkill eq 0}"><a
                                                            href="javascript:void(0)">${npCandidateSkill.npSkill.skillName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                    <c:if test="${npCandidateSkill.preferfutureSkill eq 1}"><a
                                                            href="javascript:void(0)"
                                                            class="blue_link">${npCandidateSkill.npSkill.skillName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                </c:forEach></div>
                                        </div>
                                        <div class="row">
                                            <input type="text" id="skills_input" class="pp_text"
                                                   placeholder="Enter comma seperate list of skills ">
                                        </div>
                                    </div>
                                </div>
                                <div class="border_out_heading">Future Roles</div>
                                <div class="row blue_border">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="links_js links_jsonlyclick link_no_padding  list_roles">
                                                <c:forEach items="${npCandidate.npCandidateEmpRoles}"
                                                           var="npCandidateRole">
                                                    <c:if test="${npCandidateRole.futurePreferedRole eq 0}"><a
                                                            href="javascript:void(0)">${npCandidateRole.npEmploymentRoles.roleName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                    <c:if test="${npCandidateRole.futurePreferedRole eq 1}"><a
                                                            href="javascript:void(0)"
                                                            class="blue_link">${npCandidateRole.npEmploymentRoles.roleName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="text" id="role_input" class="pp_text"
                                                   placeholder="Enter comma seperate list of roles">
                                        </div>
                                    </div>
                                </div>
                                <div class="border_out_heading">Future Tools &amp; System</div>
                                <div class="row blue_border">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="links_js links_jsonlyclick link_no_padding  list_tools">
                                                <c:forEach items="${npCandidate.npCandidateTools}"
                                                           var="npCandidateTool">
                                                    <c:if test="${npCandidateTool.futurePreferedTool eq 0}"><a
                                                            href="javascript:void(0)">${npCandidateTool.npTools.toolName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                    <c:if test="${npCandidateTool.futurePreferedTool eq 1}"><a
                                                            href="javascript:void(0)"
                                                            class="blue_link">${npCandidateTool.npTools.toolName}<i
                                                            class="fa fa-times"></i></a> </c:if>
                                                </c:forEach></div>
                                        </div>
                                        <div class="row">
                                            <input type="text" id="tool_input" class="pp_text" placeholder="Enter comma seperate list of tools level and number of years (e.g.Java 1 beginner ,C# 12 beginner
">
                                        </div>
                                    </div>
                                </div>

                                <div class="border_out_heading">Technical Skills</div>
                                <div class="row blue_border">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="links_js links_jsonlyclick link_no_padding">
                                                <c:forEach items="${npCandidate.npCandidateSkills}"
                                                           var="npCandidateSkill">
                                                    <a href="javascript:void(0)">${npCandidateSkill.npSkill.skillName}<i
                                                            class="fa fa-times"></i></a>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <input type="text" id='addskillBasket' class="pp_text" placeholder="Enter comma seperate list of tools level and number of years (e.g.Java 1 beginner ,C# 12 beginner)
">
                                        </div>
                                    </div>
                                </div>


                                <div class="border_out_heading">Employment History</div>
                                <c:if test="${empty npCandidate.npCandidateEmployers}">
                                    <div class="row blue_border">
                                        <button onclick="window.location='addMyaccountEmployment'"
                                                class="plus"></button>
                                    </div>
                                </c:if>
                                <c:forEach items="${npCandidateEmployers}" var="npCandidateEmployer"
                                           varStatus="thecount">
                                    <div class="row blue_border">
                                        <div class="col-md-5">
                                            <div class="row row_padding_right_elementd mgb20">
                                                <div class="em_box">
                                                    <button class="edit edit_btn"
                                                            style="right:0 !important; left:inherit; top:0;"
                                                            data-toggle="modal"
                                                            onclick="javascript:myFunction(${thecount.count});"
                                                            data-target="#Emp_hist_Modal"></button>
                                                    <div class="em_box_logo">
                                                    </div>
                                                    <div class="em_box_details">
                                                        <input type="hidden" id="employerId${thecount.count}"
                                                               value="${npCandidateEmployer.uid}"/>
                                                        <div class="line1"
                                                             id="dvcompany${thecount.count}">${npCandidateEmployer.npEmployer.name}</div>
                                                        <div class="line2"><span
                                                                id="dvdatefrom${thecount.count}"><fmt:formatDate
                                                                pattern="dd/MM/yyyy"
                                                                value="${npCandidateEmployer.startDate}"/></span>-<span
                                                                id="dvdateto${thecount.count}">
            <c:if test="${npCandidateEmployer.enddate eq null}">
                current
            </c:if>
            <c:if test="${npCandidateEmployer.enddate ne null}">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${npCandidateEmployer.enddate}"/>
            </c:if>
           </span></div>

                                                        <div class="line3"
                                                             id="dvpost${thecount.count}">${npCandidateEmployer.corporateTitle}</div>
                                                        <div class="line4"
                                                             id="dvtitle${thecount.count}">${npCandidateEmployer.jobTitle}</div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row row_padding_right_elementd">
                                                <div class="achv_box achv_box_top">
                                                    <div class="row pp_head_font">
                                                        <div class="col-md-12 edit_pencil_button">Achievements
                                                            <input type="button" class="edit_pencil  edit-save"
                                                                   id="myaccount_achievements">
                                                        </div>
                                                    </div>
                                                    <div class="editorr">
                                                        <textarea name="editor3" id="editor3" class="acheditor" rows="7"
                                                                  cols="40">${npCandidateEmployer.achievements}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="row border_bottom_right_one">
                                                <div class="row pp_head_font">
                                                    <div class="col-md-12">Skills
                                                        <span class="pull-right"><input type="button"
                                                                                        class="edit_pencil sk-edit"
                                                                                        id="sk-edit1"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="links_js links_jsonlyclick link_no_padding list_myaccount_skills">
                                                    <c:forEach items="${npCandidateEmployer.npSkills}" var="skill"
                                                               varStatus="status">
                                                        <a href="javascript:void(0)" name="${skill.skillName}"
                                                           id="${skill.id}">${skill.skillName}<i
                                                                class="fa fa-times"></i></a>
                                                    </c:forEach>
                                                    <div class="clearfix"></div>

                                                    <div id="sk-qeb1" class="mg10 sk-qeb" style="display:none;">
                                                        <input type="text" placeholder="Enter comma seperate list"
                                                               id="skillsInput_${npCandidateEmployer.uid}"
                                                               class="pp_text addNewSkill">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row border_bottom_right_one">
                                                <div class="row pp_head_font">
                                                    <div class="col-md-12">Roles
                                                        <span class="pull-right"><input type="button"
                                                                                        class="edit_pencil rl-edit"
                                                                                        id="rl-edit1"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="links_js links_jsonlyclick link_no_padding list_myaccount_roles">
                                                    <c:forEach items="${npCandidateEmployer.npEmploymentRoles}"
                                                               var="role" varStatus="status">
                                                        <a href="javascript:void(0)" name="${role.roleName}"
                                                           id="${role.id}">${role.roleName}<i
                                                                class="fa fa-times"></i></a>
                                                    </c:forEach>
                                                    <div class="clearfix"></div>

                                                    <div id="rl-qeb1" class="mg10 rl-qeb" style="display:none;">
                                                        <input type="text" id="roleInput_${npCandidateEmployer.uid}"
                                                               placeholder="Enter comma seperate list"
                                                               class="pp_text addNewRole">
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row border_bottom_right_one">
                                                <div class="row pp_head_font">
                                                    <div class="col-md-12">Tools And Systems
                                                        <span class="pull-right"><input type="button"
                                                                                        class="edit_pencil tas-edit"
                                                                                        id="tas-edit1"></span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="links_js links_jsonlyclick link_no_padding list_myaccount_tools">
                                                    <c:forEach items="${npCandidateEmployer.npTools}" var="tool"
                                                               varStatus="status">
                                                        <a href="javascript:void(0)" name="${tool.toolName}"
                                                           id="${tool.id}">${tool.toolName}<i
                                                                class="fa fa-times"></i></a>
                                                    </c:forEach>
                                                    <div class="clearfix"></div>

                                                    <div id="tas-qeb1" class="mg10 tas-qeb" style="display:none;">
                                                        <input type="text" id="toolInput_${npCandidateEmployer.uid}"
                                                               placeholder="Enter comma seperate list"
                                                               class="pp_text addNewTool">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <c:if test="${thecount.count eq 1}">
                                            <button onclick="window.location='addMyaccountEmployment'"
                                                    class="plus"></button>
                                        </c:if>
                                    </div>
                                </c:forEach>


                                <div class="border_out_heading">Education History</div>
                                <div class="row blue_border">

                                    <c:set var="count" value="0"/>
                                    <c:forEach items="${npCandidate.npCandidateEducations}" var="npCandidateEducation"
                                               varStatus="theCount">
                                    <c:if test="${!(npCandidateEducation.npInstituteCourse.npCourse.courseType eq ' ')}">

                                    <c:set var="count" value="${count+1}"/>


                                    <div class="col-md-6">
                                        <c:choose>
                                        <c:when test="${count mod 2 ==0}">
                                        <div class="em_box remove_3_border_last">
                                            </c:when>
                                            <c:otherwise>
                                            <div class="em_box remove_3_border">
                                                </c:otherwise>
                                                </c:choose>


                                                <button class="edit edit_btn"
                                                        style="right:0 !important; left:inherit; top:0;"
                                                        data-toggle="modal" onclick="javascript:eduFunction(${count});"
                                                        data-target="#Edu_hist_Modal"></button>

                                                <div class="em_box_logo">
                                                    <c:if test="${not empty npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}">
                                                        <img alt=""
                                                             src="${pageContext.request.contextPath}/docroot/images/${npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}"/>
                                                        <input type="hidden" id="eduinstituteLogo${count}"
                                                               value="${pageContext.request.contextPath}/docroot/images/${npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}">
                                                    </c:if>
                                                </div>
                                                <div class="em_box_details">
                                                    <input type="hidden" id="edid${count}" name="id"
                                                           value="${npCandidateEducation.npInstituteCourse.id}">
                                                    <div id="edinst${count}"
                                                         class="line1">${npCandidateEducation.npInstituteCourse.npInstitutions.instituteName }</div>
                                                    <div id="edawt${count}"
                                                         class="line2">${npCandidateEducation.npInstituteCourse.npCourse.courseName}</div>
                                                    <div id="edaws${count}"
                                                         class="line3">${npCandidateEducation.npInstituteCourse.npCourse.courseType}</div>
                                                    <div id="edela${count}"
                                                         class="line4">${npCandidateEducation.level}</div>
                                                    <div class="line5"><span id="eddfrm${count}"> <fmt:formatDate
                                                            pattern="dd/MM/yyyy"
                                                            value="${npCandidateEducation.startDate}"/></span>-<span
                                                            id="eddto${count}">
            <c:if test="${npCandidateEducation.endDate eq null}">
                current
            </c:if>
            <c:if test="${npCandidateEducation.endDate ne null}">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${npCandidateEducation.endDate}"/>
            </c:if>
           </span></div>
                                                </div>
                                            </div>
                                        </div>
                                        </c:if>
                                        </c:forEach>

                                        <button data-target="#Edu_hist_Modal" data-toggle="modal" class="plus"
                                                onclick="javascript:eduFunction();"></button>
                                    </div>
                                    <div class="border_out_heading">Qualifications</div>
                                    <div class="row blue_border">
                                        <c:set var="count" value="0"/>
                                        <c:forEach items="${npCandidate.npCandidateEducations}"
                                                   var="npCandidateEducation" varStatus="theCount">
                                        <c:if test="${npCandidateEducation.npInstituteCourse.npCourse.courseType eq ' '}">
                                        <c:set var="count" value="${count+1}"/>
                                        <div class="col-md-6">
                                            <c:choose>
                                            <c:when test="${count mod 2 ==0}">
                                            <div class="em_box remove_3_border_last">
                                                </c:when>
                                                <c:otherwise>
                                                <div class="em_box remove_3_border">
                                                    </c:otherwise>
                                                    </c:choose>


                                                    <button class="edit edit_btn"
                                                            style="right:0 !important; left:inherit; top:0;"
                                                            data-toggle="modal" data-target="#Qua_Modal"
                                                            onclick="javascript:quaFunction(${count});"></button>

                                                    <div class="em_box_logo">
                                                        <c:if test="${not empty npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}">
                                                            <img alt=""
                                                                 src="${pageContext.request.contextPath}/docroot/images/${npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}"/>
                                                            <input type="hidden" id="quainstituteLogo${count}"
                                                                   value="${pageContext.request.contextPath}/docroot/images/${npCandidateEducation.npInstituteCourse.npInstitutions.instituteLogo}">
                                                        </c:if>
                                                    </div>
                                                    <div class="em_box_details">
                                                        <input type="hidden" id="quaid${count}" name="id"
                                                               value="${npCandidateEducation.npInstituteCourse.id}">
                                                        <div id="quainst${count}"
                                                             class="line1">${npCandidateEducation.npInstituteCourse.npInstitutions.instituteName }</div>
                                                        <div id="quaawt${count}"
                                                             class="line2">${npCandidateEducation.npInstituteCourse.npCourse.courseName}</div>
                                                        <div id="quaela${count}"
                                                             class="line4">${npCandidateEducation.level}</div>
                                                        <div class="line5"><span id="quadfrm${count}"> <fmt:formatDate
                                                                pattern="dd/MM/yyyy"
                                                                value="${npCandidateEducation.startDate}"/></span>-<span
                                                                id="quadto${count}">
             <c:if test="${npCandidateEducation.endDate eq null}">
                 current
             </c:if>
            <c:if test="${npCandidateEducation.endDate ne null}">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${npCandidateEducation.endDate}"/>
            </c:if>
       </span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            </c:if>
                                            </c:forEach>

                                            <button class="plus" data-toggle="modal" data-target="#Qua_Modal"
                                                    onclick="javascript:quaFunction();"></button>
                                        </div>
                                        <div class="border_out_heading">Certifications</div>
                                        <div class="row blue_border">

                                            <c:forEach items="${npCandidate.npCandidateCertificates}"
                                                       var="npCandidateCertificate" varStatus="theCount">

                                            <div class="col-md-6">
                                                <c:choose>
                                                <c:when test="${theCount.count mod 2 ==0}">
                                                <div class="em_box remove_3_border_last">
                                                    </c:when>
                                                    <c:otherwise>
                                                    <div class="em_box remove_3_border">
                                                        </c:otherwise>
                                                        </c:choose>


                                                        <button class="edit edit_btn"
                                                                style="right:0 !important; left:inherit; top:0;"
                                                                data-toggle="modal" data-target="#Cer_Modal"
                                                                onclick="javascript:certFunction(${theCount.count});"></button>

                                                        <div class="em_box_logo">
                                                            <c:if test="${not empty npCandidateCertificate.npInstitutions.instituteLogo}">
                                                                <img alt=""
                                                                     src="${pageContext.request.contextPath}/docroot/images/${npCandidateCertificate.npInstitutions.instituteLogo}"/>
                                                                <input type="hidden"
                                                                       id="certinstituteLogo${theCount.count}"
                                                                       value="${pageContext.request.contextPath}/docroot/images/${npCandidateCertificate.npInstitutions.instituteLogo}">
                                                            </c:if>
                                                        </div>
                                                        <div class="em_box_details">
                                                            <input type="hidden" id="certid${theCount.count}" name="id"
                                                                   value="${npCandidateCertificate.npCertifications.id}">

                                                            <div id="certinst${theCount.count}"
                                                                 class="line1">${npCandidateCertificate.npInstitutions.instituteName}</div>
                                                            <div id="certaws${theCount.count}"
                                                                 class="line2">${npCandidateCertificate.npCertifications.certificateName}</div>
                                                            <div id="certela${theCount.count}"
                                                                 class="line3">${npCandidateCertificate.gradeLevel}</div>
                                                            <div id="certdfrm${theCount.count}"
                                                                 class="line5">${npCandidateCertificate.certificationYear}</div>

                                                        </div>
                                                    </div>
                                                </div>
                                                </c:forEach>

                                                <button class="plus" data-toggle="modal" data-target="#Cer_Modal"
                                                        onclick="javascript:certFunction();"></button>
                                            </div>
                                            <div class="border_out_heading">Compensation</div>

                                            <div class="row blue_border">

                                                <div class="col-md-12">
                                                    <form id="form-contact" action="" name="npCompensation"
                                                          method="post">
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <div class="row clear-padding">
                                                                    <div class="col-md-5">
                                                                        <label class="pp_head_font">Preference
                                                                            Position</label>
                                                                    </div>
                                                                    <div class="col-md-7 edit_pencil_button">
                                                                        <label class="pp_head_font">Target
                                                                            Compensation</label>
                                                                        <input type="button" id="npCompensationButton"
                                                                               class="edit_pencil edit-save">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <c:set var="compensationType"
                                                                   value="${npCompensation.compensationType}"></c:set>
                                                            <div class="form-group">
                                                                <div class="row clear-padding">
                                                                    <div class="col-md-5">
                                                                        <label class="pp_label">&nbsp;</label>

                                                                        <div class="radio">
                                                                            <label for="radios-0" class="pp_label">

                                                                                <input type="radio"
                                                                                       name="compensationType"
                                                                                       name="radios-switch"
                                                                                       id="radios-0" data-size="mini"
                                                                                       value="Permanent"
                                                                                       data-inverse="true" ${fn:containsIgnoreCase(compensationType,'permanent') ? 'checked' : ''} />
                                                                                <!--                                         <input type="radio" name="radios" id="radios-0" value="1" checked="checked"> -->
                                                                                &nbsp; Permanent
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-7" id="permanent"  <c:if
                                                                            test="${fn:containsIgnoreCase(npCompensation.compensationType, 'contract')}"> style="display:none" </c:if>>
                                                                        <div class="row clear-padding">

                                                                            <div class="col-md-4">

                                                                                <label for="lname" class="pp_label">Salary</label>
                                                                                <input type="text" name="salary"
                                                                                       placeholder="Salary"
                                                                                       disabled="disabled"
                                                                                       value="${npCompensation.salary}"
                                                                                       class="pp_text input-md"/>
                                                                                <span id="salary_error"
                                                                                      class="errorMessage"></span>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label for="lname" class="pp_label">Bonus</label>
                                                                                <input type="text" name="bonus"
                                                                                       placeholder="Bonus"
                                                                                       disabled="disabled"
                                                                                       value="${npCompensation.bonus}"
                                                                                       class="pp_text input-md"/>
                                                                                <span id="bonus_error"
                                                                                      class="errorMessage"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="row clear-padding">
                                                                    <div class="col-md-5">
                                                                        <label class="pp_label">&nbsp;</label>
                                                                        <div class="radio">
                                                                            <label for="radios-1" class="pp_label">

                                                                                <input type="radio"
                                                                                       name="compensationType"
                                                                                       name="radios-switch"
                                                                                       id="radios-1" value="Contract"
                                                                                       data-size="mini"
                                                                                       data-inverse="true" ${fn:containsIgnoreCase(compensationType , 'Contract') ? 'checked' : ''} />
                                                                                &nbsp; Contract
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-7" id="contract" <c:if
                                                                            test="${fn:containsIgnoreCase(npCompensation.compensationType, 'permanent')}"> style="display:none" </c:if>>
                                                                        <div class="row clear-padding">
                                                                            <div class="col-md-4">
                                                                                <label for="lname" class="pp_label">Day
                                                                                    Rate</label>
                                                                                <input type="text" name="dayrate"
                                                                                       placeholder="Day Rate"
                                                                                       value="${npCompensation.dayrate}"
                                                                                       id="lname" disabled="disabled"
                                                                                       class="pp_text input-md"/>
                                                                                <span id="dayrate_error"
                                                                                      class="errorMessage"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row clear-padding">
                                                                    <div class="col-md-5">
                                                                        <label class="pp_label">&nbsp;</label>
                                                                        <div class="radio">
                                                                            <label for="radios-2" class="pp_label">
                                                                                <input type="radio"
                                                                                       name="compensationType"
                                                                                       name="radios-switch"
                                                                                       id="radios-2" value="Donotmind"
                                                                                       data-size="mini"
                                                                                       data-inverse="true" ${fn:containsIgnoreCase(compensationType , 'Donotmind') ? 'checked' : ''} />

                                                                                Don't Mind
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                            </div>


                                            <div class="border_out_heading">References</div>
                                            <div class="row blue_border">
                                                <c:forEach items="${npCandidate.npCandidateReferences}"
                                                           var="npCandidateReference" varStatus="theCount">
                                                <div class="col-md-6">
                                                    <c:choose>
                                                    <c:when test="${theCount.count ge 2 }">
                                                    <div class="em_box remove_3_border size_16">
                                                        </c:when>
                                                        <c:otherwise>
                                                        <div class="em_box remove_3_border size_16 bd-top">
                                                            </c:otherwise>
                                                            </c:choose>


                                                            <div class="col-md-12">
                                                                <button class="edit edit_btn"
                                                                        style="right:0 !important; left:inherit; top:0;"
                                                                        data-toggle="modal" data-target="#Ref_Modal"
                                                                        onclick="javascript:refFunction(${theCount.count});"></button>
                                                                <input type="hidden" id="refid${theCount.count}"
                                                                       value="${npCandidateReference.id}"/>
                                                                <div class="row row_padding_All"><span
                                                                        id="reffname${theCount.count}">${npCandidateReference.firstName} </span><span
                                                                        id="reflname${theCount.count}"
                                                                        style="float:right; padding-right:50px;">${npCandidateReference.surName}</span><br>
                                                                    <span id="refrelation${theCount.count}">${npCandidateReference.relationship}</span><br>
                                                                    <span id="refemail${theCount.count}">${npCandidateReference.email} </span><br>
                                                                    <span id="reffrm${theCount.count}"><fmt:formatDate
                                                                            pattern="dd/MM/yyyy"
                                                                            value="${npCandidateReference.dateFrom}"/></span>-
                 <span id="refto${theCount.count}">
                 <c:if test="${npCandidateReference.dateTo eq null}">
                     current
                 </c:if>
            <c:if test="${npCandidateReference.dateTo ne null}">
                <fmt:formatDate pattern="dd/MM/yyyy" value="${npCandidateReference.dateTo}"/>
            </c:if>
                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </c:forEach>
                                                    <button class="plus" data-toggle="modal" data-target="#Ref_Modal"
                                                            onclick="javascript:refFunction();"></button>
                                                    <button class="edit"></button>
                                                </div>
                                                <div class="border_out_heading">Eligible To Work</div>
                                                <div class="row blue_border">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="links_js links_jsonlyclick link_no_padding list_countries">
                                                                <c:forEach
                                                                        items="${npCandidate.npCandidateEligibilities}"
                                                                        var="npCandidateEligibility">
                                                                    <a href="javascript:void(0)">${npCandidateEligibility.npEligibility.countryName}<i
                                                                            class="fa fa-times"></i></a>
                                                                </c:forEach>
                                                                <c:forEach items="${npCandidate.npCandidateRegions}"
                                                                           var="npCandidateRegions">
                                                                    <a href="javascript:void(0)">${npCandidateRegions.region.regionName}<i
                                                                            class="fa fa-times"></i></a>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <input type="text" id="Eligblity_input" class="pp_text"
                                                                   placeholder="Enter prefer Countries you want to work in (e.g. Paris)"
                                                            ">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row mgb20">
                                                    <div class="g50">
                                                        <div class="border_out_heading">
                                                            <div class="col-md-12">Memberships <input type="button"
                                                                                                      class="edit_pencil  edit-save">
                                                            </div>
                                                        </div>
                                                        <div class="achv_box achv_box_top">
                                                            <div class="editorr">
                                                                <textarea name="editor4" id="editor4" class="sumeditor"
                                                                          rows="7" cols="40"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="g50a">
                                                        <div class="border_out_heading">
                                                            <div class="col-md-12">Honour &amp; Awards <input
                                                                    type="button" class="edit_pencil  edit-save"></div>
                                                        </div>
                                                        <div class="achv_box achv_box_top">
                                                            <div class="editorr">
                                                                <textarea name="editor5" id="editor5" class="sumeditor"
                                                                          rows="7" cols="40"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row uploadcv mgb20">
                                                    <div class="col-md-9 centered">
                                                        <form enctype="multipart/form-data" action="ajax/uploadCV"
                                                              method="post">
                                                            <div class="form-group col-md-9"
                                                                 style="padding-left:0;padding-right:5px;">
                                                                <label for="" class="pp_label">Please upload your
                                                                    CV</label>
                                                                <input type="file" class="pp_text"
                                                                       placeholder="File location" name="cvFile"
                                                                       value="">
                                                            </div>
                                                            <div class="form-group col-md-3" style="padding-left:0;">
                                                                <label for="" class="pp_label">&nbsp;</label><br>
                                                                <button type="submit" class="pp_button">Upload</button>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>


                                                <div class="row no_bottom_padding">
                                                    <div class="g50">
                                                        <div class="border_out_heading">
                                                            <div class="col-md-12">Languages Spoken</div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row blue_border language_tog">
                                                                <div class="links_js links_jsonlyclick link_no_padding">
                                                                    <c:forEach
                                                                            items="${npCandidate.npCandidateLanguages}"
                                                                            var="npCandidateLanguages">
                                                                        <a href="javascript:void(0)"
                                                                           class="blue_link">${npCandidateLanguages.npLanguages.languageName}-${npCandidateLanguages.languageFluency}<i
                                                                                class="fa fa-times"></i></a>
                                                                    </c:forEach>
                                                                </div>
                                                                <input type="text" class="pp_text" id="Languages"
                                                                       placeholder="Enter comma seperate list of  languages">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="g50a">
                                                        <div class="border_out_heading">
                                                            <div class="col-md-12">Interests</div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row blue_border interest_tog">
                                                                <div class="links_js links_jsonlyclick link_no_padding">
                                                                    <c:forEach
                                                                            items="${npCandidate.npCandidateInterests}"
                                                                            var="npCandidateIntrest">
                                                                        <a href="javascript:void(0)"
                                                                           class="blue_link">${npCandidateIntrest.interestArea}<i
                                                                                class="fa fa-times"></i></a>
                                                                    </c:forEach></div>
                                                                <input type="text" class="pp_text" id="interests"
                                                                       placeholder="Enter comma seperate list of Interests">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="tab-pane fade <c:if test="${isPreference}" > in active </c:if> "
                                             id="pref">
                                            <div class="col-md-8 centered">
                                                <div class="row">
                                                    <form:form class="form-horizontal" id="preferenceForm" action="?"
                                                               commandName="npCandidatePreferenceForm" method="POST"
                                                               enctype="multipart/form-data">

                                                        <fieldset>
                                                            <div class="row mgb20">
                                                                <div class="col-md-12"><label class="pp_label"
                                                                                              for="textinput">Primary
                                                                    Email</label>
                                                                    <form:input type="text" path="primaryEmail"
                                                                                placeholder="Primary Email"
                                                                                class="pp_text"/>
                                                                    <form:errors path="primaryEmail"
                                                                                 class="errorMessage"/>

                                                                </div>
                                                            </div>
                                                            <div class="row mgb20">
                                                                <div class="col-md-12"><label class="pp_label"
                                                                                              for="textinput">Secondary
                                                                    Email</label>
                                                                    <form:input type="text" path="secondaryEmail"
                                                                                placeholder="Secondary Email"
                                                                                class="pp_text"/>
                                                                    <form:errors path="secondaryEmail"
                                                                                 class="errorMessage"/>
                                                                </div>
                                                            </div>
                                                            <div class="row mgb20">
                                                                <div class="col-md-12"><label class="pp_label"
                                                                                              for="textinput">Tertiary
                                                                    Email</label>
                                                                    <form:input type="text" path="tertiaryEmail"
                                                                                placeholder="Tertiary Email"
                                                                                class="pp_text"/>
                                                                    <form:errors path="tertiaryEmail"
                                                                                 class="errorMessage"/>
                                                                </div>
                                                            </div>


                                                            <hr>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label for="check-0" class="pp_label">
                                                                            <form:checkbox path="summaryReports"
                                                                                           name="check-switch"
                                                                                           id="check-0" data-size="mini"
                                                                                           data-inverse="true"
                                                                                           data-off-color="warning"
                                                                                           data-off-text="&nbsp;&nbsp;&nbsp;"
                                                                                           data-on-text="&nbsp;&nbsp;&nbsp;"/>

                                                                            Send me summary reports on Market
                                                                            development affecting me.
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label for="check-1" class="pp_label">
                                                                            <form:checkbox path="sendOpportunities"
                                                                                           name="check-switch"
                                                                                           id="check-1" data-size="mini"
                                                                                           data-inverse="true"
                                                                                           data-off-color="warning"
                                                                                           data-off-text="&nbsp;&nbsp;&nbsp;"
                                                                                           data-on-text="&nbsp;&nbsp;&nbsp;"/>
                                                                            Let me know when opportunities come in where
                                                                            i am a good fit.
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label for="check-1" class="pp_label">
                                                                            <form:checkbox path="jobAppearance"
                                                                                           name="check-switch"
                                                                                           id="check-1" data-size="mini"
                                                                                           data-inverse="true"
                                                                                           data-off-color="warning"
                                                                                           data-off-text="&nbsp;&nbsp;&nbsp;"
                                                                                           data-on-text="&nbsp;&nbsp;&nbsp;"/>
                                                                            Let me know when my perfect job appears.
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label for="radicheckos-1" class="pp_label">
                                                                            <form:checkbox path="sendReports"
                                                                                           name="check-switch"
                                                                                           id="check-1" data-size="mini"
                                                                                           data-inverse="true"
                                                                                           data-off-color="warning"
                                                                                           data-off-text="&nbsp;&nbsp;&nbsp;"
                                                                                           data-on-text="&nbsp;&nbsp;&nbsp;"/>
                                                                            Send me my reports Daily / Weekly.
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <div class="checkbox">
                                                                        <label for="check-1" class="pp_label">
                                                                            <form:checkbox path="permissionShortlist"
                                                                                           name="check-switch"
                                                                                           id="check-1" data-size="mini"
                                                                                           data-inverse="true"
                                                                                           data-off-color="warning"
                                                                                           data-off-text="&nbsp;&nbsp;&nbsp;"
                                                                                           data-on-text="&nbsp;&nbsp;&nbsp;"/>
                                                                            Ask my permission before displaying my name
                                                                            on a shortlist.
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <hr>

                                                            <div class="form-group">
                                                                <div class="col-md-3">
                                                                    <div class="myimg">

                                                                        <c:if test="${not empty npCandidate.npCandidateDocument.photo}">
                                                                            <img src="data:image/*;base64,${profileImage}"
                                                                                 class="img-responsive" alt="">
                                                                        </c:if>
                                                                        <c:if test="${empty npCandidate.npCandidateDocument.photo}">
                                                                            <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg"
                                                                                 class="img-responsive" alt="">
                                                                        </c:if>
                                                                        <input type="button"
                                                                               class="my_edit_pencil myimg-edit">
                                                                        <input type="file" value=""
                                                                               accept=".png,.jpg,.gif" name="photo"
                                                                               id="uploadBtn1">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="mg-lt">
                                                                        <label class="col-md-12 pp_label">When to show
                                                                            my Photo.</label>
                                                                        <div class="form-group">

                                                                            <div class="col-md-12">
                                                                                <div class="radio">
                                                                                    <label for="radios-4"
                                                                                           class="pp_label">
                                                                                        <form:radiobutton
                                                                                                path="acceptPhoto"
                                                                                                value="false"
                                                                                                name="radios"
                                                                                                id="radios-4"/>
                                                                                        During entire Hiring Process
                                                                                    </label>
                                                                                </div>
                                                                                <div class="radio">
                                                                                    <label for="radios-5"
                                                                                           class="pp_label">
                                                                                        <form:radiobutton
                                                                                                path="acceptPhoto"
                                                                                                value="true"
                                                                                                name="radios"
                                                                                                id="radios-4"/>
                                                                                        When I have accepted an
                                                                                        Interview.
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 text-center"><input type="submit"
                                                                                                      value="Update Preferences"
                                                                                                      id="preferences"
                                                                                                      class="pp_button">
                                                            </div>


                                                        </fieldset>

                                                    </form:form>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--ADD Employment History Modal
                ================================================== -->
                <div class="modal fade" id="Add_Emp_hist_Modal" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Employment History</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form id="form-contact" action="">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="box_edit_logo-a">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="pCompanyName" name="pemail" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Company Name">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input id="pDateFrom" type="text"
                                                                           placeholder="Date From"
                                                                           class="datepicker-12 pp_text">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input id="pDateTo" type="text"
                                                                           placeholder="Date To"
                                                                           class="datepicker-12 pp_text">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="pTitle" name="pemail" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Corporate Title">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="pJobTitle" name="pemail" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Job Title">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="btn-confirm" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <script>

                    function mycontactdetail(i) {
                        $('#userForm .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        var fname = document.getElementById('dvfirstName' + i).innerHTML;
                        var lname = document.getElementById('dvlastName' + i).innerHTML;
                        var demail = document.getElementById('dvemail' + i).innerHTML;
                        var dtel = document.getElementById('dvtel' + i).innerHTML;

                        document.getElementById('mfirstName').value = fname;
                        document.getElementById('mlastName').value = lname;
                        document.getElementById('memail').value = demail;
                        document.getElementById('mtel').value = dtel.trim();
                    }
                    function myFunction(i) {
//             var dlogo=document.getElementById('dvlogo'+i).src;
                        $('#Emp_hist_Modal .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        var employerId = document.getElementById('employerId' + i).value;
                        var dcompany = document.getElementById('dvcompany' + i).innerHTML;
                        var ddatefrom = document.getElementById('dvdatefrom' + i).innerHTML;
                        var ddateto = document.getElementById('dvdateto' + i).innerHTML;
                        var dpost = document.getElementById('dvpost' + i).innerHTML;
                        var dtitle = document.getElementById('dvtitle' + i).innerHTML;

//             document.getElementById('mlogo').src=dlogo;employerId
                        $('#mlogo').hide();
                        $('#employerId').val(employerId);
                        $('#mcompany').val(dcompany);
                        $('#mdatefrom').val(ddatefrom);
                        $('#mdateto').val(ddateto.trim());
                        $('#mpost').val(dpost);
                        $('#mtitle').val(dtitle);
                        $(function () {
                            $(".datepicker-12").datepicker({
                                dateFormat: 'dd/mm/yy',
                                changeMonth: true,
                                changeYear: true,
                                yearRange: "1900:+30",
                                onClose: function (dateText, inst) {
                                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                }
                            });
                        });
                    }
                    function eduFunction(i) {
                        $('.educationHistory .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        if (!$.isNumeric(i)) {
                            $('#Edu_hist_Modal #id').val('');
                            $('#Edu_hist_Modal #Institution').val('');
                            $('#Edu_hist_Modal #level').val('');
                            $('#Edu_hist_Modal #awardType').val('');
                            $('#Edu_hist_Modal #awardSubject').val('');
                            $('#Edu_hist_Modal #educationStartDate').val('');
                            $('#Edu_hist_Modal  #educationEndDate').val('');
                        }
                        else {
                            var img = $('#eduinstituteLogo' + i).val();
                            if (img != undefined) {
                                $('#Edu_hist_Modal #emlogo').attr('src', img);
                                $('#Edu_hist_Modal #emlogo').show();
                            }
                            else {
                                $('#Edu_hist_Modal #emlogo').hide();
                            }
                            var id = document.getElementById('edid' + i).value;
                            var edvinst = document.getElementById('edinst' + i).innerHTML;
                            var edvawt = document.getElementById('edawt' + i).innerHTML;
                            var edvaws = document.getElementById('edaws' + i).innerHTML;
                            var edvela = document.getElementById('edela' + i).innerHTML;
                            var edvdfrm = document.getElementById('eddfrm' + i).innerHTML;
                            var edvdto = document.getElementById('eddto' + i).innerHTML;
                            $('#Edu_hist_Modal #id').val(id);
                            $('#Edu_hist_Modal #Institution').val(edvinst);
                            $('#Edu_hist_Modal #level').val(edvela);
                            $('#Edu_hist_Modal #awardType').val(edvawt);
                            $('#Edu_hist_Modal #awardSubject').val(edvaws);
                            $('#Edu_hist_Modal #educationStartDate').val(edvdfrm);
                            $('#Edu_hist_Modal  #educationEndDate').val(edvdto.trim());
                        }

                        $(function () {
                            $(".datepicker-12").datepicker({
                                dateFormat: 'dd/mm/yy',
                                changeMonth: true,
                                changeYear: true,
                                yearRange: "1900:+1",
                                onClose: function (dateText, inst) {
                                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                }
                            });
                        });
                    }
                    function quaFunction(i) {
                        $('.qualifications .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        if (!$.isNumeric(i)) {
                            $('#Qua_Modal #id').val('');
                            $('#Qua_Modal #Institution').val('');
                            $('#Qua_Modal #level').val('');
                            $('#Qua_Modal #awardSubject').val('');
                            $('#Qua_Modal #qualificationStartDate').val('');
                            $('#Qua_Modal  #qualificationEndDate').val('');
                        }
                        else {
                            var id = document.getElementById('quaid' + i).value;
                            var quinst = document.getElementById('quainst' + i).innerHTML;
                            var quawt = document.getElementById('quaawt' + i).innerHTML;
                            var quela = document.getElementById('quaela' + i).innerHTML;
                            var qudfrm = document.getElementById('quadfrm' + i).innerHTML;
                            var qudto = document.getElementById('quadto' + i).innerHTML;

                            var img = $('#quainstituteLogo' + i).val();
                            if (img != undefined) {
                                $('#Qua_Modal #qmlogo').attr('src', img);
                                $('#Qua_Modal #qmlogo').show();
                            }
                            else {
                                $('#Qua_Modal #qmlogo').hide();
                            }
                            $('#Qua_Modal #id').val(id);
                            $('#Qua_Modal #Institution').val(quinst);
                            $('#Qua_Modal #level').val(quela);
                            $('#Qua_Modal #awardSubject').val(quawt);
                            $('#Qua_Modal #qualificationStartDate').val(qudfrm);
                            $('#Qua_Modal  #qualificationEndDate').val(qudto.trim());
                        }

                        $(function () {
                            $(".datepicker-12").datepicker({
                                dateFormat: 'dd/mm/yy',
                                changeMonth: true,
                                changeYear: true,
                                yearRange: "1900:+1",
                                onClose: function (dateText, inst) {
                                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                }
                            });
                        });
                    }
                    function certFunction(i) {
                        $('.certifications .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        if (!$.isNumeric(i)) {
                            $('#Cer_Modal #id').val('');
                            $('#Cer_Modal #Institution').val('');
                            $('#Cer_Modal #level').val('');
                            $('#Cer_Modal #awardSubject').val('');
                            $('#Cer_Modal #startDate').val('');
                        }
                        else {
                            var id = document.getElementById('certid' + i).value;
                            var quinst = document.getElementById('certinst' + i).innerHTML;
                            var quaws = document.getElementById('certaws' + i).innerHTML;
                            var quela = document.getElementById('certela' + i).innerHTML;
                            var qudfrm = document.getElementById('certdfrm' + i).innerHTML;

                            var img = $('#certinstituteLogo' + i).val();
                            if (img != undefined) {
                                $('#Cer_Modal #cermlogo').attr('src', img);
                                $('#Cer_Modal #cermlogo').show();
                            }
                            else {
                                $('#Cer_Modal #cermlogo').hide();
                            }
                            $('#Cer_Modal #id').val(id);
                            $('#Cer_Modal #Institution').val(quinst);
                            $('#Cer_Modal #level').val(quela);
                            $('#Cer_Modal #awardSubject').val(quaws);
                            $('#Cer_Modal #startDate').val(qudfrm);
                        }


                        $(function () {
                            $(".datepicker-12").datepicker({
                                dateFormat: 'yy',
                                changeYear: true,
                                yearRange: "1900:+1",
                                onClose: function (dateText, inst) {
                                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                }
                            });
                        });
                    }
                    function refFunction(i) {
                        $('#Ref_Modal .errorText').text('');
                        $(".form-control").removeClass('errorTag');
                        if (!$.isNumeric(i)) {
                            $('#Ref_Modal #id').val('');
                            $('#Ref_Modal #firstName').val('');
                            $('#Ref_Modal #surName').val('');
                            $('#Ref_Modal #relationship').val('select');
                            $('#Ref_Modal #email').val('');
                            $('#Ref_Modal #dateFrom').val('');
                            $('#Ref_Modal #dateTo').val('');
                        }
                        else {
                            var refid = document.getElementById('refid' + i).value;
                            var rfname = document.getElementById('reffname' + i).innerHTML;
                            var rlname = document.getElementById('reflname' + i).innerHTML;
                            var rrelation = document.getElementById('refrelation' + i).innerHTML;
                            var remail = document.getElementById('refemail' + i).innerHTML;
                            var rfrm = document.getElementById('reffrm' + i).innerHTML;
                            var rto = document.getElementById('refto' + i).innerHTML;

                            document.getElementById('spid').value = refid;
                            $('#Ref_Modal #id').val(id);
                            $('#Ref_Modal #firstName').val(rfname);
                            $('#Ref_Modal #surName').val(rlname);
                            $('#Ref_Modal #relationship').val(rrelation);
                            $('#Ref_Modal #email').val(remail);
                            $('#Ref_Modal #dateFrom').val(rfrm);
                            $('#Ref_Modal #dateTo').val(rto.trim());
                        }
                    }
                </script>


                <!--Employment History Modal
                ================================================== -->
                <div class="modal fade" id="Emp_hist_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Employment History</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid em_box">

                                    <form id="form-contact" action="" name="myaccount">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="box_edit_logo">
                                                                <img src="" class="em_box_img" id="mlogo">
                                                                <a href="#">Edit Logo</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="errorText"></div>
                                                <fieldset>
                                                    <input type="hidden" id="employerId"/>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12 line1">
                                                                <input id="mcompany" name="pemail" type="text"
                                                                       class="form-control input-md" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="mdatefrom"
                                                                           placeholder="Date From"
                                                                           class="datepicker-12 pp_text line2ip1">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="mdateto"
                                                                           placeholder="Date From"
                                                                           class="datepicker-12 pp_text line2ip2">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12 line4">
                                                                <input id="mpost" name="pemail" type="text"
                                                                       class="form-control input-md " value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12 line3">
                                                                <input id="mtitle" name="pemail" type="text"
                                                                       class="form-control input-md " value="">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button name="btn-send"
                                                                        class="btn btn-confirm update_btn">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%--  <!--Add Education History Modal
                 ================================================== -->
                 <div class="modal fade" id="Add_Edu_hist_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                 <h3 class="modal-title text-center" id="myModalLabel">Education History</h3>
                             </div>
                             <div class="modal-body">
                                 <div class="container-fluid" id="form-contact">
                                     <form id="educationForm" action="" name="myaccount">
                                       <div class="row">
                                         <div class="col-md-4">
                                           <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="box_edit_logo-a">
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="col-md-8">
                                           <fieldset>
                                            <input type="hidden" id="id" name="id">
                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="Institution" name="Institution" type="text" class="form-control input-md" placeholder="Enter Institution">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="awardType" name="awardType" type="text" class="form-control input-md" placeholder="Enter Award Type">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="awardSubject" name="awardSubject" type="text" class="form-control input-md" placeholder="Enter Award Subject">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="level" name="level" type="text" class="form-control input-md" placeholder="Enter Level Attained">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-6">
                                                       <div class="input-group">
                                                           <input type="text" placeholder="Date From" name="startDate"  class="datepicker-12 pp_text hasDatepicker" id="educationStartDate">
                                                           <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                       </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                       <div class="input-group">
                                                           <input type="text" placeholder="Date From" name="endDate"  id="educationEndDate" class="datepicker-12 pp_text hasDatepicker">
                                                           <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                       </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <button id="saveEducation" name="btn-send" class="btn btn-confirm">Save</button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </fieldset>
                                         </div>
                                       </div>

                                   </form>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
              --%>
                <!--Education History Modal
                ================================================== -->
                <div class="modal fade" id="Edu_hist_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Education History</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid educationHistory" id="form-contact">
                                    <form id="educationForm" action="" name="myaccount" method="Post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="em_box_logo">
                                                                <img src="" class="em_box_img" id="emlogo">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="errorText"></div>
                                                <fieldset>
                                                    <input type="hidden" id="id" name="id">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="Institution" name="Institution" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Institution" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="awardType" name="awardType" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Award Type">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="awardSubject" name="awardSubject" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Award Subject">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="level" name="level" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Level Attained">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="educationStartDate"
                                                                           name="startDate" placeholder="Date From"
                                                                           class="datepicker-12 pp_text ">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="educationEndDate"
                                                                           name="endDate" placeholder="Date To"
                                                                           class="datepicker-12 pp_text ">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="saveEducation" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%--     <!--Add Qualification Modal
                    ================================================== -->
                    <div class="modal fade" id="Add_Qua_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title text-center" id="myModalLabel">Qualification</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <form id="form-contact" action="">
                                          <div class="row">
                                            <div class="col-md-4">
                                              <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="box_edit_logo-a">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                              <fieldset>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Institution">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Award Subject">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Level Attained">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                          <div class="input-group">
                                                              <input type="text" placeholder="Date From" class="datepicker-12 pp_text hasDatepicker">
                                                              <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                          </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                          <div class="input-group">
                                                              <input type="text" placeholder="Date From" class="datepicker-12 pp_text hasDatepicker">
                                                              <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button id="btn-confirm" name="btn-send" class="btn btn-confirm">Save</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                            </div>
                                          </div>

                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 --%>

                <!--Qualification Modal
                ================================================== -->
                <div class="modal fade" id="Qua_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Qualification</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid qualifications" id="form-contact">
                                    <form id="qualificationForm" action="" name="myaccount" method="Post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="em_box_logo">
                                                                <img src="" class="em_box_img" id="qmlogo">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="errorText"></div>
                                                <fieldset>
                                                    <input type="hidden" id="id" name="id">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="Institution" name="Institution" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Institution">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="awardSubject" name="awardSubject" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Award Subject">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="level" name="level" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Level Attained">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="qualificationStartDate"
                                                                           name="startDate" placeholder="Date From"
                                                                           class="datepicker-12 pp_text">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="qualificationEndDate"
                                                                           name="endDate" placeholder="Date From"
                                                                           class="datepicker-12 pp_text ">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="saveQualification" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%--  <!--Add Certification Modal
                 ================================================== -->
                 <div class="modal fade" id="Add_Cer_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                     <div class="modal-dialog">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                 <h3 class="modal-title text-center" id="myModalLabel">Certification</h3>
                             </div>
                             <div class="modal-body">
                                 <div class="container-fluid">
                                     <form id="form-contact" action="">
                                       <div class="row">
                                         <div class="col-md-4">
                                           <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <div class="box_edit_logo-a">
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="col-md-8">
                                           <fieldset>
                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Institution">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Award Subject">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Enter Level Attained">
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-6">
                                                       <div class="input-group">
                                                           <input type="text" placeholder="Date From" class="datepicker-12 pp_text hasDatepicker">
                                                           <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                       </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                       <div class="input-group">
                                                           <input type="text" placeholder="Date From" class="datepicker-12 pp_text hasDatepicker">
                                                           <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                                                       </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <div class="form-group">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                         <button id="btn-confirm" name="btn-send" class="btn btn-confirm">Save</button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </fieldset>
                                         </div>
                                       </div>

                                   </form>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div> --%>

                <!--Certification Modal
                ================================================== -->
                <div class="modal fade" id="Cer_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Certification</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid certifications" id="form-contact">
                                    <form id="certificationForm" action="" name="myaccount" method="Post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="em_box_logo">
                                                                <img src="" class="em_box_img" id="cermlogo">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="errorText"></div>
                                                <fieldset>
                                                    <input type="hidden" id="id" name="id">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="Institution" name="Institution" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Institution">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="awardSubject" name="awardSubject" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Award Subject">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="level" name="level" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Enter Level Attained">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="startDate" name="startDate"
                                                                           placeholder="Date From" class="pp_text">
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="saveCertification" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--     Add References Modal
                <!--     ================================================== -->
                <!--     <div class="modal fade" id="Add_Ref_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
                <!--         <div class="modal-dialog"> -->
                <!--             <div class="modal-content"> -->
                <!--                 <div class="modal-header"> -->
                <!--                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <!--                     <h3 class="modal-title text-center" id="myModalLabel">References</h3> -->
                <!--                 </div> -->
                <!--                 <div class="modal-body"> -->
                <!--                     <div class="container-fluid"> -->
                <%--                         <form id="form-contact" action=""> --%>
                <!--                           <div class="row"> -->
                <!--                             <div class="col-md-12"> -->
                <!--                               <fieldset> -->
                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-6"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="First Name"> -->
                <!--                                         </div> -->
                <!--                                         <div class="col-md-6"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Last Name"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Job Title"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Company"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                           <div class="input-group"> -->
                <!--                                               <input type="text" placeholder="Date of Refrence" class="datepicker-12 pp_text hasDatepicker"> -->
                <!--                                               <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span> -->
                <!--                                           </div> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="State"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Telephone"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <input id="pemail" name="pemail" type="text" class="form-control input-md" placeholder="Email"> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                                 <div class="form-group"> -->
                <!--                                     <div class="row"> -->
                <!--                                         <div class="col-md-12"> -->
                <!--                                             <button id="btn-confirm" name="btn-send" class="btn btn-confirm">Save</button> -->
                <!--                                         </div> -->
                <!--                                     </div> -->
                <!--                                 </div> -->

                <!--                             </fieldset> -->
                <!--                             </div> -->
                <!--                           </div> -->

                <%--                       </form> --%>
                <!--                     </div> -->
                <!--                 </div> -->
                <!--             </div> -->
                <!--         </div> -->
                <!--     </div> -->


                <!--References Modal
                ================================================== -->
                <div class="modal fade" id="Ref_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">References</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">

                                    <form id="form-contact" action="" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="errorText"></span>
                                                <fieldset>
                                                    <input name="edit" value="" type="hidden"/> <input type="hidden"
                                                                                                       name="spid"
                                                                                                       id="spid"/>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input id="firstName" name="firstName" type="text"
                                                                       maxlength="20" class="form-control input-md"
                                                                       placeholder="First Name"/>
                                                                <!--                                             <errors id ="firstName" class="errorMessage" /> -->
                                                                <span id="info_fname" class="errorMessage"></span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input name="surName" id="surName" type="text"
                                                                       maxlength="20" class="form-control input-md"
                                                                       placeholder="Last Name"/>
                                                                <%--                                             <errors path="surName" class="errorMessage" /> --%>
                                                                <span id="info_sname" class="errorMessage"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <!--                                             <input id="refmtitle" name="pemail" type="text" class="form-control input-md" placeholder="Job Title"> -->
                                                                <select id="relationship" name="relationship"
                                                                        class="form-control input-md">
                                                                    <option value="select">select</option>
                                                                    <option value="Personal">Personal</option>
                                                                    <option value="Professional">Professional</option>
                                                                </select>
                                                                <!--                                 <errors path="relationship" class="errorMessage" />  -->
                                                                <span id="info_relationship"
                                                                      class="errorMessage"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="email" id="email" maxlength="128"
                                                                       type="text" class="form-control input-md"
                                                                       placeholder="Email"/>
                                                                <!--                                              <errors path="email" class="errorMessage" /> -->
                                                                <span id="info_email" class="errorMessage"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" id="dateFrom" name="dateFrom"
                                                                           placeholder="Date of Refrence"
                                                                           class="datepicker-12 pp_text "/>
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                    <!--                                               <errors path="dateFrom" class="errorMessage" /> -->
                                                                    <span id="info_fromdate"
                                                                          class="errorMessage"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    <input type="text" name="dateTo" id="dateTo"
                                                                           placeholder="Date of Refrence"
                                                                           class="datepicker-12 pp_text "/>
                                                                    <span class="input-group-addon addon-custom"><span
                                                                            class="fa fa-calendar"></span></span>
                                                                    <%--                                                <errors path="dateTo" class="errorMessage" /> --%>
                                                                    <span id="info_todate"
                                                                          class="errorMessage">${errDate}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                            <textarea class="pp_textarea_height" placeholder="Additional Comments"
                                                      name="coveringNote" maxlength="128"
                                                      style="width: 100%;"></textarea>
                                                                <span id="info_stmt" class="errorMessage"></span>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="update" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Contact Details Modal
                ================================================== -->
                <div class="modal fade" id="Con_Det_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Contact Details</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid" id="form-contact">
                                    <form id="userForm" action="" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="errorText"></div>
                                                <fieldset>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input id="mfirstName" name="firstName" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="First Name" value="">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input id="mlastName" name="lastName" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Last Name" value="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="memail" name="primaryEmailAddress"
                                                                       type="text" class="form-control input-md"
                                                                       placeholder="Enter Email ID" value=""
                                                                       disabled="true">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input id="mtel" name="phone" type="text"
                                                                       class="form-control input-md"
                                                                       placeholder="Contact No.">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <button id="updateUser" name="btn-send"
                                                                        class="btn btn-confirm">Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!--editor Modal
                ================================================== -->
                <%-- <div class="modal fade" id="editor_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center" id="myModalLabel">Editor</h3>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form id="form-contact" action="">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <fieldset>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <iframe src="editor.html" width="100%" height="220" frameborder="0"></iframe>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button id="btn-confirm" name="btn-send" class="btn btn-confirm">Save</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        </div>
                                      </div>

                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --%>


                <script>

                    /* $('.search_icon').on('click', function(e)
                     {
                     $('.search_bar').toggleClass("collapsed pressed"); //you can list several class names
                     e.preventDefault();
                     });
                     $('.login_icon').on('click', function(e)
                     {
                     if (is_visible == false)
                     {
                     is_visible = true;
                     }
                     else if (is_visible == true)
                     {
                     is_visible = false;
                     }
                     $('.login_drop').toggleClass("collapsed pressed"); //you can list several class names
                     e.preventDefault();
                     });

                     $('.online_icon').on('click', function(e)
                     {
                     if (is_visible == false)
                     {
                     is_visible = true;
                     }
                     else if (is_visible == true)
                     {
                     is_visible = false;
                     }
                     $('.online_drop').toggleClass("collapsed pressed"); //you can list several class names
                     e.preventDefault();
                     });

                     $('.ben_icon').on('click', function(e)
                     {
                     if (is_visible == false)
                     {
                     is_visible = true;
                     }
                     else if (is_visible == true)
                     {
                     is_visible = false;
                     }
                     $('.ben_drop').toggleClass("collapsed pressed"); //you can list several class names
                     e.preventDefault();

                     }); */

                    $("[name='check-switch']").bootstrapSwitch();
                    $('#compToggle').click(function () {
                        $(".pstate").prop('disabled', false);
                    });

                    $('input[type="radio"]').click(function () {
                        if ($(this).attr("value") == "Permanent") {
                            $("#contract").hide();
                            $("#permanent").show();
                        }
                        if ($(this).attr("value") == "Contract") {
                            $("#permanent").hide();
                            $("#contract").show();
                        }
                        if ($(this).attr("value") == "Donotmind") {
                            $("#permanent, #contract").show();
                        }
                    });


                    var is_visible = false;
                    /*$(document).mouseup(function (e)
                     {
                     var container = $("#login_drop");
                     if(is_visible == true)
                     {
                     //alert('visible');
                     if (!container.is(e.target) // if the target of the click isn't the container...
                     && container.has(e.target).length === 0) // ... nor a descendant of the container
                     {
                     $('.login_drop').toggleClass("collapsed pressed"); //you can list several class names
                     e.preventDefault();
                     }
                     }
                     else
                     {
                     //alert('Not visible');
                     }
                     });*/

                </script>

                <script>
                    $(window).load(function () {
                        $("#rotator").rotator({
                            color: '#5192d2',
                            backgroundColor: '#67b5f2',
                            ending: $("#profilePercentage").val()
                        });
                    });
                </script>


                <script type="text/javascript">

                    $('#myTab').tabCollapse();
                </script>
                <script type="text/javascript">
                    $(function () {

                        $(".knob").knob({
                            /*change : function (value) {
                             //console.log("change : " + value);
                             },
                             release : function (value) {
                             console.log("release : " + value);
                             },
                             cancel : function () {
                             console.log("cancel : " + this.value);
                             },*/
                            draw: function () {

                                // "tron" case
                                if (this.$.data('skin') == 'tron') {

                                    var a = this.angle(this.cv)  // Angle
                                            , sa = this.startAngle          // Previous start angle
                                            , sat = this.startAngle         // Start angle
                                            , ea                            // Previous end angle
                                            , eat = sat + a                 // End angle
                                            , r = true;

                                    this.g.lineWidth = this.lineWidth;

                                    this.o.cursor
                                    && (sat = eat - 0.3)
                                    && (eat = eat + 0.3);

                                    if (this.o.displayPrevious) {
                                        ea = this.startAngle + this.angle(this.value);
                                        this.o.cursor
                                        && (sa = ea - 0.3)
                                        && (ea = ea + 0.3);
                                        this.g.beginPath();
                                        this.g.strokeStyle = this.previousColor;
                                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                                        this.g.stroke();
                                    }

                                    this.g.beginPath();
                                    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                                    this.g.stroke();

                                    this.g.lineWidth = 2;
                                    this.g.beginPath();
                                    this.g.strokeStyle = this.o.fgColor;
                                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                                    this.g.stroke();

                                    return false;
                                }
                            }
                        });

                        // Example of infinite knob, iPod click wheel
                        var v, up = 0, down = 0, i = 0
                                , $idir = $("div.idir")
                                , $ival = $("div.ival")
                                , incr = function () {
                            i++;
                            $idir.show().html("+").fadeOut();
                            $ival.html(i);
                        }
                                , decr = function () {
                            i--;
                            $idir.show().html("-").fadeOut();
                            $ival.html(i);
                        };
                        $("input.infinite").knob(
                                {
                                    min: 0
                                    , max: 20
                                    , stopper: false
                                    , change: function () {
                                    if (v > this.cv) {
                                        if (up) {
                                            decr();
                                            up = 0;
                                        } else {
                                            up = 1;
                                            down = 0;
                                        }
                                    } else {
                                        if (v < this.cv) {
                                            if (down) {
                                                incr();
                                                down = 0;
                                            } else {
                                                down = 1;
                                                up = 0;
                                            }
                                        }
                                    }
                                    v = this.cv;
                                }
                                });
                    });

                    $(document).ready(function () {
                        $("canvas").click(function () {
                            var value_data = $("#current_value").val()
                            if (value_data >= 0 && value_data < 25) {
// 	 alert($(".abhishek_value_fetch").text().trim());
// 	 alert($(".abhishek_value_fetch").text().trim()!= "Not available");
                                if ($(".abhishek_value_fetch").text().trim() != "Not available") {
                                    var status = "Not available";
                                    changeAvailiabilityStatus(status);
                                }
                            }
                            else if (value_data >= 25 && value_data < 50) {

                                if ($(".abhishek_value_fetch").text().trim() != "Available from") {
                                    var status = "Available from";
                                    changeAvailiabilityStatus(status);
                                }
                            }
                            else if (value_data >= 50 && value_data < 75) {

                                if ($(".abhishek_value_fetch").text().trim() != "Interested") {
                                    var status = "Interested";
                                    changeAvailiabilityStatus(status);
                                }
                            }
                            else if (value_data >= 75 && value_data <= 100) {

                                if ($(".abhishek_value_fetch").text().trim() != "Available Immediately") {
                                    var status = "Available Immediately";
                                    changeAvailiabilityStatus(status);
                                }
                            }
                        });

                        function changeAvailiabilityStatus(status) {
                            $.ajax({
                                url: "ajax/myStatus?status=" + status,
                                type: 'POST',
                                async: false,
                                success: function (response) {
                                    $(".abhishek_value_fetch").text(status);
                                },
                                error: function (error) {
                                }
                            });
                        }
                    });
                </script>

                <script type="text/javascript">
                    $(document).ready(function () {
                        /*  $("#sk-edit").click(function(){
                         $("#sk-qeb").slideToggle();
                         });
                         $("#rl-edit").click(function(){
                         $("#rl-qeb").slideToggle();
                         });
                         $("#tas-edit").click(function(){
                         $("#tas-qeb").slideToggle();
                         });
                         $("#sk-edit1").click(function(){
                         var input=$(this).parent(".border_bottom_right_one").find(".sk-qeb1");
                         $(input).slideToggle();
                         });
                         $("#rl-edit1").click(function(){
                         $("#rl-qeb1").slideToggle();
                         });
                         $("#tas-edit1").click(function(){
                         $("#tas-qeb1").slideToggle();
                         }); */


                    });

                    $('.note-toolbar').css('display', 'none');
                    $('.note-editor .note-statusbar').css('display', 'none');
                </script>
