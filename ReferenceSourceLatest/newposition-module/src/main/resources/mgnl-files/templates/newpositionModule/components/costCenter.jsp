<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="./resources/templating-kit/js/jquery.form-validator.js"></script>
<!-- <script src="./resources/templating-kit/js/jquery.validate.min.js"></script> -->
<!-- <script src="./resources/templating-kit/js/additional-methods.js"></script> -->
<script src="./resources/templating-kit/js/costCentre.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-4"><input type="hidden" id="perPageResult" value="${noOfResultPerPage}">

        </div>
        <div class="col-md-4"><span id='msg' class="sucessMessage">${message}</span></div>
        <div class="col-md-3 form-group">
            <button data-toggle="modal" data-target="#myModalAdd"
                    class="btn btn-rd-cor" name="btn-send" id="btn-rd-cor">Add
                CostCentre
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select1" class="form-control selectbox ">
                                        <option style="display: none;">Filter By</option>
                                        <option value="">Show all</option>
                                        <option value="CostCentreCode">CostCentre Code</option>
                                        <option value="CostCenterName">CostCentre Name</option>
                                        <option value="CostCentreOwner">Cost Center Owner</option>
                                        <option value="CostCenterLoc">Cost Center Location</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select2" class="form-control selectbox ">
                                        <option style="display: none;">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="col-md-8 ">
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input placeholder="Search" id="qSearch" class="form-control input-md" type="text">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="select3">
                            <option value="">Sort by</option>
                            <option value="CostCentreCode">CostCentreCode</option>
                            <option value="CostCentreName">CostCentreName</option>
                            <option value="CostCentreSystem">CostCentreSystem</option>
                            <option value="Location">Location</option>
                            <option value="Description">Description</option>
                        </select>
                    </div>
                    <div class="sort pull-left">
                            <span class="result-pop one"><c:if
                                    test="${fn:length(costCentreData) le 1}">${fn:length(costCentreData)} Result Found</c:if>
                            
                            <c:if test="${fn:length(costCentreData) gt 1 }"><span
                                    class='count'>${fn:length(costCentreData)}</span> Results Found</c:if>
                            </span>
                    </div>
                </div>
            </div>

            <div id="table1Div"></div>

            <div class="paging" id="header">
                <nav role="navigation">
                    <ul class="cd-pagination">
                        <li id="indexId1"><a class="disabled" href="#0" id="ancher1" onclick="moveToFirst()"><i
                                class="fa fa-angle-double-left"></i></a></li>
                        <li id="indexId2"><a class="disabled" href="#0" id="ancher2" onclick="moveOneStepLeft()"><i
                                class="fa fa-angle-left"></i></a></li>

                        <li id="indexId3"><a href="#0" id="ancher3"><i class="fa fa-angle-right"
                                                                       onclick="moveOneStepRight()"></i></a></li>
                        <li id="indexId4"><a href="#0" id="ancher4"><i class="fa fa-angle-double-right"
                                                                       onclick="moveToLast()"></i></a></li>
                    </ul>
                </nav>
            </div>


        </div>

    </div>

</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalAdd" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">x</span></button>
                <h3 id="myModalLabel" class="modal-title">Cost Center Details</h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="form-cost-centre-add" action="?" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">Cost Center Code :</label>
                                        <input type="text" id="Text6" class="form-control input-md"
                                               name="costCentreCode" maxlength="50" placeholder="Cost Centre Code"
                                               maxlength="15" data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre code"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="fname">Description</label>
                                        <input type="text" name="description" id="textarea" placeholder="Description"
                                               class="form-control form-ctrl" maxlength="45" data-validation="required"
                                               data-validation-error-msg="Enter Description"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="fname">Cost Centre Name</label>
                                        <input type="text" name="costCentreName" id="textarea"
                                               placeholder="Cost Centre Name" class="form-control form-ctrl"
                                               maxlength="45" data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Name"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="cjtitle">Location</label>
                                        <input type="text" name="location" class="form-control input-md"
                                               placeholder="Location" id="cjtitle" maxlength="45"
                                               data-validation="required" data-validation-error-msg="Enter Location"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="pemail">Cost Center Owner</label>
                                        <input type="text" class="form-control input-md" placeholder="Cost Center Owner"
                                               name="costCentreOwner" id="pemail" maxlength="45"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Owner"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="pemail">Cost Center System</label>
                                        <input type="text" class="form-control input-md"
                                               placeholder="Cost Center System" maxlength="45" name="costCentreSystem"
                                               id="" data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre System"/>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea rows="6" name="overview" id="textarea" placeholder="Enter Overview"
                                                  class="form-control form-ctrl" maxlength="200"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="file" id="uploadBtn" name="video" placeholder="browse file">
                                        <video id="addVideo" controls></video>
                                        <div id="v_message"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <!--   <div class="col-md-6">
                                          <input type="button" class="btn btn-confirm" name="btn-send" id="btn-confirm" value="Upload" />
                                      </div> -->
                                    <div class="col-md-6">
                                        <button class="btn btn-confirm" name="btn-send" id="btn-confirm"
                                                style="margin-left:120px">Apply
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cost Centre Id with video url -->

<c:forEach items="${costCentreData}" var="ccData1" varStatus="index">
    <input id="url${ccData1.costCentreId}" value="${ccData1.videoUrl}" type="hidden">
</c:forEach>

<!-- cost Centre Id with video url -->
<%-- <c:forEach items="${costCentreData}" var="ccData" varStatus="index"> --%>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEdit" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">x</span></button>
                <h3 id="myModalLabel" class="modal-title">Cost Center Details</h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="form-cost-centre-edit" action="?" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="costCentreId" readonly="readonly" value="${ccData.costCentreId}"/>
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">Cost Center Code :</label>
                                        <input type="text" name="costCentreCode" readonly="readonly"
                                               value="${ccData.costCentreCode}" maxlength="50" readonly="readonly"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Code"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="fname">Description</label>
                                        <input type="text" name="description" id="textarea" placeholder="Description"
                                               value="${ccData.description}" class="form-control form-ctrl"
                                               maxlength="45" readonly="readonly" data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Desciption">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="fname">Cost Centre Name</label>
                                        <input type="text" name="costCentreName" id="textarea"
                                               value="${ccData.costCentreName}" maxlength="45"
                                               placeholder="Cost Centre Name" class="form-control form-ctrl"
                                               maxlength="40" readonly="readonly" data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Name"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="cjtitle">Location</label>
                                        <input type="text" name="location" class="form-control input-md"
                                               placeholder="Location" maxlength="45" id="cjtitle"
                                               value="${ccData.costCentreLocation}" readonly="readonly"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Location">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="pemail">Cost Center Owner</label>
                                        <input type="text" class="form-control input-md" placeholder="Cost Center Owner"
                                               maxlength="45" name="costCentreOwner" id="pemail"
                                               value="${ccData.costCentreOwner}" readonly="readonly"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre Owner">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="pemail">Cost Center System</label>
                                        <input type="text" class="form-control input-md"
                                               placeholder="Cost Center System" maxlength="45" name="costCentreSystem"
                                               id="pemail" value="${ccData.costCentreSystem}" readonly="readonly"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Cost Centre System">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea rows="6" name="overview" id="textarea" placeholder="Enter Overview"
                                                  class="form-control form-ctrl" maxlength="200"
                                                  readonly="readonly">${ccData.costCentreDoc.comments}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="file" name="video" id="editFile" placeholder="browse file"
                                               disabled="disabled">
                                        <video id="editVideo" controls>
                                            <source src="${ccData.videoUrl}"/>
                                        </video>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="button" class="btn btn-confirm " name="btn-send" id="btn-edit-pp"
                                               value="Edit"/>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="button" class="btn btn-confirm" name="btn-send"
                                               id="btn-confirm-submit" value="Apply">
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%--     </c:forEach> --%>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="./resources/templating-kit/js/jquery.plugin.js"></script> -->


<script type="text/javascript">

    $.validate();
    (function localFileVideoPlayerInit(win) {
        var URL = win.URL || win.webkitURL,
                displayMessage = (function displayMessageInit() {
                    var node = document.querySelector('#v_message');

                    return function displayMessage(message, isError) {
                        node.innerHTML = message;
                        node.className = isError ? 'error' : 'info';
                    };
                }()),
                playSelectedFile = function playSelectedFileInit(event) {
                    var file = this.files[0];

                    var type = file.type;

                    var videoNode = document.querySelector('#addVideo');

                    var canPlay = videoNode.canPlayType(type);

                    canPlay = (canPlay === '' ? 'no' : canPlay);

                    var message = 'Can play type "' + type + '": ' + canPlay;

                    var isError = canPlay === 'no';

                    displayMessage(message, isError);

                    if (isError) {
                        return;
                    }

                    var fileURL = URL.createObjectURL(file);

                    videoNode.src = fileURL;
                },
                inputNode = document.querySelector('#uploadBtn');

        if (!URL) {
            displayMessage('Your browser is not ' +
                    '<a href="http://caniuse.com/bloburls">supported</a>!', true);

            return;
        }

        inputNode.addEventListener('change', playSelectedFile, false);
    }(window));
</script>
