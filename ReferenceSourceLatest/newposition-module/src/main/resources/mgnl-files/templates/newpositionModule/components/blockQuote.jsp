<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
           prefix="cmsfn" %>
<blockquote class="blockquote">
    ${cmsfn:decode(content).content}
</blockquote>