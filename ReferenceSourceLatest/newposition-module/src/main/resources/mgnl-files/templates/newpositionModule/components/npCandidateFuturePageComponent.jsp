<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<script src="./resources/templating-kit/js/myFuturePage.js"></script>
<script src="./resources/templating-kit/js/summernote.min.js"></script>
<script src="./resources/templating-kit/js/jquery-knob.js"></script>
<div class="myFuturePage">
    <div class="container">
        <div class="col-md-11 centered bottom_50 tpf_space">
            <div class="row mgb20">
                <div class="col-md-2">
                    <a href="javascript:void(0)" class="Link_nordc Link_norac-a">Me</a>
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)" class="Link_nordc Link_norac-a">My Career History</a>
                </div>
                <div class="col-md-3">
                    <a href="javascript:void(0)" class="Link_nordc Link_norac-a">My Education</a>
                </div>
                <div class="col-md-2">
                    <a href="javascript:void(0)" class="Link_nordc Link_norac-a">My Sponsors</a>
                </div>
                <div class="col-md-2">
                    <a href="javascript:void(0)" class="Link_norac Link_norac-a">My Future</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <center><label class="errorMessage" id="errorMsg"></label></center>
            </div>
        </div>

        <div class="col-md-9 centered">
            <form:form action="?" commandName="myFutureDetails" method="post" enctype="multipart/form-data">
                <div class="row mgb20">
                    <div class="col-md-3 text-center">
                        <div class="some_graphicss">
                            <label class="pp_label rotate_value">Not Available</label>
                            <div style="">
                                <canvas width="0" height="100px"></canvas>
                                <input style="width: 79px; height: 50px; position: absolute; vertical-align: middle; margin-top: 50px; margin-left: -114px; border: 0px none; background: transparent none repeat scroll 0% 0%; font: bold 30px Arial; text-align: center; color: rgb(34, 34, 34); padding: 0px; display:none;"
                                       class="knob" data-width="170" data-cursor="true" data-fgcolor="#222222"
                                       data-thickness=".3" value="21" id="current_value">
                            </div>
                            <form:hidden path="availableStatus" value="Not available"/>
                        </div>
                    </div>
                    <div class="col-md-9" id="personalTextStatement">
                        <label class="pp_label">Personal Statement</label>
                        <form:textarea path="personalStatement" class="pp_textarea ps_textarea"
                                       placeholder="Personal Statement" cols="40" rows="6" maxlength='45'/>
                        <span id="pstmt_error" class="errorMessage"></span>
                    </div>
                </div>


                <div class="row mgb20">
                    <div class="col-md-12">
                        <label class="pp_label">Do you expect you next role to be Permanent or Contract?</label>
                        <form:select path="type" id="Permanent_select" class="pp_select selectbox2">
                            <form:option value="Donotmind">Don’t mind</form:option>
                            <form:option value="Contract">Contract</form:option>
                            <form:option value="Permanent">Permanent</form:option>
                        </form:select>
                        <form:errors path="type" class="errorMessage"></form:errors>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="permanent_row">
                        <div class="col-md-4 ">
                            <div class="vertical">
                                <label class="pp_label">Permanent</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="pp_label">Compensation</label>
                            <form:input path="compensation" type="text" class="pp_text" placeholder="Compensation"
                                        maxlength="10"/>
                            <form:errors path="compensation" class="errorMessage"></form:errors>
                            <span id="compensation_error" class="errorMessage"></span>

                        </div>
                        <div class="col-md-4">
                            <label class="pp_label">Bonus</label>
                            <form:input path="bonus" type="text" class="pp_text" placeholder="Bonus" maxlength="10"/>
                            <form:errors path="bonus" class="errorMessage"></form:errors>
                            <span id="bonus_error" class="errorMessage"></span>
                        </div>
                    </div>

                </div>
                <div class="row mgb20">
                    <div class="contract_row">
                        <div class="col-md-4">
                            <div class="vertical">
                                <label class="pp_label">Contract</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="pp_label">Day Rate</label>
                            <form:input path="dayRate" type="text" class="pp_text" placeholder="Day Rate"
                                        maxlength="10"/>
                            <form:errors path="dayRate" class="errorMessage"></form:errors>
                            <span id="dayRate_error" class="errorMessage"></span>
                        </div>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-12">
                        <div class="skills_box">
                            <div class="hear">
                                Future
                                Skills<span>(Please highlight your skills that you want to use in next role)</span>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick list_skills">
                                        <c:forEach var="skill" items="${futureSkills}">
                                            <c:if test="${skill.preferfutureSkill eq 1}">
                                                <a class="blue_link"
                                                   href="javascript:void(0)">${skill.npSkill.skillName} <i
                                                        class="fa fa-times"></i></a>
                                            </c:if>
                                            <c:if test="${skill.preferfutureSkill eq 0}">
                                                <a class="" href="javascript:void(0)">${skill.npSkill.skillName} <i
                                                        class="fa fa-times"></i></a>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered ui-widget">
                                    <input id="skills_input" type="text" class="pp_text"
                                           placeholder="Enter Skills you want to highlight in future (e.g.Java )"> <span
                                        class='errSkill errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-12">
                        <div class="skills_box">
                            <div class="hear">
                                Future Roles<span>(Please highlight your roles that you want to use in next role)</span>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick list_roles">
                                        <c:forEach var="role" items="${futureRoles}">
                                            <c:if test="${role.futurePreferedRole eq 1}">
                                                <a class="blue_link"
                                                   href="javascript:void(0)">${role.npEmploymentRoles.roleName} <i
                                                        class="fa fa-times"></i></a>
                                            </c:if>
                                            <c:if test="${role.futurePreferedRole eq 0}">
                                                <a class="" href="javascript:void(0)">${role.npEmploymentRoles.roleName}
                                                    <i
                                                            class="fa fa-times"></i></a>
                                            </c:if>

                                        </c:forEach>

                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered ">
                                    <input type="text" class="pp_text" id="role_input"
                                           placeholder="Enter future roles you want to highlight (e.g. Manager or Developer )"> <span
                                        class='errRole errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row mgb20">
                    <div class="col-md-12">
                        <div class="skills_box">
                            <div class="hear">
                                Future Tools & System<span>(Please highlight your tools & systems that you want to use in next role)</span>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick list_tools">
                                        <c:forEach var="tool" items="${futureTools}">
                                            <c:if test="${tool.futurePreferedTool eq 1}">
                                                <a class="blue_link" href="javascript:void(0)">${tool.npTools.toolName}
                                                    <i
                                                            class="fa fa-times"></i></a>
                                            </c:if>
                                            <c:if test="${tool.futurePreferedTool eq 0}">
                                                <a class="" href="javascript:void(0)">${tool.npTools.toolName} <i
                                                        class="fa fa-times"></i></a>
                                            </c:if>

                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered ui-widget">
                                    <input type="text" class="pp_text" id="tool_input"
                                           placeholder="Enter prefer tools you want to work on (e.g. Groovy or Selenium )"> <span
                                        class='errTool errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-6">
                        <div class="skills_box">
                            <div class="hear">Future Company</div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick future_comp">
                                        <c:forEach var="preferCompany" items="${futurePreferCompany}">
                                            <a class="blue_link"
                                               href="javascript:void(0)">${preferCompany.npEmployer.name} <i
                                                    class="fa fa-times"></i></a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered">
                                    <input type="text" class="pp_text" id="input_FC"
                                           placeholder="Enter comma seperate list of Future Companies">
                                    <span class='errFuture_comp errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="skills_box">
                            <div class="hear">Excluded company</div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick exclude_comp">
                                        <c:forEach var="excludeCompany" items="${futureExcludeCompany}">
                                            <a class="blue_link"
                                               href="javascript:void(0)">${excludeCompany.npEmployer.name} <i
                                                    class="fa fa-times"></i></a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered">
                                    <input type="text" class="pp_text" id="input_ExC"
                                           placeholder="Enter comma seperate list of Excluded Companies">
                                    <span class='errExcld_comp errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-12">
                        <div class="skills_box">
                            <div class="hear">
                                Eligible<span>(Please tell us which countries you are currently eligible to work)</span>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-12">
                                    <div class="links_js links_jsonlyclick list_countries"
                                         style="text-transform: capitalize;">
                                        <c:forEach var="eligibility" items="${preferCountries}">
                                            <a class="blue_link" href="javascript:void(0)">${eligibility}<i
                                                    class="fa fa-times"></i></a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="row mgb20">
                                <div class="col-md-11 centered ui-widget">
                                    <input type="text" class="pp_text" id="Eligblity_input"
                                           placeholder="Enter prefer Countries you want to work in (e.g. Paris"> <span
                                        class='errEligibility errorMessage'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row uploadcv mgb20">
                    <div class="col-md-9 centered">
                        <div class="form-group col-md-3" style="padding-right: 0;">
                            <span class=" btn-file pp_button"> Browse <form:input path="cvFile" id="uploadBtn"
                                                                                  type="file" class=""/>
                            </span>
                        </div>
                        <div class="form-group col-md-9" style="padding-left: 0; padding-right: 5px;">
                            <label for="" class="pp_label">Please upload your CV</label> <input id="uploadFile"
                                                                                                class="pp_text"
                                                                                                placeholder="File location"
                                                                                                readonly="readonly"/>
                        </div>

                    </div>
                </div>

                <div class="row mgb20 uploadcv">
                    <div class="col-md-9 centered">
                        <div class="form-group col-md-3" style="padding-right: 0;">

                            <span class="  btn-file pp_button"> Browse <form:input path="photo" id="uploadBtn1"
                                                                                   type="file"
                                                                                   class="" accept=".png,.jpg,.gif"/>
                            </span>
                        </div>
                        <div class="form-group col-md-9" style="padding-left: 0; padding-right:5px;">
                            <label for="" class="pp_label">Please upload your photo</label> <input id="uploadFile1"
                                                                                                   class="pp_text"
                                                                                                   placeholder="File location"
                                                                                                   readonly="readonly"/>
                        </div>

                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-12" id="coveringLetterText">
                        <label class="pp_label">Covering Letter Template</label>
                        <form:textarea path="coverLetter" class="pp_textarea" placeholder="Covering Letter"
                                       maxlength='100'/>
                        <span id="cletter_error" class="errorMessage"></span>
                    </div>
                </div>

                <div class="row mgb20">
                    <div class="col-md-12 text-center">
                        <input type="button" id="finish" class="pp_button" value="Finish">
                    </div>
                </div>

            </form:form>
        </div>
    </div>
</div>
<div class="browser_prev">
    <a href="mySponsor"><img src="./resources/templating-kit/images/blank.png"></a>
</div>


<script type="text/javascript">
    document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
    };
    document.getElementById("uploadBtn1").onchange = function () {
        document.getElementById("uploadFile1").value = this.value;
    };
    $(document).ready(function (e) {

        $(".skills_box .hear").click(function () {
            $(this).toggleClass("hear_plus");
            $(this).parent().toggleClass("skills_boxCL")
        });


        $("#Eligblity_input").keyup(function () {
            $("#Eligblity_data").append("abhishek");
        });

        $(".links_jsonlyclick a").click(function () {
            $(this).toggleClass("blue_link");
        });

        $("#Permanent_select").change(function () {
            $("select option:selected").each(function () {
                if ($(this).attr("value") == "Permanent") {
                    $(".permanent_row").show();
                    $(".contract_row,.don_row").hide();
                }
                if ($(this).attr("value") == "Contract") {
                    $(".contract_row").show();
                    $(".permanent_row,.don_row").hide();
                }
                if ($(this).attr("value") == "Don") {
                    $(".don_row").show();
                    $(".permanent_row,.contract_row").hide();
                }
            });
        }).change();

    });

    $(function () {

        $(".knob").knob({
            /*change : function (value) {
             //console.log("change : " + value);
             },
             release : function (value) {
             console.log("release : " + value);
             },
             cancel : function () {
             console.log("cancel : " + this.value);
             },*/
            draw: function () {

                // "tron" case
                if (this.$.data('skin') == 'tron') {

                    var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                    this.g.lineWidth = this.lineWidth;

                    this.o.cursor
                    && (sat = eat - 0.3)
                    && (eat = eat + 0.3);

                    if (this.o.displayPrevious) {
                        ea = this.startAngle + this.angle(this.value);
                        this.o.cursor
                        && (sa = ea - 0.3)
                        && (ea = ea + 0.3);
                        this.g.beginPath();
                        this.g.strokeStyle = this.previousColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                        this.g.stroke();
                    }

                    this.g.beginPath();
                    this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                    this.g.stroke();

                    this.g.lineWidth = 2;
                    this.g.beginPath();
                    this.g.strokeStyle = this.o.fgColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                    this.g.stroke();

                    return false;
                }
            }
        });

        // Example of infinite knob, iPod click wheel
        var v, up = 0, down = 0, i = 0
                , $idir = $("div.idir")
                , $ival = $("div.ival")
                , incr = function () {
            i++;
            $idir.show().html("+").fadeOut();
            $ival.html(i);
        }
                , decr = function () {
            i--;
            $idir.show().html("-").fadeOut();
            $ival.html(i);
        };
        $("input.infinite").knob(
                {
                    min: 0
                    , max: 20
                    , stopper: false
                    , change: function () {
                    if (v > this.cv) {
                        if (up) {
                            decr();
                            up = 0;
                        } else {
                            up = 1;
                            down = 0;
                        }
                    } else {
                        if (v < this.cv) {
                            if (down) {
                                incr();
                                down = 0;
                            } else {
                                down = 1;
                                up = 0;
                            }
                        }
                    }
                    v = this.cv;
                }
                });
    });


    $(document).ready(function () {
        $("canvas").click(function () {
            var value_data = $("#current_value").val();
            if (value_data >= 0 && value_data < 25) {
                $(".rotate_value").text("Not available");
                $('input[name=availableStatus]').val("Not available");
            }
            else if (value_data >= 25 && value_data < 50) {
                $(".rotate_value").text("Available from");
                $('input[name=availableStatus]').val("Available from");
            }
            else if (value_data >= 50 && value_data < 75) {
                $(".rotate_value").text("Interested");
                $('input[name=availableStatus]').val("Interested");
            }
            else if (value_data >= 75 && value_data <= 100) {
                $(".rotate_value").text("Available Immediately");
                $('input[name=availableStatus]').val("Available Immediately");
            }
        });

    });
</script>

