<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>


<div class="accordion-container">
    <a href="#" class="accordion-toggle">${content.question}<span class="toggle-icon"><i class="fa fa-plus"></i></span></a>
    <div class="accordion-content">
        <p>${content.answer}</p>
    </div>
</div>