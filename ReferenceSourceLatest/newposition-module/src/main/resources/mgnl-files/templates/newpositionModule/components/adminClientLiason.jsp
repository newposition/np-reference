<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" href="./resources/templating-kit//css/style-vin.css"></link>

<script src="./resources/templating-kit/js/adminClientLiason.js"></script>


<div class="container-fluid adminclient">
    <div class="pd-head">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="circle-outer pull-left">
                    <div class="sm_circle">
                            <span class="sm_circle_center">
                                <img width="50px" alt="user" src="./resources/templating-kit/images/user_ico.png">
                            </span>
                    </div>
                </div>
                <div class="heading-title pull-left">
                    <h2>Admin User</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<c:set var="active" value="class='active'"/>
<c:set var="disableClick" value="href='javascript:void(0);'"/>
<c:set var="enableLiason" value="href='#cl'"/>
<c:set var="enableAnalyst" value="href='#sa'"/>
<c:set var="enableSupport" value="href='#cs'"/>

<div class="container-fluid adminclient">

    <div class="tab_collapse_master">
        <ul id="myTab" class="nav nav-tabs hidden-xs">

            <li <sec:authorize access="hasRole('ROLE_CLIENT_LIASON')">${active}</sec:authorize>><a  <sec:authorize
                    access="hasRole('ROLE_CLIENT_LIASON')">${enableLiason}</sec:authorize>>Client Liason</a></li>
            <li <sec:authorize access="hasRole('ROLE_SKILLS_ANALYST')">${active}</sec:authorize>><a  <sec:authorize
                    access="hasRole('ROLE_SKILLS_ANALYST')">${enableAnalyst}</sec:authorize>>Skill Analyst</a></li>
            <li <sec:authorize access="hasAnyRole('ROLE_NP_USERSUPPORT')">${active}</sec:authorize>><a <sec:authorize
                    access="hasAnyRole('ROLE_NP_USERSUPPORT','ROLE_CLIENT_SUPPORT')">${enableSupport}</sec:authorize>>Np
                User Support</a></li>
        </ul>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_LIASON')">active</sec:authorize>"
                         id="cl">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                               <a class="" href="clientUserManagement"> <img
                                                                       src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="clientUserManagement">View All
                                                        Clients</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                              <a class="" href="add-client"><img
                                                                      src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="add-client">Add Client</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a class="image_white_text2" href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                                <span class="notify">3</span>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Inbox</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
					                                        <span class="notify_parent">
					                                            <a class="image_white_text2" href="referenceList"><img
                                                                        src="${pageContext.request.contextPath}/docroot/img/icon1.png">

                                                                    <span class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>
					                                            
					                                        </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Quick Links</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-15 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Communication
                                                        Center</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-15 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico14.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Search
                                                        Job Board</a></div>
                                                </div>
                                            </div>

                                            <div class="col-md-15 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a class="" href="#"><img
                                                                        src="./resources/templating-kit/images/ico15.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Contact
                                                        Support</a></div>
                                                </div>
                                            </div>

                                            <div class="col-md-15 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a class="" href="#"><img
                                                                        src="./resources/templating-kit/images/ico18.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="FAQ"><a
                                                            class="image_white_text2" href="#">FAQ</a></div>
                                                </div>
                                            </div>

                                            <div class="col-md-15 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico17.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Knowledge
                                                        Center</a></div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                    <div class="tab-pane fade in <sec:authorize access="hasRole('ROLE_SKILLS_ANALYST')">active</sec:authorize>"
                         id="sa">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                                <span class="notify">5</span>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Inbox</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
					                                        <span class="notify_parent">
					                                            <a class="image_white_text2" href="referenceList"><img
                                                                        src="${pageContext.request.contextPath}/docroot/img/icon1.png">

                                                                    <span class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>
					                                            
					                                        </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Reports</h3>
                                    </div>
                                    <div id="carousel-generic2" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic2" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-generic2" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Employee</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                               <a href="skillManagement"> <img
                                                                       src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="skillManagement">Skills</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="roleManagement"><img
                                                                        src="./resources/templating-kit/images/ico14.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="roleManagement">Roles</a></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="toolManagement"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="toolManagement">Tools</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="blue-panel">

                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Industry</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="industryManagement"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="industryManagement">Industry</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text">&nbsp;</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="blue-panel">

                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Education</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="qualificationManagement"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="qualificationManagement">Qualification</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="certificationManagement"><img
                                                                        src="./resources/templating-kit/images/ico14.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="certificationManagement">Certification</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="institutionManagement"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="institutionManagement">Institution</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">Communication
                                                Center</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">Search
                                                Jobboard</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">Contact
                                                Support</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">Knowledge
                                                Center</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="tab-pane fade in <sec:authorize access="hasAnyRole('ROLE_NP_USERSUPPORT')">active</sec:authorize>"
                         id="cs">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a class="image_white_text2" href="add-user"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a class="image_white_text2"
                                                                                   href="add-user">Add User</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="clientSupportImpersonateUser"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="clientSupportImpersonateUser">Impersonate</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="clientSupportUserManagement"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="clientSupportUserManagement">View
                                                        All Users</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
					                                        <span class="notify_parent">
					                                            <a class="image_white_text2" href="referenceList"><img
                                                                        src="${pageContext.request.contextPath}/docroot/img/icon1.png">

                                                                    <span class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>
					                                            
					                                        </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                                 <div class="col-md-3"> -->
                                            <!--                                                     <div class="black_ico_box"> -->
                                            <!--                                                         <div class="black_ico"> -->
                                            <!--                                                             <span class="notify_parent"> -->
                                            <%--                                                                 <img src="./resources/templating-kit/images/ico1.png"> --%>
                                            <!--                                                             </span> -->
                                            <!--                                                         </div> -->
                                            <!--                                                         <div class="black_ico_text">Activate</div> -->
                                            <!--                                                     </div> -->
                                            <!--                                                 </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Reports</h3>
                                    </div>
                                    <div id="carousel-generic3" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">255</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic3" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-generic3" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Positions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Approval</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Adverts</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">State</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">MI</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Candidate</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Reports</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Client Reports</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Communication Center</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Earth Job Board</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Contact Support</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Knowledge Center</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>


                    <div class="tab-pane fade in" id="interviewer">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Interviewer</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Inbox</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                       <a href="#"> <img
                                                               src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Search Jobs</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                        <span class="notify">6</span>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Applications</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                        <span class="notify">2</span>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Interviews</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                        <span class="notify">3</span>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">My History</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                        <span class="notify">1</span>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Favourite Jobs</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Communication Center</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Search Jobboard</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Contact Support</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Knowledge Center</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="tab-pane fade in" id="manager">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                               <a href="#"> <img
                                                                       src="./resources/templating-kit/images/ico1.png"></a>
                                                                <span class="notify notify-plus"></span>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Create Positions</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Create Shortlist</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Inbox</a></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                                <span class="notify notify-plus"></span>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Interviewers</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Approved</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">offer</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Positions</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Interviews</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Reservations</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Preferences</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Filled Positions</a></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">On</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">My History</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico1.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Communication Center</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico14.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Search Job Board</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico15.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Contact Support</a></div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico18.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <a href="#"><img
                                                                src="./resources/templating-kit/images/ico17.png"></a>
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a href="#">Knowledge Center</a></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div class="tab-pane fade in" id="support">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">User Management</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Correct Workflow</a></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Role Management</a></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
                                                            <span class="notify_parent">
                                                                <a href="#"><img
                                                                        src="./resources/templating-kit/images/ico1.png"></a>
                                                            </span>
                                                    </div>
                                                    <div class="black_ico_text"><a href="#">Inbox</a></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                         data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc"><span class="crc-m">255<br>JCR</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                        </a>
                                    </div> <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico1.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Impersonate User</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico14.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Positions</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico15.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Office Locations</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico18.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico17.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">My History</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico1.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Communication Center</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico14.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Search Job Board</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico15.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Contact Support</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico18.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text"><a class="image_white_text2" href="FAQ">FAQ</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                    <span class="notify_parent">
                                                        <img src="./resources/templating-kit/images/ico17.png">
                                                    </span>
                                            </div>
                                            <div class="black_ico_text">Knowledge Center</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>