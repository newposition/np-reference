<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="np" uri="newpositionTags" %>

<link rel="stylesheet"
      href="./resources/templating-kit/css/style-vin.css"></link>
<script src="./resources/templating-kit/js/clientUserManagement.js"></script>
<div class="container usermanag">
    <div class="row">

        <div class="col-md-12">
            <div class="row centerText">
             <span class="sucessMessage">
             <c:if test='${param["showAddMsg"]}'>A new client has been added successfully </c:if>
             <c:if test='${param["showEditMsg"]}'>Client has been edited successfully</c:if>
             <c:if test='${param["showDeleteMsg"]}'>Client has been deleted successfully</c:if>
             <c:if test='${param["showStatusMsg"]}'>Client Status has been updated Successfully</c:if>
             </span>
            </div>
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 col-md-2 pull-right">
                                    <a href="add-client" class="btn btn-rd-cor pull-right">Add
                                        Client</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Asp" id="qSearch"
                                           class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">


                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="sortNpClients">
                            <option value="">Sort by</option>
                            <option value="firstName"
                                    <c:if test="${sortBy eq 'firstName'}">selected</c:if>>Client
                                Name
                            </option>
                            <option value="emailId"
                                    <c:if test="${sortBy eq 'emailId'}">selected</c:if>>Email
                            </option>
                            <option value="sector"
                                    <c:if test="${sortBy eq 'sector'}">selected</c:if>>Sector
                            </option>
                            <option value="status"
                                    <c:if test="${sortBy eq 'status'}">selected</c:if>>Status
                            </option>
                        </select>
                    </div>
                    <div class="sort pull-left">
						<span class="result-pop">
						<c:if test="${npClientCount eq 0}"> No Results found</c:if>
						<c:if test="${npClientCount eq 1}"> 1 Result found</c:if>
						<c:if test="${npClientCount gt 1}"> ${npClientCount} Results found</c:if></span>
                    </div>
                </div>
            </div>

            <div class="imp-user-list">
                <div class="row no-pad">
                    <div class="tbl_panel">
                        <div class="row no-pad">
                            <div class="col-xs-2 col-md-1">
                                <div class="tbl-user">
                                    <strong>Client Id</strong>
                                </div>
                            </div>

                            <div class="col-xs-3 col-md-2">
                                <div class="tbl-user">
                                    <strong>Client Name</strong>
                                </div>
                            </div>

                            <div class="col-xs-3 col-md-3">
                                <div class="tbl-user">
                                    <strong>Principal Email Address</strong>
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-2">
                                <div class="tbl-user">
                                    <strong>Sector</strong>
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-2">
                                <div class="tbl-name">
                                    <strong>Status</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row no-pad">
                    <div class="tbl_panel">
                        <c:forEach items="${clients}" var="npClient">
                            <div class="row no-pad">
                                <div class="col-xs-2 col-md-1">
                                    <div class="tbl-user">
                                        <span>${npClient.id}</span>
                                    </div>
                                </div>
                                <!--                                 <div class="col-xs-3 col-md-1"> -->
                                <!--                                     <div class="tbl-user"> -->
                                <!--                                         UserSupport -->
                                <!--                                     </div> -->
                                <!--                                 </div> -->

                                <div class="col-xs-3 col-md-2">
                                    <div class="tbl-user clientUserName">${npClient.companyName}</div>
                                </div>


                                <div class="col-xs-3 col-md-3">
                                    <div class="tbl-user">${npClient.principleContactEmail}</div>
                                </div>

                                <div class="col-xs-3 col-md-2">
                                    <div class="tbl-user">${npClient.sector.sectorName}</div>
                                </div>

                                <div class="col-xs-3 col-md-2">
                                    <div class="tbl-name">${npClient.clientStatus.status}</div>
                                    <input type="hidden" id="stsId" value="${npClient.clientStatus.id}">
                                </div>
                                <div class="col-xs-12 col-md-2">

                                    <div class="tbl-mo-button  pull-left">

                                        <a id="${npClient.clientStatus.id}" data-target="#myModalSuspend"
                                           data-toggle="modal"
                                           class=" btn suspendClient">
                                            <img src="./resources/templating-kit/img/changeState.png"/>

                                        </a>

                                    </div>
                                    <div class="tbl-mo-button pad   pull-left">
                                        <a href="edit-client?clientId=${npClient.id}" class=" btn edit-icon">
                                            <!-- 												<span class="fa fa-pencil-square-o"></span> Edit -->
                                            <img src="./resources/templating-kit/img/edit-icon.png"/>
                                        </a>
                                    </div>
                                    <div class="tbl-mo-button mglft-rgt tbl-lst-button pull-left">
                                        <a id="btn-rd-cor" data-target="#myModalDelete"
                                           data-toggle="modal"
                                           class="btn tbl-trash-button deleteClient">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        <%--                             <c:forEach items="${npUserTypes}" var="npUserType"> --%>
                        <!--                             <div class="col-xs-8 col-md-2"> -->
                        <!--                                     <div class="tbl-name userType"> -->
                        <%--                                            ${npUserTypes.UserType} --%>
                        <!--                                     </div> -->
                        <!--                                 </div> -->
                        <%--                             </c:forEach> --%>

                    </div>
                </div>


            </div>

            <c:if test="${npClientTotalPages ge 1}">
                <div class="paging">
                    <nav role="navigation">
                        <ul class="cd-pagination">
                            <c:if test="${npClientTotalPages ge 1 && currentPageNumber > 0}">
                                <li><a class="fristPagination" href="?pageNumber=0"><i
                                        class="fa fa-angle-double-left"></i></a></li>
                                <li><a class="prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
                            </c:if>
                            <c:forEach begin="0" end="${npClientTotalPages-1}" varStatus="index">
                                <li>
                                    <c:choose>
                                        <c:when test="${currentPageNumber eq index.index}">
                                            <a href="?pageNumber=${index.index}"
                                               class="current currentPageNo">${index.index + 1}</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="?pageNumber=${index.index}"
                                               class="pagination">${index.index + 1}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </li>
                            </c:forEach>
                            <c:if test="${npClientTotalPages ge 1 && currentPageNumber < npClientTotalPages-1}">
                                <li><a class="next" href="#0"><i class="fa fa-angle-right"></i></a></li>
                                <li><a class="lastPagination" href="${npClientTotalPages-1}"><i
                                        class="fa fa-angle-double-right"></i></a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </c:if>

        </div>
    </div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
     tabindex="-1" id="myModalSuspend" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button aria-label="Close" data-dismiss="modal" class="close"
                        type="button">
                    <span aria-hidden="true">x</span>
                </button>


                <h3 id="myModalLabel" class="modal-title">Changing State of <span id="clientName"></span></h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid reasonPopup">
                    <form:form id="form-contact" action="?" class="suspendClientForm"
                               commandName="reasonForm" method="POST">
                        <%--                         <form action="" id="form-contact"> --%>
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label error" for="">Please enter the
                                            below mandatory details</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="false" name="suspendOrDelete"/> <input
                                type="hidden" value="" name="userId"/>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="select state">Select state</label>
                                        <form:select path="statusId"
                                                     class="form-control selectbox">
                                            <np:listStatusOptions/>
                                        </form:select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="reasonCode">Reason
                                            Code</label>

                                        <form:select path="reasonCode" class="form-control selectbox "
                                                     id="select">
                                            <c:forEach items="${npUsersReasons}" var="npUsersReason">
                                                <form:option
                                                        value="${npUsersReason.id}">${npUsersReason.reasonCode}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="reasonLines">Additional
                                            Comments</label>
                                        <form:textarea path="reasonLines" placeholder="Overview"
                                                       rows="6" class="form-control form-ctrl" maxlength="300"/>
                                    </div>
                                </div>
                            </div>

                            <hr>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button class="btn btn-confirm" name="btn-send"
                                                id="btn-confirm">Submit
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
     tabindex="-1" id="myModalDelete" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close"
                        type="button">
                    <span aria-hidden="true">x</span>
                </button>
                <h3 id="myModalLabel" class="modal-title">Delete Client</h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid reasonPopup">
                    <form:form id="form-delete" action="?" class="suspendClientForm"
                               commandName="reasonForm" method="POST">
                        <%--                         <form action="" id="form-contact"> --%>
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">Please enter the
                                            below mandatory details</label>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" value="true" name="suspendOrDelete"/> <input
                                type="hidden" value="" name="userId"/>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="reasonCode">Reason
                                            Code</label>

                                        <form:select path="reasonCode" class="form-control selectbox "
                                                     id="select">
                                            <c:forEach items="${npUsersReasons}" var="npUsersReason">
                                                <form:option
                                                        value="${npUsersReason.id}">${npUsersReason.reasonCode}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="reasonLines">Additional
                                            Comments</label>
                                        <form:textarea path="reasonLines" placeholder="Overview"
                                                       rows="6" class="form-control form-ctrl" maxlength="300"/>
                                    </div>
                                </div>
                            </div>

                            <hr>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button class="btn btn-confirm" name="btn-send"
                                                id="btn-confirm">Submit
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>



