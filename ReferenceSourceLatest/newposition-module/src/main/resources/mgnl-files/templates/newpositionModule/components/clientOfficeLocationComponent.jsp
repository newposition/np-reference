<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!-- Bootstrap core CSS -->
<link href="./resources/templating-kit/css/bootstrap.css" rel="stylesheet">
<link href="./resources/templating-kit/css/font-awesome.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="./resources/templating-kit/css/style.css" rel="stylesheet">
<script src="./resources/templating-kit/js/clientOfficeLocation.js"></script>
<script src="./resources/templating-kit/js/jquery.form-validator.js"></script>

<div class="container">
    <div class="tlt-heading">

        <h2>Client Office Locations</h2>

    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select1" class="form-control selectbox "
                                            onchange="filterFunction1()">
                                        <option value="">Country</option>
                                        <c:forEach var="country" items="${cities}">
                                            <option value="${country}">${country}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select2" class="form-control selectbox "
                                            onchange="filterFunction2()">
                                        <option value="">State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select3" class="form-control selectbox "
                                            onchange="filterFunction3()">
                                        <option value="">City</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4"><span id='msg' class="sucessMessage">${message}</span></div>
                    <div class="col-xs-4 col-md-4 pull-right">
                        <input type="button" class="btn btn-rd-cor" data-toggle="modal" data-target="#myModalAdd"
                               onclick="clearAll()" value="Add OfficeLocation">
                    </div>
                </div>
            </div>
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Search" id="qSearch" class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="select4" onchange="sortByFunction()">
                            <option>Sort by</option>
                            <!--                             <option value="address1">Address1</option> -->
                            <!--                                             <option value="address2">Address2</option> -->
                            <option value="City">City</option>
                            <option value="County">County</option>
                            <!--                                               <option value="stateDescription">State Description</option> -->
                            <option value="Country">Country</option>

                        </select>
                    </div>
                    <div class="sort pull-left">
                        <span class="result-pop"><label id="noOfRows"></label> result found</span>
                    </div>
                </div>
            </div>


            <div id="table1Div"></div>


            <div class="paging" id="header">
                <nav role="navigation">
                    <ul class="cd-pagination">
                        <li id="indexId1"><a class="disabled" href="#0" id="ancher1" onclick="moveToFirst()"><i
                                class="fa fa-angle-double-left"></i></a></li>
                        <li id="indexId2"><a class="disabled" href="#0" id="ancher2" onclick="moveOneStepLeft()"><i
                                class="fa fa-angle-left"></i></a></li>

                        <li id="indexId3"><a href="#0" id="ancher3"><i class="fa fa-angle-right"
                                                                       onclick="moveOneStepRight()"></i></a></li>
                        <li id="indexId4"><a href="#0" id="ancher4"><i class="fa fa-angle-double-right"
                                                                       onclick="moveToLast()"></i></a></li>
                    </ul>
                </nav>
            </div>

        </div>

    </div>
</div>

<!--Bootsrap Modal details
================================================== -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="makeReadOnly()">
                    <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Client Office Location</h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="form-contact" action="?" method="POST">
                        <fieldset>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="clientOfficeLocID" name="clientOfficeLocID" type="hidden">
                                        <input id="locationID" name="locationID" type="hidden">
                                        <label for="" class="control-label">Address1</label>
                                        <input id="Text1" name="address1" type="text" placeholder="Address1"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'address1','Text1')"
                                               readonly="readonly" data-validation="required"
                                               data-validation-error-msg="Enter Address1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">

                                        <label for="fname" class="control-label">Address2</label>
                                        <input class="form-control input-md" placeholder="Address2" id="Text2"
                                               name="address2" readonly="readonly"
                                               onkeyup="getCommonBean(this.value,'address2','Text2')"
                                               data-validation="required" data-validation-error-msg="Enter Address2">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">

                                        <label for="pemail" class="control-label">Address3</label>
                                        <input id="Text3" name="address3" type="text" placeholder="Address3"
                                               class="form-control input-md" readonly="readonly"
                                               onkeyup="getCommonBean(this.value,'address3','Text3')">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="cityID" name="cityID" type="hidden">
                                        <label for="pemail" class="control-label">City</label>
                                        <input id="Text4" name="cityDescription" type="text" placeholder="City"
                                               class="form-control input-md" readonly="readonly"
                                               onkeyup="getCommonBean(this.value,'city','Text4','cityID')"
                                               data-validation="required" data-validation-error-msg="Enter City Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="countyID" name="countyID" type="hidden">
                                        <label for="pemail" class="control-label">County</label>
                                        <input id="Text5" name="countyDescription" type="text" placeholder="County"
                                               class="form-control input-md" readonly="readonly"
                                               onkeyup="getCommonBean(this.value,'county','Text5','countyID')"
                                               data-validation="required" data-validation-error-msg="Enter County Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="zIPID" name="zIP" type="hidden">
                                        <input id="stateID" name="stateID" type="hidden">
                                        <label for="pemail" class="control-label">State</label>
                                        <input id="Text6" name="stateDescription" type="text" placeholder="State"
                                               class="form-control input-md" readonly="readonly"
                                               onkeyup="getZipBean(this.value,'state','Text6','zIPID')"
                                               data-validation="required" data-validation-error-msg="Enter State Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="countryID" name="countryID" type="hidden">
                                        <label for="pemail" class="control-label">Country</label>
                                        <input id="Text7" name="countryDescription" type="text" placeholder="Country"
                                               class="form-control input-md" readonly="readonly"
                                               onkeyup="getCommonBean(this.value,'country','Text7','countryID')"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Country Name">
                                    </div>
                                </div>
                            </div>


                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="button" id="btn-confirm" name="btn-send" class="btn btn-confirm"
                                               onclick="editFunction()" value="Edit"/>
                                    </div>
                                    <div class="col-md-6">
                                        <button id="btn-confirm" name="btn-send" class="btn btn-confirm" type="submit">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Bootsrap my Modal add
================================================== -->
<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Client Office Location</h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form id="form-contact" action="?" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!--  <input id="clientOfficeLocID1" name="clientOfficeLocID" type="hidden"  > -->
                                        <!--  <input id="locationID1" name="locationID" type="hidden"  > -->
                                        <label for="" class="control-label">Address1</label>
                                        <input id="Text9" name="address1" type="text" placeholder="Address1"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'address1','Text9')"
                                               data-validation="required" data-validation-error-msg="Enter Address1">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="fname" class="control-label">Address2</label>
                                        <input class="form-control input-md" placeholder="Address2" id="Text10"
                                               name="address2" onkeyup="getCommonBean(this.value,'address2','Text10')"
                                               data-validation="required" data-validation-error-msg="Enter Address2">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pemail" class="control-label">Address3</label>
                                        <input id="Text11" name="address3" type="text" placeholder="Address3"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'address3','Text11')">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="cityID1" name="cityID" type="hidden">
                                        <label for="pemail" class="control-label">City</label>
                                        <input id="Text12" name="cityDescription" type="text" placeholder="City"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'city','Text12','cityID1')"
                                               data-validation="required" data-validation-error-msg="Enter City Name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="countyID1" name="countyID" type="hidden">
                                        <label for="pemail" class="control-label">County</label>
                                        <input id="Text13" name="countyDescription" type="text" placeholder="County"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'county','Text13','countyID1')"
                                               data-validation="required" data-validation-error-msg="Enter County Name">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="zIPID1" name="zIP" type="hidden">
                                        <label for="pemail" class="control-label">State Id</label>
                                        <input id="Text16" name="stateID" type="text" placeholder="StateId"
                                               class="form-control input-md"
                                               onkeyup="getZipBean(this.value,'stateId','Text14','zIPID1')"
                                               data-validation="required" data-validation-error-msg="Enter State Id">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">

                                        <label for="pemail" class="control-label">State</label>
                                        <input id="Text14" name="stateDescription" type="text" placeholder="State"
                                               class="form-control input-md"
                                               onkeyup="getZipBean(this.value,'state','Text14','zIPID1')"
                                               data-validation="required" data-validation-error-msg="Enter State Name">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="countryID1" name="countryID" type="hidden">
                                        <label for="pemail" class="control-label">Country</label>
                                        <input id="Text15" name="countryDescription" type="text" placeholder="Country"
                                               class="form-control input-md"
                                               onkeyup="getCommonBean(this.value,'country','Text15','countryID1')"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Country Name">
                                    </div>

                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <!--  <div class="col-md-6">
                                         <a href=""><button id="btn-confirm" name="btn-send" class="btn btn-confirm" >Upload</button></a>
                                     </div> -->
                                    <div class="col-md-6">
                                        <button id="btn-confirm" name="btn-send" class="btn btn-confirm" type="submit"
                                                style="margin-left:120px">Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $.validate();
</script>
