<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>

<center class="quickPage_msg">
    <b class="errorMessage">${cpm}</b> <b class="sucessMessage">${epm}</b>
</center>
<div class="container">
    <div class="row">

        <div class="col-md-6 col-md-offset-3">
            <form:form id="form-contact" action="?" commandName="quickRegistrationForm" method="POST">
                <fieldset>
                    <legend class="qr_legend text-right">* - mandatory field</legend>
                    <div class="form-group">
                        <blossom:pecid-input/>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="fname" class="control-label">First Name*</label>
                                <form:input path="candidate.firstName" placeholder="Firstname"
                                            class="form-control input-md"/>
                                <form:errors path="candidate.firstName" class="errorMessage"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lname" class="control-label">Last Name*</label>
                                <form:input path="candidate.lastName" placeholder="Lastname"
                                            class="form-control input-md"/>
                                <form:errors path="candidate.lastName" class="errorMessage"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cjtitle" class="control-label">Current Job Title</label>
                                <form:input path="candidate.jobTitle" placeholder="Current Job Title"
                                            class="form-control input-md"/>
                                <form:errors path="candidate.jobTitle" class="errorMessage"/>
                                <span class="errorMessage" id="jobtitle">${wrongJtitlepatrn}</span>
                            </div>
                            <div class="col-md-6">
                                <label for="cemployer" class="control-label">Current Employer</label>
                                <form:input path="candidate.currentCompany" placeholder="Current Employer"
                                            class="form-control input-md"/>
                                <form:errors path="candidate.currentCompany" class="errorMessage"/>
                                <span class="errorMessage" id="company">${wrongcompanypatrn}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="pemail" class="control-label">Primary Email Address*</label>
                                <form:input path="candidate.primaryEmail" placeholder="Enter your primary Email Address"
                                            class="form-control input-md"/>
                                <form:errors path="candidate.primaryEmail" class="errorMessage"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cemail" class="control-label">Confirm Email Address*</label>
                                <input type="text" name="confirmPrimaryEmailAddress"
                                       placeholder="Confirm primary Email Address" class="form-control input-md"/>
                            </div>
                            <form:errors path="confirmPrimaryEmailAddress" class="errorMessage"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="password" class="control-label">Password*</label>
                                <form:password path="password" placeholder="Enter a password"
                                               class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="cpassword" class="control-label">Confirm Password*</label>
                                <input type="password" name="confirmPassword" placeholder="Confirm your Password"
                                       class="form-control input-md"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form:errors path="password" class="errorMessage"/>
                                <form:errors path="confirmPassword" class="errorMessage"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="text" id="defaultReal" name="defaultReal" placeHolder="Enter Code"
                                       class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="checkbox-inline" for="agree">
                                    <input type="checkbox" name="agree" id="agree" class="agree" value="1">I agree to
                                    the terms and conditions
                                </label><br>
                                <form:errors path="agree" class="errorMessage"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Confirm Via Email" class="quickRegBtn btn btn-confirm">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>


    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#defaultReal').realperson({
            chars: $.realperson.alphanumeric
        });
    });
</script>
