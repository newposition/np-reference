<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<script src="./resources/templating-kit/js/myEducationForm.js"></script>


<div class="container">
    <div class="col-md-11 centered bottom_50 tpf_space">
        <div class="row mgb20">
            <div class="col-md-2"><a href="" class="Link_nordc  Link_norac-a">Me</a></div>
            <div class="col-md-3"><a href="" class="Link_nordc Link_norac-a">My Career History</a></div>
            <div class="col-md-3"><a href="" class="Link_norac Link_norac-a">My Education</a></div>
            <div class="col-md-2"><a href="" class="Link_nor Link_norac-a">My Sponsors</a></div>
            <div class="col-md-2"><a href="" class="Link_nor">My Future</a></div>
        </div>
    </div>
    <div class="col-md-10 centered">
        <div class="row mgb20">
            <div class="hpe education_history_head">Education History</div>
        </div>
        <div class="row Education_history_row mgb20">

            <div class="scroll">
                <ul class="educationHistory">
                    <li>
                        <div class="em_box apc">
                            <div class="em_box_logo-a">

                            </div>
                            <div class="em_box_details">
                                <div class=""><span class="errorText"></span></div>
                                <form id="educationForm" method="post">
                                    <input type="hidden" id="id" name="id">
                                    <div class="line1 ui-widget"><input type="text" placeholder="Enter Institution"
                                                                        id="Institution" name="institution"
                                                                        class="pp_text"/></div>
                                    <div class="line2"><input type="text" placeholder="Enter Award Type" id="awardType"
                                                              name="awardType" class="pp_text"/></div>
                                    <div class="line3"><input type="text" placeholder="Enter Award Subject"
                                                              id="awardSubject" name="awardSubject" class="pp_text"/>
                                    </div>
                                    <div class="line4"><input type="text" placeholder="Enter Level Attained" id="level"
                                                              name="level" class="pp_text"/></div>
                                    <div class="line5"><input class="datepicker-12 pp_text" type="text"
                                                              id="educationStartDate" name="startDate"
                                                              placeholder="Date From" style="width:47.5%;"/><input
                                            type="text" id="educationEndDate" name="endDate" placeholder="Date To"
                                            style="width:47.5%;" class="datepicker-12 pp_text"/></div>
                                    <button id="saveEducation" class="save save_btn_es"></button>
                                    <button class="cancel cancel_btn_es"></button>
                                </form>
                            </div>

                        </div>
                    </li>
                    <c:set var="educationList" value="${educationHistories}"/>
                    <c:forEach var="education" items="${educationList}">
                        <li>
                            <div class="em_box apc">
                                <div class="em_box_logo">
                                    <c:if test="${not empty education.instituteLogo}">
                                        <img alt=""
                                             src="${pageContext.request.contextPath}/docroot/images/${education.instituteLogo}"/>
                                        <input type="hidden" id="instituteLogo${education.id}"
                                               value="${pageContext.request.contextPath}/docroot/images/${education.instituteLogo}">
                                    </c:if>
                                </div>
                                <div class="em_box_details">
                                    <div class="line1${education.id}">${education.institution}</div>
                                    <div class="line2${education.id}">${education.awardType}</div>
                                    <div class="line3${education.id}">${education.awardSubject}</div>
                                    <div class="line4${education.id}">${education.level}</div>
                                    <span class="line5${education.id}">${education.startDate}</span>&nbsp;-&nbsp;
                                    <span class="line6${education.id}">${education.endDate}</span>
                                </div>
                                <button id="${education.id}" class="edit edit_btn_es"></button>
                                <button id="${education.id}" class="trash trash_btn_es"></button>
                            </div>
                        </li>
                    </c:forEach>


                </ul>
            </div>


        </div>


        <div class="row mgb20">
            <div class="hpe personal_qualification_head">Professional Qualifications</div>
        </div>
        <div class="row personal_qualification_row mgb20">

            <div class="scroll">
                <ul class="qualifications">
                    <li>
                        <div class="em_box apc">
                            <div class="em_box_logo-a">

                            </div>
                            <div class="em_box_details">
                                <div class=""><span class="errorText"></span></div>
                                <form id="qualificationForm" method="post">
                                    <input type="hidden" id="id" name="id">
                                    <div class="line1 ui-widget"><input type="text" placeholder="Enter Institution"
                                                                        id="Institution" name="institution"
                                                                        class="pp_text"></div>
                                    <div class="line3"><input type="text" placeholder="Enter Award Subject"
                                                              id="awardSubject" name="awardSubject" class="pp_text">
                                    </div>
                                    <div class="line4"><input type="text" placeholder="Enter Level Attained" id="level"
                                                              name="level" class="pp_text"></div>
                                    <div class="line5"><input class="datepicker-12 pp_text" type="text"
                                                              id="qualificationStartDate" name="startDate"
                                                              placeholder="Date From" style="width:47.5%;"><input
                                            type="text" id="qualificationEndDate" name="endDate" placeholder="Date To"
                                            style="width:47.5%;" class="datepicker-12 pp_text"></div>

                                    <button class="save save_btn_es" id="saveQualification"></button>
                                    <button class="cancel cancel_btn_es"></button>
                                </form>
                            </div>
                        </div>
                    </li>
                    <c:set var="qualificationList" value="${candidateQualifications}"/>
                    <c:forEach var="qualification" items="${qualificationList}">
                        <li>
                            <div class="em_box apc">
                                <div class="em_box_logo">
                                    <c:if test="${not empty qualification.instituteLogo}">
                                        <img alt=""
                                             src="${pageContext.request.contextPath}/docroot/images/${qualification.instituteLogo}"/>
                                        <input type="hidden" id="instituteLogo${qualification.id}"
                                               value="${pageContext.request.contextPath}/docroot/images/${qualification.instituteLogo}">
                                    </c:if>
                                </div>
                                <div class="em_box_details">
                                    <div class="line1${qualification.id}">${qualification.institution}</div>
                                    <div class="line3${qualification.id}">${qualification.awardSubject}</div>
                                    <div class="line4${qualification.id}">${qualification.level}</div>
                                    <span class="line5${qualification.id}">${qualification.startDate}</span>&nbsp;-&nbsp;
                                    <span class="line6${qualification.id}">${qualification.endDate}</span>
                                </div>
                                <button id="${qualification.id}" class="edit edit_btn_es"></button>
                                <button id="${qualification.id}" class="trash trash_btn_es"></button>
                            </div>
                        </li>
                    </c:forEach>

                </ul>
            </div>


        </div>

        <div class="row mgb20">
            <div class="hpe certification_head">Certifications</div>
        </div>
        <div class="row certification_row mgb20">

            <div class="scroll">
                <ul class="certifications">
                    <li>
                        <div class="em_box apc">
                            <div class="em_box_logo-a">

                            </div>
                            <div class="em_box_details">
                                <div class=""><span class="errorText"></span></div>
                                <form id="certificationForm" method="post">
                                    <input type="hidden" id="id" name="id">
                                    <div class="line1 ui-widget"><input type="text" placeholder="Enter Institution"
                                                                        id="Institution" name="institution"
                                                                        class="pp_text"></div>
                                    <div class="line3"><input type="text" placeholder="Enter Award Subject"
                                                              id="awardSubject" name="awardSubject" class="pp_text">
                                    </div>
                                    <div class="line4"><input type="text" placeholder="Enter Level Attained" id="level"
                                                              name="level" class="pp_text"></div>
                                    <div class="line5"><input type="text" id="startDate" name="startDate"
                                                              placeholder="Year" style="width:47.5%;" class="pp_text">
                                    </div>

                                    <button class="save save_btn_es" id="saveCertification"></button>
                                    <button class="cancel cancel_btn_es"></button>
                                </form>
                            </div>
                        </div>
                    </li>
                    <c:set var="certificatesList" value="${candidateCertificates}"/>
                    <c:forEach var="certificate" items="${certificatesList}">
                        <li>
                            <div class="em_box apc">
                                <div class="em_box_logo">
                                    <c:if test="${not empty certificate.instituteLogo}">
                                        <img alt=""
                                             src="${pageContext.request.contextPath}/docroot/images/${certificate.instituteLogo}"/>
                                        <input type="hidden" id="instituteLogo${certificate.id}"
                                               value="${pageContext.request.contextPath}/docroot/images/${certificate.instituteLogo}">
                                    </c:if>
                                </div>
                                <div class="em_box_details">
                                    <div class="line1${certificate.id}">${certificate.institution}</div>
                                    <div class="line3${certificate.id}">${certificate.awardSubject}</div>
                                    <div class="line4${certificate.id}">${certificate.level}</div>
                                    <div class="line5${certificate.id}">${certificate.startDate}</div>
                                </div>
                                <button id="${certificate.id}" class="edit edit_btn_es"></button>
                                <button id="${certificate.id}" class="trash trash_btn_es"></button>
                            </div>
                        </li>
                    </c:forEach>
                </ul>
            </div>


        </div>

        <div class="row mgb20">
            <div class="fify_without_paddoing">
                <div class="hpe languages_head">Languages</div>
                <div class="language_tog">
                    <div class="links_js links_jsonlyclick">
                        <c:set var="languageNamesList" value="${candidateLanguageNames}"/>
                        <c:forEach var="languageName" items="${languageNamesList}">
                            <a class="blue_link" href="javascript:void(0)">${languageName}<i
                                    class="fa fa-times"></i></a>
                        </c:forEach>
                    </div>
                    <input type="text" class="pp_text" placeholder="Enter Languages e.g. English fluent" id="Languages">
                </div>
            </div>
            <div class="fify_without_paddoing2">
                <div class="hpe interest_head">Interests</div>
                <div class="interest_tog">
                    <div class="links_js links_jsonlyclick">
                        <c:set var="interestNamesList" value="${candidateInterestNames}"/>
                        <c:forEach var="interestName" items="${interestNamesList}">
                            <a class="blue_link" href="javascript:void(0)">${interestName}<i
                                    class="fa fa-times"></i></a>
                        </c:forEach>
                    </div>
                    <input type="text" class="pp_text" placeholder="Enter Interests" id="interests">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="browser_next"><a href="mySponsor"><img src="./resources/templating-kit/images/blank.png"></a></div>
<div class="browser_prev"><a href="employment"><img src="./resources/templating-kit/images/blank.png"></a></div>
   