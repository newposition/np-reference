<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%-- <link href="${pageContext.request.contextPath}/docroot/css/style.css" rel="stylesheet"> --%>
<%-- <link href="${pageContext.request.contextPath}/docroot/css/font-awesome.css" rel="stylesheet"> --%>
<link href="./resources/templating-kit/css/font-awesome.min.css" rel="stylesheet">
<link href="./resources/templating-kit/css/media.css" rel="stylesheet">
<link href="./resources/templating-kit/css/common.css" rel="stylesheet">
<link href="./resources/templating-kit/css/magnific-popup.css" rel="stylesheet">
<!-- <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet"> -->
<!-- Bootstrap core CSS -->
<%-- <link href="${pageContext.request.contextPath}/docroot/css/bootstrap.css" rel="stylesheet"> --%>

<!-- Custom styles for this template -->
<!-- <script src="./resources/templating-kit/js/jquery.plugin.js"></script> -->
<!-- <script src="./resources/templating-kit/js/jquery.realperson.js"></script> -->
<!-- <script src="./resources/templating-kit/js/bootstrap.js"></script> -->
<!-- <script src="./resources/templating-kit/js/bootstrap-tabcollapse.js"></script> -->
<!-- <script src="./resources/templating-kit/js/jquery-1.11.2.js"></script> -->
<!-- <script src="./resources/templating-kit/js/jquery-ui.js"></script> -->
<!-- <script src="./resources/templating-kit/js/jquery.circliful.min.js"></script> -->
<!-- <link href="./resources/templating-kit/js/jquery.magnific-popup.min.js" rel="stylesheet"> -->


<script>
    $(document).ready(function (e) {
        $(".back_to_top").click(function () {
            $("html, body").animate({scrollTop: 0}, "slow");
            return false;
        });


        $("#search").click(function () {
            $(".search").slideToggle();
            $("div.menu_container").slideUp();
        });


        $(".menu li a").click(function () {
            $(this).parent().find("div.menu_container").slideToggle(500);
        });

        $(".re_check label").click(function () {
            $(this).toggleClass("re_checked");
        });

        $(function () {
            $(".datepicker-12").datepicker();
        });

        $('.tabs .tab-links a').on('click', function (e) {
            var currentAttrValue = $(this).attr('href');
            $('.tabs ' + currentAttrValue).show().siblings().hide();
            $(this).parent('li').addClass('active').siblings().removeClass('active');
            e.preventDefault();
        });
        $('#myStat2').circliful();

    });
</script>
<div class="user_header_section">
    <div class="wraper">

        <div class="rows">
            <div class="hs_col fl">

                <div class="ls_box">
                    <div class="rows">
                        <div class="ths_box_user text-center">
                            <div class="sm_circle"><span class="sm_circle_center"><img
                                    src="./resources/templating-kit/img/user_ico.png" alt="user" width="50px"></span>
                            </div>
                            <a href="">edit</a>
                        </div>
                        <div class="ths_line_user">
                            <h2>Joe Bloggs</h2>
                            <span class="gray_text">Available from: dd/mm/yyyy</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="hs_col fl">
                <div class="rs_box">
                    <div class="rows">
                        <div class="ths_box_user">
                            <div id="myStat2" data-dimension="110" data-text="75%" data-width="10" data-fontsize="22"
                                 data-percent="75" data-fgcolor="#5192d2" data-bgcolor="#67b5f2"></div>
                        </div>
                        <div class="ths_line_user">
                            <h2>Profile is 75% complete</h2>
                            <p>Make your profile more appealing for potetial <br>
                                employers by making your profile 100% complete </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="tabs">
        <ul class="tab-links">
            <li class="active"><a href="#tab1">Profile Details</a></li>
            <li><a href="#tab2">Preferences</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab1" class="tab active">
                <div class="wraper">
                    <div class="rows">
                        <div class="tab-sect">
                            <div class="rows">
                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <h3>Personal details <span class="btn_edit"><input type="button" class="edit"
                                                                                           value="Edit"></span></h3>

                                        <p>Joe Bloggs<br>
                                            joeblogg@pollen.co.uk<br>
                                            Linked in</p>
                                        <p>07777 123 456</p>

                                        <p>current role<br>
                                            Current employer<br>
                                            Technical compitency<br>
                                            Sector</p>
                                        <p>Eligable to work in the EU</p>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <h3>Personal statements <span class="btn_edit"><input type="button" class="edit"
                                                                                              value="Edit"></span></h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                            tempor incididunt
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusm tempor
                                            incididunt</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusm tempor
                                            incididunt
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusm tempor
                                            incididunt</p>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont">
                                        <div class="tb_hfs bd_bottom">
                                            <h3>Address <span class="btn_edit"><input type="button" class="edit"
                                                                                      value="Edit"></span></h3>
                                            <p>Lorem ipsum <br> dolor sit<br> amet sed do<br> eiusm tempor</p>
                                        </div>
                                        <div class="tb_hfs">
                                            <h3>Travel <span class="btn_edit"><input type="button" class="edit"
                                                                                     value="Edit"></span></h3>
                                            <p>Lorem ipsum <br> dolor sit<br> amet sed do</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Technical Skills <span class="btn_edit"><input type="button" class="edit"
                                                                                   value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Technical<br>
                                            <span class="pStrong">Tech Skill 1</span><br>
                                            6-12 Months</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Technical<br>
                                            <span class="pStrong">Tech Skill 1</span><br>
                                            1-3 Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Technical<br>
                                            <span class="pStrong">Tech Skill 1</span><br>
                                            5+ Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Design</br>
                                            <span class="pStrong">Design Skill 1</span><br>
                                            1-3 Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont">
                                        <p>Design</br>
                                            <span class="pStrong">Design Skill 2</span><br>
                                            5+ Years</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Sector Skills <span class="btn_edit"><input type="button" class="edit"
                                                                                value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Company<br>
                                            <span class="pStrong">Sector 1</span><br>
                                            6-12 Months</p>

                                        <p>Role<br>
                                            <span class="pStrong">Role 1</span><br>
                                            6-12 Months</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Company<br>
                                            <span class="pStrong">Sector 2</span><br>
                                            1-3 Years</p>

                                        <p>Role<br>
                                            <span class="pStrong">Role 2</span><br>
                                            1-3 Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Company<br>
                                            <span class="pStrong">Sector 3</span><br>
                                            5+ Years</p>

                                        <p>Role<br>
                                            <span class="pStrong">Role 3</span><br>
                                            5+ Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont bd_right">
                                        <p>Company<br>
                                            <span class="pStrong">Sector 4</span><br>
                                            1-3 Years</p>

                                        <p>Role<br>
                                            <span class="pStrong">Role 4</span><br>
                                            1-3 Years</p>
                                    </div>
                                </div>

                                <div class="tb_col5">
                                    <div class="tb_cont">
                                        <p>Company<br>
                                            <span class="pStrong">Sector 5</span><br>
                                            1-3 Years</p>

                                        <p>Role<br>
                                            <span class="pStrong">Role 5</span><br>
                                            5+ Years</p>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Brainbech <span class="btn_edit"><input type="button" class="edit" value="Add/Edit"></span>
                                </h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_circle">
                                                    <span class="sm_circle_center">4.3</span>
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p class="pStrong">Job certification</p>
                                                <p>11/07/2014</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_circle">
                                                    <span class="sm_circle_center">4.3</span>
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p class="pStrong">Job certification</p>
                                                <p>11/07/2014</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_circle">
                                                    <span class="sm_circle_center">4.3</span>
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p class="pStrong">Job certification</p>
                                                <p>11/07/2014</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Employment History <span class="btn_edit"><input type="button" class="edit"
                                                                                     value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_box">
                                                    <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg">
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p>March 2013 - May 2014</p>
                                                <p class="pStrong">Employer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_box">
                                                    <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg">
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p>March 2013 - May 2014</p>
                                                <p class="pStrong">Employer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_box">
                                                    <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg">
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p>March 2013 - May 2014</p>
                                                <p class="pStrong">Employer</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Education <span class="btn_edit"><input type="button" class="edit" value="Add/Edit"></span>
                                </h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col4">
                                    <div class="tb_cont bd_right">
                                        <p class="pStrong">Subject</p>
                                        <p>Ba Hons <br>
                                            2.1
                                        </p>
                                        <p> h 2013 - May 2014 <br>
                                            Institution</p>
                                    </div>
                                </div>

                                <div class="tb_col4">
                                    <div class="tb_cont bd_right">
                                        <p class="pStrong">Subject</p>
                                        <p>Ba Hons <br>
                                            2.1
                                        </p>
                                        <p> h 2013 - May 2014 <br>
                                            Institution</p>
                                    </div>
                                </div>

                                <div class="tb_col4">
                                    <div class="tb_cont bd_right">
                                        <p class="pStrong">Subject</p>
                                        <p>Ba Hons <br>
                                            2.1
                                        </p>
                                        <p> h 2013 - May 2014 <br>
                                            Institution</p>
                                    </div>
                                </div>

                                <div class="tb_col4">
                                    <div class="tb_cont">
                                        <p class="pStrong">Subject</p>
                                        <p>Ba Hons <br>
                                            2.1
                                        </p>
                                        <p> h 2013 - May 2014 <br>
                                            Institution</p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Compensation & Availibility <span class="btn_edit"><input type="button" class="edit"
                                                                                              value="Add/Edit"></span>
                                </h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col2">
                                    <div class="tb_cont bd_right">
                                        <p class="pStrong">Permanent</p>
                                        <p><i class="fa fa-gbp"></i>2000 per months <br>
                                            <i class="fa fa-gbp"></i>2000 bonus per year
                                        </p>
                                        <p> h 2013 - May 2014 <br>
                                            Institution</p>
                                    </div>
                                </div>

                                <div class="tb_col2">
                                    <div class="tb_cont">
                                        <p class="pStrong">Freelance</p>
                                        <p><i class="fa fa-gbp"></i>200 per day </p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>References & Sponsorships <span class="btn_edit"><input type="button" class="edit"
                                                                                            value="Add/Edit"></span>
                                </h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <p>Reference<br>
                                            <span class="pStrong">Name Surname</span><br>
                                            Personlal reference</p>

                                        <p><span class="pStrong">Job title lorem ipsum</span><br>
                                            Company</p>

                                        <p>Tel: 0208 977 1234<br>
                                            Email: name@company.co.uk</p>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont bd_right">
                                        <p>Reference<br>
                                            <span class="pStrong">Name Surname</span><br>
                                            Personlal reference</p>

                                        <p><span class="pStrong">Job title lorem ipsum</span><br>
                                            Company</p>

                                        <p>Tel: 0208 977 1234<br>
                                            Email: name@company.co.uk</p>
                                    </div>
                                </div>

                                <div class="tb_col3">
                                    <div class="tb_cont">
                                        <p>Sponsorships<br>
                                            <span class="pStrong">Name Surname</span><br>
                                            Personlal reference</p>

                                        <p><span class="pStrong">Job title lorem ipsum</span><br>
                                            Company</p>

                                        <p>Tel: 0208 977 1234<br>
                                            Email: name@company.co.uk</p>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>CV & Photo <span class="btn_edit"><input type="button" class="edit"
                                                                             value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_col2">
                                    <div class="tb_cont bd_right">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_box">
                                                    <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg">
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p>File Type</p>
                                                <p class="pStrong">File Name</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tb_col2">
                                    <div class="tb_cont">
                                        <div class="rows">
                                            <div class="jc_box">
                                                <div class="sm_box">
                                                    <img src="http://celoeuropa.net/wp-content/uploads/2013/08/light-gray-gradient.jpg">
                                                </div>
                                            </div>
                                            <div class="jc_line">
                                                <p>File Type</p>
                                                <p class="pStrong">Photo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="rows">
                        <div class="tab-sect2 fl">
                            <div class="tb_heading">
                                <h3>Membership <span class="btn_edit"><input type="button" class="edit"
                                                                             value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_cont">
                                    <ul>
                                        <li>Lorem ispum dolor set amet</li>
                                        <li>Lorem ispum dolor sit</li>
                                        <li>Lorem ispum dolor</li>
                                        <li>Lorem ispum</li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div class="tab-sect2 fr">
                            <div class="tb_heading">
                                <h3>Honours & awards <span class="btn_edit"><input type="button" class="edit"
                                                                                   value="Add/Edit"></span></h3>
                            </div>
                            <div class="rows">

                                <div class="tb_cont">
                                    <ul>
                                        <li>Lorem ispum dolor set amet</li>
                                        <li>Lorem ispum dolor sit</li>
                                        <li>Lorem ispum dolor</li>
                                        <li>Lorem ispum</li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="rows">
                        <div class="tab-sect">
                            <div class="tb_heading">
                                <h3>Interests <span class="btn_edit"><input type="button" class="edit" value="Add/Edit"></span>
                                </h3>
                            </div>
                            <div class="rows">
                                <div class="tb_cont">
                                    <ul class="btn_tag">
                                        <li><input type="button" class="tag" value="Interests"></li>
                                        <li><input type="button" class="tag" value="Lorem ipsum"></li>
                                        <li><input type="button" class="tag" value="Dolor sit amet"></li>
                                        <li><input type="button" class="tag" value="Elit"></li>
                                        <li><input type="button" class="tag" value="Ut wisi"></li>
                                        <li><input type="button" class="tag" value="Quis nosturd"></li>
                                        <li><input type="button" class="tag" value="Interests"></li>
                                        <li><input type="button" class="tag" value="Lorem ipsum"></li>
                                        <li><input type="button" class="tag" value="Dolor sit amet"></li>
                                        <li><input type="button" class="tag" value="Elit"></li>
                                        <li><input type="button" class="tag" value="Ut wisi"></li>
                                        <li><input type="button" class="tag" value="Quis nosturd"></li>
                                        <li><input type="button" class="tag" value="Interests"></li>
                                        <li><input type="button" class="tag" value="Lorem ipsum"></li>
                                        <li><input type="button" class="tag" value="Dolor sit amet"></li>
                                        <li><input type="button" class="tag" value="Elit"></li>
                                        <li><input type="button" class="tag" value="Ut wisi"></li>
                                        <li><input type="button" class="tag" value="Quis nosturd"></li>
                                        <li><input type="button" class="tag" value="Ut wisi"></li>
                                    </ul>
                                </div>


                            </div>
                        </div>

                    </div>

                </div>
            </div>


            <div id="tab2" class="tab">
                <div class="wraperMid">


                    <div id="" class="wraperSmall wraperSmallTop">
                        <div class="row">
                            <div class="rpw_left">
                                <label>New Password*</label>
                                <input type="text" placeholder="Input field">
                            </div>
                            <div class="rpw_left">
                                <label>Confirm New Password*</label>
                                <input type="text" placeholder="Input field">
                            </div>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div id="" class="wraperSmall wraperSmallTop">
                        <div class="row">
                            <label>Email Notifications*</label>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="emailOn" class="dark">On</label>
                                    <input type="checkbox" id="emailOn">
                                </div>
                            </div>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="emailOff" class="dark">Off</label>
                                    <input type="checkbox" id="emailOff">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label>SMS Notifications*</label>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="smsOn" class="dark">On</label>
                                    <input type="checkbox" id="smsOn">
                                </div>
                            </div>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="smsOff" class="dark">On</label>
                                    <input type="checkbox" id="smsOff">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label>Chat*</label>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="chatOn" class="dark">On</label>
                                    <input type="checkbox" id="chatOn">
                                </div>
                            </div>
                            <div class="rpw_left">
                                <div class="re_check">
                                    <label for="chatOff" class="dark">On</label>
                                    <input type="checkbox" id="chatOff">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div id="step_1" class="wraperSmall wraperSmallTop">
                        <div class="row">
                            <label>Email (primary)*</label>
                            <input type="text" placeholder="Pre-populated">
                        </div>
                        <div class="row">
                            <label>Email (secondary)</label>
                            <input type="text" placeholder="Input field">
                        </div>
                        <div class="row">
                            <label>Email (tertiary)</label>
                            <input type="text" placeholder="Input field">
                        </div>
                    </div>

                    <div class="line"></div>

                    <div id="step_1" class="wraperSmall wraperSmallTop">
                        <div class="row center_row">
                            <input type="button" value="Save">
                        </div>
                    </div>

                    <div class="line"></div>

                    <div id="step_1" class="wraperSmall wraperSmallTop">
                        <div class="row">
                            <div class="rpw_left">
                                <input type="button" class="btn_wide" value="Disable account">
                            </div>
                            <div class="rpw_left">
                                <input type="button" class="btn_wide" value="Remove account">
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>



