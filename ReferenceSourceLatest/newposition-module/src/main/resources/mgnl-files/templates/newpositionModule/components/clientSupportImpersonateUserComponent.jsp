<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="./resources/templating-kit/js/impersonateUserPage.js"></script>
<link rel="stylesheet" href="./resources/templating-kit/css/style-vin.css"></link>

<div class="container impersonateUser">
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="filterBy" id="filterBy" class="form-control selectbox ">
                                        <option value="">Filter By</option>
                                        <c:forEach items="${npUserNames}" var="npUser"> <c:set var="name"
                                                                                               value="${npUser.firstName} ${npUser.lastName}"></c:set>
                                            <option value="${npUser.firstName},${npUser.lastName}"
                                                    <c:if test="${fn:containsIgnoreCase(filterBy, name)}">selected</c:if>>${npUser.firstName} ${npUser.lastName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label> <input type="text"
                                                                                                            placeholder="Asp"
                                                                                                            id="qSearch"
                                                                                                            class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="sortBy">
                            <option VALUE="">Sort by</option>
                            <option value="userId" <c:if test="${sortBy eq 'userId'}">selected</c:if>>User Id</option>
                            <option value="fullName" <c:if test="${sortBy eq 'fullName'}">selected</c:if>>Full Name
                            </option>
                            <option value="email" <c:if test="${sortBy eq 'email'}">selected</c:if>>Email</option>
                        </select>
                    </div>
                    <div class="sort pull-left">
                        <span class="result-pop">
                       <c:if test="${noOfNpUsers gt 2}">
                           ${noOfNpUsers} Results found
                       </c:if>
                            <c:if test="${noOfNpUsers eq 2}">
                                No Results found
                            </c:if>
                            <c:if test="${noOfNpUsers eq 1}">
                                1 Result found
                            </c:if> </span>
                    </div>
                </div>
            </div>

            <div class="imp-user-list">
                <div class="row no-pad">
                    <div class="tbl_panel">
                        <div class="row no-pad">
                            <div class="col-xs-6 col-md-2">
                                <div class="tbl-user">
                                    <strong>User</strong>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <div class="tbl-name">
                                    <strong>Full Name</strong>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <div class="tbl-name">
                                    <strong>Email Address</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <c:set var='space' value=' '></c:set>
                <c:forEach items="${npUsersList}" var="npUser">
                    <div class="row no-pad">

                        <div class="tbl_panel">

                            <div class="row no-pad">
                                <div class="col-xs-6 col-md-2">
                                    <div class="tbl-user">${npUser.id}</div>
                                    <input type="hidden" name="userId" value="${npUser.id}"/>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <div class="fulname tbl-name">${npUser.firstName}&nbsp;${npUser.lastName}</div>
                                    <c:set var="username" value="${npUser.firstName}${space}${npUser.lastName}"></c:set>
                                    <input type="hidden" name="userName" value="${username}"/>
                                </div>
                                <div class="col-xs-6 col-md-4">
                                    <div class="tbl-name">${npUser.primaryEmail}</div>
                                    <input type="hidden" name="userEmail" value="${npUser.primaryEmail}"/>
                                </div>

                                <div class="col-xs-12 col-md-2">
                                    <div class="tbl-mo-button">
                                            <%-- <a href="switchUser?j_username=${npUser.primaryEmailAddress}" id="btn-rd-cor"  class="btn btn-rd-cor" data-toggle="#myModal"
                                                >Impersonate</a> --%>
                                        <c:if test="${npUser.id ne user.id}">
                                            <button id="btn-rd-cor" name="btn-send"
                                                    class="btn btn-rd-cor ${npUser.npUserStatus.id gt 1  ? 'non-active' : '' }"
                                                    data-toggle="modal"
                                                    data-target="#myModal">Impersonate
                                            </button>
                                        </c:if>
                                        <c:if test="${npUser.id eq user.id}">
                                            <button id="cantImpersonate" name="btn-send" class="btn btn-rd-cor"
                                                    data-toggle="modal"
                                                    data-target="#myModal1">Impersonate
                                            </button>
                                        </c:if>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </c:forEach>
            </div>


            <c:if test="${npUserTotalPages ge 1}">
                <div class="paging">
                    <nav role="navigation">
                        <ul class="cd-pagination">
                            <c:if test="${npUserTotalPages ge 1 && currentPageNumber > 0}">
                                <li><a class="fristPagination" href="?pageNumber=0"><i
                                        class="fa fa-angle-double-left"></i></a></li>
                                <li><a class="prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
                            </c:if>
                            <c:forEach begin="0" end="${npUserTotalPages-1}" varStatus="index">
                                <li>
                                    <c:choose>
                                        <c:when test="${currentPageNumber eq index.index}">
                                            <a href="?pageNumber=${index.index}"
                                               class="current currentPageNo">${index.index + 1}</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="?pageNumber=${index.index}"
                                               class="pagination">${index.index + 1}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </li>
                            </c:forEach>
                            <c:if test="${npUserTotalPages ge 1 && currentPageNumber < npUserTotalPages-1}">
                                <li><a class="next" href="#0"><i class="fa fa-angle-right"></i></a></li>
                                <li><a class="lastPagination" href="${npUserTotalPages-1}"><i
                                        class="fa fa-angle-double-right"></i></a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </c:if>

        </div>

    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">x</span></button>
                <h3 id="myModalLabel" class="modal-title">${user.firstName} Impersonating <span
                        id="modal_username"></span></h3>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form:form id="form-contact" action="?" class="suspendUserForm" commandName="impersonateForm"
                               method="POST">
                        <%--                         <form action="" id="form-contact"> --%>
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="">You are about to take control of <span
                                                id="modal_username"></span>&nbsp; [<span id="modal_useremail"></span>].
                                            All activity will be
                                            logged &amp; monitored.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--                         <input type="hidden" value="false" name="suspendOrDelete"/> -->
                            <input type="hidden" value="" name="userId"/>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="fname">Reason Code</label>
                                        <!-- <select class="form-control selectbox " id="select" name="select">
                                        <option value="1">Select Reason Code</option>
                                        <option value="2">User on Holiday</option>
                                        <option value="3">Urgent Request</option>
                                        <option value="4">Support Issue</option>
                                        <option value="4">Other</option>
                                    </select> -->
                                        <form:select path="reasonCode" class="form-control selectbox " id="select">
                                            <c:forEach items="${npUsersReasons}" var="npUsersReason">
                                                <form:option
                                                        value="${npUsersReason.id}">${npUsersReason.reasonCode}</form:option>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label" for="pemail">Additional Comments</label>
                                        <!--                                     <textarea rows="6" name="textarea" id="textarea" placeholder="Overview" class="form-control form-ctrl"></textarea> -->
                                        <form:textarea path="reasonLines" placeholder="Overview" rows="6"
                                                       class="form-control form-ctrl"
                                                       maxlength="300"/>
                                    </div>
                                </div>
                            </div>

                            <hr>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 linkIm">
                                        <!--                                         <button class="btn btn-confirm" name="btn-send" id="btn-confirm">Submit</button> -->
                                        <a href="" id="impersonateLink" class="btn btn-rd-cor" data-toggle="#myModal"
                                        >Impersonate</a>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal1" class="modal fade"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span
                        aria-hidden="true">x</span></button>
                <%--                 <h3 id="myModalLabel" class="modal-title">${user.firstName} Impersonating <span id="modal_username"></span></h3> --%>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label" for="">You are not allowed to impersonate youself..
                                    </label>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
