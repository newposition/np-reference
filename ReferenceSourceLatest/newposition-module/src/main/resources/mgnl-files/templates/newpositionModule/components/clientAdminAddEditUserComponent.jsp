<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<script src="./resources/templating-kit/js/clientAdminUserPage.js"></script>
<link rel="stylesheet" href="./resources/templating-kit/css/style-vin.css"></link>
<div class="tlt-heading">
    <h2><c:if test="${not empty clientSupportAddEditUserForm.userId }">Edit User </c:if>
        <c:if test="${empty clientSupportAddEditUserForm.userId }">Add User </c:if></h2>
</div>
<hr>
<div class="container addedit">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="AddUserForm" action="?" commandName="clientSupportAddEditUserForm" method="POST">
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">

                                <b class="errorMessage">${epm}</b>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="userId" class="control-label">User ID</label>
                                <input type="text" disabled="disabled" value="${clientSupportAddEditUserForm.userId}"
                                       class="form-control input-md" placeholder="User ID" id="fname">
                                <input type="hidden" value="${clientSupportAddEditUserForm.userId}" name="userId"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="fname" class="control-label">First Name</label>
                                <form:input type="text" id="fname" path="firstName" placeholder="First name"
                                            class="form-control input-md" maxlength="30"/>
                                <form:errors path="firstName" class="errorMessage"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lname" class="control-label">Last Name</label>
                                <form:input id="lname" type="text" path="lastName" placeholder="Last Name"
                                            class="form-control input-md" maxlength="30"/>
                                <form:errors path="lastName" class="errorMessage"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cjtitle" class="control-label">Email Address</label>
                                <form:input id="cjtitle" type="text" path="primaryEmailAddress"
                                            placeholder="Email Address" class="form-control input-md" maxlength="128"/>
                                <form:errors path="primaryEmailAddress" class="errorMessage"/>
                                <span class="errorMessage">${errEmail}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cemployer" class="control-label">Confirm Email Address</label>
                                <form:input id="cemployer" type="text" path="confirmEmailAddress"
                                            placeholder="Confirm Email Address" class="form-control input-md"/>
                                <form:errors path="confirmEmailAddress" class="errorMessage"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="cemployer" class="control-label">Jobtitle</label>
                                <form:input id="cemployer" type="text" path="jobTitle" placeholder="Jobtitle"
                                            class="form-control input-md"/>
                                    <%--     								    <form:errors path="confirmEmailAddress"  class="errorMessage"/> --%>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="pemail" class="control-label">Mobile</label>
                                <form:input type="text" id="pemail" placeholder="Mobile" path="mobile"
                                            class="form-control input-md" maxlength="16"/>
                                <form:errors path="mobile" class="errorMessage"/>
                            </div>
                            <div class="col-md-6">
                                <label for="cemail" class="control-label">Office Tel.</label>
                                <form:input id="cemail" type="text" placeholder="Office Tel." path="officeTelePhone"
                                            class="form-control input-md" maxlength="16"/>
                                <form:errors path="officeTelePhone" class="errorMessage"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="blue-panel bp-min-height">
                            <div class="blue-panel-hpe">
                                <h3 class="blue-panel-title">Roles / Groups</h3>
                            </div>
                            <div class="blue-panel-body bp-hpe">
                                <div class="row no-pad">
                                    <div class="links_jss links_jsonlyclick links_bd">
                                        <c:if test="${not empty adminRoles}">
                                            <c:forEach items="${adminRoles}" var="adminRole">
                                                <a href="javascript:void(0)">${adminRole.npRole.roleName}<i
                                                        class="fa fa-times"></i></a>
                                                <c:if test="${fn:containsIgnoreCase(adminRole.npRole.roleName,'ROLE_CANDIDATE')}">
                                                    <input type="hidden" id="roleCan" value="candidate">
                                                </c:if>

                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="row no-pad">
                                    <div class="col-md-12">
                                        <input type="text" id="currentRole" class="form-control input-md"
                                               placeholder="Enter Roles / Groups">
                                        <form:hidden id="_currentRole" path="currentRole"
                                                     placeholder="Enter Roles / Groups" class="form-control input-md"/>
                                        <form:errors path="currentRole" class="errorMessage"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="button" value="Submit" id="btn-send" class="pp_button">
                            </div>
                        </div>
                    </div>

                </fieldset>
            </form:form>
        </div>
    </div>
</div>
