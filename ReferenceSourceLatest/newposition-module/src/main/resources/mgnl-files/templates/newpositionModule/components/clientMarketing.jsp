<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>


<div class="container">
    <div class="login_headings">
        <h2>Content</h2>
        Interested in using New Position to find the best candidates in the market? <br/>Simply register here and our
        Sales team will be in touch within 48 hours
    </div>
</div>
<hr>
<form:form id="form-clientMarketing" action="?" commandName="clientMarketingForm" method="POST"
           enctype="multipart/form-data">
    <div class="container">
        <div class="col-md-6 centered">
            <div class="row">
                <div class="row mgb20">
                    <div class="col-md-12"><label class="pp_label">Overview</label>

                        <form:textarea path="clientOverview" class="pp_textarea"
                                       placeholder="Please provide a brief note on your hiring needs"/>
                    </div>
                </div>


            </div>
            <div class="row mgb20">
                <div class="col-md-6">
                    <label for="sector" class="pp_label">Type Of Video</label>
                    <select name="Select1" id="Select1" class="form-control selectbox ">
                        <option value="0">Select</option>
                        <option value="1">I have youtube url with me</option>
                        <option value="2">I have my own video</option>
                        <option value="3">I want to upload a video</option>
                    </select>
                </div>

                <div class="col-md-6"><label class="pp_label">Url</label>
                    <input type="text" id="url-text" name="url-text" placeholder="Enter Url" class="pp_text">
                </div>
            </div>
            <div class="row mgb20">
                <div class="col-md-6">
                    <!--  <input type="file" id="choose-button" class="btn btn-confirm" />  -->
                    <input type="file" id="uploadBtn" class="btn btn-confirm" name="video" placeholder="browse file">

                    <!-- <video id="addVideo" controls autoplay></video>
                    <div id="v_message"></div> -->

                </div>


                <div class="col-md-6">
                    <button type="button" id="uupload" class="btn btn-confirm">Upload</button>
                </div>
            </div>
            <div class="row mgb20">
                <video id="addVideo" controls autoplay></video>
                <div id="v_message"></div>
            </div>


            <div>
                <iframe width="543" height="345" id="frame" name="videotwo" src=" "></iframe>
            </div>

            <br>


            <div class="row mgb20">
                <div class="col-md-12 text-center"><input type="button" id="saveMarketingForm" value="Submit"
                                                          class="pp_button"></div>
            </div>


        </div>
    </div>


</form:form>

<script type="text/javascript">


    (function localFileVideoPlayerInit(win) {
        var URL = win.URL || win.webkitURL,
                displayMessage = (function displayMessageInit() {
                    var node = document.querySelector('#v_message');

                    return function displayMessage(message, isError) {
                        node.innerHTML = message;
                        node.className = isError ? 'error' : 'info';
                    };
                }()),
                playSelectedFile = function playSelectedFileInit(event) {
                    var file = this.files[0];

                    var type = file.type;

                    var videoNode = document.querySelector('#addVideo');

                    var canPlay = videoNode.canPlayType(type);

                    canPlay = (canPlay === '' ? 'no' : canPlay);

                    var message = 'Can play type "' + type + '": ' + canPlay;

                    var isError = canPlay === 'no';

                    displayMessage(message, isError);

                    if (isError) {
                        return;
                    }

                    var fileURL = URL.createObjectURL(file);

                    videoNode.src = fileURL;
                },
                inputNode = document.querySelector('#uploadBtn');

        if (!URL) {
            displayMessage('Your browser is not ' +
                    '<a href="http://caniuse.com/bloburls">supported</a>!', true);

            return;
        }

        inputNode.addEventListener('change', playSelectedFile, false);
    }(window));


    $(document).ready(function () {
        $("#saveMarketingForm").click(function () {
            $("#form-clientMarketing").submit();

        });

        $("#Select1").on('change', function () {
            $('#uploadBtn').prop('disabled', this.value === '2' ? false : true);
        });

        $("#Select1").on('change', function () {
            $('#uupload').prop('disabled', this.value === '1' || this.value === '3' ? false : true);
        });

        $("#Select1").on('change', function () {
            $('#url-text').prop('disabled', this.value === '1' || this.value === '3' ? false : true);

        });

        $("#addVideo").hide();

        $("#frame").hide();

        var Options = jQuery('#Select1');
        var select = this.value;
        Options.change(function () {
            if ($(this).val() == '0') {
                $('#addVideo').hide();
            }
            else if ($(this).val() == '1') {
                $('#addVideo').hide();
            }
            else if ($(this).val() == '3') {
                $('#addVideo').hide();
            }

            else $('#addVideo').show();
        });

        var Options = jQuery('#Select1');
        var select = this.value;
        Options.change(function () {
            if ($(this).val() == '0') {
                $('#frame').hide();
            }
            else if ($(this).val() == '2') {
                $('#frame').hide();
            }
            else if ($(this).val() == '3') {
                $('#frame').hide();
            }

            else $('#frame').show();
        });

    });


</script>


<script>


    $(function () {
        $("#uupload").click(function () {
                    alert();
                    var parts = $("#url-text").val().split('=');

                    alert(parts);

                    var part = parts[parts.length - 1];
                    url = "https://www.youtube.com/embed/" + part;
                    document.getElementById('frame').setAttribute('src', url);

                }
        );
    });


</script>