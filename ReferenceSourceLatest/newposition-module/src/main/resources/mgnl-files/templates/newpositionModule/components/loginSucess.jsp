<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<c:if test="${not empty user}">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="media">
                    <div class="media-left text-center">
                        <div class="pro_pic">
                            <img src="./resources/templating-kit/img/user_ico.png" alt="user">
                        </div>
                        <a href="#">Edit</a>
                    </div>
                    <div class="media-body">
                        <div class="heading">${user.firstName} ${user.lastName}</div>
                        <span class="gray_text">Job Title</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
    <div class="tab_collapse_master">
    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#profile" data-toggle="tab">Profile
            Details</a></li>
        <!--<li><a href="#company" data-toggle="tab">Company Details</a></li>
    <li><a href="#legal" data-toggle="tab">Legal</a></li>-->
        <li><a href="#preferences" data-toggle="tab">Preferences</a></li>
        <li><a href="#audit" data-toggle="tab">Audit</a></li>
    </ul>
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div id="myTabContent" class="tab-content">
    <div class="tab-pane fade in active" id="profile">
    <div class="tab_outer">
        <div class="row">
            <div class="col-md-4">
                <div class="tab_outer_line">
                    <div class="heading">
                        Personal Details
                        <button type="submit" class="btn1">Edit</button>
                    </div>
                    <p>
                            ${user.firstName}<br> ${user.primaryEmailAddress}
                    </p>
                    <p>${user.phone}</p>
                    <p>
                        Current Role<br> Current Employer
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tab_outer_line">
                    <div class="heading">
                        Personal Statement
                        <button type="submit" class="btn1">Edit</button>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit, sed do eiusmod tempor incididunt</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="heading">
                    Address
                    <button type="submit" class="btn1">Edit</button>
                </div>
                <p>
                    1 Lorem Road<br> Lorem Town<br> Lorem City<br>
                    TW11 1TW
                </p>
            </div>
        </div>
    </div>
</c:if>
  