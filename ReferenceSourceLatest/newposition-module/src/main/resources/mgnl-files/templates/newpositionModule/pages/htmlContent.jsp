<html lang="en">
<head>

    <%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>

    <cms:init/>

</head>

<body>
<cms:area name="main"/>
</body>

</html>
