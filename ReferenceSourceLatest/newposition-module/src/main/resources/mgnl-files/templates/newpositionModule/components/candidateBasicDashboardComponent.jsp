<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>


<div class="container-fluid">
    <div class="row">

        <div class="head_filled">
            <div class="container">
                <div class="row">

                    <div class="col-md-9">
                        <h3>You have limited access to newposition, Do you want to continue registration</h3>
                    </div>

                    <div class="col-md-3">
                        <div class="refrence">
                            <a href="employment">Complete Registration</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="pd-head">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="circle-outer pull-left">
                    <div class="sm_circle">
                            <span class="sm_circle_center">
                                <img src="./resources/templating-kit/img/user_ico.png" alt="user" width="50px">
                            </span>
                    </div>
                </div>
                <div class="heading-title pull-left">
                    <h2>${user.firstName} ${user.lastName}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-8">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Quick Links</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon3.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Contact
                                        Support</a></div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon4.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a></div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon5.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Knowledge
                                        Center</a></div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                            <a class="image_white_text2" href="referenceList"><img
                                                    src="./resources/templating-kit/img/icon1.png">

                                                <span class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>
                                            
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a href="referenceList">References</a></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Market View</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                            <div class="carousel-caption">

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                       data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                       data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                </div> <!-- Carousel -->


                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $('#completeReference').click(function () {
        $('#referee').submit();
    });
</script>