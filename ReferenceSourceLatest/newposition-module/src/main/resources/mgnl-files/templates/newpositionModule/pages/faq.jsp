<html lang="en">
<head>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    <%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
    <%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="blossom-taglib" prefix="blossom" %>

    <title>New Position</title>

    <!-- Bootstrap core CSS -->
    <link
            href="./resources/templating-kit/css/bootstrap.css"
            rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./resources/templating-kit/css/style.css" rel="stylesheet"/>
    <link href="./resources/templating-kitcss/font-awesome.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/media.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/common.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/magnific-popup.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/jquery-ui.css" rel="stylesheet"/>
    <!-- Bootstrap core CSS -->
    <%-- <link href="${pageContext.request.contextPath}/docroot/css/bootstrap.css" rel="stylesheet"/> --%>
    <link href="${pageContext.request.contextPath}/docroot/js/jquery.magnific-popup.min.js" rel="stylesheet"/>

    <script src="${pageContext.request.contextPath}/docroot/js/jquery.plugin.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery.realperson.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/bootstrap-tabcollapse.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery-1.11.2.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery.circliful.min.js"></script>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery.plugin.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/jquery.realperson.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/docroot/js/bootstrap-tabcollapse.js"></script>

    <script>
        $(document).ready(function (e) {
            $(".back_to_top").click(function () {
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            });

            $('.accordion-container').on('show.bs.collapse', function () {
                alert("hi");
                $('.accordion-toogle .open').collapse('hide');
            });

            $("#search").click(function () {
                $(".search").slideToggle();
                $("div.menu_container").slideUp();
            });


            $(".menu li a").click(function () {
                $(this).parent().find("div.menu_container").slideToggle(500);
            });

            $('.accordion-toggle').on('click', function (event) {
                event.preventDefault();
                var accordion = $(this);
                var accordionContent = accordion.next('.accordion-content');
                var accordionToggleIcon = $(this).children('.toggle-icon');

                accordion.toggleClass("open");
                accordionContent.slideToggle(250);


                if (accordion.hasClass("open")) {
                    accordionToggleIcon.html("<i class='fa fa-minus'></i>");
                } else {
                    accordionToggleIcon.html("<i class='fa fa-plus'></i>");
                }
            });


        });
    </script>

    <cms:init/>

</head>

<body>

<cms:area name="header"/>

<div class="heading">
    <cms:area name="pageheading"/>
</div>


<div class="container">
    <div class="wraperMid">


        <section class="top_margin">
            <cms:area name="main"/>
        </section>
    </div>
</div>
<cms:area name="footer"/>

</body>
</html>