<%@ taglib prefix="cms" uri="http://magnolia-cms.com/taglib/templating-components/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="top_bar">
    <nav class="navbar">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"> <img src="./resources/templating-kit/img/logo.png" width="158"
                                                       height="86">
                </a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <cms:area name="navLeft"/>
                <cms:area name="navRight"/>

            </div>
        </div>
    </nav>
</div>