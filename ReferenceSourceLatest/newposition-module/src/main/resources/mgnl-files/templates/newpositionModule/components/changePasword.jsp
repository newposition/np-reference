<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>

<center>
    <c:if test="${not empty sucessmsg}">
        <b class="sucessMessage">${sucessmsg}</b>
    </c:if>
    <c:if test="${not empty errmsg}">
        <b class="errorMessage">${errmsg}</b>
    </c:if>
</center>
<div class="container">
    <div class="wraper">
        <ul class="login_box">
            <li><form:form id="newpaswordform" name="changePassword" action="?" commandName="changePasword"
                           method="POST">
                <blossom:pecid-input/>
                <div class="form-group">
                    <form:password path="oldPassword" placeholder="Old Password" class="form-control"/>
                    <form:errors path="oldPassword" style=" margin-left: 5%;" class="errorMessage"/>
                </div>
                <div class="form-group">
                    <form:password path="newPassword" placeholder="New Password" class="form-control"/>
                    <form:errors path="newPassword" style=" margin-left: 5%;" class="errorMessage"/>
                </div>
                <div class="form-group">
                    <form:password path="confirmPassword" placeholder="Confirm New Password" class="form-control"/>
                    <form:errors path="confirmPassword" style="margin-left: 5%;" class="errorMessage"/>
                </div>
                <div class="form-group">
                    <input class="lgn_btn" type="submit" value="Set Password"/>
                </div>
            </form:form></li>
        </ul>

    </div>
</div>
<!-- <script>
$( "#newpaswordform" ).submit(function( event ) {
$("span").hide();
$("b").hide();
var fname = $("input[name='newPassword']").val();
var lname = $("input[name='confirmPassword']").val();
var valid = true;
if (fname == "" ||lname=""){
$( ".info_fname" ).text( "Password should not be empty").show();
valid=false;
}
if (valid == false){
event.preventDefault();
}
else {
return;
}
});
</script> -->
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib
	uri="http://magnolia-cms.com/taglib/templating-components/cms"
	prefix="cms"%>
<%@ taglib
	uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
	prefix="cmsfn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="blossom-taglib" prefix="blossom"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse"%>

  <center>
    <c:if test="${not empty sucessmsg}">
	  <b class="sucessMessage" >${sucessmsg}<a href="home">Click here to Login</a></b>
    </c:if>
   <c:if test="${not empty errmsg}">
	  <b  class="errorMessage">${errmsg}</b>
   </c:if>
 </center>
  <div class="container">
  <div class="wraper">
<form:form id="newpaswordform" name="newpaswordform" action="?" commandName="newPaswordForm"	method="POST">
	<blossom:pecid-input />

			<div class="row">
					<form:hidden path="email" class="form-control" value="${not empty email ? email : '' }"/>
				</div>
				<div class="row">
					<form:password path="newPassword" placeholder="New Password" class="form-control"/>
					<form:errors path="newPassword" style="float:left;" class="errorMessage"/>
				</div>
				<div class="row">
					<form:password path="confirmPassword" placeholder="Confirm New Password" class="form-control"/>
				    <form:errors path="confirmPassword" style="float:left;" class="errorMessage"/>
				</div>
				<div class="row">
					<input class="lgn_btn" type="submit" value="Set Password" />
				</div>
</form:form>
</div>
</div>
<!-- <script>
$( "#newpaswordform" ).submit(function( event ) {
    $("span").hide();
    $("b").hide();
    var fname = $("input[name='newPassword']").val();
    var lname = $("input[name='confirmPassword']").val();
		var valid = true;
  if (fname == "" ||lname=""){
      $( ".info_fname" ).text( "Password should not be empty").show();
      valid=false;
  }
   if (valid == false){
       event.preventDefault();
   }
   else {
	return;   
   }
   });
</script> --> --%>