<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="np" uri="newpositionTags" %>
<script src="./resources/templating-kit/js/addRoleGroupPage.js"></script>
<link rel="stylesheet"
      href="./resources/templating-kit/css/style-vin.css"></link>

<div class="container usermanag">
    <center><span class="sucessMessage">${sm}</span></center>
    <div class="row">

        <div class="col-md-12">

            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 col-md-2 pull-right">
                                    <a href="addClientRoleGroup" class="btn btn-rd-cor pull-right">Add
                                        RoleGroup</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Asp" id="qSearch"
                                           class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="sortNpClientGroups" style="width:164px">
                            <option value="">Sort by</option>
                            <option value="groupName"
                                    <c:if test="${sortBy eq 'groupName'}">selected</c:if>>GroupName
                            </option>
                            <option value="groupDescription"
                                    <c:if test="${sortBy eq 'groupDescription'}">selected</c:if>>GroupDescription
                            </option>
                        </select>
                    </div>
                    <div class="sort pull-left"></div>
                </div>
            </div>

            <div class="imp-user-list">
                <div class="row no-pad">
                    <div class="tbl_panel">
                        <div class="row no-pad">
                            <div class="col-xs-4 col-md-3">
                                <div class="tbl-user">
                                    <strong>RoleGroup Name</strong>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-3">
                                <div class="tbl-user">
                                    <strong>RoleGroup Description</strong>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <div class="tbl-user">
                                    <strong>Created By</strong>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <div class="tbl-name">
                                    <strong>Modified By</strong>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <div class="tbl-name">
                                    <strong></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row no-pad" style="margin-left: 115px;margin-right: 115px;">
    <div class="tbl_panel">
        <c:forEach items="${coGroups}" var="coGroup">
            <div class="row no-pad">
                <div class="col-xs-3 col-md-3">
                    <div class="tbl-user">${coGroup.groupName}</div>
                </div>

                <div class="col-xs-5 col-md-3">
                    <div class="tbl-name emailId">${coGroup.groupDescription}</div>
                </div>

                <div class=" col-xs-5 col-md-2">
                    <div class="tbl-name fullName">${coGroup.createdBy}</div>
                </div>

                <div class=" col-xs-5 col-md-2">
                    <div class="tbl-name fullName">${coGroup.modifiedBy}</div>
                </div>

                <div class="col-xs-12 col-md-2">

                    <!-- <div class="tbl-mo-button  pull-left">

                        <a data-target="#myModalSuspend" data-toggle="modal"
                            class=" btn suspendClient suspendUser"> <img
                            src="./resources/templating-kit/img/changeState.png" />

                        </a>

                    </div> -->
                    <div class="tbl-mo-button pad   pull-left">
                        <a href="editClientRoleGroup?id=${coGroup.id}" class=" btn edit-icon"> <img
                                src="./resources/templating-kit/img/edit-icon.png"/>
                        </a>
                    </div>
                    <div class="tbl-mo-button mglft-rgt tbl-lst-button pull-left">
                        <a id="${coGroup.id}" data-target="#myModalDelete"
                           data-toggle="modal"
                           class="btn tbl-trash-button deleteClient deleteUser"> <i
                                class="fa fa-trash"></i>
                        </a>
                        <input id="groupId" type="hidden" value="${coGroup.id}"/>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<div class="paging">
    <nav role="navigation">
        <ul class="cd-pagination">
            <%-- <c:if test="${npClientTotalPages ge 1 && currentPageNumber > 0}">
                 <li><a class="fristPagination" href="?pageNumber=0"><i class="fa fa-angle-double-left"></i></a></li>
                 <li><a class="prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
             </c:if> --%>
            <%-- <c:forEach begin="0" end="${npClientTotalPages-1}" varStatus="index">
                    <li>
                    <c:choose>
                    <c:when test="${currentPageNumber eq index.index}">
                     <a  href="?pageNumber=${index.index}"  class="current currentPageNo">${index.index + 1}</a>
                     </c:when>
                    <c:otherwise> --%>
            <a href="" class="current currentPageNo pagination">1</a>
            <%--  </c:otherwise>
             </c:choose>
             </li>
     </c:forEach> --%>
            <%-- <c:if test="${npClientTotalPages ge 1 && currentPageNumber < npClientTotalPages-1}">
                <li><a class="next" href="#0"><i class="fa fa-angle-right"></i></a></li>
                <li><a class="lastPagination" href="${npClientTotalPages-1}"><i class="fa fa-angle-double-right"></i></a></li>
            </c:if> --%>
        </ul>
    </nav>
</div>





