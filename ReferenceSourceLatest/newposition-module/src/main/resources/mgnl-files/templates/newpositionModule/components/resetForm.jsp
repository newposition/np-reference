<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>

<center>
    <c:if test="${not empty errmsg}">
        <b class="errorMessage" style="margin-left:30px">${errmsg}</b>
    </c:if>
    <c:if test="${not empty sucessmsg}">
        <b class="sucessMessage" style="margin-left:12px" align="center">${sucessmsg}</b>
    </c:if>
</center>
<div class="container">
    <ul class="login_box">
        <li>
            <form:form id="forgottenPassword" action="?" commandName="resetForm" method="POST">
                <blossom:pecid-input/>
                <div class="form-group">

                    <form:input path="resetMail" placeholder="Email address or mobile no." id="" class="form-control"/>
                    <form:errors path="resetMail" style="float:left;" class="errorMessage"/>
                </div>

                <div class="form-group">
                    <button class="lgn_btn" type="submit">New password</button>
                </div>
            </form:form>
        </li>
    </ul>
</div>

