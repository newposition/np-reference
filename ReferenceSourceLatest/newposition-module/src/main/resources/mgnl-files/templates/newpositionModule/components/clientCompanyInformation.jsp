<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="np" uri="newpositionTags" %>


<div class="container">
    <div class="tlt-heading">

        <h2>Company Information</h2>

    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="clientCompanyInformation" action="" method="GET" commandName="form">

                <fieldset>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="fname" class="pp_label">Company Name</label>
                                <form:input path="companyName" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lname" class="pp_label">Sector</label>
                                <select name="sector" id="Select1" class="form-control selectbox " disabled="true">
                                    <np:listSectorOptions defaultSelect="${form.sector.id}"
                                                          defaultValue="Select Sector"/>
                                </select>

                            </div>
                        </div>
                    </div>

                    <hr>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="fname" class="pp_label">First Name</label>
                                <form:input path="firstName" name="fname" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lname" class="pp_label">Last Name</label>
                                <form:input path="lastName" name="lname" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="pemail" class="pp_label">Principle Contact Email Address</label>
                                <form:input path="principleContactEmail" name="pemail" type="text"
                                            class="form-control input-md" disabled="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cjtitle" class="pp_label">Tel. no.</label>
                                <form:input path="teleNumber" name="cjtitle" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="cemployer" class="pp_label">Mobile no.</label>
                                <form:input path="mobileNumber" name="cemployer" type="text"
                                            class="form-control input-md" disabled="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cjtitle" class="pp_label">Address 1</label>
                                <form:input path="address1" name="cjtitle" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="cemployer" class="pp_label">Address 2</label>
                                <form:input path="address2" name="cemployer" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cjtitle" class="pp_label">Town</label>
                                <form:input path="town" name="cjtitle" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="cemployer" class="pp_label">Country</label>
                                <form:input path="country" name="cemployer" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="cjtitle" class="pp_label">Postcode</label>
                                <form:input path="postalCode" name="cjtitle" type="text" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="pemail" class="pp_label">Account Status</label>
                                <select name="status" id="Select2" class="form-control selectbox " disabled="disabled">
                                    <np:listStatusOptions defaultSelect="${form.clientStatus.id}"/>
                                </select>

                            </div>
                        </div>
                    </div>


                </fieldset>
            </form:form>
        </div>


    </div>
</div>