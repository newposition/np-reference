<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>

<link rel="stylesheet" href="./resources/templating-kit/css/style-vin.css"></link>
<script src="./resources/templating-kit/js/qualificationManagement.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select id="instituteFilter" class="form-control selectbox ">
                                        <option value="" style="display:none;">Select Institution</option>
                                        <c:forEach items="${npInstitutions}" var="npInstitution">
                                            <option value="${npInstitution.id}"
                                                    <c:if test="${instituteFilter eq npInstitution.id}">selected</c:if>>${npInstitution.instituteName}</option>
                                        </c:forEach>
                                        <option value="0" <c:if test="${instituteFilter eq '0'}">selected</c:if>>Show
                                            All
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select id="courseTypeFilter" class="form-control selectbox ">
                                        <option value="" style="display:none;">Select CourseType</option>
                                        <c:forEach items="${npCourseTypes}" var="npCourseType">
                                            <option value="${npCourseType}"
                                                    <c:if test="${courseTypeFilter eq npCourseType}">selected</c:if>>${npCourseType}</option>
                                        </c:forEach>
                                        <option value="0" <c:if test="${courseTypeFilter eq '0'}">selected</c:if>>Show
                                            All
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select id="validFilter" class="form-control selectbox ">
                                        <option value="" style="display:none;">Select Valid</option>
                                        <option value="true" <c:if test="${validFilter eq 'true'}">selected</c:if>>
                                            Valid
                                        </option>
                                        <option value="false" <c:if test="${validFilter eq 'false'}">selected</c:if>>
                                            Invalid
                                        </option>
                                        <option value="0" <c:if test="${validFilter eq '0'}">selected</c:if>>Show All
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="sort col-md-10">
                                      <span class="result-pop"><c:if
                                              test='${param["showMergeMsg"]}'>Merged Qualifications Successfully<br></c:if>
                            
                                      <c:if test='${param["showAddMsg"]}'>Added Qualifications Successfully<br></c:if>
                            
                                       <c:if test='${param["showEditMsg"]}'>Edited Qualifications Successfully<br></c:if>
                            
                                      <c:if test='${param["showDelMsg"]}'>Deleted Qualifications Successfully<br></c:if></span>
                                </div>
                                <div class="col-xs-4 col-md-2 pull-right">
                                    <a href="#" class="btn btn-rd-cor" data-toggle="modal"
                                       data-target="#myModalAdd">Add</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Asp" id="qSearch" class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="sortNpCourses">
                            <option value="">Sort by</option>
                            <option value="courseName" <c:if test="${sortBy eq 'courseName'}">selected</c:if>>Course
                                Name
                            </option>
                            <option value="courseType" <c:if test="${sortBy eq 'courseType'}">selected</c:if>>Course
                                Type
                            </option>
                            <option value="instituteName" <c:if test="${sortBy eq 'instituteName'}">selected</c:if>>
                                Institution
                            </option>
                            <option value="valid" <c:if test="${sortBy eq 'valid'}">selected</c:if>>Valid</option>
                        </select>
                    </div>
                    <div class="sort pull-left">
                        <span class="result-pop"><c:if
                                test="${npInstituteCourseCount lt 2}">${npInstituteCourseCount} Result found</c:if><c:if
                                test="${npInstituteCourseCount gt 1}">${npInstituteCourseCount} Results found</c:if></span>
                    </div>

                </div>
            </div>

            <div class="imp-user-list">
                <div class="row no-pad">
                    <div class="tbl_panel">
                        <div class="row no-pad">
                            <div class="col-xs-4 col-md-3">
                                <div class="tbl-user">
                                    <strong>Course Name</strong>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-3">
                                <div class="tbl-name">
                                    <strong>Institution</strong>
                                </div>
                            </div>
                            <div class="col-xs-2 col-md-2">
                                <div class="tbl-name">
                                    <strong>Course Type</strong>
                                </div>
                            </div>
                            <div class="col-xs-2 col-md-2">
                                <div class="tbl-name">
                                    <strong>Valid</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <c:forEach items="${npInstituteCourses}" var="npInstituteCourse" varStatus="index">
                    <div class="row no-pad">
                        <div class="tbl_panel">
                            <div class="row no-pad">
                                <div class="col-xs-4 col-md-3">
                                    <div class="tbl-user">
                                        <input type="hidden" id="courseName${index.index}"
                                               value="${npInstituteCourse.npCourse.courseName}"/>
                                            ${npInstituteCourse.npCourse.courseName}
                                    </div>
                                </div>
                                <div class="col-xs-4 col-md-3">
                                    <div class="tbl-name">
                                        <input type="hidden" id="instituteName${index.index}"
                                               value="${npInstituteCourse.npInstitutions.id}"/>
                                            ${npInstituteCourse.npInstitutions.instituteName}
                                    </div>
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    <div class="tbl-name">
                                        <input type="hidden" id="courseType${index.index}"
                                               value="${npInstituteCourse.npCourse.courseType}"/>
                                            ${npInstituteCourse.npCourse.courseType}
                                    </div>
                                </div>
                                <div class="col-xs-2 col-md-2">
                                    <div class="tbl-name">
                                        <input type="hidden" id="valid${index.index}"
                                               value="${npInstituteCourse.npCourse.valid}"/>
                                        <c:if test="${npInstituteCourse.npCourse.valid eq 'true'}">Valid</c:if>
                                        <c:if test="${npInstituteCourse.npCourse.valid eq 'false'}">Invalid</c:if>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-2">
                                    <div class="tbl-mo-button">
                                        <a class=" btn edit-icon" id="${index.index}" data-toggle="modal"
                                           data-target="#myModal${index.index}"><img
                                                src="./resources/templating-kit/img/edit-icon.png"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>

            <c:if test="${npInstituteCourseTotalPages ge 1}">
                <div class="paging">
                    <nav role="navigation">
                        <ul class="cd-pagination">
                            <c:if test="${npInstituteCourseTotalPages ge 1 && currentPageNumber > 0}">
                                <li><a class="fristPagination" href="?pageNumber=0"><i
                                        class="fa fa-angle-double-left"></i></a></li>
                                <li><a class="prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
                            </c:if>
                            <c:forEach begin="0" end="${npInstituteCourseTotalPages-1}" varStatus="index">
                                <li>
                                    <c:choose>
                                        <c:when test="${currentPageNumber eq index.index}">
                                            <a href="?pageNumber=${index.index}"
                                               class="current currentPageNo">${index.index + 1}</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="?pageNumber=${index.index}"
                                               class="pagination">${index.index + 1}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </li>
                            </c:forEach>
                            <c:if test="${npInstituteCourseTotalPages ge 1 && currentPageNumber < npInstituteCourseTotalPages-1}">
                                <li><a class="next" href="#0"><i class="fa fa-angle-right"></i></a></li>
                                <li><a class="lastPagination" href="${npInstituteCourseTotalPages-1}"><i
                                        class="fa fa-angle-double-right"></i></a></li>
                            </c:if>
                        </ul>
                    </nav>
                </div>
            </c:if>

        </div>

    </div>
</div>

<!--Bootsrap Modal
================================================== -->
<c:forEach items="${npInstituteCourses}" var="npInstituteCourse" varStatus="index">
    <div class="modal fade" id="myModal${index.index}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title text-center" id="myModalLabel">Qualifications Edit</h3>
                </div>
                <div class="modal-body">
                    <div class="row" style="text-align:center;">
                        <span id="errPop" class="errorMessage"></span>
                    </div>
                    <div class="container-fluid">

                        <div class="row">
                            <form:form id="addNpQualificationsForm" action="?" modelAttribute="addNpQualificationForm"
                                       method="POST">
                                <form:input type="hidden" path="npInstituteCourseId" value="${npInstituteCourse.id}"/>
                                <form:input type="hidden" path="mergeId"/>
                                <div class="col-md-5 col-xs-12">
                                    <div class="bd-blue-panel">
                                        <div class="skl_btx">
                                            <fieldset>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form:select path="institutionId"
                                                                         class="form-control selectbox " id="Select19">
                                                                <option value="" style="display:none;">Select
                                                                    Institution
                                                                </option>
                                                                <c:forEach items="${npInstitutions}"
                                                                           var="npInstitution">
                                                                    <c:choose>
                                                                        <c:when test="${npInstituteCourse.npInstitutions.id eq npInstitution.id}">
                                                                            <form:option value="${npInstitution.id}"
                                                                                         selected="selected">${npInstitution.instituteName}</form:option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <form:option
                                                                                    value="${npInstitution.id}">${npInstitution.instituteName}</form:option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </form:select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form:select path="courseType"
                                                                         class="form-control selectbox " id="Select20">
                                                                <option value="" style="display:none;">Select
                                                                    CourseType
                                                                </option>
                                                                <c:forEach items="${npCourseTypes}" var="npCourseType">
                                                                    <c:choose>
                                                                        <c:when test="${npInstituteCourse.npCourse.courseType eq npCourseType}">
                                                                            <form:option value="${npCourseType}"
                                                                                         selected="selected">${npCourseType}</form:option>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <form:option
                                                                                    value="${npCourseType}">${npCourseType}</form:option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>

                                                            </form:select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form:input type="text" path="courseName"
                                                                        placeholder="Course Name" class="pp_text"
                                                                        value="${npInstituteCourse.npCourse.courseName}"/>
                                                            <!--                                                             <select name="" id="" class="form-control selectbox "> -->
                                                            <!--                                                                 <option value="1">Course Name</option> -->
                                                            <!--                                                             </select> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="radio-inline f-label" for="radios-0">
                                                                <input type="radio" name="valid" id="radio1"
                                                                       value="true"
                                                                       <c:if test="${npInstituteCourse.npCourse.valid eq 'true'}">checked="checked"</c:if>>
                                                                Valid
                                                            </label>
                                                            <label class="radio-inline f-label" for="radios-1">
                                                                <input type="radio" name="valid" id="radio2"
                                                                       value="false"
                                                                       <c:if test="${npInstituteCourse.npCourse.valid eq 'false'}">checked="checked"</c:if>>
                                                                Invalid
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group1">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="f-label">
                                                                No. of Active Candidate
                                                                : ${fn:length(npInstituteCourse.npCandidateEducations)}
                                                            </label>
                                                            <label class="f-label">
                                                                No. of Active Positions : 20
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <!--                                             <input type="submit" value="Add certification" id="addCertification" class="pp_button"> -->
                                                <%--                                         </form:form> --%>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="/qualificationManagement/removeCoCourse" id="del-url">
                                <input type="hidden" value="/qualificationManagement/editAlias" id="edit-url">

                                <div class="col-md-7 col-xs-12 coAliasDiv">
                                    <div class="bd-blue-panel-scroll">
                                        <div class="skl_btx">
                                            <!--                                           <input type="submit" value="Add certification" id="addCertification" class="pp_button"> -->
                                            <!--                                         <button class="remove" type="submit"><span class="fa fa-plus"></span></button> -->
                                                <%--                                         <form action=""> --%>
                                            <fieldset class="coAliasSection">
                                                <input type="hidden" id="coCourseAliasIndex"
                                                       value="${fn:length(npInstituteCourse.npCourse.coCourseAlias) }">
                                                <c:if test="${not empty npInstituteCourse.npCourse.coCourseAlias}">
                                                    <c:forEach items="${npInstituteCourse.npCourse.coCourseAlias}"
                                                               var="coCourseAlias" varStatus="index">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-4 col-xs-6">
                                                                    <input type="hidden"
                                                                           name="certificationAliases[${index.index}].aliasId"
                                                                           value="${coCourseAlias.courseAliasID}"
                                                                           class="aliasId${index.index} coAliasId"/>

                                                                    <form:input type="text"
                                                                                path="certificationAliases[${index.index}].aliasName"
                                                                                readonly="true" id="Text20"
                                                                                value="${coCourseAlias.courseAliasName}"
                                                                                placeholder="Alias Course Name"
                                                                                class="pp_text"/>

                                                                </div>
                                                                <div class="col-md-5">
                                                                    <form:select
                                                                            path="certificationAliases[${index.index}].aliasSource"
                                                                            disabled="true"
                                                                            class="form-control selectbox aliasSource"
                                                                            id="Select22">
                                                                        <option value="" style="display:none;">Select
                                                                            CoSourceType
                                                                        </option>
                                                                        <c:forEach items="${coSources}" var="coSource">
                                                                            <c:choose>
                                                                                <c:when test="${coCourseAlias.coSource.sourceID eq coSource.sourceID}">
                                                                                    <form:option
                                                                                            value="${coSource.sourceID}"
                                                                                            selected="selected">${coSource.sourceName}</form:option>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <form:option
                                                                                            value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach>
                                                                    </form:select>
                                                                </div>
                                                                <div class="col-md-3 col-xs-6">
                                                                    <button id="${index.index}" name="btn-send"
                                                                            class="btn btn-edt editAliasRow"
                                                                            title="Click to edit alias for this Qualification">
                                                                        <span id="editCoCourse"
                                                                              class="fa fa-pencil-square-o"></span>
                                                                    </button>
                                                                    <button id="${index.index}" name="btn-send"
                                                                            class="btn btn-edt pull-right deleteAliasRow"
                                                                            title="Click to delete alias for this Qualification">
                                                                        <span class="fa fa-trash"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </c:if>
                                                <c:if test="${empty npInstituteCourse.npCourse.coCourseAlias}">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-4 col-xs-6">
                                                                <input type="hidden"
                                                                       name="certificationAliases[0].aliasId" value=""/>
                                                                <input type="hidden" id="coCourseAliasIndex" value="1">
                                                                <!--                                                                <input type="text" class="pp_text" id="Text20" placeholder="Alias Course Name"> -->
                                                                <form:input type="text"
                                                                            path="certificationAliases[0].aliasName"
                                                                            id="Text20" placeholder="Alias Name"
                                                                            class="aliasName${index.index} pp_text"
                                                                            value=""/>

                                                            </div>
                                                            <div class="col-md-5">
                                                                <form:select path="certificationAliases[0].aliasSource"
                                                                             class="aliasSource${index.index} form-control selectbox "
                                                                             id="Select22">
                                                                    <option value="" style="display:none;">Select Alias
                                                                        Source
                                                                    </option>
                                                                    <c:forEach items="${coSources}" var="coSource">
                                                                        <form:option
                                                                                value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                                    </c:forEach>
                                                                </form:select>
                                                                <!--                                                             <select name="" id="Select22" class="form-control selectbox "> -->
                                                                <!--                                                                 <option value="1">Alias Name Source</option> -->
                                                                <!--                                                                <option value="1">Alias Name Source</option> -->
                                                                <!--                                                             </select> -->
                                                            </div>
                                                            <div class="col-md-3 col-xs-6">
                                                                <button id="${index.index}" name="btn-send"
                                                                        class="btn btn-edt pull-right deleteAliasRow">
                                                                    <span class="fa fa-trash"></span></button>
                                                                <!--                                                             <button id="Button37" name="btn-send" class="btn btn-edt"><span class="fa fa-pencil-square-o"></span></button> -->
                                                                <!--                                                             <button id="Button38" name="btn-send" class="btn btn-edt pull-right"><span class="fa fa-trash"></span></button> -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                </c:if>
                                            </fieldset>
                                        </div>
                                    </div>

                                    <div class="pull-right">
                                        <button class="btn btn-edt deleteAlias removeId" type="button"
                                                id="${npInstituteCourse.id}" title="Click to delete this Qualification">
                                            <span class="fa fa-trash"></span></button>
                                        <button class="btn btn-edt" type="submit"
                                                title="Click to save details for this Qualification"><span
                                                class="fa fa-save"></span></button>
                                        <button class="btn btn-edt addAlias"
                                                title="Click to add a new alias for this Qualification"><span
                                                class="fa fa-plus"></span></button>
                                    </div>
                                </div>

                            </form:form>

                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-md-12 quickEntryFilter">
                                <input type="text" placeholder="Search Alias Name" id="qSearch"
                                       class="form-control input-md">
                                <label for=""><i class="fa fa-search control-label"></i></label>
                            </div>
                            <div id="searchResults"></div>
                        </div>


                        <!--  <div class="row">
                             <div class="paging">
                                 <nav role="navigation">
                                     <ul class="cd-pagination">
                                         <li><a class="disabled" href="#0"><i class="fa fa-angle-double-left"></i></a></li>
                                         <li><a class="disabled" href="#0"><i class="fa fa-angle-left"></i></a></li>
                                         <li><a class="current" href="#0">1</a></li>
                                         <li><a href="#0">2</a></li>
                                         <li><a href="#0">3</a></li>
                                         <li><a href="#0">4</a></li>
                                         <li><a href="#0">5</a></li>
                                         <li><a href="#0">6</a></li>
                                         <li><a href="#0">7</a></li>
                                         <li><a href="#0">8</a></li>
                                         <li><a href="#0">9</a></li>
                                         <li><span>...</span></li>
                                         <li><a href="#0"><i class="fa fa-angle-right"></i></a></li>
                                         <li><a href="#0"><i class="fa fa-angle-double-right"></i></a></li>
                                     </ul>
                                 </nav>
                             </div>
                         </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</c:forEach>


<!-- Bootstrap Add myModal -->

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-center" id="H1">Add Qualification</h3>
            </div>
            <div class="modal-body">
                <div class="row" style="text-align:center;">
                    <span id="errPop" class="errorMessage"></span>
                </div>
                <div class="container-fluid">

                    <div class="row">
                        <form:form id="addNpQualificationsForm" action="?" modelAttribute="addNpQualificationForm"
                                   method="POST">
                            <form:input type="hidden" path="npInstituteCourseId" value=""/>
                            <div class="col-md-5 col-xs-12">
                                <div class="bd-blue-panel">
                                    <div class="skl_btx">
                                        <fieldset>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form:select path="institutionId"
                                                                     class="form-control selectbox " id="Select19">
                                                            <option value="" style="display:none;">Select Institution
                                                            </option>
                                                            <c:forEach items="${npInstitutions}" var="npInstitution">
                                                                <form:option
                                                                        value="${npInstitution.id}">${npInstitution.instituteName}</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form:select path="courseType" class="form-control selectbox "
                                                                     id="Select20">
                                                            <option value="" style="display:none;">Select CourseType
                                                            </option>
                                                            <c:forEach items="${npCourseTypes}" var="npCourseType">
                                                                <form:option
                                                                        value="${npCourseType}">${npCourseType}</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                        <!--                                                             <select name="" id="Select20" class="form-control selectbox "> -->
                                                        <!-- 															<option value="">Select CourseType</option> -->
                                                            <%--                                           					 <c:forEach items="${npCourseTypes}" var="npCourseType"> --%>
                                                            <%--                                           						  <option value="${npCourseType}" <c:if test="${CourseTypeFilter eq npCourseType}">selected</c:if>>${npCourseType}</option> --%>
                                                            <%--                                           					 </c:forEach>                                                             --%>
                                                        <!--                                           					 </select> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form:input type="text" path="courseName"
                                                                    placeholder="Course Name" class="pp_text"/>
                                                        <!--                                                             <select name="" id="Select21" class="form-control selectbox "> -->
                                                        <!--                                                                 <option value="1">Course Name</option> -->
                                                        <!--                                                             </select> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="radio-inline f-label" for="radios-0">
                                                            <input type="radio" name="valid" id="radio1" value="true"
                                                                   checked="checked">
                                                            Valid
                                                        </label>
                                                        <label class="radio-inline f-label" for="radios-1">
                                                            <input type="radio" name="valid" id="radio2" value="false">
                                                            Invalid
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <!--                                             <input type="submit" value="Add Course" id="addCourse" class="pp_button"> -->
                                            <%--                                         </form:form> --%>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-7 col-xs-12 coAliasDiv">
                                <div class="bd-blue-panel-scroll">
                                    <div class="skl_btx">
                                        <!--                                           <input type="submit" value="Add Course" id="addCourse" class="pp_button"> -->
                                        <!--                                         <button class="remove" type="submit"><span class="fa fa-plus"></span></button> -->
                                            <%--                                         <form action=""> --%>
                                        <fieldset class="coAliasSection">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 col-xs-6">
                                                        <input type="hidden" name="certificationAliases[0].aliasId"
                                                               value=""/>
                                                        <input type="hidden" id="coCourseAliasIndex" value="1">
                                                        <!--                                                                <input type="text" class="pp_text" id="Text20" placeholder="Alias Course Name"> -->
                                                        <form:input type="text" path="certificationAliases[0].aliasName"
                                                                    id="Text20" placeholder="Alias Name" class="pp_text"
                                                                    value=""/>

                                                    </div>
                                                    <div class="col-md-5">
                                                        <form:select path="certificationAliases[0].aliasSource"
                                                                     class="form-control selectbox aliasSource"
                                                                     id="Select22">
                                                            <option value="" style="display:none;">Select Alias Source
                                                            </option>
                                                            <c:forEach items="${coSources}" var="coSource">
                                                                <form:option
                                                                        value="${coSource.sourceID}">${coSource.sourceName}</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                        <!--                                                             <select name="" id="Select22" class="form-control selectbox "> -->
                                                        <!--                                                                 <option value="1">Alias Name Source</option> -->
                                                        <!--                                                                <option value="1">Alias Name Source</option> -->
                                                        <!--                                                             </select> -->
                                                    </div>
                                                    <div class="col-md-3 col-xs-6">
                                                        <!--                                                             <button id="Button37" name="btn-send" class="btn btn-edt"><span class="fa fa-pencil-square-o"></span></button> -->
                                                        <button id="${index.index}" name="btn-send"
                                                                class="btn btn-edt pull-right deleteAliasRow"
                                                                title="Click to delete alias for this Qualification">
                                                            <span class="fa fa-trash"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-edt" type="submit"
                                            title="Click to save details for this Qualification"><span
                                            class="fa fa-save"></span></button>
                                    <button class="btn btn-edt addAlias"
                                            title="Click to add a new alias for this Qualification"><span
                                            class="fa fa-plus"></span></button>
                                </div>
                            </div>

                        </form:form>
                        <input type="hidden" id="type" value="certification"/>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>






    