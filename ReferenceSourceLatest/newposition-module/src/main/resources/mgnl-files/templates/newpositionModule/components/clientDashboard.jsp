<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<link rel="stylesheet"
      href="./resources/templating-kit/css/style-vin.css"></link>
<script src="./resources/templating-kit/js/clientDashboard.js"></script>
<div class="container">
    <div class="tlm-heading cldash">

        <h2>
            <c:if test="${isImpersonate}">Impersonating <span>${user.firstName}
					${user.lastName} ..</span>
            </c:if>
            <c:if test="${not isImpersonate}">${user.firstName} ${user.lastName}</c:if>
        </h2>
        ${companyName}


    </div>
</div>
<c:set var='active' value="class='active'"/>
<c:set var="disableClick" value="href='javascript:void(0);'"/>
<c:set var="enableEmployeeTab" value="href='#employee'"/>
<c:set var="enableAdminTab" value="href='#administration'"/>
<c:set var="enableIntrvewTab" value="href='#interviewer'"/>
<c:set var="enableManagerTab" value="href='#manager'"/>
<c:set var="enableSupportTab" value="href='#support'"/>
<div class="container-fluid">

    <div class="tab_collapse_master">
        <ul id="myTab1" class="nav nav-tabs">
            <li
                    <sec:authorize access="hasRole('ROLE_CLIENT_EMPLOYEE')">${active}</sec:authorize>>
                <a
                        <sec:authorize access="hasRole('ROLE_CLIENT_EMPLOYEE')">${enableEmployeeTab}</sec:authorize>>Employee</a>
            </li>
            <li
                    <sec:authorize access="hasRole('ROLE_CLIENT_ADMINISTRATOR')">${active}</sec:authorize>><a
                    <sec:authorize access="hasRole('ROLE_CLIENT_ADMINISTRATOR')">${enableAdminTab}</sec:authorize>>Administration</a>
            </li>
            <li
                    <sec:authorize access="hasRole('ROLE_CLIENT_INTERVIEWER')">${active}</sec:authorize>><a
                    <sec:authorize access="hasRole('ROLE_CLIENT_INTERVIEWER')">${enableIntrvewTab}</sec:authorize>>Interviewer</a>
            </li>
            <li
                    <sec:authorize access="hasRole('ROLE_CLIENT_MANAGER')">${active}</sec:authorize>><a
                    <sec:authorize access="hasRole('ROLE_CLIENT_MANAGER')">${enableManagerTab}</sec:authorize>>
                Manager</a></li>
            <li
                    <sec:authorize access="hasRole('ROLE_CLIENT_SUPPORT')">${active}</sec:authorize>><a
                    <sec:authorize
                            access="hasRole('ROLE_CLIENT_SUPPORT')">${enableSupportTab}</sec:authorize>>Support</a></li>
        </ul>


        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="myTabContent" class="tab-content">
                    <div
                            class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_EMPLOYEE')">active</sec:authorize>"
                            id="employee">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png"> <span
                                                                class="notify notify-plus"></span>
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Positions</div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png"> <span
                                                                class="notify notify-plus"></span>
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Shortlist</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Positions</div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Shortlist</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png"> <span
                                                                class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text">
                                                        <a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-generic1" class="carousel slide"
                                         data-ride="carousel" data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic1"
                                           role="button" data-slide="prev"> <span
                                                class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a> <a class="right carousel-control" href="#carousel-generic1"
                                                role="button" data-slide="next"> <span
                                            class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                    </div>
                                    <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Position</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png"> <span
                                                        class="notify notify-left">3</span> <span
                                                        class="notify notify-plus"></span>
												</span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png"> <span
                                                        class="notify">3</span>
												</span>
                                            </div>
                                            <div class="black_ico_text">Reservation</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Filled Positions</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">My Application</div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">My History</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Shortlisted Positions</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Favourite Positions</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">My Application</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>


                    <div
                            class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_ADMINISTRATOR')">active</sec:authorize>"
                            id="administration">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">User Management</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box no-mar">
                                                    <div class="black_ico">
                                                        <a href="user_management"> <span class="notify_parent">
																<img src="./resources/templating-kit/img/ico11.png">
<%-- 																<span class="notify">${clientUserCount}</span> --%>
														</span>
                                                        </a>
                                                    </div>
                                                    <div class="black_ico_text no-pad-bottom">
                                                        <a href="user_management">User Management</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="black_ico_box no-mar">
                                                    <div class="black_ico">
                                                        <a href="clientRoleGroupManagement"> <span
                                                                class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                        </a>
                                                    </div>
                                                    <div class="black_ico_text no-pad-bottom">
                                                        <a href="clientRoleGroupManagement">RoleGroup
                                                            Management</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="black_ico_box no-mar">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png"> <span
                                                                class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text ">
                                                        <a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-5">
                                                <div class="black_ico_box no-mar">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png">
                                                        </a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text ">
                                                        <a href="clientMarketing">Marketing</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div class="row pad-10"></div>
                                    <div id="carousel-generic2" class="carousel slide"
                                         data-ride="carousel" data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic2"
                                           role="button" data-slide="prev"> <span
                                                class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a> <a class="right carousel-control" href="#carousel-generic2"
                                                role="button" data-slide="next"> <span
                                            class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                    </div>
                                    <!-- Carousel -->
                                    <div class="row pad-10"></div>
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">
                                                <a href='clientCompanyInformation'>Company Information</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                <a href="projects"><span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span></a>
                                            </div>
                                            <div class="black_ico_text">
                                                <a href="projects">Projects</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">
                                                <a href="npProgramme">Programs</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
                                                <a href="cost_centre"> <span class="notify_parent">
														<img src="./resources/templating-kit/img/ico18.png">
<%-- 														<span class="notify">${costCentreCount}</span> --%>
												</span>
                                                </a>
                                            </div>
                                            <div class="black_ico_text">
                                                <a href="cost_centre">Dept. Codes</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text"><a href="clientOfficeLocation">Office
                                                Locations</a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text"><a href="">Custom Fields</a></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                </div>

                            </div>
                        </div>

                        <!-- <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Office Locations</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row qf-margin">
                                    <div class="col-md-12 quickFilter">
                                        <label for=""><i class="fa fa-search control-label"></i></label>
                                        <input type="text" placeholder="Search locations" id="qSearch"
                                            class="form-control input-md">
                                    </div>
                                </div>

                            </div>
                        </div> -->

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Communication Center</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Earth Job Board</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Contact Support</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">FAQ</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Knowledge Center</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div
                            class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_INTERVIEWER')">active</sec:authorize>"
                            id="interviewer">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Interviewers</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Inbox</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png"> <span
                                                                class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text">
                                                        <a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-generic3" class="carousel slide"
                                         data-ride="carousel" data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic3"
                                           role="button" data-slide="prev"> <span
                                                class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a> <a class="right carousel-control" href="#carousel-generic3"
                                                role="button" data-slide="next"> <span
                                            class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                    </div>
                                    <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Search Jobs</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png"> <span
                                                        class="notify">6</span>
												</span>
                                            </div>
                                            <div class="black_ico_text">Applications</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png"> <span
                                                        class="notify">2</span>
												</span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png"> <span
                                                        class="notify">3</span>
												</span>
                                            </div>
                                            <div class="black_ico_text">My History</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png"> <span
                                                        class="notify">1</span>
												</span>
                                            </div>
                                            <div class="black_ico_text">Favourite Jobs</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Communication Center</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Earth Job Board</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Contact Support</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">FAQ</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Knowledge Center</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div
                            class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_MANAGER')">active</sec:authorize>"
                            id="manager">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Common Actions</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png"> <span
                                                                class="notify notify-plus"></span>
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Create Positions</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Create Shortlist</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Inbox</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png"> <span
                                                                class="notify notify-plus"></span>
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Interviewers</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Approved</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">offer</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4 col-sm-4">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png"> <span
                                                                class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text">
                                                        <a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-generic4" class="carousel slide"
                                         data-ride="carousel" data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic4"
                                           role="button" data-slide="prev"> <span
                                                class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a> <a class="right carousel-control" href="#carousel-generic4"
                                                role="button" data-slide="next"> <span
                                            class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                    </div>
                                    <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Positions</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Reservations</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Preferences</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Filled Positions</div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">On</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">My History</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Communication Center</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Earth Job Board</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Contact Support</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">FAQ</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Knowledge Center</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <div
                            class="tab-pane fade in <sec:authorize access="hasRole('ROLE_CLIENT_SUPPORT')">active</sec:authorize>"
                            id="support">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">User Management</h3>
                                    </div>
                                    <div class="blue-panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <span
                                                                class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png"> <span
                                                                class="notify">${clientUserCount}</span>
														</span>
														</span>
                                                    </div>
                                                    <div class="black_ico_text">
                                                        <a href="user_management">User Management</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Correct Workflow</div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <a
                                                                class="image_white_text2" href="referenceList"><img
                                                                src="./resources/templating-kit/img/icon1.png"> <span
                                                                class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>

														</span>
                                                    </div>
                                                    <div class="black_ico_text">
                                                        <a href="referenceList">References</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="black_ico_box">
                                                    <div class="black_ico">
														<span class="notify_parent"> <img
                                                                src="./resources/templating-kit/img/ico11.png">
														</span>
                                                    </div>
                                                    <div class="black_ico_text">Inbox</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="blue-panel">
                                    <div class="blue-panel-heading">
                                        <h3 class="blue-panel-title">Market View</h3>
                                    </div>
                                    <div id="carousel-generic5" class="carousel slide"
                                         data-ride="carousel" data-interval="false">


                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">15</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="caro_circ">
                                                    <div class="row">
                                                        <div class="crc">
															<span class="crc-m">255<br>JCR
															</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-generic5"
                                           role="button" data-slide="prev"> <span
                                                class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                        </a> <a class="right carousel-control" href="#carousel-generic5"
                                                role="button" data-slide="next"> <span
                                            class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                    </div>
                                    <!-- Carousel -->
                                </div>
                            </div>
                        </div>

                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Status</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <a
                                                        href="impersonate_user"><img
                                                        src="./resources/templating-kit/images/ico1.png"></a>
												</span>
                                            </div>
                                            <div class="black_ico_text">
                                                <a href="impersonate_user">Impersonate</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Positions</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Office Locations</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Interviews</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">My History</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="blue-panel">
                            <div class="blue-panel-heading">
                                <h3 class="blue-panel-title">Quick Links</h3>
                            </div>
                            <div class="blue-panel-body">
                                <div class="row">
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico11.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Communication Center</div>
                                        </div>
                                    </div>
                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico14.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Earth Job Board</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico15.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Contact Support</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico18.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">FAQ</div>
                                        </div>
                                    </div>

                                    <div class="col-md-15 col-sm-3">
                                        <div class="black_ico_box">
                                            <div class="black_ico">
												<span class="notify_parent"> <img
                                                        src="./resources/templating-kit/img/ico17.png">
												</span>
                                            </div>
                                            <div class="black_ico_text">Knowledge Center</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>