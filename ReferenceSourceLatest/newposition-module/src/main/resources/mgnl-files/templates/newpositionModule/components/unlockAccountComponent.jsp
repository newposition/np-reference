<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>

<div class="container">
    <center>
        <c:if test="${not empty sucessmsg}">
            <b class="sucessMessage">${sucessmsg}</b>
        </c:if>

        <c:if test="${not empty errmsg}">
            <b class="errorMessage"> ${errmsg}</b>
        </c:if>
    </center>
    <div class="container">
        <ul class="login_box">
            <li>
                <form:form id="login" action="?" commandName="resetForm" method="POST">
                    <div class="form-group">
                        <blossom:pecid-input/>
                        <form:input path="resetMail" placeholder="Email address or mobile no." id=""
                                    class="form-control"/>
                        <form:errors path="resetMail" style="float:left;" class="errorMessage"/>
                    </div>
                    <div class="form-group">
                        <input type="text" id="defaultReal" name="defaultReal"
                               placeHolder="Enter Code" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input class="lgn_btn" type="submit" value="Unlock Account">
                    </div>
                </form:form>
            </li>
        </ul>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#defaultReal').realperson({
            chars: $.realperson.alphanumeric
        });
    });
</script>