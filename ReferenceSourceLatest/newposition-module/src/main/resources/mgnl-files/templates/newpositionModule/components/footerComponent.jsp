<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4>Legal</h4>
                <ul>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">Legal Disclaimer</a></li>
                    <li><a href="#">Accessibility</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Cookie Policy</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Get to know us</h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Meet the Team</a></li>
                    <li><a href="#">In the News</a></li>
                    <li><a href="#">Testimonials</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Worth Knowing</h4>
                <ul>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Site Map</a></li>
                    <li><a href="#">Trust & Safety</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Get in touch</h4>
                <ul>
                    <li><a href="#">012 234 567 7890</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li class="social-links"><a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i
                            class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-btm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-left"> &copy; NewPosition</div>
                    <div class="pull-right"><a href="#">Back to top <i class="fa fa-chevron-up"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</footer>
