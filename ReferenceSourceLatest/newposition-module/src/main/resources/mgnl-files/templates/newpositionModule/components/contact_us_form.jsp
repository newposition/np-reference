<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cms"
        prefix="cms" %>
<%@ taglib
        uri="http://magnolia-cms.com/taglib/templating-components/cmsfn"
        prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<c:if test="${contactsucess}">
    <center>
        <b class="sucessMessage"> Thank You For Contacting us, we will get
            back to you within 48 hours.</b>
    </center>
</c:if>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form:form id="form-contact" name="contactUs" action="?"
                       commandName="contactUs" method="POST">
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">

                                <label for="fname" class="control-label">First Name*</label><br>
                                <form:input path="firstName" id="fname" placeholder="Enter first name"
                                            class="form-control input-md"/>
                                <form:errors path="firstName" Class="errorMessage"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lname" class="control-label">Last Name*</label><br>
                                <form:input path="lastName" id="lname" placeholder="Enter last name"
                                            class="form-control input-md"/>
                                <form:errors path="lastName" Class="errorMessage"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="company" class="control-label">Company</label><br>
                                <form:input path="company" id="company" placeholder="Please enter company"
                                            class="form-control input-md"/>
                                <span class="errorMessage" id="company">${wrongcompanypatrn}</span>
                            </div>
                            <div class="col-md-6">
                                <label for="telephone" class="control-label">Telephone</label><br>
                                <form:input path="telePhone" id="telephone" placeholder="Please enter telephone No"
                                            class="form-control input-md"/>
                                <span class="errorMessage" id="telephone">${wrongtelpatrn}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="email" class="control-label">Email*</label>
                                <form:input path="primaryEmailAddress" id="email" placeholder="Please enter your email"
                                            class="form-control input-md"/>
                                <form:errors path="primaryEmailAddress" Class="errorMessage"/>
                                <span class="info_email"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="select" class="control-label">Reason Code</label>
                                <form:select path="reasonForEnquiry" id="select" class="form-control selectbox">
                                    <form:option id="0" value="None">--select--</form:option>
                                    <form:options items="${reasons}" itemValue="reason" itemLabel="reason"/>
                                </form:select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="enquiry" class="control-label">Please enter some details on your enquiry so
                                    we
                                    can target the right person to get in touch with you</label><br>
                                <form:textarea path="enquiry" class="form-control form-ctrl" cols="30" rows="6"
                                               id="enquiry"/>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-send" id="btn-send" value="Submit"/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </div>
</div>