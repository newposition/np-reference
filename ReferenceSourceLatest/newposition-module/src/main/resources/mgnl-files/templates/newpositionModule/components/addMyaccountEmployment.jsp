<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="np" uri="newpositionTags" %>


<script src="./resources/templating-kit/js/employment.js"></script>
<script src="./resources/templating-kit/js/summernote.min.js"></script>
<div class="container">
    <div class='col-md-11 centered' style='text-align:center;'>
        <span id='companyNo' class='errorMessage'></span>
    </div>
    <div class="col-md-10 centered">
        <div class="row mgb20">
            <div class="col-md-6 employeRecords">
                <label class="pp_label">Employment History</label>
                <div class="em_box">
                    <div class="em_box_logo">
                        <div class="em_box_edit_logo"><a href="javascript:void(0)"></a></div>
                    </div>
                    <div class="em_box_details">
                        <div class="errorText"></div>
                        <input type="hidden" id="employerId" name="employerId" value="${employer.uid}"/>
                        <div class="line1">${employer.npEmployer.name}</div>
                        <div class="line2"><span class="line2a"><fmt:formatDate pattern="dd/MM/yyyy"
                                                                                value="${employer.startDate}"/></span>
                <span class="line2b">
                <c:if test="${employer.enddate eq null and employer.enddate ne null and employer.uid ne null}">
                    current
                </c:if>
                <c:if test="${employer.enddate ne null}">
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${employer.enddate}"/>
                </c:if>
                </span>
                            </span></div>
                        <div class="line3">${employer.jobTitle}</div>
                        <div class="line4">${employer.corporateTitle}</div>
                    </div>
                    <button class="plusbox plus_btn"></button>
                    <button class="edit edit_btn"></button>
                    <button class="save save_btn"></button>
                    <button class="trash trash_btn"></button>
                    <button class="cancel cancel_btn"></button>
                </div>
                <%--  <div class="row mgb20">
                  <c:if test="${notShowNext ne true}">
                   <div class="col-xs-6 col-md-6">
         <!--             <label id="" class="pn_label prev"><< Prev</label> -->
                   </div>
                   </c:if>
                    <c:if test="${notShowNext ne true}">
                       <div class="col-xs-6 col-md-6 text-right">
                         <label id="1" class="pn_label next">Next >></label>
                       </div>
                    </c:if>
                 </div> --%>
            </div>
            <div class="col-md-6" id="achievements">
                <label class="pp_label">Achievements</label>
                <textarea id="editor1" cols="40" rows="15">${employer.achievements}</textarea>
            </div>
        </div>
        <div class="row mgb20">
            <div class="col-md-12">
                <div class="skills_box">
                    <div class="hear">Skills <span class="companyNameSkills"> (Please select skills you used at your time with ${employer.npEmployer.name} )</span>
                    </div>
                    <div class="row center_lines">
                        <div class="col-md-6 tp-relative">
                            <div class="links_but">
                                <span class="tp-label">Top 50 skills</span>
                                <label class="pp_label">Category</label>
                                <select class="pp_select2 selectbox2" style="height:25px;" id="selectbox1">
                                    <np:listDomainOptions defaultSelect="0"/>
                                </select>

                            </div>
                            <div class="links_js links_jsonlyclick links_skills">
                                <c:forEach items="${listOfNpSkills}" var="skill">
                                    <a href="javascript:void(0)" class="get-blue"
                                       name='${skill.id}'>${skill.skillName}</a>
                                </c:forEach>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right mgb20"><span class='errorMessage skill-Btn'
                                                                          style="padding-right:20px"></span>
                                <button class="pp_button pp_button2 myBtn" id="skilBtn"><span class="fa fa-plus"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 SkillBasket">
                            <div class="links_but">
                                <label class="pp_label">Skill Basket</label>
                            </div>
                            <div class="links_js links_jsonlyclick ">
                                <c:forEach items="${npCandidate.npCandidateSkills}" var="skills" varStatus="status">
                                    <a href="javascript:void(0)" id="${skills.npSkill.id}"
                                       name="${skills.npSkill.skillName}">${skills.npSkill.skillName} ${skills.yearsOfExperience}
                                        <c:choose>
                                            <c:when test="${skills.yearsOfExperience eq 1}">Yr</c:when>
                                            <c:otherwise>Yrs</c:otherwise>
                                        </c:choose>
                                            ${skills.npExpertLevel.expertLevelName}<i class="fa fa-times"></i></a>
                                </c:forEach>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right">
                                <input type="text" id="addskillBasket" placeholder="Java 1 Beginner" class="pp_text">
                                <span class='errorMessage'></span>
                            </div>
                        </div>
                    </div>
                    <div id="skillsWithCurrentCompany" class="row top_lines mgb20">
                        <div class="col-md-12">
                            <label class="pp_label companyNameSkillsLabel">Skills
                                With ${employer.npEmployer.name}</label>
                            <div class="links_js links_jsonlyclick skillsWithCurrentCompany">
                                <%--                  <c:forEach items="${employer.npSkills}" var="skill" varStatus="status"> --%>
                                <%--                     <a href="javascript:void(0)" name="${skill.skillName}" id="${skill.id}">${skill.skillName}<i class="fa fa-times"></i></a> --%>
                                <%--                  </c:forEach> --%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mgb20">
            <div class="col-md-12">
                <div class="skills_box">
                    <div class="hear">Roles <span class="companyNameRoles">(Please select roles you used at your time with ${employer.npEmployer.name} )</span>
                    </div>
                    <div class="row center_lines">
                        <div class="col-md-6 tp-relative">
                            <div class="links_but">
                                <span class="tp-label">Top 50 Roles</span>
                                <label class="pp_label">Category</label>
                                <select class="pp_select2 selectbox2" style="height:25px;" id="selectbox2">
                                    <np:listDomainOptions defaultSelect="0"/>
                                </select>
                            </div>
                            <div class="links_js links_jsonlyclick roles">
                                <c:forEach items="${listOfNpRoles}" var="role">
                                    <a href="javascript:void(0)" class="get-blue" name='${role.id}'>${role.roleName}</a>
                                </c:forEach>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right mgb20"><span class='errorMessage role-Btn'
                                                                          style="padding-right:20px"></span>
                                <button class="pp_button pp_button2 myBtn" id="roleBtn"><span class="fa fa-plus"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 rolesBask">
                            <div class="links_but">
                                <label class="pp_label">Roles Basket</label>
                            </div>
                            <div class="links_js links_jsonlyclick ">
                                <c:forEach items="${npCandidate.npCandidateEmpRoles}" var="roles" varStatus="status">
                                    <a href="javascript:void(0)" id="${roles.npEmploymentRoles.id}"
                                       name="${roles.npEmploymentRoles.roleName}">${roles.npEmploymentRoles.roleName} ${roles.yearsOfExperience}
                                        <c:choose>
                                            <c:when test="${roles.yearsOfExperience eq 1}">Yr</c:when>
                                            <c:otherwise>Yrs</c:otherwise>
                                        </c:choose><i class="fa fa-times"></i></a>
                                </c:forEach>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right">
                                <input type="text" id="addroleBasket" placeholder="Software 2" class="pp_text"
                                       id="roles_input">
                                <span class='errorMessage'></span>
                            </div>
                        </div>
                    </div>
                    <div id="rolesWithCurrentCompany" class="row top_lines mgb20">
                        <div class="col-md-12">
                            <label class="pp_label companyNameRolesLabel">Roles With ${employer.npEmployer.name}</label>
                            <div class="links_js links_jsonlyclick rolesWithCurrentCompany">
                                <%--                  <c:forEach items="${employer.npEmploymentRoles}" var="role" varStatus="status"> --%>
                                <%--                     <a href="javascript:void(0)" name="${role.roleName}" id="${role.id}">${role.roleName}<i class="fa fa-times"></i></a> --%>
                                <%--                  </c:forEach> --%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mgb20">
            <div class="col-md-12">
                <div class="skills_box">
                    <div class="hear">Tools &amp; Systems <span class="companyNameTools"> (please select Tools & Systems you used at your time with ${employer.npEmployer.name} ) </span>
                    </div>
                    <div class="row center_lines">
                        <div class="col-md-6 tp-relative">
                            <div class="links_but">
                                <span class="tp-label">Top 50 Tools & Systems</span>
                                <label class="pp_label">Category</label>
                                <select class="pp_select2 selectbox2" style="height:25px;" id="selectbox3">
                                    <np:listDomainOptions defaultSelect="0"/>
                                </select>
                            </div>
                            <div class="links_js links_jsonlyclick tools">
                                <%-- 			     <c:forEach items="${listOfNpTools}" var="tool"> --%>
                                <%--                    <a href="javascript:void(0)" class="get-blue" name='${tool.id}'>${tool.toolName}</a> --%>
                                <%--                  </c:forEach> --%>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right mgb20"><span class='errorMessage tool-Btn'
                                                                          style="padding-right:20px"></span>
                                <button class="pp_button pp_button2 myBtn" id="toolBtn"><span class="fa fa-plus"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 toolsBask">
                            <div class="links_but">
                                <label class="pp_label">Tools & Systems Basket</label>
                            </div>
                            <div class="links_js links_jsonlyclick">
                                <c:forEach items="${npCandidate.npCandidateTools}" var="tools" varStatus="status">
                                    <a href="javascript:void(0)" id="${tools.npTools.id}"
                                       name="${tools.npTools.toolName}">${tools.npTools.toolName} ${tools.yearsOfExperience}
                                        <c:choose>
                                            <c:when test="${tools.yearsOfExperience eq 1}">Yr</c:when>
                                            <c:otherwise>Yrs</c:otherwise>
                                        </c:choose>
                                            ${tools.npExpertLevel.expertLevelName}<span></span><i
                                                class="fa fa-times"></i></a>
                                </c:forEach>
                            </div>
                            <div class="clear"></div>
                            <div class="links_but text-right mgb20">
                                <input type="text" id="addtoolBasket" placeholder="Jenkins 1 Beginner" class="pp_text"
                                       id="tools_input">
                                <span class='errorMessage'></span>
                            </div>
                        </div>
                    </div>
                    <div id="toolsWithCurrentCompany" class="row top_lines mgb20">
                        <div class="col-md-12">
                            <label class="pp_label companyNameToolsLabel">Tools
                                With ${employer.npEmployer.name} </label>
                            <div class="links_js links_jsonlyclick toolsWithCurrentCompany">
                                <%--                   <c:forEach items="${employer.npTools}" var="tool" varStatus="status"> --%>
                                <%--                     <a href="javascript:void(0)" name="${tool.toolName}" id="${tool.id}">${tool.toolName}<i class="fa fa-times"></i></a> --%>
                                <%--                  </c:forEach>  --%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--   <input type="hidden" value="${employer.npEmployer.name}" class="companyNameValue"/> --%>
<input type="hidden" value="" class="companyNameValue"/>

<div class="modal fade model_popup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span class='skill-Modal errorMessage'></span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<c:set var="hidden" value="hidden='true'"/>
<div class="row mgb20">
    <div class="col-md-10 col-md-offset-1">
        <button onclick="window.location='myaccount'" class="btn btn-send" name="btn-send" id="btn-send">Save</button>
    </div>
</div>

