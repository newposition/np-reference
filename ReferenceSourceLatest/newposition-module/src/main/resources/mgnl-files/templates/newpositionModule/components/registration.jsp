<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<div class="container">
    <div class="col-md-11 centered bottom_50 tpf_space">
        <div class="row mgb20">
            <div class="col-md-2"><a href="javascript:void(0)" class="Link_norac Link_norac-a">Me</a></div>
            <div class="col-md-3"><a href="javascript:void(0)" class="Link_nor">My Career History</a></div>
            <div class="col-md-3"><a href="javascript:void(0)" class="Link_nor">My Education</a></div>
            <div class="col-md-2"><a href="javascript:void(0)" class="Link_nor">My Sponsors</a></div>
            <div class="col-md-2"><a href="javascript:void(0)" class="Link_nor">My Future</a></div>
        </div>
    </div>
    <div class="col-md-6 centered">
        <div class="row">
            <div class="col-md-12">
                <b class="errorMessage">${cpm}</b>
                <b class="errorMessage">${epm}</b>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12"><p class="text-right import_via">Import via <img
                    src="./resources/templating-kit/images/IN.png" width="28" height="29"></p>

            </div>
        </div>

        <form:form id="form-contact" action="?" commandName="quickRegistrationForm" method="POST">
            <fieldset>
                <legend><label class="qr_legend ">* - mandatory field</label></legend>
                <div class="form-group">
                    <blossom:pecid-input/>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="fname" class="control-label">First Name*</label>
                            <form:input path="candidate.firstName" placeholder="FirstName"
                                        class="form-control input-md"/>
                            <form:errors path="candidate.firstName" class="errorMessage"/>
                        </div>
                        <div class="col-md-6">
                            <label for="lname" class="control-label">Last Name*</label>
                            <form:input path="candidate.lastName" placeholder="LastName" class="form-control input-md"/>
                            <form:errors path="candidate.lastName" class="errorMessage"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="pemail" class="control-label">Primary Email Address*</label>
                            <form:input path="candidate.primaryEmail" placeholder="Enter your primary Email Address"
                                        class="form-control input-md"/>
                            <form:errors path="candidate.primaryEmail" class="errorMessage"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="cemail" class="control-label">Confirm Email Address*</label>
                            <input type="text" name="confirmPrimaryEmailAddress"
                                   placeholder="Confirm primary Email Address" class="form-control input-md"/>
                        </div>
                        <form:errors path="confirmPrimaryEmailAddress" class="errorMessage"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="password" class="control-label">Password*</label>
                            <form:password path="password" placeholder="Enter a password"
                                           class="form-control input-md"/>
                        </div>
                        <div class="col-md-6">
                            <label for="cpassword" class="control-label">Confirm Password*</label>
                            <input type="password" name="confirmPassword" placeholder="Confirm your Password"
                                   class="form-control input-md"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form:errors path="password" class="errorMessage"/>
                            <form:errors path="confirmPassword" class="errorMessage"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="pp_label">Mobile number</label>
                            <form:input type="text" placeholder="Enter mobile number" path="candidate.phone"
                                        class="pp_text" maxlength="16"/>
                            <form:errors path="candidate.phone" class="errorMessage"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="cjtitle" class="control-label">Current Role</label>
                            <form:input path="candidate.jobTitle" placeholder="Current Job Title"
                                        class="form-control input-md"/>
                            <form:errors path="candidate.jobTitle" class="errorMessage"/>
                            <span class="errorMessage" id="jobtitle">${wrongJtitlepatrn}</span>
                        </div>
                        <div class="col-md-6">
                            <label for="cemployer" class="control-label">Current Employer</label>
                            <form:input path="candidate.currentCompany" placeholder="Current Employer"
                                        class="form-control input-md"/>
                            <form:errors path="candidate.currentCompany" class="errorMessage"/>
                            <span class="errorMessage" id="company">${wrongcompanypatrn}</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <input type="text" id="defaultReal" name="defaultReal" placeHolder="Enter Code"
                                   class="form-control input-md"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="checkbox-inline" for="agree">
                                <input type="checkbox" name="agree" id="agree" class="agree" value="1">I agree to the
                                terms and conditions
                            </label><br>
                            <form:errors path="agree" class="errorMessage"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Activate account" class="quickRegBtn btn btn-confirm">
                        </div>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#defaultReal').realperson({
            chars: $.realperson.alphanumeric
        });
    });
</script>
    
