<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<script src="./resources/templating-kit/js/summernote.min.js"></script>
<script src="./resources/templating-kit/js/mySponsorForm.js"></script>

<div class="container">
    <div class="col-md-11 centered bottom_50 tpf_space">
        <div class="row">
            <div class="col-md-2">
                <a class="Link_nordc  Link_norac-a" href="javascript:void(0)">Me</a>
            </div>
            <div class="col-md-3">
                <a class="Link_nordc" href="javascript:void(0)">My Career History</a>
            </div>
            <div class="col-md-3">
                <a class="Link_nordc" href="javascript:void(0)">My Education</a>
            </div>
            <div class="col-md-2">
                <a class="Link_norac Link_norac-a" href="javascript:void(0)">My Sponsors</a>
            </div>
            <div class="col-md-2">
                <a class="Link_nor" href="javascript:void(0)">My Future</a>
            </div>
        </div>
    </div>
    <div class="col-md-11 centered">
        <center>
            <span class="errorMessage">${errMsg}</span> <label>${sucesMsg}</label>
        </center>
        <form:form action="?" id="sponsorForm" commandName="npCandidateReferenceForm" method="POST" class="sponsors_b">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <form:input path="firstName" placeholder="Enter First name" class="pp_text"/>
                            <form:errors path="firstName" class="errorMessage"/>
                            <span id="info_fname" class="errorMessage"></span>
                        </div>
                        <div class="col-md-6">
                            <form:input path="surName" placeholder="Enter surname" class="pp_text"/>
                            <form:errors path="surName" class="errorMessage"/>
                            <span id="info_sname" class="errorMessage"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form:select type="text" path="relationship" placeholder="Enter Relationship"
                                         class="pp_text">
                                <form:option value="" label="Select relationship"/>
                                <form:option value="Personal" label="Personal"/>
                                <form:option value="Professional" label="Professional"/>
                            </form:select>
                            <form:errors path="relationship" class="errorMessage"/>
                            <span id="info_relationship" class="errorMessage"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form:input path="email" placeholder="Enter your email address" class="pp_text"/>
                            <form:errors path="email" class="errorMessage"/>
                            <span id="info_email" class="errorMessage">${errEmail}</span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <form:input path="dateFrom" type="text" class="datepicker-12 pp_text"
                                            placeholder="Date From" id=""/>
                                <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                            </div>
                            <form:errors path="dateFrom" class="errorMessage"/>
                            <span id="info_fromdate" class="errorMessage"></span>
                        </div>
                        <div class="col-md-6">
                            <span class="akt hidden-lg">&nbsp;</span>
                            <div class="input-group">
                                <form:input path="dateTo" type="text" class="datepicker-12 pp_text"
                                            placeholder="Date To" id=""/>
                                <span class="input-group-addon addon-custom"><span class="fa fa-calendar"></span></span>
                            </div>

                            <form:errors path="dateTo" class="errorMessage"/>
                        </div>
                        <div class='col-md-12'>
                            <span id="info_todate" class="errorMessage">${errDate}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6" id="coveringNoteText">
                    <div class="row">
                        <div class="col-md-12">
                            <form:textarea path="coveringNote" placeholder="Additional Comments"
                                           class="pp_textarea pp_textarea_height" maxlength='200'/>
                        </div>
                        <div class='col-md-12'>
                            <span id="info_stmt" class="errorMessage"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="submit" value="Add" class="pp_button">
                        </div>

                    </div>
                </div>
            </div>
        </form:form>

        <c:forEach var="mySponser" items="${referenceList}" varStatus="loop">
            <c:set var="showbtn" value="${loop.index}"/>
            <form action="${pageContext.request.contextPath}/mySponsor" id="sponsorEditForm${loop.index}" method="POST"
                  class="sponsors_b">
                <input name="edit" value="edit" type="hidden"/> <input type="hidden" name="spid"
                                                                       value="${mySponser.id}"/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="_firstName" placeholder="Enter FirstName" class="pp_text"
                                       value="${mySponser.firstName}" disabled="disabled" maxlength="20"/>
                                <span id="info_fname" class="errorMessage"></span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="_surName" placeholder="Enter Surname" class="pp_text"
                                       value="${mySponser.surName}"
                                       disabled="disabled" maxlength="20"/> <span id="info_sname"
                                                                                  class="errorMessage"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <select name="_relationship" disabled="disabled" class="pp_text">
                                    <option value="Personal" ${mySponser.relationship eq 'Personal' ? 'selected' : '' }>
                                        Personal
                                    </option>
                                    <option value="Professional" ${mySponser.relationship eq 'Professional' ? 'selected' : '' }>
                                        Professional
                                    </option>
                                </select> <span id="info_relationship" class="errorMessage"></span>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" name="_email" placeholder="Enter email address" class="pp_text"
                                       value="${mySponser.email}"
                                       disabled="disabled" maxlength="128"/>
                                <c:if test="${mySponser.id eq spid}">
                                    <span id="info_email" class="errorMessage">${errEditEmail}</span>
                                </c:if>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" name="_dateFrom" class="datepicker-12 pp_text "
                                           placeholder="Date From" id=""
                                           value="${mySponser.dateFrom}" disabled="disabled"> <span
                                        class="input-group-addon addon-custom"><span
                                        class="fa fa-calendar"></span></span>
                                </div>
                                <span id="info_fromdate" class="errorMessage"></span>
                            </div>
                            <div class="col-md-6">
                                <span class="akt hidden-lg">&nbsp;</span>
                                <div class="input-group">
                                    <input type="text" name="_dateTo" class="datepicker-12 pp_text "
                                           placeholder="Date To" id=""
                                           value="${mySponser.dateTo}" disabled="disabled"> <span
                                        class="input-group-addon addon-custom"><span
                                        class="fa fa-calendar"></span></span>
                                </div>

                            </div>
                            <div class='col-md-12'>
                                <c:if test="${mySponser.id eq spid}">
                                    <span id="info_email" class="errorMessage">${errDateEdit}</span>
                                </c:if>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6" id='coveringNoteText'>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea disabled="disabled" class="pp_textarea pp_textarea_height"
                                          placeholder="Additional Comments"
                                          name="_coveringNote" maxlength="128">${mySponser.coveringNote}</textarea>
                                <span id="info_stmt" class="errorMessage"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <input id="update" type="submit" value="Edit" class="pp_button">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </c:forEach>
    </div>
    <c:set var="hidden" value="hidden='true'"/>
    <div class="col-md-12 text-center">
        <form id="invite" action="ajax/inviteAll" method="post">
            <input type="hidden" name="inviteall" value="send"> <input id="inviteAll" type="submit" class="pp_button"
                                                                       value="Invite All" ${showbtn ge 0 ? '' : hidden}>
        </form>
    </div>
</div>
<div class="browser_next">
    <a href="myFuture"><img src="./resources/templating-kit/images/blank.png" ${showbtn ge 0 ? '' : hidden}></a>
</div>
<div class="browser_prev">
    <a href="myEducation"><img src="./resources/templating-kit/images/blank.png"></a>
</div>
