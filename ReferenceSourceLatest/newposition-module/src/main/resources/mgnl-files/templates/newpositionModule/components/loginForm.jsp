<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<script src="./resources/templating-kit/js/loginForm.js"></script>

<div class="container">
    <center>
        <c:if test="${not empty successmsg}">
            <b class="sucessMessage">${successmsg}</b>
        </c:if>

        <c:if test="${lock}">
            <b class="errorMessage">Your Account Has Been Locked.Reason has been sent to the Registered Email.</b>
            <br>
            &nbsp;<span id="resend" class="resend_Email_link">Click here to Re-send Link</span>
        </c:if>

        <c:if test="${disabled}">
            <b class="errorMessage">Your Account Has Been Disabled.Reason has been sent to the Registered Email.</b>
            <!-- <br>
            &nbsp;<span id="resend" class="resend_Email_link">Click here to Re-send Link</span> -->
        </c:if>

        <c:if test="${not empty errmsg}">
            <b class="errorMessage">${errmsg}</b>
        </c:if>

    </center>

    <ul class="login_box">
        <li>
            <form:form id="login" action="j_spring_security_check" commandName="loginForm" method="POST">
                <blossom:pecid-input/>

                <div class="form-group">
                    <form:input path="j_username" id="j_username" placeholder="Username" class="form-control"/>
                    <c:if test="${not empty errmsgUsn}">
                        <span id="err_j_username" class="errorMessage">${errmsgUsn}</span>
                    </c:if>
                </div>
                <div class="form-group">
                    <form:password path="j_password" id="j_password" placeholder="Password" class="form-control"/>
                    <c:if test="${not empty errmsgPwd}">
                        <span id="err_j_password" class="errorMessage">${errmsgPwd}</span>
                    </c:if>
                </div>
                <div class="form-group">
                    <button class="lgn_btn" type="submit">Login</button>
                </div>
                <hr>
                <div class="form-group form_group_gap_5">
                    <a href="registration" class="lgn_hrf text-center">Register</a>
                </div>
                <div class="form-group form_group_gap_5">
                    <a href="forgottenPassword" class="lgn_hrf text-center">Forgotten Password</a>
                </div>
            </form:form>

        </li>
    </ul>
</div>

<form id="sendEmailForm" action="${pageContext.request.contextPath }/home" style="display: none">
    <input name="recentEmail" value="${recentEnterEmail}"/>
</form>


