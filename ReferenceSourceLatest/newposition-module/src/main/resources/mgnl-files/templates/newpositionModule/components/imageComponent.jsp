<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>

<div class="${cmsfn:decode(content).css}">
    <c:set var="imgdesc" value="${cmsfn:decode(content).imageDescription}"></c:set>
    <img src="${cmsfn:link(content.image)}" class="picture_img" alt="">
    <c:if test="${not empty  imgdesc}">
        <span class="img_caption">${cmsfn:decode(content).imageDescription}</span></c:if>
</div>
  
   