<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script src="./resources/templating-kit/js/referenceFormPage.js"></script>

<div class="container">
    <div class="tlt-heading">

        <h2>Reference request
            for ${npCandidateReference.npCandidate.firstName} ${npCandidateReference.npCandidate.lastName}</h2>
        Please fill out the form below, lorem ipsum sit dolor

    </div>
</div>
<hr>


<div class="container referenceForm">
    <div class="row">
        <form id="form-contact" action="candidateReferenceForm" method="POST">
            <input type="hidden" name="candidateId" value="${id}">
            <input type="hidden" name="referenceVerified" value="${npCandidateReference.referenceVerified}">
            <fieldset>
                <div class="col-md-8 col-md-offset-2">

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="candidatefname" class="control-label">First Name*</label>
                                        <input id="candidatefname" value="${npCandidateReference.npCandidate.firstName}"
                                               name="firstName" type="text" placeholder="First Name"
                                               class="form-control input-md">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="candidatelname" class="control-label">Last Name*</label>
                                        <input id="candidatelname" value="${npCandidateReference.npCandidate.lastName}"
                                               name="lastName" type="text" placeholder="Last Name"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="candidatejtitle" class="control-label">Job Title*</label>
                                        <input id="candidatejtitle" value="${npCandidateReference.npCandidate.jobTitle}"
                                               name="jobTitle" type="text" placeholder="Job Title"
                                               class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <fmt:formatDate var="fromDate" pattern='dd/MM/yyyy'
                                                        value='${npCandidateReference.dateFrom}'/>
                                        <label for="sdate" class="control-label">Start date*</label>
                                        <div class="input-group">
                                            <input id="sdate" name="dateFrom" value="${fromDate}" type="text"
                                                   placeholder="From Date" class="form-control datepicker-12">
                                            <span class="input-group-addon addon-custom"><span
                                                    class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="edate" class="control-label">End date*</label>
                                        <div class="input-group">
                                            <c:choose>
                                                <c:when test="${npCandidateReference.dateTo ne null}">
                                                    <fmt:formatDate var="dateTo" pattern='dd/MM/yyyy'
                                                                    value='${npCandidateReference.dateTo}'/>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="dateTo" value="current"></c:set>
                                                </c:otherwise>
                                            </c:choose>

                                            <input id="edate" name="dateTo" value="${dateTo}" type="text"
                                                   placeholder="To Date" class="form-control datepicker-12">
                                            <span class="input-group-addon addon-custom"><span
                                                    class="fa fa-calendar"></span></span>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="reference" class="control-label">Reference*</label>
                                        <textarea name="coveringNote" id="reference" class="form-control form-ctrl"
                                                  cols="30" rows="6">${npCandidateReference.coveringNote}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="skills_box sk-grp">
                                <div class="hear">Skills</div>
                                <div class="row">
                                    <div class="links_js links_jsonlyclick text-center sk-table">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-1 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Agree
                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Disagree
                                                </div>
                                                <div class="col-xs-3 b_head">
                                                    Not Observed
                                                </div>
                                            </div>
                                            <c:forEach items="${npCandidateReference.npCandidate.npCandidateSkills}"
                                                       var="candidateSkill" varStatus="index">
                                                <div class="row">
                                                    <div class="col-xs-2 b_bg">
                                                            ${candidateSkill.npSkill.skillName}
                                                    </div>
                                                    <div class="col-xs-1 b_bg">
                                                            ${candidateSkill.yearsOfExperience} yrs.
                                                    </div>
                                                    <div class="col-xs-2 b_bg">
                                                            ${candidateSkill.npExpertLevel.expertLevelName}
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <c:set var="group"
                                                               value="${candidateSkill.npSkill.skillName}"></c:set>
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="skills_box sk-grp">
                                <div class="hear">Roles</div>
                                <div class="row">
                                    <div class="links_js links_jsonlyclick text-center sk-table">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-1 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Agree
                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Disagree
                                                </div>
                                                <div class="col-xs-3 b_head">
                                                    Not Observed
                                                </div>
                                            </div>
                                            <c:forEach items="${npCandidateReference.npCandidate.npCandidateEmpRoles}"
                                                       var="candidateRoles" varStatus="index1">
                                                <div class="row">
                                                    <div class="col-xs-2 b_bg">
                                                            ${candidateRoles.npEmploymentRoles.roleName}
                                                    </div>
                                                    <div class="col-xs-1 b_bg">
                                                            ${candidateRoles.yearsOfExperience} yrs.
                                                    </div>
                                                    <!-- <div class="col-xs-2 b_bg">
                                                        Experience
                                                    </div> -->
                                                    <div class="col-xs-2">
                                                        <c:set var="group"
                                                               value="${candidateRoles.npEmploymentRoles.roleName}"></c:set>
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="skills_box sk-grp">
                                <div class="hear">Tools & Systems</div>
                                <div class="row">
                                    <div class="links_js links_jsonlyclick text-center sk-table">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-1 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">

                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Agree
                                                </div>
                                                <div class="col-xs-2 b_head">
                                                    Disagree
                                                </div>
                                                <div class="col-xs-3 b_head">
                                                    Not Observed
                                                </div>
                                            </div>
                                            <c:forEach items="${npCandidateReference.npCandidate.npCandidateTools}"
                                                       var="candidateTools" varStatus="index1">
                                                <div class="row">
                                                    <div class="col-xs-2 b_bg">
                                                            ${candidateTools.npTools.toolName}
                                                    </div>
                                                    <div class="col-xs-1 b_bg">
                                                            ${candidateTools.yearsOfExperience} yrs.
                                                    </div>
                                                    <div class="col-xs-2 b_bg">
                                                            ${candidateTools.npExpertLevel.expertLevelName}
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <c:set var="group"
                                                               value="${candidateTools.npTools.toolName}"></c:set>
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="radio">
                                                    <span>
                                                      <input type="radio" name="${group}" checked="" id="radio">
                                                      <label for="radio"></label>
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                    <hr>

                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="reference" class="control-label">Additional Comments</label>
                                        <textarea name="reference" id="additionalcomments"
                                                  class="form-control form-ctrl" cols="30"
                                                  rows="6">Enter your comments</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-1">
                                        <input type="button" id="btn-send" name="btn-send" class="btn btn-flat"
                                               value="Submit Reference">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="button" id="Button1" name="btn-send" class="btn btn-flat"
                                               value="Prefer not to Give Reference">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>


  