<html lang="en">
<head>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    <%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
    <%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
    <%@ taglib uri="blossom-taglib" prefix="blossom" %>

    <title>New Position</title>

    <!-- Bootstrap core CSS -->
    <link href="./resources/templating-kit/css/bootstrap.css" rel="stylesheet">
    <link href="./resources/templating-kit/css/bootstrap-switch.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./resources/templating-kit/css/style.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/font-awesome.css" rel="stylesheet"/>
    <link href="./resources/templating-kit/css/jquery-ui.css" rel="stylesheet"/>

    <link href="./resources/templating-kit/js/jquery.magnific-popup.min.js" rel="stylesheet"/>

    <link href="./resources/templating-kit/css/summernote.css" rel="stylesheet">


    <script src="./resources/templating-kit/js/jquery1.11.2.js"></script>
    <script src="./resources/templating-kit/js/jquery.plugin.js"></script>
    <script src="./resources/templating-kit/js/jquery-ui.js"></script>
    <script src="./resources/templating-kit/js/jquery.circliful.min.js"></script>
    <script src="./resources/templating-kit/js/jquery.realperson.js"></script>
    <script src="./resources/templating-kit/js/bootstrap.js"></script>
    <script src="./resources/templating-kit/js/bootstrap-tabcollapse.js"></script>
    <script src="./resources/templating-kit/js/headerComponent.js"></script>
    <cms:init/>

</head>

<body>
<cms:area name="header"/>

<cms:area name="main"/>

<cms:area name="footer"/>
</body>
</html>
