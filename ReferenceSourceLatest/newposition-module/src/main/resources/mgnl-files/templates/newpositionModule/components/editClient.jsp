<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="np" uri="newpositionTags" %>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div id="errormessages">
                <center><label class="errorMessage"><h4>${errorMsg}</h4></label></center>
            </div>
            <form:form id="addClient" action="?" method="POST" commandName="form" enctype="multipart/form-data">
                <fieldset>
                    <legend class="tl_legend">Client Details</legend>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">

                                <div class="add_logo logoName myimg">
                                    <img src='<c:if test='${not empty company}'>data:image/*;base64,${company}</c:if>'
                                         class="img-responsive" alt="">
                                    <span> Add / change image / logo</span>
                                    <form:input path="companyLogo" type="file" class="" accept=".png,.jpg,.gif"
                                                id="uploadBtn1"/>
                                </div>
                            </div>

                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <label for="clientStatusId" class="control-label">Account Status</label>
                                    <%-- <form:select path="clientStatusId"
                                        class="form-control selectbox " disabled="true"  >
                                        <c:if test="${not empty form.clientStatusId}">
                                        <np:listStatusOptions defaultSelect="${form.clientStatusId}"/>
                                        </c:if>
                                        <c:if test="${empty form.clientStatusId}">
                                        <np:listStatusOptions defaultSelect="${3}" />
                                        </c:if>
                                    </form:select> --%>
                                <select
                                        class="form-control selectbox " disabled="disabled">
                                    <c:if test="${not empty form.clientStatusId}">
                                        <np:listStatusOptions defaultSelect="${form.clientStatusId}"/>
                                    </c:if>
                                    <c:if test="${empty form.clientStatusId}">
                                        <np:listStatusOptions defaultSelect="${5}"/>
                                    </c:if>
                                </select>
                                <form:hidden path="clientStatusId" value="${form.clientStatusId}"/>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" name="id" value="${form.id}"/>
                                <label for="companyName" class="control-label">Company Name</label> <form:input
                                    path="companyName" type="text"
                                    placeholder="Enter Company Name" class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="sectorId" class="control-label">Sector</label> <form:select path="sectorId"
                                                                                                        class="form-control selectbox ">
                                <np:listSectorOptions defaultSelect="${form.sectorId}" defaultValue="Select Sector"/>
                            </form:select>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <legend class="tl_legend">Principal Account Contact</legend>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="firstName" class="control-label">First Name</label> <form:input
                                    path="firstName" type="text"
                                    placeholder="Enter First Name" class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="lastName" class="control-label">Last Name</label> <form:input
                                    path="lastName" type="text"
                                    placeholder="Enter Last Name" class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="principleContactEmail" class="control-label">Principle Contact Email
                                    Address*</label> <form:input path="principleContactEmail"
                                                                 type="text" placeholder="Enter your Email Address"
                                                                 class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="teleNumber" class="control-label">Tel. no.</label> <form:input
                                    path="teleNumber" type="text"
                                    placeholder="Enter tel. no." class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="mobileNumber" class="control-label">Mobile no.</label> <form:input
                                    path="mobileNumber" type="text"
                                    placeholder="Enter Mobile no." class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="address1" class="control-label">Address 1</label> <form:input
                                    path="address1" type="text"
                                    placeholder="10 Lorem Cresent" class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="address2" class="control-label">Address 2</label> <form:input
                                    path="address2" type="text"
                                    placeholder="Lorem Road" class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="town" class="control-label">Town</label> <form:input path="town" type="text"
                                                                                                 placeholder="Enter Town"
                                                                                                 class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="country" class="control-label">Country</label> <form:input path="country"
                                                                                                       type="text"
                                                                                                       placeholder="Enter Country"
                                                                                                       class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="postCode" class="control-label">Postcode</label> <form:input path="postCode"
                                                                                                         type="text"
                                                                                                         placeholder="PW118WW"
                                                                                                         class="form-control input-md"/>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <legend class="tl_legend">Administration Details</legend>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="adminFirstName" class="control-label">First Name</label> <form:input
                                    path="adminFirstName" type="text"
                                    placeholder="Enter First Name" class="form-control input-md"/>
                            </div>
                            <div class="col-md-6">
                                <label for="adminLastName" class="control-label">Last Name</label> <form:input
                                    path="adminLastName" type="text"
                                    placeholder="Enter Last Name" class="form-control input-md"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="adminEmail" class="control-label">Email Address</label> <form:input
                                    path="adminEmail" type="text"
                                    placeholder="Enter your Email Address" class="form-control input-md"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="adminPassword" class="control-label">Password</label> <form:input
                                    path="adminPassword" type="password"
                                    placeholder="Enter Password" class="form-control input-md" disabled="true"/>
                            </div>
                            <div class="col-md-6">
                                <label for="adminConfirmPassword" class="control-label">Confirm Password</label>
                                <form:input path="adminConfirmPassword" type="password"
                                            placeholder="Confirm Password" class="form-control input-md"
                                            disabled="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>

                    <hr>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <input value="Cancel" class="btn btn-flat" onclick="backtopage();">
                            </div>
                            <div class="col-md-6">
                                <input id="addClientButton" type="submit" value="Submit" class="btn btn-flat col-xs-11">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form:form>
        </div>


    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="./resources/templating-kit/js/jquery.plugin.js"></script> -->
<script src="./resources/templating-kit/js/jquery.validate.min.js"></script>
<script src="./resources/templating-kit/js/additional-methods.js"></script>
<script src="./resources/templating-kit/js/addEditClientLiason.js"></script>

<script type="text/javascript">
    function backtopage() {
        window.history.back();
    }
</script>