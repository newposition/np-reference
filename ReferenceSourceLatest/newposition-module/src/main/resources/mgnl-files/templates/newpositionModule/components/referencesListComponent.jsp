<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script
        src="./resources/templating-kit/js/referenceListPage.js"></script>


<div class="referenceList">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="row1">
                    <form class="filterby" action="">
                        <fieldset>
                            <legend>Fiter result by:</legend>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <select class="form-control selectbox filByName" id="" name="">
                                            <option value="">select</option>
                                            <option value="Name"
                                                    <c:if test="${sortBy eq 'Name'}">selected</c:if>>Name
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-10">
                                        <select class="form-control selectbox " id="" name="">
                                            <option value="1">Job</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

            <div class="col-md-9">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" class="form-control input-md" id="qSearch"
                                           placeholder="Search">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <c:if test="${fn:length(candidateReferences) eq 0}">

                    <div class="">
                        <div class="row no-pad">
                            <div class="col-md-10"> You dont have any reference. <br> Please <a class="sucessMessage"
                                                                                                href="home"> back </a>
                                to dashboard
                            </div>
                        </div>
                    </div>
                </c:if>
                <c:forEach items="${candidateReferences}" var="referCandidate"
                           varStatus="index">
                    <div class="dtl_panel">
                        <div class="row no-pad">
                            <div class="col-md-10">
                                <div class="dtl_list">
                                    <ul>
                                        <li><span class="col-md-3">First Name:</span>${referCandidate.firstName}</li>
                                        <li><span class="col-md-3">Last Name:</span>${referCandidate.lastName}</li>
                                        <li><span class="col-md-3">Email ID:</span>${referCandidate.primaryEmail}</li>
                                        <li><span class="col-md-3">Job Title:</span>${referCandidate.jobTitle}</li>
                                    </ul>
                                </div>
                                <div class="dtl_img">
                                    <img class="img-responsive-margin img-responsive"
                                         src="./resources/templating-kit/img/man_shortlist.png">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="dtl_tab">
                                    <form action="?" id="viewReferenceForm" method="POST">
                                        <input type="hidden" name="userId"
                                               value="${referCandidate.id}"> <i
                                            class="fa fa-list-alt"></i> <a href="#" id="verifyDetailsBtn">Verify
                                        Details</a>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>
    </div>
</div>