<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<div align="center" style="color:blue;">
    ${error1}
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <form:form id="form-contact" action="?" commandName="npMailConfigProp" method="POST">
            <fieldset>

                <div class="form-group">
                    <blossom:pecid-input/>
                    <div class="row">
                        <div class="col-md-5">
                            <label for="fname" class="control-label">Property Name*</label>
                            <form:input path="npMailUserName" placeholder="Variable Name"
                                        class="form-control input-md"/>
                            <form:errors path="npMailUserName" class="errorMessage"/>
                        </div>
                        <div class="col-md-5">
                            <label for="lname" class="control-label">Property Value*</label>
                            <form:input path="npMailPassword" placeholder="Value" class="form-control input-md"/>
                            <form:errors path="npMailPassword" class="errorMessage"/>
                        </div>
                        <div class="col-md-2">
                            <label>*</label>
                            <input type="submit" value="Submit" class="btn btn-confirm">
                        </div>
                    </div>
                </div>
                    <%--   <div class="form-group">
                          <div class="row">
                            <div class="col-md-6">
                             <label for="pemail" class="control-label">Primary Email Address*</label>
                             <form:input path="javaMailProperties.mailSmtpHost" placeholder="Enter your primary Email Address" class="form-control input-md" />
                             <form:errors path="javaMailProperties.mailSmtpHost" class="errorMessage" />
                           </div>

                            <div class="col-md-6">
                              <label for="cemail" class="control-label">Confirm Email Address*</label>
                              <input type="text" path="javaMailProperties.mailSmtpSocketFactoryPort" placeholder="Confirm primary Email Address" class="form-control input-md" />
                            </div>
                         <form:errors path="javaMailProperties.mailSmtpSocketFactoryPort" class="errorMessage" />
                       </div>
                     </div> --%>
                    <%--  <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                           <label for="password" path="javaMailProperties.mailSmtpSocketFactoryClass" class="control-label">Password*</label>
                           <form:password path="javaMailProperties.mailSmtpSocketFactoryClass" placeholder="Enter a password" class="form-control input-md"/>
                        </div>
                     <div class="col-md-6">
                           <label for="password" path="javaMailProperties.mailSmtpAuth" class="control-label">Password*</label>
                           <form:password path="javaMailProperties.mailSmtpAuth" placeholder="Enter a password" class="form-control input-md"/>
                        </div>
                     </div>
                     </div> --%>
                    <%--   <div class="form-group">
                         <div class="row">
                            <div class="col-md-6">
                            <label class="pp_label">Mobile number</label>
                     <form:input type="text" placeholder="Enter mobile number" path="javaMailProperties.mailSmtpPort" class="pp_text" maxlength="16"/>
                     <form:errors path="javaMailProperties.mailSmtpPort"  class="errorMessage"/>
                    </div>
                    <div class="col-md-6"></div>
                    </div>
                    </div>
               --%>
                <!--
                   <div class="form-group">
                       <div class="row">
                         <div class="col-md-4"></div>
                           <input type="submit" value="Mail Properties" class="btn btn-confirm">

                       </div>
                  </div> -->
            </fieldset>
        </form:form>
    </div>
    <div class="col-md-2"></div>
</div>
</body>
</html>