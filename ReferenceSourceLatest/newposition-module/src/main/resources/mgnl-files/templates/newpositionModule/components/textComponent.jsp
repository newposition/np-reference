<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<div class="${content.css}">
    <h2>${content.title}</h2>
    <p>${cmsfn:decode(content).content}</p>
</div>