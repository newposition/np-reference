<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<footer>
    <div class="wraper">
        <div class="coloum">
            <h3><cms:area name="footerSection1Heading"/></h3>
            <ul>
                <cms:area name="footerSection1"/>
            </ul>
        </div>
        <div class="coloum">
            <h3><cms:area name="footerSection2Heading"/></h3>
            <ul>
                <cms:area name="footerSection2"/>
            </ul>
        </div>
        <div class="coloum">
            <h3><cms:area name="footerSection3Heading"/></h3>
            <ul>
                <cms:area name="footerSection3"/>
            </ul>
        </div>
        <div class="coloum">
            <h3><cms:area name="footerSection4Heading"/></h3>
            <ul>
                <cms:area name="footerSection4"/>
                <li class="social-links">
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                </li>
            </ul>
        </div>
    </div>
</footer>
     