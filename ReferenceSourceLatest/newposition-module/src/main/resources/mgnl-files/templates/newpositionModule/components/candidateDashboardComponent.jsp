<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>


<div class="container">
    <div class="pd-head1">
        <div class="row">
            <div class="col-md-12">
                <div class="tlt-heading pull-right text-center">
                    Available <br> from: 24/07/2015
                </div>
                <div class="circle-outer pull-left">
                    <div class="sm_circle">
                            <span class="sm_circle_center">
                             <c:if test="${empty profileImage}">
                                 <img src="./resources/templating-kit/img/user_ico.png" alt="user" width="50px">
                             </c:if>
                             <c:if test="${not empty profileImage}">
                                 <img src="data:image/*;base64,${profileImage}" alt="user" width="100px"/>
                             </c:if>
                            </span>
                    </div>
                </div>
                <div class="heading-title text-center">
                    <h2>${user.firstName} ${user.lastName}</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-6">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Job Board</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"
                                     data-interval="false">


                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                            <div class="carousel-caption">

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                       data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                       data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                </div> <!-- Carousel -->


                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Market View</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div id="carousel-generic" class="carousel slide" data-ride="carousel"
                                     data-interval="false">


                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                            <div class="carousel-caption">

                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="caro_circ">
                                                <div class="row">
                                                    <div class="crc">255</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-generic" role="button"
                                       data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left caro_ctrl_black"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-generic" role="button"
                                       data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right caro_ctrl_black"></span>
                                    </a>
                                </div> <!-- Carousel -->


                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Status</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                         <a class="image_white_text2" href="#">
                                             <img src="./resources/templating-kit/img/icon1.png">
                                             <span class="notify">5</span></a>
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Inbox</a></div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                         <a class="image_white_text2" href="#">
                                             <img src="./resources/templating-kit/img/ico14.png">
                                             <span class="notify">6</span></a>
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Shortlisted
                                        Positions</a></div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                         <a class="image_white_text2" href="#">
                                             <img src="./resources/templating-kit/img/ico15.png">
                                             <span class="notify">2</span></a>
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Interviews</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                          <a class="image_white_text2" href="#">
                                              <img src="./resources/templating-kit/img/ico18.png">
                                              <span class="notify">3</span></a>
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Favourite
                                        Positions</a></div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                         <a class="image_white_text2" href="#">
                                             <img src="./resources/templating-kit/img/ico17.png">
                                             <span class="notify">1</span></a>
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">My Application</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2">
                                <div class="black_ico_box">
                                    <div class="black_ico">
                                        <span class="notify_parent">
                                            <a class="image_white_text2" href="referenceList"><img
                                                    src="${pageContext.request.contextPath}/docroot/img/icon1.png">

                                                <span class="notify">${not empty refereCandidateNo ? refereCandidateNo : '0' }</span></a>
                                            
                                        </span>
                                    </div>
                                    <div class="black_ico_text"><a href="referenceList">References</a></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="blue-panel">
                    <div class="blue-panel-heading">
                        <h3 class="blue-panel-title">Quick Links</h3>
                    </div>
                    <div class="blue-panel-body">
                        <div class="row">
                            <div class="col-md-15 col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon1.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Communication
                                        center</a></div>
                                </div>
                            </div>
                            <div class="col-md-15 col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/ico19.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Search
                                        jobboard</a></div>
                                </div>
                            </div>

                            <div class="col-md-15 col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon3.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">Conact support</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-15 col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon4.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">FAQ</a></div>
                                </div>
                            </div>

                            <div class="col-md-15 col-sm-3">
                                <div class="black_ico_box">
                                    <div class="black_ico"><a class="image_white_text2" href="#"><img
                                            src="./resources/templating-kit/img/icon5.png"></a></div>
                                    <div class="black_ico_text"><a class="image_white_text2" href="#">knowledge
                                        centre</a></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

</div>
 
