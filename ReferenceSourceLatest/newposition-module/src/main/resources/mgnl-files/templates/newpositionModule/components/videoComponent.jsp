<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>

<div class="video_wrapper">
    <video width="400" controls>
        <source src="${cmsfn:link(content.video)}" type="video/mp4">
    </video>
</div>
    