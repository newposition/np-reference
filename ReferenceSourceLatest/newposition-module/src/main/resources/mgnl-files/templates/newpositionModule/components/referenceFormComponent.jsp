<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>

<c:set var="crl" value="${cand.npCandidateEmployers}"/>
<c:set var="cul" value="${cand.userId}"/>
<c:set var="ccr" value="${cand.npCandidateReferences}"/>
<c:set var="npCandEmpId" value="${npCandEmpId}"/>
<div class="container">
    <div class="wraperMid">

        <div id="Quick_registration">
            <div class="row">
                <div class="rpw_left">
                    <label>First Name*</label><br>
                    <input type="text" placeholder="Pre-filled" value="${cul.firstName}">
                </div>
                <div class="rpw_left">
                    <label>Last Name*</label><br>
                    <input type="text" placeholder="Pre-filled" value="${cul.lastName}">
                </div>
            </div>
            <div class="row">
                <div class="rpw_left">
                    <label>Job Title*</label><br>
                    <input type="text" placeholder="pre-filled"
                           value="<c:forEach items="${crl}" var="sam">${sam.jobTitle}
					</c:forEach>">
                </div>
            </div>

            <div class="row">
                <div class="rpw_left">

                    <label>Start date*</label>
                    <div class="input-group">
                        <input type="text" placeholder="Date" class="datepicker-12" value="${npCandEmpId.startDate}">
                        <span class="input-group-addon fa fa-calendar cal_custom"></span>
                    </div>
                </div>
                <div class="rpw_left">
                    <label>End date*</label><br>
                    <div class="input-group">
                        <input type="text" placeholder="Date" class="datepicker-12" value="${npCandEmpId.enddate}">
                        <span class="input-group-addon fa fa-calendar cal_custom"></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="rpw">
                    <label>Covering Note*</label><br>
                    <textarea id="textarea" name="textarea" placeholder="Text area"><c:forEach items="${ccr}"
                                                                                               var="coverNote">${coverNote.coveringNote}</c:forEach></textarea>
                </div>
            </div>

        </div>

        <div class="down_table_left">
            <div class="emp_con emp_con_activer">
                <div class="skilltable">
                    <ul>
                        <li class="head">
                            <div class="emp_div lnk">&nbsp;</div>

                            <div class="emp_div Role">Skill Name</div>
                            <div class="emp_div lvl">Level</div>
                            <div class="emp_div noy">No Years</div>
                            <div class="emp_div typ">Type</div>
                            <div class="emp_div noy">Agree</div>
                            <div class="emp_div noy">DisAgree</div>
                            <div class="emp_div Role">Not Observed</div>
                        </li>
                        <li>
                            <div class="emp_div lnk"><i class="fa fa-link"></i></div>

                            <div class="emp_div Role">Java</div>
                            <div class="emp_div lvl">Expert</div>
                            <div class="emp_div noy">5</div>
                            <div class="emp_div typ">Language</div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div Role">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="emp_div lnk"><i class="fa fa-link"></i></div>

                            <div class="emp_div Role">J2EE</div>
                            <div class="emp_div lvl">Expert</div>
                            <div class="emp_div noy">5</div>
                            <div class="emp_div typ">Language</div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div Role">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="emp_div lnk"><i class="fa fa-link"></i></div>

                            <div class="emp_div Role">Spring</div>
                            <div class="emp_div lvl">Expert</div>
                            <div class="emp_div noy">5</div>
                            <div class="emp_div typ">Language</div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div Role">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="emp_div lnk"><i class="fa fa-link"></i></div>

                            <div class="emp_div Role">Hibernate</div>
                            <div class="emp_div lvl">Expert</div>
                            <div class="emp_div noy">5</div>
                            <div class="emp_div typ">Language</div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div noy">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                            <div class="emp_div Role">
                                <div class="radio">
                    <span>
                      <input type="radio" id="radio3">
                      <label for="radio3"></label>
                    </span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <br>

            </div>
        </div>

        <div id="step_1">
            <div class="row center_row">
                <input type="button" value="Send">
            </div>
        </div>


    </div>
</div>

 