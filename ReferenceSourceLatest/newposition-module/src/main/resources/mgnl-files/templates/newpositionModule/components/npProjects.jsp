<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="blossom-taglib" prefix="blossom" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!-- Bootstrap core CSS -->
<link href="./resources/templating-kit/css/bootstrap.css" rel="stylesheet">
<link href="./resources/templating-kit/css/font-awesome.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="./resources/templating-kit/css/style.css" rel="stylesheet">
<script src="./resources/templating-kit/js/clientProjects.js"></script>
<script src="./resources/templating-kit/js/jquery.form-validator.js"></script>

<div class="container">
    <div class="tlt-heading">

        <h2>Client Projects List</h2>

    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="row1">
                <form action="" class="filterby">
                    <fieldset>
                        <legend>Filter result by:</legend>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select1" class="form-control selectbox "
                                            onchange="filterFunction1()">
                                        <option value="">Type</option>
                                        <option value="projectCode">Project Code</option>
                                        <option value="projectOwner">Project Owner Name</option>
                                        <option value="projectCostcenterCode">Cost Center Code</option>
                                        <option value="projectCostcenterName">Cost Center Name</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-10">
                                    <select name="" id="select2" class="form-control selectbox "
                                            onchange="filterFunction2()">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <div class="col-md-9">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5"><span id='msg' class="sucessMessage">${message}</span></div>
                    <div class="col-xs-4 col-md-3 pull-right">
                        <a href="#" class="btn btn-rd-cor" data-toggle="modal" data-target="#myModalAdd"
                           onclick="clearAll()">Add Project</a>
                    </div>
                </div>
            </div>
            <div class="row no-pad">
                <form>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 quickFilter">
                                    <label for=""><i class="fa fa-search control-label"></i></label>
                                    <input type="text" placeholder="Search" id="qSearch" class="form-control input-md">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <div class="row no-pad">
                <div class="sortings">
                    <div class="sort pull-right">
                        <select id="select3" onchange="sortByFunction()">
                            <option>Sort by</option>
                            <option value="projectCode">Project Code</option>
                            <option value="projectOwner">Project Owner Name</option>
                            <option value="projectCostcenterCode">Cost Center Code</option>
                            <option value="projectCostcenterName">Cost Center Name</option>
                        </select>
                    </div>
                    <div class="sort pull-left">
                        <span class="result-pop"><label id="noOfRows"></label> result found</span>
                    </div>
                </div>
            </div>


            <div id="table1Div"></div>


            <div class="paging" id="header">
                <nav role="navigation">
                    <ul class="cd-pagination">
                        <li id="indexId1"><a class="disabled" href="#0" id="ancher1" onclick="moveToFirst()"><i
                                class="fa fa-angle-double-left"></i></a></li>
                        <li id="indexId2"><a class="disabled" href="#0" id="ancher2" onclick="moveOneStepLeft()"><i
                                class="fa fa-angle-left"></i></a></li>

                        <li id="indexId3"><a href="#0" id="ancher3"><i class="fa fa-angle-right"
                                                                       onclick="moveOneStepRight()"></i></a></li>
                        <li id="indexId4"><a href="#0" id="ancher4"><i class="fa fa-angle-double-right"
                                                                       onclick="moveToLast()"></i></a></li>
                    </ul>
                </nav>
            </div>

        </div>

    </div>
</div>

<div style="display: none;">
    <c:forEach items="${p_videos}" var="pro">
        <input id="url${pro.clientProjectID}" value="${pro.videoUrl}">
    </c:forEach>
</div>
<!--Bootsrap Modal details
================================================== -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Project Details</h3>
            </div>
            <div class="modal-body">
                <div id="editProjectModal" class="container-fluid">
                    <form id="form-contact" action="?" method="POST" enctype="multipart/form-data">
                        <fieldset>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="programID" name="clientProjectID" type="hidden">
                                        <label for="" class="control-label">Project Code</label>
                                        <input id="Text1" name="projectCode" type="text" placeholder="Project Code"
                                               class="form-control input-md" readonly="readonly"
                                               data-validation="required" maxlength="45"
                                               data-validation-error-msg="Enter Project Code">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="fname" class="control-label">Project Description</label>
                                        <textarea class="form-control input-md" placeholder="Project Description"
                                                  id="Text2" name="projectDescription" readonly="readonly"
                                                  data-validation="required" maxlength="45"
                                                  data-validation-error-msg="Enter Project Name"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pemail" class="control-label">Project Owner</label>
                                        <input id="Text4" name="projectOwner" type="text" placeholder="Project Owner"
                                               class="form-control input-md" readonly="readonly" maxlength="45"
                                               data-validation="required"
                                               data-validation-error-msg="Enter Project Owner">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pemail" class="control-label">Cost Center</label>
                                        <div class="search_field"><i class="fa fa-search"></i>
                                            <input type="text" placeholder="Search Cost Center" id="Text5"
                                                   class="form-control"
                                                   onkeyup="getCostCenter(this.value,'Text5','Text13')"
                                                   data-validation="required"
                                                   data-validation-error-msg="Select Cost Center">
                                            <input type="hidden" name="costCentreId" id="Text13" class="form-control"
                                                   data-validation="required"
                                                   data-validation-error-msg="Select Cost Center">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control form-ctrl" placeholder="Overview" id="Textarea"
                                                  name="projectOverview" rows="6" readonly="readonly" maxlength="200"
                                                  data-validation="required"
                                                  data-validation-error-msg="Enter Project Overview"></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="file" name="video" id="editFile" placeholder="browse file"
                                               disabled="disabled">
                                        <video id="editVideo" controls>

                                        </video>
                                        <div id="v_message1"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="button" btn-confirm" name="btn-send" class="btn btn-confirm"
                                        onclick="editFunction()" value="Edit"/>
                                    </div>
                                    <div class="col-md-6">
                                        <button id="btn-confirm" name="btn-send" class="btn btn-confirm" type="submit">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Bootsrap my Modal add
================================================== -->
<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Project Details</h3>
            </div>
            <div class="modal-body">
                <div id="addProjectModal" class="container-fluid">
                    <form id="form-contact" action="?" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="" class="control-label">Project Code</label>
                                        <input id="Text6" name="projectCode" type="text" placeholder="Project Code"
                                               onkeyup="submitFunction('Text6')" class="form-control input-md"
                                               data-validation="required" maxlength="45"
                                               data-validation-error-msg="Enter Project Code">
                                    </div>
                                </div>
                            </div>
                            <!--  <div class="form-group">
                                 <div class="row">
                                     <div class="col-md-12">
                                         <label for="" class="control-label">Programme Name</label>
                                         <input id="Text7" name="programmeName" type="text" placeholder="Programme Name" class="form-control input-md" data-validation="required" data-validation-error-msg="Enter Programme Name">
                                     </div>
                                 </div>
                             </div> -->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="fname" class="control-label">Project Description</label>
                                        <input type="text" class="form-control form-ctrl"
                                               placeholder="Project Description" id="Text8" name="projectDescription"
                                               data-validation="required" maxlength="45"
                                               data-validation-error-msg="Enter Project Description">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pemail" class="control-label">Project Owner</label>
                                        <input id="Text10" name="projectOwner" type="text" placeholder="Project Owner"
                                               class="form-control input-md" data-validation="required" maxlength="45"
                                               data-validation-error-msg="Enter Project Owner">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pemail" class="control-label">Cost Center</label>
                                        <div class="search_field"><i class="fa fa-search"></i>
                                            <input type="text" placeholder="Search Cost Center" id="Text11"
                                                   class="form-control"
                                                   onkeyup="getCostCenter(this.value,'Text11','Text12')"
                                                   data-validation="required"
                                                   data-validation-error-msg="Select Cost Center">
                                            <input type="hidden" name="costCentreId" id="Text12" class="form-control"
                                                   data-validation="required"
                                                   data-validation-error-msg="Select Cost Center">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="form-control form-ctrl" placeholder="Overview" id="Textarea1"
                                                  name="projectOverview" rows="6" data-validation="required"
                                                  maxlength="200"
                                                  data-validation-error-msg="Enter Project Overview"></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="file" id="uploadBtn" name="video" placeholder="browse file">
                                        <video id="addVideo" controls></video>
                                        <div id="v_message"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <!--  <div class="col-md-6">
                                         <button id="btn-confirm" name="btn-send" class="btn btn-confirm" >Upload</button>
                                     </div> -->
                                    <div class="col-md-6">
                                        <button id="btn-confirm" name="btn-send" class="btn btn-confirm" type="submit"
                                                style="margin-left:120px;">Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.validate();

    (function localFileVideoPlayerInit(win) {
        var URL = win.URL || win.webkitURL,
                displayMessage = (function displayMessageInit() {
                    var node = document.querySelector('#v_message');

                    return function displayMessage(message, isError) {
                        node.innerHTML = message;
                        node.className = isError ? 'error' : 'info';
                    };
                }()),
                playSelectedFile = function playSelectedFileInit(event) {
                    var file = this.files[0];

                    var type = file.type;

                    var videoNode = document.querySelector('#addVideo');

                    var canPlay = videoNode.canPlayType(type);

                    canPlay = (canPlay === '' ? 'no' : canPlay);

                    var message = 'Can play type "' + type + '": ' + canPlay;

                    var isError = canPlay === 'no';

                    displayMessage(message, isError);

                    if (isError) {
                        return;
                    }

                    var fileURL = URL.createObjectURL(file);

                    videoNode.src = fileURL;
                },
                inputNode = document.querySelector('#uploadBtn');

        if (!URL) {
            displayMessage('Your browser is not ' +
                    '<a href="http://caniuse.com/bloburls">supported</a>!', true);

            return;
        }

        inputNode.addEventListener('change', playSelectedFile, false);
    }(window));

    (function localFileVideoPlayerInit(win) {
        var URL = win.URL || win.webkitURL,
                displayMessage = (function displayMessageInit() {
                    var node = document.querySelector('#v_message1');

                    return function displayMessage(message, isError) {
                        node.innerHTML = message;
                        node.className = isError ? 'error' : 'info';
                    };
                }()),
                playSelectedFile = function playSelectedFileInit(event) {
                    var file = this.files[0];

                    var type = file.type;

                    var videoNode = document.querySelector('#editVideo');

                    var canPlay = videoNode.canPlayType(type);

                    canPlay = (canPlay === '' ? 'no' : canPlay);

                    var message = 'Can play type "' + type + '": ' + canPlay;

                    var isError = canPlay === 'no';

                    displayMessage(message, isError);

                    if (isError) {
                        return;
                    }

                    var fileURL = URL.createObjectURL(file);

                    videoNode.src = fileURL;
                },
                inputNode = document.querySelector('#editFile');

        if (!URL) {
            displayMessage('Your browser is not ' +
                    '<a href="http://caniuse.com/bloburls">supported</a>!', true);

            return;
        }

        inputNode.addEventListener('change', playSelectedFile, false);
    }(window));
</script>
