
    create table npdbs.cl_client_documents (
        ClientID integer not null,
        Comments varchar(255),
        Document tinyblob,
        DocumentTypeID integer not null,
        primary key (ClientID)
    );

    create table npdbs.cl_client_office_location (
        ClientOfficeLocID integer not null auto_increment,
        LocationID bigint,
        cl_client_ClientID integer,
        primary key (ClientOfficeLocID)
    );

    create table npdbs.cl_cost_center_status (
        CostCenterStatusID integer not null auto_increment unique,
        Status varchar(255),
        primary key (CostCenterStatusID)
    );

    create table npdbs.cl_cost_centre (
        CostCentreID bigint not null auto_increment unique,
        CostCentreCode varchar(50),
        CostCentreLocation varchar(30),
        CostCentreName varchar(50),
        CostCentreOwner varchar(50),
        CostCentreSystem varchar(30),
        Description varchar(50),
        LastActiveDate date,
        Status varchar(15),
        CostCenterStatusID integer not null,
        ClientID integer not null,
        primary key (CostCentreID)
    );

    create table npdbs.cl_costcenter_documents (
        CostCentreID bigint not null,
        Comments varchar(255),
        Document tinyblob,
        DocumentTypeID integer not null,
        primary key (CostCentreID)
    );

    create table npdbs.cl_program_documents (
        ClientProgramID bigint not null,
        Comments varchar(255),
        Document tinyblob,
        DocumentTypeID integer not null,
        primary key (ClientProgramID)
    );

    create table npdbs.cl_programs (
        ClientProgramID bigint not null auto_increment,
        ProgramCode varchar(255),
        ProgramDescription varchar(255),
        ProgramOverview varchar(255),
        ProgramOwner varchar(255),
        ProgramSponsor varchar(255),
        ClientID integer,
        CostCentreID bigint,
        primary key (ClientProgramID)
    );

    create table npdbs.cl_project_documents (
        ClientProjectID bigint not null,
        Comments varchar(255),
        Document tinyblob,
        DocumentTypeID integer not null,
        primary key (ClientProjectID)
    );

    create table npdbs.cl_projects (
        ClientProjectID bigint not null auto_increment,
        ProjectCode varchar(255),
        ProjectDescription varchar(255),
        ProjectOverview varchar(255),
        ProjectOwner varchar(255),
        ClientID integer,
        CostCentreID bigint,
        primary key (ClientProjectID)
    );

    create table npdbs.co_certificate_alias (
        CertificateAliasID integer not null auto_increment unique,
        CertificateAliasName varchar(255),
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        Status varchar(255),
        SourceID integer unique,
        CertificateID integer unique,
        primary key (CertificateAliasID),
        unique (CertificateAliasName)
    );

    create table npdbs.co_cities (
        CityID bigint not null auto_increment,
        CityDescription varchar(255),
        primary key (CityID)
    );

    create table npdbs.co_client_activation_status (
        clientActivationId integer not null auto_increment unique,
        Comments varchar(255),
        ClientId integer not null unique,
        ClientStatusId integer not null unique,
        TermReason integer not null,
        primary key (clientActivationId)
    );

    create table npdbs.co_country (
        CountryID bigint not null auto_increment,
        CountryDescription varchar(255),
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        primary key (CountryID)
    );

    create table npdbs.co_county (
        CountyID bigint not null auto_increment,
        CountyDescription varchar(255),
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        primary key (CountyID)
    );

    create table npdbs.co_course_alias (
        CourseAliasID integer not null auto_increment unique,
        CourseAliasName varchar(255),
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        Status varchar(255),
        SourceID integer unique,
        CourseID integer unique,
        primary key (CourseAliasID)
    );

    create table npdbs.co_document_types (
        DocumentTypeID integer not null unique,
        DocumentDescription varchar(50),
        DocumentName varchar(255) not null,
        DocumentSubstype varchar(10),
        DocumentType varchar(50),
        primary key (DocumentTypeID)
    );

    create table npdbs.co_employer_alias (
        EmployerAliasID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        EmployerAliasName varchar(255),
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        Status varchar(255),
        SourceID integer unique,
        EmployerID integer unique,
        primary key (EmployerAliasID),
        unique (EmployerAliasID)
    );

    create table npdbs.co_groups (
        ID integer not null auto_increment unique,
        ClientId integer,
        CreateBy varchar(255),
        CreatedDate datetime,
        GroupDescription varchar(255),
        GroupName varchar(255),
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        primary key (ID)
    );

    create table npdbs.co_industry (
        ID integer not null auto_increment unique,
        Valid varchar(20) not null,
        IndustryName varchar(20) not null,
        IndustryType varchar(20) not null,
        primary key (ID)
    );

    create table npdbs.co_industry_sector (
        IndustryID integer not null auto_increment unique,
        SectorID integer not null,
        Status bit not null,
        primary key (IndustryID)
    );

    create table npdbs.co_institute_alias (
        InstituteAliasID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        InstituteAliasName varchar(255),
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        Status varchar(255),
        SourceID integer unique,
        InstituteID integer unique,
        primary key (InstituteAliasID)
    );

    create table npdbs.co_location_state (
        StateID varchar(255) not null,
        StateDescription varchar(255),
        primary key (StateID)
    );

    create table npdbs.co_locations (
        LocationID bigint not null auto_increment,
        Address1 varchar(255),
        Address2 varchar(255),
        Address3 varchar(255),
        LocationXML varchar(255),
        CityID bigint,
        CountryID bigint,
        CountyID bigint,
        ZIP integer,
        primary key (LocationID)
    );

    create table npdbs.co_role_alias (
        RoleAliasID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        RoleAliasName varchar(255),
        Status varchar(255),
        SourceID integer unique,
        RoleID integer unique,
        primary key (RoleAliasID),
        unique (RoleAliasID)
    );

    create table npdbs.co_role_groups (
        GroupID integer not null,
        RoleID integer not null,
        CreateBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        primary key (GroupID, RoleID)
    );

    create table npdbs.co_skill_alias (
        SkillAliasID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        SkillAliasName varchar(255),
        Status varchar(255),
        SourceID integer unique,
        SkillID integer unique,
        primary key (SkillAliasID),
        unique (SkillAliasID)
    );

    create table npdbs.co_source (
        SourceID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        SourceName varchar(255),
        SourceSubType varchar(255),
        SourceType varchar(255),
        primary key (SourceID),
        unique (SourceID)
    );

    create table npdbs.co_tool_alias (
        ToolAliasID integer not null auto_increment unique,
        CreatedBy varchar(255),
        CreatedDate datetime,
        ModifiedBy varchar(255),
        ModifiedDate datetime,
        Status varchar(255),
        ToolAliasName varchar(255),
        SourceID integer unique,
        ToolID integer unique,
        primary key (ToolAliasID),
        unique (ToolAliasID)
    );

    create table npdbs.co_user_activation_status (
        UserActivationId integer not null auto_increment unique,
        Comments varchar(255),
        UserID integer not null unique,
        Userstatusid integer not null unique,
        TermReason integer not null,
        primary key (UserActivationId)
    );

    create table npdbs.co_zip (
        ZIP integer not null auto_increment,
        StateID varchar(255),
        primary key (ZIP)
    );

    create table npdbs.np_candidate (
        AvailableStatus varchar(20),
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        EligibilityCheckEU varchar(20),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        personalStatement varchar(500),
        PhotoVisibility char(1),
        RegistrationStatus varchar(20),
        ID integer not null,
        LocationID integer,
        primary key (ID)
    );

    create table npdbs.np_candidate_certificate (
        CandidateID integer not null,
        CertificateID integer not null,
        AwardObtained varchar(20),
        CertificateVerified char(1),
        CertificationYear integer,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        DigitalCertificationCode varchar(100),
        GradeLevel varchar(20),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        InstituteID integer not null,
        primary key (CandidateID, CertificateID)
    );

    create table npdbs.np_candidate_curremp_roles (
        CandidateEmployerId integer not null,
        EmploymentRoleID integer not null,
        primary key (CandidateEmployerId, EmploymentRoleID)
    );

    create table npdbs.np_candidate_document (
        ID integer not null auto_increment unique,
        coverletter varchar(255) unique,
        cv varchar(255) unique,
        photo tinyblob,
        candidateId integer,
        primary key (ID)
    );

    create table npdbs.np_candidate_education (
        CandidateID integer not null,
        InstituteCourseID integer not null,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        EndDate date,
        GradeLevel varchar(20),
        Level varchar(20),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        StartDate date,
        primary key (CandidateID, InstituteCourseID)
    );

    create table npdbs.np_candidate_eligibility (
        CandidateID integer not null,
        NpEligibilityId integer not null,
        eligibilityPreference integer,
        primary key (CandidateID, NpEligibilityId)
    );

    create table npdbs.np_candidate_emp_roles (
        CandidateID integer not null,
        EmploymentRoleID integer not null,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        futurerolepreference integer,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        YearsOfExperience integer,
        ExpertLevelID integer,
        primary key (CandidateID, EmploymentRoleID)
    );

    create table npdbs.np_candidate_employer (
        uid integer not null auto_increment,
        Achievements varchar(255),
        CorporateTitle varchar(255),
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        EmploymentType varchar(20),
        Enddate datetime not null,
        JobTitle varchar(20) not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        SalaryCompensation integer,
        StartDate datetime not null,
        CandidateID integer not null,
        EmployerID integer not null,
        primary key (uid)
    );

    create table npdbs.np_candidate_employer_skills (
        CandidateEmployerId integer not null,
        SkillId integer not null,
        primary key (CandidateEmployerId, SkillId)
    );

    create table npdbs.np_candidate_employer_tools (
        CandidateEmployerId integer not null,
        ToolID integer not null,
        primary key (CandidateEmployerId, ToolID)
    );

    create table npdbs.np_candidate_interests (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50),
        CreatedDate date,
        InterestArea varchar(400),
        InterestType varchar(100),
        ModifiedBy varchar(20),
        ModifiedDate date,
        CandidateID integer,
        primary key (ID)
    );

    create table npdbs.np_candidate_language (
        CandidateID integer not null,
        LanguageID integer not null,
        languageFluency varchar(255),
        primary key (CandidateID, LanguageID)
    );

    create table npdbs.np_candidate_prefercompany (
        CandidateID integer not null,
        employerId integer not null,
        futurePreferenceCompany integer,
        primary key (CandidateID, employerId)
    );

    create table npdbs.np_candidate_preference (
        ID integer not null auto_increment unique,
        bonus double precision,
        compensation double precision,
        dayRate double precision,
        isAcceptPhoto TINYINT,
        isJobAppearance TINYINT,
        isPermissionShortlist TINYINT,
        isSendOpportunities TINYINT,
        isSendReports TINYINT,
        isSummaryReports TINYINT,
        type varchar(255),
        candidateID integer not null,
        primary key (ID)
    );

    create table npdbs.np_candidate_reference (
        ID integer not null auto_increment unique,
        CoveringNote varchar(255),
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        dateFrom datetime,
        dateTo datetime,
        Email varchar(50),
        FirstName varchar(100) not null,
        JobTitle varchar(50),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        ReferenceVerified varchar(1),
        relationship varchar(50),
        SurName varchar(100),
        CandidateID integer,
        EmployerID integer,
        primary key (ID)
    );

    create table npdbs.np_candidate_region (
        CandidateID integer not null,
        NpRegionId integer not null,
        primary key (CandidateID, NpRegionId)
    );

    create table npdbs.np_candidate_skills (
        CandidateID integer not null,
        SkillID integer not null,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        futureskillpreference integer,
        YearsOfExperience integer,
        ExpertLevelID integer,
        primary key (CandidateID, SkillID)
    );

    create table npdbs.np_candidate_tools (
        CandidateID integer not null,
        ToolID integer not null,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        futuretoolpreference integer,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        YearsOfExperience integer not null,
        ExpertLevelID integer,
        primary key (CandidateID, ToolID)
    );

    create table npdbs.np_certifications (
        ID integer not null auto_increment unique,
        CertificateName varchar(20),
        CertificateType varchar(20),
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        Valid bit,
        primary key (ID)
    );

    create table npdbs.np_client (
        Id integer not null auto_increment unique,
        address1 varchar(30),
        address2 varchar(30),
        ClientOverview varchar(200),
        company_logo tinyblob,
        company_name varchar(50),
        country varchar(10),
        first_name varchar(30),
        last_name varchar(30),
        mobile_number varchar(15),
        postal_code varchar(10),
        principle_contact_email varchar(30),
        tele_number varchar(15),
        town varchar(30),
        clientStatus_Id integer,
        sector_Id integer,
        primary key (Id)
    );

    create table npdbs.np_client_status (
        Id integer not null auto_increment unique,
        status varchar(255) not null unique,
        primary key (Id)
    );

    create table npdbs.np_client_user (
        ID integer not null,
        client_id integer not null,
        primary key (ID)
    );

    create table npdbs.np_course (
        ID integer not null auto_increment unique,
        CourseName varchar(18),
        CourseType varchar(20),
        CreatedBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(20),
        ModifiedDate date,
        Valid bit,
        primary key (ID)
    );

    create table npdbs.np_domain_types (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        TypeName varchar(20) not null,
        primary key (ID)
    );

    create table npdbs.np_eligibility (
        ID integer not null auto_increment unique,
        country_name varchar(255) unique,
        regionId integer not null,
        primary key (ID)
    );

    create table npdbs.np_employer (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        IndustryType varchar(10) not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        Name varchar(100) not null,
        status bit,
        Valid varchar(10) not null,
        SectorID integer,
        primary key (ID)
    );

    create table npdbs.np_employment_roles (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        RoleName varchar(20),
        status bit,
        Valid varchar(255),
        DomainTypeID integer,
        primary key (ID)
    );

    create table npdbs.np_enquiry (
        ID integer not null auto_increment,
        reason varchar(255),
        primary key (ID)
    );

    create table npdbs.np_expert_level (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ExpertLevelName varchar(18),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        primary key (ID)
    );

    create table npdbs.np_institute_certificate (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(20),
        ModifiedDate date,
        Status bit,
        CertificateID integer,
        InstituteID integer,
        primary key (ID)
    );

    create table npdbs.np_institute_course (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(20),
        ModifiedDate date,
        Status bit,
        CourseID integer,
        InstituteID integer,
        primary key (ID)
    );

    create table npdbs.np_institute_location (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        InstitutionID integer,
        LocationID integer,
        primary key (ID)
    );

    create table npdbs.np_institutions (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        InstituteLogo varchar(255),
        InstituteName varchar(100) not null,
        InstituteType varchar(20),
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        Sector varchar(20),
        Status bit,
        Valid bit,
        primary key (ID)
    );

    create table npdbs.np_java_mail_properties (
        mailPropId integer not null auto_increment,
        mailSmtpAuth varchar(255),
        mailSmtpHost varchar(255),
        mailSmtpPort varchar(255),
        mailSmtpSocketFactoryClass varchar(255),
        mailSmtpSocketFactoryPort varchar(255),
        primary key (mailPropId)
    );

    create table npdbs.np_languages (
        Id integer not null auto_increment unique,
        Fluency varchar(255),
        LanguageName varchar(255),
        primary key (Id)
    );

    create table npdbs.np_locations (
        ID integer not null auto_increment unique,
        Address1 varchar(20) not null,
        Address2 varchar(20),
        Address3 varchar(20),
        CityID integer,
        CountyID integer,
        CoutryID integer,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        ZIP varchar(20),
        primary key (ID)
    );

    create table npdbs.np_mail_config (
        mailConfigId integer not null auto_increment,
        npMailPassword varchar(255),
        npMailUserName varchar(255),
        Mailprop_id integer,
        primary key (mailConfigId)
    );

    create table npdbs.np_preferred_location (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        LocationID integer not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        CandidateID integer,
        primary key (ID)
    );

    create table npdbs.np_region (
        ID integer not null auto_increment unique,
        regionName varchar(255),
        primary key (ID)
    );

    create table npdbs.np_reset (
        id integer not null auto_increment unique,
        activation varchar(255),
        email varchar(45),
        time datetime,
        primary key (id)
    );

    create table npdbs.np_roles (
        Id integer not null auto_increment unique,
        RoleName varchar(20) not null unique,
        RoleStatus varchar(20),
        primary key (Id)
    );

    create table npdbs.np_sector (
        Id integer not null auto_increment unique,
        SectorName varchar(255) not null unique,
        primary key (Id)
    );

    create table npdbs.np_skills (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        SkillName varchar(20),
        SkillType varchar(20),
        status bit,
        Valid varchar(255),
        DomainTypeID integer,
        primary key (ID)
    );

    create table npdbs.np_tools (
        ID integer not null auto_increment unique,
        CreatedBy varchar(50) not null,
        CreatedDate date not null,
        ModifiedBy varchar(20) not null,
        ModifiedDate date not null,
        status bit,
        ToolName varchar(20) not null,
        ToolType varchar(10) not null,
        Valid varchar(10) not null,
        Version varchar(20),
        DomainTypeID integer,
        primary key (ID)
    );

    create table npdbs.np_user_attempts (
        id integer not null auto_increment unique,
        attempts integer,
        lastModified datetime,
        primary key (id)
    );

    create table npdbs.np_user_roles (
        RoleID integer not null,
        UserID integer not null,
        CreateBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(50),
        ModifiedDate date,
        primary key (RoleID, UserID)
    );

    create table npdbs.np_user_status (
        ID integer not null auto_increment unique,
        CreateBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(50),
        ModifiedDate date,
        Status varchar(20) unique,
        StatusType varchar(20) not null,
        primary key (ID),
        unique (Status)
    );

    create table npdbs.np_user_types (
        ID integer not null auto_increment unique,
        CreateBy varchar(50),
        CreatedDate date,
        ModifiedBy varchar(50),
        ModifiedDate date,
        UserDesc varchar(20),
        UserType varchar(20) not null unique,
        primary key (ID),
        unique (UserType)
    );

    create table npdbs.np_users (
        Id integer not null auto_increment unique,
        company varchar(255),
        EncryptedPassword varchar(150),
        FirstName varchar(20) not null,
        Jobtitle varchar(255),
        LastName varchar(20) not null,
        officeTelephone varchar(255),
        Phone varchar(15),
        PrimaryEmailAddress varchar(50) not null,
        SecondaryEmailAddress varchar(50),
        ThirdEmailAddress varchar(50),
        UserAttemptsID integer unique,
        UserStatusID integer,
        UserTypeID integer,
        primary key (Id)
    );

    create table npdbs.np_users_reason (
        id integer not null auto_increment unique,
        reasonCode varchar(255),
        userType varchar(255),
        primary key (id)
    );

    alter table npdbs.cl_client_documents 
        add index FKEC8FD45AB046BDB3 (DocumentTypeID), 
        add constraint FKEC8FD45AB046BDB3 
        foreign key (DocumentTypeID) 
        references npdbs.co_document_types (DocumentTypeID);

    alter table npdbs.cl_client_office_location 
        add index FKBD11A5FA7C0ACE9 (LocationID), 
        add constraint FKBD11A5FA7C0ACE9 
        foreign key (LocationID) 
        references npdbs.co_locations (LocationID);

    alter table npdbs.cl_client_office_location 
        add index FKBD11A5FA24314DEA (cl_client_ClientID), 
        add constraint FKBD11A5FA24314DEA 
        foreign key (cl_client_ClientID) 
        references npdbs.np_client (Id);

    alter table npdbs.cl_cost_centre 
        add index FK502A3FB784BDDD8F (CostCenterStatusID), 
        add constraint FK502A3FB784BDDD8F 
        foreign key (CostCenterStatusID) 
        references npdbs.cl_cost_center_status (CostCenterStatusID);

    alter table npdbs.cl_cost_centre 
        add index FK502A3FB7CBCB8BCC (ClientID), 
        add constraint FK502A3FB7CBCB8BCC 
        foreign key (ClientID) 
        references npdbs.np_client (Id);

    alter table npdbs.cl_costcenter_documents 
        add index FKBF650331B046BDB3 (DocumentTypeID), 
        add constraint FKBF650331B046BDB3 
        foreign key (DocumentTypeID) 
        references npdbs.co_document_types (DocumentTypeID);

    alter table npdbs.cl_program_documents 
        add index FK42C62467B046BDB3 (DocumentTypeID), 
        add constraint FK42C62467B046BDB3 
        foreign key (DocumentTypeID) 
        references npdbs.co_document_types (DocumentTypeID);

    alter table npdbs.cl_programs 
        add index FK6D26BD858AA95151 (CostCentreID), 
        add constraint FK6D26BD858AA95151 
        foreign key (CostCentreID) 
        references npdbs.cl_cost_centre (CostCentreID);

    alter table npdbs.cl_programs 
        add index FK6D26BD85CBCB8BCC (ClientID), 
        add constraint FK6D26BD85CBCB8BCC 
        foreign key (ClientID) 
        references npdbs.np_client (Id);

    alter table npdbs.cl_project_documents 
        add index FKED5C5ABCB046BDB3 (DocumentTypeID), 
        add constraint FKED5C5ABCB046BDB3 
        foreign key (DocumentTypeID) 
        references npdbs.co_document_types (DocumentTypeID);

    alter table npdbs.cl_projects 
        add index FK6D4B23908AA95151 (CostCentreID), 
        add constraint FK6D4B23908AA95151 
        foreign key (CostCentreID) 
        references npdbs.cl_cost_centre (CostCentreID);

    alter table npdbs.cl_projects 
        add index FK6D4B2390CBCB8BCC (ClientID), 
        add constraint FK6D4B2390CBCB8BCC 
        foreign key (ClientID) 
        references npdbs.np_client (Id);

    alter table npdbs.co_certificate_alias 
        add index FK7436A0952DE23A76 (SourceID), 
        add constraint FK7436A0952DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_certificate_alias 
        add index FK7436A095F1D52B5A (CertificateID), 
        add constraint FK7436A095F1D52B5A 
        foreign key (CertificateID) 
        references npdbs.np_certifications (ID);

    alter table npdbs.co_client_activation_status 
        add index FK9B65C03ACBCB8BCC (ClientId), 
        add constraint FK9B65C03ACBCB8BCC 
        foreign key (ClientId) 
        references npdbs.np_client (Id);

    alter table npdbs.co_client_activation_status 
        add index FK9B65C03AB259B190 (ClientStatusId), 
        add constraint FK9B65C03AB259B190 
        foreign key (ClientStatusId) 
        references npdbs.np_client_status (Id);

    alter table npdbs.co_client_activation_status 
        add index FK9B65C03AE4DA5841 (TermReason), 
        add constraint FK9B65C03AE4DA5841 
        foreign key (TermReason) 
        references npdbs.np_users_reason (id);

    alter table npdbs.co_course_alias 
        add index FK972D061F2DE23A76 (SourceID), 
        add constraint FK972D061F2DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_course_alias 
        add index FK972D061F6E6EB500 (CourseID), 
        add constraint FK972D061F6E6EB500 
        foreign key (CourseID) 
        references npdbs.np_course (ID);

    alter table npdbs.co_employer_alias 
        add index FK630E23FF2DE23A76 (SourceID), 
        add constraint FK630E23FF2DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_employer_alias 
        add index FK630E23FFE395810C (EmployerID), 
        add constraint FK630E23FFE395810C 
        foreign key (EmployerID) 
        references npdbs.np_employer (ID);

    alter table npdbs.co_institute_alias 
        add index FK5BB819D32DE23A76 (SourceID), 
        add constraint FK5BB819D32DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_institute_alias 
        add index FK5BB819D3FC3F5ADA (InstituteID), 
        add constraint FK5BB819D3FC3F5ADA 
        foreign key (InstituteID) 
        references npdbs.np_institutions (ID);

    alter table npdbs.co_locations 
        add index FKE8C1C74B7745487D (ZIP), 
        add constraint FKE8C1C74B7745487D 
        foreign key (ZIP) 
        references npdbs.co_zip (ZIP);

    alter table npdbs.co_locations 
        add index FKE8C1C74B94517D14 (CountyID), 
        add constraint FKE8C1C74B94517D14 
        foreign key (CountyID) 
        references npdbs.co_county (CountyID);

    alter table npdbs.co_locations 
        add index FKE8C1C74B5916AAF4 (CityID), 
        add constraint FKE8C1C74B5916AAF4 
        foreign key (CityID) 
        references npdbs.co_cities (CityID);

    alter table npdbs.co_locations 
        add index FKE8C1C74BF5DB3382 (CountryID), 
        add constraint FKE8C1C74BF5DB3382 
        foreign key (CountryID) 
        references npdbs.co_country (CountryID);

    alter table npdbs.co_role_alias 
        add index FK3F6B56FA2DE23A76 (SourceID), 
        add constraint FK3F6B56FA2DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_role_alias 
        add index FK3F6B56FA8DE7B00E (RoleID), 
        add constraint FK3F6B56FA8DE7B00E 
        foreign key (RoleID) 
        references npdbs.np_employment_roles (ID);

    alter table npdbs.co_role_groups 
        add index FKB8942E6AE4B751C2 (RoleID), 
        add constraint FKB8942E6AE4B751C2 
        foreign key (RoleID) 
        references npdbs.np_roles (Id);

    alter table npdbs.co_role_groups 
        add index FKB8942E6AC2F63193 (GroupID), 
        add constraint FKB8942E6AC2F63193 
        foreign key (GroupID) 
        references npdbs.co_groups (ID);

    alter table npdbs.co_skill_alias 
        add index FK37C72B8F2DE23A76 (SourceID), 
        add constraint FK37C72B8F2DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_skill_alias 
        add index FK37C72B8F43FA78A2 (SkillID), 
        add constraint FK37C72B8F43FA78A2 
        foreign key (SkillID) 
        references npdbs.np_skills (ID);

    alter table npdbs.co_tool_alias 
        add index FKD1ED5E9C2DE23A76 (SourceID), 
        add constraint FKD1ED5E9C2DE23A76 
        foreign key (SourceID) 
        references npdbs.co_source (SourceID);

    alter table npdbs.co_tool_alias 
        add index FKD1ED5E9CBD660286 (ToolID), 
        add constraint FKD1ED5E9CBD660286 
        foreign key (ToolID) 
        references npdbs.np_tools (ID);

    alter table npdbs.co_user_activation_status 
        add index FKFEBD38FA2D60F270 (Userstatusid), 
        add constraint FKFEBD38FA2D60F270 
        foreign key (Userstatusid) 
        references npdbs.np_user_status (ID);

    alter table npdbs.co_user_activation_status 
        add index FKFEBD38FAE4DA5841 (TermReason), 
        add constraint FKFEBD38FAE4DA5841 
        foreign key (TermReason) 
        references npdbs.np_users_reason (id);

    alter table npdbs.co_user_activation_status 
        add index FKFEBD38FAEA0CF4AC (UserID), 
        add constraint FKFEBD38FAEA0CF4AC 
        foreign key (UserID) 
        references npdbs.np_users (Id);

    alter table npdbs.co_zip 
        add index FKAF38FCCE216AFBC3 (StateID), 
        add constraint FKAF38FCCE216AFBC3 
        foreign key (StateID) 
        references npdbs.co_location_state (StateID);

    alter table npdbs.np_candidate 
        add index FK37F5D346E6B59E1F (LocationID), 
        add constraint FK37F5D346E6B59E1F 
        foreign key (LocationID) 
        references npdbs.np_locations (ID);

    alter table npdbs.np_candidate 
        add index FK37F5D3461BE1CFA1 (ID), 
        add constraint FK37F5D3461BE1CFA1 
        foreign key (ID) 
        references npdbs.np_users (Id);

    alter table npdbs.np_candidate_certificate 
        add index FK75F0BA1EF1D52B5A (CertificateID), 
        add constraint FK75F0BA1EF1D52B5A 
        foreign key (CertificateID) 
        references npdbs.np_certifications (ID);

    alter table npdbs.np_candidate_certificate 
        add index FK75F0BA1EFC3F5ADA (InstituteID), 
        add constraint FK75F0BA1EFC3F5ADA 
        foreign key (InstituteID) 
        references npdbs.np_institutions (ID);

    alter table npdbs.np_candidate_certificate 
        add index FK75F0BA1E2365F2D2 (CandidateID), 
        add constraint FK75F0BA1E2365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_curremp_roles 
        add index FK41AE495B12A7B2FA (EmploymentRoleID), 
        add constraint FK41AE495B12A7B2FA 
        foreign key (EmploymentRoleID) 
        references npdbs.np_employment_roles (ID);

    alter table npdbs.np_candidate_curremp_roles 
        add index FK41AE495BCC111528 (CandidateEmployerId), 
        add constraint FK41AE495BCC111528 
        foreign key (CandidateEmployerId) 
        references npdbs.np_candidate_employer (uid);

    alter table npdbs.np_candidate_document 
        add index FK78BE7DF42365F2D2 (candidateId), 
        add constraint FK78BE7DF42365F2D2 
        foreign key (candidateId) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_education 
        add index FK557F55EFCF2ED48C (InstituteCourseID), 
        add constraint FK557F55EFCF2ED48C 
        foreign key (InstituteCourseID) 
        references npdbs.np_institute_course (ID);

    alter table npdbs.np_candidate_education 
        add index FK557F55EF2365F2D2 (CandidateID), 
        add constraint FK557F55EF2365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_eligibility 
        add index FKCA15DC142365F2D2 (CandidateID), 
        add constraint FKCA15DC142365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_eligibility 
        add index FKCA15DC146F204946 (NpEligibilityId), 
        add constraint FKCA15DC146F204946 
        foreign key (NpEligibilityId) 
        references npdbs.np_eligibility (ID);

    alter table npdbs.np_candidate_emp_roles 
        add index FKEE04B2CD12A7B2FA (EmploymentRoleID), 
        add constraint FKEE04B2CD12A7B2FA 
        foreign key (EmploymentRoleID) 
        references npdbs.np_employment_roles (ID);

    alter table npdbs.np_candidate_emp_roles 
        add index FKEE04B2CD2365F2D2 (CandidateID), 
        add constraint FKEE04B2CD2365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_emp_roles 
        add index FKEE04B2CDBC3A38A0 (ExpertLevelID), 
        add constraint FKEE04B2CDBC3A38A0 
        foreign key (ExpertLevelID) 
        references npdbs.np_expert_level (ID);

    alter table npdbs.np_candidate_employer 
        add index FK8C849394E395810C (EmployerID), 
        add constraint FK8C849394E395810C 
        foreign key (EmployerID) 
        references npdbs.np_employer (ID);

    alter table npdbs.np_candidate_employer 
        add index FK8C8493942365F2D2 (CandidateID), 
        add constraint FK8C8493942365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_employer_skills 
        add index FKDDB07B4D43FA78A2 (SkillId), 
        add constraint FKDDB07B4D43FA78A2 
        foreign key (SkillId) 
        references npdbs.np_skills (ID);

    alter table npdbs.np_candidate_employer_skills 
        add index FKDDB07B4DCC111528 (CandidateEmployerId), 
        add constraint FKDDB07B4DCC111528 
        foreign key (CandidateEmployerId) 
        references npdbs.np_candidate_employer (uid);

    alter table npdbs.np_candidate_employer_tools 
        add index FK8B57C170BD660286 (ToolID), 
        add constraint FK8B57C170BD660286 
        foreign key (ToolID) 
        references npdbs.np_tools (ID);

    alter table npdbs.np_candidate_employer_tools 
        add index FK8B57C170CC111528 (CandidateEmployerId), 
        add constraint FK8B57C170CC111528 
        foreign key (CandidateEmployerId) 
        references npdbs.np_candidate_employer (uid);

    alter table npdbs.np_candidate_interests 
        add index FK84C92CD02365F2D2 (CandidateID), 
        add constraint FK84C92CD02365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_language 
        add index FKE53440312225B5BF (LanguageID), 
        add constraint FKE53440312225B5BF 
        foreign key (LanguageID) 
        references npdbs.np_languages (Id);

    alter table npdbs.np_candidate_language 
        add index FKE53440312365F2D2 (CandidateID), 
        add constraint FKE53440312365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_prefercompany 
        add index FKB6EE8B34E395810C (employerId), 
        add constraint FKB6EE8B34E395810C 
        foreign key (employerId) 
        references npdbs.np_employer (ID);

    alter table npdbs.np_candidate_prefercompany 
        add index FKB6EE8B342365F2D2 (CandidateID), 
        add constraint FKB6EE8B342365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_preference 
        add index FK1CA697742365F2D2 (candidateID), 
        add constraint FK1CA697742365F2D2 
        foreign key (candidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_reference 
        add index FK2FAF2D92E395810C (EmployerID), 
        add constraint FK2FAF2D92E395810C 
        foreign key (EmployerID) 
        references npdbs.np_employer (ID);

    alter table npdbs.np_candidate_reference 
        add index FK2FAF2D922365F2D2 (CandidateID), 
        add constraint FK2FAF2D922365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_region 
        add index FK43B1360D91BD1B12 (NpRegionId), 
        add constraint FK43B1360D91BD1B12 
        foreign key (NpRegionId) 
        references npdbs.np_region (ID);

    alter table npdbs.np_candidate_region 
        add index FK43B1360D2365F2D2 (CandidateID), 
        add constraint FK43B1360D2365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_skills 
        add index FK45BB8F5B43FA78A2 (SkillID), 
        add constraint FK45BB8F5B43FA78A2 
        foreign key (SkillID) 
        references npdbs.np_skills (ID);

    alter table npdbs.np_candidate_skills 
        add index FK45BB8F5B2365F2D2 (CandidateID), 
        add constraint FK45BB8F5B2365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_skills 
        add index FK45BB8F5BBC3A38A0 (ExpertLevelID), 
        add constraint FK45BB8F5BBC3A38A0 
        foreign key (ExpertLevelID) 
        references npdbs.np_expert_level (ID);

    alter table npdbs.np_candidate_tools 
        add index FK8EB2F3A2BD660286 (ToolID), 
        add constraint FK8EB2F3A2BD660286 
        foreign key (ToolID) 
        references npdbs.np_tools (ID);

    alter table npdbs.np_candidate_tools 
        add index FK8EB2F3A22365F2D2 (CandidateID), 
        add constraint FK8EB2F3A22365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_candidate_tools 
        add index FK8EB2F3A2BC3A38A0 (ExpertLevelID), 
        add constraint FK8EB2F3A2BC3A38A0 
        foreign key (ExpertLevelID) 
        references npdbs.np_expert_level (ID);

    alter table npdbs.np_client 
        add index FK786E44E859226E75 (sector_Id), 
        add constraint FK786E44E859226E75 
        foreign key (sector_Id) 
        references npdbs.np_sector (Id);

    alter table npdbs.np_client 
        add index FK786E44E875727555 (clientStatus_Id), 
        add constraint FK786E44E875727555 
        foreign key (clientStatus_Id) 
        references npdbs.np_client_status (Id);

    alter table npdbs.np_client_user 
        add index FK373230C21BE1CFA1 (ID), 
        add constraint FK373230C21BE1CFA1 
        foreign key (ID) 
        references npdbs.np_users (Id);

    alter table npdbs.np_client_user 
        add index FK373230C2242837B5 (client_id), 
        add constraint FK373230C2242837B5 
        foreign key (client_id) 
        references npdbs.np_client (Id);

    alter table npdbs.np_eligibility 
        add index FKA103C90BC3DEF50 (regionId), 
        add constraint FKA103C90BC3DEF50 
        foreign key (regionId) 
        references npdbs.np_region (ID);

    alter table npdbs.np_employer 
        add index FK26EE6498B9C478E2 (SectorID), 
        add constraint FK26EE6498B9C478E2 
        foreign key (SectorID) 
        references npdbs.np_sector (Id);

    alter table npdbs.np_employment_roles 
        add index FK7B694587F7B9DBD8 (DomainTypeID), 
        add constraint FK7B694587F7B9DBD8 
        foreign key (DomainTypeID) 
        references npdbs.np_domain_types (ID);

    alter table npdbs.np_institute_certificate 
        add index FKF07293B0F1D52B5A (CertificateID), 
        add constraint FKF07293B0F1D52B5A 
        foreign key (CertificateID) 
        references npdbs.np_certifications (ID);

    alter table npdbs.np_institute_certificate 
        add index FKF07293B0FC3F5ADA (InstituteID), 
        add constraint FKF07293B0FC3F5ADA 
        foreign key (InstituteID) 
        references npdbs.np_institutions (ID);

    alter table npdbs.np_institute_course 
        add index FKB9C8E262FC3F5ADA (InstituteID), 
        add constraint FKB9C8E262FC3F5ADA 
        foreign key (InstituteID) 
        references npdbs.np_institutions (ID);

    alter table npdbs.np_institute_course 
        add index FKB9C8E2626E6EB500 (CourseID), 
        add constraint FKB9C8E2626E6EB500 
        foreign key (CourseID) 
        references npdbs.np_course (ID);

    alter table npdbs.np_institute_location 
        add index FKF25E2C3C586CE9BD (InstitutionID), 
        add constraint FKF25E2C3C586CE9BD 
        foreign key (InstitutionID) 
        references npdbs.np_institutions (ID);

    alter table npdbs.np_institute_location 
        add index FKF25E2C3CE6B59E1F (LocationID), 
        add constraint FKF25E2C3CE6B59E1F 
        foreign key (LocationID) 
        references npdbs.np_locations (ID);

    alter table npdbs.np_mail_config 
        add index FK2A7FC7CDCF3E8C05 (Mailprop_id), 
        add constraint FK2A7FC7CDCF3E8C05 
        foreign key (Mailprop_id) 
        references npdbs.np_java_mail_properties (mailPropId);

    alter table npdbs.np_preferred_location 
        add index FK2BC6D3502365F2D2 (CandidateID), 
        add constraint FK2BC6D3502365F2D2 
        foreign key (CandidateID) 
        references npdbs.np_candidate (ID);

    alter table npdbs.np_skills 
        add index FK93ADD15FF7B9DBD8 (DomainTypeID), 
        add constraint FK93ADD15FF7B9DBD8 
        foreign key (DomainTypeID) 
        references npdbs.np_domain_types (ID);

    alter table npdbs.np_tools 
        add index FK702E611EF7B9DBD8 (DomainTypeID), 
        add constraint FK702E611EF7B9DBD8 
        foreign key (DomainTypeID) 
        references npdbs.np_domain_types (ID);

    alter table npdbs.np_user_roles 
        add index FK8E2613E6E4B751C2 (RoleID), 
        add constraint FK8E2613E6E4B751C2 
        foreign key (RoleID) 
        references npdbs.np_roles (Id);

    alter table npdbs.np_user_roles 
        add index FK8E2613E6EA0CF4AC (UserID), 
        add constraint FK8E2613E6EA0CF4AC 
        foreign key (UserID) 
        references npdbs.np_users (Id);

    alter table npdbs.np_users 
        add index FK703E254B2D60F270 (UserStatusID), 
        add constraint FK703E254B2D60F270 
        foreign key (UserStatusID) 
        references npdbs.np_user_status (ID);

    alter table npdbs.np_users 
        add index FK703E254BD0BA9D8 (UserAttemptsID), 
        add constraint FK703E254BD0BA9D8 
        foreign key (UserAttemptsID) 
        references npdbs.np_user_attempts (id);

    alter table npdbs.np_users 
        add index FK703E254BF8726233 (UserTypeID), 
        add constraint FK703E254BF8726233 
        foreign key (UserTypeID) 
        references npdbs.np_user_types (ID);
