use npdbs;

insert into np_sector (SectorName) values ('sector1');
insert into np_sector (SectorName) values ('sector2');

insert into np_employer (CreatedBy, CreatedDate, IndustryType, ModifiedBy, ModifiedDate, Name, status, Valid, SectorID) 
values ('sys', CURDATE(), 'indType1', 'sys', CURDATE(), 'employer1', 1, 'valid', (select Id from np_sector where sectorName='sector1'));
insert into np_employer (CreatedBy, CreatedDate, IndustryType, ModifiedBy, ModifiedDate, Name, status, Valid, SectorID) 
values ('sys', CURDATE(), 'indType1', 'sys', CURDATE(), 'employer2', 1, 'valid', (select Id from np_sector where sectorName='sector2'));

insert into co_source (CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, SourceName) values( 'sys', CURDATE(), 'sys', CURDATE(), 'source1');
insert into co_source (CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, SourceName) values( 'sys', CURDATE(), 'sys', CURDATE(), 'source2');

insert into co_employer_alias (CreatedBy,CreatedDate,EmployerAliasName,ModifiedBy,ModifiedDate,SourceID,EmployerID)
values ('sys', CURDATE(), 'alias1', 'sys', CURDATE(), (select SourceID from co_source where SourceName='source1' limit 1), (select Id from np_employer where Name='employer1' limit 1));
insert into co_employer_alias (CreatedBy,CreatedDate,EmployerAliasName,ModifiedBy,ModifiedDate,SourceID,EmployerID)
values ('sys', CURDATE(), 'alias2', 'sys', CURDATE(), (select SourceID from co_source where SourceName='source2'limit 1), (select Id from np_employer where Name='employer2' limit 1));


insert into np_user_status (StatusType) values ('active');

insert into np_user_attempts (attempts) values(1);
insert into np_users (EncryptedPassword, FirstName, LastName, PrimaryEmailAddress, UserStatusID,UserAttemptsID)
values
(
'$2a$10$GDqygSxqy7Hj62wlsFgQbOxenydyJ9Kxj8noIz.3Bc/03B2p9caJu',
'Super',
'User',
'superuser',
1,
1
);

insert into np_roles (RoleName, RoleStatus) values ('ROLE_NP_USERSUPPORT', 'ACTIVE');
insert into np_roles (RoleName, RoleStatus) values ('ROLE_SKILLS_ANALYST', 'ACTIVE');
insert into np_user_roles (RoleId, UserId) values (1, 1);
insert into np_user_roles (RoleId, UserId) values (2, 1);
