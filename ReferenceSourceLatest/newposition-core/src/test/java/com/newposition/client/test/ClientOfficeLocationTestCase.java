package com.newposition.client.test;

import java.util.List;

import javax.validation.spi.ConfigurationState;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.service.NpClientOfficeLocationService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class ClientOfficeLocationTestCase extends TestCase
{

    @Autowired
    private NpClientService clientService;

    @Autowired
    private NpClientOfficeLocationService npClientOfficeLocationService;

    public void saveClientAdministrator()
    {
        AddClientForm addClientForm = new AddClientForm();
        addClientForm.setFirstName("Test User");
        addClientForm.setLastName("Test LastName");
        addClientForm.setCompanyName("TestCompany");
        addClientForm.setMobileNumber("90873635546");
        addClientForm.setAddress1("test address");
        addClientForm.setAddress2("address2");
        addClientForm.setPrincipleContactEmail("test@principal.career");
        addClientForm.setPostCode("4343dd");
        addClientForm.setCountry("Us");
        addClientForm.setTown("CapTown");
        addClientForm.setSectorId(1);

        addClientForm.setAdminFirstName("testadmin");
        addClientForm.setAdminLastName("last name");
        addClientForm.setAdminEmail("testadmin@test.carrers");
        addClientForm.setAdminPassword("Jarvis123");

        clientService.addClient(addClientForm);
    }

    @Test
    public void comeList()
    {
        saveClientAdministrator();
        NpClientImpl npClient = (NpClientImpl) clientService.findClientByCompanyName("TestCompany");
        List<NpClientOfficeLocation> list = npClientOfficeLocationService.getAllClientOfficeLocations(npClient);
    }

    public Integer test_Save_OfficeLocation()
    {
        saveClientAdministrator();
        NpClientImpl npClient = (NpClientImpl) clientService.findClientByCompanyName("TestCompany");
        CoCountry coCountry = new CoCountry();
        coCountry.setCountryDescription("Tanjania");
        npClientOfficeLocationService.saveCoCountry(coCountry);
        CoCities coCities = new CoCities();

        coCities.setCityDescription("Baker");
        npClientOfficeLocationService.saveCoCities(coCities);

        CoCounty coCounty = new CoCounty();
        coCounty.setCountyDescription("MyDistrict");
        npClientOfficeLocationService.saveCoCounty(coCounty);

        CoLocationState coLocaState = new CoLocationState();
        coLocaState.setStateID("xy");
        coLocaState.setStateDescription("MyState");

        CoZip coZip = new CoZip();
        coZip.setCoLocationState(coLocaState);

        CoLocations coLocations = new CoLocations();
        coLocations.setAddress1("Address");
        coLocations.setAddress2("AAddress 2");
        coLocations.setAddress3("Address 3");
        coLocations.setCoCities(coCities);
        coLocations.setCoCounty(coCounty);
        coLocations.setCoCountry(coCountry);
        coLocations.setCoZip(coZip);

        NpClientOfficeLocation clientOfficeLocation = new NpClientOfficeLocation();
        clientOfficeLocation.setNpClient(npClient);
        clientOfficeLocation.setCoLocations(coLocations);

        npClientOfficeLocationService.saveNpClientOfficeLocation(clientOfficeLocation, coLocations, coCities, coCounty, coLocaState, coZip, coCountry);
        return clientOfficeLocation.getClientOfficeLocID();
    }

    @Test
    public void test_Success_Save_OfficeLocation()
    {
        saveClientAdministrator();
        NpClientImpl npClient = (NpClientImpl) clientService.findClientByCompanyName("TestCompany");
        CoCountry coCountry = new CoCountry();
        coCountry.setCountryDescription("Tanjania");
        npClientOfficeLocationService.saveCoCountry(coCountry);
        CoCities coCities = new CoCities();

        coCities.setCityDescription("Baker");
        npClientOfficeLocationService.saveCoCities(coCities);

        CoCounty coCounty = new CoCounty();
        coCounty.setCountyDescription("MyDistrict");
        npClientOfficeLocationService.saveCoCounty(coCounty);

        CoLocationState coLocaState = new CoLocationState();
        coLocaState.setStateID("xy");
        coLocaState.setStateDescription("MyState");

        CoZip coZip = new CoZip();
        coZip.setCoLocationState(coLocaState);

        CoLocations coLocations = new CoLocations();
        coLocations.setAddress1("Address");
        coLocations.setAddress2("AAddress 2");
        coLocations.setAddress3("Address 3");
        coLocations.setCoCities(coCities);
        coLocations.setCoCounty(coCounty);
        coLocations.setCoCountry(coCountry);
        coLocations.setCoZip(coZip);

        NpClientOfficeLocation clientOfficeLocation = new NpClientOfficeLocation();
        clientOfficeLocation.setNpClient(npClient);
        clientOfficeLocation.setCoLocations(coLocations);

        npClientOfficeLocationService.saveNpClientOfficeLocation(clientOfficeLocation, coLocations, coCities, coCounty, coLocaState, coZip, coCountry);
    }

    @Test
    public void updateofficeLocation()
    {
        Integer locatioId = test_Save_OfficeLocation();
        npClientOfficeLocationService.getnpClientOfficeLocationServiceById(locatioId);
    }

    @Test
    public void test_Update_OfficeLocation()
    {

        NpClientImpl npClient = (NpClientImpl) clientService.findClientByCompanyName("TestCompany");
        CoCountry coCountry = new CoCountry();
        coCountry.setCountryDescription("Tanjania");
        npClientOfficeLocationService.saveCoCountry(coCountry);
        CoCities coCities = new CoCities();

        coCities.setCityDescription("Baker");
        npClientOfficeLocationService.saveCoCities(coCities);

        CoCounty coCounty = new CoCounty();
        coCounty.setCountyDescription("MyDistrict");
        npClientOfficeLocationService.saveCoCounty(coCounty);

        CoLocationState coLocaState = new CoLocationState();
        coLocaState.setStateID("xy");
        coLocaState.setStateDescription("MyState");

        CoZip coZip = new CoZip();
        coZip.setCoLocationState(coLocaState);

        CoLocations coLocations = new CoLocations();
        coLocations.setAddress1("Address");
        coLocations.setAddress2("AAddress 2");
        coLocations.setAddress3("Address 3");
        coLocations.setCoCities(coCities);
        coLocations.setCoCounty(coCounty);
        coLocations.setCoCountry(coCountry);
        coLocations.setCoZip(coZip);

        NpClientOfficeLocation clientOfficeLocation = new NpClientOfficeLocation();
        clientOfficeLocation.setNpClient(npClient);
        clientOfficeLocation.setCoLocations(coLocations);
        npClientOfficeLocationService.saveNpClientOfficeLocation(clientOfficeLocation, coLocations, coCities, coCounty, coLocaState, coZip, coCountry);

        CoCountry coCountry1 = clientOfficeLocation.getCoLocations().getCoCountry();
        coCountry1.setCountryDescription("Zembawe");

        npClientOfficeLocationService.saveNpClientOfficeLocation(clientOfficeLocation, coLocations, coCities, coCounty, coLocaState, coZip, coCountry1);
    }
}
