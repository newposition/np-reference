package com.newposition.client.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.dao.NpClientSupportAddEditUserDao;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.service.NpClientService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.exception.UserNotFoundException;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class ClientAdminAddUserTestCase extends TestCase
{

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private NpClientService npClientService;

    @Autowired
    private NpClientSupportAddEditUserService npClientsupportAddEditService;

    @Autowired
    private NpClientSupportAddEditUserDao npClientSupportAddEditUserDao;

    public void saveClientAdministrator()
    {
        AddClientForm addClientForm = new AddClientForm();
        addClientForm.setFirstName("Test User");
        addClientForm.setLastName("Test LastName");
        addClientForm.setCompanyName("TestCompany");
        addClientForm.setMobileNumber("90873635546");
        addClientForm.setAddress1("test address");
        addClientForm.setAddress2("address2");
        addClientForm.setPrincipleContactEmail("testprincipale@principal.career");
        addClientForm.setPostCode("4343dd");
        addClientForm.setCountry("Us");
        addClientForm.setTown("CapTown");
        addClientForm.setSectorId(1);

        addClientForm.setAdminFirstName("testadmin");
        addClientForm.setAdminLastName("last name");
        addClientForm.setAdminEmail("testadmin@test.carrers");
        addClientForm.setAdminPassword("Jarvis123");

        npClientService.addClient(addClientForm);
    }

    //test case for saving  new user in the db
    @Test
    public void test_Success_GetCompanyOfCurrentUser()
    {
        saveClientAdministrator();
        npClientsupportAddEditService.getCompanyOfCurrentUser("testadmin@test.carrers");
    }

    @Test(expected = UserNotFoundException.class)
    public void test_Failure_GetCompanyOfCurrentUser_Giving_Wrong_UserName()
    {
        saveClientAdministrator();
        npClientsupportAddEditService.getCompanyOfCurrentUser("testadminasda@test.carrers");
    }

    @Test(expected = UserNotFoundException.class)
    public void test_Failure_GetCompanyOfCurrentUser_Giving_Empty_UserName()
    {
        saveClientAdministrator();
        npClientsupportAddEditService.getCompanyOfCurrentUser("");
    }

    @Test
    public void test_Successe_Save_ClientUser_Without_Id()
    {
        saveClientAdministrator();
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("998777894564");
        clientUserForm.setOfficeTelePhone("1123788584354");
        clientUserForm.setCurrentRole("Role_Client_Employee");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test
    public void test_Successe_Save_ClientUser_With_SingleRole()
    {
        saveClientAdministrator();
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("998777894564");
        clientUserForm.setCurrentRole("Role_Client_Employee");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test
    public void test_Successe_Save_ClientUser_With_Multiple_Role()
    {
        saveClientAdministrator();
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("998777894564");
        clientUserForm.setCurrentRole("ROLE_CLIENT_EMPLOYEE,ROLE_CLIENT_ADMINISTRATOR,ROLE_CLIENT_SUPPORT");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test(expected = Exception.class)
    public void test_Failure_Save_ClientUser_With_LongInputMobileNumber()
    {
        saveClientAdministrator();
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("99877789456456456546456456546456456");
        clientUserForm.setCurrentRole("Role_Client_Employee");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test
    public void test_Successe_Save_ClientUser_With_Id()
    {
        test_Successe_Save_ClientUser_With_SingleRole();
        NpClientUser clientUser = npClientSupportAddEditUserDao.getNpClientUserByName("testclientuser@mail.com");
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setUserId(clientUser.getId().toString());
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("998777894564");
        clientUserForm.setOfficeTelePhone("1123788584354");
        clientUserForm.setCurrentRole("Role_Client_Employee");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test
    public void test_Success_Update_ClientUser_With_MultipleRole()
    {
        test_Successe_Save_ClientUser_With_SingleRole();
        NpClientUser clientUser = npClientSupportAddEditUserDao.getNpClientUserByName("testclientuser@mail.com");
        NpClientSupportAddEditUserForm clientUserForm = new NpClientSupportAddEditUserForm();
        clientUserForm.setUserId(clientUser.getId().toString());
        clientUserForm.setFirstName("John");
        clientUserForm.setLastName("Doe");
        clientUserForm.setMobile("998777894564");
        clientUserForm.setOfficeTelePhone("1123788584354");
        clientUserForm.setCurrentRole("ROLE_CLIENT_ADMINISTRATOR,ROLE_CLIENT_SUPPORT");
        clientUserForm.setPrimaryEmailAddress("testclientuser@mail.com");

        npClientsupportAddEditService.saveClientUser("testadmin@test.carrers", clientUserForm);
    }

    @Test
    public void test_Find_NpUser_By_Exist_Admin_UserName_Name()
    {
        saveClientAdministrator();
        npClientSupportAddEditUserDao.getNpClientUserByName("testadmin@test.carrers");
    }

    @Test(expected = UserNotFoundException.class)
    public void test_Find_NpUser_By_NonExist_Admin_UserName_Name()
    {
        saveClientAdministrator();
        npClientSupportAddEditUserDao.getNpClientUserByName("testadmin1233@test.carrers");
    }

    @Test(expected = UserNotFoundException.class)
    public void test_Find_NpUser_By_Null_Input()
    {
        saveClientAdministrator();
        npClientSupportAddEditUserDao.getNpClientUserByName(null);
    }
}
