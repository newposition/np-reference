package com.newposition.client.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.service.NpClientCompanyInformationService;
import com.newposition.client.service.NpClientService;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class NpClientCompanyInformationServiceTestCase extends TestCase
{

    @Autowired
    private NpClientCompanyInformationService npClientCompanyInformationService;

    @Autowired
    private NpClientService npClientService;

    public void saveClientAdministrator()
    {
        AddClientForm addClientForm = new AddClientForm();
        addClientForm.setFirstName("Test User");
        addClientForm.setLastName("Test LastName");
        addClientForm.setCompanyName("TestCompany");
        addClientForm.setMobileNumber("90873635546");
        addClientForm.setAddress1("test address");
        addClientForm.setAddress2("address2");
        addClientForm.setPrincipleContactEmail("test@principal.career");
        addClientForm.setPostCode("4343dd");
        addClientForm.setCountry("Us");
        addClientForm.setTown("CapTown");
        addClientForm.setSectorId(1);

        addClientForm.setAdminFirstName("testadmin");
        addClientForm.setAdminLastName("last name");
        addClientForm.setAdminEmail("test1@YOPmail.com");
        addClientForm.setAdminPassword("Jarvis123");

        npClientService.addClient(addClientForm);
    }

    // testcase for getting np client company details by email
    @Test
    public void testFindByUserName()
    {
        saveClientAdministrator();

        npClientCompanyInformationService.findByUserName("test1@YOPmail.com");
    }

    @Test(expected = Exception.class)
    public void testFindByUserNameWithNonExistId()
    {
        saveClientAdministrator();
        npClientCompanyInformationService.findByUserName("abcs@mail.com");
    }
}
