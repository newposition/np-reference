package com.newposition.client.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.forms.AddClientCostCentreForm;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.service.NpClientCostCentreService;
import com.newposition.client.service.NpClientService;
import com.newposition.exception.CostCentreNotFoundException;
import com.newposition.exception.StatusNotFoundException;

/**
 * Unit test for Np CostCentre.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class NpClientCostCentreTestCase extends TestCase
{

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private NpClientService npClientService;

    @Autowired
    private NpClientCostCentreService npClientCostCentreService;

    public void saveClientAdministrator()
    {
        AddClientForm addClientForm = new AddClientForm();
        addClientForm.setFirstName("Test User");
        addClientForm.setLastName("Test LastName");
        addClientForm.setCompanyName("TestCompany");
        addClientForm.setMobileNumber("90873635546");
        addClientForm.setAddress1("test address");
        addClientForm.setAddress2("address2");
        addClientForm.setPrincipleContactEmail("test@principal.career");
        addClientForm.setPostCode("4343dd");
        addClientForm.setCountry("Us");
        addClientForm.setTown("CapTown");
        addClientForm.setSectorId(1);

        addClientForm.setAdminFirstName("testadmin");
        addClientForm.setAdminLastName("last name");
        addClientForm.setAdminEmail("testadmin@test.carrers");
        addClientForm.setAdminPassword("Jarvis123");

        npClientService.addClient(addClientForm);
    }

    public void addCostCentre()
    {
        saveClientAdministrator();

        AddClientCostCentreForm ccForm = new AddClientCostCentreForm();

        ccForm.setCostCentreCode("CC01");
        ccForm.setCostCentreName("testCCName");
        ccForm.setCostCentreOwner("testCCOwner");
        ccForm.setCostCentreSystem("testCCSystem");
        ccForm.setDescription("description");
        ccForm.setLocation("testLocation");

        npClientCostCentreService.saveCostCentre(ccForm, "testadmin@test.carrers");
    }

    @Test
    public void test_Success_find_CostCentreBy_Code()
    {
        addCostCentre();
        npClientCostCentreService.findCostCentreByCode("CC01");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_find_CostCentreBy_Code()
    {

        npClientCostCentreService.findCostCentreByCode("sdfsdf");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_find_CostCentreBy_Code_For_EmptyInput()
    {

        npClientCostCentreService.findCostCentreByCode("");
    }

    @Test
    public void test_Success_find_CostCentreBy_Id()
    {
        addCostCentre();
        NpClientCostCentre costC = npClientCostCentreService.findCostCentreByCode("CC01");
        npClientCostCentreService.findCostCentreById(costC.getCostCentreId().toString());
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_find_CostCentreBy_Id_Not_Exist()
    {

        npClientCostCentreService.findCostCentreById("1221");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_find_CostCentreBy_Id_For_EmptyInput()
    {

        npClientCostCentreService.findCostCentreById("");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_find_CostCentreBy_Id_For_Invalid_Input()
    {

        npClientCostCentreService.findCostCentreById("sdfgsfg");
    }

    @Test
    public void test_Successe_Save_CostCentre_Without_Id()
    {
        saveClientAdministrator();

        AddClientCostCentreForm ccForm = new AddClientCostCentreForm();

        ccForm.setCostCentreCode("CC01");
        ccForm.setCostCentreName("testCCName");
        ccForm.setCostCentreOwner("testCCOwner");
        ccForm.setCostCentreSystem("testCCSystem");
        ccForm.setDescription("description");
        ccForm.setLocation("testLocation");

        npClientCostCentreService.saveCostCentre(ccForm, "testadmin@test.carrers");
    }

    @Test
    public void test_Successe_Save_CostCentre_With_Id()
    {
        addCostCentre();
        NpClientCostCentre cc = npClientCostCentreService.findCostCentreByCode("CC01");

        AddClientCostCentreForm ccForm = new AddClientCostCentreForm();
        ccForm.setCostCentreId(cc.getCostCentreId());
        ccForm.setCostCentreCode("CC01");
        ccForm.setCostCentreName("testCCName1");
        ccForm.setCostCentreOwner("testCCOwner1");
        ccForm.setCostCentreSystem("testCCSystem1");
        ccForm.setDescription("descriptio1n");
        ccForm.setLocation("testLocation1");

        npClientCostCentreService.updateCostCentre(ccForm);
    }

    @Test
    public void test_Success_Delete_CostCentre()
    {
        addCostCentre();
        NpClientCostCentre cc = npClientCostCentreService.findCostCentreByCode("CC01");
        npClientCostCentreService.deleteCostCentre(cc.getCostCentreId().toString());
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_Delete_CostCentre_With_NoexistCostCentre()
    {

        npClientCostCentreService.deleteCostCentre("234234");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_Delete_CostCentre_With_InvalidInputId()
    {

        npClientCostCentreService.deleteCostCentre("dfgdfg");
    }

    @Test(expected = CostCentreNotFoundException.class)
    public void test_Failure_Delete_CostCentre_With_Empty_Id()
    {

        npClientCostCentreService.deleteCostCentre("");
    }

    @Test
    public void test_Find_Active_Status_Of_CostCentre()
    {
        npClientCostCentreService.findCostCentreStatusByStatusCode("Active");
    }

    @Test
    public void test_Find_Inactive_Status_Of_CostCentre()
    {
        npClientCostCentreService.findCostCentreStatusByStatusCode("Inactive");
    }

    @Test
    public void test_Find_Deleted_Status_Of_CostCentre()
    {
        npClientCostCentreService.findCostCentreStatusByStatusCode("Deleted");
    }

    @Test(expected = StatusNotFoundException.class)
    public void test_Failure_Find_Status_Of__Non_ExistCostCentreStaus()
    {

        npClientCostCentreService.findCostCentreStatusByStatusCode("Enabled");
    }

    @Test(expected = StatusNotFoundException.class)
    public void test_Failure_Find_Status_Of_Empty_Input()
    {

        npClientCostCentreService.findCostCentreStatusByStatusCode("");
    }
}
