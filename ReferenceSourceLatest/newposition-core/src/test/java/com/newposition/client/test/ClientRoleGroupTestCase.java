package com.newposition.client.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.CoGroups;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class ClientRoleGroupTestCase extends TestCase
{

    @Autowired
    private NpClientRoleManagementService npClientRoleManagementService;

    @Autowired
    private NpClientService clientService;

    public void saveClientAdministrator()
    {
        AddClientForm addClientForm = new AddClientForm();
        addClientForm.setFirstName("Test User");
        addClientForm.setLastName("Test LastName");
        addClientForm.setCompanyName("TestCompany");
        addClientForm.setMobileNumber("90873635546");
        addClientForm.setAddress1("test address");
        addClientForm.setAddress2("address2");
        addClientForm.setPrincipleContactEmail("test@principal.career");
        addClientForm.setPostCode("4343dd");
        addClientForm.setCountry("Us");
        addClientForm.setTown("CapTown");
        addClientForm.setSectorId(1);

        addClientForm.setAdminFirstName("testadmin");
        addClientForm.setAdminLastName("last name");
        addClientForm.setAdminEmail("testadmin@test.carrers");
        addClientForm.setAdminPassword("Jarvis123");

        clientService.addClient(addClientForm);
    }

    public int saveClientRoleGroupTest()
    {
        saveClientAdministrator();
        NpClient npClient = clientService.findClientByPrincipal("test@principal.career");
        NpClientAddRoleGroupForm npClientAddRoleGroupForm = new NpClientAddRoleGroupForm();
        npClientAddRoleGroupForm.setGroupName("TestGroupName");
        npClientAddRoleGroupForm.setGroupDescription("TestGroupDescription");
        npClientAddRoleGroupForm.setCurrentRole("ROLE_CLIENT_ADMINISTRATOR,ROLE_CLIENT_INTERVIEWER");
        return npClientRoleManagementService.saveGroups(npClientAddRoleGroupForm, npClient.getId(), "Test User");
    }

    @Test
    public void getCoGroupByIdTest()
    {

        npClientRoleManagementService.getCoGroupById(saveClientRoleGroupTest());
    }

    @Test
    public void deleteGroupByIdTest()
    {
        npClientRoleManagementService.deleteGroupById(saveClientRoleGroupTest());
    }

	/*@Test
    public void getListOfRolesInClientGroupsTest(){
		saveClientRoleGroupTest();
		NpClient npClient=clientService.findClientByPrincipal("test@principal.career");
		CoGroups coGroups=npClientRoleManagementService.getListOfRolesInClientGroups(76, "uvu");
		assertEquals(1, coGroups.getCoRoleGroups().size());
	}*/
}
