package com.newposition.admin.test;

import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.admin.service.NpAdminToolManagementService;
import com.newposition.common.domain.CoSource;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class ToolManagementServiceTestCase extends TestCase
{

    @Autowired
    private NpAdminToolManagementService toolManagementService;

    @Autowired
    private UserDetailsService userDetailsService;

    //testcase for updating the skills in the db
    @Test
    public void testSuccessSaveNpSkillsWithId()
    {
        AddNpToolsForm addNpToolsForm = new AddNpToolsForm();
        addNpToolsForm.setToolId("1");
        addNpToolsForm.setToolName("TestSkill");
        addNpToolsForm.setValid("Invalid");
        addNpToolsForm.setDomainId("1");
        addNpToolsForm.setToolType("sofware");

        toolManagementService.saveNpTools(addNpToolsForm);
    }

    //testcase for saving the skills in the db
    @Test
    public void testSuccessSaveNpSkillsWithOutId()
    {
        AddNpToolsForm addNpToolsForm = new AddNpToolsForm();
        addNpToolsForm.setToolName("TestSkill1");
        addNpToolsForm.setValid("Invalid");
        addNpToolsForm.setDomainId("1");
        addNpToolsForm.setToolType("sofware");

        toolManagementService.saveNpTools(addNpToolsForm);
    }

    //testcase for saving the skills With Domain Not In Db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testSuccessSaveNpSkillsWithDomainNotInDb()
    {
        AddNpToolsForm addNpToolsForm = new AddNpToolsForm();
        addNpToolsForm.setToolName("TestTool1");
        addNpToolsForm.setValid("Invalid");
        addNpToolsForm.setDomainId("10");
        addNpToolsForm.setToolType("sofware");

        toolManagementService.saveNpTools(addNpToolsForm);
    }

    //testcase for deleting the skills with improper format id
    @Test(expected = NumberFormatException.class)
    public void testDeletNpSkillsWithOutProperFormat()
    {
        toolManagementService.deletNpTools("154rf");
    }

    //testcase for deleting skills that is in db
    @Test
    public void testDeleteNpSkills()
    {
        toolManagementService.deletNpTools("1");
    }

    //testcase for deleting skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteNpSkillsNotInDb()
    {
        toolManagementService.deletNpTools("0");
    }

    //testcase for deleting coalias skills that is in db
    @Test
    public void testDeleteCoAliasSkills()
    {
        toolManagementService.deleteCoAliasTools("13");
    }

    //testcase for deleting coalias skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteCoAliasSkillsNotInDb()
    {
        toolManagementService.deleteCoAliasTools("12");
    }

    //testcase for deleting coalias skills with alphanumeric id
    @Test(expected = NumberFormatException.class)
    public void testDeleteCoAliasSkillsWithOutProperFormat()
    {
        toolManagementService.deleteCoAliasTools("12weee");
    }

    //testcase for getting list of cosources  in db
    @Test
    public void testGetCoSources()
    {
        Set<CoSource> coSources = toolManagementService.getcoSources();
        assertEquals(4, coSources.size());
    }
}
