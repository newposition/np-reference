package com.newposition.admin.test;

import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.admin.service.NpAdminIndustryManagementService;
import com.newposition.common.domain.CoSource;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class IndustryManagementServiceTestCase extends TestCase
{

    @Autowired
    private NpAdminIndustryManagementService npAdminIndustryManagementService;

    //testcase for updating the Industries in the db
    @Test
    public void testSuccessSaveNpIndustriesWithId()
    {
        AddNpIndustriesForm addNpIndustriesForm = new AddNpIndustriesForm();
        addNpIndustriesForm.setIndustryId("1");
        addNpIndustriesForm.setIndustryName("TestIndustry");
        addNpIndustriesForm.setValid("Valid");
        addNpIndustriesForm.setIndustryType("sofware");
        addNpIndustriesForm.setSectorId("1");

        npAdminIndustryManagementService.saveNpIndustries(addNpIndustriesForm);
    }

    //testcase for saving the Industries in the db
    @Test
    public void testSuccessSaveNpIndustriesWithOutId()
    {
        AddNpIndustriesForm addNpIndustriesForm = new AddNpIndustriesForm();
        addNpIndustriesForm.setIndustryName("TestIndustry");
        addNpIndustriesForm.setValid("Valid");
        addNpIndustriesForm.setIndustryType("sofware");
        addNpIndustriesForm.setSectorId("1");

        npAdminIndustryManagementService.saveNpIndustries(addNpIndustriesForm);
    }

    //testcase for saving the Industries in the db without sector
    @Test(expected = NumberFormatException.class)
    public void testSuccessSaveNpIndustriesWithOutSector()
    {
        AddNpIndustriesForm addNpIndustriesForm = new AddNpIndustriesForm();
        addNpIndustriesForm.setIndustryName("TestIndustry");
        addNpIndustriesForm.setValid("Valid");
        addNpIndustriesForm.setIndustryType("sofware");

        npAdminIndustryManagementService.saveNpIndustries(addNpIndustriesForm);
    }

    //testcase for deleting the Industries with improper format id
    @Test(expected = NumberFormatException.class)
    public void testDeletNpIndustriesWithOutProperFormat()
    {
        npAdminIndustryManagementService.deletNpIndustries("154rf");
    }

    //testcase for deleting Industries that is in db
    @Test
    public void testDeleteNpIndustries()
    {
        npAdminIndustryManagementService.deletNpIndustries("1");
    }

    //testcase for deleting Industries that is not in db
    @Test(expected = Exception.class)
    public void testDeleteNpIndustriesNotInDb()
    {
        npAdminIndustryManagementService.deletNpIndustries("10222");
    }

    //testcase for deleting coalias Industries that is in db
    @Test
    public void testDeleteCoAliasCertificate()
    {
        npAdminIndustryManagementService.deleteCoAliasIndustries("13");
    }

    //testcase for deleting coalias Industries that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteCoAliasIndustriesNotInDb()
    {
        npAdminIndustryManagementService.deleteCoAliasIndustries("0");
    }

    //testcase for deleting coalias Industries with alphanumeric id
    @Test(expected = NumberFormatException.class)
    public void testDeleteCoAliasIndustriesWithOutProperFormat()
    {
        npAdminIndustryManagementService.deleteCoAliasIndustries("12weee");
    }

    //testcase for getting list of cosources  in db
    @Test
    public void testGetCoSources()
    {
        List<CoSource> coSources = npAdminIndustryManagementService.getcoSources();
        assertNotNull(coSources);
    }
}
