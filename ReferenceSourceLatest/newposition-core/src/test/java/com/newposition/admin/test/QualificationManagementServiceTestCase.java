package com.newposition.admin.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.forms.AddNpQualificationForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.QualificationManagementService;
import com.newposition.common.domain.CoSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class QualificationManagementServiceTestCase extends TestCase
{

    @Autowired
    private QualificationManagementService qualificationManagementService;

    // testcase for saving the institute course
    @Test
    public void testSaveNpInstituteCourse()
    {

        AddNpQualificationForm addNpQualificationForm = new AddNpQualificationForm();
        CertificationAliases certificationAliases = new CertificationAliases();
        certificationAliases.setAliasName("TSt");
        certificationAliases.setAliasSource("1");

        List<CertificationAliases> certificationAliasesList = new ArrayList<>();
        certificationAliasesList.add(certificationAliases);
        addNpQualificationForm.setInstitutionId(220);
        addNpQualificationForm.setCourseType("DAS");
        addNpQualificationForm.setCourseName("Test");
        addNpQualificationForm.setValid(true);
        addNpQualificationForm
                .setCertificationAliases(certificationAliasesList);
        qualificationManagementService
                .saveNpInstituteCourse(addNpQualificationForm);
    }

    // testcase for saving the institute course for the institute not in the db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testSaveNpInstituteCourseNotInDb()
    {

        AddNpQualificationForm addNpQualificationForm = new AddNpQualificationForm();
        CertificationAliases certificationAliases = new CertificationAliases();
        certificationAliases.setAliasName("TSt");
        certificationAliases.setAliasSource("1");

        List<CertificationAliases> certificationAliasesList = new ArrayList<>();
        certificationAliasesList.add(certificationAliases);
        addNpQualificationForm.setInstitutionId(2202);
        addNpQualificationForm.setCourseType("DAS");
        addNpQualificationForm.setCourseName("Test");
        addNpQualificationForm.setValid(true);
        addNpQualificationForm
                .setCertificationAliases(certificationAliasesList);
        qualificationManagementService
                .saveNpInstituteCourse(addNpQualificationForm);
    }

    // testcase for getting list of cosources in db
    @Test
    public void testGetCoSources()
    {
        Set<CoSource> coSources = qualificationManagementService.getCoSources();
        assertNotNull(coSources);
    }

    // testcase for removing institutecoursebyid
    @Test
    public void testRemoveNpInstituteCourseById()
    {
        qualificationManagementService.removeNpInstituteCourseById(201);
    }

    // testcase for getting np institute course by id
    @Test
    public void testGetNpInstituteCourseById()
    {
        qualificationManagementService.getNpInstituteCourseById(201);
    }

    //testcase for removing cocourse alias
    @Test
    public void testRemoveCoCourseAlias()
    {
        qualificationManagementService.removeCoCourseAlias(118);
    }
}
