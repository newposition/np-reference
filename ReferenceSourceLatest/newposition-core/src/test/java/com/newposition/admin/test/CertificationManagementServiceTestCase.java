package com.newposition.admin.test;

import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.forms.AddNpCertificationForm;
import com.newposition.admin.service.CertificationManagementService;
import com.newposition.common.domain.CoSource;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class CertificationManagementServiceTestCase extends TestCase
{

    @Autowired
    private CertificationManagementService certificationManagementService;

    @Autowired
    private UserDetailsService userDetailsService;

    //testcase for updating the Certifications in the db
    @Test
    public void testSuccessSaveNpCertificationsWithId()
    {
        AddNpCertificationForm addNpCertificationForm = new AddNpCertificationForm();
        addNpCertificationForm.setNpInstituteCertificateId(31);
        addNpCertificationForm.setCertificationName("TestCertification");
        addNpCertificationForm.setValid(true);
        addNpCertificationForm.setCertificationType("sofware");
        addNpCertificationForm.setInstitutionId(200);

        certificationManagementService.saveNpCertifications(addNpCertificationForm);
    }

    //testcase for saving the Certifications in the db
    @Test
    public void testSuccessSaveNpCertificationsWithOutId()
    {
        AddNpCertificationForm addNpCertificationForm = new AddNpCertificationForm();
        addNpCertificationForm.setCertificationName("TestCertificate");
        addNpCertificationForm.setValid(true);
        addNpCertificationForm.setCertificationType("sofware");
        addNpCertificationForm.setInstitutionId(200);

        certificationManagementService.saveNpCertifications(addNpCertificationForm);
    }

    //testcase for deleting Certificates that is in db
    @Test
    public void testDeleteNpCertifications()
    {
        certificationManagementService.removeNpInstituteCertificateById(1);
    }

    //testcase for deleting Certificates that is not in db
    /*@Test(expected = IndexOutOfBoundsException.class)
	public void testDeleteNpCertificationsNotInDb() {
		certificationManagementService.removeNpInstituteCertificateById(0);	
	}*/

    //testcase for deleting coalias Certificates that is in db
    @Test
    public void testDeleteCoAliasCertificate()
    {
        certificationManagementService.removeCoCertificateAlias(13);
    }

    //testcase for deleting coalias Certificates that is not in db
	/*@Test(expected = IndexOutOfBoundsException.class)
	public void testDeleteCoAliasCertificatesNotInDb() {
		certificationManagementService.removeCoCertificateAlias(165162);
	}*/
	
	/*//testcase for deleting coalias Certificates with alphanumeric id
	@Test(expected = NumberFormatException.class)
	public void testDeleteCoAliasCertificatesWithOutProperFormat() {
		certificationManagementService.deleteCoAliasTools("12weee");
	}*/

    //testcase for getting list of cosources  in db
    @Test
    public void testGetCoSources()
    {
        Set<CoSource> coSources = certificationManagementService.getCoSources();
        assertNotNull(coSources);
    }
}
