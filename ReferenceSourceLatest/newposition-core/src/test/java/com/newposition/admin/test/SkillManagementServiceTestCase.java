package com.newposition.admin.test;

import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.exceptions.DomainNotFoundException;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class SkillManagementServiceTestCase extends TestCase
{

    @Autowired
    private SkillManagementService skillManagementService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SkillManagementDao skillManagementDao;

    //testcase for updating the skills in the db
    @Test
    public void testSuccessSaveNpSkillsWithId()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillId("1");
        addNpSKillsForm.setSkillName("TestSkill");
        addNpSKillsForm.setValid("Invalid");
        addNpSKillsForm.setDomainId("1");
        addNpSKillsForm.setSkillType("sofware");

        skillManagementService.saveNpSkills(addNpSKillsForm);
    }

    //testcase for saving the skills in the db
    @Test
    public void testSuccessSaveNpSkillsWithOutId()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillName("TestSkill1");
        addNpSKillsForm.setValid("Invalid");
        addNpSKillsForm.setDomainId("1");
        addNpSKillsForm.setSkillType("sofware");

        skillManagementService.saveNpSkills(addNpSKillsForm);
    }

    //testcase for saving the skills With Domain Not In Db
    @Test(expected = DomainNotFoundException.class)
    public void testSuccessSaveNpSkillsWithDomainNotInDb()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillName("TestSkill1");
        addNpSKillsForm.setValid("Invalid");
        addNpSKillsForm.setDomainId("-10");
        addNpSKillsForm.setSkillType("sofware");

        skillManagementService.saveNpSkills(addNpSKillsForm);
    }

    //testcase for deleting the skills with improper format id
    @Test(expected = NumberFormatException.class)
    public void testDeletNpSkillsWithOutProperFormat()
    {
        skillManagementService.deletNpSkills("154rf");
    }

    //testcase for deleting skills that is in db
    @Test
    public void testDeleteNpSkills()
    {
        skillManagementService.deletNpSkills("1");
    }

    //testcase for deleting skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteNpSkillsNotInDb()
    {
        skillManagementService.deletNpSkills("0");
    }

    //testcase for deleting coalias skills that is in db
    @Test
    public void testDeleteCoAliasSkills()
    {
        skillManagementService.deleteCoAliasSkills("13");
    }

    //testcase for deleting coalias skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteCoAliasSkillsNotInDb()
    {
        skillManagementService.deleteCoAliasSkills("12");
    }

    //testcase for deleting coalias skills with alphanumeric id
    @Test(expected = NumberFormatException.class)
    public void testDeleteCoAliasSkillsWithOutProperFormat()
    {
        skillManagementService.deleteCoAliasSkills("12weee");
    }

    //testcase for getting list of cosources  in db
    @Test
    public void testGetCoSources()
    {
        Set<CoSource> coSources = skillManagementService.getcoSources();
        assertNotNull(coSources);
    }

    //testcase for getting domain not in the DB
    @Test(expected = DomainNotFoundException.class)
    public void testGetDomainById()
    {
        skillManagementDao.getDomainById("-10");
    }

    //testcase for getting aliassearch skills that which is there in db
    @Test
    public void testGetNpSKillsforAliasSearch()
    {
        Set<NpSkillImpl> npSkills = skillManagementService.getNpSKillsforAliasSearch("testing", "238");
        assertEquals(1, npSkills.size());
    }

    //testcase for getting npskills not in db
    @Test
    public void testGetNpSKillsforAliasSearchNotInDb()
    {
        Set<NpSkillImpl> npSkills = skillManagementService.getNpSKillsforAliasSearch("---testing", "-238");
        assertEquals(0, npSkills.size());
    }

    //testcase for getting npskills alias search with impoper format input
    @Test(expected = NumberFormatException.class)
    public void testGetNpSKillsforAliasSearchForImproperFormat()
    {
        Set<NpSkillImpl> npSkills = skillManagementService.getNpSKillsforAliasSearch("---testing", "-ss238");
    }

    //testcase for merging npskills
    @Test
    public void testMergeNpSkill()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillId("1");
        addNpSKillsForm.setMergeToId("238");
        skillManagementService.mergeNpSkill(addNpSKillsForm);
    }

    //testcase for merging the skills with improper format
    @Test(expected = NumberFormatException.class)
    public void testMergeNpSkillNotImproperFormat()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillId("1");
        addNpSKillsForm.setMergeToId("--23889");
        skillManagementService.mergeNpSkill(addNpSKillsForm);
    }

    //testcase for merging skills not in the db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testMergeNpSkillNotInDb()
    {
        AddNpSKillsForm addNpSKillsForm = new AddNpSKillsForm();
        addNpSKillsForm.setSkillId("1");
        addNpSKillsForm.setMergeToId("-23889");
        skillManagementService.mergeNpSkill(addNpSKillsForm);
    }

    //testcases for getting NpFiltered Skills
    @Test
    public void getFilteredNpSkills()
    {
        List npskillsList = skillManagementService.getFilteredNpSkills("", "", "", "3", "0");
        assertEquals(2, npskillsList.get(1));
    }

    @Test
    public void getFilteredNpSkillsWithSkillTypeAndInvalid()
    {
        List npskillsList = skillManagementService.getFilteredNpSkills("", "software", "Invalid", "3", "0");
        assertEquals(0, npskillsList.get(1));
    }
}
