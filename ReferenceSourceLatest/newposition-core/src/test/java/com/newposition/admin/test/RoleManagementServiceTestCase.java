package com.newposition.admin.test;

import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.service.RoleManagementService;
import com.newposition.common.domain.CoSource;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class RoleManagementServiceTestCase extends TestCase
{

    @Autowired
    private RoleManagementService roleManagementService;

    @Autowired
    private UserDetailsService userDetailsService;

    //testcase for updating the Roles in the db
    @Test
    public void testSuccessSaveNpEmploymentRoleWithId()
    {
        AddNpEmploymentRoleForm addNpEmploymentRoleForm = new AddNpEmploymentRoleForm();
        addNpEmploymentRoleForm.setRoleId("1");
        addNpEmploymentRoleForm.setRoleName("TestSkill");
        addNpEmploymentRoleForm.setValid("Invalid");
        addNpEmploymentRoleForm.setDomainId("1");

        roleManagementService.saveNpEmploymentRoles(addNpEmploymentRoleForm);
    }

    //testcase for saving the Roles in the db
    @Test
    public void testSuccessSaveNpEmploymentRoleWithOutId()
    {
        AddNpEmploymentRoleForm addNpEmploymentRoleForm = new AddNpEmploymentRoleForm();
        addNpEmploymentRoleForm.setRoleName("TestSkill");
        addNpEmploymentRoleForm.setValid("Invalid");
        addNpEmploymentRoleForm.setDomainId("1");

        roleManagementService.saveNpEmploymentRoles(addNpEmploymentRoleForm);
    }

    //testcase for saving the Roles With Domain Not In Db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testSuccessSaveNpEmploymentRoleWithDomainNotInDb()
    {
        AddNpEmploymentRoleForm addNpEmploymentRoleForm = new AddNpEmploymentRoleForm();
        addNpEmploymentRoleForm.setRoleId("1");
        addNpEmploymentRoleForm.setRoleName("TestSkill");
        addNpEmploymentRoleForm.setValid("Invalid");
        addNpEmploymentRoleForm.setDomainId("10");

        roleManagementService.saveNpEmploymentRoles(addNpEmploymentRoleForm);
    }

    //testcase for deleting the Roles with improper format id
    @Test(expected = NumberFormatException.class)
    public void testDeletNpEmploymentRoleWithOutProperFormat()
    {
        roleManagementService.deletNpEmploymentRoles("154rf");
    }

    //testcase for deleting skills that is in db
    @Test
    public void testDeleteNpEmploymentRole()
    {
        roleManagementService.deletNpEmploymentRoles("1");
    }

    //testcase for deleting skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteNpEmploymentRoleNotInDb()
    {
        roleManagementService.deletNpEmploymentRoles("0");
    }

    //testcase for deleting coalias skills that is in db
    @Test
    public void testDeleteCoAliasRoles()
    {
        roleManagementService.deleteCoAliasRoles("10");
    }

    //testcase for deleting coalias skills that is not in db
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteCoAliasSkillsNotInDb()
    {
        roleManagementService.deleteCoAliasRoles("12");
    }

    //testcase for deleting coalias skills with alphanumeric id
    @Test(expected = NumberFormatException.class)
    public void testDeleteCoAliasRolesWithOutProperFormat()
    {
        roleManagementService.deleteCoAliasRoles("12weee");
    }

    //testcase for getting list of cosources  in db
    public void testGetCoSources()
    {
        Set<CoSource> coSources = roleManagementService.getcoSources();
        assertEquals(4, coSources.size());
    }
}
