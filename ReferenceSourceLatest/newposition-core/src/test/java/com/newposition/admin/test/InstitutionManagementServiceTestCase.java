package com.newposition.admin.test;

import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.forms.AddNpInstitutionForm;
import com.newposition.admin.service.NpAdminInstitutionManagementService;
import com.newposition.common.domain.CoSource;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-test.xml")
@Transactional
public class InstitutionManagementServiceTestCase extends TestCase
{

    @Autowired
    private NpAdminInstitutionManagementService institutionManagementService;

    @Autowired
    private UserDetailsService userDetailsService;

    //testcase for updating the Certifications in the db
    @Test
    public void testSuccessSaveNpCertificationsWithId()
    {
        AddNpInstitutionForm addNpInstitutionForm = new AddNpInstitutionForm();
        addNpInstitutionForm.setInstituteId(1);
        addNpInstitutionForm.setInstituteName("TestInstitution");
        addNpInstitutionForm.setValid(true);
        addNpInstitutionForm.setInstituteType("sofware");

        institutionManagementService.saveNpInstitute(addNpInstitutionForm);
    }

    //testcase for saving the Certifications in the db
    @Test
    public void testSuccessSaveNpCertificationsWithOutId()
    {
        AddNpInstitutionForm addNpInstitutionForm = new AddNpInstitutionForm();
        addNpInstitutionForm.setInstituteId(1);
        addNpInstitutionForm.setInstituteName("TestInstitution");
        addNpInstitutionForm.setValid(true);
        addNpInstitutionForm.setInstituteType("sofware");

        institutionManagementService.saveNpInstitute(addNpInstitutionForm);
    }

	/*//testcase for deleting the Certificates with improper format id
    @Test(expected = NumberFormatException.class)
	public void testDeletNpCertificationsWithOutProperFormat() {
		institutionManagementService.deleteNpTools("154rf");	
	}*/

    //testcase for deleting Certificates that is in db
    @Test
    public void testDeleteNpCertifications()
    {
        institutionManagementService.removeNpInstituteById(1);
    }

    //testcase for deleting Certificates that is not in db
    @Test(expected = Exception.class)
    public void testDeleteNpCertificationsNotInDb()
    {
        institutionManagementService.removeNpInstituteById(10222);
    }

    //testcase for deleting coalias Certificates that is in db
    @Test
    public void testDeleteCoAliasCertificate()
    {
        institutionManagementService.removeCoInstituteAlias(13);
    }

    //testcase for deleting coalias Certificates that is not in db
	/*@Test(expected = IndexOutOfBoundsException.class)
	public void testDeleteCoAliasCertificatesNotInDb() {
		institutionManagementService.removeCoInstituteAlias(12);
	}*/
	
	/*//testcase for deleting coalias Certificates with alphanumeric id
	@Test(expected = NumberFormatException.class)
	public void testDeleteCoAliasCertificatesWithOutProperFormat() {
		institutionManagementService.deleteCoAliasTools("12weee");
	}*/

    //testcase for getting list of cosources  in db
    @Test
    public void testGetCoSources()
    {
        Set<CoSource> coSources = institutionManagementService.getCoSources();
        assertNotNull(coSources);
    }
}
