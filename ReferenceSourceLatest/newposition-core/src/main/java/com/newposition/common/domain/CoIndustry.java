package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_industry", catalog = "npdbs")
public class CoIndustry
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private int industryID;

    @Column(name = "IndustryName", nullable = false, length = 20)
    private String industryName;

    @Column(name = "IndustryType", nullable = false, length = 20)
    private String industryType;

    @Column(name = "Valid", nullable = false, length = 20)
    private String Valid;

    public int getIndustryID()
    {
        return industryID;
    }

    public void setIndustryID(int industryID)
    {
        this.industryID = industryID;
    }

    public String getIndustryName()
    {
        return industryName;
    }

    public void setIndustryName(String industryName)
    {
        this.industryName = industryName;
    }

    public String getIndustryType()
    {
        return industryType;
    }

    public void setIndustryType(String industryType)
    {
        this.industryType = industryType;
    }

    public String getValid()
    {
        return Valid;
    }

    public void setValid(String valid)
    {
        Valid = valid;
    }
}
