package com.newposition.common.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.candidate.domain.NpCandidateTools;

public interface NpTool
{

    public Integer getId();

    public void setId(Integer id);

    public String getVersion();

    public void setVersion(String version);

    public NpDomainImpl getNpDomain();

    public void setNpDomain(NpDomainImpl npDomain);

    public String getToolName();

    public void setToolName(String toolName);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public String getValid();

    public void setValid(String valid);

    public String getToolType();

    public void setToolType(String toolType);

    public boolean isStatus();

    public void setStatus(boolean status);

    public Set<NpCandidateTools> getNpCandidateTools();

    public void setNpCandidateTools(Set<NpCandidateTools> npCandidateTools);

    public Set<CoToolAlias> getCoToolAlias();

    public void setCoToolAlias(Set<CoToolAlias> coToolAlias);
}
