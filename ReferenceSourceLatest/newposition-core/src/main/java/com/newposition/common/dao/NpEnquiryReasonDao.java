package com.newposition.common.dao;

import java.util.List;

import com.newposition.common.domain.NpEnquiry;

public interface NpEnquiryReasonDao
{

    public List<NpEnquiry> getReasonsForEnquiry();
}
