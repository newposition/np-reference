package com.newposition.common.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.common.dao.NpEnquiryReasonDao;
import com.newposition.common.domain.NpEnquiry;

public class NpEnquiryReasonDaoImpl implements NpEnquiryReasonDao
{

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpEnquiry> getReasonsForEnquiry()
    {
        // TODO Auto-generated method stub
        List<NpEnquiry> reasons = (List<NpEnquiry>) hibernateTemplate.find("from NpEnquiry");
        return reasons;
    }
}
