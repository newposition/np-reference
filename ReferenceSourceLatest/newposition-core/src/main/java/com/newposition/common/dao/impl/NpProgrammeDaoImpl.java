package com.newposition.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.common.dao.NpProgrammeDao;
import com.newposition.common.domain.NpDocumentTypes;
import com.newposition.common.domain.NpProgram;

public class NpProgrammeDaoImpl implements NpProgrammeDao
{

    protected static final Logger LOG = Logger.getLogger(NpUsersReasonDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

	/*@Override
    public List<NpProgram> getAllClientProgramme(NpClientImpl npClient) {
		List<NpProgram> programmes=getHibernateTemplate().getSessionFactory().getCurrentSession()
				      .createCriteria(NpProgram.class).add(Restrictions.eq("npClient", npClient)).list();
		return programmes;
	}*/

    @SuppressWarnings("unchecked")
    @Override
    public List<NpProgram> getAllClientProgramme(NpClientImpl npClient)
    {
        List<NpProgram> programmes = (List<NpProgram>) getHibernateTemplate().find("from  NpProgram np where np.npClient.id= ?", npClient.getId());
        return programmes;
    }

    @Override
    public void saveProgramme(NpProgram npProgram, NpClientProgramDocument proDocument)
    {
        NpDocumentTypes docType = new NpDocumentTypes();
        docType.setDocumentTypeID(1);
        proDocument.setDocumentTypeID(docType);
        Long programId = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(npProgram);
        proDocument.setClientProgramID(programId);
        getHibernateTemplate().getSessionFactory().getCurrentSession().save(proDocument);
    }

    @Override
    public NpProgram getNpProgrammeById(long id)
    {
        NpProgram npProgramme = (NpProgram) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpProgram.class).add(Restrictions.eq("clientProgramID", id)).uniqueResult();
        return npProgramme;
    }

    @Override
    public void deleteProgramme(NpProgram npProgramme)
    {
        System.out.println("in dao npProgramme=" + npProgramme);
        getHibernateTemplate().getSessionFactory().getCurrentSession().delete(npProgramme);
        System.out.println("after dao ");
    }

    @Override
    public NpProgram updateNpProgram(NpProgram npProgram)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().update(npProgram);
        return npProgram;
    }

    @Override
    public List<NpClientCostCentreImpl> getCostCenterForCode(String str)
    {
        System.out.println("in dao my");
        List<NpClientCostCentreImpl> costcenterlist = (List<NpClientCostCentreImpl>) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientCostCentreImpl.class).add(Restrictions.like("costCentreCode", "%" + str + "%")).list();
        System.out.println("costcenterlist=" + costcenterlist.size());
        return costcenterlist;
    }

    @Override
    public List<NpClientCostCentreImpl> getCostCenterForName(String str)
    {
        System.out.println("in dao my");
        List<NpClientCostCentreImpl> costcenterlist = (List<NpClientCostCentreImpl>) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientCostCentreImpl.class).add(Restrictions.like("costCentreName", "%" + str + "%")).list();
        System.out.println("costCentreName=" + costcenterlist.size());
        return costcenterlist;
    }

    @Override
    public List<NpClientCostCentreImpl> getCostCenterForLocation(String str)
    {
        System.out.println("in dao my");
        List<NpClientCostCentreImpl> costcenterlist = (List<NpClientCostCentreImpl>) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientCostCentreImpl.class).add(Restrictions.like("costCentreLocation", "%" + str + "%")).list();
        System.out.println("costCentreLocation=" + costcenterlist.size());
        return costcenterlist;
    }

    @Override
    public List<NpClientCostCentreImpl> getCostCenterForOwner(String str)
    {
        System.out.println("in dao my");
        List<NpClientCostCentreImpl> costcenterlist = (List<NpClientCostCentreImpl>) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientCostCentreImpl.class).add(Restrictions.like("costCentreOwner", "%" + str + "%")).list();
        System.out.println("costCentreOwner=" + costcenterlist.size());
        return costcenterlist;
    }
}
