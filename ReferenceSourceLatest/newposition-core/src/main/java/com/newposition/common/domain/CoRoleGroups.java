package com.newposition.common.domain;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "co_role_groups", catalog = "npdbs")
public class CoRoleGroups
{

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "groupId", column = @Column(name = "GroupID", nullable = false)),
            @AttributeOverride(name = "roleId", column = @Column(name = "RoleID", nullable = false))})
    private CoRoleGroupsId id;

    @Column(name = "CreateBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RoleID", nullable = false, insertable = false, updatable = false)
    private NpRoleImpl npRole;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GroupID", nullable = false, insertable = false, updatable = false)
    private CoGroups coGroups;

    public CoGroups getCoGroups()
    {
        return coGroups;
    }

    public void setCoGroups(CoGroups coGroups)
    {
        this.coGroups = coGroups;
    }

    public NpRoleImpl getNpRole()
    {
        return npRole;
    }

    public void setNpRole(NpRoleImpl npRole)
    {
        this.npRole = npRole;
    }

    public CoRoleGroupsId getId()
    {
        return id;
    }

    public void setId(CoRoleGroupsId id)
    {
        this.id = id;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
