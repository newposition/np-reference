package com.newposition.common.forms;

import org.hibernate.validator.constraints.NotEmpty;

import com.newposition.common.domain.NpUser;

public class ChangePaswordForm
{

    private String oldPassword;

    private String newPassword;

    @NotEmpty(message = "Please Provide ConfirmPassword")
    private String confirmPassword;

    private NpUser user;

    public NpUser getUser()
    {
        return user;
    }

    public void setUser(NpUser user)
    {
        this.user = user;
    }

    public String getNewPassword()
    {
        return newPassword;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword()
    {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword)
    {
        this.oldPassword = oldPassword;
    }
}
