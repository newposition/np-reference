package com.newposition.common.service;

import java.util.List;

import com.newposition.common.domain.NpEnquiry;

public interface NpEnquiryReasonService
{

    public List<NpEnquiry> getReasonsForEnquiry();
}
