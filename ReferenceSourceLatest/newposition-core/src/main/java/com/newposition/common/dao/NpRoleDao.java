package com.newposition.common.dao;

import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleType;

public interface NpRoleDao extends GenericDao<NpRole, Long>
{

    public NpRole findRoleByType(NpRoleType roleType);
}
