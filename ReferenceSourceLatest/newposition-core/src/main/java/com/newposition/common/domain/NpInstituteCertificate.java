package com.newposition.common.domain;

import java.util.Date;

import com.newposition.candidate.domain.NpCertifications;
import com.newposition.candidate.domain.NpInstitutions;

public interface NpInstituteCertificate extends java.io.Serializable
{

    public Integer getId();

    public void setId(Integer id);

    public NpCertifications getNpCertifications();

    public void setNpCertifications(NpCertifications npCertifications);

    public NpInstitutions getNpInstitutions();

    public void setNpInstitutions(NpInstitutions npInstitutions);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public boolean getStatus();

    public void setStatus(boolean status);
}
