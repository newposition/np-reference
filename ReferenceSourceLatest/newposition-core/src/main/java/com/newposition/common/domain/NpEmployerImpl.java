package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.admin.domain.CoEmployerAliasImpl;
import com.newposition.candidate.domain.NpCandidateEmployerImpl;

@Entity
@Table(name = "np_employer", catalog = "npdbs")
public class NpEmployerImpl implements NpEmployer
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SectorID")
    private NpSectorImpl npSector;

    @Column(name = "Name", nullable = false, length = 100)
    private String name;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @Column(name = "Valid", nullable = false, length = 10)
    private String valid;

    @Column(name = "IndustryType", nullable = false, length = 10)
    private String industryType;

    @Column(name = "status")
    private boolean status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "npEmployer")
    private Set<NpCandidateEmployerImpl> npCandidateEmployers = new HashSet<NpCandidateEmployerImpl>(0);

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "npEmployerImpl")
    private Set<CoEmployerAliasImpl> coEmployerAlias;

    @Override
    public Set<NpCandidateEmployerImpl> getNpCandidateEmployers()
    {
        return npCandidateEmployers;
    }

    @Override
    public void setNpCandidateEmployers(
            Set<NpCandidateEmployerImpl> npCandidateEmployers)
    {
        this.npCandidateEmployers = npCandidateEmployers;
    }

    public Set<CoEmployerAliasImpl> getCoEmployerAlias()
    {
        return coEmployerAlias;
    }

    public void setCoEmployerAlias(Set<CoEmployerAliasImpl> coEmployerAlias)
    {
        this.coEmployerAlias = coEmployerAlias;
    }

    @Override
    public Integer getId()
    {
        return this.id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public NpSector getNpSector()
    {
        return this.npSector;
    }

    @Override
    public void setNpSector(NpSector npSector)
    {
        this.npSector = (NpSectorImpl) npSector;
    }

    @Override
    public String getName()
    {
        return this.name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String getValid()
    {
        return valid;
    }

    @Override
    public void setValid(String valid)
    {
        this.valid = valid;
    }

    @Override
    public String getIndustryType()
    {
        return industryType;
    }

    @Override
    public void setIndustryType(String industryType)
    {
        this.industryType = industryType;
    }

    @Override
    public boolean isStatus()
    {
        return status;
    }

    @Override
    public void setStatus(boolean status)
    {
        this.status = status;
    }
}
