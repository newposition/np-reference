package com.newposition.common.presentation.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;

/**
 * Handler class on successful authentication.
 *
 * @author sachi N
 */
public class RedirectingAuthenticationSuccessHandler implements AuthenticationSuccessHandler
{

    protected static final Logger LOG = Logger.getLogger(RedirectingAuthenticationSuccessHandler.class);

    private NpUserDao npUserDao;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException
    {

        String userName = request.getParameter("j_username");

        /**
         *
         * getting original User
         *
         */
        NpUser user1 = (NpUser) request.getSession().getAttribute("user");

        String name = authentication.getName();
        LOG.info("SwitchAuthenticationSuccess!" + name + " username: " + userName);

        NpUser user = npUserDao.findByUserName(name);
        if (user != null)
        {
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("swtchUser", "j_spring_security_exit_user");
            request.getSession().setAttribute("isImpersonate", true);

            LOG.debug("Switch in username is: " + user.getFirstName());

            if (StringUtils.isNotEmpty(userName))
            {
                request.getSession().setAttribute("user1", user1);

                response.sendRedirect("home");
            }
            else
            {
                NpUser _user = (NpUser) request.getSession().getAttribute("user1");

                String pageName = getBackPage(_user);
                request.getSession().setAttribute("isImpersonate", null);
                request.getSession().setAttribute("swtchUser", null);
                request.getSession().setAttribute("user1", null);
                response.sendRedirect(pageName);
            }
        }
        else
        {
            request.getSession().setAttribute("user1", null);
            request.getSession().setAttribute("user", null);
            request.getSession().setAttribute("swtchUser", null);
            request.getSession().setAttribute("isImpersonate", null);
            response.sendRedirect("clientSupportImpersonateUser");
        }
    }

    private String getBackPage(NpUser _user)
    {

        String pageName = "";
        Set<NpUserRole> userRoles = _user.getNpUserRole();
        List<NpUserRole> userRoleList = new ArrayList<NpUserRole>(userRoles);
        StringBuilder roleNames = new StringBuilder();
        for (NpUserRole uRole : userRoleList)
        {

            roleNames.append(uRole.getNpRole().getRoleName() + ",");
        }
        String userRoleName = roleNames.toString();

        if (userRoleName.contains("ROLE_NP_USERSUPPORT") || userRoleName.contains("ROLE_CLIENT_LIASON") || userRoleName.contains("ROLE_SKILLS_ANALYST"))
        {
            pageName = "clientSupportImpersonateUser";
        }
        else if (userRoleName.contains("ROLE_CLIENT_ADMINISTRATOR") || userRoleName.contains("ROLE_CLIENT_INTERVIEWER") || userRoleName.contains("ROLE_CLIENT_MANAGER") ||
                userRoleName.contains("ROLE_CLIENT_EMPLOYEE") || userRoleName.contains("ROLE_CLIENT_SUPPORT"))
        {
            pageName = "impersonate_user";
        }

        return pageName;
    }

    public void setNpUserDao(NpUserDao npUserDao)
    {
        this.npUserDao = npUserDao;
    }
}
