package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_industry_sector", catalog = "npdbs")
public class CoIndustrySector
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "IndustryID", unique = true, nullable = false)
    private int industryID;

    @Column(name = "SectorID", nullable = false, length = 20)
    private int sectorID;

    @Column(name = "Status", nullable = false, length = 20)
    private boolean status;

    public boolean isStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public int getIndustryID()
    {
        return industryID;
    }

    public void setIndustryID(int industryID)
    {
        this.industryID = industryID;
    }

    public int getSectorID()
    {
        return sectorID;
    }

    public void setSectorID(int sectorID)
    {
        this.sectorID = sectorID;
    }
}
