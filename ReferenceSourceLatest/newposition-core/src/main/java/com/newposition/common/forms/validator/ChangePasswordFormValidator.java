/**
 *
 */
package com.newposition.common.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.common.forms.ChangePaswordForm;
import com.newposition.common.forms.NewPaswordForm;

/*
 * @author ravi
 *
 *
 */
public class ChangePasswordFormValidator implements Validator
{
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return clazz.equals(ChangePasswordFormValidator.class);
    }

    @Override
    public void validate(Object target, Errors errors)
    {

        ChangePaswordForm form = (ChangePaswordForm) target;

        ValidationUtils.rejectIfEmpty(errors, "oldPassword", "oldpassword.required");

        ValidationUtils.rejectIfEmpty(errors, "newPassword", "newpassword.required");

        boolean isValidPwd = true;
        if (StringUtils.isNotEmpty(form.getNewPassword()))
        {

            if (StringUtils.length(form.getNewPassword()) < 7)
            {
                isValidPwd = false;
                errors.rejectValue("newPassword", "confirmpassword.required");
            }
            else if (!isValidPassword(form.getNewPassword()))
            {
                isValidPwd = false;
                errors.rejectValue("newPassword", "confirmpassword.required");
            }
        }
        else
        {
            isValidPwd = false;
        }

        if (isValidPwd && (form.getConfirmPassword() == null || form.getConfirmPassword().length() == 0))
        {
            errors.rejectValue("confirmPassword", "confirmpassword.required");
        }
        else if (isValidPwd && form.getConfirmPassword().length() > 0)
        {

            if (!(form.getNewPassword()).equals(form.getConfirmPassword()))
            {
                errors.rejectValue("confirmPassword", "confirm.pwd.nomatch");
            }
        }
    }

    public boolean isValidPassword(String password)
    {
        Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$");
        Pattern pattern1 = Pattern.compile(".*[-~\\!\\@\\#\\$\\%\\^\\&\\.\\,\\*\\(\\)\\_\\+\\=\\{\\}\\|\\:\\\\'\\[\\]\\<\\>\\?\\/\\'\\^].*");
        Matcher match = pattern.matcher(password);
        Matcher match1 = pattern1.matcher(password);

        //boolean c1 = true, c2 = true;
        // LOG.info("@@  "+match.find()+" -- "+match1.find());
        boolean validCon_1 = match.find();
        boolean spclChar = match1.find();
        boolean validCon_2 = (spclChar && password.matches(".*[A-Z].*") && password.matches(".*[a-z].*"));
        // LOG.info("@@  "+fst+" -- "+scnd);
        if (validCon_1 && spclChar)
        {
            return false;
        }
        else if (validCon_1)
        {
            return true;
        }
        else if (validCon_2)
        {

            return true;
        }
        else
        {
            return false;
        }
    }
}
