package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CoRoleGroupsId implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "GroupId", nullable = false)
    private int groupId;

    @Column(name = "RoleID", nullable = false)
    private int roleId;

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getRoleId()
    {
        return roleId;
    }

    public void setRoleId(int roleId)
    {
        this.roleId = roleId;
    }
}
