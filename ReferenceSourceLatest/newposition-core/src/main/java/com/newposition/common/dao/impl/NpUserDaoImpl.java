package com.newposition.common.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpResetImpl;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleImpl;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserAttempts;
import com.newposition.common.domain.NpUserAttemptsImpl;
import com.newposition.common.domain.NpUserImpl;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.domain.NpUserStatusImpl;
import com.newposition.common.forms.NewPaswordForm;
import com.newposition.util.DataSecurity;

public class NpUserDaoImpl extends GenericDaoImpl<NpUser, Long> implements NpUserDao
{

//    private HibernateTemplate hibernateTemplate;

    private PasswordEncoder passwordEncoder;

    public void setPasswordEncoder(PasswordEncoder passwordEncoder)
    {
        this.passwordEncoder = passwordEncoder;
    }

   /* public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
    this.hibernateTemplate = hibernateTemplate;
    }*/

    @Override
    @SuppressWarnings("unchecked")
    public NpUser findByUserName(String userName)
    {

        List<NpUser> users = (List<NpUser>) getHibernateTemplate().find(
                "from com.newposition.common.domain.NpUserImpl U WHERE U.primaryEmail=?", userName);
        if (users != null && users.size() > 0)
        {
            return users.get(0);
        }
        return null;
    }

    @Override
    public List<NpUser> findAll()
    {
        return getHibernateTemplate().loadAll(NpUser.class);
    }

    @Override
    public NpUser findByID(Integer id)
    {
        return getHibernateTemplate().get(NpUserImpl.class, id);
    }

    @Override
    public void setActivationCode(NpReset npReset)
    {
        // TODO Auto-generated method stub
        @SuppressWarnings("unchecked")
        List<NpReset> npResetList = (List<NpReset>) getHibernateTemplate().find("from NpResetImpl where email=?", npReset.getEmail());
        Calendar cal = Calendar.getInstance();

        if (npResetList.size() > 0)
        {
            try
            {
                npResetList.get(0).setActivation(DataSecurity.encrypt(npReset.getActivation().trim()));
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
            npResetList.get(0).setTime(cal.getTime());
            getHibernateTemplate().update(npResetList.get(0));
        }
        else
        {
            try
            {
                npReset.setActivation(DataSecurity.encrypt(npReset.getActivation().trim()));
                npReset.setTime(cal.getTime());
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
            getHibernateTemplate().save(npReset);
        }
    }

    @Override
    public NpUser updateFailureAttempts(String userName)
    {
        NpUser npUser = findByUserName(userName);
        if (npUser != null)
        {
            Integer attempt = npUser.getNpUserAttempts().getAttempts();

            if (attempt < 2)
            {

                npUser.getNpUserAttempts().setAttempts(attempt + 1);
//            getHibernateTemplate().update(npUser);

            }
            else if (attempt == 2)
            {
                // at 3rd attempt

                npUser.getNpUserAttempts().setAttempts(attempt + 1);
                NpUserStatus userStatus = getHibernateTemplate().get(NpUserStatusImpl.class, 2);
                npUser.setNpUserStatus(userStatus);
//            getHibernateTemplate().update(npUser);

            }
            else
            {

                // attempt more than 3 times attempts incremented
                npUser.getNpUserAttempts().setAttempts(attempt + 1);
//            getHibernateTemplate().update(npUser);
            }

            // email not exist
            return npUser;
        }
        else
        {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpUserStatus getNpUserStatusById(Integer id)
    {
        // TODO Auto-generated method stub
        List<NpUserStatus> npUserStatus = (List<NpUserStatus>) getHibernateTemplate().find("from NpUserStatusImpl where id=?", id);
        return npUserStatus.get(0);
    }

    @Override
    public boolean setnewpassword(NewPaswordForm newPaswordForm)
    {
        // TODO Auto-generated method stub

        /*Query updateQuery =  (Query) getHibernateTemplate().getSessionFactory().openSession()
                .createQuery("update NpUserImpl l set l.encryptedPassword = :pwd where primaryEmailAddress = :email")
                .setParameter("pwd", newPaswordForm.getNewPassword())
                .setParameter("email", newPaswordForm.getEmail());
        updateQuery.executeUpdate();*/

        NpUser user = findByUserName(newPaswordForm.getEmail());
        user.setEncryptedPassword(newPaswordForm.getNewPassword());
        getHibernateTemplate().update(user);
        return true;
    }

    @Override
    public boolean unlockAccount(Integer userId)
    {
        // TODO Auto-generated method stub

        NpUser user = findByID(userId);

        /*
         * Check if the status is already active or not
         *  if active then link has been visited before
         *     else active the user 
         * 
         */
        if (user.getNpUserStatus().getId() == 1)
        {
            return false;
        }
	    /*
         * Enable the Status of user
         */
        NpUserStatus userStatus = (NpUserStatus) getHibernateTemplate().get(NpUserStatusImpl.class, 1);
        user.setNpUserStatus(userStatus);

        /*
         * Reset the user Attempts to 0
         */

        NpUserAttempts userAtempts = (NpUserAttempts) getHibernateTemplate().get(NpUserAttemptsImpl.class, user.getNpUserAttempts().getId());
        userAtempts.setAttempts(0);
        user.setNpUserAttempts(userAtempts);

//        ht.save(user);
        if (user.getNpUserStatus().getId() == 1)
        {
            return true;
        }
        return false;
    }

    @Override
    public void updateNpUser(NpUser npUser)
    {
        getHibernateTemplate().update(npUser);
    }

    @Override
    public void updateNpCandidateRole(NpUserRole npUserRole)
    {
        getHibernateTemplate().save(npUserRole);
    }

    @Override
    public NpReset getActivationCode(String email)
    {

        // TODO Auto-generated method stub
        @SuppressWarnings("unchecked")
        List<NpResetImpl> npResetList = (List<NpResetImpl>) getHibernateTemplate().find("from NpResetImpl where email=?", email);
        if (npResetList.size() > 0)
        {
            return npResetList.get(0);
        }
        else
        {
            return null;
        }
    }

    @Override
    public NpReset resetPassword(NpReset resetCode)
    {
        // TODO Auto-generated method stub

        NpReset npReset = getActivationCode(resetCode.getEmail());
        Calendar cal = Calendar.getInstance();

        if (npReset != null)
        {
            try
            {
                npReset.setActivation(DataSecurity.encrypt(resetCode.getActivation().trim()));
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
            npReset.setTime(cal.getTime());
            getHibernateTemplate().update(npReset);
        }
        else
        {
            try
            {
                resetCode.setActivation(DataSecurity.encrypt(resetCode.getActivation().trim()));
                resetCode.setTime(cal.getTime());
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
            getHibernateTemplate().save(resetCode);
            npReset = resetCode;
        }
        return npReset;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpReset verifyActivationCode(String activationCode)
    {
        List<NpResetImpl> list = null;

        try
        {
            System.out.println(activationCode + "=" + DataSecurity.encrypt(activationCode));
            list = (List<NpResetImpl>) getHibernateTemplate().find("from NpResetImpl where activation=?", DataSecurity.encrypt(activationCode));
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }
        if (list.size() > 0)
        {

            return (NpReset) list.get(0);
        }
        else
        {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpUserStatus> fetchAllUserStatus()
    {

        return (List<NpUserStatus>) getHibernateTemplate().find("from NpUserStatusImpl where id!=4");
    }

    @Override
    public NpUser updateSuccessAttempts(String userName)
    {
        NpUser npUser = findByUserName(userName);
        if (npUser != null)
        {
            npUser.getNpUserAttempts().setAttempts(0);
            return npUser;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void activateUserStatus(String decoded)
    {
        NpUser npUser = findByUserName(decoded);

        NpUserStatus npUserStatus = getHibernateTemplate().get(NpUserStatusImpl.class, 1);
        npUser.setNpUserStatus(npUserStatus);

        getHibernateTemplate().save(npUser);
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpRole findRoleByRoleName(String roleName)
    {

        List<NpRoleImpl> roleList = (List<NpRoleImpl>) getHibernateTemplate().find("from NpRoleImpl where roleName=?", roleName);
        return roleList.get(0);
    }
}
