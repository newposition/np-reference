package com.newposition.common.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.common.dao.NpRoleDao;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleImpl;
import com.newposition.common.domain.NpRoleType;

public class NpRoleDaoImpl extends GenericDaoImpl<NpRole, Long> implements NpRoleDao
{

    private HibernateTemplate hibernateTemplate;

	 /*   public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
	    }
*/
//    private PasswordEncoder encoder;

    /*@PostConstruct
    public void initIt() throws Exception {

	Session session = hibernateTemplate.getSessionFactory().openSession();
	session.setFlushMode(FlushMode.AUTO);
	Transaction transaction = session.beginTransaction();

	NpRole admin = new NpRoleImpl();
	admin.setRoleName("ROLE_ADMIN");
	admin.setRoleStatus("Active");

	NpRole clientuser = new NpRoleImpl();
	clientuser.setRoleName("ROLE_CLIENT_ADMIN");
	clientuser.setRoleStatus("Active");

	NpRole candidate = new NpRoleImpl();
	candidate.setRoleName("ROLE_CANDIDATE");
	candidate.setRoleStatus("Active");

	session.save(admin);
	session.save(clientuser);
	session.save(candidate);

	NpUser user = new NpUserImpl();
	user.setFirstName("Avinash");
	user.setLastName("gunne");
	user.setPhone("929292929");
	user.setPrimaryEmail("avinash@jarvis.com");
	user.setEncryptedPassword(encoder.encode("password"));

	user.setNpRoles(Collections.singletonList(admin));

	NpUser user1 = new NpUserImpl();
	user1.setFirstName("Keerthi");
	user1.setLastName("gunne");
	user1.setPhone("929292929");
	user1.setPrimaryEmail("keerthi@jarvis.com");
	user1.setEncryptedPassword(encoder.encode("password"));

	user1.setNpRoles(Collections.singletonList(candidate));

	NpSector sector = new NpSectorImpl();
	sector.setSectorName("IT");
	NpSector sector1 = new NpSectorImpl();
	sector1.setSectorName("Construction");
	NpSector sector2 = new NpSectorImpl();
	sector2.setSectorName("Educational Services");
	NpSector sector3 = new NpSectorImpl();
	sector3.setSectorName("Real Estate");
	NpSector sector4 = new NpSectorImpl();
	sector4.setSectorName("Automobiles");

	NpClientStatus status1 = new NpClientStatusImpl();
	status1.setStatus("New Client");
	session.save(status1);

	NpClientStatus status2 = new NpClientStatusImpl();
	status2.setStatus("Onboarding");
	session.save(status2);

	NpClientStatus status3 = new NpClientStatusImpl();
	status3.setStatus("Active");
	session.save(status3);

	NpClientStatus status4 = new NpClientStatusImpl();
	status4.setStatus("Suspended");
	session.save(status4);

	NpClientStatus status5 = new NpClientStatusImpl();
	status5.setStatus("Disable");
	session.save(status5);

	NpClientStatus status6 = new NpClientStatusImpl();
	status6.setStatus("In Active");
	session.save(status6);

	session.save(sector);
	session.save(sector1);
	session.save(sector2);
	session.save(sector3);
	session.save(sector4);

	session.save(user);

	session.save(user1);

	transaction.commit();
	session.close();
    }*/

    public NpRole findRoleByType(NpRoleType roleType)
    {

        @SuppressWarnings("unchecked")
        List<NpRole> roles = (List<NpRole>) getHibernateTemplate().findByNamedParam(
                "from com.newposition.common.domain.NpRole R WHERE R.roleName=:roleName", "roleName", roleType.toString());
        if (roles != null && roles.size() > 0)
        {
            return roles.get(0);
        }
        return null;
    }

    @Override
    public List<NpRole> findAll()
    {
        return getHibernateTemplate().loadAll(NpRole.class);
    }

    @Override
    public NpRole findByID(Integer id)
    {
        return getHibernateTemplate().get(NpRoleImpl.class, id);
    }
}
