package com.newposition.common.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.common.domain.CommonBean;
import com.newposition.common.domain.NpProgram;

public interface NpProgrammeService
{

    List<NpProgram> getAllClientProgramme(NpClientImpl npClient);

    void saveProgramme(NpProgram npProgram, NpClientProgramDocument proDoc);

    NpProgram getNpProgrammeById(long id);

    List<String> getStringList(String str, List<NpProgram> programmes);

    List<NpProgram> getClientProjectsByFilter(String str1, String str2,
                                              List<NpProgram> programmes);

    List<NpProgram> getProjectsBySort(ArrayList<NpProgram> programelist,
                                      String str);

    String deleteProject(int id);

    NpProgram saveOrUpdate(NpProgram npProgram);

    List<CommonBean> getCostCentercommonlist(String str);

    String checkProgramCode(String str, NpClientImpl npClient);

    byte[] getVideo(File video);

    NpProgram updateWithNewValue(NpProgram npProgram, NpProgram program);

    List<NpProgram> getAllClientProgramsWithVideo(NpClient client);
}
