package com.newposition.common.domain;

import java.io.Serializable;

public interface NpRole extends Serializable
{

    public int getId();

    public void setId(int id);

    public String getRoleName();

    public void setRoleName(String roleName);

    public String getRoleStatus();

    public void setRoleStatus(String roleStatus);
}
