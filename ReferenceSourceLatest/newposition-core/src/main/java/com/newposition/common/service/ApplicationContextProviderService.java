package com.newposition.common.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextProviderService implements ProviderService<ApplicationContext>, ApplicationContextAware
{

    private static ApplicationContextProviderService provider = new ApplicationContextProviderService();

    private ApplicationContext applicationContext;

    @Override
    public ApplicationContext get()
    {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    /**
     * This method returns a singleton instance of the ApplicationContextProviderService
     * class
     *
     * @return ApplicationContextProviderService singleton instance
     */
    public static ApplicationContextProviderService getInstance()
    {
        return provider;
    }
}
