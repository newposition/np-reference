package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "co_zip", catalog = "npdbs")
public class CoZip
{

    @Id
    @GeneratedValue
    @Column(name = "ZIP")
    private Integer zIP;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "StateID")
    private CoLocationState coLocationState;

    public Integer getzIP()
    {
        return zIP;
    }

    public void setzIP(Integer zIP)
    {
        this.zIP = zIP;
    }

    public CoLocationState getCoLocationState()
    {
        return coLocationState;
    }

    public void setCoLocationState(CoLocationState coLocationState)
    {
        this.coLocationState = coLocationState;
    }

    @Override
    public int hashCode()
    {
        return this.coLocationState.getStateDescription().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        return (((CoZip) obj).getzIP() == this.zIP);
    }
}
