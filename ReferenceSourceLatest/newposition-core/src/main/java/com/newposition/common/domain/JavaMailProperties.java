package com.newposition.common.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "np_java_mail_properties", catalog = "npdbs")
public class JavaMailProperties
{

    @Id
    @GeneratedValue
    private Integer mailPropId;
    private String mailSmtpHost;
    private String mailSmtpSocketFactoryPort;
    private String mailSmtpSocketFactoryClass;
    private String mailSmtpAuth;
    private String mailSmtpPort;

    public Integer getMailPropId()
    {
        return mailPropId;
    }

    public void setMailPropId(Integer mailPropId)
    {
        this.mailPropId = mailPropId;
    }

    public String getMailSmtpHost()
    {
        return mailSmtpHost;
    }

    public void setMailSmtpHost(String mailSmtpHost)
    {
        this.mailSmtpHost = mailSmtpHost;
    }

    public String getMailSmtpSocketFactoryPort()
    {
        return mailSmtpSocketFactoryPort;
    }

    public void setMailSmtpSocketFactoryPort(String mailSmtpSocketFactoryPort)
    {
        this.mailSmtpSocketFactoryPort = mailSmtpSocketFactoryPort;
    }

    public String getMailSmtpSocketFactoryClass()
    {
        return mailSmtpSocketFactoryClass;
    }

    public void setMailSmtpSocketFactoryClass(String mailSmtpSocketFactoryClass)
    {
        this.mailSmtpSocketFactoryClass = mailSmtpSocketFactoryClass;
    }

    public String getMailSmtpAuth()
    {
        return mailSmtpAuth;
    }

    public void setMailSmtpAuth(String mailSmtpAuth)
    {
        this.mailSmtpAuth = mailSmtpAuth;
    }

    public String getMailSmtpPort()
    {
        return mailSmtpPort;
    }

    public void setMailSmtpPort(String mailSmtpPort)
    {
        this.mailSmtpPort = mailSmtpPort;
    }
}
