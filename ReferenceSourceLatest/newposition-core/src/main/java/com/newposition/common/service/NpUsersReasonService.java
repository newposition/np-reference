package com.newposition.common.service;

import java.util.List;

import com.newposition.common.domain.NpUsersReason;

public interface NpUsersReasonService
{
    public List<NpUsersReason> getReasonsForUser();

    public List<NpUsersReason> getReasonsForClient();

    public NpUsersReason getReasonForId(int id);
}
