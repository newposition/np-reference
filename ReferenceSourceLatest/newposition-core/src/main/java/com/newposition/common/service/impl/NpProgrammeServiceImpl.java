package com.newposition.common.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.client.domain.NpClientProject;
import com.newposition.common.dao.NpProgrammeDao;
import com.newposition.common.domain.CommonBean;
import com.newposition.common.domain.NpProgram;
import com.newposition.common.service.NpProgrammeService;

@Service
public class NpProgrammeServiceImpl implements NpProgrammeService
{

    private NpProgrammeDao npProgrammeDao;

    public NpProgrammeDao getNpProgrammeDao()
    {
        return npProgrammeDao;
    }

    public void setNpProgrammeDao(NpProgrammeDao npProgrammeDao)
    {
        this.npProgrammeDao = npProgrammeDao;
    }

    @Override
    @Transactional
    public List<NpProgram> getAllClientProgramme(NpClientImpl npClient)
    {
        List<NpProgram> programmes = npProgrammeDao.getAllClientProgramme(npClient);
        return programmes;
    }

    @Override
    @Transactional
    public void saveProgramme(NpProgram npProgramme, NpClientProgramDocument proDoc)
    {
        NpProgram program = getProgramInitCap(npProgramme);
        npProgrammeDao.saveProgramme(program, proDoc);
    }

    @Override
    @Transactional
    public NpProgram getNpProgrammeById(long id)
    {
        NpProgram npProgramme = npProgrammeDao.getNpProgrammeById(id);
        String videoUrl = getVideoUrl(npProgramme);
        npProgramme.setVideoUrl(videoUrl);

        return npProgramme;
    }

    private String getVideoUrl(NpProgram npProgramme)
    {

        if (npProgramme.getProgramDoc().getDocument() != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("data:video/mp4;base64,");
            String videoUrl = "";

            videoUrl = org.apache.commons.codec.binary.StringUtils.newStringUtf8(org.apache.commons.codec.binary.Base64.encodeBase64(npProgramme.getProgramDoc().getDocument()));

            sb.append(videoUrl);
            String url = sb.toString();
//				System.out.println("video url : "+url);

            return url;
        }

        return null;
    }

    @Override
    @Transactional
    public String deleteProject(int id)
    {
        NpProgram npProgramme = npProgrammeDao.getNpProgrammeById(id);
        System.out.println("in delete npProgramme=" + npProgramme);
        npProgrammeDao.deleteProgramme(npProgramme);
        return "success";
    }

    @Override
    @Transactional
    public List<String> getStringList(String str, List<NpProgram> programmes)
    {
        List<String> strList = new ArrayList<String>();
        for (int i = 0; i < programmes.size(); i++)
        {
            NpProgram npProgramme = programmes.get(i);
            if (str.equals("programCode"))
            {
                strList.add(npProgramme.getProgramCode());
            }
            else if (str.equals("programOwner"))
            {
                strList.add(npProgramme.getProgramOwner());
            }
            else if (str.equals("programSponsor"))
            {
                strList.add(npProgramme.getProgramSponsor());
            }
            else if (str.equals("programCostcenterCode"))
            {
                strList.add(npProgramme.getNpClientCostCentreImpl().getCostCentreCode());
            }
            else if (str.equals("programCostcenterName"))
            {
                strList.add(npProgramme.getNpClientCostCentreImpl().getCostCentreName());
            }
        }

        List<String> strList1 = new ArrayList<String>(new HashSet<String>(strList));
        return strList1;
    }

    @Override
    @Transactional
    public List<NpProgram> getClientProjectsByFilter(String str1, String str2,
                                                     List<NpProgram> programmes)
    {
        List<NpProgram> programmes1 = new ArrayList<NpProgram>();
        for (int i = 0; i < programmes.size(); i++)
        {
            NpProgram npProgramme = programmes.get(i);
            if (str1.equals("programCode"))
            {
                if (str2.equals(npProgramme.getProgramCode()))
                {
                    programmes1.add(npProgramme);
                }
            }
            else if (str1.equals("programOwner"))
            {
                if (str2.equals(npProgramme.getProgramOwner()))
                {
                    programmes1.add(npProgramme);
                }
            }
            else if (str1.equals("programSponsor"))
            {
                if (str2.equals(npProgramme.getProgramSponsor()))
                {
                    programmes1.add(npProgramme);
                }
            }
            else if (str1.equals("programCostcenterCode"))
            {
                if (str2.equals(npProgramme.getNpClientCostCentreImpl().getCostCentreCode()))
                {
                    programmes1.add(npProgramme);
                }
            }
            else if (str1.equals("programCostcenterName"))
            {
                if (str2.equals(npProgramme.getNpClientCostCentreImpl().getCostCentreName()))
                {
                    programmes1.add(npProgramme);
                }
            }
        }
        return programmes1;
    }

    @Override
    @Transactional
    public List<NpProgram> getProjectsBySort(
            ArrayList<NpProgram> programelist, String str)
    {
        NpProgram npProgramme = new NpProgram();
        if (str.equals("programCode"))
        {
            npProgramme.sortByProgramCode(programelist);
        }
        else if (str.equals("programSponsor"))
        {
            npProgramme.sortByProgramSponsor(programelist);
        }
        else if (str.equals("programOwner"))
        {
            npProgramme.sortByProgramOwner(programelist);
        }
        else if (str.equals("programCostcenterCode"))
        {
            npProgramme.sortByClientCostCentreCode(programelist);
        }
        else if (str.equals("programCostcenterName"))
        {
            npProgramme.sortByClientCostCentreName(programelist);
        }
        return programelist;
    }

    @Override
    @Transactional
    public NpProgram saveOrUpdate(NpProgram npProgram)
    {
        npProgram = getProgramInitCap(npProgram);
        NpProgram npProgram1 = npProgrammeDao.updateNpProgram(npProgram);
        return npProgram1;
    }

    private NpProgram getProgramInitCap(NpProgram npProgram)
    {

        npProgram.setProgramCode(StringUtils.capitalize(npProgram.getProgramCode()));
        npProgram.setProgramOwner(StringUtils.capitalize(npProgram.getProgramOwner()));
        npProgram.setProgramSponsor(StringUtils.capitalize(npProgram.getProgramSponsor()));
        npProgram.setProgramDescription(StringUtils.capitalize(npProgram.getProgramDescription()));

        return npProgram;
    }

    //query for Cost Center
    @Override
    @Transactional
    public List<CommonBean> getCostCentercommonlist(String str)
    {
        List<CommonBean> commonBeanList = new ArrayList<CommonBean>();
        List<NpClientCostCentreImpl> costCenterList = new ArrayList<NpClientCostCentreImpl>();
        List<NpClientCostCentreImpl> costCenterList1 = npProgrammeDao.getCostCenterForCode(str);
        List<NpClientCostCentreImpl> costCenterList2 = npProgrammeDao.getCostCenterForName(str);
        List<NpClientCostCentreImpl> costCenterList3 = npProgrammeDao.getCostCenterForLocation(str);
        List<NpClientCostCentreImpl> costCenterList4 = npProgrammeDao.getCostCenterForOwner(str);
        costCenterList.addAll(costCenterList1);
        costCenterList.addAll(costCenterList2);
        costCenterList.addAll(costCenterList3);
        costCenterList.addAll(costCenterList4);
        System.out.println("costCenterList==" + costCenterList.size());
        List<NpClientCostCentreImpl> costCenterList11 = new ArrayList<NpClientCostCentreImpl>(new HashSet<NpClientCostCentreImpl>(costCenterList));
        System.out.println("costCenterList11==" + costCenterList11.size());
        for (int i = 0; i < costCenterList11.size(); i++)
        {
            NpClientCostCentreImpl ccBean = costCenterList11.get(i);
            CommonBean commonBean = new CommonBean();
            commonBean.setId(ccBean.getCostCentreId());
            commonBean.setValue(ccBean.getCostCentreCode() + "-" + ccBean.getCostCentreName() + "-" + ccBean.getCostCentreLocation() + "-" + ccBean.getCostCentreOwner());
            commonBeanList.add(commonBean);
        }
        return commonBeanList;
    }

    @Override
    public String checkProgramCode(String str, NpClientImpl npClient)
    {
        int refCount = 0;
        String result = "";
        List<NpProgram> programmes = npProgrammeDao.getAllClientProgramme(npClient);
        for (int i = 0; i < programmes.size(); i++)
        {
            NpProgram npProgram = programmes.get(i);
            if (npProgram.getProgramCode().equals(str))
            {
                refCount++;
            }
        }
        if (refCount > 0)
        {
            return "NOT OK";
        }
        else
        {
            return "OK";
        }
    }

    @Override
    public byte[] getVideo(File video)
    {

        byte[] videoInByte = null;
        try
        {
            File file = video;
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            videoInByte = multipartFile.getBytes();
        }
        catch (Exception e)
        {
            System.out.println("Error occured during Multipart file convertion");
        }
        return videoInByte;
    }

    @Override
    public NpProgram updateWithNewValue(NpProgram source, NpProgram target)
    {

        target.setProgramDescription(source.getProgramDescription());
        target.setProgramSponsor(source.getProgramSponsor());
        target.setProgramOwner(source.getProgramOwner());
        target.setProgramOverview(source.getProgramOverview());
        target.setNpClientCostCentreImpl(source.getNpClientCostCentreImpl());
        target.setProgramDoc(source.getProgramDoc());
        return target;
    }

    @Override
    public List<NpProgram> getAllClientProgramsWithVideo(NpClient client)
    {

        List<NpProgram> programs = getAllClientProgramme((NpClientImpl) client);
        List<NpProgram> _program = getProgramsVideo(programs);
        return _program;
    }

    private List<NpProgram> getProgramsVideo(
            List<NpProgram> programs)
    {

        List<NpProgram> programsWithVideo = new ArrayList<NpProgram>();

        for (NpProgram _program : programs)
        {
            NpProgram pro = new NpProgram();

            pro.setClientProgramID(_program.getClientProgramID());
            pro.setVideoUrl(getVideoUrl(_program));

            programsWithVideo.add(pro);
        }
        return programsWithVideo;
    }
}
