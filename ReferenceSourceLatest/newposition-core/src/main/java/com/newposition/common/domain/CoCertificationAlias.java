package com.newposition.common.domain;

import java.util.Date;

import com.newposition.candidate.domain.NpCertifications;

public interface CoCertificationAlias extends java.io.Serializable
{

    public NpCertifications getNpCertifications();

    public void setNpCertifications(NpCertifications npCertifications);

    public String getCertificationAliasName();

    public void setCertificationAliasName(String certificationAliasName);

    public CoSource getCoSource();

    public void setCoSource(CoSource coSource);

    public String getStatus();

    public void setStatus(String status);

    public Integer getCertificationAliasID();

    public void setCertificationAliasID(Integer certificationAliasID);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
