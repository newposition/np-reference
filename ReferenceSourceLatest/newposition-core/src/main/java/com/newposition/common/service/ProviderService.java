package com.newposition.common.service;

public interface ProviderService<E>
{
    public E get();
}
