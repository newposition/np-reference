package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "co_locations", catalog = "npdbs")
public class CoLocations
{
    @Id
    @GeneratedValue
    @Column(name = "LocationID")
    private Long locationID;

    @Column(name = "Address1")
    private String address1;

    @Column(name = "Address2")
    private String address2;

    @Column(name = "Address3")
    private String address3;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CityID")
    private CoCities coCities;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ZIP")
    private CoZip coZip;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CountryID")
    private CoCountry coCountry;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CountyID")
    private CoCounty coCounty;

    @Column(name = "LocationXML")
    private String locationXML;

    public Long getLocationID()
    {
        return locationID;
    }

    public void setLocationID(Long locationID)
    {
        this.locationID = locationID;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getAddress3()
    {
        return address3;
    }

    public void setAddress3(String address3)
    {
        this.address3 = address3;
    }

    public CoCities getCoCities()
    {
        return coCities;
    }

    public void setCoCities(CoCities coCities)
    {
        this.coCities = coCities;
    }

    public CoZip getCoZip()
    {
        return coZip;
    }

    public void setCoZip(CoZip coZip)
    {
        this.coZip = coZip;
    }

    public CoCountry getCoCountry()
    {
        return coCountry;
    }

    public void setCoCountry(CoCountry coCountry)
    {
        this.coCountry = coCountry;
    }

    public CoCounty getCoCounty()
    {
        return coCounty;
    }

    public void setCoCounty(CoCounty coCounty)
    {
        this.coCounty = coCounty;
    }

    public String getLocationXML()
    {
        return locationXML;
    }

    public void setLocationXML(String locationXML)
    {
        this.locationXML = locationXML;
    }

    @Override
    public int hashCode()
    {
        return this.address1.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((CoLocations) obj).getLocationID();
        long id2 = this.locationID;
        return (id1 == id2);
    }
}
