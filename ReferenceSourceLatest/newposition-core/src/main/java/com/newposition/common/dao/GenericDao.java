package com.newposition.common.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T, ID extends Serializable>
{

    public T save(T entity);

    public void merge(T entity);

    public void delete(T entity);

    public List<T> findAll();

    public T findByID(Integer id);
}
