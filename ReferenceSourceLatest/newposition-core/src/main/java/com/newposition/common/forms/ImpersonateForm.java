/**
 *
 */
package com.newposition.common.forms;

/**
 * @author Sachi N
 */

public class ImpersonateForm
{

    private String userId;
    private String reasonCode;
    private String reasonLines;
    private String userRole;

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getReasonCode()
    {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode)
    {
        this.reasonCode = reasonCode;
    }

    public String getReasonLines()
    {
        return reasonLines;
    }

    public void setReasonLines(String reasonLines)
    {
        this.reasonLines = reasonLines;
    }

    public String getUserRole()
    {
        return userRole;
    }

    public void setUserRole(String userRole)
    {
        this.userRole = userRole;
    }
}
