package com.newposition.common.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface NpUser extends Serializable
{

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getPrimaryEmail();

    public void setPrimaryEmail(String primaryEmail);

    public String getSecondaryEmail();

    public void setSecondaryEmail(String secondaryEmail);

    public String getThirdEmail();

    public void setThirdEmail(String thirdEmail);

    public String getPhone();

    public void setPhone(String phone);

    public String getEncryptedPassword();

    public void setEncryptedPassword(String encryptedPassword);

    public String getOfficeTelephone();

    public void setOfficeTelephone(String officeTelephone);

    public String getJobtitle();

    public void setJobtitle(String jobtitle);

    public String getCompany();

    public void setCompany(String company);

    public Set<NpUserRole> getNpUserRole();

    public void setNpUserRole(Set<NpUserRole> npUserRole);

    public List<NpRole> getNpRoles();

    public void setNpRoles(List<NpRole> npRoles);

    public Integer getId();

    public void setId(Integer id);

    public NpUserAttempts getNpUserAttempts();

    public void setNpUserAttempts(NpUserAttempts npUserAttempts);

    public NpUserStatus getNpUserStatus();

    public void setNpUserStatus(NpUserStatus npUserStatus);

    public NpUserTypes getNpUserTypes();

    public void setNpUserTypes(NpUserTypes npUserTypes);
}
