package com.newposition.common.domain;

import java.util.Date;

public interface NpReset
{

    public Integer getId();

    public void setId(Integer id);

    public String getActivation();

    public void setActivation(String activation);

    public String getEmail();

    public void setEmail(String email);

    public Date getTime();

    public void setTime(Date time);
}
