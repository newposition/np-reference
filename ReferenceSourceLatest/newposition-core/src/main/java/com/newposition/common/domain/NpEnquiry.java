package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "np_enquiry", catalog = "npdbs")
public class NpEnquiry
{

    @Id
    @GeneratedValue
    @Column(name = "ID")
    Integer id;
    @Column(name = "reason")
    String reason;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }
}