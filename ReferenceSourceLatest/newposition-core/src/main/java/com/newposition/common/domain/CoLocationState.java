package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_location_state", catalog = "npdbs")
public class CoLocationState
{

    @Id
    @Column(name = "StateID")
    private String stateID;

    @Column(name = "StateDescription")
    private String stateDescription;

    public String getStateID()
    {
        return stateID;
    }

    public void setStateID(String stateID)
    {
        this.stateID = stateID;
    }

    public String getStateDescription()
    {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription)
    {
        this.stateDescription = stateDescription;
    }

    @Override
    public int hashCode()
    {
        return this.stateDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        String str1 = ((CoLocationState) obj).getStateID();
        String str2 = this.stateID;
        return str1.equals(str2);
    }
}
