package com.newposition.common.dao;

import java.util.List;

import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.forms.NewPaswordForm;

public interface NpUserDao extends GenericDao<NpUser, Long>
{

    public NpUser findByUserName(String userName);

    public void setActivationCode(NpReset npReset);

    public NpUserStatus getNpUserStatusById(Integer id);

    public boolean setnewpassword(NewPaswordForm newPaswordForm);

    public NpUser updateFailureAttempts(String userName);

    public boolean unlockAccount(Integer userId);

    public void updateNpUser(NpUser npUser);

    public void updateNpCandidateRole(NpUserRole npUserRole);

    public NpReset getActivationCode(String email);

    public NpReset resetPassword(NpReset resetCode);

    public NpReset verifyActivationCode(String activationCode);

    public List<NpUserStatus> fetchAllUserStatus();

    public NpUser updateSuccessAttempts(String userName);

    public void activateUserStatus(String decoded);

    public NpRole findRoleByRoleName(String roleName);
}
