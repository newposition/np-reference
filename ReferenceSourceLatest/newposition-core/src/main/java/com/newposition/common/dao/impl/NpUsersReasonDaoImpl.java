package com.newposition.common.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.common.dao.NpUsersReasonDao;
import com.newposition.common.domain.NpUsersReason;

public class NpUsersReasonDaoImpl implements NpUsersReasonDao
{

    protected static final Logger LOG = Logger.getLogger(NpUsersReasonDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpUsersReason> getReasonsForUser()
    {
        return (List<NpUsersReason>) getHibernateTemplate().find("from NpUsersReason where userType='user'");
    }

    @Override
    public NpUsersReason getReasonForId(int id)
    {
        return (NpUsersReason) getHibernateTemplate().find("from NpUsersReason where id=?", id).get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpUsersReason> getReasonsForClient()
    {
        return (List<NpUsersReason>) getHibernateTemplate().find("from NpUsersReason where userType='client'");
    }
}
