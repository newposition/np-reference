package com.newposition.common.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_cities", catalog = "npdbs")
public class CoCities
{

    @Id
    @GeneratedValue
    @Column(name = "CityID")
    private Long cityID;

    @Column(name = "CityDescription")
    private String cityDescription;

    public Long getCityID()
    {
        return cityID;
    }

    public void setCityID(Long cityID)
    {
        this.cityID = cityID;
    }

    public String getCityDescription()
    {
        return cityDescription;
    }

    public void setCityDescription(String cityDescription)
    {
        this.cityDescription = cityDescription;
    }

    @Override
    public int hashCode()
    {
        return this.cityDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((CoCities) obj).getCityID();
        long id2 = this.cityID;
        return (id1 == id2);
    }
}
