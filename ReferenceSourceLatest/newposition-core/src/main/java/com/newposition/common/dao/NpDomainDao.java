package com.newposition.common.dao;

import com.newposition.common.domain.NpDomain;

public interface NpDomainDao extends GenericDao<NpDomain, Long>
{

}
