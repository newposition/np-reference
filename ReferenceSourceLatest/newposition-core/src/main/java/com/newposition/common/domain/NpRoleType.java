package com.newposition.common.domain;

public enum NpRoleType
{
    ROLE_ANONYMOUS, ROLE_ADMIN, ROLE_CANDIDATE, ROLE_CLIENT_ADMINISTRATOR
}
