package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "np_users", catalog = "npdbs")
public class NpUserImpl implements NpUser
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "FirstName", nullable = false, length = 20)
    private String firstName;

    @Column(name = "LastName", nullable = false, length = 20)
    private String lastName;

    @Column(name = "PrimaryEmailAddress", nullable = false, length = 50)
    private String primaryEmail;

    @Column(name = "SecondaryEmailAddress", length = 50)
    private String secondaryEmail;

    @Column(name = "ThirdEmailAddress", length = 50)
    private String thirdEmail;

    @Column(name = "Phone", length = 15)
    private String phone;

    @Column(name = "EncryptedPassword", length = 150)
    private String encryptedPassword;

    @ManyToMany(targetEntity = NpRoleImpl.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "np_user_roles", joinColumns = @JoinColumn(name = "UserID"), inverseJoinColumns = @JoinColumn(name = "RoleID"))
    private List<NpRole> npRoles;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "UserAttemptsID", unique = true)
    private NpUserAttemptsImpl npUserAttempts;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UserStatusID")
    private NpUserStatusImpl npUserStatus;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "npUser")
    private Set<NpUserRole> npUserRole = new HashSet<NpUserRole>(0);

    @Column(name = "officeTelephone")
    private String officeTelephone;

    @Column(name = "company")
    private String company;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "UserTypeID")
    private NpUserTypes npUserTypes;

    @Column(name = "Jobtitle")
    private String jobtitle;

    @Override
    public NpUserTypes getNpUserTypes()
    {
        return this.npUserTypes;
    }

    @Override
    public void setNpUserTypes(NpUserTypes npUserTypes)
    {
        this.npUserTypes = npUserTypes;
    }

    @Override
    public String getOfficeTelephone()
    {
        return this.officeTelephone;
    }

    @Override
    public void setOfficeTelephone(String officeTelephone)
    {
        this.officeTelephone = officeTelephone;
    }

    @Override
    public String getCompany()
    {
        return company;
    }

    @Override
    public void setCompany(String company)
    {
        this.company = company;
    }

    @Override
    public Set<NpUserRole> getNpUserRole()
    {
        return this.npUserRole;
    }

    @Override
    public void setNpUserRole(Set<NpUserRole> npUserRole)
    {
        this.npUserRole = npUserRole;
    }

    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public String getFirstName()
    {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String getLastName()
    {
        return lastName;
    }

    @Override
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @Override
    public String getPrimaryEmail()
    {
        return primaryEmail;
    }

    @Override
    public void setPrimaryEmail(String primaryEmail)
    {
        this.primaryEmail = primaryEmail;
    }

    @Override
    public String getSecondaryEmail()
    {
        return secondaryEmail;
    }

    @Override
    public void setSecondaryEmail(String secondaryEmail)
    {
        this.secondaryEmail = secondaryEmail;
    }

    @Override
    public String getThirdEmail()
    {
        return thirdEmail;
    }

    @Override
    public void setThirdEmail(String thirdEmail)
    {
        this.thirdEmail = thirdEmail;
    }

    @Override
    public String getPhone()
    {
        return phone;
    }

    @Override
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Override
    public String getEncryptedPassword()
    {
        return encryptedPassword;
    }

    @Override
    public void setEncryptedPassword(String encryptedPassword)
    {
        this.encryptedPassword = encryptedPassword;
    }

    @Override
    public List<NpRole> getNpRoles()
    {
        return npRoles;
    }

    @Override
    public void setNpRoles(List<NpRole> npRoles)
    {
        this.npRoles = npRoles;
    }

    @Override
    public NpUserStatus getNpUserStatus()
    {
        return this.npUserStatus;
    }

    @Override
    public void setNpUserStatus(NpUserStatus npUserStatus)
    {
        this.npUserStatus = (NpUserStatusImpl) npUserStatus;
    }

    @Override
    public NpUserAttempts getNpUserAttempts()
    {
        // TODO Auto-generated method stub
        return this.npUserAttempts;
    }

    @Override
    public void setNpUserAttempts(NpUserAttempts npUserAttempts)
    {
        this.npUserAttempts = (NpUserAttemptsImpl) npUserAttempts;
    }

    @Override
    public String getJobtitle()
    {
        // TODO Auto-generated method stub
        return this.jobtitle;
    }

    @Override
    public void setJobtitle(String jobtitle)
    {
        // TODO Auto-generated method stub
        this.jobtitle = jobtitle;
    }
}
