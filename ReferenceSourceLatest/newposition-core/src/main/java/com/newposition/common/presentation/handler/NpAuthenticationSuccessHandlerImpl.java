package com.newposition.common.presentation.handler;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Component("npAuthenticationSuccessHandler")
public class NpAuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler
{

    protected static final Logger LOG = Logger.getLogger(NpAuthenticationSuccessHandlerImpl.class);

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private NpUserService npUserService;

    private String invalidcredentials;

    public void setNpUserService(NpUserService npUserService)
    {
        this.npUserService = npUserService;
    }

    public void setInvalidcredentials(String invalidcredentials)
    {
        this.invalidcredentials = invalidcredentials;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException
    {
        handle(request, response, authentication);

        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException
    {

        String targetUrl = "";

        String userName = authentication.getName();
        NpUser user = npUserService.findByUserName(userName);

        if (user.getNpUserStatus().getId() == 1)
        {
            npUserService.updateSuccessAttempts(userName);
            request.getSession().setAttribute("user", user);
            request.getSession().setAttribute("lock", false);
            request.getSession().setAttribute("errmsg", "");
            targetUrl = determineTargetUrl(authentication);
        }
        else if (user.getNpUserStatus().getId() == 2)
        {
            LOG.warn("User is currently disabled ");
            targetUrl = "/home";
            request.getSession().setAttribute("errmsg", "");
            request.getSession().setAttribute("recentEnterEmail", userName);
            request.getSession().setAttribute("check", "disabled");
            //		request.getSession().setAttribute("lock", true);
        }
        else if (user.getNpUserStatus().getId() == 4)
        {
            LOG.warn("User is currently locked ");
            targetUrl = "/home";
            request.getSession().setAttribute("errmsg", "");
            request.getSession().setAttribute("recentEnterEmail", userName);
            request.getSession().setAttribute("check", "locked");
            //		request.getSession().setAttribute("lock", true);
        }
        else
        {
            LOG.warn("User is currently deleted ");
            request.getSession().setAttribute("errmsg", invalidcredentials);
            request.getSession().setAttribute("lock", false);
            targetUrl = "/home";
        }

        if (response.isCommitted())
        {
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    /**
     * Builds the target URL according to the logic defined in the main class
     * Javadoc.
     */
    protected String determineTargetUrl(Authentication authentication)
    {

        boolean isClient = false;
        boolean isAdmin = false;
        boolean isCandidate = false;
        boolean isClientSupport = false;

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        for (GrantedAuthority grantedAuthority : authorities)
        {

            if (grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_NP_USERSUPPORT") || grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_SKILLS_ANALYST") || grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_CLIENT_LIASON"))
            {
                isAdmin = true;
                break;
            }
            else if (grantedAuthority.getAuthority().equals("ROLE_CANDIDATE") || grantedAuthority.getAuthority().equals("REGISTERED_USER"))
            {
                isCandidate = true;
                break;
            }
            else if (grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_ADMINISTRATOR") ||
                    grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_INTERVIEWER") ||
                    grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_MANAGER") ||
                    grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_EMPLOYEE") ||
                    grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_SUPPORT"))
            {
                isClient = true;
                break;
            }
            if (grantedAuthority.getAuthority().startsWith("ROLE_NP_USERSUPPORT") || grantedAuthority.getAuthority().startsWith("ROLE_SKILLS_ANALYST") || grantedAuthority.getAuthority().startsWith("ROLE_CLIENT_LIASON"))
            {
                isClientSupport = true;
            }
        }

        if (isAdmin)
        {
            return "/adminClientLiason";
        }
        else if (isCandidate)
        {
            return "/basicDashboard";
        }
        else if (isClient)
        {
            return "/clientDashboard";
        }
        else if (isClientSupport)
        {
            return "/adminClientLiason";
        }
        else
        {
            throw new IllegalStateException("User do not have any valid role");
        }
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request)
    {
        HttpSession session = request.getSession(false);
        if (session == null)
        {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy)
    {
        this.redirectStrategy = redirectStrategy;
    }

    protected RedirectStrategy getRedirectStrategy()
    {
        return redirectStrategy;
    }
}
