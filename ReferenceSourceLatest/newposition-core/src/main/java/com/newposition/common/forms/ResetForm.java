package com.newposition.common.forms;

import javax.validation.constraints.Pattern;

public class ResetForm
{

    @Pattern(regexp = "^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\\.[a-zA-Z]{2,}$", message = "{email.format}")
    private String resetMail;

    public String getResetMail()
    {
        return resetMail;
    }

    public void setResetMail(String resetMail)
    {
        this.resetMail = resetMail;
    }
}
