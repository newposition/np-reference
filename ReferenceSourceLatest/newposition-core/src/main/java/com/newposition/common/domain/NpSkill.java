package com.newposition.common.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.candidate.domain.NpCandidateSkills;

public interface NpSkill
{

    public Integer getId();

    public void setId(Integer id);

    public NpDomainImpl getNpDomain();

    public void setNpDomainTypes(NpDomainImpl npDomain);

    public String getSkillName();

    public void setSkillName(String skillName);

    public String getSkillType();

    public void setSkillType(String skillType);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public String getValid();

    public void setValid(String valid);

    boolean isStatus();

    void setStatus(boolean status);

    Set<CoSkillAlias> getCoSkillAlias();

    void setCoSkillAlias(Set<CoSkillAlias> coSkillAlias);

    void setNpCandidateSkillses(Set<NpCandidateSkills> npCandidateSkillses);

    Set<NpCandidateSkills> getNpCandidateSkillses();
}
