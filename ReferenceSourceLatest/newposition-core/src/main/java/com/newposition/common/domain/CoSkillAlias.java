package com.newposition.common.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * CoSkillAlias generated by Trinadh
 */
@Entity
@Table(name = "co_skill_alias", catalog = "npdbs", uniqueConstraints = @UniqueConstraint(columnNames = "SkillAliasID"))
public class CoSkillAlias implements java.io.Serializable
{

    private static final long serialVersionUID = 1L;

    private NpSkillImpl npSkillImpl;
    private String skillAliasName;
    private CoSource coSource;
    private String status;
    private Integer skillAliasID;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SkillID", unique = true)
    public NpSkillImpl getNpSkillImpl()
    {
        return npSkillImpl;
    }

    public void setNpSkillImpl(NpSkillImpl npSkillImpl)
    {
        this.npSkillImpl = npSkillImpl;
    }

    @Column(name = "SkillAliasName")
    public String getSkillAliasName()
    {
        return skillAliasName;
    }

    public void setSkillAliasName(String skillAliasName)
    {
        this.skillAliasName = skillAliasName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SourceID", unique = true)
    public CoSource getCoSource()
    {
        return coSource;
    }

    public void setCoSource(CoSource coSource)
    {
        this.coSource = coSource;
    }

    @Column(name = "Status")
    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "SkillAliasID", unique = true, nullable = false)
    public Integer getSkillAliasID()
    {
        return skillAliasID;
    }

    public void setSkillAliasID(Integer skillAliasID)
    {
        this.skillAliasID = skillAliasID;
    }

    @Column(name = "CreatedBy")
    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Column(name = "CreatedDate")
    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Column(name = "ModifiedBy")
    public String getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "ModifiedDate")
    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
