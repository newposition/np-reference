package com.newposition.common.service.impl;

import java.util.List;

import com.newposition.common.dao.NpEnquiryReasonDao;
import com.newposition.common.domain.NpEnquiry;
import com.newposition.common.service.NpEnquiryReasonService;

public class NpEnquiryReasonServiceImpl implements NpEnquiryReasonService
{

    private NpEnquiryReasonDao npEnquiryReasonDao;

    public void setNpEnquiryReasonDao(NpEnquiryReasonDao npEnquiryReasonDao)
    {
        this.npEnquiryReasonDao = npEnquiryReasonDao;
    }

    @Override
    public List<NpEnquiry> getReasonsForEnquiry()
    {
        // TODO Auto-generated method stub
        return npEnquiryReasonDao.getReasonsForEnquiry();
    }
}
