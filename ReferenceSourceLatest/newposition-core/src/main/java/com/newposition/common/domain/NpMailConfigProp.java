package com.newposition.common.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "np_mail_config", catalog = "npdbs")
public class NpMailConfigProp
{

    @Id
    @GeneratedValue
    private Integer mailConfigId;
    private String npMailUserName;
    private String npMailPassword;

    @OneToOne
    @JoinColumn(name = "Mailprop_id")
    private JavaMailProperties javaMailProperties;

    public Integer getMailConfigId()
    {
        return mailConfigId;
    }

    public void setMailConfigId(Integer mailConfigId)
    {
        this.mailConfigId = mailConfigId;
    }

    public String getNpMailUserName()
    {
        return npMailUserName;
    }

    public void setNpMailUserName(String npMailUserName)
    {
        this.npMailUserName = npMailUserName;
    }

    public String getNpMailPassword()
    {
        return npMailPassword;
    }

    public void setNpMailPassword(String npMailPassword)
    {
        this.npMailPassword = npMailPassword;
    }

    public JavaMailProperties getJavaMailProperties()
    {
        return javaMailProperties;
    }

    public void setJavaMailProperties(JavaMailProperties javaMailProperties)
    {
        this.javaMailProperties = javaMailProperties;
    }
}