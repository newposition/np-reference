package com.newposition.common.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.newposition.common.dao.NpSectorDao;
import com.newposition.common.domain.NpSector;
import com.newposition.common.domain.NpSectorImpl;

public class NpSectorDaoImpl extends GenericDaoImpl<NpSector, Long> implements NpSectorDao
{

	 /* private HibernateTemplate hibernateTemplate;

   public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
	this.hibernateTemplate = hibernateTemplate;
    }*/

    @Override
    public NpSector findSectorByName(String name)
    {
        @SuppressWarnings("unchecked")
        List<NpSector> sectors = (List<NpSector>) getHibernateTemplate().findByNamedParam(
                "from NpSectorImpl S WHERE S.sectorName=:sectorName", "sectorName", name);
        if (sectors != null && sectors.size() > 0)
        {
            return sectors.get(0);
        }
        return null;
    }

    @Override
    public List<NpSector> findAll()
    {
        return getHibernateTemplate().loadAll(NpSector.class);
    }

    @Override
    public NpSector findByID(Integer id)
    {
        return getHibernateTemplate().get(NpSectorImpl.class, id);
    }
}
