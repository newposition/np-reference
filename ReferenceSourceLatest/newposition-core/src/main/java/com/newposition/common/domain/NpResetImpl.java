package com.newposition.common.domain;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "np_reset", catalog = "npdbs")
public class NpResetImpl implements NpReset
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "activation")
    private String activation;

    @Column(name = "email", length = 45)
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "time", length = 19)
    private Date time;

    @Override
    public Integer getId()
    {
        // TODO Auto-generated method stub
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        // TODO Auto-generated method stub
        this.id = id;
    }

    @Override
    public String getActivation()
    {
        // TODO Auto-generated method stub
        return activation;
    }

    @Override
    public void setActivation(String activation)
    {
        // TODO Auto-generated method stub
        this.activation = activation;
    }

    @Override
    public String getEmail()
    {
        // TODO Auto-generated method stub
        return email;
    }

    @Override
    public void setEmail(String email)
    {
        // TODO Auto-generated method stub
        this.email = email;
    }

    @Override
    public Date getTime()
    {
        return this.time;
    }

    @Override
    public void setTime(Date time)
    {
        this.time = time;
    }
}
