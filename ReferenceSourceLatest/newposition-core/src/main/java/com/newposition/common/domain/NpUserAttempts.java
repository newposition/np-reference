package com.newposition.common.domain;

import java.util.Date;

public interface NpUserAttempts
{

    public Integer getId();

    public void setId(Integer id);

    public Integer getAttempts();

    public void setAttempts(Integer attempts);

    public Date getLastModified();

    public void setLastModified(Date lastModified);

    public NpUserImpl getNpUser();

    public void setNpUser(NpUserImpl npUser);
}
