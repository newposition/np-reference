package com.newposition.common.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.candidate.domain.NpCandidateEmployerImpl;

public interface NpEmployer
{

    public Integer getId();

    public void setId(Integer id);

    public NpSector getNpSector();

    public void setNpSector(NpSector npSector);

    public String getName();

    public void setName(String name);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public String getValid();

    public void setValid(String valid);

    public String getIndustryType();

    public void setIndustryType(String industryType);

    public boolean isStatus();

    public void setStatus(boolean status);

    public Set<NpCandidateEmployerImpl> getNpCandidateEmployers();

    public void setNpCandidateEmployers(Set<NpCandidateEmployerImpl> npCandidateEmployers);
}
