package com.newposition.common.service.impl;

import java.util.List;

import com.newposition.common.dao.NpUsersReasonDao;
import com.newposition.common.domain.NpUsersReason;
import com.newposition.common.service.NpUsersReasonService;

public class NpUsersReasonServiceImpl implements NpUsersReasonService
{

    private NpUsersReasonDao npUsersReasonDao;

    public NpUsersReasonDao getNpUsersReasonDao()
    {
        return npUsersReasonDao;
    }

    public void setNpUsersReasonDao(NpUsersReasonDao npUsersReasonDao)
    {
        this.npUsersReasonDao = npUsersReasonDao;
    }

    @Override
    public List<NpUsersReason> getReasonsForUser()
    {
        return npUsersReasonDao.getReasonsForUser();
    }

    @Override
    public NpUsersReason getReasonForId(int id)
    {
        return npUsersReasonDao.getReasonForId(id);
    }

    @Override
    public List<NpUsersReason> getReasonsForClient()
    {
        return npUsersReasonDao.getReasonsForClient();
    }
}
