package com.newposition.common.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.common.dao.NpDomainDao;
import com.newposition.common.domain.NpDomain;

public class NpDomainDaoImpl implements NpDomainDao
{

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public NpDomain save(NpDomain npDomain)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void merge(NpDomain npDomain)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(NpDomain npDomain)
    {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpDomain> findAll()
    {
        // TODO Auto-generated method stub
        return (List<NpDomain>) hibernateTemplate.find("from NpDomainImpl ");
    }

    @Override
    public NpDomain findByID(Integer id)
    {

        return null;
    }
}
