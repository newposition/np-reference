package com.newposition.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_county", catalog = "npdbs")
public class CoCounty
{

    @Id
    @GeneratedValue
    @Column(name = "CountyID")
    private Long countyID;
    @Column(name = "CountyDescription")
    private String countyDescription;
    @Column(name = "CreatedBy")
    private String createdBy;
    @Column(name = "CreatedDate")
    private Date createdDate;
    @Column(name = "ModifiedBy")
    private String modifiedBy;
    @Column(name = "ModifiedDate")
    private Date modifiedDate;

    public Long getCountyID()
    {
        return countyID;
    }

    public void setCountyID(Long countyID)
    {
        this.countyID = countyID;
    }

    public String getCountyDescription()
    {
        return countyDescription;
    }

    public void setCountyDescription(String countyDescription)
    {
        this.countyDescription = countyDescription;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public int hashCode()
    {
        return this.countyDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((CoCounty) obj).getCountyID();
        long id2 = this.countyID;
        return (id1 == id2);
    }
}
