package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "np_user_attempts", catalog = "npdbs")
public class NpUserAttemptsImpl implements NpUserAttempts
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "attempts")
    private Integer attempts;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastModified", length = 19)
    private Date lastModified;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "npUserAttempts")
    private NpUserImpl npUser;

    @Override
    public Integer getId()
    {
        return this.id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public Integer getAttempts()
    {
        return this.attempts;
    }

    @Override
    public void setAttempts(Integer attempts)
    {
        this.attempts = attempts;
    }

    @Override
    public Date getLastModified()
    {
        return this.lastModified;
    }

    @Override
    public void setLastModified(Date lastModified)
    {
        this.lastModified = lastModified;
    }

    @Override
    public NpUserImpl getNpUser()
    {
        return npUser;
    }

    @Override
    public void setNpUser(NpUserImpl npUser)
    {
        this.npUser = npUser;
    }
}
