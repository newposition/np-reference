package com.newposition.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "co_country", catalog = "npdbs")
public class CoCountry
{

    @Id
    @GeneratedValue
    @Column(name = "CountryID")
    private Long countryID;
    @Column(name = "CountryDescription")
    private String countryDescription;
    @Column(name = "CreatedBy")
    private String createdBy;
    @Column(name = "CreatedDate")
    private Date createdDate;
    @Column(name = "ModifiedBy")
    private String modifiedBy;
    @Column(name = "ModifiedDate")
    private Date modifiedDate;

    public Long getCountryID()
    {
        return countryID;
    }

    public void setCountryID(Long countryID)
    {
        this.countryID = countryID;
    }

    public String getCountryDescription()
    {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription)
    {
        this.countryDescription = countryDescription;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public int hashCode()
    {
        return this.countryDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((CoCountry) obj).getCountryID();
        long id2 = this.countryID;
        return (id1 == id2);
    }
}
