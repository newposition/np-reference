package com.newposition.common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.context.Context;

import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpResetImpl;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.ChangePaswordForm;
import com.newposition.common.forms.NewPaswordForm;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;

public class NpUserServiceImpl implements NpUserService
{

    private NpUserDao npUserDao;

    private NpMailService npMailService;

    private PasswordEncoder passwordEncoder;

    public void setPasswordEncoder(PasswordEncoder passwordEncoder)
    {
        this.passwordEncoder = passwordEncoder;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    public void setNpUserDao(NpUserDao npUserDao)
    {
        this.npUserDao = npUserDao;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
    {
        NpUser npUser = npUserDao.findByUserName(userName);

        return getUserDetailsFromNpUser(npUser);
    }

    private UserDetails getUserDetailsFromNpUser(NpUser npUser)
    {
        if (npUser != null)
        {
            List<GrantedAuthority> authorities = new ArrayList<>();

            for (NpRole npRole : npUser.getNpRoles())
            {
                authorities.add(new SimpleGrantedAuthority(npRole.getRoleName()));
            }

            return new User(npUser.getPrimaryEmail(), npUser.getEncryptedPassword(), authorities);
        }
        return null;
    }

    @Override
    @Transactional
    public void setActivationCode(NpReset npReset)
    {
        npUserDao.setActivationCode(npReset);
    }

    @Override
    public NpUser findByUserName(String userName)
    {
        return npUserDao.findByUserName(userName);
    }

    @Override
    @Transactional
    public NpUser updateFailureAttempts(String userName)
    {
//		NpUserfindByUserName(userName);
        return npUserDao.updateFailureAttempts(userName);
    }

    @Transactional
    @Override
    public boolean setnewpassword(NewPaswordForm newPaswordForm)
    {
        // TODO Auto-generated method stub
//		NpUser user = npUserDao.findByUserName(newPaswordForm.getEmail());
        newPaswordForm.setNewPassword(passwordEncoder.encode(newPaswordForm.getNewPassword()));
//		if (user.getEncryptedPassword().equals(passwordEncoder.encode(newPaswordForm.getOldPassword()))) {

        return npUserDao.setnewpassword(newPaswordForm);
//		}

//		return false;
    }

    @Transactional
    @Override
    public void activateUserStatus(String decoded)
    {
        // TODO Auto-generated method stub
        npUserDao.activateUserStatus(decoded);
    }

    @Override
    public void resendUnockAccountLink(String userName, String unlockAccountLink, String imageContext)
    {
        // TODO Auto-generated method stub
        NpUser user = npUserDao.findByUserName(userName);

        Long currentTime = System.currentTimeMillis();
        String currentTimeString = "";
        currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());

        String userId = new String(DatatypeConverter.printBase64Binary(user.getId().toString().getBytes()));
        String information = "Your Account Lock";
        Context context = new Context();
        context.setVariable("unlockAccountLink", unlockAccountLink + userId + "&dur=" + currentTimeString);
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("template", "unlock-email.html");
        // send email
        npMailService.sendMail(information, user.getPrimaryEmail(), context);
    }

    @Transactional
    @Override
    public boolean unlockAccount(Integer userId)
    {

        return npUserDao.unlockAccount(userId);
    }

    @Override
    @Transactional
    public void updateNpUser(NpUser npUser)
    {
        npUserDao.updateNpUser(npUser);
    }

    @Override
    public NpReset getActivationCode(String email)
    {

        return npUserDao.getActivationCode(email);
    }

    @Transactional
    @Override
    public NpReset resetPassword(String email)
    {
        Random rand = new Random();
        Integer number = rand.nextInt(1000000);

        NpReset resetCode = new NpResetImpl();
        resetCode.setEmail(email);
        resetCode.setActivation(number.toString());

        return npUserDao.resetPassword(resetCode);
    }

    @Override
    public NpReset verifyActivationCode(String activationCode)
    {
        return npUserDao.verifyActivationCode(activationCode);
    }

    @Transactional
    @Override
    public boolean changePassword(ChangePaswordForm changePasword)
    {

//		NpUser user = npUserDao.findByUserName(changePasword.getEmail());
        changePasword.setNewPassword(passwordEncoder.encode(changePasword.getNewPassword()));
//		System.out.println(passwordEncoder.matches(changePasword.getOldPassword().trim(), changePasword.getUser().getEncryptedPassword()));
        if (passwordEncoder.matches(changePasword.getOldPassword().trim(), changePasword.getUser().getEncryptedPassword()))
        {
//		System.out.println("returning true "+changePasword.getUser().getFirstName());
            NewPaswordForm newPassword = new NewPaswordForm();
            newPassword.setEmail(changePasword.getUser().getPrimaryEmail());
            newPassword.setNewPassword(changePasword.getNewPassword());
            return npUserDao.setnewpassword(newPassword);
        }
//		System.out.println("false");
        return false;
    }

    @Override
    @Transactional
    public NpUser updateSuccessAttempts(String userName)
    {
        //  NpUserfindByUserName(userName);
        return npUserDao.updateSuccessAttempts(userName);
    }

    @Override
    public NpUser findUserById(Integer id)
    {

        return npUserDao.findByID(id);
    }

    @Override
    public boolean isLinkExpired(Long time)
    {

//		NpUser user = npUserDao.findByUserName(username);
        Long recieveTime = System.currentTimeMillis();
        Long duration = (recieveTime - time) / 1000L / 60L;
        if (duration > 48)
        {
            return true;
        }

        return false;
    }
}
