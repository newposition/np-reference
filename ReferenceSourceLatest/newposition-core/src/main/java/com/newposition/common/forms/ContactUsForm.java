/**
 *
 */
package com.newposition.common.forms;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Vijay Nalla
 */

public class ContactUsForm
{

    @NotEmpty(message = "{firstName.required}")
    public String firstName;

    @NotEmpty(message = "{lastName.required}")
    public String lastName;

    public String telePhone;

    public String company;

    @Pattern(regexp = "[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}", message = "{email.format}")
    public String primaryEmailAddress;

    public String reasonForEnquiry;

    public String enquiry;

    public String getReasonForEnquiry()
    {
        return reasonForEnquiry;
    }

    public void setReasonForEnquiry(String reasonForEnquiry)
    {
        this.reasonForEnquiry = reasonForEnquiry;
    }

    public String getEnquiry()
    {
        return enquiry;
    }

    public void setEnquiry(String enquiry)
    {
        this.enquiry = enquiry;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getTelePhone()
    {
        return telePhone;
    }

    public void setTelePhone(String telePhone)
    {
        this.telePhone = telePhone;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getPrimaryEmailAddress()
    {
        return this.primaryEmailAddress;
    }

    public void setPrimaryEmailAddress(String primaryEmailAddress)
    {
        this.primaryEmailAddress = primaryEmailAddress;
    }
}
