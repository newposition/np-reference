package com.newposition.common.domain;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.newposition.client.domain.NpClientCostCentreDocument;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.client.domain.NpClientProject;

@Entity
@Table(name = "cl_programs", catalog = "npdbs")
public class NpProgram implements java.io.Serializable, Comparable<NpProgram>
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    @Column(name = "ClientProgramID")
    private Long clientProgramID;

    @OneToOne
    @JoinColumn(name = "ClientProgramID")
    private NpClientProgramDocument programDoc;

    public NpClientProgramDocument getProgramDoc()
    {
        return programDoc;
    }

    public void setProgramDoc(NpClientProgramDocument programDoc)
    {
        this.programDoc = programDoc;
    }

    @Column(name = "ProgramCode")
    private String programCode;
    @Column(name = "ProgramDescription")
    private String programDescription;
    @Column(name = "ProgramOwner")
    private String programOwner;
    @Column(name = "ProgramSponsor")
    private String programSponsor;
    @Column(name = "ProgramOverview")
    private String programOverview;

    @Transient
    private String videoUrl;

    public String getVideoUrl()
    {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl)
    {
        this.videoUrl = videoUrl;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private NpClientImpl npClient;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CostCentreID")
    private NpClientCostCentreImpl npClientCostCentreImpl;

    public Long getClientProgramID()
    {
        return clientProgramID;
    }

    public void setClientProgramID(Long clientProgramID)
    {
        this.clientProgramID = clientProgramID;
    }

    public String getProgramDescription()
    {
        return programDescription;
    }

    public void setProgramDescription(String programDescription)
    {
        this.programDescription = programDescription;
    }

    public String getProgramOwner()
    {
        return programOwner;
    }

    public void setProgramOwner(String programOwner)
    {
        this.programOwner = programOwner;
    }

    public String getProgramSponsor()
    {
        return programSponsor;
    }

    public void setProgramSponsor(String programSponsor)
    {
        this.programSponsor = programSponsor;
    }

    public String getProgramOverview()
    {
        return programOverview;
    }

    public void setProgramOverview(String programOverview)
    {
        this.programOverview = programOverview;
    }

    public NpClientImpl getNpClient()
    {
        return npClient;
    }

    public void setNpClient(NpClientImpl npClient)
    {
        this.npClient = npClient;
    }

    public String getProgramCode()
    {
        return programCode;
    }

    public void setProgramCode(String programCode)
    {
        this.programCode = programCode;
    }

    public NpClientCostCentreImpl getNpClientCostCentreImpl()
    {
        return npClientCostCentreImpl;
    }

    public void setNpClientCostCentreImpl(
            NpClientCostCentreImpl npClientCostCentreImpl)
    {
        this.npClientCostCentreImpl = npClientCostCentreImpl;
    }

    @Override
    public int compareTo(NpProgram o)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    @JsonIgnore
    public static void sortByProgramCode(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByProgramCode = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getProgramCode().compareTo(npProgram2.getProgramCode());
            }
        };
        Collections.sort(projectlist, sortByProgramCode);
    }

    @JsonIgnore
    public static void sortByProgramSponsor(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByProgramSponsor = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getProgramSponsor().compareTo(npProgram2.getProgramSponsor());
            }
        };
        Collections.sort(projectlist, sortByProgramSponsor);
    }

    @JsonIgnore
    public static void sortByProgramDescription(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByProgramDescription = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getProgramDescription().compareTo(npProgram2.getProgramDescription());
            }
        };
        Collections.sort(projectlist, sortByProgramDescription);
    }

    @JsonIgnore
    public static void sortByProgramOwner(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByProgramOwner = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getProgramOwner().compareTo(npProgram2.getProgramOwner());
            }
        };
        Collections.sort(projectlist, sortByProgramOwner);
    }

    @JsonIgnore
    public static void sortByClientCostCentreCode(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByClientCostCentreCode = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getNpClientCostCentreImpl().getCostCentreCode().compareTo(npProgram2.getNpClientCostCentreImpl().getCostCentreCode());
            }
        };
        Collections.sort(projectlist, sortByClientCostCentreCode);
    }

    @JsonIgnore
    public static void sortByClientCostCentreName(List<NpProgram> projectlist)
    {

        Comparator<NpProgram> sortByClientCostCentreName = new Comparator<NpProgram>()
        {
            @Override
            public int compare(NpProgram npProgram1, NpProgram npProgram2)
            {

                return npProgram1.getNpClientCostCentreImpl().getCostCentreName().compareTo(npProgram2.getNpClientCostCentreImpl().getCostCentreName());
            }
        };
        Collections.sort(projectlist, sortByClientCostCentreName);
    }
}
