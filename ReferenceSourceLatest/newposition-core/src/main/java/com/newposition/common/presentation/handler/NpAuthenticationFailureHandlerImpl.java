package com.newposition.common.presentation.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.util.StringUtils;
import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.LoginForm;
import com.newposition.common.service.NpUserService;
import com.newposition.infra.mail.NpMailService;

public class NpAuthenticationFailureHandlerImpl implements AuthenticationFailureHandler
{

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private NpUserService npUserService;

    private String usernamevalidation;

    private String passwordvalidation;

    private String invalidcredentials;

    private NpMailService npMailService;

    private String imageContext;

    private String loginPageLink;

    public void setLoginPageLink(String loginPageLink)
    {
        this.loginPageLink = loginPageLink;
    }

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    public void setInvalidcredentials(String invalidcredentials)
    {
        this.invalidcredentials = invalidcredentials;
    }

    public void setUsernamevalidation(String usernamevalidation)
    {
        this.usernamevalidation = usernamevalidation;
    }

    public void setPasswordvalidation(String passwordvalidation)
    {
        this.passwordvalidation = passwordvalidation;
    }

    public void setNpUserService(NpUserService npUserService)
    {
        this.npUserService = npUserService;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException
    {
        // TODO Auto-generated method stub

        request.getSession().setAttribute("check", "check");
        String userName = request.getParameter("j_username");
        String password = request.getParameter("j_password");
        boolean isEmpty = false;
        if (StringUtils.isEmpty(userName))
        {
            isEmpty = true;
            request.getSession().setAttribute("errmsgUsn", usernamevalidation);
        }
        else
        {
            request.getSession().setAttribute("errmsgUsn", "");
        }
        if (StringUtils.isEmpty(password))
        {
            isEmpty = true;
            request.getSession().setAttribute("errmsgPwd", passwordvalidation);
        }
        else
        {
            request.getSession().setAttribute("errmsgPwd", "");
        }
        if (isEmpty)
        {
            request.getSession().setAttribute("successmsg", "");
            request.getSession().setAttribute("errmsg", "");
            LoginForm loginForm = new LoginForm();
            request.setAttribute("loginForm", loginForm);
            redirectStrategy.sendRedirect(request, response, "/home");
        }
        else
        {
            NpUser user = null;
            user = npUserService.updateFailureAttempts(userName);
            if (user != null)
            {
                if (user.getNpUserAttempts().getAttempts() == 3)
                {

                    /**
                     *
                     * sending current time to check expire link duration
                     *
                     */
                    Long currentTime = System.currentTimeMillis();
                    String currentTimeString = "";
                    currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());

                    String userId = new String(DatatypeConverter.printBase64Binary(user.getId().toString().getBytes()));
                    String candidateName = user.getFirstName() + " " + user.getLastName();
                    String information = "NewPosition Account Lock";
                    Context context = new Context();
                    context.setVariable("candidateName", candidateName);
                    context.setVariable("loginPageLink", loginPageLink + "?id=" + userId + "&dur=" + currentTimeString);
                    context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
                    context.setVariable("template", "user-account-lock-email.html");

                    npMailService.sendMail(information, user.getPrimaryEmail(), context);
                }
            }
            if (!response.isCommitted())
            {
                request.getSession().setAttribute("successmsg", "");
//	            request.getSession().setAttribute("errmsgUsn", "");
//	            request.getSession().setAttribute("errmsgPwd", "");
                request.getSession().setAttribute("errmsg", invalidcredentials);
                request.getSession().setAttribute("lock", false);
                redirectStrategy.sendRedirect(request, response, "/home");
            }
        }
    }
}
