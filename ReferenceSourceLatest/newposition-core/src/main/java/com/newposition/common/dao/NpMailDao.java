package com.newposition.common.dao;

import com.newposition.common.domain.NpMailConfigProp;

public interface NpMailDao
{

    NpMailConfigProp getNpMailConfigProp();
}
