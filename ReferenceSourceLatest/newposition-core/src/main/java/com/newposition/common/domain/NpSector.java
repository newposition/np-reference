package com.newposition.common.domain;

import java.io.Serializable;

public interface NpSector extends Serializable
{

    public Integer getId();

    public void setId(Integer id);

    public String getSectorName();

    public void setSectorName(String sectorName);
}
