package com.newposition.common.dao;

import java.util.List;

import com.newposition.common.domain.NpUsersReason;

public interface NpUsersReasonDao
{

    public List<NpUsersReason> getReasonsForUser();

    public List<NpUsersReason> getReasonsForClient();

    public NpUsersReason getReasonForId(int id);
}
