package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.candidate.domain.NpCandidateEmpRoles;

@Entity
@Table(name = "np_employment_roles", catalog = "npdbs")
public class NpEmploymentRoleImpl implements NpEmploymentRole
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DomainTypeID")
    private NpDomainImpl npDomain;

    @Column(name = "RoleName", length = 20)
    private String roleName;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npEmploymentRoleImpl")
    private Set<CoRoleAlias> coRoleAlias;

    @Column(name = "status")
    private boolean status;

    @Column(name = "Valid")
    private String valid;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npEmploymentRoles")
    private Set<NpCandidateEmpRoles> npCandidateEmpRoleses = new HashSet<NpCandidateEmpRoles>(
            0);

    @Override
    public Set<NpCandidateEmpRoles> getNpCandidateEmpRoleses()
    {
        return this.npCandidateEmpRoleses;
    }

    @Override
    public void setNpCandidateEmpRoleses(
            Set<NpCandidateEmpRoles> npCandidateEmpRoleses)
    {
        this.npCandidateEmpRoleses = npCandidateEmpRoleses;
    }

    @Override
    public String getValid()
    {
        return valid;
    }

    @Override
    public void setValid(String valid)
    {
        this.valid = valid;
    }

    @Override
    public boolean isStatus()
    {
        return status;
    }

    @Override
    public void setStatus(boolean status)
    {
        this.status = status;
    }

    @Override
    public Set<CoRoleAlias> getCoRoleAlias()
    {
        return coRoleAlias;
    }

    @Override
    public void setCoRoleAlias(Set<CoRoleAlias> coRoleAlias)
    {
        this.coRoleAlias = coRoleAlias;
    }

    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public NpDomainImpl getNpDomain()
    {
        return this.npDomain;
    }

    public void setNpDomain(NpDomainImpl npDomain)
    {
        this.npDomain = npDomain;
    }

    public String getRoleName()
    {
        return this.roleName;
    }

    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }

    public String getCreatedBy()
    {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
