package com.newposition.common.dao.impl;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.common.dao.NpMailDao;
import com.newposition.common.domain.NpMailConfigProp;

public class NpMailDaoImpl implements NpMailDao
{

    private HibernateTemplate hibernateTemplate;

    @Override
    public NpMailConfigProp getNpMailConfigProp()
    {

        System.out.println(" Mail dao executed ");
        NpMailConfigProp npMailConfigProp = hibernateTemplate.get(NpMailConfigProp.class, 1);

        System.out.println("user name " + npMailConfigProp.getNpMailUserName());
        return npMailConfigProp;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }
}
