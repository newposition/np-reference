package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.candidate.domain.NpCandidateTools;

@Entity
@Table(name = "np_tools", catalog = "npdbs")
public class NpToolImpl implements NpTool
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "Version", length = 20)
    private String version;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DomainTypeID")
    private NpDomainImpl npDomain;

    @Column(name = "ToolName", nullable = false, length = 20)
    private String toolName;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @Column(name = "Valid", nullable = false, length = 10)
    private String valid;

    @Column(name = "ToolType", nullable = false, length = 10)
    private String toolType;

    @Column(name = "status")
    private boolean status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npTools")
    private Set<NpCandidateTools> npCandidateTools = new HashSet<NpCandidateTools>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npToolImpl")
    private Set<CoToolAlias> coToolAlias;

    @Override
    public Set<CoToolAlias> getCoToolAlias()
    {
        return coToolAlias;
    }

    @Override
    public void setCoToolAlias(Set<CoToolAlias> coToolAlias)
    {
        this.coToolAlias = coToolAlias;
    }

    @Override
    public boolean isStatus()
    {
        return status;
    }

    @Override
    public void setStatus(boolean status)
    {
        this.status = status;
    }

    @Override
    public String getToolType()
    {
        return toolType;
    }

    @Override
    public void setToolType(String toolType)
    {
        this.toolType = toolType;
    }

    @Override
    public String getValid()
    {
        return valid;
    }

    @Override
    public void setValid(String valid)
    {
        this.valid = valid;
    }

    @Override
    public Integer getId()
    {
        return this.id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public NpDomainImpl getNpDomain()
    {
        return this.npDomain;
    }

    @Override
    public void setNpDomain(NpDomainImpl npDomain)
    {
        this.npDomain = npDomain;
    }

    @Override
    public String getToolName()
    {
        return this.toolName;
    }

    @Override
    public void setToolName(String toolName)
    {
        this.toolName = toolName;
    }

    @Override
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String getVersion()
    {
        return this.version;
    }

    @Override
    public void setVersion(String version)
    {
        this.version = version;
    }

    @Override
    public Set<NpCandidateTools> getNpCandidateTools()
    {
        return this.npCandidateTools;
    }

    @Override
    public void setNpCandidateTools(Set<NpCandidateTools> npCandidateTools)
    {
        this.npCandidateTools = npCandidateTools;
    }
}
