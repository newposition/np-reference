package com.newposition.common.domain;

import java.util.Date;

public interface NpDomain
{

    public Integer getId();

    public void setId(Integer id);

    public String getTypeName();

    public void setTypeName(String typeName);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
