package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "np_user_status", catalog = "npdbs", uniqueConstraints = @javax.persistence.UniqueConstraint(columnNames = "Status"))
public class NpUserStatusImpl implements NpUserStatus
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "StatusType", nullable = false, length = 20)
    private String statusType;

    @Column(name = "Status", unique = true, length = 20)
    private String status;

    @Column(name = "CreateBy", length = 50)
    private String createBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", length = 50)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", length = 10)
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "npUserStatus")
    private NpUserImpl npUser;

    @Override
    public Integer getId()
    {
        return this.id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public String getStatusType()
    {
        return this.statusType;
    }

    @Override
    public void setStatusType(String statusType)
    {
        this.statusType = statusType;
    }

    @Override
    public String getStatus()
    {
        return this.status;
    }

    @Override
    public void setStatus(String status)
    {
        this.status = status;
    }

    @Override
    public String getCreateBy()
    {
        return this.createBy;
    }

    @Override
    public void setCreateBy(String createBy)
    {
        this.createBy = createBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public NpUserImpl getNpUser()
    {
        return npUser;
    }

    @Override
    public void setNpUser(NpUserImpl npUser)
    {
        this.npUser = npUser;
    }
}
