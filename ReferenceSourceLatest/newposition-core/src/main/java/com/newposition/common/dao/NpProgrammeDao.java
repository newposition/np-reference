package com.newposition.common.dao;

import java.util.List;

import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProgramDocument;
import com.newposition.common.domain.NpProgram;

public interface NpProgrammeDao
{

    List<NpProgram> getAllClientProgramme(NpClientImpl npClient);

    void saveProgramme(NpProgram npProgram, NpClientProgramDocument proDoc);

    NpProgram getNpProgrammeById(long id);

    void deleteProgramme(NpProgram npProgram);

    NpProgram updateNpProgram(NpProgram npProgram);

    List<NpClientCostCentreImpl> getCostCenterForCode(String str);

    List<NpClientCostCentreImpl> getCostCenterForName(String str);

    List<NpClientCostCentreImpl> getCostCenterForLocation(String str);

    List<NpClientCostCentreImpl> getCostCenterForOwner(String str);
}
