package com.newposition.common.forms;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class NewPaswordForm
{

    @Pattern(regexp = "^(((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[-!@#%^&_*]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-!@#%^&_*]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-!@#%^&_*]))).{8,}$", message = "{encryptedpassword.required}")
    private String newPassword;

    @NotEmpty(message = "Please Provide ConfirmPassword")
    private String confirmPassword;

    private String email;

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getNewPassword()
    {
        return newPassword;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }
}
