package com.newposition.common.dao;

import com.newposition.common.domain.NpSector;

public interface NpSectorDao extends GenericDao<NpSector, Long>
{

    public NpSector findSectorByName(String name);
}
