package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * CoSource generated by Trinadh
 */
@Entity
@Table(name = "co_source", catalog = "npdbs", uniqueConstraints = @UniqueConstraint(columnNames = "SourceID"))
public class CoSource implements java.io.Serializable
{

    private static final long serialVersionUID = 1L;

    private Integer sourceID;
    private String sourceName;
    private String sourceType;
    private String sourceSubType;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "SourceID", unique = true, nullable = false)
    public Integer getSourceID()
    {
        return sourceID;
    }

    @Column(name = "SourceName")
    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    @Column(name = "SourceType")
    public String getSourceType()
    {
        return sourceType;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    @Column(name = "SourceSubType")
    public String getSourceSubType()
    {
        return sourceSubType;
    }

    public void setSourceSubType(String sourceSubType)
    {
        this.sourceSubType = sourceSubType;
    }

    public void setSourceID(Integer sourceID)
    {
        this.sourceID = sourceID;
    }

    @Column(name = "CreatedBy")
    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Column(name = "CreatedDate")
    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Column(name = "ModifiedBy")
    public String getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Column(name = "ModifiedDate")
    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
