package com.newposition.common.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.candidate.domain.NpCandidateSkills;

@Entity
@Table(name = "np_skills", catalog = "npdbs")
public class NpSkillImpl implements NpSkill
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DomainTypeID")
    private NpDomainImpl npDomain;

    @Column(name = "SkillName", length = 20)
    private String skillName;

    @Column(name = "SkillType", length = 20)
    private String skillType;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @Column(name = "Valid")
    private String valid;

    @Column(name = "status")
    private boolean status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npSkillImpl")
    private Set<CoSkillAlias> coSkillAlias;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npSkill")
    private Set<NpCandidateSkills> npCandidateSkillses = new HashSet<NpCandidateSkills>(0);

    @Override
    public Set<NpCandidateSkills> getNpCandidateSkillses()
    {
        return this.npCandidateSkillses;
    }

    @Override
    public void setNpCandidateSkillses(
            Set<NpCandidateSkills> npCandidateSkillses)
    {
        this.npCandidateSkillses = npCandidateSkillses;
    }

    @Override
    public Set<CoSkillAlias> getCoSkillAlias()
    {
        return coSkillAlias;
    }

    @Override
    public void setCoSkillAlias(Set<CoSkillAlias> coSkillAlias)
    {
        this.coSkillAlias = coSkillAlias;
    }

    @Override
    public boolean isStatus()
    {
        return status;
    }

    @Override
    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public NpDomainImpl getNpDomain()
    {
        return this.npDomain;
    }

    @Override
    public void setNpDomainTypes(NpDomainImpl npDomain)
    {
        this.npDomain = npDomain;
    }

    @Override
    public String getSkillName()
    {
        return this.skillName;
    }

    @Override
    public void setSkillName(String skillName)
    {
        this.skillName = skillName;
    }

    @Override
    public String getSkillType()
    {
        return this.skillType;
    }

    @Override
    public void setSkillType(String skillType)
    {
        this.skillType = skillType;
    }

    @Override
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String getValid()
    {
        return valid;
    }

    @Override
    public void setValid(String valid)
    {
        this.valid = valid;
    }
}
