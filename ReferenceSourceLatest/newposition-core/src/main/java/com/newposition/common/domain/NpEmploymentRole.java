package com.newposition.common.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.candidate.domain.NpCandidateEmpRoles;

public interface NpEmploymentRole
{

    public Integer getId();

    public void setId(Integer id);

    public NpDomainImpl getNpDomain();

    public void setNpDomain(NpDomainImpl npDomain);

    public String getRoleName();

    public void setRoleName(String roleName);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    void setCoRoleAlias(Set<CoRoleAlias> coRoleAlias);

    Set<CoRoleAlias> getCoRoleAlias();

    boolean isStatus();

    void setStatus(boolean status);

    String getValid();

    void setValid(String valid);

    Set<NpCandidateEmpRoles> getNpCandidateEmpRoleses();

    void setNpCandidateEmpRoleses(Set<NpCandidateEmpRoles> npCandidateEmpRoleses);
}
