package com.newposition.common.dao.impl;

import java.io.Serializable;

import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.newposition.common.dao.GenericDao;

public abstract class GenericDaoImpl<T, ID extends Serializable> extends HibernateDaoSupport implements GenericDao<T, ID>
{

    protected HibernateTemplate hibernateTemplate;

    /* protected void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
     this.hibernateTemplate = hibernateTemplate;
     }
 */
    @Override
    public T save(T entity)
    {
        getHibernateTemplate().saveOrUpdate(entity);
        return entity;
    }

    @Override
    public void merge(T entity)
    {
        getHibernateTemplate().merge(entity);
    }

    @Override
    public void delete(T entity)
    {
        getHibernateTemplate().delete(entity);
    }
}
