package com.newposition.common.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.newposition.common.domain.NpReset;
import com.newposition.common.domain.NpUser;
import com.newposition.common.forms.ChangePaswordForm;
import com.newposition.common.forms.NewPaswordForm;

public interface NpUserService extends UserDetailsService
{

    public void setActivationCode(NpReset npReset);

    public NpUser findByUserName(String userName);

    public NpUser updateFailureAttempts(String userName);

    public boolean setnewpassword(NewPaswordForm newPaswordForm);

    public void activateUserStatus(String decoded);

    void resendUnockAccountLink(String userName, String unlockAccountLink,
                                String imageContext);

    boolean unlockAccount(Integer userId);

    public void updateNpUser(NpUser npUser);

    public NpReset getActivationCode(String email);

    public NpReset resetPassword(String email);

    public NpReset verifyActivationCode(String activationCode);

    public boolean changePassword(ChangePaswordForm changePasword);

    public NpUser updateSuccessAttempts(String userName);

    public NpUser findUserById(Integer id);

    public boolean isLinkExpired(Long time);
}
