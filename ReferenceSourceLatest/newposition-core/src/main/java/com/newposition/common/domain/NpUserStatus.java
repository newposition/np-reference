package com.newposition.common.domain;

import java.io.Serializable;
import java.util.Date;

public interface NpUserStatus extends Serializable
{

    public Integer getId();

    public void setId(Integer id);

    public String getStatusType();

    public void setStatusType(String statusType);

    public String getStatus();

    public void setStatus(String status);

    public String getCreateBy();

    public void setCreateBy(String createBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public NpUserImpl getNpUser();

    public void setNpUser(NpUserImpl npUser);
}
