package com.newposition.common.domain;

import org.springframework.stereotype.Component;

@Component
public class CommonBean
{
    private Long id;
    private String value;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
