package com.newposition.common.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.candidate.domain.NpCertifications;
import com.newposition.candidate.domain.NpInstitutions;

@Entity
@Table(name = "np_institute_certificate", catalog = "npdbs")
public class NpInstituteCertificateImpl implements NpInstituteCertificate
{

    private Integer id;
    private NpCertifications npCertifications;
    private NpInstitutions npInstitutions;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private boolean status;

    public NpInstituteCertificateImpl()
    {
    }

    public NpInstituteCertificateImpl(String createdBy, Date createdDate,
                                      String modifiedBy, Date modifiedDate)
    {
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
    }

    public NpInstituteCertificateImpl(NpCertifications npCertifications, NpInstitutions npInstitutions,
                                      String createdBy, Date createdDate, String modifiedBy,
                                      Date modifiedDate)
    {
        this.npInstitutions = npInstitutions;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
        this.npCertifications = npCertifications;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CertificateID")
    public NpCertifications getNpCertifications()
    {
        return this.npCertifications;
    }

    public void setNpCertifications(NpCertifications npCertifications)
    {
        this.npCertifications = npCertifications;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "InstituteID")
    public NpInstitutions getNpInstitutions()
    {
        return this.npInstitutions;
    }

    public void setNpInstitutions(NpInstitutions npInstitutions)
    {
        this.npInstitutions = npInstitutions;
    }

    @Column(name = "CreatedBy", length = 50)
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", length = 10)
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Column(name = "ModifiedBy", length = 20)
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", length = 10)
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Column(name = "Status")
    public boolean getStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }
}
