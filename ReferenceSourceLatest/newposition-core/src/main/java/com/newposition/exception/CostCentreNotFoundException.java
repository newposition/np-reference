package com.newposition.exception;

public class CostCentreNotFoundException extends RuntimeException
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public CostCentreNotFoundException(String message)
    {
        super(message);
    }
}
