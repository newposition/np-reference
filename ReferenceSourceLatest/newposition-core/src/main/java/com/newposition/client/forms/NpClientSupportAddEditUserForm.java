/**
 *
 */
package com.newposition.client.forms;

/**
 * @author Trinadh
 */
public class NpClientSupportAddEditUserForm
{

    public String userId;

    public String firstName;

    public String lastName;

    public String primaryEmailAddress;

    public String confirmEmailAddress;

    public String officeTelePhone;

    public String jobTitle;

    public String currentRole;

    public String mobile;

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getConfirmEmailAddress()
    {
        return confirmEmailAddress;
    }

    public void setConfirmEmailAddress(String confirmEmailAddress)
    {
        this.confirmEmailAddress = confirmEmailAddress;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPrimaryEmailAddress()
    {
        return primaryEmailAddress;
    }

    public void setPrimaryEmailAddress(String primaryEmailAddress)
    {
        this.primaryEmailAddress = primaryEmailAddress;
    }

    public String getCurrentRole()
    {
        return currentRole;
    }

    public void setCurrentRole(String currentRole)
    {
        this.currentRole = currentRole;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getOfficeTelePhone()
    {
        return officeTelePhone;
    }

    public void setOfficeTelePhone(String officeTelePhone)
    {
        this.officeTelePhone = officeTelePhone;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }
}
