package com.newposition.client.dao;

import com.newposition.client.domain.NpClient;

public interface NpClientCompanyInformationDao
{

    public NpClient findByUserName(Integer userId);
}
