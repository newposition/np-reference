package com.newposition.client.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpUsersReason;

/**
 * NpUsers generated by hbm2java
 */
@Entity
@Table(name = "co_client_activation_status", catalog = "npdbs")
public class CommonClientActivationStatusImpl implements CommonClientActivationStatus
{

    private int clientActivationId;

    private NpUsersReason termReason;

    private NpClient npClient;

    private NpClientStatus npClientStatus;

    private String comments;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "clientActivationId", unique = true, nullable = false)
    public int getClientActivationId()
    {
        return clientActivationId;
    }

    public void setClientActivationId(int clientActivationId)
    {
        this.clientActivationId = clientActivationId;
    }

    @OneToOne(targetEntity = NpClientImpl.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "ClientId", nullable = false, unique = true)
    public NpClient getNpClient()
    {
        return npClient;
    }

    public void setNpClient(NpClient npClient)
    {
        this.npClient = npClient;
    }

    @OneToOne(fetch = FetchType.EAGER, targetEntity = NpClientStatusImpl.class)
    @JoinColumn(name = "ClientStatusId", unique = true, nullable = false)
    public NpClientStatus getNpClientStatus()
    {
        return npClientStatus;
    }

    public void setNpClientStatus(NpClientStatus npClientStatus)
    {
        this.npClientStatus = npClientStatus;
    }

    @Column(name = "Comments")
    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TermReason", nullable = false)
    public NpUsersReason getTermReason()
    {
        return termReason;
    }

    public void setTermReason(NpUsersReason termReason)
    {
        this.termReason = termReason;
    }
}
