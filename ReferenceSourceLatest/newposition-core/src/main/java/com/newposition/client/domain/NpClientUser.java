package com.newposition.client.domain;

import com.newposition.common.domain.NpUser;

public interface NpClientUser extends NpUser
{

    public NpClient getOrganization();

    public void setOrganization(NpClient organization);
}
