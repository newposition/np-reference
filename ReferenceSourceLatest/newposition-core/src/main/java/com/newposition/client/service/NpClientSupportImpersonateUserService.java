package com.newposition.client.service;

import java.util.List;

import com.newposition.common.domain.NpUser;

public interface NpClientSupportImpersonateUserService
{

    public List<NpUser> getListOfNpUser();

    public List getSortedNpUsers(String sortBy, String pageNumber, String filterBy);

    public NpUser getUserById(int userId);
}
