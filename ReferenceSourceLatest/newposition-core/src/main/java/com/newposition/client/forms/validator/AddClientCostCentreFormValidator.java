package com.newposition.client.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.newposition.client.forms.AddClientCostCentreForm;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;

public class AddClientCostCentreFormValidator implements Validator
{

    @Override
    public boolean supports(Class<?> clazz)
    {
        return NpClientSupportAddEditUserForm.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors)
    {

        final AddClientCostCentreForm form = (AddClientCostCentreForm) object;
    }
}
