package com.newposition.client.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.client.dao.NpClientDao;
import com.newposition.client.domain.CommonClientActivationStatus;
import com.newposition.client.domain.CommonClientActivationStatusImpl;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientStatus;
import com.newposition.client.domain.NpClientStatusImpl;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.client.forms.ReasonForm;
import com.newposition.common.dao.NpUsersReasonDao;
import com.newposition.common.dao.impl.GenericDaoImpl;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.domain.NpUserStatusImpl;

public class NpClientDaoImpl extends GenericDaoImpl<NpClient, Integer> implements NpClientDao
{

	 /*private HibernateTemplate hibernateTemplate;

   public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
	this.hibernateTemplate = hibernateTemplate;
    }

    public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}*/

    private NpUsersReasonDao npUsersReasonDao;
    private int numberOfReusltsPerPage;

    public NpUsersReasonDao getNpUsersReasonDao()
    {
        return npUsersReasonDao;
    }

    public void setNpUsersReasonDao(NpUsersReasonDao npUsersReasonDao)
    {
        this.npUsersReasonDao = npUsersReasonDao;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @Override
    public NpClient findClientByPrincipal(String principal)
    {
        @SuppressWarnings("unchecked")
        List<NpClient> clients = (List<NpClient>) getHibernateTemplate().findByNamedParam(
                "from com.newposition.client.domain.NpClientImpl C WHERE C.principleContactEmail=:principleContactEmail",
                "principleContactEmail", principal);
        if (clients != null && clients.size() > 0)
        {
            return clients.get(0);
        }
        return null;
    }

    @Override
    public NpClient findClientByCompanyName(String name)
    {
        @SuppressWarnings("unchecked")
        List<NpClient> clients = (List<NpClient>) getHibernateTemplate().findByNamedParam(
                "from com.newposition.client.domain.NpClientImpl C WHERE C.companyName=:companyName",
                "companyName", name);
        if (clients != null && clients.size() > 0)
        {
            return clients.get(0);
        }
        return null;
    }

    @Override
    public List<NpClientStatus> fetchAllClientStatus()
    {
        return getHibernateTemplate().loadAll(NpClientStatus.class);
    }

    @Override
    public NpClientStatus fetchClientStatusById(Integer id)
    {
        return getHibernateTemplate().get(NpClientStatusImpl.class, id);
    }

    @Override
    public List<NpClient> findAll()
    {
        return getHibernateTemplate().loadAll(NpClient.class);
    }

    @Override
    public NpClient findByID(Integer id)
    {
        return getHibernateTemplate().get(NpClientImpl.class, id);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void deleteNpClient(ReasonForm reasonForm)
    {
        NpClient npClient = (NpClient) findByID(Integer.parseInt(reasonForm.getUserId()));
        NpClientStatus clientStatus = new NpClientStatusImpl();
        clientStatus.setId(3);
        npClient.setClientStatus(clientStatus);
        save(npClient);

        List<CommonClientActivationStatus> commonClientActivationStatusList = (List<CommonClientActivationStatus>) getHibernateTemplate().find("from  com.newposition.client.domain.CommonClientActivationStatus C where C.npClient.id=?", Integer.parseInt(reasonForm.getUserId()));
        if (commonClientActivationStatusList.size() > 0)
        {
            CommonClientActivationStatus commonClientActivationStatus = commonClientActivationStatusList.get(0);
            commonClientActivationStatus.setComments(reasonForm.getReasonLines());
            commonClientActivationStatus.setNpClient(npClient);
            commonClientActivationStatus.setTermReason(getNpUsersReasonDao().getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commonClientActivationStatus.setNpClientStatus(clientStatus);
            getHibernateTemplate().saveOrUpdate(commonClientActivationStatus);
        }
        else
        {
            CommonClientActivationStatus commonClientActivationStatus = new CommonClientActivationStatusImpl();
            commonClientActivationStatus.setComments(reasonForm.getReasonLines());
            commonClientActivationStatus.setNpClient(npClient);
            commonClientActivationStatus.setTermReason(getNpUsersReasonDao().getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commonClientActivationStatus.setNpClientStatus(clientStatus);
            getHibernateTemplate().saveOrUpdate(commonClientActivationStatus);
        }
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void updateNpClientStatus(ReasonForm reasonForm)
    {
        NpClient npClient = (NpClient) findByID(Integer.parseInt(reasonForm.getUserId()));
//    	NpClientStatus npClientStatus=new NpClientStatusImpl();
        NpClientStatus npClientStatus = getHibernateTemplate().get(NpClientStatusImpl.class, reasonForm.getStatusId());
        /*if(npClient.getClientStatus().getId()==1)
		{
			npClientStatus.setId(2);
		}
		else{
			npClientStatus.setId(1);
		}*/
        npClient.setClientStatus(npClientStatus);
        save(npClient);
        List<CommonClientActivationStatus> commonClientActivationStatusList = (List<CommonClientActivationStatus>) getHibernateTemplate().find("from  com.newposition.client.domain.CommonClientActivationStatus C where C.npClient.id=?", Integer.parseInt(reasonForm.getUserId()));
        if (commonClientActivationStatusList.size() > 0)
        {
            CommonClientActivationStatus commonClientActivationStatus = commonClientActivationStatusList.get(0);
            commonClientActivationStatus.setComments(reasonForm.getReasonLines());
            commonClientActivationStatus.setNpClient(npClient);
            commonClientActivationStatus.setTermReason(getNpUsersReasonDao().getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commonClientActivationStatus.setNpClientStatus(npClientStatus);
            getHibernateTemplate().saveOrUpdate(commonClientActivationStatus);
        }
        else
        {
            CommonClientActivationStatus commonClientActivationStatus = new CommonClientActivationStatusImpl();
            commonClientActivationStatus.setComments(reasonForm.getReasonLines());
            commonClientActivationStatus.setNpClient(npClient);
            commonClientActivationStatus.setTermReason(getNpUsersReasonDao().getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commonClientActivationStatus.setNpClientStatus(npClientStatus);
            getHibernateTemplate().saveOrUpdate(commonClientActivationStatus);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpClient> getNpClients()
    {
        List<NpClient> npClients = null;
        npClients = (List<NpClient>) getHibernateTemplate().find("from com.newposition.client.domain.NpClient C where C.clientStatus.id !=3 order by C.sector.id ");

        return npClients;
    }

    @Override
    public NpUserStatus getDisableUserStatus()
    {

        NpUserStatus npUserStatus = getHibernateTemplate().get(NpUserStatusImpl.class, 2);
        return npUserStatus;
    }

    @Override
    public List getSortedNpClients(String sortBy, String pageNumber)
    {

        List clientList = new ArrayList();

        String query = "from NpClientImpl C ";

        query = query + " where C.clientStatus.id !=3 ";

        List<NpClient> npClients = null;
        if (StringUtils.isNotBlank(sortBy) && !StringUtils.equalsIgnoreCase(sortBy, "undefined"))
        {
            if (sortBy.equalsIgnoreCase("firstName"))
            {
                sortBy = "C.companyName";
            }

            if (sortBy.equalsIgnoreCase("emailId"))
            {
                sortBy = "C.principleContactEmail";
            }
            if (sortBy.equalsIgnoreCase("sector"))
            {
                sortBy = "C.sector.sectorName";
            }
            if (sortBy.equalsIgnoreCase("status"))
            {
                sortBy = "C.clientStatus.status";
            }
            query = query + "  order by " + sortBy;
        }

        Long npClientCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        clientList.add(getPaginatedresult(query, pageNo));
        clientList.add((int) npClientCount.longValue());

        return clientList;
    }

    @SuppressWarnings("unchecked")
    private List<NpClientImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpClientImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @Override
    public NpClientUser findClientUserById(String userId, Integer clientId)
    {

        List<NpClientUserImpl> clientUser = (List<NpClientUserImpl>) getHibernateTemplate().find("from NpClientUserImpl cu where cu.organization.id= ? AND cu.id=? ", clientId, Integer.parseInt(userId));
        if (clientUser.size() > 0)
        {
            return clientUser.get(0);
        }

        return null;
    }

    @Override
    public List getSortedNpUserClients(String sortBy, String pageNumber, NpClient client)
    {

        List clientUserList = new ArrayList();

        String query = "from NpClientUserImpl uc where uc.npUserStatus.id !=3 and uc.organization.id='" + client.getId() + "'  ";

        if (StringUtils.isNotEmpty(sortBy) && !StringUtils.equalsIgnoreCase(sortBy, "undefined"))
        {
            if (sortBy.equalsIgnoreCase("fullname"))
            {
                sortBy = "uc.firstName";
            }
            if (sortBy.equalsIgnoreCase("userId"))
            {
                sortBy = "uc.id";
            }
            if (sortBy.equalsIgnoreCase("email"))
            {
                sortBy = "uc.primaryEmail";
            }

            query = query + " order by " + sortBy;
        }

        Long npClientUserCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        List<NpClientUserImpl> npClientUsers = null;
        npClientUsers = getPaginatedUserresult(query, pageNo);
        clientUserList.add(npClientUsers);
        clientUserList.add((int) npClientUserCount.longValue());

        return clientUserList;
    }

    @SuppressWarnings("unchecked")
    private List<NpClientUserImpl> getPaginatedUserresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpClientUserImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @Override
    public NpClientUserImpl getNpClientImplId(Integer id)
    {
        NpClientUserImpl npClientUserImpl = (NpClientUserImpl) getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(NpClientUserImpl.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
        return npClientUserImpl;
    }
}
