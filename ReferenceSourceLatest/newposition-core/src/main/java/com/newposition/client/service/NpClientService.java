package com.newposition.client.service;

import java.util.ArrayList;
import java.util.List;

import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.ReasonForm;

public interface NpClientService
{

    public NpClient addClient(AddClientForm form);

    public NpClient updateClient(AddClientForm client);

    public List<NpClient> getAllClients();

    public NpClient getClientById(Integer id);

    public List getSortedNpClients(String sortBy, String pageNumber);

    public void updateNpClientStatus(ReasonForm reasonForm);

    public void deleteNpClient(ReasonForm reasonForm);

    public List<NpClient> getNpClients();

    public NpClient findClientByCompanyName(String companyName);

    public NpClient findClientByPrincipal(String principal);

    public boolean isAddedDuplicateEmail(Integer clientId, Integer adminId, String clientEmail);

    public void sendAddClientSucessMail(AddClientForm addClientForm, String directActivationLink, String subject);

    public Integer getUserReferences(String referenceEmail);

    //sreenu

    public NpClientImpl getNpClientImpl(Integer id);

    public Integer getCountNpUserClients(String name);

    boolean isAddedDuplicateUserEmail(Integer clientId, Integer adminId,
                                      String clientEmail);

    List getSortedNpUserClients(String sortBy, String pageNumber,
                                String userName);

    NpClient getCompanyOfCurrentUser(String userName);
}
