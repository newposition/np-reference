package com.newposition.client.domain;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Set;

import com.newposition.common.domain.NpSector;

public interface NpClient extends Serializable
{

    public Integer getId();

    public void setId(Integer id);

    public String getCompanyName();

    public void setCompanyName(String companyName);

    public byte[] getCompanyLogo();

    public void setCompanyLogo(byte[] companyLogo);

    public NpSector getSector();

    public void setSector(NpSector sector);

    public Set<NpClientUser> getClientUsers();

    public void setClientUsers(Set<NpClientUser> clientUsers);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getPrincipleContactEmail();

    public void setPrincipleContactEmail(String principleContactEmail);

    public String getTeleNumber();

    public void setTeleNumber(String teleNumber);

    public String getMobileNumber();

    public void setMobileNumber(String mobileNumber);

    public String getAddress1();

    public void setAddress1(String address1);

    public String getAddress2();

    public void setAddress2(String address2);

    public String getTown();

    public void setTown(String town);

    public String getPostalCode();

    public void setPostalCode(String postalCode);

    public String getCountry();

    public void setCountry(String country);

    public NpClientStatus getClientStatus();

    public void setClientStatus(NpClientStatus clientStatus);

    public String getClientOverview();

    public void setClientOverview(String clientOverview);
}

