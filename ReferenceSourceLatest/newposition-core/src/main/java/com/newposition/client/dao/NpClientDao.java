package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientStatus;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.client.forms.ReasonForm;
import com.newposition.common.dao.GenericDao;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.client.domain.NpClientProject;

public interface NpClientDao extends GenericDao<NpClient, Integer>
{

    public NpClient findClientByPrincipal(String principal);

    public List<NpClientStatus> fetchAllClientStatus();

    public NpClientStatus fetchClientStatusById(Integer id);

    public List<NpClient> getSortedNpClients(String sortBy, String pageNumber);

    public void updateNpClientStatus(ReasonForm reasonForm);

    public void deleteNpClient(ReasonForm reasonForm);

    public List<NpClient> getNpClients();

    public NpUserStatus getDisableUserStatus();

    public NpClient findClientByCompanyName(String companyName);

    public NpClientUserImpl getNpClientImplId(Integer id);

    public List getSortedNpUserClients(String sortBy, String pageNumber, NpClient client);

    NpClientUser findClientUserById(String userId, Integer clientId);
}
