package com.newposition.client.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cl_cost_center_status", catalog = "npdbs")
public class NpClientCostCentreStatus implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "CostCenterStatusID", unique = true, nullable = false)
    private Integer costCenterStatusID;

    @Column(name = "Status")
    private String status;

    public Integer getCostCenterStatusID()
    {
        return costCenterStatusID;
    }

    public void setCostCenterStatusID(Integer costCenterStatusID)
    {
        this.costCenterStatusID = costCenterStatusID;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
