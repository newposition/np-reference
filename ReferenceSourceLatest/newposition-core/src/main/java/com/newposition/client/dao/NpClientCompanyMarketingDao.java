package com.newposition.client.dao;

import com.newposition.client.domain.NpClClientDocuments;
import com.newposition.client.domain.NpClient;

public interface NpClientCompanyMarketingDao
{

    public void saveClientMarketingForm(NpClient client, NpClClientDocuments npClClientDocuments);
}
