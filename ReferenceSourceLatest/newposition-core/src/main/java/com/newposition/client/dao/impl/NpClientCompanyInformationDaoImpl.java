package com.newposition.client.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientCompanyInformationDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientUserImpl;

public class NpClientCompanyInformationDaoImpl implements NpClientCompanyInformationDao
{

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    @Override
    public NpClient findByUserName(Integer userId)
    {
        List<NpClientUserImpl> clientUsers = (List<NpClientUserImpl>) getHibernateTemplate().find("from NpClientUserImpl C WHERE C.id=?", userId);

        if (clientUsers.size() > 0)
        {
            return (NpClient) clientUsers.get(0).getOrganization();
        }

        return null;
    }
}
