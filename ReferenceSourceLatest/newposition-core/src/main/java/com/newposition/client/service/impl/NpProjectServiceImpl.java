package com.newposition.client.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.client.dao.NpProjectDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.domain.NpClientProjectDocument;
import com.newposition.client.service.NpProjectService;

@Service
public class NpProjectServiceImpl implements NpProjectService
{

    private NpProjectDao npProjectDao;

    public NpProjectDao getNpProjectDao()
    {
        return npProjectDao;
    }

    public void setNpProjectDao(NpProjectDao npProjectDao)
    {
        this.npProjectDao = npProjectDao;
    }

    @Override
    @Transactional
    public void saveProgramme(NpClientProject npProject, NpClientProjectDocument prodoc)
    {
        npProjectDao.saveProject(npProject, prodoc);
    }

    @Override
    @Transactional
    public List<NpClientProject> getAllClientProjects(NpClientImpl npClient)
    {
        List<NpClientProject> projects = npProjectDao.getAllClientProjects(npClient);

        return projects;
    }

    @Override
    @Transactional
    public NpClientProject getNpProjectById(int id)
    {
        NpClientProject npClientProject = npProjectDao.getNpProjectById(id);
        String videoUrl = getVideoUrl(npClientProject);
        npClientProject.setVideoUrl(videoUrl);

        return npClientProject;
    }

    private String getVideoUrl(
            NpClientProject project)
    {

        if (project.getProjectDoc().getDocument() != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("data:video/mp4;base64,");
            String videoUrl = "";

            videoUrl = org.apache.commons.codec.binary.StringUtils.newStringUtf8(org.apache.commons.codec.binary.Base64.encodeBase64(project.getProjectDoc().getDocument()));

            sb.append(videoUrl);
            String url = sb.toString();
//				System.out.println("video url : "+url);

            return url;
        }

        return null;
    }

    @Override
    @Transactional
    public List<String> getStringList(String str, List<NpClientProject> projects)
    {
        List<String> strList = new ArrayList<String>();
        for (int i = 0; i < projects.size(); i++)
        {
            NpClientProject npClientProject = projects.get(i);
            if (str.equals("projectCode"))
            {
                strList.add(npClientProject.getProjectCode());
            }
            else if (str.equals("projectOwner"))
            {
                strList.add(npClientProject.getProjectOwner());
            }
            else if (str.equals("projectCostcenterCode"))
            {
                strList.add(npClientProject.getNpClientCostCentreImpl().getCostCentreCode());
            }
            else if (str.equals("projectCostcenterName"))
            {
                strList.add(npClientProject.getNpClientCostCentreImpl().getCostCentreName());
            }
        }

        List<String> strList1 = new ArrayList<String>(new HashSet<String>(strList));
        return strList1;
    }

    @Override
    @Transactional
    public String deleteProject(int id)
    {
        NpClientProject npClientProject = npProjectDao.getNpProjectById(id);
        npProjectDao.deleteProject(npClientProject);
        return "success";
    }

    @Override
    @Transactional
    public List<NpClientProject> getClientProjectsByFilter(String str1,
                                                           String str2, List<NpClientProject> projects)
    {
        List<NpClientProject> projects1 = new ArrayList<NpClientProject>();
        for (int i = 0; i < projects.size(); i++)
        {
            NpClientProject npClientProject = projects.get(i);
            if (str1.equals("projectCode"))
            {
                if (str2.equals(npClientProject.getProjectCode()))
                {
                    projects1.add(npClientProject);
                }
            }
            else if (str1.equals("projectOwner"))
            {
                if (str2.equals(npClientProject.getProjectOwner()))
                {
                    projects1.add(npClientProject);
                }
            }
            else if (str1.equals("projectCostcenterCode"))
            {
                if (str2.equals(npClientProject.getNpClientCostCentreImpl().getCostCentreCode()))
                {
                    projects1.add(npClientProject);
                }
            }
            else if (str1.equals("projectCostcenterName"))
            {
                if (str2.equals(npClientProject.getNpClientCostCentreImpl().getCostCentreName()))
                {
                    projects1.add(npClientProject);
                }
            }
        }
        return projects1;
    }

    @Override
    @Transactional
    public List<NpClientProject> getProjectsBySort(
            ArrayList<NpClientProject> projectlist, String str)
    {

        NpClientProject npClientProject = new NpClientProject();
        if (str.equals("projectCode"))
        {
            npClientProject.sortByProjectCode(projectlist);
        }
        else if (str.equals("projectOwner"))
        {
            npClientProject.sortByProjectOwner(projectlist);
        }
        else if (str.equals("projectCostcenterCode"))
        {
            npClientProject.sortByClientCostCentreCode(projectlist);
        }
        else if (str.equals("projectCostcenterName"))
        {
            npClientProject.sortByClientCostCentreName(projectlist);
        }
        return projectlist;
    }

    @Transactional
    @Override
    public NpClientProject saveOrUpdate(NpClientProject npClientProject)
    {
        NpClientProject npClientProject1 = npProjectDao.updateNpClientProject(npClientProject);
        return npClientProject1;
    }

    @Override
    public String checkProjectCode(String str, NpClientImpl npClient)
    {
        int refCount = 0;
        String result = "";
        List<NpClientProject> projects = npProjectDao.getAllClientProjects(npClient);
        for (int i = 0; i < projects.size(); i++)
        {
            NpClientProject npClientProject = projects.get(i);
            if (npClientProject.getProjectCode().equals(str))
            {
                refCount++;
            }
        }
        if (refCount > 0)
        {
            return "NOT OK";
        }
        else
        {
            return "OK";
        }
    }

    @Override
    public byte[] getVideo(File video)
    {
        byte[] videoInByte = null;
        try
        {
            File file = video;
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            videoInByte = multipartFile.getBytes();
        }
        catch (Exception e)
        {
            System.out.println("Error occured during Multipart file convertion");
        }
        return videoInByte;
    }

    @Override
    public NpClientProject updateWithNewValue(NpClientProject source,
                                              NpClientProject target)
    {

        target.setProjectDescription(source.getProjectDescription());
        target.setProjectOwner(source.getProjectOwner());
        target.setProjectOverview(source.getProjectOverview());
        target.setNpClientCostCentreImpl(source.getNpClientCostCentreImpl());
        target.setProjectDoc(source.getProjectDoc());
        return target;
    }

    @Override
    public List<NpClientProject> getAllClientProjectsWithVideo(NpClient client)
    {

        List<NpClientProject> projects = getAllClientProjects((NpClientImpl) client);
        List<NpClientProject> _projects = getProjectsVideo(projects);
        return _projects;
    }

    private List<NpClientProject> getProjectsVideo(
            List<NpClientProject> projects)
    {

        List<NpClientProject> projectsWithVideo = new ArrayList<NpClientProject>();

        for (NpClientProject _project : projects)
        {
            NpClientProject pro = new NpClientProject();

            pro.setClientProjectID(_project.getClientProjectID());
            pro.setVideoUrl(getVideoUrl(_project));

            projectsWithVideo.add(pro);
        }
        return projectsWithVideo;
    }
}
