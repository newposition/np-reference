package com.newposition.client.forms;

import java.io.File;
import java.io.Serializable;

import com.newposition.client.domain.NpClient;

public class AddClientCostCentreForm implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long costCentreId;

    private String costCentreCode;

    private String description;

    private String location;

    private String costCentreOwner;

    private String costCentreSystem;

    @Override
    public String toString()
    {
        return "AddClientCostCentreForm [costCentreId=" + costCentreId
                + ", costCentreCode=" + costCentreCode + ", description="
                + description + ", location=" + location + ", costCentreOwner="
                + costCentreOwner + ", costCentreSystem=" + costCentreSystem
                + ", costCentreName=" + costCentreName + ", overview="
                + overview + ", client=" + client + "]";
    }

    private String costCentreName;

    private String overview;

    private NpClient client;

    private File video;

    public File getVideo()
    {
        return video;
    }

    public void setVideo(File video)
    {
        this.video = video;
    }

    public Long getCostCentreId()
    {
        return costCentreId;
    }

    public void setCostCentreId(Long costCentreId)
    {
        this.costCentreId = costCentreId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getCostCentreOwner()
    {
        return costCentreOwner;
    }

    public void setCostCentreOwner(String costCentreOwner)
    {
        this.costCentreOwner = costCentreOwner;
    }

    public String getCostCentreName()
    {
        return costCentreName;
    }

    public void setCostCentreName(String costCentreName)
    {
        this.costCentreName = costCentreName;
    }

    public String getCostCentreSystem()
    {
        return costCentreSystem;
    }

    public void setCostCentreSystem(String costCentreSystem)
    {
        this.costCentreSystem = costCentreSystem;
    }

    public String getOverview()
    {
        return overview;
    }

    public void setOverview(String overview)
    {
        this.overview = overview;
    }

    public NpClient getClient()
    {
        return client;
    }

    public void setClient(NpClient client)
    {
        this.client = client;
    }

    public String getCostCentreCode()
    {
        return costCentreCode;
    }

    public void setCostCentreCode(String costCentreCode)
    {
        this.costCentreCode = costCentreCode;
    }
}
