package com.newposition.client.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.newposition.client.dao.NpClientSupportImpersonateUserDao;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserImpl;
import com.newposition.common.domain.NpUserRole;
import com.newposition.utils.CompareByFirstName;
import com.newposition.utils.CompareObjectByEmail;
import com.newposition.utils.CompareObjectById;

@Repository
public class NpClientSupportImpersonateUserDaoImpl implements NpClientSupportImpersonateUserDao
{

    protected static final Logger LOG = Logger.getLogger(NpClientSupportImpersonateUserDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    private int numberOfReusltsPerPage;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    /**
     * @return List of NpUsers
     */
    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public List<NpUser> getListOfNpUser()
    {

        List<NpUserRole> listOfNpUserRoles = (List<NpUserRole>) hibernateTemplate.find("from NpUserRole where roleId not in(2)");
        List<NpUser> listOfNpUsers = new ArrayList<NpUser>();
        LOG.info(listOfNpUserRoles.size());
        if (listOfNpUserRoles.size() > 0)
        {
            for (NpUserRole npUserRoles : listOfNpUserRoles)
            {
                if (StringUtils.isNotEmpty(npUserRoles.getNpUser().getEncryptedPassword()))
                {
                    listOfNpUsers.add(npUserRoles.getNpUser());
                    LOG.info(npUserRoles.getNpRole().getRoleName());
                }
            }
        }
        return listOfNpUsers;
    }

    /**
     * @param sortBy
     * @return List<NpUsers> list of NpUser sorted according to parameter sortBy
     */
    @SuppressWarnings("unchecked")
    @Override
    public List getSortedNpUsers(String sortBy, String pageNumber, String filterBy)
    {

        List paginatedData = new ArrayList();
        String firstName = "", lastName = "";

        List<NpUser> listOfSortedNpUsers = new ArrayList<NpUser>();

        String query = "from NpUserRole U where U.npRole.id != 2 AND U.npRole.id != 11 ";

        if (StringUtils.isNotEmpty(filterBy) && filterBy != "undefined")
        {
            String[] name = filterBy.split(",");
            firstName = name[0];
            lastName = name[1];

            query = query + " and U.npUser.firstName ='" + firstName + "' and U.npUser.lastName = '" + lastName + "'";
        }

        if (StringUtils.isNotEmpty(sortBy))
        {
            if (sortBy.equalsIgnoreCase("email"))
            {
                query = query + " order by U.npUser.primaryEmail";
            }

            if (sortBy.equalsIgnoreCase("fullName"))
            {
                query = query + " order by U.npUser.firstName";
            }
            if (sortBy.equalsIgnoreCase("userId"))
            {
                query = query + " order by U.npUser.id";
            }
        }

        Long npUserCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        List<NpUserRole> npUserRoleList = getPaginatedresult(query, pageNo);
        for (NpUserRole npUserRole : npUserRoleList)
        {
            listOfSortedNpUsers.add(npUserRole.getNpUser());
        }
            /*if (StringUtils.isNotEmpty(sortBy)) {
			if(sortBy.equalsIgnoreCase("email"))
			{
				java.util.Collections.sort(listOfSortedNpUsers, new CompareObjectByEmail());
			}
			
			if(sortBy.equalsIgnoreCase("fullName"))
			{
				java.util.Collections.sort(listOfSortedNpUsers, new CompareByFirstName());
			}
			if(sortBy.equalsIgnoreCase("userId"))
			{
				java.util.Collections.sort(listOfSortedNpUsers, new CompareObjectById());
				
			}
		}*/

        paginatedData.add(listOfSortedNpUsers);
        paginatedData.add((int) npUserCount.longValue());
        paginatedData.add(getListOfNpUser());

        return paginatedData;
    }

    @SuppressWarnings("unchecked")
    private List<NpUserRole> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpUserRole>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    /**
     * @param userId
     * @return NpUsers having Id = userId
     */
    @Override
    public NpUser getUserById(int userId)
    {

        NpUser npUsers = (NpUser) hibernateTemplate.find("from NpUsers where id = ?", userId).get(0);
        return npUsers;
    }
}
