package com.newposition.client.forms;

import java.io.File;

public class ClientMarketingForm
{

    private String ClientOverview;

    private File video;

    public String getClientOverview()
    {
        return ClientOverview;
    }

    public void setClientOverview(String clientOverview)
    {
        ClientOverview = clientOverview;
    }

    public File getVideo()
    {
        return video;
    }

    public void setVideo(File video)
    {
        this.video = video;
    }
}
