package com.newposition.client.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpDocumentTypes;

@Entity
@Table(name = "cl_costcenter_documents", catalog = "npdbs")
public class NpClientCostCentreDocument implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CostCentreID")
    private Long costCentreID;

    @OneToOne
    @JoinColumn(name = "CostCentreID", nullable = false, insertable = false, updatable = false)
    private NpClientCostCentreImpl costCentre;

    @OneToOne
    @JoinColumn(name = "DocumentTypeID", nullable = false)
    private NpDocumentTypes documentTypeID;

    @Column(name = "Document")
    private byte[] document;

    @Column(name = "Comments")
    private String comments;

    public NpClientCostCentreImpl getCostCentre()
    {
        return costCentre;
    }

    public void setCostCentre(NpClientCostCentreImpl costCentre)
    {
        this.costCentre = costCentre;
    }

    public Long getCostCentreID()
    {
        return costCentreID;
    }

    public void setCostCentreID(Long costCentreID)
    {
        this.costCentreID = costCentreID;
    }

    public NpDocumentTypes getDocumentTypeID()
    {
        return documentTypeID;
    }

    public void setDocumentTypeID(NpDocumentTypes documentTypeID)
    {
        this.documentTypeID = documentTypeID;
    }

    public byte[] getDocument()
    {
        return document;
    }

    public void setDocument(byte[] document)
    {
        this.document = document;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }
}
