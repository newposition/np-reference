package com.newposition.client.forms.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.newposition.client.forms.NpClientAddRoleGroupForm;

public class NpClientAddRoleGroupFormValidator implements Validator
{

    @Override
    public boolean supports(Class<?> clazz)
    {
        return NpClientAddRoleGroupForm.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors)
    {

        final NpClientAddRoleGroupForm npClientAddRoleGroupForm = (NpClientAddRoleGroupForm) object;

        String groupName = npClientAddRoleGroupForm.getGroupName();

        String groupDescription = npClientAddRoleGroupForm.getGroupDescription();

        if (groupName == null || groupName.length() == 0)
        {
            errors.rejectValue("groupName", "groupName.required");
        }

        if (groupName.length() > 20)
        {
            errors.rejectValue("groupName", "groupName.length");
        }

        if (groupDescription == null || groupDescription.length() == 0)
        {
            errors.rejectValue("groupDescription", "groupDescription.required");
        }

        if (groupDescription.length() > 200)
        {
            errors.rejectValue("groupDescription", "groupName.length");
        }

        if (npClientAddRoleGroupForm.currentRole == null || npClientAddRoleGroupForm.currentRole.length() == 0)
        {
            errors.rejectValue("currentRole", "groupRoles.required");
        }
    }
}
