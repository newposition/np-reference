package com.newposition.client.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.newposition.client.forms.NpClientSupportAddEditUserForm;

public class ClientSupportAddEditUserFormValidator implements Validator
{

    @Override
    public boolean supports(Class<?> clazz)
    {
        return NpClientSupportAddEditUserForm.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors)
    {

        final NpClientSupportAddEditUserForm clientSupportAddEditUserForm = (NpClientSupportAddEditUserForm) object;

        String email = clientSupportAddEditUserForm.getPrimaryEmailAddress().trim();

        String confirmEmail = clientSupportAddEditUserForm.getConfirmEmailAddress().trim();

        String emailPattern = "^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\\.[a-zA-Z]{2,}$";

        Pattern pattern = null;

        Matcher match = null;

        boolean isMatchTel = true;

        if (clientSupportAddEditUserForm.getMobile().length() != 0)
        {
            if (clientSupportAddEditUserForm.getMobile().matches("^[+]?\\d+(\\d+)?$"))
            {

                if (clientSupportAddEditUserForm.getMobile().length() < 10
                        || (clientSupportAddEditUserForm.getMobile().length() > 16))
                {
                    errors.rejectValue("mobile", "error.mobile.length");
                }
            }
            else
            {
                errors.rejectValue("mobile", "mobile.required");
            }
        }
        else
        {
            errors.rejectValue("mobile", "mobile.required");
        }

        if (clientSupportAddEditUserForm.getOfficeTelePhone().length() != 0)
        {
            if (clientSupportAddEditUserForm.getOfficeTelePhone().matches("^[+]?\\d+(\\d+)?$"))
            {

                if (clientSupportAddEditUserForm.getOfficeTelePhone().length() < 10
                        || (clientSupportAddEditUserForm.getOfficeTelePhone().length() > 16))
                {
                    errors.rejectValue("officeTelePhone", "error.officeTelephone.length");
                }
            }
            else
            {
                errors.rejectValue("officeTelePhone", "officeTelephone.required");
            }
        }

//		if (clientSupportAddEditUserForm.getUserId() == null
//				|| clientSupportAddEditUserForm.getUserId().length() == 0) {
//			errors.rejectValue("userId", "userId.required");
//		}

        if (clientSupportAddEditUserForm.getFirstName() == null
                || clientSupportAddEditUserForm.getFirstName().trim().length() == 0)
        {
            errors.rejectValue("firstName", "firstName.required");
        }
        if (clientSupportAddEditUserForm.getCurrentRole() == null
                || clientSupportAddEditUserForm.getCurrentRole().length() == 0)
        {
            errors.rejectValue("currentRole", "currentRole.required");
        }
        if (clientSupportAddEditUserForm.getLastName() == null
                || clientSupportAddEditUserForm.getLastName().trim().length() == 0)
        {
            errors.rejectValue("lastName", "lastName.required");
        }

        boolean isValidEmail = true;
        if (email == null || email.length() == 0)
        {
            isValidEmail = false;
            errors.rejectValue("primaryEmailAddress", "email.required");
        }

        if (email.length() > 0 && email.length() <= 50)
        {

            // Create a Pattern object
            pattern = Pattern.compile(emailPattern);

            // Now create matcher object.
            match = pattern.matcher(email.trim());

            if (!(isMatchTel = match.find()))
            {
                isValidEmail = false;
                errors.rejectValue("primaryEmailAddress", "email.format");
            }
            else
            {
                if (email.contains("..") || email.contains("--") || email.contains("__"))
                {
                    isValidEmail = false;
                    errors.rejectValue("primaryEmailAddress", "email.consecutive.periods");
                }
            }
        }
        else
        {
            isValidEmail = false;
        }
        if (isValidEmail && (confirmEmail == null || confirmEmail.length() == 0))
        {
            errors.rejectValue("confirmEmailAddress", "email.required");
        }
        else if (isValidEmail && confirmEmail.length() > 0)
        {

            if (!email.equals(confirmEmail))
            {

                errors.rejectValue("confirmEmailAddress", "email.same");
            }
        }
    }
}
