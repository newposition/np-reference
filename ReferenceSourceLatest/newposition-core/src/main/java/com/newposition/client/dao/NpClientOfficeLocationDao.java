package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;

public interface NpClientOfficeLocationDao
{

    List<NpClientOfficeLocation> getAllClientOfficeLocations(
            NpClientImpl npClient);

    NpClientOfficeLocation getnpClientOfficeLocationServiceById(int id);

    void saveCoCities(CoCities coCities);

    void saveCoCounty(CoCounty coCounty);

    void saveCoCountry(CoCountry coCountry);

    void saveCoLocationState(CoLocationState coLocationState);

    void saveCoZip(CoZip coZip);

    void saveCoLocations(CoLocations coLocations);

    void saveNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation);

    List<CoLocations> getCoLocationsList(String str1);

    List<CoLocations> getCoLocationsList1(String str1);

    List<CoLocations> getCoLocationsList2(String str1);

    List<CoCities> getCoCitiesList(String str1);

    List<CoCounty> getCoCountyList(String str1);

    List<CoCountry> getCoCountryList(String str1);

    List<CoLocationState> getCoStateList(String str1);

    List<CoLocationState> getCoStateList1(String str1);

    List<CoZip> getAllZips();

    void deleteNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation);

    List<CoCountry> getAllCountries();

    List<CoLocations> getAllCoLocations(String str);

    List<CoLocations> getAllCoLocations(String str1, String str2);

    List<NpClientOfficeLocation> getLocationListByCountryName(String str);

    List<NpClientOfficeLocation> getLocationListByCountryName(String str1,
                                                              String str2);

    List<CoCities> getCoCitiesListByName(String cityDescription);

    List<CoCounty> getCoCountyListByName(String countyDescription);

    List<CoCountry> getCoCountryListByName(String countryDescription);

    List<CoLocationState> getCoLocationStateListByName(String stateDescription);
}
