package com.newposition.client.domain;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.newposition.common.domain.CoLocations;

@SuppressWarnings("serial")
@Entity
@Table(name = "cl_client_office_location", catalog = "npdbs")
public class NpClientOfficeLocation implements java.io.Serializable, Comparable<NpClientOfficeLocation>
{

    @Id
    @GeneratedValue
    @Column(name = "ClientOfficeLocID")
    private Integer clientOfficeLocID;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cl_client_ClientID")
    private NpClientImpl npClient;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "LocationID")
    private CoLocations coLocations;

    public Integer getClientOfficeLocID()
    {
        return clientOfficeLocID;
    }

    public void setClientOfficeLocID(Integer clientOfficeLocID)
    {
        this.clientOfficeLocID = clientOfficeLocID;
    }

    public NpClientImpl getNpClient()
    {
        return npClient;
    }

    public void setNpClient(NpClientImpl npClient)
    {
        this.npClient = npClient;
    }

    public CoLocations getCoLocations()
    {
        return coLocations;
    }

    public void setCoLocations(CoLocations coLocations)
    {
        this.coLocations = coLocations;
    }

    @Override
    public int hashCode()
    {
        return coLocations.getAddress1().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((NpClientOfficeLocation) obj).getClientOfficeLocID();
        long id2 = this.clientOfficeLocID;
        return (id1 == id2);
    }

    @Override
    public int compareTo(NpClientOfficeLocation ncol)
    {

        return (this.getClientOfficeLocID() - ncol.getClientOfficeLocID());
    }

    @JsonIgnore
    public static void sortByAddress1(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByAddress1 = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getAddress1().compareTo(officeLocation2.getCoLocations().getAddress1());
            }
        };
        Collections.sort(officeLocations, sortByAddress1);
    }

    @JsonIgnore
    public static void sortByAddress2(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByAddress2 = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getAddress2().compareTo(officeLocation2.getCoLocations().getAddress2());
            }
        };
        Collections.sort(officeLocations, sortByAddress2);
    }

    @JsonIgnore
    public static void sortByAddress3(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByAddress3 = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getAddress3().compareTo(officeLocation2.getCoLocations().getAddress3());
            }
        };
        Collections.sort(officeLocations, sortByAddress3);
    }

    @JsonIgnore
    public static void sortByCity(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByCity = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getCoCities().getCityDescription().compareTo(officeLocation2.getCoLocations().getCoCities().getCityDescription());
            }
        };
        Collections.sort(officeLocations, sortByCity);
    }

    @JsonIgnore
    public static void sortByCounty(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByCounty = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getCoCounty().getCountyDescription().compareTo(officeLocation2.getCoLocations().getCoCounty().getCountyDescription());
            }
        };
        Collections.sort(officeLocations, sortByCounty);
    }

    @JsonIgnore
    public static void sortByCountry(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByCountry = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getCoCountry().getCountryDescription().compareTo(officeLocation2.getCoLocations().getCoCountry().getCountryDescription());
            }
        };
        Collections.sort(officeLocations, sortByCountry);
    }

    @JsonIgnore
    public static void sortByState(List<NpClientOfficeLocation> officeLocations)
    {

        Comparator<NpClientOfficeLocation> sortByState = new Comparator<NpClientOfficeLocation>()
        {
            @Override
            public int compare(NpClientOfficeLocation officeLocation1, NpClientOfficeLocation officeLocation2)
            {

                return officeLocation1.getCoLocations().getCoZip().getCoLocationState().getStateDescription().compareTo(officeLocation2.getCoLocations().getCoZip().getCoLocationState().getStateDescription());
            }
        };
        Collections.sort(officeLocations, sortByState);
    }
}