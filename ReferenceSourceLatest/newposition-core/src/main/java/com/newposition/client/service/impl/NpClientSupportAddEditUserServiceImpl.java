package com.newposition.client.service.impl;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.newposition.client.dao.NpClientDao;
import com.newposition.client.dao.NpClientSupportAddEditUserDao;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserAttempts;
import com.newposition.common.domain.NpUserAttemptsImpl;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.domain.NpUserTypes;
import com.newposition.common.domain.NpUsersReason;
import com.newposition.common.service.NpUsersReasonService;
import com.newposition.infra.mail.NpMailService;

@Service
public class NpClientSupportAddEditUserServiceImpl implements NpClientSupportAddEditUserService
{

    private NpClientSupportAddEditUserDao clientSupportAddEditUserDao;

    private String imageContext;

    private NpMailService npMailService;

    private NpUsersReasonService npUsersReasonService;

    private NpClientDao clientDao;

    @Override
    public void saveClientSupportAddEditUser(NpClientSupportAddEditUserForm clientSupportAddEditUserForm)
    {
        getClientSupportAddEditUserDao().saveClientSupportAddEditUser(clientSupportAddEditUserForm);
    }

    public NpClientDao getClientDao()
    {
        return clientDao;
    }

    public void setClientDao(NpClientDao clientDao)
    {
        this.clientDao = clientDao;
    }

    @Override
    public void sendAccountDisabledMail(ReasonForm reasonForm,
                                        String subject)
    {
        NpUser npUser = getNpUsersForId(reasonForm.getUserId());
        NpUsersReason npUsersReason = npUsersReasonService.getReasonForId(Integer.parseInt(reasonForm.getReasonCode()));
        String candidateName = npUser.getFirstName() + " " + npUser.getFirstName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", candidateName);
        context.setVariable("reasonCode", npUsersReason.getReasonCode());
        context.setVariable("template", "userDisabled-email.html");
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);

        getNpMailService().sendMail(information, npUser.getPrimaryEmail(), context);
    }

    public NpClientSupportAddEditUserDao getClientSupportAddEditUserDao()
    {
        return clientSupportAddEditUserDao;
    }

    public void setClientSupportAddEditUserDao(
            NpClientSupportAddEditUserDao clientSupportAddEditUserDao)
    {
        this.clientSupportAddEditUserDao = clientSupportAddEditUserDao;
    }

    @Override
    public List<String> getNpRoles()
    {
        return clientSupportAddEditUserDao.getNpRoles();
    }

    @Override
    public List<NpUser> getNpUsers()
    {
        return clientSupportAddEditUserDao.getNpUsers();
    }

    @Override
    public void deleteNpuser(ReasonForm reasonForm)
    {
        clientSupportAddEditUserDao.deleteNpUser(reasonForm);
    }

    @Override
    public NpUser getNpUsersForId(String id)
    {
        return clientSupportAddEditUserDao.getNpUsersForId(id);
    }

    @Override
    public void saveAndUpdateNpUser(
            NpClientSupportAddEditUserForm clientSupportAddEditUserForm)
    {
        clientSupportAddEditUserDao.saveAndUpdateNpUsers(clientSupportAddEditUserForm);
    }

    @Override
    public void updateNpuserStatus(ReasonForm reasonForm)
    {
        clientSupportAddEditUserDao.UpdateNpUsersStatus(reasonForm);
    }

    @Override
    public List getSortedNpUsers(String sortBy, String number)
    {
        return clientSupportAddEditUserDao.getSortedNpUsers(sortBy, number);
    }

    public List<NpUserStatus> fetchAllUserStatus()
    {

        return clientSupportAddEditUserDao.fetchAllUserStatus();
    }

    public String getImageContext()
    {
        return imageContext;
    }

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    public NpMailService getNpMailService()
    {
        return npMailService;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    public NpUsersReasonService getNpUsersReasonService()
    {
        return npUsersReasonService;
    }

    public void setNpUsersReasonService(NpUsersReasonService npUsersReasonService)
    {
        this.npUsersReasonService = npUsersReasonService;
    }

    @Override
    public NpClientUser getCompanyOfCurrentUser(String name)
    {
        System.out.println("service ");
//		NpUser user = npUserService.findByUserName(name);
        NpClientUser clientUser = clientSupportAddEditUserDao.getNpClientUserByName(name);
        return clientUser;
    }

    @Transactional
    @Override
    public void saveClientUser(String userName, NpClientSupportAddEditUserForm clientSupportAddEditUserForm)
    {
        // TODO Auto-generated method stub
        NpClientUser _clientUser = getCompanyOfCurrentUser(userName);
        System.out.println("Saving clientuser to the : " + _clientUser.getOrganization().getCompanyName());

        NpClientUser clientUser = null;
        if (StringUtils.isEmpty(clientSupportAddEditUserForm.getUserId()))
        {
            clientUser = new NpClientUserImpl();
        }
        else
        {
            clientUser = clientDao.findClientUserById(clientSupportAddEditUserForm.getUserId(), _clientUser.getOrganization().getId());
        }

        clientUser.setFirstName(clientSupportAddEditUserForm.getFirstName());

        clientUser.setLastName(clientSupportAddEditUserForm.getLastName());

        clientUser.setOrganization(_clientUser.getOrganization());

        clientUser.setPrimaryEmail(clientSupportAddEditUserForm.getPrimaryEmailAddress());

        clientUser.setPhone(clientSupportAddEditUserForm.getMobile());

        clientUser.setOfficeTelephone(clientSupportAddEditUserForm.getOfficeTelePhone());

        clientUser.setNpUserStatus(clientDao.getDisableUserStatus());

        clientUser.setJobtitle(clientSupportAddEditUserForm.getJobTitle());

        NpUserAttempts npUserAttempts = new NpUserAttemptsImpl();
        npUserAttempts.setAttempts(0);
        npUserAttempts.setLastModified(new Date());
        clientUser.setNpUserAttempts(npUserAttempts);
        NpUserTypes userType = new NpUserTypes();
        userType.setId(3);
        clientUser.setNpUserTypes(userType);

        String userRoles = clientSupportAddEditUserForm.getCurrentRole();
        clientSupportAddEditUserDao.saveClientUser(clientUser, userRoles);
    }

    @Override
    public List<String> getNpClientRoles()
    {
        // TODO Auto-generated method stub
        return clientSupportAddEditUserDao.getNpClientRoles();
    }

    @Override
    public void sendAccountActivationMail(NpClientSupportAddEditUserForm clientSupportAddEditUserForm, String clientSupportAddEditUseractiveLink)
    {
        Long currentTime = System.currentTimeMillis();
        String currentTimeString = "";
        String link = null;
        try
        {
            link = DatatypeConverter.printBase64Binary(clientSupportAddEditUserForm.getPrimaryEmailAddress().getBytes());
            currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        String activeLink = clientSupportAddEditUseractiveLink + "?id=" + link + "&dur=" + currentTimeString;
        String subject = "NewPosition Account activation request";

        String userName = clientSupportAddEditUserForm.getFirstName() + " " + clientSupportAddEditUserForm.getLastName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", userName);
        context.setVariable("activationPageLink", activeLink);
        context.setVariable("template", "adminAdduserEmail.html");
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);
        npMailService.sendMail(information, clientSupportAddEditUserForm.getPrimaryEmailAddress(), context);
    }
}
