package com.newposition.client.domain;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.common.domain.NpProgram;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "cl_projects", catalog = "npdbs")
public class NpClientProject implements java.io.Serializable, Comparable<NpClientProject>
{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "ClientProjectID")
    private Long clientProjectID;
    @Column(name = "ProjectCode")
    private String projectCode;
    @Column(name = "ProjectDescription")
    private String projectDescription;
    @Column(name = "ProjectOwner")
    private String projectOwner;
    @Column(name = "ProjectOverview")
    private String projectOverview;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ClientID")
    private NpClientImpl npClient;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CostCentreID")
    private NpClientCostCentreImpl npClientCostCentreImpl;

    @OneToOne
    @JoinColumn(name = "ClientProjectID")
    private NpClientProjectDocument projectDoc;

    public NpClientProjectDocument getProjectDoc()
    {
        return projectDoc;
    }

    public void setProjectDoc(NpClientProjectDocument projectDoc)
    {
        this.projectDoc = projectDoc;
    }

    @Transient
    private String videoUrl;

    public String getVideoUrl()
    {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl)
    {
        this.videoUrl = videoUrl;
    }

    public Long getClientProjectID()
    {
        return clientProjectID;
    }

    public void setClientProjectID(Long clientProjectID)
    {
        this.clientProjectID = clientProjectID;
    }

    public String getProjectCode()
    {
        return projectCode;
    }

    public void setProjectCode(String projectCode)
    {
        this.projectCode = projectCode;
    }

    public String getProjectDescription()
    {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription)
    {
        this.projectDescription = projectDescription;
    }

    public String getProjectOwner()
    {
        return projectOwner;
    }

    public void setProjectOwner(String projectOwner)
    {
        this.projectOwner = projectOwner;
    }

    public String getProjectOverview()
    {
        return projectOverview;
    }

    public void setProjectOverview(String projectOverview)
    {
        this.projectOverview = projectOverview;
    }

    public NpClientImpl getNpClient()
    {
        return npClient;
    }

    public void setNpClient(NpClientImpl npClient)
    {
        this.npClient = npClient;
    }

    public NpClientCostCentreImpl getNpClientCostCentreImpl()
    {
        return npClientCostCentreImpl;
    }

    public void setNpClientCostCentreImpl(
            NpClientCostCentreImpl npClientCostCentreImpl)
    {
        this.npClientCostCentreImpl = npClientCostCentreImpl;
    }

    @Override
    public int hashCode()
    {
        // TODO Auto-generated method stub
        return this.projectDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((NpClientProject) obj).getClientProjectID();
        long id2 = this.clientProjectID;
        return (id1 == id2);
    }

    @Override
    public int compareTo(NpClientProject npProject)
    {
        return npProject.getProjectDescription().compareTo(this.projectDescription);
    }

    @JsonIgnore
    public static void sortByProjectCode(List<NpClientProject> projectlist)
    {

        Comparator<NpClientProject> sortByProjectCode = new Comparator<NpClientProject>()
        {
            @Override
            public int compare(NpClientProject npProject1, NpClientProject npProject2)
            {

                return npProject1.getProjectCode().compareTo(npProject2.getProjectCode());
            }
        };
        Collections.sort(projectlist, sortByProjectCode);
    }

    @JsonIgnore
    public static void sortByProjectDescription(List<NpClientProject> projectlist)
    {

        Comparator<NpClientProject> sortByProjectDescription = new Comparator<NpClientProject>()
        {
            @Override
            public int compare(NpClientProject npProject1, NpClientProject npProject2)
            {

                return npProject1.getProjectDescription().compareTo(npProject2.getProjectDescription());
            }
        };
        Collections.sort(projectlist, sortByProjectDescription);
    }

    @JsonIgnore
    public static void sortByProjectOwner(List<NpClientProject> projectlist)
    {

        Comparator<NpClientProject> sortByProjectOwner = new Comparator<NpClientProject>()
        {
            @Override
            public int compare(NpClientProject npProject1, NpClientProject npProject2)
            {

                return npProject1.getProjectOwner().compareTo(npProject2.getProjectOwner());
            }
        };
        Collections.sort(projectlist, sortByProjectOwner);
    }

    @JsonIgnore
    public static void sortByClientCostCentreCode(List<NpClientProject> projectlist)
    {

        Comparator<NpClientProject> sortByClientCostCentreCode = new Comparator<NpClientProject>()
        {
            @Override
            public int compare(NpClientProject npProject1, NpClientProject npProject2)
            {

                return npProject1.getNpClientCostCentreImpl().getCostCentreCode().compareTo(npProject2.getNpClientCostCentreImpl().getCostCentreCode());
            }
        };
        Collections.sort(projectlist, sortByClientCostCentreCode);
    }

    @JsonIgnore
    public static void sortByClientCostCentreName(List<NpClientProject> projectlist)
    {

        Comparator<NpClientProject> sortByClientCostCentreName = new Comparator<NpClientProject>()
        {
            @Override
            public int compare(NpClientProject npProject1, NpClientProject npProject2)
            {

                return npProject1.getNpClientCostCentreImpl().getCostCentreName().compareTo(npProject2.getNpClientCostCentreImpl().getCostCentreName());
            }
        };
        Collections.sort(projectlist, sortByClientCostCentreName);
    }
}
