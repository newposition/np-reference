package com.newposition.client.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.client.dao.NpClientSupportAddEditUserDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.common.dao.NpUsersReasonDao;
import com.newposition.common.domain.CommonUserActivationStatus;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserAttempts;
import com.newposition.common.domain.NpUserAttemptsImpl;
import com.newposition.common.domain.NpUserImpl;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.domain.NpUserRoleId;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.domain.NpUserStatusImpl;
import com.newposition.common.domain.NpUserTypes;
import com.newposition.exception.UserNotFoundException;

public class NpClientSupportAddEditUserDaoImpl implements NpClientSupportAddEditUserDao
{

    protected static final Logger LOG = Logger.getLogger(NpClientSupportAddEditUserDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    private NpUsersReasonDao npUsersReasonDao;

    private int numberOfReusltsPerPage;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public void setNpUsersReasonDao(NpUsersReasonDao npUsersReasonDao)
    {
        this.npUsersReasonDao = npUsersReasonDao;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void saveClientSupportAddEditUser(NpClientSupportAddEditUserForm clientSupportAddEditUserForm)
    {

        String[] commaSeparatedRoles = clientSupportAddEditUserForm.getCurrentRole().split(",");
        String role1 = "";

        /** checking the role if it has been assigned candidate role or not
         *
         *  if Role_candidate then save candidate
         *   else  save npUser
         *
         */
        for (String role : commaSeparatedRoles)
        {
            if (role.equalsIgnoreCase("Role_Candidate"))
            {
                role1 = role;
                break;
            }
        }

        NpUser npUser = null;

        if (role1.equalsIgnoreCase("Role_Candidate"))
        {
            npUser = new NpCandidateImpl();
        }
        else
        {
            npUser = new NpUserImpl();
        }

        npUser.setFirstName(clientSupportAddEditUserForm.getFirstName());
        npUser.setLastName(clientSupportAddEditUserForm.getLastName());
        npUser.setPhone(clientSupportAddEditUserForm.getMobile());
        npUser.setPrimaryEmail(clientSupportAddEditUserForm.getPrimaryEmailAddress());
        npUser.setOfficeTelephone(clientSupportAddEditUserForm.getOfficeTelePhone());

        List<NpUserStatus> npUserStatus = (List<NpUserStatus>) getHibernateTemplate().find("from NpUserStatusImpl where id=?", 2);
        NpUserAttempts userAtempts = new NpUserAttemptsImpl();
        userAtempts.setAttempts(0);
        npUser.setNpUserStatus(npUserStatus.get(0));
        npUser.setNpUserAttempts(userAtempts);
        getHibernateTemplate().saveOrUpdate(npUser);

        //saving roles for specific user

        role1 = "";
        for (String role : commaSeparatedRoles)
        {
            List<NpRole> listOfNpRoles = (List<NpRole>) getHibernateTemplate().find("from NpRoleImpl where roleName= ?", role);
            if (listOfNpRoles.size() > 0)
            {
                role1 = role;
                NpRole npRole = listOfNpRoles.get(0);
                NpUserRole npUserRoles = new NpUserRole();
                NpUserRoleId npUserRolesId = new NpUserRoleId();
                npUserRolesId.setUserId(npUser.getId());
                npUserRolesId.setRoleId(npRole.getId());
                npUserRoles.setId(npUserRolesId);
                LOG.info("saving user Roles Table with role: " + npRole.getRoleName());
                getHibernateTemplate().saveOrUpdate(npUserRoles);
            }
        }

        //setting the Usertype to the user on basis of roles
        List<NpUserTypes> listOfNpUserTypes = null;

        if (StringUtils.containsIgnoreCase(role1, "candidate"))
        {

            listOfNpUserTypes = (List<NpUserTypes>) getHibernateTemplate().find("from NpUserTypes where id= ?", 1);
        }
        else if (StringUtils.containsIgnoreCase(role1, "np") || StringUtils.containsIgnoreCase(role1, "Liason") || StringUtils.contains(role1, "ADMIN"))
        {

            listOfNpUserTypes = (List<NpUserTypes>) getHibernateTemplate().find("from NpUserTypes where id= ?", 2);
        }
        else
        {

            listOfNpUserTypes = (List<NpUserTypes>) getHibernateTemplate().find("from NpUserTypes where id= ?", 3);
        }
        npUser.setNpUserTypes((NpUserTypes) listOfNpUserTypes.get(0));

        getHibernateTemplate().saveOrUpdate(npUser);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getNpRoles()
    {
        List<String> listOfRoles = (List<String>) getHibernateTemplate()
                .find("select roleName from NpRoleImpl where id not in (2,11)");
        LOG.info("Returning List Of Roles : " + listOfRoles.size());

        return listOfRoles;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpUser> getNpUsers()
    {
        return (List<NpUser>) getHibernateTemplate().find("from NpUserImpl where UserStatusID<3");
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void deleteNpUser(ReasonForm reasonForm)
    {
        NpUser npUser = (NpUserImpl) getHibernateTemplate().find("from NpUserImpl where id=?", Integer.parseInt(reasonForm.getUserId())).get(0);
        NpUserStatus npUserStatus = new NpUserStatusImpl();
        npUserStatus.setId(3);
        npUser.setNpUserStatus(npUserStatus);
        getHibernateTemplate().update(npUser);
        List<CommonUserActivationStatus> commanUserActivationStatusList = (List<CommonUserActivationStatus>) getHibernateTemplate().find("from CommonUserActivationStatus where UserID=?", Integer.parseInt(reasonForm.getUserId()));

        if (commanUserActivationStatusList.size() > 0)
        {
            CommonUserActivationStatus commanUserActivationStatus = commanUserActivationStatusList.get(0);
            commanUserActivationStatus.setComments(reasonForm.getReasonLines());
            commanUserActivationStatus.setNpUser((NpUserImpl) npUser);
            commanUserActivationStatus.setTermReason(npUsersReasonDao.getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commanUserActivationStatus.setNpUserStatus((NpUserStatusImpl) npUserStatus);
            getHibernateTemplate().saveOrUpdate(commanUserActivationStatus);
        }
        else
        {
            CommonUserActivationStatus commanUserActivationStatus = new CommonUserActivationStatus();
            commanUserActivationStatus.setComments(reasonForm.getReasonLines());
            commanUserActivationStatus.setNpUser((NpUserImpl) npUser);
            commanUserActivationStatus.setTermReason(npUsersReasonDao.getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commanUserActivationStatus.setNpUserStatus((NpUserStatusImpl) npUserStatus);
            getHibernateTemplate().save(commanUserActivationStatus);
        }
    }

    @Override
    public NpUser getNpUsersForId(String id)
    {
        NpUser npUser = (NpUser) getHibernateTemplate().find("from NpUserImpl where id=?", Integer.parseInt(id)).get(0);
        return npUser;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void saveAndUpdateNpUsers(NpClientSupportAddEditUserForm clientSupportAddEditUserForm)
    {
        NpUser npUser = (NpUser) getHibernateTemplate().find("from NpUserImpl where id=?", Integer.parseInt(clientSupportAddEditUserForm.getUserId())).get(0);
        npUser.setFirstName(clientSupportAddEditUserForm.getFirstName());
        npUser.setLastName(clientSupportAddEditUserForm.getLastName());
        npUser.setPrimaryEmail(clientSupportAddEditUserForm.getPrimaryEmailAddress());
        npUser.setPhone(clientSupportAddEditUserForm.getMobile());
        npUser.setOfficeTelephone(clientSupportAddEditUserForm.getOfficeTelePhone());
        getHibernateTemplate().update(npUser);

        List<NpUserRole> npUserRoleList = (List<NpUserRole>) getHibernateTemplate().find("from NpUserRole where userId=?", npUser.getId());
        for (NpUserRole npUserRole : npUserRoleList)
        {
            getHibernateTemplate().delete(npUserRole);
        }

        String[] commaSeparatedRoles = clientSupportAddEditUserForm.getCurrentRole().split(",");
        for (String role : commaSeparatedRoles)
        {
            List<NpRole> listOfNpRoles = (List<NpRole>) getHibernateTemplate().find("from NpRoleImpl where roleName= ?", role);
            if (listOfNpRoles.size() > 0)
            {
                NpRole npRoles = listOfNpRoles.get(0);

                List<NpUserRole> listOfNpUserRoles = (List<NpUserRole>) getHibernateTemplate().find("from NpUserRole where roleId = ? AND userId = ?", npRoles.getId(), npUser.getId());
                if (listOfNpUserRoles.size() == 0)
                {
                    NpUserRole npUserRoles = new NpUserRole();
                    NpUserRoleId npUserRolesId = new NpUserRoleId();
                    npUserRolesId.setUserId(npUser.getId());
                    npUserRolesId.setRoleId(npRoles.getId());
                    npUserRoles.setId(npUserRolesId);
                    LOG.info("saving user Roles Table with role: " + npRoles.getRoleName());
                    getHibernateTemplate().saveOrUpdate(npUserRoles);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public void UpdateNpUsersStatus(ReasonForm reasonForm)
    {
        NpUser npUser = (NpUser) getHibernateTemplate().find("from NpUserImpl where id=?", Integer.parseInt(reasonForm.getUserId())).get(0);
        NpUserStatus npUserStatus = new NpUserStatusImpl();
        npUserStatus.setId(reasonForm.getStatusId());
        /*if(npUser.getNpUserStatus().getId()==1)
		{
			npUserStatus.setId(2);
		}
		else{
			npUserStatus.setId(1);
		}*/
        npUser.setNpUserStatus(npUserStatus);
        getHibernateTemplate().update(npUser);
        List<CommonUserActivationStatus> commanUserActivationStatusList = (List<CommonUserActivationStatus>) getHibernateTemplate().find("from CommonUserActivationStatus where UserID=?", Integer.parseInt(reasonForm.getUserId()));
        if (commanUserActivationStatusList.size() > 0)
        {
            CommonUserActivationStatus commanUserActivationStatus = commanUserActivationStatusList.get(0);
            commanUserActivationStatus.setComments(reasonForm.getReasonLines());
            commanUserActivationStatus.setNpUser((NpUserImpl) npUser);
            commanUserActivationStatus.setTermReason(npUsersReasonDao.getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commanUserActivationStatus.setNpUserStatus((NpUserStatusImpl) npUserStatus);
            getHibernateTemplate().saveOrUpdate(commanUserActivationStatus);
        }
        else
        {
            CommonUserActivationStatus commanUserActivationStatus = new CommonUserActivationStatus();
            commanUserActivationStatus.setComments(reasonForm.getReasonLines());
            commanUserActivationStatus.setNpUser((NpUserImpl) npUser);
            commanUserActivationStatus.setTermReason(npUsersReasonDao.getReasonForId(Integer.parseInt(reasonForm.getReasonCode())));
            commanUserActivationStatus.setNpUserStatus((NpUserStatusImpl) npUserStatus);
            getHibernateTemplate().save(commanUserActivationStatus);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getSortedNpUsers(String sortBy, String pageNumber)
    {
		/*List<NpUser> npUsers=null;
		if(sortBy.equalsIgnoreCase("emailId"))
		{
		 npUsers=(List<NpUser>) getHibernateTemplate().find("from NpUserImpl where UserStatusID<3  order by PrimaryEmailAddress");
		 
		}
		
		if(sortBy.equalsIgnoreCase("firstName"))
		{
			 npUsers=(List<NpUser>) getHibernateTemplate().find("from NpUserImpl where UserStatusID<3 order by FirstName");
		}
		if(sortBy.equalsIgnoreCase("userType"))
		{
		     npUsers=(List<NpUser>) getHibernateTemplate().find("from NpUserImpl where UserStatusID<3 order by UserTypeID ");
		 
		}
		if(sortBy.equalsIgnoreCase("clientName")) {
			npUsers = new ArrayList<NpUser>();
		}*/

        List userList = new ArrayList();

        String query = "from NpUserImpl U ";

        query = query + " where U.npUserStatus.id !=3 ";

        if (StringUtils.isNotBlank(sortBy) && !StringUtils.equalsIgnoreCase(sortBy, "undefined"))
        {
            if (sortBy.equalsIgnoreCase("firstName"))
            {
                sortBy = "U.firstName";
            }

            if (sortBy.equalsIgnoreCase("emailId"))
            {
                sortBy = "U.primaryEmail";
            }
            if (sortBy.equalsIgnoreCase("userType"))
            {
                sortBy = "U.npUserTypes.userType";
            }
            query = query + "  order by " + sortBy;
        }

        Long npUserCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        userList.add(getPaginatedresult(query, pageNo));
        userList.add((int) npUserCount.longValue());

        return userList;
    }

    @SuppressWarnings("unchecked")
    private List<NpUserImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpUserImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @Override
    public List<NpUserStatus> fetchAllUserStatus()
    {
        // TODO Auto-generated method stub
        return getHibernateTemplate().loadAll(NpUserStatus.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpClientUser getNpClientUserByName(String name)
    {

        LOG.info("executing dao to find ClientUser");

        List<NpClientUser> listNpClientUser = (List<NpClientUser>) getHibernateTemplate().find("from NpClientUserImpl cu where cu.primaryEmail=?", name);
        try
        {
            NpClientUser user = listNpClientUser.get(0);
            System.out.println("executing dao" + user.getOrganization().getCompanyName());
            return user;
        }
        catch (Exception e)
        {
            throw new UserNotFoundException("Client User Not exist");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveClientUser(NpClientUser clientUser, String userRoles)
    {

        Integer userId = 0;
        if (clientUser.getId() == null)
        {
            System.out.println("saving");
            userId = (Integer) getHibernateTemplate().save(clientUser);
        }
        else
        {
            userId = clientUser.getId();
            Set<NpUserRole> userRoleSet = clientUser.getNpUserRole();
            List<NpUserRole> roleList = new ArrayList<NpUserRole>(userRoleSet);
            for (NpUserRole urole : roleList)
            {
                getHibernateTemplate().delete(urole);
            }
        }
        LOG.info("@@@@@@@@@@@@@@@@@@  user id : " + userId);
        clientUser.setId(userId);
        /**
         *
         * Deleting the existing Roles
         *
         */

        String[] commaSeparatedRoles = userRoles.split(",");
        for (String role : commaSeparatedRoles)
        {
            List<NpRole> listOfNpRoles = (List<NpRole>) getHibernateTemplate().find("from NpRoleImpl where roleName= ?", role);
            if (listOfNpRoles.size() > 0)
            {
                NpRole npRoles = listOfNpRoles.get(0);

                List<NpUserRole> listOfNpUserRoles = (List<NpUserRole>) getHibernateTemplate().find("from NpUserRole where roleId = ? AND userId = ?", npRoles.getId(), userId);
                if (listOfNpUserRoles.size() == 0)
                {
                    NpUserRole npUserRoles = new NpUserRole();
                    NpUserRoleId npUserRolesId = new NpUserRoleId();
                    npUserRolesId.setUserId(userId);
                    npUserRolesId.setRoleId(npRoles.getId());
                    npUserRoles.setId(npUserRolesId);
                    getHibernateTemplate().saveOrUpdate(npUserRoles);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getNpClientRoles()
    {

        List<String> listOfRoles = (List<String>) getHibernateTemplate()
                .find("select roleName from NpRoleImpl where id in (1,7,8,9,10)");
        LOG.info("Returning List Of Roles : " + listOfRoles.size());

        return listOfRoles;
    }
}
