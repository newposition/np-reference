package com.newposition.client.service;

import java.util.ArrayList;
import java.util.List;

import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientCostCentreStatus;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.forms.AddClientCostCentreForm;

public interface NpClientCostCentreService
{

    void saveCostCentre(AddClientCostCentreForm costCentreForm, String name);

    NpClientCostCentreStatus findCostCentreStatusByStatusCode(String status);

    List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClient(String userName);

    NpClientCostCentreImpl findCostCentreById(String id);

    NpClientCostCentre findCostCentreByCode(String code);

    void deleteCostCentre(String id);

    List<NpClientCostCentreImpl> getSortedListOfCostCentre(
            ArrayList<NpClientCostCentreImpl> costCentreList, String sortby);

    NpClientCostCentreImpl getCostCentreLightWeightData(
            NpClientCostCentreImpl _costCentre);

    List<NpClientCostCentreImpl> getLightList(
            List<NpClientCostCentreImpl> costCentres);

    void updateCostCentre(AddClientCostCentreForm costCentreForm);

    String checkCostCentreCode(String str, NpClientImpl npClient);

    Integer getCostCentreCount(String name);

    List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClientWithVideo(
            String userName);

    List<String> getStringList(String str,
                               List<NpClientCostCentreImpl> costCentres);

    List<NpClientCostCentreImpl> getClientCostCentreByFilter(String str1, String str2,
                                                             List<NpClientCostCentreImpl> costCentres);
}
