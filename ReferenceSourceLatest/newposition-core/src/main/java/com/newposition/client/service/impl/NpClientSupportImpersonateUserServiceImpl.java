package com.newposition.client.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.newposition.client.dao.NpClientSupportImpersonateUserDao;
import com.newposition.client.service.NpClientSupportImpersonateUserService;
import com.newposition.common.domain.NpUser;

/**
 * @author Sachi N
 */

@Service
public class NpClientSupportImpersonateUserServiceImpl implements NpClientSupportImpersonateUserService
{

    private NpClientSupportImpersonateUserDao clientSupportImpersonateUserDao;

    public NpClientSupportImpersonateUserDao getClientSupportImpersonateUserDao()
    {
        return clientSupportImpersonateUserDao;
    }

    public void setClientSupportImpersonateUserDao(
            NpClientSupportImpersonateUserDao clientSupportImpersonateUserDao)
    {
        this.clientSupportImpersonateUserDao = clientSupportImpersonateUserDao;
    }

    @Override
    public List<NpUser> getListOfNpUser()
    {

        List<NpUser> listOfNpUsers = clientSupportImpersonateUserDao.getListOfNpUser();
        return listOfNpUsers;
    }

    @Override
    public List getSortedNpUsers(String sortBy, String pageNumber, String filterBy)
    {

        List listOfSortedNpUsers = clientSupportImpersonateUserDao.getSortedNpUsers(sortBy, pageNumber, filterBy);
        return listOfSortedNpUsers;
    }

    @Override
    public NpUser getUserById(int userId)
    {

        NpUser npUser = clientSupportImpersonateUserDao.getUserById(userId);
        return npUser;
    }
}
