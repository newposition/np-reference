package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserStatus;

public interface NpClientSupportAddEditUserDao
{
    public void saveClientSupportAddEditUser(NpClientSupportAddEditUserForm clientSupportAddEditUserForm);

    public void deleteNpUser(ReasonForm reasonForm);

    public NpUser getNpUsersForId(String id);

    public void saveAndUpdateNpUsers(NpClientSupportAddEditUserForm clientSupportAddEditUserForm);

    void UpdateNpUsersStatus(ReasonForm reasonForm);

    public List getSortedNpUsers(String sortBy, String number);

    public List<String> getNpRoles();

    public List<NpUser> getNpUsers();

    public List<NpUserStatus> fetchAllUserStatus();

    public NpClientUser getNpClientUserByName(String name);

    public void saveClientUser(NpClientUser clientUser, String userRoles);

    public List<String> getNpClientRoles();
}
