package com.newposition.client.domain;

import java.io.Serializable;
import java.util.Date;

public interface NpClientCostCentre extends Serializable
{

    public Long getCostCentreId();

    public void setCostCentreId(Long id);

    public String getCostCentreOwner();

    public void setCostCentreOwner(String costCentreOwner);

    public String getCostCentreName();

    public void setCostCentreName(String costCentreName);

    public String getCostCentreLocation();

    public void setCostCentreLocation(String costCentreLocation);

    public String getCostCentreSystem();

    public void setCostCentreSystem(String costCentreSystem);

    public Date getLastActiveDate();

    public void setLastActiveDate(Date lastActiveDate);

    public String getStatus();

    public void setStatus(String status);

    public NpClientCostCentreStatus getCostCentreStatus();

    public void setCostCentreStatus(NpClientCostCentreStatus costCentreStatus);

    public NpClientImpl getNpClient();

    public void setNpClient(NpClientImpl npClient);

    public String getCostCentreCode();

    public void setCostCentreCode(String costCentreCode);

    String getDescription();

    void setDescription(String description);

    NpClientCostCentreDocument getCostCentreDoc();

    void setCostCentreDoc(NpClientCostCentreDocument costCentreDoc);
}

