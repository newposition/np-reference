/**
 *
 */
package com.newposition.client.forms;

/**
 * @author Trinadh
 */
public class ReasonForm
{

    private String userId;
    private String reasonCode;
    private String reasonLines;
    private boolean suspendOrDelete;
    private Integer statusId;

    public Integer getStatusId()
    {
        return statusId;
    }

    public void setStatusId(Integer statusId)
    {
        this.statusId = statusId;
    }

    public boolean isSuspendOrDelete()
    {
        return suspendOrDelete;
    }

    public void setSuspendOrDelete(boolean suspendOrDelete)
    {
        this.suspendOrDelete = suspendOrDelete;
    }

    public String getReasonCode()
    {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode)
    {
        this.reasonCode = reasonCode;
    }

    public String getReasonLines()
    {
        return reasonLines;
    }

    public void setReasonLines(String reasonLines)
    {
        this.reasonLines = reasonLines;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }
}
