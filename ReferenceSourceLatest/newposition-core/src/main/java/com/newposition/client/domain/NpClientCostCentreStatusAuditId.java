package com.newposition.client.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NpClientCostCentreStatusAuditId implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "CostCentreID", nullable = false)
    private long costCentreID;

    @Column(name = "ModifiedDate", nullable = false)
    private Date modifiedDate;

    public long getCostCentreID()
    {
        return costCentreID;
    }

    public void setCostCentreID(int costCentreID)
    {
        this.costCentreID = costCentreID;
    }

    public Date getModifiedDate()
    {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
