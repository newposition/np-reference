package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.domain.NpClientProjectDocument;

public interface NpProjectDao
{

    void saveProject(NpClientProject npProject, NpClientProjectDocument prodoc);

    List<NpClientProject> getAllClientProjects(NpClientImpl npClient);

    NpClientProject getNpProjectById(int id);

    void deleteProject(NpClientProject npClientProject);

    NpClientProject updateNpClientProject(NpClientProject npClientProject);
}
