package com.newposition.client.service;

import java.util.ArrayList;
import java.util.List;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;
import com.newposition.common.domain.CommonBean;

public interface NpClientOfficeLocationService
{

    List<NpClientOfficeLocation> getAllClientOfficeLocations(
            NpClientImpl npClient);

    NpClientOfficeLocation getnpClientOfficeLocationServiceById(int id);

    void saveNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation,
            CoLocations coLocations, CoCities coCities, CoCounty coCounty,
            CoLocationState coLocationState, CoZip coZip, CoCountry coCountry);

    List<CommonBean> getCoLocationsListByAddress1(String str1);

    List<CommonBean> getCoLocationsListByAddress2(String str1);

    List<CommonBean> getCoLocationsListByAddress3(String str1);

    List<CommonBean> getCoCitiesList(String str1);

    List<CommonBean> getCoCountyList(String str1);

    List<CommonBean> getCoCountryList(String str1);

    List<CoZip> getCoStateListByName(String str1);

    List<CoZip> getCoStateListById(String str1);

    List<CommonBean> getCommonBeanList(String str1, String str2);

    List<CoZip> getZipList(String str1, String str2);

    List<NpClientOfficeLocation> deleteOfficeLocation(int id,
                                                      NpClientImpl npClient);

    List<NpClientOfficeLocation> getSortedList(
            ArrayList<NpClientOfficeLocation> officeLocations, String str);

    List<String> getAllCountries();

    List<String> getStateListByCountryName(String str);

    List<String> getCitesByStateAndCountry(String str1, String str2);

    List<NpClientOfficeLocation> getLocationsByStateAndCountryAndCity(
            String str1, String str2, String str3, NpClientImpl npClient);

    List<NpClientOfficeLocation> getLocationListByCountryName(String str);

    List<NpClientOfficeLocation> getLocationListByStateAndCountry(String str1,
                                                                  String str2);

    CoLocations saveCoLocations(CoCities coCities, CoCounty coCounty,
                                CoCountry coCountry, CoZip coZip, CoLocations coLocations);

    CoZip saveCoZip(CoLocationState coLocationState, CoZip coZip);

    CoLocationState saveCoLocationState(CoLocationState coLocationState);

    CoCountry saveCoCountry(CoCountry coCountry);

    CoCounty saveCoCounty(CoCounty coCounty);

    CoCities saveCoCities(CoCities coCities);
}
