package com.newposition.client.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientUserDao;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.common.dao.impl.GenericDaoImpl;

public class NpClientUserDaoImpl extends GenericDaoImpl<NpClientUser, Integer> implements NpClientUserDao
{


    /*private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
	this.hibernateTemplate = hibernateTemplate;
    }
    
    public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}*/

    public List<NpClientUser> fetchAllClientUsersByClientId(Integer id)
    {
        @SuppressWarnings("unchecked")
        List<NpClientUser> clientUsers = (List<NpClientUser>) getHibernateTemplate().findByNamedParam(
                "from com.newposition.client.domain.NpClientUser CU WHERE CU.organization=:id",
                "id", id);
        return clientUsers;
    }

    @Override
    public List<NpClientUser> findAll()
    {
        return getHibernateTemplate().loadAll(NpClientUser.class);
    }

    @Override
    public NpClientUser findByID(Integer id)
    {
        return getHibernateTemplate().get(NpClientUserImpl.class, id);
    }
}
