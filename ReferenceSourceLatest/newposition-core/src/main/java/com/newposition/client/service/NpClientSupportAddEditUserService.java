package com.newposition.client.service;

import java.util.List;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.NpClientSupportAddEditUserForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserStatus;

public interface NpClientSupportAddEditUserService
{

    public void saveClientSupportAddEditUser(NpClientSupportAddEditUserForm clientSupportAddEditUserForm);

    public List<String> getNpRoles();

    public List<NpUser> getNpUsers();

    public void deleteNpuser(ReasonForm reasonForm);

    public NpUser getNpUsersForId(String id);

    public void saveAndUpdateNpUser(NpClientSupportAddEditUserForm clientSupportAddEditUserForm);

    public void updateNpuserStatus(ReasonForm reasonForm);

    public List getSortedNpUsers(String sortBy, String pageNumber);

    List<NpUserStatus> fetchAllUserStatus();

    void sendAccountDisabledMail(ReasonForm reasonForm, String subject);

    public NpClientUser getCompanyOfCurrentUser(String name);

    public void saveClientUser(String userName,
                               NpClientSupportAddEditUserForm clientSupportAddEditUserForm);

    public List<String> getNpClientRoles();

    void sendAccountActivationMail(
            NpClientSupportAddEditUserForm clientSupportAddEditUserForm, String clientSupportAddEditUseractiveLink);
}

