package com.newposition.client.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientRoleManagementDao;
import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.common.domain.CoGroups;
import com.newposition.common.domain.CoRoleGroups;
import com.newposition.common.domain.CoRoleGroupsId;
import com.newposition.common.domain.NpRole;

public class NpClientRoleManagementDaoImpl implements NpClientRoleManagementDao
{

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoGroups> getListOfClientGroupsById(int id, String sortBy)
    {
        // TODO Auto-generated method stub

        String query = "from CoGroups where ClientId=" + id;
        if (StringUtils.isNotBlank(sortBy))
        {
            query = query + "  order by " + sortBy;
        }
        return (List<CoGroups>) getHibernateTemplate().find(query);
    }

    @SuppressWarnings("unchecked")
    @Override
    public int saveGroups(NpClientAddRoleGroupForm npClientAddRoleGroupForm, int clientId, String userName)
    {
        // TODO Auto-generated method stub

        CoGroups coGroup = new CoGroups();
        coGroup.setClientId(clientId);
        coGroup.setGroupName(npClientAddRoleGroupForm.getGroupName());
        coGroup.setGroupDescription(npClientAddRoleGroupForm.getGroupDescription());
        coGroup.setCreatedBy(userName);

        Integer sid = 0;
        if (npClientAddRoleGroupForm.getGroupId() != null)
        {
            sid = Integer.parseInt(npClientAddRoleGroupForm.getGroupId());
            coGroup.setId(sid);
            coGroup.setModifiedBy(userName);
            getHibernateTemplate().update(coGroup);
        }
        else
        {
            sid = (Integer) getHibernateTemplate().save(coGroup);
        }

        List<CoRoleGroups> listOfRoleGroups = (List<CoRoleGroups>) getHibernateTemplate().find("from CoRoleGroups where groupId = ?", sid);

        if (listOfRoleGroups.size() > 0)
        {
            for (CoRoleGroups coRoleGroups : listOfRoleGroups)
            {
                getHibernateTemplate().delete(coRoleGroups);
            }
        }
        String[] commaSeparatedRoles = npClientAddRoleGroupForm.getCurrentRole().split(",");
        for (String role : commaSeparatedRoles)
        {
            List<NpRole> listOfNpRoles = (List<NpRole>) getHibernateTemplate().find("from NpRoleImpl where roleName= ?", role);

            if (listOfNpRoles.size() > 0)
            {
                NpRole npRoles = listOfNpRoles.get(0);

                List<CoRoleGroups> listOfCoRoleGroups = (List<CoRoleGroups>) getHibernateTemplate().find("from CoRoleGroups where roleId = ? AND groupId = ?", npRoles.getId(), sid);
                if (listOfCoRoleGroups.size() == 0)
                {

                    CoRoleGroups coRoleGroups = new CoRoleGroups();
                    CoRoleGroupsId coRoleGroupsId = new CoRoleGroupsId();
                    coRoleGroupsId.setGroupId(sid);
                    coRoleGroupsId.setRoleId(npRoles.getId());
                    coRoleGroups.setId(coRoleGroupsId);
                    getHibernateTemplate().saveOrUpdate(coRoleGroups);
                }
            }
        }
        return sid;
    }

    @SuppressWarnings("unchecked")
    @Override
    public CoGroups getCoGroupById(int id)
    {
        // TODO Auto-generated method stub

        List<CoGroups> listOfCoGroups = (List<CoGroups>) getHibernateTemplate().find("from CoGroups where id = ?", id);

        if (listOfCoGroups.size() > 0)
        {
            return listOfCoGroups.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteGroupById(int id)
    {
        // TODO Auto-generated method stub

        List<CoGroups> listOfCoGroups = (List<CoGroups>) getHibernateTemplate().find("from CoGroups where id = ?", id);

        if (listOfCoGroups.size() > 0)
        {
            getHibernateTemplate().delete(listOfCoGroups.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public CoGroups getListOfRolesInClientGroups(int clientId, String groupName)
    {
        // TODO Auto-generated method stub
        List<CoGroups> listOfCoGroups = (List<CoGroups>) getHibernateTemplate().find("from CoGroups where clientId = ? and groupName = ?", clientId, groupName);

        if (listOfCoGroups.size() > 0)
        {
            return listOfCoGroups.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String checkClientGroupExistsOrNot(int clientId, String groupName)
    {
        // TODO Auto-generated method stub
        List<CoGroups> listOfCoGroups = (List<CoGroups>) getHibernateTemplate().find("from CoGroups where clientId = ? and groupName = ?", clientId, groupName);

        if (listOfCoGroups.size() > 0)
        {
            return "exists";
        }
        return null;
    }
}
