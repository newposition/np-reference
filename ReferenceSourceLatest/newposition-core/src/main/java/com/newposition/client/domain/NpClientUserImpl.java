package com.newposition.client.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.newposition.common.domain.NpUserImpl;

@Entity
@Table(name = "np_client_user", catalog = "npdbs")
@PrimaryKeyJoinColumn(name = "ID")
public class NpClientUserImpl extends NpUserImpl implements NpClientUser
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(targetEntity = NpClientImpl.class)
    @JoinColumn(name = "client_id", nullable = false)
    private NpClient organization;

    public NpClient getOrganization()
    {
        return organization;
    }

    public void setOrganization(NpClient organization)
    {
        this.organization = organization;
    }
}
