package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.common.domain.CoGroups;

public interface NpClientRoleManagementDao
{

    public List<CoGroups> getListOfClientGroupsById(int id, String sortBy);

    public int saveGroups(NpClientAddRoleGroupForm npClientAddRoleGroupForm, int clientId, String userName);

    public CoGroups getCoGroupById(int id);

    public void deleteGroupById(int id);

    public CoGroups getListOfRolesInClientGroups(int clientId, String groupName);

    public String checkClientGroupExistsOrNot(int clientId, String groupName);
}
