package com.newposition.client.service;

import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.ClientMarketingForm;

public interface NpClientCompanyMarketingService
{

    void saveClientMarketingForm(NpClient client, ClientMarketingForm clientMarketingForm);
}
