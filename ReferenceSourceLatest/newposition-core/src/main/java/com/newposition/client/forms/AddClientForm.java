package com.newposition.client.forms;

import java.io.File;

public class AddClientForm
{

    private Integer id;

    private String companyName;

    private String companyLogo;

    private Integer sectorId;

    private String firstName;

    private String lastName;

    private String principleContactEmail;

    private String teleNumber;

    private String mobileNumber;

    private String address1;

    private String address2;

    private String town;

    private String country;

    private String postCode;

    private Integer adminId;

    private String adminFirstName;

    private String adminLastName;

    private String adminEmail;

    private String adminPassword;

    private String adminConfirmPassword;

    private Integer clientStatusId;

    private File companyImage;

    public File getCompanyImage()
    {
        return companyImage;
    }

    public void setCompanyImage(File companyImage)
    {
        this.companyImage = companyImage;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyLogo()
    {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo)
    {
        this.companyLogo = companyLogo;
    }

    public Integer getSectorId()
    {
        return sectorId;
    }

    public void setSectorId(Integer sectorId)
    {
        this.sectorId = sectorId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getTeleNumber()
    {
        return teleNumber;
    }

    public void setTeleNumber(String teleNumber)
    {
        this.teleNumber = teleNumber;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getAddress2()
    {
        return address2;
    }

    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(String town)
    {
        this.town = town;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getPostCode()
    {
        return postCode;
    }

    public void setPostCode(String postCode)
    {
        this.postCode = postCode;
    }

    public Integer getAdminId()
    {
        return adminId;
    }

    public void setAdminId(Integer adminId)
    {
        this.adminId = adminId;
    }

    public String getAdminFirstName()
    {
        return adminFirstName;
    }

    public void setAdminFirstName(String adminFirstName)
    {
        this.adminFirstName = adminFirstName;
    }

    public String getAdminLastName()
    {
        return adminLastName;
    }

    public void setAdminLastName(String adminLastName)
    {
        this.adminLastName = adminLastName;
    }

    public String getAdminEmail()
    {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail)
    {
        this.adminEmail = adminEmail;
    }

    public String getAdminPassword()
    {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword)
    {
        this.adminPassword = adminPassword;
    }

    public String getAdminConfirmPassword()
    {
        return adminConfirmPassword;
    }

    public void setAdminConfirmPassword(String adminConfirmPassword)
    {
        this.adminConfirmPassword = adminConfirmPassword;
    }

    public Integer getClientStatusId()
    {
        return clientStatusId;
    }

    public void setClientStatusId(Integer clientStatusId)
    {
        this.clientStatusId = clientStatusId;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPrincipleContactEmail()
    {
        return principleContactEmail;
    }

    public void setPrincipleContactEmail(String principleContactEmail)
    {
        this.principleContactEmail = principleContactEmail;
    }
}
