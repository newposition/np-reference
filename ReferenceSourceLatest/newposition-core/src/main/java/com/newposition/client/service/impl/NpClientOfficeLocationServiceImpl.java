package com.newposition.client.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.newposition.client.dao.NpClientOfficeLocationDao;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.client.service.NpClientOfficeLocationService;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;
import com.newposition.common.domain.CommonBean;

@Service
public class NpClientOfficeLocationServiceImpl implements NpClientOfficeLocationService
{

    private NpClientOfficeLocationDao npClientOfficeLocationDao;

    public NpClientOfficeLocationDao getNpClientOfficeLocationDao()
    {
        return npClientOfficeLocationDao;
    }

    public void setNpClientOfficeLocationDao(
            NpClientOfficeLocationDao npClientOfficeLocationDao)
    {
        this.npClientOfficeLocationDao = npClientOfficeLocationDao;
    }

    @Override
    @Transactional
    public List<NpClientOfficeLocation> getAllClientOfficeLocations(
            NpClientImpl npClient)
    {
        List<NpClientOfficeLocation> officeLocations = npClientOfficeLocationDao.getAllClientOfficeLocations(npClient);
        return officeLocations;
    }

    @Override
    @Transactional
    public NpClientOfficeLocation getnpClientOfficeLocationServiceById(int id)
    {
        NpClientOfficeLocation npClientOfficeLocation = npClientOfficeLocationDao.getnpClientOfficeLocationServiceById(id);
        return npClientOfficeLocation;
    }

    @Override
    @Transactional
    public void saveNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation,
            CoLocations coLocations, CoCities coCities, CoCounty coCounty,
            CoLocationState coLocationState, CoZip coZip, CoCountry coCountry)
    {
        coCities.setCityDescription(coCities.getCityDescription().toUpperCase());
        coCounty.setCountyDescription(coCounty.getCountyDescription().toUpperCase());
        coCountry.setCountryDescription(coCountry.getCountryDescription().toUpperCase());
        coLocationState.setStateID(coLocationState.getStateID().toUpperCase());
        coLocationState.setStateDescription(coLocationState.getStateDescription().toUpperCase());
        coLocations.setAddress1(coLocations.getAddress1().toUpperCase());
        coLocations.setAddress2(coLocations.getAddress2().toUpperCase());
        coLocations.setAddress3(coLocations.getAddress3().toUpperCase());
        saveCoCities(coCities);
        saveCoCounty(coCounty);
        saveCoCountry(coCountry);
        saveCoLocationState(coLocationState);
        saveCoZip(coLocationState, coZip);
        saveCoLocations(coCities, coCounty, coCountry, coZip, coLocations);
        System.out.println("coLocations=" + coLocations);
        npClientOfficeLocation.setCoLocations(coLocations);
        System.out.println("last1");
        npClientOfficeLocationDao.saveNpClientOfficeLocation(npClientOfficeLocation);
        System.out.println("last2");
    }

    public CoCities saveCoCities(CoCities coCities)
    {
        npClientOfficeLocationDao.saveCoCities(coCities);
        return coCities;
    }

    public CoCounty saveCoCounty(CoCounty coCounty)
    {
        npClientOfficeLocationDao.saveCoCounty(coCounty);
        return coCounty;
    }

    public CoCountry saveCoCountry(CoCountry coCountry)
    {
        npClientOfficeLocationDao.saveCoCountry(coCountry);
        return coCountry;
    }

    public CoLocationState saveCoLocationState(CoLocationState coLocationState)
    {
        System.out.println("coLocationState=" + coLocationState.getStateID());
        System.out.println("coLocationState=" + coLocationState.getStateDescription());
        npClientOfficeLocationDao.saveCoLocationState(coLocationState);
        System.out.println("last coLocationState=" + coLocationState.getStateID());
        System.out.println("last coLocationState=" + coLocationState.getStateDescription());
        return coLocationState;
    }

    public CoZip saveCoZip(CoLocationState coLocationState, CoZip coZip)
    {
        System.out.println("saveCoZip coLocationState=" + coLocationState.getStateID());
        System.out.println("saveCoZip coLocationState=" + coLocationState.getStateDescription());
        coZip.setCoLocationState(coLocationState);
        npClientOfficeLocationDao.saveCoZip(coZip);
        System.out.println("coZip=" + coZip.getzIP());
        return coZip;
    }

    public CoLocations saveCoLocations(CoCities coCities, CoCounty coCounty, CoCountry coCountry, CoZip coZip, CoLocations coLocations)
    {
        System.out.println("coZip=" + coZip.getzIP());
        coLocations.setCoCities(coCities);
        coLocations.setCoCountry(coCountry);
        coLocations.setCoCounty(coCounty);
        coLocations.setCoZip(coZip);
        npClientOfficeLocationDao.saveCoLocations(coLocations);
        System.out.println("coLocations=" + coLocations.getCoZip().getzIP());
        return coLocations;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoLocationsListByAddress1(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoLocations> coLocationList = npClientOfficeLocationDao.getCoLocationsList(str1);
        for (int i = 0; i < coLocationList.size(); i++)
        {
            CoLocations coLocations = coLocationList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coLocations.getLocationID());
            cb.setValue(coLocations.getAddress1());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoLocationsListByAddress2(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoLocations> coLocationList = npClientOfficeLocationDao.getCoLocationsList1(str1);
        for (int i = 0; i < coLocationList.size(); i++)
        {
            CoLocations coLocations = coLocationList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coLocations.getLocationID());
            cb.setValue(coLocations.getAddress2());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoLocationsListByAddress3(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoLocations> coLocationList = npClientOfficeLocationDao.getCoLocationsList2(str1);
        for (int i = 0; i < coLocationList.size(); i++)
        {
            CoLocations coLocations = coLocationList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coLocations.getLocationID());
            cb.setValue(coLocations.getAddress3());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoCitiesList(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoCities> citiesList = npClientOfficeLocationDao.getCoCitiesList(str1);
        for (int i = 0; i < citiesList.size(); i++)
        {
            CoCities coCities = citiesList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coCities.getCityID());
            cb.setValue(coCities.getCityDescription());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoCountyList(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoCounty> coCountyList = npClientOfficeLocationDao.getCoCountyList(str1);
        for (int i = 0; i < coCountyList.size(); i++)
        {
            CoCounty coCounty = coCountyList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coCounty.getCountyID());
            cb.setValue(coCounty.getCountyDescription());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCoCountryList(String str1)
    {
        List<CommonBean> commonList = new ArrayList<CommonBean>();
        List<CoCountry> coCountryList = npClientOfficeLocationDao.getCoCountryList(str1);
        for (int i = 0; i < coCountryList.size(); i++)
        {
            CoCountry coCountry = coCountryList.get(i);
            CommonBean cb = new CommonBean();
            cb.setId(coCountry.getCountryID());
            cb.setValue(coCountry.getCountryDescription());
            commonList.add(cb);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CoZip> getCoStateListByName(String str1)
    {
        List<CoLocationState> coStateList = npClientOfficeLocationDao.getCoStateList(str1);
        List<CoZip> zipList1 = npClientOfficeLocationDao.getAllZips();
        System.out.println("zipList1=" + zipList1.size());
        List<CoZip> zipList = new ArrayList<CoZip>();
        for (int i = 0; i < coStateList.size(); i++)
        {
            CoLocationState coLocationState = coStateList.get(i);
            for (int j = 0; j < zipList1.size(); j++)
            {
                CoZip coZip = zipList1.get(j);
                if (coLocationState.getStateID().equals(coZip.getCoLocationState().getStateID()))
                {
                    zipList.add(coZip);
                }
            }
        }
        return zipList;
    }

    @Override
    @Transactional
    public List<CoZip> getCoStateListById(String str1)
    {
        List<CoLocationState> coStateList = npClientOfficeLocationDao.getCoStateList1(str1);
        List<CoZip> zipList1 = npClientOfficeLocationDao.getAllZips();
        System.out.println("zipList1=" + zipList1.size());
        List<CoZip> zipList = new ArrayList<CoZip>();
        for (int i = 0; i < coStateList.size(); i++)
        {
            CoLocationState coLocationState = coStateList.get(i);
            for (int j = 0; j < zipList1.size(); j++)
            {
                CoZip coZip = zipList1.get(j);
                if (coLocationState.getStateID().equals(coZip.getCoLocationState().getStateID()))
                {
                    zipList.add(coZip);
                }
            }
        }
        return zipList;
    }

    @Override
    @Transactional
    public List<CommonBean> getCommonBeanList(String str1, String str2)
    {
        List<CommonBean> commonList = null;
        if (str2.equals("address1"))
        {
            return getCoLocationsListByAddress1(str1);
        }
        else if (str2.equals("address2"))
        {
            return getCoLocationsListByAddress2(str1);
        }
        else if (str2.equals("address3"))
        {
            return getCoLocationsListByAddress3(str1);
        }
        else if (str2.equals("city"))
        {
            return getCoCitiesList(str1);
        }
        else if (str2.equals("county"))
        {
            return getCoCountyList(str1);
        }
        else if (str2.equals("country"))
        {
            return getCoCountryList(str1);
        }
        return commonList;
    }

    @Override
    @Transactional
    public List<CoZip> getZipList(String str1, String str2)
    {
        List<CoZip> zips = null;
        if (str2.equals("stateId"))
        {
            return getCoStateListById(str1);
        }
        else if (str2.equals("state"))
        {
            return getCoStateListByName(str1);
        }
        return zips;
    }

    @Override
    @Transactional
    public List<NpClientOfficeLocation> deleteOfficeLocation(int id,
                                                             NpClientImpl npClient)
    {
        NpClientOfficeLocation npClientOfficeLocation = npClientOfficeLocationDao.getnpClientOfficeLocationServiceById(id);
        npClientOfficeLocationDao.deleteNpClientOfficeLocation(npClientOfficeLocation);
        List<NpClientOfficeLocation> officeLocations = npClientOfficeLocationDao.getAllClientOfficeLocations(npClient);
        return officeLocations;
    }

    @SuppressWarnings("static-access")
    @Override
    @Transactional
    public List<NpClientOfficeLocation> getSortedList(
            ArrayList<NpClientOfficeLocation> officeLocations, String str)
    {
        NpClientOfficeLocation npClientOfficeLocation = new NpClientOfficeLocation();
        if (str.equals("address1"))
        {
            npClientOfficeLocation.sortByAddress1(officeLocations);
        }
        else if (str.equals("address2"))
        {
            npClientOfficeLocation.sortByAddress2(officeLocations);
        }
        else if (str.equals("City"))
        {
            npClientOfficeLocation.sortByCity(officeLocations);
        }
        else if (str.equals("County"))
        {
            npClientOfficeLocation.sortByCounty(officeLocations);
        }
        else if (str.equals("State"))
        {
            npClientOfficeLocation.sortByState(officeLocations);
        }
        else if (str.equals("Country"))
        {
            npClientOfficeLocation.sortByCountry(officeLocations);
        }
        return officeLocations;
    }

    @Override
    @Transactional
    public List<String> getAllCountries()
    {
        List<String> countries = new ArrayList<String>();
        List<CoCountry> countryList = npClientOfficeLocationDao.getAllCountries();
        for (int i = 0; i < countryList.size(); i++)
        {
            countries.add(countryList.get(i).getCountryDescription());
        }
        List<String> countries1 = new ArrayList<String>(new HashSet<String>(countries));
        return countries1;
    }

    @Override
    @Transactional
    public List<String> getStateListByCountryName(String str)
    {
        List<String> states = new ArrayList<String>();
        List<CoLocations> colocationList = npClientOfficeLocationDao.getAllCoLocations(str);
        for (int i = 0; i < colocationList.size(); i++)
        {
            states.add(colocationList.get(i).getCoZip().getCoLocationState().getStateDescription());
        }
        List<String> states1 = new ArrayList<String>(new HashSet<String>(states));
        return states1;
    }

    @Override
    public List<NpClientOfficeLocation> getLocationListByCountryName(String str)
    {
        List<NpClientOfficeLocation> list = npClientOfficeLocationDao.getLocationListByCountryName(str);
        return list;
    }

    @Override
    public List<NpClientOfficeLocation> getLocationListByStateAndCountry(
            String str1, String str2)
    {
        System.out.println("in service getLocationListByStateAndCountry str1=" + str1 + ", str2=" + str2);
        List<NpClientOfficeLocation> list1 = new ArrayList<NpClientOfficeLocation>();
        List<NpClientOfficeLocation> list = npClientOfficeLocationDao.getLocationListByCountryName(str1, str2);
        for (int i = 0; i < list.size(); i++)
        {
            NpClientOfficeLocation npClientOfficeLocation = list.get(i);
            if (npClientOfficeLocation.getCoLocations().getCoCountry().getCountryDescription().equals(str1) && npClientOfficeLocation.getCoLocations().getCoZip().getCoLocationState().getStateDescription().equals(str2))
            {
                list1.add(npClientOfficeLocation);
            }
        }
        System.out.println("service list size=" + list.size());
        return list1;
    }

    @Override
    @Transactional
    public List<String> getCitesByStateAndCountry(String str1, String str2)
    {
        List<String> cities = new ArrayList<String>();
        List<CoLocations> colocationList = npClientOfficeLocationDao.getAllCoLocations(str1, str2);
        for (int i = 0; i < colocationList.size(); i++)
        {
            cities.add(colocationList.get(i).getCoCities().getCityDescription());
        }
        List<String> cities1 = new ArrayList<String>(new HashSet<String>(cities));
        return cities1;
    }

    @Override
    @Transactional
    public List<NpClientOfficeLocation> getLocationsByStateAndCountryAndCity(
            String str1, String str2, String str3, NpClientImpl npClient)
    {
        List<NpClientOfficeLocation> officeLocations = new ArrayList<NpClientOfficeLocation>();
        List<NpClientOfficeLocation> officeLocations1 = npClientOfficeLocationDao.getAllClientOfficeLocations(npClient);
        for (int i = 0; i < officeLocations1.size(); i++)
        {
            NpClientOfficeLocation npClientOfficeLocation = officeLocations1.get(i);
            if (str1.equals(npClientOfficeLocation.getCoLocations().getCoCountry().getCountryDescription()) && str2.equals(npClientOfficeLocation.getCoLocations().getCoZip().getCoLocationState().getStateDescription()) && str3.equals(npClientOfficeLocation.getCoLocations().getCoCities().getCityDescription()))
            {
                officeLocations.add(npClientOfficeLocation);
            }
        }
        return officeLocations;
    }
}