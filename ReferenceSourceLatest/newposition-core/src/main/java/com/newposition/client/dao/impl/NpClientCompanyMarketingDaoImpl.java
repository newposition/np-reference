package com.newposition.client.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientCompanyMarketingDao;
import com.newposition.client.domain.NpClClientDocuments;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.client.forms.ClientMarketingForm;
import com.newposition.common.domain.NpDocumentTypes;

public class NpClientCompanyMarketingDaoImpl implements NpClientCompanyMarketingDao
{

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

	

	
	
	
    
   
/*
    @Override
	public NpClient findByUserName(Integer userId) {
		 List<NpClientUserImpl> clientUsers = (List<NpClientUserImpl>) getHibernateTemplate().find("from NpClientUserImpl C WHERE C.id=?", userId);
		 
		 if (clientUsers.size() > 0) {
			 return (NpClient) clientUsers.get(0).getOrganization();
		 }
		 
		return null;
		
	}*/

    @Override
    public void saveClientMarketingForm(NpClient client, NpClClientDocuments npClClientDocuments)
    {
        List<NpClientImpl> npClient = (List<NpClientImpl>) getHibernateTemplate().find("from NpClientImpl where id = ?", client.getId());

        npClient.get(0).setClientOverview(client.getClientOverview());

        getHibernateTemplate().saveOrUpdate(npClient.get(0));

        getHibernateTemplate().save(npClClientDocuments);
    }
}
