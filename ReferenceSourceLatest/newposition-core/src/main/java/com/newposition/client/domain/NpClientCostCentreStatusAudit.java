package com.newposition.client.domain;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.newposition.common.domain.NpUsersReason;

public class NpClientCostCentreStatusAudit implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "CostCentreID", column = @Column(name = "CostCentreID", nullable = false)),
            @AttributeOverride(name = "ModifiedDate", column = @Column(name = "ModifiedDate", nullable = false))})
    private NpClientCostCentreStatusAuditId id;

    @Column(name = "Status")
    private String status;

    @ManyToOne(targetEntity = NpUsersReason.class)
    @JoinColumn(name = "ReasonCodeID", nullable = false)
    private NpUsersReason reasonCode;

    @ManyToOne(targetEntity = NpClientCostCentreStatus.class)
    @JoinColumn(name = "CostCenterStatusID", nullable = false)
    private NpClientCostCentreStatus costCentreStatus;

    @Column(name = "Comments")
    private String comments;

    public NpClientCostCentreStatusAuditId getId()
    {
        return id;
    }

    public void setId(NpClientCostCentreStatusAuditId id)
    {
        this.id = id;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public NpUsersReason getReasonCode()
    {
        return reasonCode;
    }

    public void setReasonCode(NpUsersReason reasonCode)
    {
        this.reasonCode = reasonCode;
    }

    public NpClientCostCentreStatus getCostCentreStatus()
    {
        return costCentreStatus;
    }

    public void setCostCentreStatus(NpClientCostCentreStatus costCentreStatus)
    {
        this.costCentreStatus = costCentreStatus;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }
}
