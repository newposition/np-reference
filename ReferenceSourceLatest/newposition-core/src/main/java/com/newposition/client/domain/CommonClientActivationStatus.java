package com.newposition.client.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import com.newposition.common.domain.NpUsersReason;

public interface CommonClientActivationStatus extends java.io.Serializable
{

    public int getClientActivationId();

    public void setClientActivationId(int clientActivationId);

    public NpClient getNpClient();

    public void setNpClient(NpClient npClient);

    public NpClientStatus getNpClientStatus();

    public void setNpClientStatus(NpClientStatus npClientStatus);

    public String getComments();

    public void setComments(String comments);

    public void setTermReason(NpUsersReason termReason);
}
