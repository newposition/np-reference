package com.newposition.client.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpDocumentTypes;
import com.newposition.common.domain.NpSector;
import com.newposition.common.domain.NpSectorImpl;

@Entity
@Table(name = "cl_client_documents", catalog = "npdbs")
public class NpClClientDocuments
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ClientID")
    private Integer clientID;

    @OneToOne
    @JoinColumn(name = "ClientID", nullable = false, insertable = false, updatable = false)
    private NpClientImpl npClient;

    @OneToOne
    @JoinColumn(name = "DocumentTypeID", nullable = false)
    private NpDocumentTypes documentTypeID;

    @Column(name = "Document")
    private byte[] document;

    @Column(name = "Comments")
    private String comments;

    public Integer getClientID()
    {
        return clientID;
    }

    public void setClientID(Integer clientID)
    {
        this.clientID = clientID;
    }

    public NpClientImpl getNpClient()
    {
        return npClient;
    }

    public void setNpClient(NpClientImpl npClient)
    {
        this.npClient = npClient;
    }

    public NpDocumentTypes getDocumentTypeID()
    {
        return documentTypeID;
    }

    public void setDocumentTypeID(NpDocumentTypes documentTypeID)
    {
        this.documentTypeID = documentTypeID;
    }

    public byte[] getDocument()
    {
        return document;
    }

    public void setDocument(byte[] document)
    {
        this.document = document;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }
}

