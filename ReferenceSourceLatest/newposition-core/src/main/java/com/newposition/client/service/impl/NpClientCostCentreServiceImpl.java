package com.newposition.client.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.client.dao.NpClientCostCentreDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.domain.NpClientCostCentreDocument;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientCostCentreStatus;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.forms.AddClientCostCentreForm;
import com.newposition.client.service.NpClientCostCentreService;
import com.newposition.client.service.NpClientService;

public class NpClientCostCentreServiceImpl implements NpClientCostCentreService
{

    private NpClientCostCentreDao npClientCostCentreDao;

    private NpClientService npClientService;

    public NpClientService getNpClientService()
    {
        return npClientService;
    }

    public void setNpClientService(NpClientService npClientService)
    {
        this.npClientService = npClientService;
    }

    public NpClientCostCentreDao getNpClientCostCentreDao()
    {
        return npClientCostCentreDao;
    }

    public void setNpClientCostCentreDao(NpClientCostCentreDao npClientCostCentreDao)
    {
        this.npClientCostCentreDao = npClientCostCentreDao;
    }

    @Override
    public NpClientCostCentreStatus findCostCentreStatusByStatusCode(String status)
    {

        NpClientCostCentreStatus costCentreStatus = npClientCostCentreDao.findCostCentreStatusByStatusCode(status);

        return costCentreStatus;
    }

    @Override
    @Transactional
    public void saveCostCentre(AddClientCostCentreForm costCentreForm, String currentUserName)
    {

        NpClient client = npClientService.getCompanyOfCurrentUser(currentUserName);

        NpClientCostCentre costCentre = getCostCentre(costCentreForm);
        costCentre.setNpClient((NpClientImpl) client);
        NpClientCostCentreDocument ccDoc = new NpClientCostCentreDocument();
        if (StringUtils.isNotEmpty(costCentreForm.getOverview()) || costCentreForm.getVideo() != null)
        {

            ccDoc.setComments(costCentreForm.getOverview());
            File video = costCentreForm.getVideo();
            if (video != null)
            {
                byte[] document = getVideo(video);
                ccDoc.setDocument(document);
            }
        }

        npClientCostCentreDao.saveCostCentre(costCentre, ccDoc);
    }

    @Override
    @Transactional
    public void updateCostCentre(AddClientCostCentreForm costCentreForm)
    {

        NpClientCostCentre costCentre = findCostCentreById(costCentreForm.getCostCentreId().toString());

        NpClientCostCentre _costCentre = getCostCentre(costCentreForm);
        costCentre.setCostCentreCode(StringUtils.capitalize(_costCentre.getCostCentreCode().trim()));
        costCentre.setCostCentreName(StringUtils.capitalize(_costCentre.getCostCentreName().trim()));
        costCentre.setCostCentreOwner(StringUtils.capitalize(_costCentre.getCostCentreOwner().trim()));
        costCentre.setCostCentreSystem(StringUtils.capitalize(_costCentre.getCostCentreSystem().trim()));
        costCentre.setCostCentreLocation(StringUtils.capitalize(_costCentre.getCostCentreLocation().trim()));
        costCentre.setDescription(StringUtils.capitalize(_costCentre.getDescription().trim()));
//		costCentre.getCostCentreDoc().setComments(costCentreForm.getOverview());
//		costCentre.getCostCentreDoc().setDocument(getVideo(costCentreForm.getVideo()));

        NpClientCostCentreDocument ccDoc = costCentre.getCostCentreDoc();
        if (ccDoc == null)
        {
            if (StringUtils.isNotEmpty(costCentreForm.getOverview()) || costCentreForm.getVideo() != null)
            {
                ccDoc = new NpClientCostCentreDocument();
                ccDoc.setComments(costCentreForm.getOverview());
            }
        }
        else
        {
            ccDoc.setComments(costCentreForm.getOverview());
        }
        try
        {
            File video = costCentreForm.getVideo();
            if (video != null)
            {
                byte[] document = getVideo(video);
                ccDoc.setDocument(document);
            }
        }
        catch (Exception e)
        {

        }

//		npClientCostCentreDao.updateCostCentreCostCentre(_costCentre, ccDoc);

    }

    public NpClientCostCentre getCostCentre(AddClientCostCentreForm costCentreForm)
    {

        NpClientCostCentreImpl costCentre = new NpClientCostCentreImpl();

        costCentre.setCostCentreCode(StringUtils.capitalize(costCentreForm.getCostCentreCode().trim()));
        costCentre.setDescription(StringUtils.capitalize(costCentreForm.getDescription().trim()));
        costCentre.setCostCentreLocation(StringUtils.capitalize(costCentreForm.getLocation().trim()));
        costCentre.setCostCentreSystem(StringUtils.capitalize(costCentreForm.getCostCentreSystem().trim()));
        costCentre.setCostCentreName(StringUtils.capitalize(costCentreForm.getCostCentreName().trim()));
        costCentre.setCostCentreOwner(StringUtils.capitalize(costCentreForm.getCostCentreOwner().trim()));
        costCentre.setCostCentreStatus(findCostCentreStatusByStatusCode("Active"));
        costCentre.setLastActiveDate(new Date());
        return costCentre;
    }

    private byte[] getVideo(File video)
    {
        byte[] videoInByte = null;
        try
        {
            File file = video;
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            videoInByte = multipartFile.getBytes();
        }
        catch (Exception e)
        {
            System.out.println("Error occured during Multipart file convertion");
        }
        return videoInByte;
    }

    @SuppressWarnings("static-access")
    @Override
    public List<NpClientCostCentreImpl> getSortedListOfCostCentre(ArrayList<NpClientCostCentreImpl> costCentreList, String sortby)
    {
        NpClientCostCentreImpl np = new NpClientCostCentreImpl();
        if (sortby.equals("CostCentreCode"))
        {
            np.sortByCostcentreCode(costCentreList);
        }
        else if (sortby.equals("CostCentreName"))
        {
            np.sortByCostCentreName(costCentreList);
        }
        else if (sortby.equals("CostCentreSystem"))
        {
            np.sortByCostCentreSystem(costCentreList);
        }
        else if (sortby.equals("Location"))
        {
            np.sortByCostCenterLocation(costCentreList);
        }
        else if (sortby.equals("Description"))
        {
            np.sortByCostCentreDescription(costCentreList);
        }
        System.out.println("service");
        return costCentreList;
    }

    @Override
    public List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClient(
            String userName)
    {
        NpClient client = npClientService.getCompanyOfCurrentUser(userName);
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreDao.findAllCostCentreOfCurrentClient(client.getId().toString());
        return costCentres;
    }

    @Override
    @Transactional
    public List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClientWithVideo(
            String userName)
    {

        List<NpClientCostCentreImpl> costCentres = findAllCostCentreOfCurrentClient(userName);
        costCentres = getVideoUrl(costCentres);

        return costCentres;
    }

    private List<NpClientCostCentreImpl> getVideoUrl(
            List<NpClientCostCentreImpl> costCentres)
    {

        List<NpClientCostCentreImpl> ccs = new ArrayList<NpClientCostCentreImpl>();
        for (NpClientCostCentreImpl cc : costCentres)
        {
            if (cc.getCostCentreDoc() != null && cc.getCostCentreDoc().getDocument() != null)
            {
                NpClientCostCentreImpl cc1 = new NpClientCostCentreImpl();
                cc1.setCostCentreId(cc.getCostCentreId());
                StringBuilder sb = new StringBuilder();
                sb.append("data:video/mp4;base64,");
                String videoUrl = "";

                videoUrl = org.apache.commons.codec.binary.StringUtils.newStringUtf8(org.apache.commons.codec.binary.Base64.encodeBase64(cc.getCostCentreDoc().getDocument()));

                sb.append(videoUrl);
                String url = sb.toString();

                cc1.setVideoUrl(url);
                cc = cc1;
//				System.out.println("video url : "+url);
            }
            ccs.add(cc);
        }
        return ccs;
    }

    @Transactional
    @Override
    public NpClientCostCentreImpl findCostCentreById(String id)
    {
        NpClientCostCentreImpl costCentre = (NpClientCostCentreImpl) npClientCostCentreDao.findCostCentreById(id);
        return costCentre;
    }

    @Transactional
    @Override
    public void deleteCostCentre(String id)
    {

        npClientCostCentreDao.deleteCostCentreOfCurrentClient(id);
    }

    public List<NpClientCostCentreImpl> getLightList(List<NpClientCostCentreImpl> costCentres)
    {

        List<NpClientCostCentreImpl> costCentreData = new ArrayList<NpClientCostCentreImpl>();
        for (NpClientCostCentreImpl costCentre : costCentres)
        {

            costCentreData.add(getCostCentreLightWeightData(costCentre));
        }
        return costCentreData;
    }

    @Override
    public NpClientCostCentreImpl getCostCentreLightWeightData(NpClientCostCentreImpl _costCentre)
    {
        //
        NpClientCostCentreImpl form = new NpClientCostCentreImpl();
        form.setCostCentreCode(_costCentre.getCostCentreCode());
        form.setCostCentreId(_costCentre.getCostCentreId());
        form.setCostCentreName(_costCentre.getCostCentreName());
        form.setCostCentreOwner(_costCentre.getCostCentreOwner());
        form.setCostCentreSystem(_costCentre.getCostCentreSystem());
        form.setDescription(_costCentre.getDescription());
        form.setCostCentreLocation(_costCentre.getCostCentreLocation());
        NpClientCostCentreDocument doc = new NpClientCostCentreDocument();
        doc.setComments(_costCentre.getCostCentreDoc().getComments());
        form.setCostCentreDoc(doc);
        //System.out.println("###########  "+_costCentre);
        return form;
    }

    @Override
    public NpClientCostCentre findCostCentreByCode(String code)
    {
        // TODO Auto-generated method stub
        return npClientCostCentreDao.findCostCentreByCode(code);
    }

    @Override
    public String checkCostCentreCode(String str, NpClientImpl npClient)
    {
        int refCount = 0;
        String result = "";
        List<NpClientCostCentreImpl> costCentre = npClientCostCentreDao.findAllCostCentreOfCurrentClient(npClient.getId().toString());
        ;
        for (int i = 0; i < costCentre.size(); i++)
        {
            NpClientCostCentreImpl _costCentre = costCentre.get(i);
            if (_costCentre.getCostCentreCode().equals(str))
            {
                refCount++;
            }
        }
        if (refCount > 0)
        {
            System.out.println("N@@@@@@@@@@@@@@@@");
            return "NOT OK";
        }
        else
        {
            System.out.println("O@@@@@@@@@@@@@@@@");
            return "OK";
        }
    }

    @Override
    public Integer getCostCentreCount(String name)
    {
        // TODO Auto-generated method stub
        NpClient client = npClientService.getCompanyOfCurrentUser(name);
        Integer count = npClientCostCentreDao.findCostCentreCount(client.getId().toString());

        return count;
    }

    @Override
    public List<String> getStringList(String str, List<NpClientCostCentreImpl> costCentres)
    {
        List<String> strList = new ArrayList<String>();
        for (int i = 0; i < costCentres.size(); i++)
        {
            NpClientCostCentreImpl npCostCentre = costCentres.get(i);
            if (str.equals("CostCentreCode"))
            {
                strList.add(npCostCentre.getCostCentreCode());
            }
            else if (str.equals("CostCentreOwner"))
            {
                strList.add(npCostCentre.getCostCentreOwner());
            }
            else if (str.equals("CostCenterLoc"))
            {
                strList.add(npCostCentre.getCostCentreLocation());
            }
            else if (str.equals("CostCenterName"))
            {
                strList.add(npCostCentre.getCostCentreName());
            }
        }

        List<String> strList1 = new ArrayList<String>(new HashSet<String>(strList));
        return strList1;
    }

    @Override
    public List<NpClientCostCentreImpl> getClientCostCentreByFilter(
            String str1, String str2, List<NpClientCostCentreImpl> costCentres)
    {
        List<NpClientCostCentreImpl> costCentre1 = new ArrayList<NpClientCostCentreImpl>();
        for (int i = 0; i < costCentres.size(); i++)
        {
            NpClientCostCentreImpl npClientCCentre = costCentres.get(i);
            if (str1.equals("CostCentreCode"))
            {
                if (str2.equals(npClientCCentre.getCostCentreCode()))
                {
                    costCentre1.add(npClientCCentre);
                }
            }
            else if (str1.equals("CostCentreOwner"))
            {
                if (str2.equals(npClientCCentre.getCostCentreOwner()))
                {
                    costCentre1.add(npClientCCentre);
                }
            }
            else if (str1.equals("CostCenterLoc"))
            {
                if (str2.equals(npClientCCentre.getCostCentreLocation()))
                {
                    costCentre1.add(npClientCCentre);
                }
            }
            else if (str1.equals("CostCenterName"))
            {
                if (str2.equals(npClientCCentre.getCostCentreName()))
                {
                    costCentre1.add(npClientCCentre);
                }
            }
        }
        return costCentre1;
    }
}
