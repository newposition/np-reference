package com.newposition.client.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.newposition.client.dao.NpClientRoleManagementDao;
import com.newposition.client.forms.NpClientAddRoleGroupForm;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.common.domain.CoGroups;

public class NpClientRoleManagementServiceImpl implements NpClientRoleManagementService
{

    private NpClientRoleManagementDao npClientRoleManagementDao;

    public void setNpClientRoleManagementDao(NpClientRoleManagementDao npClientRoleManagementDao)
    {
        this.npClientRoleManagementDao = npClientRoleManagementDao;
    }

    @Override
    @Transactional
    public int saveGroups(NpClientAddRoleGroupForm npClientAddRoleGroupForm, int clientId, String userName)
    {
        // TODO Auto-generated method stub
        return npClientRoleManagementDao.saveGroups(npClientAddRoleGroupForm, clientId, userName);
    }

    @Override
    public List<CoGroups> getListOfClientGroupsById(int id, String sortBy)
    {
        // TODO Auto-generated method stub
        return npClientRoleManagementDao.getListOfClientGroupsById(id, sortBy);
    }

    @Override
    public CoGroups getCoGroupById(int id)
    {
        // TODO Auto-generated method stub
        return npClientRoleManagementDao.getCoGroupById(id);
    }

    @Override
    @Transactional
    public void deleteGroupById(int id)
    {
        // TODO Auto-generated method stub
        npClientRoleManagementDao.deleteGroupById(id);
    }

    @Override
    public CoGroups getListOfRolesInClientGroups(int clientId,
                                                 String groupName)
    {
        // TODO Auto-generated method stub
        return npClientRoleManagementDao.getListOfRolesInClientGroups(clientId, groupName);
    }

    @Override
    public String checkClientGroupExistsOrNot(int clientId, String groupName)
    {
        // TODO Auto-generated method stub
        return npClientRoleManagementDao.checkClientGroupExistsOrNot(clientId, groupName);
    }
}
