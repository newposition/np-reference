package com.newposition.client.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.newposition.common.domain.NpSector;
import com.newposition.common.domain.NpSectorImpl;

@Entity
@Table(name = "np_client", catalog = "npdbs")
public class NpClientImpl implements NpClient
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "company_name", length = 50)
    private String companyName;

    @Column(name = "company_logo")
    private byte[] companyLogo;

    @ManyToOne(targetEntity = NpSectorImpl.class)
    private NpSector sector;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organization", targetEntity = NpClientUserImpl.class, fetch = FetchType.EAGER)
    private Set<NpClientUser> clientUsers;

    @Column(name = "first_name", length = 30)
    private String firstName;

    @Column(name = "last_name", length = 30)
    private String lastName;

    @Column(name = "principle_contact_email", length = 30)
    private String principleContactEmail;

    @Column(name = "tele_number", length = 15)
    private String teleNumber;

    @Column(name = "mobile_number", length = 15)
    private String mobileNumber;

    @Column(name = "address1", length = 30)
    private String address1;

    @Column(name = "address2", length = 30)
    private String address2;

    @Column(name = "town", length = 30)
    private String town;

    @Column(name = "postal_code", length = 10)
    private String postalCode;

    @Column(name = "country", length = 10)
    private String country;

    @ManyToOne(targetEntity = NpClientStatusImpl.class)
    private NpClientStatus clientStatus;

    @Column(name = "ClientOverview", length = 200)
    private String clientOverview;

    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public String getCompanyName()
    {
        return companyName;
    }

    @Override
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    @Override
    public NpSector getSector()
    {
        return sector;
    }

    @Override
    public void setSector(NpSector sector)
    {
        this.sector = sector;
    }

    @Override
    public Set<NpClientUser> getClientUsers()
    {
        return clientUsers;
    }

    @Override
    public void setClientUsers(Set<NpClientUser> clientUsers)
    {
        this.clientUsers = clientUsers;
    }

    @Override
    public String getFirstName()
    {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    @Override
    public String getLastName()
    {
        return lastName;
    }

    @Override
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    @Override
    public String getPrincipleContactEmail()
    {
        return principleContactEmail;
    }

    @Override
    public void setPrincipleContactEmail(String principleContactEmail)
    {
        this.principleContactEmail = principleContactEmail;
    }

    @Override
    public String getTeleNumber()
    {
        return teleNumber;
    }

    @Override
    public void setTeleNumber(String teleNumber)
    {
        this.teleNumber = teleNumber;
    }

    @Override
    public String getMobileNumber()
    {
        return mobileNumber;
    }

    @Override
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String getAddress1()
    {
        return address1;
    }

    @Override
    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    @Override
    public String getAddress2()
    {
        return address2;
    }

    @Override
    public void setAddress2(String address2)
    {
        this.address2 = address2;
    }

    @Override
    public String getTown()
    {
        return town;
    }

    @Override
    public void setTown(String town)
    {
        this.town = town;
    }

    @Override
    public String getPostalCode()
    {
        return postalCode;
    }

    @Override
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    @Override
    public String getCountry()
    {
        return country;
    }

    @Override
    public void setCountry(String country)
    {
        this.country = country;
    }

    @Override
    public NpClientStatus getClientStatus()
    {
        return clientStatus;
    }

    @Override
    public void setClientStatus(NpClientStatus clientStatus)
    {
        this.clientStatus = clientStatus;
    }

    @Override
    public byte[] getCompanyLogo()
    {
        return companyLogo;
    }

    @Override
    public void setCompanyLogo(byte[] companyLogo)
    {
        this.companyLogo = companyLogo;
    }

    @Override
    public int hashCode()
    {

        return this.firstName.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        int id1 = ((NpClientImpl) obj).getId();
        int id2 = this.id;
        return (id1 == id2);
    }

    @Override
    public String getClientOverview()
    {
        return clientOverview;
    }

    @Override
    public void setClientOverview(String clientOverview)
    {
        this.clientOverview = clientOverview;
    }
}

