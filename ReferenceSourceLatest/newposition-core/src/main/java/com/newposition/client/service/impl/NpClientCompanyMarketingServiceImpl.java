package com.newposition.client.service.impl;

import java.io.File;
import java.io.FileInputStream;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.newposition.client.dao.NpClientCompanyMarketingDao;
import com.newposition.client.domain.NpClClientDocuments;
import com.newposition.client.domain.NpClient;
import com.newposition.client.forms.ClientMarketingForm;
import com.newposition.client.service.NpClientCompanyMarketingService;

public class NpClientCompanyMarketingServiceImpl implements NpClientCompanyMarketingService
{

    private NpClientCompanyMarketingDao npClientCompanyMarketingDao;

    public NpClientCompanyMarketingDao getNpClientMarketingDao()
    {
        return npClientCompanyMarketingDao;
    }

    public void setNpClientCompanyMarketingDao(NpClientCompanyMarketingDao npClientCompanyMarketingDao)
    {
        this.npClientCompanyMarketingDao = npClientCompanyMarketingDao;
    }

    @Override
    @Transactional
    public void saveClientMarketingForm(NpClient client, ClientMarketingForm clientMarketingForm)
    {

        NpClClientDocuments npClClientDocuments = new NpClClientDocuments();
        if (StringUtils.isNotEmpty(clientMarketingForm.getClientOverview()) || clientMarketingForm.getVideo() != null)
        {

            npClClientDocuments.setClientID(client.getId());

            File video = clientMarketingForm.getVideo();
            if (video != null)
            {
                byte[] document = getVideo(video);
                npClClientDocuments.setDocument(document);
            }
        }

        npClientCompanyMarketingDao.saveClientMarketingForm(client, npClClientDocuments);
    }

    private byte[] getVideo(File video)
    {
        byte[] videoInByte = null;
        try
        {
            File file = video;
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            videoInByte = multipartFile.getBytes();
        }
        catch (Exception e)
        {
            System.out.println("Error occured during Multipart file convertion");
        }
        return videoInByte;
    }
}
