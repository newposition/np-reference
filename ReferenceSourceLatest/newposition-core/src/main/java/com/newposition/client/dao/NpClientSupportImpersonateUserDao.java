package com.newposition.client.dao;

import java.util.List;

import com.newposition.common.domain.NpUser;

/**
 * @author Sachi N
 */

public interface NpClientSupportImpersonateUserDao
{

    public List<NpUser> getListOfNpUser();

    public List getSortedNpUsers(String sortBy, String pageNumber, String filterBy);

    public NpUser getUserById(int userId);
}
