package com.newposition.client.service;

import com.newposition.client.domain.NpClient;

public interface NpClientCompanyInformationService
{

    NpClient findByUserName(String name);
}
