package com.newposition.client.dao;

import java.util.List;

import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.domain.NpClientCostCentreDocument;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientCostCentreStatus;

public interface NpClientCostCentreDao
{

    NpClientCostCentreStatus findCostCentreStatusByStatusCode(String statusId);

    NpClientCostCentre saveCostCentre(NpClientCostCentre costCentre, NpClientCostCentreDocument ccDoc);

    List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClient(
            String clientId);

    void deleteCostCentreOfCurrentClient(String costCentreId);

    NpClientCostCentre findCostCentreById(String clientId);

    NpClientCostCentre findCostCentreByCode(String code);

    NpClientCostCentre updateCostCentreCostCentre(
            NpClientCostCentre costCentre, NpClientCostCentreDocument ccDoc);

    Integer findCostCentreCount(String Id);
}
