package com.newposition.client.forms;

public class NpClientAddRoleGroupForm
{

    public String groupId;

    public String groupName;

    public String groupDescription;

    public String currentRole;

    public String getGroupId()
    {
        return groupId;
    }

    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupDescription()
    {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription)
    {
        this.groupDescription = groupDescription;
    }

    public String getCurrentRole()
    {
        return currentRole;
    }

    public void setCurrentRole(String currentRole)
    {
        this.currentRole = currentRole;
    }
}
