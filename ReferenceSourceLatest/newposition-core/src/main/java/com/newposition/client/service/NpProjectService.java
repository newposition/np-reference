package com.newposition.client.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.domain.NpClientProjectDocument;

public interface NpProjectService
{

    void saveProgramme(NpClientProject npProject, NpClientProjectDocument proDoc);

    List<NpClientProject> getAllClientProjects(NpClientImpl npClient);

    NpClientProject getNpProjectById(int id);

    List<String> getStringList(String str, List<NpClientProject> projects);

    String deleteProject(int id);

    List<NpClientProject> getClientProjectsByFilter(String str1, String str2,
                                                    List<NpClientProject> projects);

    List<NpClientProject> getProjectsBySort(
            ArrayList<NpClientProject> projectlist, String str);

    NpClientProject saveOrUpdate(NpClientProject npClientProject);

    String checkProjectCode(String str, NpClientImpl npClient);

    byte[] getVideo(File video);

    NpClientProject updateWithNewValue(NpClientProject source, NpClientProject target);

    List<NpClientProject> getAllClientProjectsWithVideo(NpClient client);
}
