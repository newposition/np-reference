package com.newposition.client.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpDocumentTypes;

@Entity
@Table(name = "cl_project_documents", catalog = "npdbs")
public class NpClientProjectDocument implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ClientProjectID")
    private Long clientProjectID;

    @OneToOne
    @JoinColumn(name = "ClientProjectID", nullable = false, insertable = false, updatable = false)
    private NpClientProject npProject;

    @OneToOne
    @JoinColumn(name = "DocumentTypeID", nullable = false)
    private NpDocumentTypes documentTypeID;

    public Long getClientProjectID()
    {
        return clientProjectID;
    }

    public void setClientProjectID(Long clientProjectID)
    {
        this.clientProjectID = clientProjectID;
    }

    public NpClientProject getNpProject()
    {
        return npProject;
    }

    public void setNpProject(NpClientProject npProject)
    {
        this.npProject = npProject;
    }

    public NpDocumentTypes getDocumentTypeID()
    {
        return documentTypeID;
    }

    public void setDocumentTypeID(NpDocumentTypes documentTypeID)
    {
        this.documentTypeID = documentTypeID;
    }

    public byte[] getDocument()
    {
        return document;
    }

    public void setDocument(byte[] document)
    {
        this.document = document;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    @Column(name = "Document")
    private byte[] document;

    @Column(name = "Comments")
    private String comments;
}
