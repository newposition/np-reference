package com.newposition.client.dao.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientOfficeLocationDao;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;

public class NpClientOfficeLocationDaoImpl implements NpClientOfficeLocationDao
{

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public List<NpClientOfficeLocation> getAllClientOfficeLocations(
            NpClientImpl npClient)
    {
        List<NpClientOfficeLocation> officeLocations = (List<NpClientOfficeLocation>) hibernateTemplate.find("from NpClientOfficeLocation np where np.npClient.id=?", npClient.getId());
        return officeLocations;
    }

    @Override
    public NpClientOfficeLocation getnpClientOfficeLocationServiceById(int id)
    {
        NpClientOfficeLocation npClientOfficeLocation = (NpClientOfficeLocation) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientOfficeLocation.class).add(Restrictions.eq("clientOfficeLocID", id)).uniqueResult();
        return npClientOfficeLocation;
    }

    @Override
    public void saveCoCities(CoCities coCities)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coCities);
    }

    @Override
    public void saveCoCounty(CoCounty coCounty)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coCounty);
    }

    @Override
    public void saveCoCountry(CoCountry coCountry)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coCountry);
    }

    @Override
    public void saveCoLocationState(CoLocationState coLocationState)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coLocationState);
    }

    @Override
    public void saveCoZip(CoZip coZip)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coZip);
    }

    @Override
    public void saveCoLocations(CoLocations coLocations)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(coLocations);
    }

    @Override
    public void saveNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(npClientOfficeLocation);
    }

    @Override
    public List<CoLocations> getCoLocationsList(String str1)
    {
        List<CoLocations> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoLocations.class).add(Restrictions.like("address1", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoLocations> getCoLocationsList1(String str1)
    {
        List<CoLocations> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoLocations.class).add(Restrictions.like("address2", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoLocations> getCoLocationsList2(String str1)
    {
        List<CoLocations> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoLocations.class).add(Restrictions.like("address3", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoCities> getCoCitiesList(String str1)
    {
        List<CoCities> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoCities.class).add(Restrictions.like("cityDescription", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoCounty> getCoCountyList(String str1)
    {
        List<CoCounty> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoCounty.class).add(Restrictions.like("countyDescription", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoCountry> getCoCountryList(String str1)
    {
        List<CoCountry> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoCountry.class).add(Restrictions.like("countryDescription", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoLocationState> getCoStateList(String str1)
    {
        List<CoLocationState> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoLocationState.class).add(Restrictions.like("stateDescription", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoLocationState> getCoStateList1(String str1)
    {
        List<CoLocationState> list = getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(CoLocationState.class).add(Restrictions.like("stateID", "%" + str1 + "%")).list();
        return list;
    }

    @Override
    public List<CoZip> getAllZips()
    {
        List<CoZip> zips = (List<CoZip>) hibernateTemplate.find("from CoZip");
        return zips;
    }

    @Override
    public void deleteNpClientOfficeLocation(
            NpClientOfficeLocation npClientOfficeLocation)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().delete(npClientOfficeLocation);
    }

    @Override
    public List<CoCountry> getAllCountries()
    {
        List<CoCountry> contries = (List<CoCountry>) hibernateTemplate.find("from CoCountry");
        return contries;
    }

    @Override
    public List<CoLocations> getAllCoLocations(String str)
    {
        List<CoLocations> officeLocations = (List<CoLocations>) hibernateTemplate.find("from CoLocations np where np.coCountry.countryDescription=?", str);
        return officeLocations;
    }

    @Override
    public List<CoLocations> getAllCoLocations(String str1, String str2)
    {
        List<CoLocations> officeLocations = (List<CoLocations>) hibernateTemplate.find("from CoLocations np where np.coCountry.countryDescription=? and np.coZip.coLocationState.stateDescription=?", str1, str2);
        return officeLocations;
    }

    @Override
    public List<NpClientOfficeLocation> getLocationListByCountryName(String str)
    {
        List<NpClientOfficeLocation> list = (List<NpClientOfficeLocation>) hibernateTemplate.find("from NpClientOfficeLocation np where np.coLocations.coCountry.countryDescription=?", str);
        return list;
    }

    @Override
    public List<NpClientOfficeLocation> getLocationListByCountryName(
            String str1, String str2)
    {
        List<NpClientOfficeLocation> list = (List<NpClientOfficeLocation>) hibernateTemplate.find("from NpClientOfficeLocation");
        return list;
    }

    @Override
    public List<CoCities> getCoCitiesListByName(String cityDescription)
    {
        List<CoCities> list = (List<CoCities>) hibernateTemplate.find("from CoCities ci where ci.cityDescription=?", cityDescription);
        return list;
    }

    @Override
    public List<CoCounty> getCoCountyListByName(String countyDescription)
    {
        List<CoCounty> list = (List<CoCounty>) hibernateTemplate.find("from CoCounty ci where ci.countyDescription=?", countyDescription);
        return list;
    }

    @Override
    public List<CoCountry> getCoCountryListByName(String countryDescription)
    {
        List<CoCountry> list = (List<CoCountry>) hibernateTemplate.find("from CoCountry ci where ci.countryDescription=?", countryDescription);
        return list;
    }

    @Override
    public List<CoLocationState> getCoLocationStateListByName(String stateDescription)
    {
        List<CoLocationState> list = (List<CoLocationState>) hibernateTemplate.find("from CoLocationState ci where ci.stateDescription=?", stateDescription);
        return list;
    }
}
