package com.newposition.client.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpClientCostCentreDao;
import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.domain.NpClientCostCentreDocument;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientCostCentreStatus;
import com.newposition.common.domain.NpDocumentTypes;
import com.newposition.exception.CostCentreNotFoundException;
import com.newposition.exception.StatusNotFoundException;

public class NpClientCostCentreDaoImpl implements NpClientCostCentreDao
{

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public NpClientCostCentreStatus findCostCentreStatusByStatusCode(
            String status) throws StatusNotFoundException
    {

        try
        {

            NpClientCostCentreStatus costCentreStatus = (NpClientCostCentreStatus) hibernateTemplate
                    .find("from NpClientCostCentreStatus where status=?", status).get(0);

            return costCentreStatus;
        }
        catch (Exception e)
        {
            throw new StatusNotFoundException(
                    "Cost Centre Status Not Found with Name: " + status);
        }
    }

    @Override
    public NpClientCostCentre saveCostCentre(NpClientCostCentre costCentre, NpClientCostCentreDocument ccDoc)
    {

        Long costCentreId = (Long) hibernateTemplate.save(costCentre);
        costCentre.setCostCentreId(costCentreId);
        if (ccDoc != null)
        {
            ccDoc.setCostCentreID(costCentreId);
            NpDocumentTypes doc = new NpDocumentTypes();
            doc.setDocumentTypeID(1);
            ccDoc.setDocumentTypeID(doc);
            hibernateTemplate.save(ccDoc);
        }
        return costCentre;
    }

    @Override
    public NpClientCostCentre updateCostCentreCostCentre(NpClientCostCentre costCentre, NpClientCostCentreDocument ccDoc)
    {

        hibernateTemplate.saveOrUpdate(costCentre);
        if (ccDoc != null)
        {
            hibernateTemplate.saveOrUpdate(ccDoc);
        }

        return costCentre;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpClientCostCentreImpl> findAllCostCentreOfCurrentClient(String clientId)
    {

        List<NpClientCostCentreImpl> costCentreList;
        costCentreList = (List<NpClientCostCentreImpl>) hibernateTemplate.find(" from NpClientCostCentreImpl cc where clientID=? AND cc.costCentreStatus.costCenterStatusID != 3", Long.parseLong(clientId));

        return costCentreList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpClientCostCentre findCostCentreById(String costCentreId) throws CostCentreNotFoundException
    {
        Long id;
        try
        {
            id = Long.parseLong(costCentreId);
        }
        catch (Exception e)
        {
            throw new CostCentreNotFoundException("Cost Centre Not found with id" + costCentreId);
        }
        List<NpClientCostCentreImpl> costCentreList;

        costCentreList = (List<NpClientCostCentreImpl>) hibernateTemplate.find(" from NpClientCostCentreImpl where costCentreId=? ", id);
        if (costCentreList.size() == 0)
        {
            throw new CostCentreNotFoundException("Cost Centre Not found with id" + costCentreId);
        }
        return costCentreList.get(0);
    }

    @Override
    public void deleteCostCentreOfCurrentClient(String costCentreId)
    {

        NpClientCostCentreImpl costCentre = (NpClientCostCentreImpl) findCostCentreById(costCentreId);
        NpClientCostCentreStatus costCentreStatus = new NpClientCostCentreStatus();
        costCentreStatus.setCostCenterStatusID(3);
        costCentre.setCostCentreStatus(costCentreStatus);
        hibernateTemplate.saveOrUpdate(costCentre);
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpClientCostCentre findCostCentreByCode(String code) throws CostCentreNotFoundException
    {
        List<NpClientCostCentreImpl> costCentreList;

        costCentreList = (List<NpClientCostCentreImpl>) hibernateTemplate.find(" from NpClientCostCentreImpl where costCentreCode=? ", code);
        if (costCentreList.size() == 0)
        {
            throw new CostCentreNotFoundException("Cost Centre Not found with code " + code);
        }
        return costCentreList.get(0);
    }

    @Override
    public Integer findCostCentreCount(String Id)
    {

        Integer clientId = Integer.parseInt(Id);
        Integer count = ((Long) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("select count(*) from NpClientCostCentreImpl cc where cc.npClient.id=" + clientId + " AND cc.costCentreStatus.costCenterStatusID != 3").uniqueResult()).intValue();
        return count;
    }
}
