package com.newposition.client.domain;

import java.io.Serializable;

public interface NpClientStatus extends Serializable
{

    public Integer getId();

    public void setId(Integer id);

    public String getStatus();

    public void setStatus(String status);
}
