package com.newposition.client.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpDocumentTypes;
import com.newposition.common.domain.NpProgram;

@Entity
@Table(name = "cl_program_documents", catalog = "npdbs")
public class NpClientProgramDocument implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ClientProgramID")
    private Long clientProgramID;

    @OneToOne
    @JoinColumn(name = "ClientProgramID", nullable = false, insertable = false, updatable = false)
    private NpProgram npProgram;

    @OneToOne
    @JoinColumn(name = "DocumentTypeID", nullable = false)
    private NpDocumentTypes documentTypeID;

    @Column(name = "Document")
    private byte[] document;

    @Column(name = "Comments")
    private String comments;

    public Long getClientProgramID()
    {
        return clientProgramID;
    }

    public void setClientProgramID(Long clientProgramID)
    {
        this.clientProgramID = clientProgramID;
    }

    public NpProgram getNpProgram()
    {
        return npProgram;
    }

    public void setNpProject(NpProgram npProgram)
    {
        this.npProgram = npProgram;
    }

    public NpDocumentTypes getDocumentTypeID()
    {
        return documentTypeID;
    }

    public void setDocumentTypeID(NpDocumentTypes documentTypeID)
    {
        this.documentTypeID = documentTypeID;
    }

    public byte[] getDocument()
    {
        return document;
    }

    public void setDocument(byte[] document)
    {
        this.document = document;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }
}
