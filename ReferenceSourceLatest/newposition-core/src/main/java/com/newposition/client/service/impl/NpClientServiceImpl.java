package com.newposition.client.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.config.SortedResourcesFactoryBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.client.dao.NpClientDao;
import com.newposition.client.dao.NpClientSupportAddEditUserDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientStatus;
import com.newposition.client.domain.NpClientUser;
import com.newposition.client.domain.NpClientUserImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.forms.AddClientForm;
import com.newposition.client.forms.ReasonForm;
import com.newposition.client.service.NpClientService;
import com.newposition.common.dao.NpRoleDao;
import com.newposition.common.dao.NpSectorDao;
import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleType;
import com.newposition.common.domain.NpSector;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserAttempts;
import com.newposition.common.domain.NpUserAttemptsImpl;
import com.newposition.exception.UserNotFoundException;
import com.newposition.infra.mail.NpMailService;

public class NpClientServiceImpl implements NpClientService
{

    private NpClientDao clientDao;

    private NpSectorDao sectorDao;

    private NpRoleDao roleDao;

    private NpUserDao npUserDao;

    private String imageContext;

    private NpMailService npMailService;

    private NpCandidateService npCandidateService;

    private NpClientSupportAddEditUserDao clientSupportAddEditUserDao;

    public void setClientSupportAddEditUserDao(
            NpClientSupportAddEditUserDao clientSupportAddEditUserDao)
    {
        this.clientSupportAddEditUserDao = clientSupportAddEditUserDao;
    }

    public NpCandidateService getNpCandidateService()
    {
        return npCandidateService;
    }

    public void setNpCandidateService(NpCandidateService npCandidateService)
    {
        this.npCandidateService = npCandidateService;
    }

    public void setNpUserDao(NpUserDao npUserDao)
    {
        this.npUserDao = npUserDao;
    }

    private BCryptPasswordEncoder bcryptEncoder;

    public void setClientDao(NpClientDao clientDao)
    {
        this.clientDao = clientDao;
    }

    public void setSectorDao(NpSectorDao sectorDao)
    {
        this.sectorDao = sectorDao;
    }

    public void setBcryptEncoder(BCryptPasswordEncoder bcryptEncoder)
    {
        this.bcryptEncoder = bcryptEncoder;
    }

    public void setRoleDao(NpRoleDao roleDao)
    {
        this.roleDao = roleDao;
    }

    @Override
    @Transactional
    public NpClient addClient(AddClientForm form)
    {
        NpClient client = getClient(form);
        clientDao.save(client);
        return client;
    }

    private NpClient getClient(AddClientForm form)
    {

        NpClient client = new NpClientImpl();

        client.setAddress1(form.getAddress1());
        client.setAddress2(form.getAddress2());
        client.setCompanyName(form.getCompanyName());
        client.setCountry(form.getCountry());
        client.setFirstName(form.getFirstName());
        client.setLastName(form.getLastName());
        client.setMobileNumber(form.getMobileNumber());
        client.setTeleNumber(form.getTeleNumber());
        client.setTown(form.getTown());
        client.setPostalCode(form.getPostCode());
        client.setPrincipleContactEmail(form.getPrincipleContactEmail());
        try
        {
            File file = form.getCompanyImage();
            //byte[] bFile = new byte[(int) file.length()];
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            client.setCompanyLogo(multipartFile.getBytes());
        }
        catch (Exception e)
        {
//			 e.printStackTrace();
        }

        NpRole role = roleDao.findRoleByType(NpRoleType.ROLE_CLIENT_ADMINISTRATOR);

        NpSector sector = sectorDao.findByID(form.getSectorId());

        client.setSector(sector);

        NpClientStatus status = clientDao.fetchClientStatusById(4);

        client.setClientStatus(status);

        NpClientUser clientUser = new NpClientUserImpl();

        String encryptedPassword = bcryptEncoder.encode(form.getAdminPassword());

        clientUser.setEncryptedPassword(encryptedPassword);

        clientUser.setFirstName(form.getAdminFirstName());

        clientUser.setLastName(form.getAdminLastName());

        clientUser.setOrganization(client);

        clientUser.setPrimaryEmail(form.getAdminEmail());

        clientUser.setNpRoles(Collections.singletonList(role));

        clientUser.setNpUserStatus(clientDao.getDisableUserStatus());

        NpUserAttempts npUserAttempts = new NpUserAttemptsImpl();
        npUserAttempts.setAttempts(0);
        npUserAttempts.setLastModified(new Date());
        clientUser.setNpUserAttempts(npUserAttempts);

        client.setClientUsers(Collections.singleton(clientUser));

        return client;
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public NpClient updateClient(AddClientForm form)
    {

        NpClient client = this.getClientById(form.getId());
        client = updateClient(client, form);
        clientDao.save(client);
        return client;
    }

    private NpClient updateClient(NpClient client, AddClientForm form)
    {

        client.setAddress1(form.getAddress1());
        client.setAddress2(form.getAddress2());
        client.setCompanyName(form.getCompanyName());
        client.setCountry(form.getCountry());
        client.setFirstName(form.getFirstName());
        client.setLastName(form.getLastName());
        client.setMobileNumber(form.getMobileNumber());
        client.setTeleNumber(form.getTeleNumber());
        client.setTown(form.getTown());
        client.setPostalCode(form.getPostCode());
        client.setPrincipleContactEmail(form.getPrincipleContactEmail());
        try
        {
            File file = form.getCompanyImage();
            //byte[] bFile = new byte[(int) file.length()];
            FileInputStream input = new FileInputStream(file);
            MultipartFile multipartFile = new MockMultipartFile("file", file.getName(), "text/plain", IOUtils.toByteArray(input));

            client.setCompanyLogo(multipartFile.getBytes());
        }
        catch (Exception e)
        {
//			 e.printStackTrace();
        }

        NpSector sector = sectorDao.findByID(form.getSectorId());

        client.setSector(sector);

        NpClientStatus status = clientDao.fetchClientStatusById(form.getClientStatusId());

        client.setClientStatus(status);

        NpClientUser clientUser = null;

        for (NpClientUser user : client.getClientUsers())
        {
            clientUser = user;
            break;
        }

//	String encryptedPassword = bcryptEncoder.encode(form.getAdminPassword());
//
//	clientUser.setEncryptedPassword(encryptedPassword);

        clientUser.setFirstName(form.getAdminFirstName());

        clientUser.setLastName(form.getAdminLastName());

        clientUser.setOrganization(client);

        clientUser.setPrimaryEmail(form.getAdminEmail());

//	npUserDao.

        client.setClientUsers(Collections.singleton(clientUser));

        return client;
    }

    @Override
    public List<NpClient> getAllClients()
    {
        return clientDao.findAll();
    }

    @Override
    public NpClient getClientById(Integer id)
    {
        return clientDao.findByID(id);
    }

    @Override
    public List getSortedNpClients(String sortBy, String pageNumber)
    {
        return clientDao.getSortedNpClients(sortBy, pageNumber);
    }

    @Override
    public void updateNpClientStatus(ReasonForm reasonForm)
    {
        clientDao.updateNpClientStatus(reasonForm);
    }

    @Override
    public void deleteNpClient(ReasonForm reasonForm)
    {
        clientDao.deleteNpClient(reasonForm);
        ;
    }

    @Override
    public List<NpClient> getNpClients()
    {
        return clientDao.getNpClients();
    }

    @Override
    public NpClient findClientByCompanyName(String companyName)
    {
        return clientDao.findClientByCompanyName(companyName);
    }

    @Override
    public NpClient findClientByPrincipal(String principal)
    {
        return clientDao.findClientByPrincipal(principal);
    }

    @Override
    public boolean isAddedDuplicateEmail(Integer clientId, Integer adminId, String clientEmail)
    {

        NpClient client = getClientById(clientId);
        NpUser user = null;
        for (NpClientUser clientUser : client.getClientUsers())
        {
            user = clientUser;
            break;
        }

        if (!user.getPrimaryEmail().equals(clientEmail) && npUserDao.findByUserName(clientEmail) != null)
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean isAddedDuplicateUserEmail(Integer clientId, Integer adminId, String clientEmail)
    {

        NpClient client = getClientById(clientId);
        NpUser user = null;
        List<NpClientUserImpl> list = new ArrayList(client.getClientUsers());
        List<NpClientUserImpl> _list = new ArrayList<NpClientUserImpl>();
        for (NpClientUser clientUser : list)
        {
            user = clientUser;
            if (user.getId() != adminId)
            {
                _list.add((NpClientUserImpl) user);
            }
        }

        for (NpClientUser clientUser : _list)
        {
            user = clientUser;
            if (user.getPrimaryEmail().equals(clientEmail))
            {
                return true;
            }
        }

        return false;
    }

    public String getImageContext()
    {
        return imageContext;
    }

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    public NpMailService getNpMailService()
    {
        return npMailService;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    @Override
    public void sendAddClientSucessMail(AddClientForm addClientForm,
                                        String directActivationLink, String subject)
    {
        String candidateName = addClientForm.getAdminFirstName() + " " + addClientForm.getAdminLastName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", candidateName);
        context.setVariable("directActivationLink", directActivationLink);
        context.setVariable("template", "addClient-email.html");
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);

        getNpMailService().sendMail(information, addClientForm.getAdminEmail(), context);
    }

    @Override
    public Integer getUserReferences(String referenceEmail)
    {

        if (StringUtils.isNotEmpty(referenceEmail))
        {
            List<NpCandidateReference> candidateReferences = npCandidateService.getCandidateReferenceList(referenceEmail.trim());

            Integer referenceNumber = candidateReferences.size();

            return referenceNumber;
        }
        else
        {
            return 0;
        }
    }

    @Override
    public List getSortedNpUserClients(String sortBy, String pageNumber, String userName)
    {

        NpClient client = getCompanyOfCurrentUser(userName);
        List sortedResult = clientDao.getSortedNpUserClients(sortBy, pageNumber, client);
        return sortedResult;
    }

    @Override
    public NpClient getCompanyOfCurrentUser(String userName)
    {

        NpClientUser clientUser = null;
        try
        {
            clientUser = clientSupportAddEditUserDao.getNpClientUserByName(userName);
            return clientUser.getOrganization();
        }
        catch (UserNotFoundException unf)
        {
            throw unf;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Integer getCountNpUserClients(String name)
    {

        List sortedResult = getSortedNpUserClients("", "", name);
        List<NpClientUser> clientUsers = (List<NpClientUser>) sortedResult.get(0);
        Integer count = clientUsers.size();
        return count;
    }

    @Override
    @Transactional
    public NpClientImpl getNpClientImpl(Integer id)
    {
        NpClientUserImpl npClientUser = clientDao.getNpClientImplId(id);
        System.out.println("npClientUser=" + npClientUser);
        NpClientImpl npClientImpl = (NpClientImpl) npClientUser.getOrganization();
        System.out.println("@@@@@@@@@@@@@@@@@@@ " + npClientImpl.getCompanyName());
        return npClientImpl;
    }
}
