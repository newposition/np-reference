package com.newposition.client.service.impl;

import com.newposition.client.dao.NpClientCompanyInformationDao;
import com.newposition.client.domain.NpClient;
import com.newposition.client.service.NpClientCompanyInformationService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

public class NpClientCompanyInformationServiceImpl implements NpClientCompanyInformationService
{

    private NpClientCompanyInformationDao npClientCompanyInformationDao;

    private NpUserService npUserService;

    public void setNpUserService(NpUserService npUserService)
    {
        this.npUserService = npUserService;
    }

    public NpClientCompanyInformationDao getNpClientCompanyInformationDao()
    {
        return npClientCompanyInformationDao;
    }

    public void setNpClientCompanyInformationDao(NpClientCompanyInformationDao npClientCompanyInformationDao)
    {
        this.npClientCompanyInformationDao = npClientCompanyInformationDao;
    }

    @Override
    public NpClient findByUserName(String userName)
    {
        NpUser npUser = npUserService.findByUserName(userName);
        return npClientCompanyInformationDao.findByUserName(npUser.getId());
    }
}
