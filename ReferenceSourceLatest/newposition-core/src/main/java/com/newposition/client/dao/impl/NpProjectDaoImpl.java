package com.newposition.client.dao.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.client.dao.NpProjectDao;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.domain.NpClientProjectDocument;
import com.newposition.common.domain.NpDocumentTypes;

public class NpProjectDaoImpl implements NpProjectDao
{

    private HibernateTemplate hibernateTemplate;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public void saveProject(NpClientProject npProject, NpClientProjectDocument projectDoc)
    {
        NpDocumentTypes docType = new NpDocumentTypes();
        docType.setDocumentTypeID(1);
        projectDoc.setDocumentTypeID(docType);
        Long id = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(npProject);
        projectDoc.setClientProjectID(id);
        getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(projectDoc);
    }

/*	@Override
    public List<NpClientProject> getAllClientProjects(NpClientImpl npClient) {
		List<NpClientProject> projects=getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createCriteria(NpClientProject.class).add(Restrictions.eq("npClient", npClient)).list();
		return projects;
	}*/

    @SuppressWarnings("unchecked")
    @Override
    public List<NpClientProject> getAllClientProjects(NpClientImpl npClient)
    {
        List<NpClientProject> projects = (List<NpClientProject>) getHibernateTemplate().find("from NpClientProject np where np.npClient.id=?", npClient.getId());
        return projects;
    }

    @Override
    public NpClientProject getNpProjectById(int id)
    {
        NpClientProject npClientProject = (NpClientProject) getHibernateTemplate().getSessionFactory().getCurrentSession()
                .createCriteria(NpClientProject.class).add(Restrictions.eq("clientProjectID", new Long(id))).uniqueResult();
        return npClientProject;
    }

    @Override
    public void deleteProject(NpClientProject npClientProject)
    {
        getHibernateTemplate().getSessionFactory().getCurrentSession().delete(npClientProject);
    }

    @Override
    public NpClientProject updateNpClientProject(NpClientProject npClientProject)
    {
        System.out.println("Updating");
        System.out.println(npClientProject.getProjectDescription());
        System.out.println(npClientProject.getProjectOverview());
        getHibernateTemplate().getSessionFactory().getCurrentSession().update(npClientProject);
        return npClientProject;
    }
}
