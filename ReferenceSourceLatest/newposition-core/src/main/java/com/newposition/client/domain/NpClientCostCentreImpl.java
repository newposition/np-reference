package com.newposition.client.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "cl_cost_centre", catalog = "npdbs")
public class NpClientCostCentreImpl implements NpClientCostCentre, Comparable<NpClientCostCentreImpl>
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "CostCentreID", unique = true, nullable = false)
    private Long costCentreId;

    @Column(name = "CostCentreCode", length = 50)
    private String costCentreCode;

    @Column(name = "CostCentreName", length = 50)
    private String costCentreName;

    @Column(name = "CostCentreOwner", length = 50)
    private String costCentreOwner;

    @ManyToOne(targetEntity = NpClientImpl.class)
    @JoinColumn(name = "ClientID", nullable = false)
    private NpClientImpl npClient;

    @Column(name = "CostCentreLocation", length = 30)
    private String costCentreLocation;

    @Column(name = "CostCentreSystem", length = 30)
    private String costCentreSystem;

    @Temporal(TemporalType.DATE)
    @Column(name = "LastActiveDate", length = 30)
    private Date lastActiveDate;

    @Column(name = "Status", length = 15)
    private String status;

    @Column(name = "Description", length = 50)
    private String description;

    @Transient
    private String videoUrl;

    public String getVideoUrl()
    {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl)
    {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString()
    {
        return "NpClientCostCentreImpl [costCentreId=" + costCentreId
                + ", costCentreCode=" + costCentreCode + ", costCentreName="
                + costCentreName + ", costCentreOwner=" + costCentreOwner
                + ", npClient=" + npClient + ", costCentreLocation="
                + costCentreLocation + ", costCentreSystem=" + costCentreSystem
                + ", lastActiveDate=" + lastActiveDate + ", status=" + status
                + ", description=" + description
                + ", costCentreStatus=" + costCentreStatus + "]";
    }

    @OneToOne
    @JoinColumn(name = "CostCenterStatusID", nullable = false)
    private NpClientCostCentreStatus costCentreStatus;

    @OneToOne
    @JoinColumn(name = "CostCentreID", nullable = false)
    private NpClientCostCentreDocument costCentreDoc;

    @Override
    public NpClientCostCentreDocument getCostCentreDoc()
    {
        return costCentreDoc;
    }

    @Override
    public void setCostCentreDoc(NpClientCostCentreDocument costCentreDoc)
    {
        this.costCentreDoc = costCentreDoc;
    }

    @Override
    public String getCostCentreOwner()
    {

        return costCentreOwner;
    }

    @Override
    public void setCostCentreOwner(String costCentreOwner)
    {

        this.costCentreOwner = costCentreOwner;
    }

    @Override
    public String getCostCentreLocation()
    {

        return costCentreLocation;
    }

    @Override
    public void setCostCentreLocation(String costCentreLocation)
    {

        this.costCentreLocation = costCentreLocation;
    }

    @Override
    public String getCostCentreSystem()
    {

        return costCentreSystem;
    }

    @Override
    public void setCostCentreSystem(String costCentreSystem)
    {

        this.costCentreSystem = costCentreSystem;
    }

    @Override
    public Date getLastActiveDate()
    {

        return lastActiveDate;
    }

    @Override
    public void setLastActiveDate(Date lastActiveDate)
    {

        this.lastActiveDate = lastActiveDate;
    }

    @Override
    public String getStatus()
    {

        return status;
    }

    @Override
    public void setStatus(String status)
    {

        this.status = status;
    }

    @Override
    public NpClientCostCentreStatus getCostCentreStatus()
    {

        return costCentreStatus;
    }

    @Override
    public void setCostCentreStatus(NpClientCostCentreStatus costCentreStatus)
    {

        this.costCentreStatus = costCentreStatus;
    }

    @Override
    public NpClientImpl getNpClient()
    {
        return npClient;
    }

    @Override
    public void setNpClient(NpClientImpl npClient)
    {

        this.npClient = npClient;
    }

    @Override
    public Long getCostCentreId()
    {
        return costCentreId;
    }

    @Override
    public void setCostCentreId(Long costCentreId)
    {
        this.costCentreId = costCentreId;
    }

    @Override
    public String getCostCentreCode()
    {
        return costCentreCode;
    }

    @Override
    public void setCostCentreCode(String costCentreCode)
    {
        this.costCentreCode = costCentreCode;
    }

    @Override
    public String getDescription()
    {
        return description;
    }

    @Override
    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String getCostCentreName()
    {

        return costCentreName;
    }

    @Override
    public void setCostCentreName(String costCentreName)
    {

        this.costCentreName = costCentreName;
    }

    @Override
    public int hashCode()
    {
        // TODO Auto-generated method stub
        return this.costCentreName.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        long id1 = ((NpClientCostCentreImpl) obj).getCostCentreId();
        long id2 = this.costCentreId;
        return (id1 == id2);
    }

    @Override
    public int compareTo(NpClientCostCentreImpl o)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    @JsonIgnore
    public static void sortByCostcentreCode(List<NpClientCostCentreImpl> costCentrelist)
    {

        Comparator<NpClientCostCentreImpl> sortByCostCentreName = new Comparator<NpClientCostCentreImpl>()
        {
            @Override
            public int compare(NpClientCostCentreImpl npCostCentre1, NpClientCostCentreImpl npCostCentre2)
            {

                return npCostCentre1.getCostCentreCode().compareTo(npCostCentre2.getCostCentreCode());
            }
        };
        Collections.sort(costCentrelist, sortByCostCentreName);
    }

    @JsonIgnore
    public static void sortByCostCentreName(List<NpClientCostCentreImpl> costCentrelist)
    {

        Comparator<NpClientCostCentreImpl> sortByCostcentreName = new Comparator<NpClientCostCentreImpl>()
        {
            @Override
            public int compare(NpClientCostCentreImpl costcentre1, NpClientCostCentreImpl costcentre2)
            {

                return costcentre1.getCostCentreName().compareTo(costcentre2.getCostCentreName());
            }
        };
        Collections.sort(costCentrelist, sortByCostcentreName);
    }

    @JsonIgnore
    public static void sortByCostCentreDescription(List<NpClientCostCentreImpl> costCentrelist)
    {

        Comparator<NpClientCostCentreImpl> sortByDescription = new Comparator<NpClientCostCentreImpl>()
        {
            @Override
            public int compare(NpClientCostCentreImpl costcentre1, NpClientCostCentreImpl costcentre2)
            {

                return costcentre1.getDescription().compareTo(costcentre2.getDescription());
            }
        };
        Collections.sort(costCentrelist, sortByDescription);
    }

    @JsonIgnore
    public static void sortByCostCenterLocation(List<NpClientCostCentreImpl> costCentrelist)
    {

        Comparator<NpClientCostCentreImpl> sortByCostCenter = new Comparator<NpClientCostCentreImpl>()
        {
            @Override
            public int compare(NpClientCostCentreImpl costcentre1, NpClientCostCentreImpl costcentre2)
            {

                return costcentre1.getCostCentreLocation().compareTo(costcentre2.getCostCentreLocation());
            }
        };
        Collections.sort(costCentrelist, sortByCostCenter);
    }

    @JsonIgnore
    public static void sortByCostCentreSystem(List<NpClientCostCentreImpl> costCentrelist)
    {

        Comparator<NpClientCostCentreImpl> sortByProjectOwner = new Comparator<NpClientCostCentreImpl>()
        {
            @Override
            public int compare(NpClientCostCentreImpl costcentre1, NpClientCostCentreImpl costcentre2)
            {

                return costcentre1.getCostCentreSystem().compareTo(costcentre2.getCostCentreSystem());
            }
        };
        Collections.sort(costCentrelist, sortByProjectOwner);
    }
}

