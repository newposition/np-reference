package com.newposition.client.dao;

import java.util.List;

import com.newposition.common.dao.GenericDao;
import com.newposition.client.domain.NpClientUser;

public interface NpClientUserDao extends GenericDao<NpClientUser, Integer>
{

    public List<NpClientUser> fetchAllClientUsersByClientId(Integer id);
}
