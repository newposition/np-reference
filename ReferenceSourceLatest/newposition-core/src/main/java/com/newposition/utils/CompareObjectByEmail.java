/**
 *
 */
package com.newposition.utils;

import java.util.Comparator;

import com.newposition.common.domain.NpUser;

/**
 * @author Sachi N
 */
@SuppressWarnings("rawtypes")
public class CompareObjectByEmail implements Comparator
{

    /**
     * @param firstObj and  secondObj
     *                 method compares the Email String of firstObj with Email String of secondObj and return result
     */
    @Override
    public int compare(Object firstObj, Object secondObj)
    {
        String firstName1 = "", firstName2 = "";
        if (firstObj instanceof NpUser)
        {
            firstName1 = ((NpUser) firstObj).getPrimaryEmail();

            firstName2 = ((NpUser) secondObj).getPrimaryEmail();
        }
        return firstName1.compareTo(firstName2);
    }
}
