/**
 *
 */
package com.newposition.utils;

import java.util.Comparator;

import com.newposition.common.domain.NpUser;

/**
 * @author Sachi N
 */
@SuppressWarnings("rawtypes")
public class CompareByFirstName implements Comparator
{

    /**
     * @param firstObj and  secondObj
     *                 method compares the name String of firstObj with name String of secondObj and return result
     */
    @Override
    public int compare(Object firstObj, Object secondObj)
    {

        String firstName1 = ((NpUser) firstObj).getFirstName().toUpperCase();

        String firstName2 = ((NpUser) secondObj).getFirstName().toUpperCase();

        return firstName1.compareTo(firstName2);
    }
}
