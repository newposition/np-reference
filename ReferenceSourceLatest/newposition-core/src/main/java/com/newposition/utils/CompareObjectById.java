/**
 *
 */
package com.newposition.utils;

import java.util.Comparator;

import com.newposition.common.domain.NpUser;

/**
 * @author Sachi N
 */
@SuppressWarnings("rawtypes")
public class CompareObjectById implements Comparator
{

    /**
     * @param firstObj and  secondObj
     *                 method compares the object Id of firstObj with  Id of secondObj and return result in ascending order
     */
    @Override
    public int compare(Object firstObj, Object secondObj)
    {
        Integer firstName1 = 0, firstName2 = 0;
        if (firstObj instanceof NpUser)
        {
            firstName1 = ((NpUser) firstObj).getId();

            firstName2 = ((NpUser) secondObj).getId();
        }
        return firstName1.compareTo(firstName2);
    }
}
