package com.newposition.audit;

import java.util.Date;

public interface Auditable
{

    public String getCreateBy();

    public void setCreateBy(String createBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
