/**
 *
 */
package com.newposition.candidate.domain;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/*
 * @author ravi
 *
 *
 */
@Entity
@Table(name = "np_candidate_curremp_roles", catalog = "npdbs")
public class NpCandidateCurrentEmpRoles implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "candidateEmployerId", column = @Column(name = "CandidateEmployerId", nullable = false)),
            @AttributeOverride(name = "employmentRoleID", column = @Column(name = "EmploymentRoleID", nullable = false))})
    private NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId;

    public NpCandidateCurrentEmpRolesId getNpCandidateCurrentEmpRolesId()
    {
        return npCandidateCurrentEmpRolesId;
    }

    public void setNpCandidateCurrentEmpRolesId(NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId)
    {
        this.npCandidateCurrentEmpRolesId = npCandidateCurrentEmpRolesId;
    }
}
