package com.newposition.candidate.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.newposition.candidate.dao.NpCandidateEmploymentDao;
import com.newposition.candidate.dao.NpCandidateMyaccountDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidatePreferenceForm;
import com.newposition.candidate.service.NpCandidateMyaccountService;

public class NpCandidateMyaccountServiceImpl implements
        NpCandidateMyaccountService
{

    private NpCandidateMyaccountDao npCandidateMyaccountDao;

    @Transactional
    @Override
    public void updateCandidatePreferences(
            NpCandidatePreferenceForm npCandidatePreferenceForm,
            NpCandidate npCandidate)
    {

        npCandidateMyaccountDao.saveOrUpdateCandidatePreferences(
                npCandidatePreferenceForm, npCandidate);
    }

    @Override
    public NpCandidatePreference getCandidatePreferences(NpCandidate npCandidate)
    {

        return npCandidateMyaccountDao.getCandidatePreferences(npCandidate);
    }

    @Transactional
    @Override
    public void updateCandidateDocument(NpCandidateDocument candidateDocument)
    {
        npCandidateMyaccountDao.updateCandidateDocument(candidateDocument);
    }

    @Transactional
    @Override
    public void saveCandidatePreference(
            NpCandidatePreference candidatePreference)
    {
        npCandidateMyaccountDao.saveCandidatePreference(candidatePreference);
    }

    @Transactional
    @Override
    public void updateCandidateEmployer(NpCandidateEmployer candidateEmployer)
    {
        npCandidateMyaccountDao.updateCandidateEmployer(candidateEmployer);
    }

    @Override
    public boolean checkEmailExists(String primaryEmail)
    {
        return npCandidateMyaccountDao.checkEmailExists(primaryEmail);
    }

    @Override
    @Transactional
    public void updateCandidate(NpCandidateImpl npCandidate)
    {
        npCandidateMyaccountDao.updateCandidate(npCandidate);
    }

    @Override
    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId)
    {
        // TODO Auto-generated method stub
        return npCandidateMyaccountDao.getNpCandidateEmployer(candidateId);
    }

    public NpCandidateMyaccountDao getNpCandidateMyaccountDao()
    {
        return npCandidateMyaccountDao;
    }

    public void setNpCandidateMyaccountDao(
            NpCandidateMyaccountDao npCandidateMyaccountDao)
    {
        this.npCandidateMyaccountDao = npCandidateMyaccountDao;
    }
}
