package com.newposition.candidate.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;

import com.newposition.candidate.dao.NpCandidateDao;
import com.newposition.candidate.dao.NpCandidateEducationRegisterDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateEducationForm;
import com.newposition.candidate.service.NpCandidateEducationRegisterService;

public class NpCandidateEducationRegisterServiceImpl implements NpCandidateEducationRegisterService
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateEducationRegisterServiceImpl.class);

    private NpCandidateEducationRegisterDao npCandidateEducationRegisterDao;

    private NpCandidateDao npCandidateDao;

    public void setNpCandidateEducationRegisterDao(NpCandidateEducationRegisterDao npCandidateEducationRegisterDao)
    {
        this.npCandidateEducationRegisterDao = npCandidateEducationRegisterDao;
    }

    public void setNpCandidateDao(NpCandidateDao npCandidateDao)
    {
        this.npCandidateDao = npCandidateDao;
    }

    @Override
    @Transactional
    public void saveEducationHistory(NpCandidateEducationForm educationForm, int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        npCandidateEducationRegisterDao.saveEducationHistory(educationForm, npCandidate);
    }

    @Override
    @Transactional
    public void saveQualification(NpCandidateEducationForm educationForm, int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        npCandidateEducationRegisterDao.saveQualification(educationForm, npCandidate);
    }

    @Override
    @Transactional
    public void saveCertifications(NpCandidateEducationForm educationForm, int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        npCandidateEducationRegisterDao.saveCertifications(educationForm, npCandidate);
    }

    @Override
    @Transactional
    public List<String> getInstituteNames()
    {
        List<String> instituteNames = npCandidateEducationRegisterDao.getInstituteNames();
        return instituteNames;
    }

    @Override
    public List<NpCandidateEducationForm> getEducationHistories(int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterDao.getEducationHistories(npCandidate);
        return npCandidateEducationForms;
    }

    @Override
    public List<NpCandidateEducationForm> getCandidateCertificates(int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterDao.getCandidateCertificates(npCandidate);
        return npCandidateEducationForms;
    }

    @Override
    public List<String> getLanguages()
    {

        List<String> lanuages = npCandidateEducationRegisterDao.getLanguages();
        return lanuages;
    }

    @Override
    @Transactional
    public void removeEducation(int id, int candidateId)
    {
        npCandidateEducationRegisterDao.removeEducation(id, candidateId);
    }

    @Override
    @Transactional
    public void removeCertificate(int id, int candidateId)
    {
        npCandidateEducationRegisterDao.removeCertificate(id, candidateId);
    }

    @Override
    @Transactional
    public List<String> saveLanguages(List<String> languageNames, List<String> languageFluencies, int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        languageNames = npCandidateEducationRegisterDao.saveLanguages(languageNames, languageFluencies, npCandidate);
        return languageNames;
    }

    @Override
    @Transactional
    public List<String> saveInterests(List<String> interestNames, int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        interestNames = npCandidateEducationRegisterDao.saveInterests(interestNames, npCandidate);
        return interestNames;
    }

    @Override
    public List<NpCandidateEducationForm> getQualifications(int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterDao.getQualifications(npCandidate);
        return npCandidateEducationForms;
    }

    @Override
    @Transactional
    public void removeQualification(int id, int candidateId)
    {
        npCandidateEducationRegisterDao.removeQualification(id, candidateId);
    }

    @Override
    @Transactional
    public void removeLanguage(String languageName, int candidateId)
    {
        npCandidateEducationRegisterDao.removeLanguage(languageName, candidateId);
    }

    @Override
    @Transactional
    public void removeInterest(String interestName, int candidateId)
    {
        npCandidateEducationRegisterDao.removeInterest(interestName, candidateId);
    }

    @Override
    public List<String> getCandidateLanguageNames(int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        List<String> candidateLanguageNames = npCandidateEducationRegisterDao.getCandidateLanguageNames(npCandidate);
        return candidateLanguageNames;
    }

    @Override
    public List<String> getCandidateInterestNames(int candidateId)
    {
        NpCandidate npCandidate = npCandidateDao.findByID(candidateId);
        List<String> candidateInterestNames = npCandidateEducationRegisterDao.getCandidateInterestNames(npCandidate);
        return candidateInterestNames;
    }
}
