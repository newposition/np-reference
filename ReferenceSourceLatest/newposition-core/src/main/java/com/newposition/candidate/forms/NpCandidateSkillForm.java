
/**
 *
 */
package com.newposition.candidate.forms;

import java.io.Serializable;

/*
 * @author ravi
 *
 *
 */
public class NpCandidateSkillForm implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int id;

    private String candidateEmployerId;

    private String skill;

    private String experienceLevel;

    private String expertLevel;

    private String companyName;

    private String domainType;

    private boolean inCurrentCompany;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public boolean isInCurrentCompany()
    {
        return inCurrentCompany;
    }

    public void setInCurrentCompany(boolean inCurrentCompany)
    {
        this.inCurrentCompany = inCurrentCompany;
    }

    public String getExperienceLevel()
    {
        return experienceLevel;
    }

    public String getSkill()
    {
        return skill;
    }

    public void setSkill(String skill)
    {
        this.skill = skill;
    }

    public void setExperienceLevel(String experienceLevel)
    {
        this.experienceLevel = experienceLevel;
    }

    public String getExpertLevel()
    {
        return expertLevel;
    }

    public void setExpertLevel(String expertLevel)
    {
        this.expertLevel = expertLevel;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCandidateEmployerId()
    {
        return candidateEmployerId;
    }

    public void setCandidateEmployerId(String candidateEmployerId)
    {
        this.candidateEmployerId = candidateEmployerId;
    }

    public String getDomainType()
    {
        return domainType;
    }

    public void setDomainType(String domainType)
    {
        this.domainType = domainType;
    }
}
