/**
 *
 */
package com.newposition.candidate.service.impl;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.thymeleaf.context.Context;

import com.newposition.candidate.dao.NpCandidateSponsorDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.NpCandidateReferenceForm;
import com.newposition.candidate.service.NpCandidateSponsorService;
import com.newposition.infra.mail.NpMailService;

/**
 * @author Sachi Nayak
 *         Implementation of {@link NpCandidateSponsorService}
 */
public class NpCandidateSponsorServiceImpl implements NpCandidateSponsorService
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateSponsorServiceImpl.class);

    private NpCandidateSponsorDao npCandidateSponsorDao;

    private NpMailService npMailService;

    private String imageContext;

    private String supplyReferenceLink;

    private String loginPageLink;

    public void setLoginPageLink(String loginPageLink)
    {
        this.loginPageLink = loginPageLink;
    }

    public String getSupplyReferenceLink()
    {
        return supplyReferenceLink;
    }

    public void setSupplyReferenceLink(String supplyReferenceLink)
    {
        this.supplyReferenceLink = supplyReferenceLink;
    }

    public void setNpCandidateSponsorDao(NpCandidateSponsorDao npCandidateSponsorDao)
    {
        this.npCandidateSponsorDao = npCandidateSponsorDao;
    }

    public NpCandidateSponsorDao getNpCandidateSponsorDao()
    {
        return npCandidateSponsorDao;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    /*
     * @return list of Sponsors details
     */
    @Override
    public List<NpCandidateReferenceForm> getListOfSponsors(NpCandidate npCandidate)
    {

        List<NpCandidateReference> listOfCandidateReferences = npCandidateSponsorDao.getListOfSponsors(npCandidate);

        // converting NpCandidateRefernce to NpCandidateRefernceModel

        List<NpCandidateReferenceForm> listOfCandidateRefernceModels = new ArrayList<NpCandidateReferenceForm>();
        NpCandidateReferenceForm npCandidateReferenceModel = null;
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        for (NpCandidateReference reference : listOfCandidateReferences)
        {

            npCandidateReferenceModel = new NpCandidateReferenceForm();
            npCandidateReferenceModel.setId(reference.getId());
            npCandidateReferenceModel.setFirstName(reference.getFirstName());
            npCandidateReferenceModel.setSurName(reference.getSurName());
            npCandidateReferenceModel.setEmail(reference.getEmail());
            npCandidateReferenceModel.setRelationship(reference.getRelationship());
            npCandidateReferenceModel.setCoveringNote(reference.getCoveringNote());
            npCandidateReferenceModel.setDateFrom(format.format(reference.getDateFrom()));
            if (reference.getDateTo() != null)
            {
                npCandidateReferenceModel.setDateTo(format.format(reference.getDateTo()));
            }
            else
            {
                npCandidateReferenceModel.setDateTo("current");
            }

            listOfCandidateRefernceModels.add(npCandidateReferenceModel);
        }

        return listOfCandidateRefernceModels;
    }

    /*
     * @params email, npCandidate
     * This method returns true/false according to the validation
     */
    @Override
    public boolean isSponsorEmailExist(String email, NpCandidate npCandidate)
    {

        return npCandidateSponsorDao.isSponsorEmailExist(email, npCandidate);
    }

    /*
     * @params npCandidateReferenceModel
     * This method persists npCandidateReferenceModel
     */
    @Override
    public void saveMySponsorDetail(NpCandidateReferenceForm npCandidateReferenceForm)
    {

        npCandidateSponsorDao.saveMySponsorDetails(npCandidateReferenceForm);
    }

    /*
    * @argument  List<NpCandidateReferenceModel>
    *
    *  List of added Sponsor details
    */
    @Override
    public boolean sendEmailToAll(List<NpCandidateReferenceForm> referenceList, NpCandidate npCandidate)
    {
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());

        LOG.info(toDay + "- " + imageContext);
        for (NpCandidateReferenceForm referenceModel : referenceList)
        {

            String candidateName = npCandidate.getFirstName() + " " + npCandidate.getLastName();
            String information = "Reference Request for " + candidateName;
            Context context = new Context();
            context.setVariable("firstName", referenceModel.getFirstName());
            context.setVariable("surName", referenceModel.getSurName());
            context.setVariable("candidateName", candidateName);
            context.setVariable("supplyReferenceLink", supplyReferenceLink);
            context.setVariable("loginPageLink", loginPageLink);

            context.setVariable("personalStatement", referenceModel.getCoveringNote());
            context.setVariable("template", "sponsor-mail.html");
            context.setVariable("date", toDay);
            context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");

            npMailService.sendMail(information, referenceModel.getEmail(), context);
            LOG.info("mail sent to : " + referenceModel.getEmail());
        }
        return false;
    }

    @Override
    public boolean isEditingDuplicateSponsor(NpCandidate npCandidate, String editSponsorId, String email)
    {

        Integer sponsorId = new Integer(editSponsorId.trim());

        NpCandidateReference npCandidateReference = npCandidateSponsorDao.fetchSponsorById(sponsorId);

        if (npCandidateReference != null)
        {
            if ((!npCandidateReference.getEmail().equalsIgnoreCase(email.trim()))
                    && npCandidateSponsorDao.isSponsorEmailExist(email, npCandidate))
            {

                return true;
            }
        }
        return false;
    }
}
