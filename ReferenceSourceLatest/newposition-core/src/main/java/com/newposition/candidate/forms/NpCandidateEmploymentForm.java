package com.newposition.candidate.forms;

public class NpCandidateEmploymentForm
{

    private String employerName;
    private String jobTitle;
    private String startDate;
    private String endDate;
    private String corpTitle;
    private String achievements;
    private String employerId;

    public String getAchievements()
    {
        return achievements;
    }

    public void setAchievements(String achievements)
    {
        this.achievements = achievements;
    }

    public String getEmployerId()
    {
        return employerId;
    }

    public void setEmployerId(String employerId)
    {
        this.employerId = employerId;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public String getEmployerName()
    {
        return employerName;
    }

    public void setEmployerName(String employerName)
    {
        this.employerName = employerName;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getCorpTitle()
    {
        return corpTitle;
    }

    public void setCorpTitle(String corpTitle)
    {
        this.corpTitle = corpTitle;
    }
}
