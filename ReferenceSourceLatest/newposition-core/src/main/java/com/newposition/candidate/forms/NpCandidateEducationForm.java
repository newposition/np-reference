package com.newposition.candidate.forms;

import java.io.Serializable;

public class NpCandidateEducationForm implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public int id;
    public String institution;
    public String awardType;
    public String awardSubject;
    public String level;
    public String startDate;
    public String endDate;
    public String instituteLogo;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getInstitution()
    {
        return institution;
    }

    public void setInstitution(String institution)
    {
        this.institution = institution;
    }

    public String getAwardType()
    {
        return awardType;
    }

    public void setAwardType(String awardType)
    {
        this.awardType = awardType;
    }

    public String getAwardSubject()
    {
        return awardSubject;
    }

    public void setAwardSubject(String awardSubject)
    {
        this.awardSubject = awardSubject;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public String getInstituteLogo()
    {
        return instituteLogo;
    }

    public void setInstituteLogo(String instituteLogo)
    {
        this.instituteLogo = instituteLogo;
    }
}
