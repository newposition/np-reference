/**
 *
 */
package com.newposition.candidate.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

/*
 * @author ravi
 *
 *
 */
@Embeddable
public class NpCandidateEmployerToolsId implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int candidateEmployerId;
    private int toolID;

    public int getCandidateEmployerId()
    {
        return candidateEmployerId;
    }

    public void setCandidateEmployerId(int candidateEmployerId)
    {
        this.candidateEmployerId = candidateEmployerId;
    }

    public int getToolID()
    {
        return toolID;
    }

    public void setToolID(int toolID)
    {
        this.toolID = toolID;
    }
}
