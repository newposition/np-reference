package com.newposition.candidate.service.impl;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.context.Context;

import com.newposition.candidate.dao.NpCandidateDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleImpl;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserAttempts;
import com.newposition.common.domain.NpUserAttemptsImpl;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.domain.NpUserRoleId;
import com.newposition.common.domain.NpUserTypes;
import com.newposition.infra.mail.NpMailService;
import com.newposition.util.DataSecurity;

public class NpCadnidateServiceImpl implements NpCandidateService
{

    private PasswordEncoder passwordEncoder;

    private NpUserDao npUserDao;

    private NpCandidateDao npCandidateDao;

    private NpMailService npMailService;

    private String imageContext;

    private String loginPageLink;

    public void setImageContext(String imageContext)
    {
        this.imageContext = imageContext;
    }

    public void setNpUserDao(NpUserDao npUserDao)
    {
        this.npUserDao = npUserDao;
    }

    public void setNpCandidateDao(NpCandidateDao npCandidateDao)
    {
        this.npCandidateDao = npCandidateDao;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder)
    {
        this.passwordEncoder = passwordEncoder;
    }

    public void setNpMailService(NpMailService npMailService)
    {
        this.npMailService = npMailService;
    }

    public void setLoginPageLink(String loginPageLink)
    {
        this.loginPageLink = loginPageLink;
    }

    @Transactional
    @Override
    public NpCandidate quickRegister(NpCandidate candidate, String password)
    {

        candidate.setEncryptedPassword(passwordEncoder.encode(password));

        NpUserAttempts npUserAttempts = new NpUserAttemptsImpl();
        npUserAttempts.setAttempts(0);

        candidate.setNpUserStatus(npUserDao.getNpUserStatusById(2));

        candidate.setNpUserAttempts(npUserAttempts);

        NpCandidate npCandidate = npCandidateDao.save(candidate);

        NpUserRole npUserRole = new NpUserRole();
        NpUserRoleId npUserRoleId = new NpUserRoleId();

//		npUserRoleId.setRoleId(2);
        NpRoleImpl role = (NpRoleImpl) npUserDao.findRoleByRoleName("REGISTERED_USER");
        npUserRoleId.setRoleId(role.getId());

        npUserRoleId.setUserId(npCandidate.getId());
        npUserRole.setId(npUserRoleId);
        npUserDao.updateNpCandidateRole(npUserRole);

        Long currentTime = System.currentTimeMillis();
        String currentTimeString = "";
        currentTimeString = DatatypeConverter.printBase64Binary(currentTime.toString().getBytes());
        String userId = DatatypeConverter.printBase64Binary(npCandidate.getId().toString().getBytes());
        String directActivationLink = loginPageLink + "?id=" + userId + "&dur=" + currentTimeString;

        String subject = "NewPosition Account activation Request";

        Context context = new Context();
        context.setVariable("firstName", candidate.getFirstName());
        context.setVariable("lastName", candidate.getLastName());
        context.setVariable("directActivationLink", directActivationLink);
        context.setVariable("headerImageSrc", imageContext + "/./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("template", "quickRegistration-email.html");

        npMailService.sendMail(subject, candidate.getPrimaryEmail(), context);

        return candidate;
    }

    @Transactional
    @Override
    public NpCandidate candidateRegister(NpCandidate candidate, String password)
    {

        candidate.setEncryptedPassword(passwordEncoder.encode(password));

        NpUserAttempts npUserAttempts = new NpUserAttemptsImpl();
        npUserAttempts.setAttempts(0);

        candidate.setNpUserStatus(npUserDao.getNpUserStatusById(2));

        candidate.setNpUserAttempts(npUserAttempts);

        NpCandidate npCandidate = npCandidateDao.save(candidate);

        NpUserRole npUserRole = new NpUserRole();
        NpUserRoleId npUserRoleId = new NpUserRoleId();

        NpRoleImpl role = (NpRoleImpl) npUserDao.findRoleByRoleName("REGISTERED_USER");
        npUserRoleId.setRoleId(role.getId());
        npUserRoleId.setUserId(npCandidate.getId());
        npUserRole.setId(npUserRoleId);
        npUserDao.updateNpCandidateRole(npUserRole);
        /*String encryptId = null;
		try {
			encryptId = passwordEncoder.encode(npCandidate.getId() + "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}
		String directActivationLink = loginPageLink + "?id=" + encryptId;

		String subject = "NewPosition Account activation";

		Context context = new Context();
		context.setVariable("firstName", candidate.getFirstName());
		context.setVariable("lastName", candidate.getLastName());
		context.setVariable("directActivationLink", directActivationLink);
		context.setVariable("headerImageSrc", imageContext+"./resources/templating-kit/img/emaillogo_header.jpg");
		context.setVariable("template", "quickRegistration-email.html");


		npMailService.sendMail(subject, candidate.getPrimaryEmail(), context);*/

        return candidate;
    }

    @Override
    public NpCandidate findByID(Integer id)
    {
        // TODO Auto-generated method stub
        return npCandidateDao.findByID(id.intValue());
    }

    @Override
    public List<NpCandidateReference> getCandidateReferenceList(String referEmail)
    {
        // TODO Auto-generated method stub
        return npCandidateDao.getCandidateReferenceList(referEmail);
    }

    @Override
    public void sendRegistrationSucessMail(QuickRegistrationForm quickRegistrationForm, String[] codes, String subject)
    {

        String candidateName = quickRegistrationForm.getCandidate().getFirstName() + " " + quickRegistrationForm.getCandidate().getLastName();
        String information = subject;
        Format format = new SimpleDateFormat("MMM dd, yyyy");
        String toDay = format.format(new Date());
        Context context = new Context();
        context.setVariable("candidateName", candidateName);
        context.setVariable("loginPageLink", codes[0]);
        context.setVariable("activationPageLink", codes[2]);
        context.setVariable("activationCode", codes[1]);
        context.setVariable("template", "registration-email.html");
        context.setVariable("headerImageSrc", imageContext + "./resources/templating-kit/img/emaillogo_header.jpg");
        context.setVariable("date", toDay);

        npMailService.sendMail(information, quickRegistrationForm.getCandidate().getPrimaryEmail(), context);
    }

    @Override
    public NpCandidateReference getCurrentCandidateReference(String primaryEmail, Integer candidateId)
    {

        return npCandidateDao.getCurrentCandidateReference(primaryEmail, candidateId);
    }

    @Transactional
    @Override
    public void varifyReference(NpCandidateReference npCandidateReference)
    {

        npCandidateDao.varifyReference(npCandidateReference);
    }
}
