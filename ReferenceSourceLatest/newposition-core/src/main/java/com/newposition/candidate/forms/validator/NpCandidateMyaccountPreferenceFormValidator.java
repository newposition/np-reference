package com.newposition.candidate.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.newposition.candidate.forms.NpCandidatePreferenceForm;

public class NpCandidateMyaccountPreferenceFormValidator implements Validator
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateMyaccountPreferenceFormValidator.class);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return clazz.equals(NpCandidatePreferenceForm.class);
    }

    @Override
    public void validate(Object object, Errors errors)
    {
        NpCandidatePreferenceForm npCandidatePreferenceForm = (NpCandidatePreferenceForm) object;

        if (StringUtils.isEmpty(npCandidatePreferenceForm.getPrimaryEmail()))
        {
            errors.rejectValue("primaryEmail", "candidate.myaccount.primary.email.validation.empty");
        }

        else if (!isValidEmail(npCandidatePreferenceForm.getPrimaryEmail()))
        {
            errors.rejectValue("primaryEmail", "candidate.myaccount.primary.email.validation.empty");
        }

        if (StringUtils.isNotEmpty(npCandidatePreferenceForm.getSecondaryEmail()) && !isValidEmail(npCandidatePreferenceForm.getSecondaryEmail()))
        {
            errors.rejectValue("secondaryEmail", "candidate.myaccount.primary.email.validation.invalid");
        }
        if (StringUtils.isNotEmpty(npCandidatePreferenceForm.getTertiaryEmail()) && !isValidEmail(npCandidatePreferenceForm.getTertiaryEmail()))
        {
            errors.rejectValue("tertiaryEmail", "candidate.myaccount.primary.email.validation.invalid");
        }
    }

    private boolean isValidEmail(String email)
    {
        String emailPattern = "^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]+\\.[a-zA-Z]{2,}$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher match = pattern.matcher(email.trim());

        return match.find();
    }
}
