package com.newposition.candidate.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.newposition.candidate.dao.NpCandidateDao;
import com.newposition.candidate.dao.NpCandidateEmploymentDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateEmployerImpl;
import com.newposition.candidate.forms.NpCandidateEmploymentForm;
import com.newposition.candidate.service.NpCandidateEmploymentService;
import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpTool;

public class NpCandidateEmploymentServiceImpl implements NpCandidateEmploymentService
{

    private NpCandidateEmploymentDao npCandidateEmploymentDao;

    private NpCandidateDao npCandidateDao;

    public void setNpCandidateDao(NpCandidateDao npCandidateDao)
    {
        this.npCandidateDao = npCandidateDao;
    }

    public void setNpCandidateEmploymentDao(NpCandidateEmploymentDao npCandidateEmploymentDao)
    {
        this.npCandidateEmploymentDao = npCandidateEmploymentDao;
    }

    @Transactional
    @Override
    public int saveEmployment(NpCandidateEmploymentForm npCandidateEmploymentForm, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        NpCandidateEmployer npCandidateEmployer = setNpCandidateEmployer(npCandidateEmploymentForm, npCandidate);
        return npCandidateEmploymentDao.saveEmployment(npCandidateEmployer);
    }

    @Transactional
    @Override
    public void deleteEmployment(int candidateEmployerId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteEmployment(candidateEmployerId);
    }

    @Transactional
    @Override
    public void updateEmployment(NpCandidateEmploymentForm employmentForm)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.updateEmployment(employmentForm);
    }

    @Override
    public List<NpSkill> getSkills(String id)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getSkills(id);
    }

    @Override
    public List<NpEmploymentRole> getRoles(String id)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getRoles(id);
    }

    @Override
    public List<NpTool> getTools(String id)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getTools(id);
    }

    @Override
    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getNpCandidateEmployer(candidateId);
    }

    @Override
    public NpCandidate getNpCandidateById(int id)
    {
        // TODO Auto-generated method stub
        return npCandidateDao.findByID(id);
    }

    @Transactional
    @Override
    public int saveCandidateCompanySkill(String employerId, String skillId)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateCompanySkill(employerId, skillId);
    }

    @Transactional
    @Override
    public int saveCandidateCompanyRole(String employerId, String roleId)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateCompanyRole(employerId, roleId);
    }

    @Transactional
    @Override
    public int saveCandidateCompanyTool(String employerId, String toolId)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateCompanyTool(employerId, toolId);
    }

    @Transactional
    @Override
    public void deleteCandidateSkill(String employerId, String skillId, int candidateId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateSkill(employerId, skillId, candidateId);
    }

    @Transactional
    @Override
    public void deleteCandidateRole(String employerId, String skillId, int candidateId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateRole(employerId, skillId, candidateId);
    }

    @Transactional
    @Override
    public void deleteCandidateTool(String employerId, String skillId, int candidateId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateTool(employerId, skillId, candidateId);
    }

    @Transactional
    @Override
    public void deleteCandidateCompanySkill(String employerId, String skillId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateCompanySkill(employerId, skillId);
    }

    @Transactional
    @Override
    public void deleteCandidateCompanyRole(String employerId, String roleId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateCompanyRole(employerId, roleId);
    }

    @Transactional
    @Override
    public void deleteCandidateCompanyTool(String employerId, String toolId)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateCompanyTool(employerId, toolId);
    }

    @Transactional
    @Override
    public boolean saveCandidateSkill(String skill, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateSkill(skill, candidateId, employID, domainType, currentCompany);
    }

    @Transactional
    @Override
    public boolean saveCandidateRole(String role, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateRole(role, candidateId, employID, domainType, currentCompany);
    }

    @Transactional
    @Override
    public boolean saveCandidateTool(String tool, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.saveCandidateTool(tool, candidateId, employID, domainType, currentCompany);
    }

    @Override
    public List<String> getEmployerNames()
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getEmployerNames();
    }

    @Transactional
    @Override
    public void deleteCandidateTechnicalDetails(NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        npCandidateEmploymentDao.deleteCandidateTechnicalDetails(npCandidate);
    }

    public NpCandidateEmployer setNpCandidateEmployer(NpCandidateEmploymentForm npCandidateEmploymentForm, NpCandidate npCandidate)
    {
        NpCandidateEmployer candidateEmployer = new NpCandidateEmployerImpl();
        try
        {
            // setting data into NpCandidateEmployer

            //   NpCandidateEmployerId npCandidateEmployerId = new NpCandidateEmployerId();

            if (npCandidateEmploymentForm.getEmployerId() != null)
            {
                candidateEmployer.setUid(Integer.parseInt(npCandidateEmploymentForm.getEmployerId()));
            }

            int companyId = 0;
            if (npCandidateEmploymentDao.getCompanyId(npCandidateEmploymentForm.getEmployerName()) > 0)
            {
                companyId = npCandidateEmploymentDao.getCompanyId(npCandidateEmploymentForm.getEmployerName());
                candidateEmployer.setNpEmployer(npCandidateEmploymentDao.getNpEmployerById(companyId));
            }
            else
            {
                NpEmployer npEmployer = new NpEmployerImpl();
                npEmployer.setName(npCandidateEmploymentForm.getEmployerName());
                npEmployer = npCandidateEmploymentDao.saveEmployer(npEmployer);
                companyId = npEmployer.getId();
                candidateEmployer.setNpEmployer(npEmployer);
            }

            candidateEmployer.setNpCandidate(npCandidate);

            // converting string value into dates
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String dateInStringFrom = npCandidateEmploymentForm.getStartDate();
            String dateInStringTo = npCandidateEmploymentForm.getEndDate();

            Date dateFrom, dateTo;

            dateFrom = sdf.parse(dateInStringFrom);
            if (dateInStringTo.trim().equals("current") || dateInStringTo.trim().isEmpty() || dateInStringTo == null)
            {
                dateTo = null;
            }
            else
            {
                dateTo = sdf.parse(dateInStringTo);
            }

            candidateEmployer.setStartDate(dateFrom);
            candidateEmployer.setEnddate(dateTo);

            //candidateEmployer.setId(npCandidateEmployerId);
            candidateEmployer.setAchievements(npCandidateEmploymentForm.getAchievements());
            candidateEmployer.setJobTitle(npCandidateEmploymentForm.getJobTitle());
            candidateEmployer.setCorporateTitle(npCandidateEmploymentForm.getCorpTitle());
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return candidateEmployer;
    }

    @Override
    public List<String> getListOfSkills()
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getListOfSkills();
    }

    @Override
    public List<String> getListOfRoles()
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getListOfRoles();
    }

    @Override
    public List<String> getListOfTools()
    {
        // TODO Auto-generated method stub
        return npCandidateEmploymentDao.getListOfTools();
    }

    @Override
    @Transactional
    public NpSkill getSkillBySkillName(String skillName)
    {
        return npCandidateEmploymentDao.getSkillBySkillName(skillName);
    }

    @Override
    @Transactional
    public NpEmploymentRole getRoleByRoleName(String toolName)
    {

        return npCandidateEmploymentDao.getRoleByRoleName(toolName);
    }

    @Override
    @Transactional
    public NpTool getToolByToolName(String roleName)
    {
        return npCandidateEmploymentDao.getToolByToolName(roleName);
    }
}
