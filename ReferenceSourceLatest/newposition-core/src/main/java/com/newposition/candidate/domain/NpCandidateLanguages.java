package com.newposition.candidate.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "np_candidate_language", catalog = "npdbs")
public class NpCandidateLanguages implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private NpCandidateLanguageId id;

    @Column(name = "LanguageFluency", nullable = false)
    private String languageFluency;

    private NpCandidateImpl npCandidate;

    private NpLanguages npLanguages;

    public NpCandidateLanguages(NpCandidateLanguageId id)
    {
        this.id = id;
    }

    public NpCandidateLanguages()
    {
    }

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "LanguageId", column = @Column(name = "LanguageID", nullable = false)),
            @AttributeOverride(name = "candidateId", column = @Column(name = "CandidateID", nullable = false))})
    public NpCandidateLanguageId getId()
    {
        return this.id;
    }

    public void setId(NpCandidateLanguageId id)
    {
        this.id = id;
    }

    public String getLanguageFluency()
    {
        return languageFluency;
    }

    public void setLanguageFluency(String languageFluency)
    {
        this.languageFluency = languageFluency;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CandidateID", nullable = false, insertable = false, updatable = false)
    public NpCandidateImpl getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidateImpl npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LanguageID", nullable = false, insertable = false, updatable = false)
    public NpLanguages getNpLanguages()
    {
        return npLanguages;
    }

    public void setNpLanguages(NpLanguages npLanguages)
    {
        this.npLanguages = npLanguages;
    }
}

