/**
 *
 */
package com.newposition.candidate.domain;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/*
 * @author ravi
 *
 *
 */
@Entity
@Table(name = "np_candidate_employer_tools", catalog = "npdbs")
public class NpCandidateEmployerTools implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "candidateEmployerId", column = @Column(name = "CandidateEmployerId", nullable = false)),
            @AttributeOverride(name = "toolID", column = @Column(name = "ToolID", nullable = false))})
    private NpCandidateEmployerToolsId npCandidateEmployerToolsId;

    public NpCandidateEmployerToolsId getNpCandidateEmployerToolsId()
    {
        return npCandidateEmployerToolsId;
    }

    public void setNpCandidateEmployerToolsId(NpCandidateEmployerToolsId npCandidateEmployerToolsId)
    {
        this.npCandidateEmployerToolsId = npCandidateEmployerToolsId;
    }
}
