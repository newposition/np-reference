package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NpCandidateRegionId implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int npRegionId;
    private int candidateId;

    public NpCandidateRegionId()
    {
    }

    public NpCandidateRegionId(int npReligionId, int candidateId)
    {
        this.npRegionId = npReligionId;
        this.candidateId = candidateId;
    }

    @Column(name = "NpRegionId", nullable = false)
    public int getNpRegionId()
    {
        return npRegionId;
    }

    public void setNpRegionId(int npRegionId)
    {
        this.npRegionId = npRegionId;
    }

    @Column(name = "CandidateID", nullable = false)
    public int getCandidateId()
    {
        return this.candidateId;
    }

    public void setCandidateId(int candidateId)
    {
        this.candidateId = candidateId;
    }

    public boolean equals(Object other)
    {
        if ((this == other))
        {
            return true;
        }
        if ((other == null))
        {
            return false;
        }
        if (!(other instanceof NpCandidateRegionId))
        {
            return false;
        }
        NpCandidateRegionId castOther = (NpCandidateRegionId) other;

        return (this.getNpRegionId() == castOther.getNpRegionId())
                && (this.getCandidateId() == castOther.getCandidateId());
    }

    public int hashCode()
    {
        int result = 17;

        result = 37 * result + this.getNpRegionId();
        result = 37 * result + this.getCandidateId();
        return result;
    }
}
