package com.newposition.candidate.domain;

import javax.persistence.Embeddable;

@Embeddable
public class NpCandidateEmployerSkillsId implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int candidateEmployerId;

    public int getCandidateEmployerId()
    {
        return candidateEmployerId;
    }

    public void setCandidateEmployerId(int candidateEmployerId)
    {
        this.candidateEmployerId = candidateEmployerId;
    }

    private int skillId;

    public int getSkillId()
    {
        return skillId;
    }

    public void setSkillId(int skillId)
    {
        this.skillId = skillId;
    }
}