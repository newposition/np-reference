/**
 *
 */
package com.newposition.candidate.forms;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateImpl;

/*
 * @author ravi
 *
 *
 */
public class QuickRegistrationForm
{

    @Valid
    private NpCandidate candidate = new NpCandidateImpl();

    private String confirmPrimaryEmailAddress;

    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=\\S+$).{8,}$", message = "{encryptedpassword.required}")
    private String password;

    private String confirmPassword;

    private String agree;

    public String telePhone;

    public String getAgree()
    {
        return agree;
    }

    public void setAgree(String agree)
    {
        this.agree = agree;
    }

    public NpCandidate getCandidate()
    {
        return candidate;
    }

    public void setCandidate(NpCandidate candidate)
    {
        this.candidate = candidate;
    }

    public String getConfirmPrimaryEmailAddress()
    {
        return confirmPrimaryEmailAddress;
    }

    public void setConfirmPrimaryEmailAddress(String confirmPrimaryEmailAddress)
    {
        this.confirmPrimaryEmailAddress = confirmPrimaryEmailAddress;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getTelePhone()
    {
        return telePhone;
    }

    public void setTelePhone(String telePhone)
    {
        this.telePhone = telePhone;
    }
}
