package com.newposition.candidate.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.common.domain.NpUser;

/*
 * @author ravi
 *
 *
 */
public interface NpCandidate extends NpUser
{

    public String getAvailableStatus();

    public void setAvailableStatus(String availableStatus);

    public String getRegistrationStatus();

    public void setRegistrationStatus(String registrationStatus);

    public String getEligibilityCheckEu();

    public void setEligibilityCheckEu(String eligibilityCheckEu);

    public Character getPhotoVisibility();

    public void setPhotoVisibility(Character photoVisibility);

    public String getJobTitle();

    public void setJobTitle(String jobTitle);

    public String getCurrentCompany();

    public void setCurrentCompany(String currentCompany);

    public Set<NpCandidateSkills> getNpCandidateSkills();

    public void setNpCandidateSkills(Set<NpCandidateSkills> npCandidateSkills);

    public Set<NpCandidateTools> getNpCandidateTools();

    public void setNpCandidateTools(Set<NpCandidateTools> npCandidateTools);

    public Set<NpCandidateEmpRoles> getNpCandidateEmpRoles();

    public void setNpCandidateEmpRoles(Set<NpCandidateEmpRoles> npCandidateEmpRoles);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public Set<NpCandidateEmployerImpl> getNpCandidateEmployers();

    public void setNpCandidateEmployers(Set<NpCandidateEmployerImpl> npCandidateEmployers);

    public String getPersonalStatement();

    public void setPersonalStatement(String personalStatement);

    public Set<NpCandidateReference> getNpCandidateReferences();

    public void setNpCandidateReferences(Set<NpCandidateReference> npCandidateReferences);

    public Set<NpCandidateEducation> getNpCandidateEducations();

    public void setNpCandidateEducations(Set<NpCandidateEducation> npCandidateEducations);

    public Set<NpCandidateCertificate> getNpCandidateCertificates();

    public void setNpCandidateCertificates(Set<NpCandidateCertificate> npCandidateCertificates);

    public Set<NpPreferredLocation> getNpPreferredLocations();

    public void setNpPreferredLocations(Set<NpPreferredLocation> npPreferredLocations);

    public Set<NpCandidateInterests> getNpCandidateInterests();

    public void setNpCandidateInterests(Set<NpCandidateInterests> npCandidateInterests);

    public Set<NpCandidateLanguages> getNpCandidateLanguages();

    public void setNpCandidateLanguages(Set<NpCandidateLanguages> npCandidateLanguages);

    public Set<NpCandidateFutureEmployer> getNpCandidateFutureEmployers();

    public void setNpCandidateFutureEmployers(Set<NpCandidateFutureEmployer> npCandidateFutureEmployers);

    public Set<NpCandidateEligibility> getNpCandidateEligibilities();

    public void setNpCandidateEligibilities(Set<NpCandidateEligibility> npCandidateEligibilities);

    public Set<NpCandidateRegion> getNpCandidateRegions();

    public void setNpCandidateRegions(Set<NpCandidateRegion> npCandidateRegions);

    public NpCandidateDocument getNpCandidateDocument();

    public void setNpCandidateDocument(NpCandidateDocument npCandidateDocument);

    public NpCandidatePreference getNpCandidatePreference();

    public void setNpCandidatePreference(NpCandidatePreference npCandidatePreference);

    public void setModifiedDate(Date modifiedDate);

    public NpLocations getNpLocations();

    public void setNpLocations(NpLocations npLocations);
}
