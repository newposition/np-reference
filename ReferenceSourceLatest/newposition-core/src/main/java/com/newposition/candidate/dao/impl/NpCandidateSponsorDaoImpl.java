/**
 *
 */
package com.newposition.candidate.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.candidate.dao.NpCandidateSponsorDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.NpCandidateReferenceForm;

/**
 * @author Implementation of {@link NpCandidateSponsorDao}
 *         This class persists Candidate Reference Details.
 */
public class NpCandidateSponsorDaoImpl implements NpCandidateSponsorDao
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateSponsorDaoImpl.class);

    @Autowired
    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    @Transactional
    public List<NpCandidateReference> getListOfSponsors(NpCandidate npCandidate)
    {

        List<NpCandidateReference> listOfReferences = (List<NpCandidateReference>) hibernateTemplate
                .find("from NpCandidateReference where candidateId = ? ", npCandidate.getId());
        LOG.info("returning listof reference");
        return listOfReferences;
    }

	/*
     * @params npCandidateReferenceModel
     * This method persists Candidate Reference Details.
     */

    @SuppressWarnings("unchecked")
    @Transactional
    public void saveMySponsorDetails(NpCandidateReferenceForm npCandidateReferenceForm)
    {

        // converting string to date
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

        Calendar cal = Calendar.getInstance();
        Date createdDate = cal.getTime();
        Date modifiedDate = cal.getTime();
        Date from = null, to = null;
        try
        {
            from = formater.parse(npCandidateReferenceForm.getDateFrom());
            if (!"Current".equalsIgnoreCase(npCandidateReferenceForm.getDateTo()))
            {
                to = formater.parse(npCandidateReferenceForm.getDateTo());
            }
            LOG.info("from " + from + " to - " + to);
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            LOG.error("Problem in Parsing From/To date:  " + e.getStackTrace());
        }
        List<NpCandidateReference> candidateReferences = (List<NpCandidateReference>) hibernateTemplate
                .find("from NpCandidateReference where id= ?",
                        npCandidateReferenceForm.getId());
        NpCandidateReference updateReference = null;

        // setting all the values from NpCandidateReferenceModel to NpCandidateReference
        if (candidateReferences.size() == 0)
        {
            updateReference = new NpCandidateReference();
            updateReference.setNpCandidate((NpCandidateImpl) npCandidateReferenceForm.getNpCandidate());
            updateReference
                    .setCreatedBy(npCandidateReferenceForm.getNpCandidate().getFirstName());
            updateReference.setCreatedDate(createdDate);
        }
        else
        {
            updateReference = candidateReferences.get(0);
        }
        updateReference.setFirstName(npCandidateReferenceForm.getFirstName());
        updateReference.setSurName(npCandidateReferenceForm.getSurName());
        updateReference.setRelationship(npCandidateReferenceForm
                .getRelationship());
        updateReference.setReferenceVerified("waiting");
        updateReference.setEmail(npCandidateReferenceForm.getEmail());
        updateReference.setDateFrom(from);
        updateReference.setDateTo(to);
        updateReference.setCoveringNote(npCandidateReferenceForm
                .getCoveringNote());
        updateReference.setModifiedBy(npCandidateReferenceForm.getNpCandidate().getFirstName());
        updateReference.setModifiedDate(modifiedDate);

        if (candidateReferences.size() == 0)
        {
            hibernateTemplate.save(updateReference);
            npCandidateReferenceForm.setId(updateReference.getId());
        }

        LOG.info("Save Reference object with id: " + npCandidateReferenceForm.getId());
    }

    /**
     * @param email
     * @param npCandidate
     * @return
     */
    @SuppressWarnings("unchecked")
    public boolean isSponsorEmailExist(String email, NpCandidate npCandidate)
    {

        List<NpCandidateReference> listOfReferences = (List<NpCandidateReference>) hibernateTemplate
                .find("from NpCandidateReference where email= ? AND candidateId = ? ", email, npCandidate.getId());
        if (listOfReferences.size() > 0)
        {
            return true;
        }
        return false;
    }

    /**
     * @param sponsorId
     * @return NpCandidateReference
     * <p/>
     * This method returns NpCandidateReference Object by id
     */
    @Override
    public NpCandidateReference fetchSponsorById(Integer sponsorId)
    {

        NpCandidateReference npCandidateReference = hibernateTemplate.get(NpCandidateReference.class, sponsorId);
        if (npCandidateReference != null)
        {
            return npCandidateReference;
        }
        return null;
    }
}
