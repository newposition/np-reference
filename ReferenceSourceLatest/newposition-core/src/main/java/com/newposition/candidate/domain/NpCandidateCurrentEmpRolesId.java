/**
 *
 */
package com.newposition.candidate.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

/*
 * @author ravi
 *
 *
 */
@Embeddable
public class NpCandidateCurrentEmpRolesId implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int candidateEmployerId;
    private int employmentRoleID;

    public int getCandidateEmployerId()
    {
        return candidateEmployerId;
    }

    public void setCandidateEmployerId(int candidateEmployerId)
    {
        this.candidateEmployerId = candidateEmployerId;
    }

    public int getEmploymentRoleID()
    {
        return employmentRoleID;
    }

    public void setEmploymentRoleID(int employmentRoleID)
    {
        this.employmentRoleID = employmentRoleID;
    }
}
