package com.newposition.candidate.forms;

import com.newposition.candidate.domain.NpCandidate;


/*
 *  @author sachi N
 */

public class NpCandidateFutureForm implements java.io.Serializable
{

    private static final long serialVersionUID = 1L;

    private Integer id;
    private NpCandidate npCandidate;
    private String type;
    private String compensation;
    private String bonus;
    private String dayRate;
    private String personalStatement;
    private String coverLetter;
    private String availableStatus;

    public String getAvailableStatus()
    {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus)
    {
        this.availableStatus = availableStatus;
    }

    private String cvFile;

    private byte[] photo;

    public byte[] getPhoto()
    {
        return photo;
    }

    public void setPhoto(byte[] photo)
    {
        this.photo = photo;
    }

    public String getCvFile()
    {
        return cvFile;
    }

    public void setCvFile(String cvFile)
    {
        this.cvFile = cvFile;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public NpCandidate getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidate npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getCompensation()
    {
        return compensation;
    }

    public void setCompensation(String compensation)
    {
        this.compensation = compensation;
    }

    public String getBonus()
    {
        return bonus;
    }

    public void setBonus(String bonus)
    {
        this.bonus = bonus;
    }

    public String getDayRate()
    {
        return dayRate;
    }

    public void setDayRate(String dayRate)
    {
        this.dayRate = dayRate;
    }

    public String getPersonalStatement()
    {
        return personalStatement;
    }

    public void setPersonalStatement(String personalStatement)
    {
        this.personalStatement = personalStatement;
    }

    public String getCoverLetter()
    {
        // TODO Auto-generated method stub
        return coverLetter;
    }

    public void setCoverLetter(String coverLetter)
    {
        this.coverLetter = coverLetter;
    }
}
