package com.newposition.candidate.dao;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidatePreferenceForm;
import com.newposition.common.dao.GenericDao;

public interface NpCandidateMyaccountDao extends GenericDao<NpCandidate, Integer>
{
    public void saveOrUpdateCandidatePreferences(NpCandidatePreferenceForm npCandidatePreferenceForm, NpCandidate npCandidate);

    public NpCandidatePreference getCandidatePreferences(NpCandidate npCandidate);

    public void updateCandidateDocument(NpCandidateDocument candidateDocument);

    public void saveCandidatePreference(NpCandidatePreference candidatePreference);

    public void updateCandidateEmployer(NpCandidateEmployer candidateEmployer);

    public void updateCandidate(NpCandidateImpl npCandidate);

    public boolean checkEmailExists(String primaryEmail);

    List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId);
}
