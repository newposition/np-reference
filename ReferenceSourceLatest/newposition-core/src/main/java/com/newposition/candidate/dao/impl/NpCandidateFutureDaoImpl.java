package com.newposition.candidate.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.candidate.dao.NpCandidateFutureDao;
import com.newposition.candidate.domain.Eligibility;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateCurrentEmpRoles;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEligibility;
import com.newposition.candidate.domain.NpCandidateEligibilityId;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateEmpRolesId;
import com.newposition.candidate.domain.NpCandidateEmployerSkills;
import com.newposition.candidate.domain.NpCandidateEmployerTools;
import com.newposition.candidate.domain.NpCandidateFutureEmployer;
import com.newposition.candidate.domain.NpCandidateFutureEmployerId;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidateInterests;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.domain.NpCandidateRegion;
import com.newposition.candidate.domain.NpCandidateRegionId;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateSkillsId;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.domain.NpCandidateToolsId;
import com.newposition.candidate.domain.Region;
import com.newposition.common.dao.NpUserDao;
import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpRole;
import com.newposition.common.domain.NpRoleImpl;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.common.domain.NpTool;
import com.newposition.common.domain.NpToolImpl;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserRole;
import com.newposition.common.domain.NpUserRoleId;
import com.newposition.common.domain.NpUserTypes;

public class NpCandidateFutureDaoImpl implements NpCandidateFutureDao
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateFutureDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    private NpUserDao npUserDao;

    public void setNpUserDao(NpUserDao npUserDao)
    {
        this.npUserDao = npUserDao;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveSkills(List<String> skillNames,
                                   NpCandidate npCandidate)
    {

        List<String> addedSkills = new ArrayList<String>();
        for (String skillName : skillNames)
        {
            List<NpSkill> listOfSkills = (List<NpSkill>) hibernateTemplate
                    .find("from NpSkillImpl where skillName = ? ", skillName);
            List<NpCandidateSkills> listOfCandidateSkills = null;
            NpCandidateSkills npCandidateSkills = null;
            NpSkill npSkill = null;

            if (listOfSkills.size() == 0)
            {
                npSkill = new NpSkillImpl();
                npSkill.setSkillName(skillName);
                npSkill.setCreatedBy(npCandidate.getFirstName());
                npSkill.setCreatedDate(new Date());
                LOG.info("New Skill added ");
                hibernateTemplate.save(npSkill);
            }
            else
            {
                npSkill = listOfSkills.get(0);
            }
            listOfCandidateSkills = (List<NpCandidateSkills>) hibernateTemplate
                    .find("from NpCandidateSkills where candidateId = ? and skillId = ? ",
                            npCandidate.getId(), npSkill.getId());

            if (listOfCandidateSkills.size() == 0)
            {
                NpCandidateSkillsId npCandidateSkillsId = new NpCandidateSkillsId();
                npCandidateSkillsId.setCandidateId(npCandidate.getId());
                npCandidateSkillsId.setSkillId(npSkill.getId());

                npCandidateSkills = new NpCandidateSkills();

                npCandidateSkills.setId(npCandidateSkillsId);
                npCandidateSkills.setNpCandidate((NpCandidateImpl) npCandidate);
                npCandidateSkills.setNpSkill((NpSkillImpl) npSkill);
                npCandidateSkills.setCreatedBy(npCandidate.getFirstName());
                npCandidateSkills.setCreatedDate(new Date());
                npCandidateSkills.setPreferfutureSkill(1);
                addedSkills.add(skillName);
                hibernateTemplate.save(npCandidateSkills);
            }
        }

        LOG.info("Skill added successfully to the candidate");
        return addedSkills;
    }

    /**
     * @param skillName
     * @param npCandidate
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public boolean removeSkill(String skillName, NpCandidate npCandidate)
    {

        List<NpSkill> listOfSkills = (List<NpSkill>) hibernateTemplate.find(
                "from NpSkillImpl where skillName = ? ", skillName);
        if (listOfSkills.size() > 0)
        {
            NpSkill npSkills = listOfSkills.get(0);

            List<NpCandidateSkills> listOfCandidateSkills = (List<NpCandidateSkills>) hibernateTemplate
                    .find("from NpCandidateSkills where candidateId = ? and skillId = ?",
                            npCandidate.getId(), npSkills.getId());
            NpCandidateSkills npCandidateSkills = listOfCandidateSkills.get(0);
            hibernateTemplate.delete(npCandidateSkills);

            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", npCandidate.getId());

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateEmployerSkills> npCandidateEmployerSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate.find(
                            "from NpCandidateEmployerSkills where CandidateEmployerId=? and SkillId=?",
                            candidateEmployerId, npSkills.getId());

                    if (npCandidateEmployerSkills.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateEmployerSkills.get(0));
                    }
                }
            }

            LOG.info("Skill removed from Candidate");
        }
        return true;
    }

    /**
     * @param roleName
     * @param npCandidate
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveRoles(List<String> roleNames,
                                  NpCandidate npCandidate)
    {

        List<String> addedRoles = new ArrayList<String>();
        for (String roleName : roleNames)
        {
            List<NpEmploymentRole> listOfEmploymentRoles = (List<NpEmploymentRole>) hibernateTemplate
                    .find("from NpEmploymentRoleImpl where roleName = ? ",
                            roleName);
            NpEmploymentRole npEmploymentRole = null;

            NpCandidateEmpRoles npCandidateEmpRoles = null;
            if (listOfEmploymentRoles.size() == 0)
            {
                LOG.info("Role not exists so newly created");
                npEmploymentRole = new NpEmploymentRoleImpl();
                npEmploymentRole.setRoleName(roleName);
                npEmploymentRole.setCreatedBy(npCandidate.getFirstName());
                npEmploymentRole.setCreatedDate(new Date());

                LOG.info("New Role added");
                hibernateTemplate.save(npEmploymentRole);
            }
            else
            {
                LOG.info("Role exists");
                npEmploymentRole = listOfEmploymentRoles.get(0);
            }
            List<NpCandidateEmpRoles> listOfCandidateRoles = (List<NpCandidateEmpRoles>) hibernateTemplate
                    .find("from NpCandidateEmpRoles where candidateId = ? and employmentRoleId = ? ",
                            npCandidate.getId(), npEmploymentRole.getId());
            if (listOfCandidateRoles.size() == 0)
            {
                npCandidateEmpRoles = new NpCandidateEmpRoles();

                NpCandidateEmpRolesId id = new NpCandidateEmpRolesId();
                id.setCandidateId(npCandidate.getId());
                id.setEmploymentRoleId(npEmploymentRole.getId());

                npCandidateEmpRoles.setId(id);
                npCandidateEmpRoles.setFuturePreferedRole(1);
                npCandidateEmpRoles
                        .setNpCandidate((NpCandidateImpl) npCandidate);
                npCandidateEmpRoles
                        .setNpEmploymentRoles((NpEmploymentRoleImpl) npEmploymentRole);
                npCandidateEmpRoles.setCreatedBy(npCandidate.getFirstName());
                npCandidateEmpRoles.setCreatedDate(new Date());
                addedRoles.add(roleName);
                hibernateTemplate.save(npCandidateEmpRoles);

                LOG.info("Role added successfully");
            }
        }

        return addedRoles;
    }

    /**
     * @param roleName
     * @param npCandidate
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public boolean removeRole(String roleName, NpCandidate npCandidate)
    {

        List<NpEmploymentRole> listOfEmploymentRoles = (List<NpEmploymentRole>) hibernateTemplate
                .find("from NpEmploymentRoleImpl where roleName = ? ", roleName);
        LOG.info(" role size:  " + listOfEmploymentRoles.size() + "roleName : "
                + roleName + "  size: " + roleName.length());
        if (listOfEmploymentRoles.size() > 0)
        {
            NpEmploymentRole npEmploymentRole = listOfEmploymentRoles.get(0);

            List<NpCandidateEmpRoles> listOfCandidateRoles = (List<NpCandidateEmpRoles>) hibernateTemplate
                    .find("from NpCandidateEmpRoles where candidateId = ? and employmentRoleId = ?",
                            npCandidate.getId(), npEmploymentRole.getId());
            NpCandidateEmpRoles npCandidateEmpRoles = listOfCandidateRoles.get(0);
            hibernateTemplate.delete(npCandidateEmpRoles);
            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", npCandidate.getId());

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateCurrentEmpRoles> npCandidateCurrentEmpRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate
                            .find("from NpCandidateCurrentEmpRoles where CandidateEmployerId=? and EmploymentRoleID=?",
                                    candidateEmployerId, npEmploymentRole.getId());

                    if (npCandidateCurrentEmpRoles.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateCurrentEmpRoles.get(0));
                    }
                }
            }

            LOG.info(" Role removed successfully  ");
        }

        return true;
    }

    /**
     * @param toolName
     * @param npCandidate
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<String> addTools(List<String> toolNames, NpCandidate npCandidate)
    {

        List<String> addedTools = new ArrayList<String>();
        for (String toolName : toolNames)
        {

            List<NpTool> listOfNpTools = (List<NpTool>) hibernateTemplate.find(
                    "from NpToolImpl where toolName = ? ", toolName);
            NpTool npTools = null;

            NpCandidateTools npCandidateTools = null;
            if (listOfNpTools.size() == 0)
            {

                npTools = new NpToolImpl();
                npTools.setToolName(toolName);
                npTools.setCreatedBy(npCandidate.getFirstName());
                npTools.setCreatedDate(new Date());

                LOG.info("New Tools added : " + toolName);
                hibernateTemplate.save(npTools);
            }
            else
            {
                npTools = listOfNpTools.get(0);
            }
            List<NpCandidateTools> listOfCandidateTools = (List<NpCandidateTools>) hibernateTemplate
                    .find("from NpCandidateTools where candidateId = ? and toolId = ? ",
                            npCandidate.getId(), npTools.getId());
            if (listOfCandidateTools.size() == 0)
            {
                npCandidateTools = new NpCandidateTools();

                NpCandidateToolsId id = new NpCandidateToolsId();
                id.setCandidateId(npCandidate.getId());
                id.setToolId(npTools.getId());

                npCandidateTools.setId(id);
                npCandidateTools.setFuturePreferedTool(1);
                npCandidateTools.setNpCandidate((NpCandidateImpl) npCandidate);
                npCandidateTools.setNpTools((NpToolImpl) npTools);
                npCandidateTools.setCreatedBy(npCandidate.getFirstName());
                npCandidateTools.setCreatedDate(new Date());
                addedTools.add(toolName);
                hibernateTemplate.save(npCandidateTools);
            }
        }

        LOG.info("Tool added successfully to the Candidate");

        return addedTools;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean removeTool(String toolName, NpCandidate npCandidate)
    {

        List<NpTool> listOfNpTools = (List<NpTool>) hibernateTemplate.find(
                "from NpToolImpl where toolName = ? ", toolName);
        LOG.info(" Tool size:  " + listOfNpTools.size());
        if (listOfNpTools.size() > 0)
        {
            NpTool npTool = listOfNpTools.get(0);

            List<NpCandidateTools> listOfCandidateTools = (List<NpCandidateTools>) hibernateTemplate
                    .find("from NpCandidateTools where candidateId = ? and toolId = ?",
                            npCandidate.getId(), npTool.getId());
            NpCandidateTools npCandidateTools = listOfCandidateTools.get(0);
            hibernateTemplate.delete(npCandidateTools);
            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", npCandidate.getId());

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateEmployerTools> npCandidateEmployerTools = (List<NpCandidateEmployerTools>) hibernateTemplate.find(
                            "from NpCandidateEmployerTools where CandidateEmployerId=? and ToolID=?",
                            candidateEmployerId, npTool.getId());

                    if (npCandidateEmployerTools.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateEmployerTools.get(0));
                    }
                }
            }
            LOG.info(" Tool deleted Successfully");
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> addCountries(List<String> countryNames,
                                     NpCandidate npCandidate)
    {
        List<String> addedCountries = new ArrayList<String>();
        for (String countryName : countryNames)
        {

            List<Eligibility> listOfCountries = (List<Eligibility>) hibernateTemplate
                    .find("from Eligibility where countryName = ? ",
                            countryName);
            Eligibility eligibleCountry = null;
            NpCandidateEligibility candidateEligibleCountry = null;

            if (listOfCountries.size() > 0)
            {
                eligibleCountry = listOfCountries.get(0);
                List<NpCandidateEligibility> listOfCadidateEligibilities = (List<NpCandidateEligibility>) hibernateTemplate
                        .find("from NpCandidateEligibility where candidateId = ? and npEligibilityId = ? ",
                                npCandidate.getId(), eligibleCountry.getId());

                if (listOfCadidateEligibilities.size() == 0)
                {
                    NpCandidateEligibilityId candidateEligibleCountryId = new NpCandidateEligibilityId();
                    candidateEligibleCountryId.setCandidateId(npCandidate
                            .getId());
                    candidateEligibleCountryId
                            .setNpEligibilityId(eligibleCountry.getId());
                    candidateEligibleCountry = new NpCandidateEligibility();
                    candidateEligibleCountry.setId(candidateEligibleCountryId);
                    candidateEligibleCountry.setEligibilityPreference(1);
                    addedCountries.add(eligibleCountry.getCountryName());
                    hibernateTemplate.save(candidateEligibleCountry);
                    LOG.info("country added Successfully to the candidate");
                }
            }

            else
            {
                List<Region> listOfRegions = (List<Region>) hibernateTemplate
                        .find("from Region where regionName = ? ", countryName);
                if (listOfRegions.size() > 0)
                {
                    Region region = listOfRegions.get(0);
                    List<NpCandidateRegion> listOfCandidateRegions = (List<NpCandidateRegion>) hibernateTemplate
                            .find("from NpCandidateRegion where candidateId = ? and npRegionId = ? ",
                                    npCandidate.getId(), region.getId());

                    if (listOfCandidateRegions.size() == 0)
                    {
                        NpCandidateRegionId candidateRegionId = new NpCandidateRegionId();
                        candidateRegionId.setCandidateId(npCandidate.getId());
                        candidateRegionId.setNpRegionId(region.getId());
                        NpCandidateRegion candidateRegion = new NpCandidateRegion();
                        candidateRegion.setId(candidateRegionId);
                        addedCountries.add(region.getRegionName());
                        hibernateTemplate.save(candidateRegion);
                        LOG.info("country added Successfully to the candidate");
                    }
                }
            }
        }
        return addedCountries;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean removeCountry(String countryName, NpCandidate npCandidate)
    {
        List<Eligibility> listOfCountries = (List<Eligibility>) hibernateTemplate.find(
                "from Eligibility where countryName = ?", countryName);

        if (listOfCountries.size() > 0)
        {
            Eligibility eligibility = listOfCountries.get(0);
            List<NpCandidateEligibility> listOfEligibleCountries = (List<NpCandidateEligibility>) hibernateTemplate
                    .find("from NpCandidateEligibility where candidateId = ? and npEligibilityId = ?",
                            npCandidate.getId(), eligibility.getId());
            if (listOfEligibleCountries.size() > 0)
            {
                NpCandidateEligibility eligibleCountry = listOfEligibleCountries.get(0);

                hibernateTemplate.delete(eligibleCountry);
            }

            LOG.info("Country deleted successfully");
        }
        else
        {
            List<Region> listOfRegions = (List<Region>) hibernateTemplate.find(
                    "from Region where regionName = ?", countryName);
            if (listOfRegions.size() > 0)
            {
                Region region = listOfRegions.get(0);
                List<NpCandidateRegion> candidateRegions = (List<NpCandidateRegion>) hibernateTemplate
                        .find("from NpCandidateRegion where candidateId = ? and npRegionId = ?",
                                npCandidate.getId(), region.getId());
                if (candidateRegions.size() > 0)
                {
                    NpCandidateRegion candidateRegion = candidateRegions.get(0);

                    hibernateTemplate.delete(candidateRegion);
                }
            }
        }

        return true;
    }

    @Override
    public String saveInterests(String name, int canId)
    {
        NpCandidate candidate = hibernateTemplate.get(NpCandidateImpl.class, canId);
        NpCandidateInterests candidateInterests = new NpCandidateInterests();
        candidateInterests.setNpCandidate((NpCandidateImpl) candidate);
        candidateInterests.setInterestArea(name);
        Calendar cal = Calendar.getInstance();
        Date createdDate = cal.getTime();
        Date modifiedDate = cal.getTime();
        candidateInterests.setCreatedBy("aaa");
        candidateInterests.setModifiedBy("aaa");
        candidateInterests.setCreatedDate(createdDate);
        candidateInterests.setModifiedDate(modifiedDate);
        hibernateTemplate.save(candidateInterests);
        return name;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListOfCountries()
    {
        List<String> list = new ArrayList<String>();
        List<Eligibility> lisOfCountries = (List<Eligibility>) hibernateTemplate.find(" from Eligibility ");
        LOG.info("Returning List Of countries : " + lisOfCountries.size());
        for (Eligibility eligibility : lisOfCountries)
        {
            list.add(eligibility.getCountryName());
        }
        List<Region> listofRegions = (List<Region>) hibernateTemplate.find(" from Region ");
        for (Region region : listofRegions)
        {
            list.add(region.getRegionName());
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveFutureEmployer(List<String> companies, NpCandidate npCandidate)
    {
        List<String> addedCompanies = new ArrayList<String>();
        for (String company : companies)
        {

            NpEmployer npEmployer = null;
            NpCandidateFutureEmployer candidateFutureEmployer = null;

            // check if Company exists in NpEmployer
            List<NpEmployer> listOfCompany = (List<NpEmployer>) hibernateTemplate.find(
                    "from NpEmployerImpl where name= ?", company);
            if (listOfCompany.size() == 0)
            {

                npEmployer = new NpEmployerImpl();
                npEmployer.setName(company);
                npEmployer.setCreatedBy(npCandidate.getFirstName());
                npEmployer.setCreatedDate(new Date());
                npEmployer.setModifiedBy(npCandidate.getFirstName());
                npEmployer.setModifiedDate(new Date());

                hibernateTemplate.save(npEmployer);
            }
            else
            {

                npEmployer = listOfCompany.get(0);
            }

            // check if Company exist in the Future preferred company of the
            // candidate
            List<NpCandidateFutureEmployer> listOfPreferedCompanies = (List<NpCandidateFutureEmployer>) hibernateTemplate
                    .find("from NpCandidateFutureEmployer where employerId = ? and candidateId = ? ",
                            npEmployer.getId(), npCandidate.getId());
            if (listOfPreferedCompanies.size() == 0)
            {

                NpCandidateFutureEmployerId id = new NpCandidateFutureEmployerId();
                id.setEmployerId(npEmployer.getId());
                id.setCandidateId(npCandidate.getId());
                candidateFutureEmployer = new NpCandidateFutureEmployer();
                candidateFutureEmployer.setId(id);
                candidateFutureEmployer.setFuturePreferedCompany(1);

                // save the future prefered company
                addedCompanies.add(company);
                hibernateTemplate.save(candidateFutureEmployer);
            }
        }
        return addedCompanies;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void removeFutureEmployer(String company, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub

        List<NpEmployer> listOfCompany = (List<NpEmployer>) hibernateTemplate.find(
                "from NpEmployerImpl where name= ?", company);

        NpEmployer npEmployer = listOfCompany.get(0);

        List<NpCandidateFutureEmployer> candidateFutureEmployerList = (List<NpCandidateFutureEmployer>) hibernateTemplate
                .find("from NpCandidateFutureEmployer where employerId = ? and candidateId = ? ",
                        npEmployer.getId(), npCandidate.getId());
        if (candidateFutureEmployerList.size() > 0)
        {
            NpCandidateFutureEmployer candidateFutureEmployer = candidateFutureEmployerList
                    .get(0);

            hibernateTemplate.delete(candidateFutureEmployer);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveExcludeEmployer(List<String> companies, NpCandidate npCandidate)
    {
        List<String> addedCompanies = new ArrayList<String>();
        for (String company : companies)
        {

            NpEmployer npEmployer = null;
            NpCandidateFutureEmployer candidateFutureEmployer = null;

            // check if Company exists in NpEmployer
            List<NpEmployer> listOfCompany = (List<NpEmployer>) hibernateTemplate.find(
                    "from NpEmployerImpl where name= ?", company);
            if (listOfCompany.size() == 0)
            {

                npEmployer = new NpEmployerImpl();
                npEmployer.setName(company);
                npEmployer.setCreatedBy(npCandidate.getFirstName());
                npEmployer.setCreatedDate(new Date());
                npEmployer.setModifiedBy(npCandidate.getFirstName());
                npEmployer.setModifiedDate(new Date());

                hibernateTemplate.save(npEmployer);
            }
            else
            {

                npEmployer = listOfCompany.get(0);
            }

            // check if Company exist in the Future preferred company of the
            // candidate
            List<NpCandidateFutureEmployer> listOfPreferedCompanies = (List<NpCandidateFutureEmployer>) hibernateTemplate
                    .find("from NpCandidateFutureEmployer where employerId = ? and candidateId = ? ",
                            npEmployer.getId(), npCandidate.getId());
            if (listOfPreferedCompanies.size() == 0)
            {

                NpCandidateFutureEmployerId id = new NpCandidateFutureEmployerId();
                id.setEmployerId(npEmployer.getId());
                id.setCandidateId(npCandidate.getId());
                candidateFutureEmployer = new NpCandidateFutureEmployer();
                candidateFutureEmployer.setId(id);
                candidateFutureEmployer.setFuturePreferedCompany(0);

                // save the future prefered company
                addedCompanies.add(company);
                hibernateTemplate.save(candidateFutureEmployer);
            }
        }
        return addedCompanies;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateFutureEmployer> getFutureEmployer(NpCandidate npCandidate)
    {
        List<NpCandidateFutureEmployer> candidateFutureEmployerList = (List<NpCandidateFutureEmployer>) hibernateTemplate
                .find("from NpCandidateFutureEmployer where candidateId = ? ",
                        npCandidate.getId());

        return candidateFutureEmployerList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void changeFuturePreferenceSkill(String name, int futurePreference, NpCandidate npCandidate)
    {
        List<NpSkill> npSkill = (List<NpSkill>) hibernateTemplate.find("from NpSkillImpl where skillName=? ", name);

        List<NpCandidateSkills> futureSkillList = (List<NpCandidateSkills>) hibernateTemplate
                .find("from NpCandidateSkills where candidateId=? and skillId= ?",
                        npCandidate.getId(), npSkill.get(0).getId());
        if (futureSkillList.size() > 0)
        {
            NpCandidateSkills candidateSkills = futureSkillList.get(0);
            candidateSkills.setPreferfutureSkill(futurePreference);
            hibernateTemplate.update(candidateSkills);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void changeFuturePreferenceRole(String name, int futurePreference, NpCandidate npCandidate)
    {
        List<NpEmploymentRole> employmentRoles = (List<NpEmploymentRole>) hibernateTemplate
                .find("from NpEmploymentRoleImpl where roleName=? ", name);

        List<NpCandidateEmpRoles> candidateEmpRoles = (List<NpCandidateEmpRoles>) hibernateTemplate
                .find("from NpCandidateEmpRoles where candidateId=? and employmentRoleId= ?",
                        npCandidate.getId(), employmentRoles.get(0).getId());
        if (candidateEmpRoles.size() > 0)
        {
            NpCandidateEmpRoles npCandidateEmpRole = candidateEmpRoles.get(0);
            npCandidateEmpRole.setFuturePreferedRole(futurePreference);
            hibernateTemplate.update(npCandidateEmpRole);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void changeFuturePreferenceTools(String name, int futurePreference,
                                            NpCandidate npCandidate)
    {
        List<NpTool> tools = (List<NpTool>) hibernateTemplate
                .find("from NpToolImpl where toolName=? ", name);

        List<NpCandidateTools> candidateTools = (List<NpCandidateTools>) hibernateTemplate
                .find("from NpCandidateTools where candidateId=? and toolId= ?",
                        npCandidate.getId(), tools.get(0).getId());
        if (candidateTools.size() > 0)
        {
            NpCandidateTools npCandidateTool = candidateTools.get(0);
            npCandidateTool.setFuturePreferedTool(futurePreference);
            hibernateTemplate.update(npCandidateTool);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateSkills> getPreferedSkills(NpCandidate npCandidate)
    {

        List<NpCandidateSkills> futureSkillList = (List<NpCandidateSkills>) hibernateTemplate
                .find("from NpCandidateSkills where candidateId=? ",
                        npCandidate.getId());

        LOG.info("list of future skills:  " + futureSkillList.size());

        return futureSkillList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateTools> getPreferedTools(NpCandidate npCandidate)
    {

        List<NpCandidateTools> futureToolList = (List<NpCandidateTools>) hibernateTemplate
                .find("from NpCandidateTools where candidateId=? ", npCandidate.getId());

        LOG.info("list of future Tools:  " + futureToolList.size());
        return futureToolList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEmpRoles> getPreferedRoles(NpCandidate npCandidate)
    {

        List<NpCandidateEmpRoles> futureRoleList = (List<NpCandidateEmpRoles>) hibernateTemplate
                .find("from NpCandidateEmpRoles where candidateId=? ", npCandidate.getId());

        LOG.info("list of future Roles:  " + futureRoleList.size());

        return futureRoleList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getPreferCountries(
            NpCandidate npCandidate)
    {
        List<String> list = new ArrayList<String>();
        List<NpCandidateEligibility> futurePreferCountries = (List<NpCandidateEligibility>) hibernateTemplate
                .find("from NpCandidateEligibility where candidateId = ? and eligibilityPreference > ?",
                        npCandidate.getId(), 0);
        if (futurePreferCountries.size() > 0)
        {
            for (NpCandidateEligibility npCandidateEligibility : futurePreferCountries)
            {
                list.add(npCandidateEligibility.getNpEligibility().getCountryName());
            }
        }
        List<NpCandidateRegion> futurePreferRegions = (List<NpCandidateRegion>) hibernateTemplate
                .find("from NpCandidateRegion where candidateId = ?",
                        npCandidate.getId());
        if (futurePreferRegions.size() > 0)
        {
            for (NpCandidateRegion npCandidateRegion : futurePreferRegions)
            {
                list.add(npCandidateRegion.getRegion().getRegionName());
            }
        }
        LOG.info("list of future Countries:  " + futurePreferCountries.size());

        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean saveFutureDetails(NpCandidatePreference npCandidatePreference, NpCandidateDocument candidateDocument)
    {
        NpUser npCandidate = null;

        //changing role of Register user to Role_Candidate

        if (npCandidatePreference != null)
        {

            // saving personal statement that has been set in service
            hibernateTemplate.saveOrUpdate(npCandidatePreference);
        }
        npCandidate = npCandidatePreference.getNpCandidate();
        List<NpUserRole> roles = new ArrayList<NpUserRole>(npCandidate.getNpUserRole());
        NpUserRole npUserRole = roles.get(0);
        hibernateTemplate.delete(npUserRole);

        npUserRole = new NpUserRole();
        NpUserRoleId npUserRoleId = new NpUserRoleId();
        NpRole npRole = npUserDao.findRoleByRoleName("ROLE_CANDIDATE");
        npUserRoleId.setRoleId(npRole.getId());
        npUserRoleId.setUserId(npCandidate.getId());

        npUserRole.setId(npUserRoleId);
        npUserRole.setNpRoles((NpRoleImpl) npRole);

        hibernateTemplate.save(npUserRole);
        // saving userType as Candidate
        NpUserTypes npUserTypes = hibernateTemplate.get(NpUserTypes.class, 1);
        npCandidate.setNpUserTypes(npUserTypes);

        if (candidateDocument != null)
        {
            List<NpCandidateDocument> candidateDocuments = (List<NpCandidateDocument>) hibernateTemplate
                    .find("from NpCandidateDocument where candidateid = ? ",
                            candidateDocument.getNpCandidate().getId());

            if (candidateDocuments.size() == 0)
            {
                hibernateTemplate.save(candidateDocument);
            }
            else
            {
                candidateDocuments.get(0).setCoverLetter(
                        candidateDocument.getCoverLetter());
                candidateDocuments.get(0).setCv(candidateDocument.getCv());
                candidateDocuments.get(0)
                        .setPhoto(candidateDocument.getPhoto());
            }

            return true;
        }

        LOG.info("daoimpl is executed");
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListOfCountriesOnly()
    {
        List<String> list = new ArrayList<String>();
        List<Eligibility> lisOfCountries = (List<Eligibility>) hibernateTemplate.find(" from Eligibility ");
        LOG.info("Returning List Of countries : " + lisOfCountries.size());
        for (Eligibility eligibility : lisOfCountries)
        {
            list.add(eligibility.getCountryName());
        }

        return list;
    }
}
