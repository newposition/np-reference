/**
 *
 */
package com.newposition.candidate.forms;

import java.io.Serializable;
import java.util.List;

/*
 * @author ravi
 *
 *
 */
public class NpCandidateSkillsCommand implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<NpCandidateSkillForm> skills;

    public List<NpCandidateSkillForm> getSkills()
    {
        return skills;
    }

    public void setSkills(List<NpCandidateSkillForm> skills)
    {
        this.skills = skills;
    }
}
