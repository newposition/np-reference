package com.newposition.candidate.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;

import com.newposition.candidate.dao.NpCandidateFutureDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateFutureEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.forms.NpCandidateFutureForm;
import com.newposition.candidate.service.NpCandidateFutureService;

public class NpCandidateFutureServiceImpl implements NpCandidateFutureService
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateFutureServiceImpl.class);

    private NpCandidateFutureDao npCandidateFutureDao;

    public void setNpCandidateFutureDao(NpCandidateFutureDao npCandidateFutureDao)
    {
        this.npCandidateFutureDao = npCandidateFutureDao;
    }

    @Override
    @Transactional
    public List<String> saveSkills(List<String> skillNames, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        LOG.info("executing in service : Save Skill()");
        return npCandidateFutureDao.saveSkills(skillNames, npCandidate);
    }

    @Override
    @Transactional
    public boolean removeSkill(String skillName, NpCandidate npCandidate)
    {

        return npCandidateFutureDao.removeSkill(skillName, npCandidate);
    }

    /* (non-Javadoc)
     * @see com.newposition.service.RegisterService#saveRole(java.lang.String, com.newposition.model.NpCandidate)
     */
    @Override
    @Transactional
    public List<String> saveRoles(List<String> roleNames, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.saveRoles(roleNames, npCandidate);
    }

    @Override
    @Transactional
    public boolean removeRole(String skillName, NpCandidate npCandidate)
    {

        return npCandidateFutureDao.removeRole(skillName, npCandidate);
    }

    @Override
    @Transactional
    public List<String> addTools(List<String> toolNames, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.addTools(toolNames, npCandidate);
    }

    @Override
    @Transactional
    public boolean removeTool(String toolName, NpCandidate npCandidate)
    {

        return npCandidateFutureDao.removeTool(toolName, npCandidate);
    }

    @Override
    @Transactional
    public List<String> addCountries(List<String> toolNames, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.addCountries(toolNames, npCandidate);
    }

    @Override
    @Transactional
    public boolean removeCountry(String toolName, NpCandidate npCandidate)
    {

        return npCandidateFutureDao.removeCountry(toolName, npCandidate);
    }

    @Override
    public List<String> getListOfCountries()
    {

        return npCandidateFutureDao.getListOfCountries();
    }

    @Override
    @Transactional
    public List<String> saveFutureEmployer(List<String> companies, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.saveFutureEmployer(companies, npCandidate);
    }

    /* (non-Javadoc)
     * @see com.newposition.service.RegisterService#removeFutureEmployer(java.lang.String, com.newposition.model.NpCandidate)
     */
    @Override
    @Transactional
    public void removeFutureEmployer(String company, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        npCandidateFutureDao.removeFutureEmployer(company, npCandidate);
    }

    @Override
    @Transactional
    public List<String> saveExcludeEmployer(List<String> companies, NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.saveExcludeEmployer(companies, npCandidate);
    }

    @Override
    @Transactional
    public Map<String, List<NpCandidateFutureEmployer>> getFutureEmployer(NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        List<NpCandidateFutureEmployer> listOfFutureEmployer = npCandidateFutureDao.getFutureEmployer(npCandidate);

        Map<String, List<NpCandidateFutureEmployer>> futureEmployerEntry = new HashMap<String, List<NpCandidateFutureEmployer>>();
        List<NpCandidateFutureEmployer> futurePreferCompany = new ArrayList<NpCandidateFutureEmployer>();
        List<NpCandidateFutureEmployer> futureExcludeCompany = new ArrayList<NpCandidateFutureEmployer>();

        for (NpCandidateFutureEmployer candidateFutureEmployer : listOfFutureEmployer)
        {

            if (candidateFutureEmployer.getFuturePreferedCompany() == 1)
            {
                futurePreferCompany.add(candidateFutureEmployer);
            }
            if (candidateFutureEmployer.getFuturePreferedCompany() == 0)
            {
                futureExcludeCompany.add(candidateFutureEmployer);
            }
        }

        futureEmployerEntry.put("futurePreferCompany", futurePreferCompany);
        futureEmployerEntry.put("futureExcludeCompany", futureExcludeCompany);
        LOG.info("returning preferred & excluded companies");

        return futureEmployerEntry;
    }

    @Override
    @Transactional
    public void changeFuturePreferenceSkill(String name, int futurePreference, NpCandidate npCandidate)
    {
        npCandidateFutureDao.changeFuturePreferenceSkill(name, futurePreference, npCandidate);
    }

    @Override
    @Transactional
    public void changeFuturePreferenceRole(String name, int futurePreference, NpCandidate npCandidate)
    {
        npCandidateFutureDao.changeFuturePreferenceRole(name, futurePreference, npCandidate);
    }

    @Override
    @Transactional
    public void changeFuturePreferenceTools(String name, int futurePreference, NpCandidate npCandidate)
    {
        npCandidateFutureDao.changeFuturePreferenceTools(name, futurePreference, npCandidate);
    }

    @Override
    @Transactional
    public String saveInterests(String name, int canId)
    {
        return npCandidateFutureDao.saveInterests(name, canId);
    }

    @Override
    public List<NpCandidateSkills> getPreferedSkills(NpCandidate npCandidate)
    {
        return npCandidateFutureDao.getPreferedSkills(npCandidate);
    }

    @Override
    @Transactional
    public List<String> getPreferCountries(NpCandidate npCandidate)
    {
        return npCandidateFutureDao.getPreferCountries(npCandidate);
    }

    @Override
    public List<NpCandidateEmpRoles> getPreferedRoles(NpCandidate npCandidate)
    {
        return npCandidateFutureDao.getPreferedRoles(npCandidate);
    }

    @Override
    public List<NpCandidateTools> getPreferedTools(NpCandidate npCandidate)
    {
        return npCandidateFutureDao.getPreferedTools(npCandidate);
    }

    @Override
    @Transactional
    public NpCandidateFutureForm saveFutureDetails(NpCandidateFutureForm npCandidateFutureForm)
    {
        // TODO Auto-generated method stub
        NpCandidatePreference npCandidatePreference = null;
        Double bonus = null, dayRate = null;
        Double compensation = null;
        try
        {
            if (npCandidateFutureForm.getType().equalsIgnoreCase("donotmind"))
            {
                bonus = Double.parseDouble(npCandidateFutureForm.getBonus());
                compensation = Double.parseDouble(npCandidateFutureForm.getCompensation());
                dayRate = Double.parseDouble(npCandidateFutureForm.getDayRate());
            }
            else if (npCandidateFutureForm.getType().equalsIgnoreCase("permanent"))
            {
                bonus = Double.parseDouble(npCandidateFutureForm.getBonus());
                compensation = Double.parseDouble(npCandidateFutureForm.getCompensation());
            }
            else
            {
                dayRate = Double.parseDouble(npCandidateFutureForm.getDayRate());
            }
            if (npCandidateFutureForm.getNpCandidate().getNpCandidatePreference() != null)
            {
                npCandidatePreference = npCandidateFutureForm.getNpCandidate().getNpCandidatePreference();
            }
            else
            {
                npCandidatePreference = new NpCandidatePreference();
            }
            //npCandidatePreference.setCandidateID(npCandidateFutureForm.getNpCandidate().getId());
            npCandidatePreference.setType(npCandidateFutureForm.getType());
            npCandidatePreference.setCompensation(compensation);
            npCandidatePreference.setBonus(bonus);
            npCandidatePreference.setDayRate(dayRate);
            npCandidatePreference.setNpCandidate((NpCandidateImpl) npCandidateFutureForm.getNpCandidate());
            npCandidatePreference.getNpCandidate().setPersonalStatement(npCandidateFutureForm.getPersonalStatement());
            npCandidatePreference.getNpCandidate().setAvailableStatus(npCandidateFutureForm.getAvailableStatus());
        }
        catch (Exception e)
        {
            LOG.error("error: Carrior reference not saved " + e.getMessage());
            npCandidatePreference = null;
        }

        LOG.info("Setting Document " + npCandidateFutureForm.getPhoto() + npCandidateFutureForm.getCvFile());

        NpCandidateDocument candidateDocument = new NpCandidateDocument();
        candidateDocument.setPhoto(npCandidateFutureForm.getPhoto());
        candidateDocument.setCv(npCandidateFutureForm.getCvFile());
        candidateDocument.setCoverLetter(npCandidateFutureForm.getCoverLetter());
        candidateDocument.setNpCandidate((NpCandidateImpl) npCandidateFutureForm.getNpCandidate());

        npCandidateFutureDao.saveFutureDetails(npCandidatePreference, candidateDocument);

        LOG.info("executed service ");
        return npCandidateFutureForm;
    }

    @Override
    public List<String> getListOfCountriesOnly()
    {
        // TODO Auto-generated method stub
        return npCandidateFutureDao.getListOfCountriesOnly();
    }
}
