/**
 *
 */
package com.newposition.candidate.service;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateReferenceForm;

/**
 * @author Sachi Nayak This class pass the cadidate reference details to
 *         {@link NpCandidateSponsorDao}
 */
public interface NpCandidateSponsorService
{

    /*
     * @return list of Sponsors details
     */
    List<NpCandidateReferenceForm> getListOfSponsors(NpCandidate npCandidate);

	/*
     * @params npCandidateReferenceModel This method persists
	 * npCandidateReferenceModel
	 */

    void saveMySponsorDetail(NpCandidateReferenceForm npCandidateReferenceModel);

	/*
	 * @params referenceList This method sends mail to all the Candidate
	 * Reference mail address
	 */

    boolean sendEmailToAll(List<NpCandidateReferenceForm> referenceList, NpCandidate npCandidate);

    /**
     * @param email
     * @param npCandidate
     * @return boolean
     */
    boolean isSponsorEmailExist(String email, NpCandidate npCandidate);

    /**
     * @param npCandidate
     * @param editSponsorId
     * @param email
     * @return boolean
     * This method checks if the candidate replacing an sponsor email with an another sponsor email
     */
    boolean isEditingDuplicateSponsor(NpCandidate npCandidate, String editSponsorId, String email);
}
