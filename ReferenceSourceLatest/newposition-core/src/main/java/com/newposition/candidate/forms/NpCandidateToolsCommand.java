/**
 *
 */
package com.newposition.candidate.forms;

import java.io.Serializable;
import java.util.List;

/*
 * @author ravi
 *
 *
 */
public class NpCandidateToolsCommand implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<NpCandidateToolsForm> tools;

    public List<NpCandidateToolsForm> getTools()
    {
        return tools;
    }

    public void setTools(List<NpCandidateToolsForm> tools)
    {
        this.tools = tools;
    }
}
