package com.newposition.candidate.dao.impl;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.newposition.candidate.dao.NpCandidateEducationRegisterDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateCertificate;
import com.newposition.candidate.domain.NpCandidateCertificateId;
import com.newposition.candidate.domain.NpCandidateEducation;
import com.newposition.candidate.domain.NpCandidateEducationId;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidateInterests;
import com.newposition.candidate.domain.NpCandidateLanguageId;
import com.newposition.candidate.domain.NpCandidateLanguages;
import com.newposition.candidate.domain.NpCertifications;
import com.newposition.candidate.domain.NpCourse;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.candidate.domain.NpLanguages;
import com.newposition.candidate.forms.NpCandidateEducationForm;

public class NpCandidateEducationRegisterDaoImpl implements NpCandidateEducationRegisterDao
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateEducationRegisterDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<String> getInstituteNames()
    {

        List<String> institueNames = (List<String>) hibernateTemplate
                .find("select instituteName from NpInstitutions");
        return institueNames;
    }

    // saving educational History

    @SuppressWarnings({"unchecked"})
    @Override
    @Transactional
    public void saveEducationHistory(NpCandidateEducationForm npCandidateEducationForm, NpCandidate npCandidate)
    {
        NpInstituteCourse npInstituteCourse = new NpInstituteCourse();
        // check if the same candidate education is already existing or not
        int educationId = npCandidateEducationForm.getId();
        if (educationId > 0)
        {
            // if the same candidate education is existing then update the
            // existing candidate education
            npInstituteCourse = hibernateTemplate.get(NpInstituteCourse.class,
                    educationId);
            if (npInstituteCourse.getId() > 0)
            {
                List<NpCandidateEducation> educationHistories = (List<NpCandidateEducation>) hibernateTemplate.find("from NpCandidateEducation where npCandidate=? and npInstituteCourse=?",
                        npCandidate, npInstituteCourse);

                NpCandidateEducation candidateEducation = educationHistories.get(0);

                NpCandidateEducationId candidateEducationId = candidateEducation.getId();

                NpInstituteCourse InstituteCourse = hibernateTemplate.get(NpInstituteCourse.class, candidateEducationId.getInstituteCourseId());

                List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate.find("from NpInstitutions where instituteName=?",
                        npCandidateEducationForm.getInstitution());

                NpInstitutions npInstitution;

                if (npInstitutions.size() > 0)
                {
                    npInstitution = npInstitutions.get(0);
                }
                else
                {
                    npInstitution = new NpInstitutions();
                    npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                    Calendar cal = Calendar.getInstance();
                    Date createdDate = cal.getTime();
                    Date modifiedDate = cal.getTime();
                    npInstitution.setCreatedBy(npCandidate.getFirstName());
                    npInstitution.setModifiedBy(npCandidate.getFirstName());
                    npInstitution.setCreatedDate(createdDate);
                    npInstitution.setModifiedDate(modifiedDate);
                    Serializable npInstitutionId = hibernateTemplate.save(npInstitution);
                    npInstitution = hibernateTemplate.get(NpInstitutions.class, npInstitutionId);
                }
                if (!InstituteCourse.getNpInstitutions().getInstituteName().equals(npCandidateEducationForm.getInstitution()))
                {
                    InstituteCourse.setNpInstitutions(npInstitution);
                    hibernateTemplate.save(InstituteCourse);
                }
                NpCourse course = InstituteCourse.getNpCourse();
                if (!course.getCourseName().equalsIgnoreCase(
                        npCandidateEducationForm.getAwardSubject()))
                {
                    course.setCourseName(npCandidateEducationForm.getAwardSubject());
                }
                if (!course.getCourseType().equalsIgnoreCase(
                        npCandidateEducationForm.getAwardType()))
                {
                    course.setCourseType(npCandidateEducationForm.getAwardType());
                }
                Date startDate = null;
                Date endDate = null;
                try
                {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    startDate = simpleDateFormat.parse(npCandidateEducationForm
                            .getStartDate());
                    if (npCandidateEducationForm.getEndDate() == null
                            || npCandidateEducationForm.getEndDate().equals("current")
                            || npCandidateEducationForm.getEndDate().trim().isEmpty())
                    {
                        endDate = null;
                    }
                    else
                    {
                        endDate = simpleDateFormat.parse(npCandidateEducationForm
                                .getEndDate());
                    }
                }
                catch (ParseException e)
                {

                    e.printStackTrace();
                }
                if (candidateEducation.getStartDate() == null
                        || !candidateEducation.getStartDate().equals(startDate))
                {
                    candidateEducation.setStartDate(startDate);
                }

                if (candidateEducation.getEndDate() == null
                        || !candidateEducation.getEndDate().equals(endDate))
                {
                    candidateEducation.setEndDate(endDate);
                }
                if (!candidateEducation.getLevel().equals(
                        npCandidateEducationForm.getLevel()))
                {
                    candidateEducation.setLevel(npCandidateEducationForm.getLevel());
                    ;
                }

                hibernateTemplate.saveOrUpdate(course);
                hibernateTemplate.saveOrUpdate(candidateEducation);
            }
        }
        else
        {
            // get NpInstitutions as per instituteName
            List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate
                    .find("from NpInstitutions where instituteName=?",
                            npCandidateEducationForm.getInstitution());

            NpInstitutions npInstitution;
            if (npInstitutions.size() > 0)
            {
                npInstitution = npInstitutions.get(0);
            }
            else
            {
                npInstitution = new NpInstitutions();
                npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                Calendar cal = Calendar.getInstance();
                Date createdDate = cal.getTime();
                Date modifiedDate = cal.getTime();
                npInstitution.setCreatedBy(npCandidate.getFirstName());
                npInstitution.setModifiedBy(npCandidate.getFirstName());
                npInstitution.setCreatedDate(createdDate);
                npInstitution.setModifiedDate(modifiedDate);
                Serializable npInstitutionId = hibernateTemplate
                        .save(npInstitution);
                npInstitution = hibernateTemplate.get(NpInstitutions.class,
                        npInstitutionId);
            }

            // save NpCourse for NpCandidate
            NpCourse npCourse = new NpCourse();
            npCourse.setCourseName(npCandidateEducationForm.getAwardSubject());
            npCourse.setCourseType(npCandidateEducationForm.getAwardType());
            Calendar cal = Calendar.getInstance();
            Date createdDate = cal.getTime();
            Date modifiedDate = cal.getTime();
            npCourse.setCreatedBy(npCandidate.getFirstName());
            npCourse.setModifiedBy(npCandidate.getFirstName());
            npCourse.setCreatedDate(createdDate);
            npCourse.setModifiedDate(modifiedDate);
            Serializable npCourseId = hibernateTemplate.save(npCourse);
            npCourse = hibernateTemplate.get(NpCourse.class, npCourseId);

            // save NpCourse and NpInstitutions in NpInstituteCourse

            npInstituteCourse.setNpCourse(npCourse);
            npInstituteCourse.setNpInstitutions(npInstitution);
            npInstituteCourse.setCreatedBy(npCandidate.getFirstName());
            npInstituteCourse.setModifiedBy(npCandidate.getFirstName());
            npInstituteCourse.setCreatedDate(createdDate);
            npInstituteCourse.setModifiedDate(modifiedDate);
            Serializable npInstituteCourseId = hibernateTemplate
                    .save(npInstituteCourse);
            npInstituteCourse = hibernateTemplate.get(NpInstituteCourse.class,
                    npInstituteCourseId);

            // save NpInstituteCourse and NpCandidate in npCandidateEducation
            NpCandidateEducationId candidateEducationId = new NpCandidateEducationId();
            candidateEducationId.setCandidateId(npCandidate.getId());
            candidateEducationId
                    .setInstituteCourseId(npInstituteCourse.getId());

            // save npCandidateEducation
            NpCandidateEducation npCandidateEducation = new NpCandidateEducation();
            npCandidateEducation.setLevel(npCandidateEducationForm.getLevel());
            npCandidateEducation.setId(candidateEducationId);
            npCandidateEducation.setCreatedBy(npCandidate.getFirstName());
            npCandidateEducation.setModifiedBy(npCandidate.getFirstName());
            npCandidateEducation.setCreatedDate(createdDate);
            npCandidateEducation.setModifiedDate(modifiedDate);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

            try
            {
                Date startDate = dateFormatter.parse(npCandidateEducationForm
                        .getStartDate());
                Date endDate;
                if (npCandidateEducationForm.getEndDate() == null
                        || npCandidateEducationForm.getEndDate().equals("current")
                        || npCandidateEducationForm.getEndDate().trim().isEmpty())
                {
                    endDate = null;
                }
                else
                {
                    endDate = dateFormatter.parse(npCandidateEducationForm.getEndDate());
                }
                npCandidateEducation.setStartDate(startDate);
                npCandidateEducation.setEndDate(endDate);
            }
            catch (ParseException e)
            {

                e.printStackTrace();
            }
            hibernateTemplate.save(npCandidateEducation);
        }
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void saveCertifications(NpCandidateEducationForm npCandidateEducationForm,
                                   NpCandidate npCandidate)
    {
        // check if already existing or not
        int certificateId = npCandidateEducationForm.getId();
        if (certificateId > 0)
        {

            List<NpCertifications> certifications = (List<NpCertifications>) hibernateTemplate
                    .find("from NpCertifications where id=?", certificateId);
            NpCertifications npCertification = certifications.get(0);
            List<NpCandidateCertificate> candidateCertificates = (List<NpCandidateCertificate>) hibernateTemplate
                    .find("from NpCandidateCertificate where CandidateID=?  and CertificateID=?",
                            npCandidate.getId(), certificateId);

            NpCandidateCertificate candidateCertificate = candidateCertificates
                    .get(0);
            // NpCandidateCertificateId candidateCertificateId =
            // candidateCertificate.getId();
            if (!candidateCertificate.getGradeLevel().equals(
                    npCandidateEducationForm.getLevel()))
            {
                candidateCertificate.setGradeLevel(npCandidateEducationForm.getLevel());
            }

            if (!candidateCertificate.getCertificationYear().equals(
                    npCandidateEducationForm.getStartDate()))
            {
                candidateCertificate.setCertificationYear(Integer
                        .parseInt(npCandidateEducationForm.getStartDate()));
            }
            if (!npCertification.getCertificateName().equals(
                    npCandidateEducationForm.getAwardSubject()))
            {
                npCertification.setCertificateName(npCandidateEducationForm
                        .getAwardSubject());
                ;
            }
            List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate
                    .find("from NpInstitutions where instituteName=?",
                            npCandidateEducationForm.getInstitution());

            NpInstitutions npInstitution;
            if (npInstitutions.size() > 0)
            {
                npInstitution = npInstitutions.get(0);
            }
            else
            {
                npInstitution = new NpInstitutions();
                npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                Calendar cal = Calendar.getInstance();
                Date createdDate = cal.getTime();
                Date modifiedDate = cal.getTime();
                npInstitution.setCreatedBy(npCandidate.getFirstName());
                npInstitution.setModifiedBy(npCandidate.getFirstName());
                npInstitution.setCreatedDate(createdDate);
                npInstitution.setModifiedDate(modifiedDate);
                Serializable npInstitutionId = hibernateTemplate
                        .save(npInstitution);
                npInstitution = hibernateTemplate.get(NpInstitutions.class,
                        npInstitutionId);
            }
            if (!(candidateCertificate.getNpInstitutions().getId() == npInstitution
                    .getId()))
            {
                candidateCertificate.setNpInstitutions(npInstitution);
            }

            hibernateTemplate.saveOrUpdate(npCertification);
            hibernateTemplate.saveOrUpdate(candidateCertificate);
        }
        else
        {
            // get NpInstitutions as per instituteName
            List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate
                    .find("from NpInstitutions where instituteName=?",
                            npCandidateEducationForm.getInstitution());

            NpInstitutions npInstitution;
            if (npInstitutions.size() > 0)
            {
                npInstitution = npInstitutions.get(0);
            }
            else
            {
                npInstitution = new NpInstitutions();
                npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                Calendar cal = Calendar.getInstance();
                Date createdDate = cal.getTime();
                Date modifiedDate = cal.getTime();
                npInstitution.setCreatedBy(npCandidate.getFirstName());
                npInstitution.setModifiedBy(npCandidate.getFirstName());
                npInstitution.setCreatedDate(createdDate);
                npInstitution.setModifiedDate(modifiedDate);
                Serializable npInstitutionId = hibernateTemplate
                        .save(npInstitution);
                npInstitution = hibernateTemplate.get(NpInstitutions.class,
                        npInstitutionId);
            }

            // save NpCertifications for NpCandidate
            NpCertifications npCertifications = new NpCertifications();
            npCertifications
                    .setCertificateName(npCandidateEducationForm.getAwardSubject());
            npCertifications.setCertificateType(npCandidateEducationForm.getAwardType());
            Calendar cal = Calendar.getInstance();
            Date createdDate = cal.getTime();
            Date modifiedDate = cal.getTime();
            npCertifications.setCreatedBy(npCandidate.getFirstName());
            npCertifications.setModifiedBy(npCandidate.getFirstName());
            npCertifications.setCreatedDate(createdDate);
            npCertifications.setModifiedDate(modifiedDate);
            Serializable npCertificationsId = hibernateTemplate
                    .save(npCertifications);
            npCertifications = hibernateTemplate.get(NpCertifications.class,
                    npCertificationsId);

            // save NpCertifications and NpCandidate in NpCandidateCertificateId
            NpCandidateCertificateId candidateCertificateId = new NpCandidateCertificateId();
            candidateCertificateId.setCandidateId(npCandidate.getId());
            candidateCertificateId.setCertificateId(npCertifications.getId());

            // save NpCertifications and NpCandidate in NpCandidateCertificate
            NpCandidateCertificate candidateCertificate = new NpCandidateCertificate();
            candidateCertificate.setId(candidateCertificateId);
            candidateCertificate.setGradeLevel(npCandidateEducationForm.getLevel());
            candidateCertificate.setNpInstitutions(npInstitution);
            candidateCertificate.setNpCandidate((NpCandidateImpl) npCandidate);
            candidateCertificate.setCreatedBy(npCandidate.getFirstName());
            candidateCertificate.setModifiedBy(npCandidate.getFirstName());
            candidateCertificate.setCreatedDate(createdDate);
            candidateCertificate.setModifiedDate(modifiedDate);
            candidateCertificate.setCertificationYear(Integer
                    .parseInt(npCandidateEducationForm.getStartDate()));
            hibernateTemplate.save(candidateCertificate);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEducationForm> getEducationHistories(NpCandidate npCandidate)
    {
        // get NpCandidateEducations by using candidate Id
        List<NpCandidateEducation> candidateEducations = (List<NpCandidateEducation>) hibernateTemplate
                .find("from NpCandidateEducation where npCandidate=? order by id desc",
                        npCandidate);

        List<NpCandidateEducationForm> npCandidateEducationForms = new ArrayList<NpCandidateEducationForm>();
        for (NpCandidateEducation candidateEducation : candidateEducations)
        {
            NpCandidateEducationForm form = new NpCandidateEducationForm();
            NpCandidateEducationId candidateEducationId = candidateEducation
                    .getId();
            NpInstituteCourse InstituteCourse = hibernateTemplate.get(
                    NpInstituteCourse.class,
                    candidateEducationId.getInstituteCourseId());

            NpInstitutions InstituteName = hibernateTemplate.get(
                    NpInstitutions.class, InstituteCourse.getNpInstitutions()
                            .getId());
            // set MyEducationForm id as InstituteCourseId so that we can edit
            // or delete using InstituteCourse Id and candidate Id
            form.setId(InstituteCourse.getId());
            form.setInstitution(InstituteName.getInstituteName());
            form.setInstituteLogo(InstituteName.getInstituteLogo());
            form.setAwardSubject(InstituteCourse.getNpCourse().getCourseName());
            form.setAwardType(InstituteCourse.getNpCourse().getCourseType());
            form.setLevel(candidateEducation.getLevel());
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            String startdate;
            if (candidateEducation.getStartDate() == null)
            {
                startdate = "current";
            }
            else
            {
                startdate = dateFormatter.format(candidateEducation
                        .getStartDate());
            }
            String enddate;
            if (candidateEducation.getEndDate() == null)
            {
                enddate = "current";
            }
            else
            {
                enddate = dateFormatter.format(candidateEducation.getEndDate());
            }
            form.setStartDate(startdate);
            form.setEndDate(enddate);
            if (form.getAwardType().trim().length() > 0)
            {
                npCandidateEducationForms.add(form);
            }
        }
        return npCandidateEducationForms;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEducationForm> getQualifications(NpCandidate npCandidate)
    {
        List<NpCandidateEducation> candidateEducations = (List<NpCandidateEducation>) hibernateTemplate
                .find("from NpCandidateEducation where npCandidate=? order by id desc",
                        npCandidate);

        List<NpCandidateEducationForm> npCandidateEducationForms = new ArrayList<NpCandidateEducationForm>();
        for (NpCandidateEducation candidateEducation : candidateEducations)
        {
            NpCandidateEducationForm form = new NpCandidateEducationForm();
            NpCandidateEducationId candidateEducationId = candidateEducation
                    .getId();
            NpInstituteCourse InstituteCourse = hibernateTemplate.get(
                    NpInstituteCourse.class,
                    candidateEducationId.getInstituteCourseId());

            NpInstitutions InstituteName = hibernateTemplate.get(
                    NpInstitutions.class, InstituteCourse.getNpInstitutions()
                            .getId());
            // set NpCandidateEducationForm id as InstituteCourseId so that we can edit
            // or delete using InstituteCourse Id and candidate Id
            form.setId(InstituteCourse.getId());
            form.setInstitution(InstituteName.getInstituteName());
            form.setInstituteLogo(InstituteName.getInstituteLogo());
            form.setAwardSubject(InstituteCourse.getNpCourse().getCourseName());
            form.setAwardType(InstituteCourse.getNpCourse().getCourseType());
            form.setLevel(candidateEducation.getLevel());
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            String startdate;
            if (candidateEducation.getStartDate() == null)
            {
                startdate = "current";
            }
            else
            {
                startdate = dateFormatter.format(candidateEducation
                        .getStartDate());
            }
            String enddate;
            if (candidateEducation.getEndDate() == null)
            {
                enddate = "current";
            }
            else
            {
                enddate = dateFormatter.format(candidateEducation.getEndDate());
            }
            form.setStartDate(startdate);
            form.setEndDate(enddate);
            if (form.getAwardType().trim().length() == 0)
            {
                npCandidateEducationForms.add(form);
            }
        }
        return npCandidateEducationForms;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEducationForm> getCandidateCertificates(
            NpCandidate npCandidate)
    {
        /*
		 * NpCandidate npCandidate = hibernateTemplate.get(NpCandidate.class,
		 * candidateId);
		 */
        List<NpCandidateCertificate> candidateCertificates = (List<NpCandidateCertificate>) hibernateTemplate
                .find("from NpCandidateCertificate where npCandidate=? order by id desc",
                        npCandidate);

        List<NpCandidateEducationForm> npCandidateEducationForms = new ArrayList<NpCandidateEducationForm>();
        for (NpCandidateCertificate candidateCertificate : candidateCertificates)
        {
            NpCandidateEducationForm form = new NpCandidateEducationForm();
            NpCandidateCertificateId candidateCertificateId = candidateCertificate
                    .getId();
            NpCertifications certifications = hibernateTemplate.get(
                    NpCertifications.class,
                    candidateCertificateId.getCertificateId());

            // NpInstitutions instituteName = hibernateTemplate
            // .get(NpInstitutions.class,
            // candidateCertificate.getInstituteId());
            // set MyEducationForm id as certificationsId so that we can edit or
            // delete using certifications Id and candidate Id
            form.setId(certifications.getId());
            form.setInstitution(candidateCertificate.getNpInstitutions()
                    .getInstituteName());
            form.setInstituteLogo(candidateCertificate.getNpInstitutions()
                    .getInstituteLogo());
            form.setAwardSubject(certifications.getCertificateName());
            form.setAwardType(certifications.getCertificateType());
            form.setLevel(candidateCertificate.getGradeLevel());

            form.setStartDate(candidateCertificate.getCertificationYear()
                    .toString());
            npCandidateEducationForms.add(form);
        }
        return npCandidateEducationForms;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getLanguages()
    {
        List<String> languages = (List<String>) hibernateTemplate
                .find("select languageName from NpLanguages");
        return languages;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveLanguages(List<String> languageNames,
                                      List<String> languageFluencies, NpCandidate candidate)
    {
        String languageName = "", langFluency = "";

        List<String> addedLanguages = new ArrayList<String>();
        for (int i = 0; i < languageNames.size(); i++)
        {
            // set NpLanguages into NpCandidateLanguages if NpLanguages with
            // languageName exists in DB
            languageName = languageNames.get(i);
            langFluency = languageFluencies.get(i);
            // System.out.println(languageName+"-"+langFluency);

            List<NpLanguages> languages = (List<NpLanguages>) hibernateTemplate
                    .find("from NpLanguages where languageName=?", languageName);
            NpLanguages npLanguages;
            if (languages.size() > 0)
            {
                npLanguages = languages.get(0);

                List<NpCandidateLanguages> candidateLanguages = (List<NpCandidateLanguages>) hibernateTemplate
                        .find("from NpCandidateLanguages where candidateId=? and languageId=?",
                                candidate.getId(), npLanguages.getId());
                if (candidateLanguages.size() == 0)
                {
                    NpCandidateLanguages candidateLanguage = new NpCandidateLanguages();
                    NpCandidateLanguageId candidateLanguageId = new NpCandidateLanguageId();
                    candidateLanguageId.setCandidateId(candidate.getId());
                    candidateLanguageId.setLanguageId(npLanguages.getId());
                    candidateLanguage.setId(candidateLanguageId);
                    candidateLanguage.setLanguageFluency(langFluency);

                    hibernateTemplate.save(candidateLanguage);
                }
                else
                {
                    candidateLanguages.get(0).setLanguageFluency(langFluency);
                }
                addedLanguages.add(StringUtils.capitalize(languageName) + '-'
                        + StringUtils.capitalize(langFluency));
            }
        }

        return addedLanguages;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> saveInterests(List<String> interestNames,
                                      NpCandidate npCandidate)
    {
        List<String> addedInterests = new ArrayList<String>();
        for (String interestName : interestNames)
        {
            // search with interestName as InterestArea and CandidateId in
            // NpCandidateInterests if not exists then create new
            // NpCandidateInterests
            List<NpCandidateInterests> npCandidateInterests = (List<NpCandidateInterests>) hibernateTemplate
                    .find("from NpCandidateInterests where interestArea =? and CandidateID=?",
                            interestName, npCandidate.getId());
            if (npCandidateInterests.size() == 0)
            {
                NpCandidateInterests candidateInterests = new NpCandidateInterests();
                candidateInterests.setNpCandidate((NpCandidateImpl) npCandidate);
                candidateInterests.setInterestArea(interestName);
                Calendar cal = Calendar.getInstance();
                Date createdDate = cal.getTime();
                Date modifiedDate = cal.getTime();
                candidateInterests.setCreatedBy(npCandidate.getFirstName());
                candidateInterests.setModifiedBy(npCandidate.getFirstName());
                candidateInterests.setCreatedDate(createdDate);
                candidateInterests.setModifiedDate(modifiedDate);
                addedInterests.add(interestName);
                hibernateTemplate.save(candidateInterests);
            }
        }
        return addedInterests;
    }

    @Override
    public void removeEducation(int educationId, int candidateId)
    {
        NpCandidateEducation candidateEducation = new NpCandidateEducation();
        NpCandidateEducationId candidateEducationId = new NpCandidateEducationId();
        candidateEducationId.setCandidateId(candidateId);
        candidateEducationId.setInstituteCourseId(educationId);
        candidateEducation.setId(candidateEducationId);
        hibernateTemplate.delete(candidateEducation);
    }

    @Override
    public void removeQualification(int qualificationId, int candidateId)
    {
        NpCandidateEducation candidateEducation = new NpCandidateEducation();
        NpCandidateEducationId candidateEducationId = new NpCandidateEducationId();
        candidateEducationId.setCandidateId(candidateId);
        candidateEducationId.setInstituteCourseId(qualificationId);
        candidateEducation.setId(candidateEducationId);
        hibernateTemplate.delete(candidateEducation);
    }

    @Override
    public void removeCertificate(int certificateId, int candidateId)
    {
        NpCandidateCertificate candidateCertificate = new NpCandidateCertificate();
        NpCandidateCertificateId candidateCertificateId = new NpCandidateCertificateId();
        candidateCertificateId.setCandidateId(candidateId);
        candidateCertificateId.setCertificateId(certificateId);
        candidateCertificate.setId(candidateCertificateId);
        hibernateTemplate.delete(candidateCertificate);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void removeLanguage(String languageName, int candidateId)
    {
        NpCandidateLanguages npCandidateLanguages = new NpCandidateLanguages();
        NpCandidateLanguageId npCandidateLanguageId = new NpCandidateLanguageId();
        npCandidateLanguageId.setCandidateId(candidateId);
        List<NpLanguages> npLanguages = (List<NpLanguages>) hibernateTemplate
                .find("from NpLanguages where languageName=?", languageName);
        NpLanguages npLanguage = npLanguages.get(0);
        npCandidateLanguageId.setLanguageId(npLanguage.getId());
        npCandidateLanguages.setId(npCandidateLanguageId);
        hibernateTemplate.delete(npCandidateLanguages);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void removeInterest(String interestName, int candidateId)
    {
        List<NpCandidateInterests> npCandidateInterests = (List<NpCandidateInterests>) hibernateTemplate
                .find("from NpCandidateInterests where interestArea =? and CandidateID=?",
                        interestName, candidateId);
        if (npCandidateInterests.size() > 0)
        {
            NpCandidateInterests npCandidateInterest = npCandidateInterests
                    .get(0);
            hibernateTemplate.delete(npCandidateInterest);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveQualification(NpCandidateEducationForm npCandidateEducationForm,
                                  NpCandidate npCandidate)
    {

        NpInstituteCourse npInstituteCourse = new NpInstituteCourse();
        // check if already existing or not
        int educationId = npCandidateEducationForm.getId();
        if (educationId > 0)
        {

            npInstituteCourse = hibernateTemplate.get(NpInstituteCourse.class,
                    educationId);
            if (npInstituteCourse.getId() > 0)
            {
                List<NpCandidateEducation> educationHistories = (List<NpCandidateEducation>) hibernateTemplate
                        .find("from NpCandidateEducation where npCandidate=? and npInstituteCourse=?",
                                npCandidate, npInstituteCourse);

                NpCandidateEducation candidateEducation = educationHistories
                        .get(0);
                NpCandidateEducationId candidateEducationId = candidateEducation
                        .getId();
                NpInstituteCourse InstituteCourse = hibernateTemplate.get(
                        NpInstituteCourse.class,
                        candidateEducationId.getInstituteCourseId());
                List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate.find("from NpInstitutions where instituteName=?",
                        npCandidateEducationForm.getInstitution());

                NpInstitutions npInstitution;
                if (npInstitutions.size() > 0)
                {
                    npInstitution = npInstitutions.get(0);
                }
                else
                {
                    npInstitution = new NpInstitutions();
                    npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                    Calendar cal = Calendar.getInstance();
                    Date createdDate = cal.getTime();
                    Date modifiedDate = cal.getTime();
                    npInstitution.setCreatedBy(npCandidate.getFirstName());
                    npInstitution.setModifiedBy(npCandidate.getFirstName());
                    npInstitution.setCreatedDate(createdDate);
                    npInstitution.setModifiedDate(modifiedDate);
                    Serializable npInstitutionId = hibernateTemplate
                            .save(npInstitution);
                    npInstitution = hibernateTemplate.get(NpInstitutions.class,
                            npInstitutionId);
                }
                if (!InstituteCourse.getNpInstitutions().getInstituteName()
                        .equals(npCandidateEducationForm.getInstitution()))
                {
                    InstituteCourse.setNpInstitutions(npInstitution);
                    hibernateTemplate.save(InstituteCourse);
                }
                NpCourse course = InstituteCourse.getNpCourse();
                if (!course.getCourseName().equalsIgnoreCase(
                        npCandidateEducationForm.getAwardSubject()))
                {
                    course.setCourseName(npCandidateEducationForm.getAwardSubject());
                }

                Date startDate = null;
                Date endDate = null;
                try
                {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    startDate = simpleDateFormat.parse(npCandidateEducationForm
                            .getStartDate());
                    if (npCandidateEducationForm.getEndDate() == null
                            || npCandidateEducationForm.getEndDate().equals("current")
                            || npCandidateEducationForm.getEndDate().trim().isEmpty())
                    {
                        endDate = null;
                    }
                    else
                    {
                        endDate = simpleDateFormat.parse(npCandidateEducationForm
                                .getEndDate());
                    }
                }
                catch (ParseException e)
                {

                    e.printStackTrace();
                }
                if (candidateEducation.getStartDate() == null
                        || !candidateEducation.getStartDate().equals(startDate))
                {
                    candidateEducation.setStartDate(startDate);
                }

                if (candidateEducation.getEndDate() == null
                        || !candidateEducation.getEndDate().equals(endDate))
                {
                    candidateEducation.setEndDate(endDate);
                }
                if (!candidateEducation.getLevel().equals(
                        npCandidateEducationForm.getLevel()))
                {
                    candidateEducation.setLevel(npCandidateEducationForm.getLevel());
                    ;
                }

                hibernateTemplate.saveOrUpdate(course);
                hibernateTemplate.saveOrUpdate(candidateEducation);
            }
        }
        else
        {
            // get NpInstitutions as per instituteName
            List<NpInstitutions> npInstitutions = (List<NpInstitutions>) hibernateTemplate
                    .find("from NpInstitutions where instituteName=?",
                            npCandidateEducationForm.getInstitution());

            NpInstitutions npInstitution;
            if (npInstitutions.size() > 0)
            {
                npInstitution = npInstitutions.get(0);
            }
            else
            {
                npInstitution = new NpInstitutions();
                npInstitution.setInstituteName(npCandidateEducationForm.getInstitution());
                Calendar cal = Calendar.getInstance();
                Date createdDate = cal.getTime();
                Date modifiedDate = cal.getTime();
                npInstitution.setCreatedBy(npCandidate.getFirstName());
                npInstitution.setModifiedBy(npCandidate.getFirstName());
                npInstitution.setCreatedDate(createdDate);
                npInstitution.setModifiedDate(modifiedDate);
                Serializable npInstitutionId = hibernateTemplate
                        .save(npInstitution);
                npInstitution = hibernateTemplate.get(NpInstitutions.class,
                        npInstitutionId);
            }

            // save NpCourse for NpCandidate
            NpCourse npCourse = new NpCourse();
            npCourse.setCourseName(npCandidateEducationForm.getAwardSubject());
            npCourse.setCourseType(" ");
            Calendar cal = Calendar.getInstance();
            Date createdDate = cal.getTime();
            Date modifiedDate = cal.getTime();
            npCourse.setCreatedBy(npCandidate.getFirstName());
            npCourse.setModifiedBy(npCandidate.getFirstName());
            npCourse.setCreatedDate(createdDate);
            npCourse.setModifiedDate(modifiedDate);
            Serializable npCourseId = hibernateTemplate.save(npCourse);
            npCourse = hibernateTemplate.get(NpCourse.class, npCourseId);

            // save NpCourse and NpInstitutions in NpInstituteCourse

            npInstituteCourse.setNpCourse(npCourse);
            npInstituteCourse.setNpInstitutions(npInstitution);
            npInstituteCourse.setCreatedBy(npCandidate.getFirstName());
            npInstituteCourse.setModifiedBy(npCandidate.getFirstName());
            npInstituteCourse.setCreatedDate(createdDate);
            npInstituteCourse.setModifiedDate(modifiedDate);
            Serializable npInstituteCourseId = hibernateTemplate
                    .save(npInstituteCourse);
            npInstituteCourse = hibernateTemplate.get(NpInstituteCourse.class,
                    npInstituteCourseId);

            // save NpInstituteCourse and NpCandidate in npCandidateEducation
            NpCandidateEducationId candidateEducationId = new NpCandidateEducationId();
            candidateEducationId.setCandidateId(npCandidate.getId());
            candidateEducationId
                    .setInstituteCourseId(npInstituteCourse.getId());

            // save npCandidateEducation
            NpCandidateEducation npCandidateEducation = new NpCandidateEducation();
            npCandidateEducation.setLevel(npCandidateEducationForm.getLevel());
            npCandidateEducation.setId(candidateEducationId);
            npCandidateEducation.setCreatedBy(npCandidate.getFirstName());
            npCandidateEducation.setModifiedBy(npCandidate.getFirstName());
            npCandidateEducation.setCreatedDate(createdDate);
            npCandidateEducation.setModifiedDate(modifiedDate);
            try
            {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                        "dd/MM/yyyy");
                Date startDate = simpleDateFormat.parse(npCandidateEducationForm
                        .getStartDate());
                Date endDate;
                if (npCandidateEducationForm.getEndDate() == null
                        || npCandidateEducationForm.getEndDate().equals("current")
                        || npCandidateEducationForm.getEndDate().trim().isEmpty())
                {
                    endDate = null;
                }
                else
                {
                    endDate = simpleDateFormat
                            .parse(npCandidateEducationForm.getEndDate());
                }
                npCandidateEducation.setStartDate(startDate);

                npCandidateEducation.setEndDate(endDate);
            }
            catch (ParseException e)
            {

                e.printStackTrace();
            }
            hibernateTemplate.save(npCandidateEducation);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCandidateLanguageNames(NpCandidate npCandidate)
    {
        List<String> candidateLanguageNames = new ArrayList<String>();
        List<NpCandidateLanguages> npCandidateLanguages = (List<NpCandidateLanguages>) hibernateTemplate
                .find("from NpCandidateLanguages where  CandidateId=?",
                        npCandidate.getId());
        if (npCandidateLanguages.size() > 0)
        {
            for (NpCandidateLanguages npCandidateLanguage : npCandidateLanguages)
            {
                NpLanguages npLanguage = hibernateTemplate.get(
                        NpLanguages.class, npCandidateLanguage.getId()
                                .getLanguageId());
                if (npLanguage != null)
                {
                    candidateLanguageNames.add(npLanguage.getLanguageName()
                            + "-" + npCandidateLanguage.getLanguageFluency());
                }
            }
        }
        return candidateLanguageNames;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCandidateInterestNames(NpCandidate npCandidate)
    {
        List<String> CandidateInterestNames = (List<String>) hibernateTemplate
                .find("select interestArea from NpCandidateInterests where  CandidateID=?",
                        npCandidate.getId());
        if (CandidateInterestNames.size() > 0)
        {
            return CandidateInterestNames;
        }
        else
        {
            return null;
        }
    }
}
