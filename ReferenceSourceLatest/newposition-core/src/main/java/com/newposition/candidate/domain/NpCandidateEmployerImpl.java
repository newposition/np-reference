package com.newposition.candidate.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.common.domain.NpToolImpl;

@Entity
@Table(name = "np_candidate_employer", catalog = "npdbs")
public class NpCandidateEmployerImpl implements NpCandidateEmployer
{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "uid")
    private int uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CandidateID", nullable = false)
    private NpCandidateImpl npCandidate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EmployerID", nullable = false)
    private NpEmployerImpl npEmployer;

    @Column(name = "JobTitle", nullable = false, length = 20)
    private String jobTitle;

    @Column(name = "CorporateTitle")
    private String corporateTitle;

    @Column(name = "SalaryCompensation")
    private Integer salaryCompensation;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @Column(name = "EmploymentType", length = 20)
    private String employmentType;

    @Column(name = "Achievements")
    private String achievements;

    @Column(name = "StartDate", nullable = false)
    private Date startDate;

    @Column(name = "Enddate", nullable = false)
    private Date enddate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "np_candidate_employer_skills", catalog = "npdbs", joinColumns = {
            @JoinColumn(name = "CandidateEmployerId", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "SkillId",
                    nullable = false, updatable = false)})
    private Set<NpSkillImpl> npSkills = new HashSet<NpSkillImpl>(0);

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "np_candidate_employer_tools", catalog = "npdbs", joinColumns = {
            @JoinColumn(name = "CandidateEmployerId", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "ToolID",
                    nullable = false, updatable = false)})
    private Set<NpToolImpl> npTools = new HashSet<NpToolImpl>(0);

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "np_candidate_curremp_roles", catalog = "npdbs", joinColumns = {
            @JoinColumn(name = "CandidateEmployerId", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "EmploymentRoleID",
                    nullable = false, updatable = false)})
    private Set<NpEmploymentRoleImpl> npEmploymentRoles = new HashSet<NpEmploymentRoleImpl>(0);

    @Override
    public Date getStartDate()
    {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    @Override
    public Date getEnddate()
    {
        return enddate;
    }

    @Override
    public void setEnddate(Date enddate)
    {
        this.enddate = enddate;
    }

    @Override
    public NpCandidate getNpCandidate()
    {
        return this.npCandidate;
    }

    @Override
    public void setNpCandidate(NpCandidate npCandidate)
    {
        this.npCandidate = (NpCandidateImpl) npCandidate;
    }

    @Override
    public NpEmployer getNpEmployer()
    {
        return this.npEmployer;
    }

    @Override
    public void setNpEmployer(NpEmployer npEmployer)
    {
        this.npEmployer = (NpEmployerImpl) npEmployer;
    }

    @Override
    public String getJobTitle()
    {
        return this.jobTitle;
    }

    @Override
    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    @Override
    public Integer getSalaryCompensation()
    {
        return this.salaryCompensation;
    }

    @Override
    public void setSalaryCompensation(Integer salaryCompensation)
    {
        this.salaryCompensation = salaryCompensation;
    }

    @Override
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String getEmploymentType()
    {
        return this.employmentType;
    }

    @Override
    public void setEmploymentType(String employmentType)
    {
        this.employmentType = employmentType;
    }

    @Override
    public String getCorporateTitle()
    {
        return corporateTitle;
    }

    @Override
    public void setCorporateTitle(String corporateTitle)
    {
        this.corporateTitle = corporateTitle;
    }

    @Override
    public String getAchievements()
    {
        return achievements;
    }

    @Override
    public void setAchievements(String achievements)
    {
        this.achievements = achievements;
    }

    @Override
    public int getUid()
    {
        return uid;
    }

    @Override
    public void setUid(int uid)
    {
        this.uid = uid;
    }

    @Override
    public Set<NpToolImpl> getNpTools()
    {
        return npTools;
    }

    @Override
    public void setNpTools(Set<NpToolImpl> npTools)
    {
        // TODO Auto-generated method stub
        this.npTools = npTools;
    }

    @Override
    public Set<NpEmploymentRoleImpl> getNpEmploymentRoles()
    {
        // TODO Auto-generated method stub
        return npEmploymentRoles;
    }

    @Override
    public void setNpEmploymentRoles(Set<NpEmploymentRoleImpl> npEmploymentRoles)
    {
        // TODO Auto-generated method stub
        this.npEmploymentRoles = npEmploymentRoles;
    }

    @Override
    public Set<NpSkillImpl> getNpSkills()
    {
        // TODO Auto-generated method stub
        return npSkills;
    }

    @Override
    public void setNpSkills(Set<NpSkillImpl> npSkills)
    {
        // TODO Auto-generated method stub
        this.npSkills = npSkills;
    }
}
