/**
 *
 */
package com.newposition.candidate.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.newposition.candidate.forms.NpCandidateReferenceForm;

/**
 * @author Sachi N
 */
public class NpCandidateReferenceFormValidator implements Validator
{

    protected static final Logger LOG = Logger
            .getLogger(NpCandidateReferenceFormValidator.class);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class<?> clazz)
    {
        // TODO Auto-generated method stub
        return NpCandidateReferenceFormValidator.class.equals(clazz);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.validation.Validator#validate(java.lang.Object,
     * org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object object, Errors errors)
    {

        NpCandidateReferenceForm npCandidateReferenceForm = (NpCandidateReferenceForm) object;

        String firstName = npCandidateReferenceForm.getFirstName();
        String surName = npCandidateReferenceForm.getSurName();
        String relationship = npCandidateReferenceForm.getRelationship();
        String email = npCandidateReferenceForm.getEmail();
        String fromDate = npCandidateReferenceForm.getDateFrom();

        String emailPattern = "^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]+\\.[a-zA-Z]{2,}$";

        if (StringUtils.isEmpty(firstName))
        {
            errors.rejectValue("firstName", "firstName.required");
        }
        if (StringUtils.isEmpty(surName))
        {
            errors.rejectValue("surName", "surName.required");
        }
        if (StringUtils.isEmpty(relationship))
        {
            errors.rejectValue("relationship", "relationship.required");
        }
        if (StringUtils.isEmpty(email))
        {
            errors.rejectValue("email", "email.required");
        }
        else if (email.length() > 0 && email.length() <= 50)
        {

            // Create a Pattern object
            Pattern pattern = Pattern.compile(emailPattern);

            // Now create matcher object.
            Matcher match = pattern.matcher(email.trim());

            if (!match.find())
            {
                errors.rejectValue("email", "email.format");
            }
            else
            {
                if (email.contains("..") || email.contains("--") || email.contains("__"))
                {
                    errors.rejectValue("email", "email.consecutive.periods");
                }
            }
        }
        else
        {
            errors.rejectValue("email", "email.toolong");
        }

        if (StringUtils.isEmpty(fromDate))
        {
            errors.rejectValue("dateFrom", "fromDate.required");
        }
    }

    public boolean isValidEmail(String email)
    {
        String emailPattern = "^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]+\\.[a-zA-Z]{2,}$";
        Pattern pattern = null;
        Matcher match = null;
        boolean isValidEmail = true;

        if (email.length() > 0 && email.length() <= 50)
        {

            // Create a Pattern object
            pattern = Pattern.compile(emailPattern);

            // Now create matcher object.
            match = pattern.matcher(email.trim());

            if (!match.find())
            {
                isValidEmail = false;
            }
            else
            {
                if (email.contains("..") || email.contains("--") || email.contains("__"))
                {
                    isValidEmail = false;
                }
            }
        }

        return isValidEmail;
    }
}
