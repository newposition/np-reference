/**
 *
 */
package com.newposition.candidate.forms;

/**
 * @author Sreenivas Arumilli
 */
public class NpCandidateCompensationForm
{

    private String compensationType;
    private double salary;
    private double bonus;
    private double dayrate;

    public String getCompensationType()
    {
        return compensationType;
    }

    public void setCompensationType(String compensationType)
    {
        this.compensationType = compensationType;
    }

    public double getSalary()
    {
        return salary;
    }

    public void setSalary(double salary)
    {
        this.salary = salary;
    }

    public double getBonus()
    {
        return bonus;
    }

    public void setBonus(double bonus)
    {
        this.bonus = bonus;
    }

    public double getDayrate()
    {
        return dayrate;
    }

    public void setDayrate(double dayrate)
    {
        this.dayrate = dayrate;
    }
}
