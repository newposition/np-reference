package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.newposition.admin.domain.CoInstituteAliasImpl;
import com.newposition.common.domain.CoCertificationAliasImpl;

/**
 * NpInstitutions generated by hbm2java
 */
@Entity
@Table(name = "np_institutions", catalog = "npdbs")
public class NpInstitutions implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String instituteName;
    private String instituteType;
    private String sector;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private String instituteLogo;
    private Set<NpInstituteLocation> npInstituteLocations = new HashSet<NpInstituteLocation>(0);
    private Set<NpInstituteCourse> npInstituteCourses = new HashSet<NpInstituteCourse>(0);
    private Set<NpCandidateCertificate> npCandidateCertificates = new HashSet<NpCandidateCertificate>(0);
    private boolean valid;
    private boolean status;
    private Set<CoInstituteAliasImpl> coInstituteAlias = new HashSet<CoInstituteAliasImpl>(0);

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Column(name = "InstituteName", nullable = false, length = 100)
    public String getInstituteName()
    {
        return this.instituteName;
    }

    public void setInstituteName(String instituteName)
    {
        this.instituteName = instituteName;
    }

    @Column(name = "InstituteType", length = 20)
    public String getInstituteType()
    {
        return this.instituteType;
    }

    public void setInstituteType(String instituteType)
    {
        this.instituteType = instituteType;
    }

    @Column(name = "Sector", length = 20)
    public String getSector()
    {
        return this.sector;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    @Column(name = "CreatedBy", nullable = false, length = 50)
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npInstitutions")
    public Set<NpInstituteLocation> getNpInstituteLocations()
    {
        return this.npInstituteLocations;
    }

    public void setNpInstituteLocations(
            Set<NpInstituteLocation> npInstituteLocations)
    {
        this.npInstituteLocations = npInstituteLocations;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npInstitutions")
    public Set<NpInstituteCourse> getNpInstituteCourses()
    {
        return this.npInstituteCourses;
    }

    public void setNpInstituteCourses(Set<NpInstituteCourse> npInstituteCourses)
    {
        this.npInstituteCourses = npInstituteCourses;
    }

    @Column(name = "InstituteLogo")
    public String getInstituteLogo()
    {
        return instituteLogo;
    }

    public void setInstituteLogo(String instituteLogo)
    {
        this.instituteLogo = instituteLogo;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npInstitutions")
    public Set<NpCandidateCertificate> getNpCandidateCertificates()
    {
        return npCandidateCertificates;
    }

    public void setNpCandidateCertificates(Set<NpCandidateCertificate> npCandidateCertificates)
    {
        this.npCandidateCertificates = npCandidateCertificates;
    }

    @Column(name = "Valid")
    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npInstitutions")
    public Set<CoInstituteAliasImpl> getCoInstituteAlias()
    {
        return coInstituteAlias;
    }

    public void setCoInstituteAlias(
            Set<CoInstituteAliasImpl> coInstituteAlias)
    {
        this.coInstituteAlias = coInstituteAlias;
    }

    @Column(name = "Status")
    public boolean isStatus()
    {
        return status;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }
}
