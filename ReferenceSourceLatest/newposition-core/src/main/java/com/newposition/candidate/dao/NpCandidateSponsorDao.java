/**
 *
 */
package com.newposition.candidate.dao;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.NpCandidateReferenceForm;

/**
 * @author Sachi Nayak This class persists Candidate Reference Details.
 */
public interface NpCandidateSponsorDao
{

    /*
     * @params npCandidateReferenceModel
     * This method persists Candidate Reference Details.
     *
     */
    void saveMySponsorDetails(NpCandidateReferenceForm npCandidateReferenceModel);

    /*
     * @param npCandidate
     * @return List of Sponsor Details of above Candidate
     *
     */
    List<NpCandidateReference> getListOfSponsors(NpCandidate npCandidate);

    /*
     *
     * @param email
     * @param npCandidate
     * @return
     */
    boolean isSponsorEmailExist(String email, NpCandidate npCandidate);

    /**
     * @param sponsorId
     * @return NpCandidateReference
     * <p/>
     * This method returns NpCandidateReference Object by id
     */
    NpCandidateReference fetchSponsorById(Integer sponsorId);
}
