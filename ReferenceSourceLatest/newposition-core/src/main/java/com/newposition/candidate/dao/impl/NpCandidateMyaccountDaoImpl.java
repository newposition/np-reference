package com.newposition.candidate.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.newposition.candidate.dao.NpCandidateMyaccountDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidatePreferenceForm;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserImpl;
import com.newposition.common.domain.NpUserStatus;
import com.newposition.common.domain.NpUserStatusImpl;

public class NpCandidateMyaccountDaoImpl extends HibernateDaoSupport implements
        NpCandidateMyaccountDao
{

    @Override
    public NpCandidate save(NpCandidate npCandidate)
    {
        Serializable id = getHibernateTemplate().save(npCandidate);
        npCandidate.setId((Integer) id);
        return npCandidate;
    }

    @Override
    public void merge(NpCandidate entity)
    {

    }

    @Override
    public void delete(NpCandidate entity)
    {

    }

    @Override
    public List<NpCandidate> findAll()
    {

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpCandidate findByID(Integer id)
    {

        List<NpCandidate> npCandidateList = (List<NpCandidate>) getHibernateTemplate()
                .find("from NpCandidateImpl where id=?", id);
        return npCandidateList.get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveOrUpdateCandidatePreferences(
            NpCandidatePreferenceForm npCandidatePreferenceForm,
            NpCandidate npCandidate)
    {
        NpCandidatePreference npCandidatePreference = getCandidatePreferences(npCandidate);

        if (npCandidatePreference != null)
        {
            npCandidatePreference = createOrUpdateaNpCandidatePreference(
                    npCandidatePreferenceForm, npCandidatePreference);
            getHibernateTemplate().update(npCandidatePreference);
        }
        else
        {
            npCandidatePreference = createOrUpdateaNpCandidatePreference(
                    npCandidatePreferenceForm, new NpCandidatePreference());
            npCandidatePreference.setNpCandidate((NpCandidateImpl) npCandidate);
            getHibernateTemplate().saveOrUpdate(npCandidatePreference);
        }
        List<NpUser> npUsers = (List<NpUser>) getHibernateTemplate().find(
                "from NpUserImpl where id=?", npCandidate.getId());
        NpUser user = npUsers.get(0);
        if (StringUtils.isNotEmpty(npCandidatePreferenceForm
                .getSecondaryEmail())
                && !npCandidatePreferenceForm.getSecondaryEmail().equals(
                user.getSecondaryEmail()))
        {

            npUsers.get(0).setSecondaryEmail(
                    npCandidatePreferenceForm.getSecondaryEmail());
        }
        if (StringUtils
                .isNotEmpty(npCandidatePreferenceForm.getTertiaryEmail())
                && !npCandidatePreferenceForm.getTertiaryEmail().equals(
                user.getThirdEmail()))
        {

            npUsers.get(0).setThirdEmail(
                    npCandidatePreferenceForm.getTertiaryEmail());
        }
        if (StringUtils.isNotEmpty(npCandidatePreferenceForm.getPrimaryEmail())
                && !npCandidatePreferenceForm.getPrimaryEmail().equals(
                user.getPrimaryEmail()))
        {

            npUsers.get(0).setPrimaryEmail(
                    npCandidatePreferenceForm.getPrimaryEmail());

            NpUserStatus npUserStatus = new NpUserStatusImpl();
            npUserStatus.setId(2);
            npUsers.get(0).setNpUserStatus(npUserStatus);
        }

        // getHibernateTemplate().saveOrUpdate(npUsers);

    }

    @SuppressWarnings("unchecked")
    @Override
    public NpCandidatePreference getCandidatePreferences(NpCandidate npCandidate)
    {

        List<NpCandidatePreference> npCandidatePreferences = (List<NpCandidatePreference>) getHibernateTemplate()
                .find("from NpCandidatePreference where candidateID=?",
                        npCandidate.getId());
        if (npCandidatePreferences != null && npCandidatePreferences.size() > 0)
        {
            return npCandidatePreferences.get(0);
        }
        return null;
    }

    @Override
    public void updateCandidateDocument(NpCandidateDocument candidateDocument)
    {
        getHibernateTemplate().saveOrUpdate(candidateDocument);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void saveCandidatePreference(
            NpCandidatePreference candidatePreference)
    {
        List<NpCandidatePreference> candidatePreferences = (List<NpCandidatePreference>) getHibernateTemplate()
                .find("from NpCandidatePreference where candidateID=?",
                        candidatePreference.getNpCandidate().getId());
        if (candidatePreferences.size() > 0)
        {
            NpCandidatePreference npCandidatePreference = candidatePreferences
                    .get(0);
            npCandidatePreference.setCompensation(candidatePreference
                    .getCompensation());
            npCandidatePreference.setBonus(candidatePreference.getBonus());
            npCandidatePreference.setType(candidatePreference.getType());
            npCandidatePreference.setDayRate(candidatePreference.getDayRate());
        }
        else
        {
            getHibernateTemplate().save(candidatePreference);
        }
    }

    public NpCandidatePreference createOrUpdateaNpCandidatePreference(
            NpCandidatePreferenceForm npCandidatePreferenceForm,
            NpCandidatePreference npCandidatePreference)
    {

        npCandidatePreference.setJobAppearance(npCandidatePreferenceForm
                .isJobAppearance());
        npCandidatePreference.setSendOpportunities(npCandidatePreferenceForm
                .isSendOpportunities());
        npCandidatePreference.setSendReports(npCandidatePreferenceForm
                .isSendReports());
        npCandidatePreference.setSummaryReports(npCandidatePreferenceForm
                .isSummaryReports());
        npCandidatePreference.setPermissionShortlist(npCandidatePreferenceForm
                .isPermissionShortlist());
        npCandidatePreference.setAcceptPhoto(npCandidatePreferenceForm
                .isAcceptPhoto());
        return npCandidatePreference;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateCandidateEmployer(NpCandidateEmployer candidateEmployer)
    {
        List<NpCandidateEmployer> npCandidateEmployers = (List<NpCandidateEmployer>) getHibernateTemplate().find("from NpCandidateEmployerImpl where uid=?", candidateEmployer.getUid());
        if (npCandidateEmployers.size() > 0)
        {
            npCandidateEmployers.get(0).setAchievements(candidateEmployer.getAchievements());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void updateCandidate(NpCandidateImpl npCandidate)
    {
        getHibernateTemplate().saveOrUpdate(npCandidate);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean checkEmailExists(String primaryEmail)
    {
        List<NpUserImpl> npUsers = (List<NpUserImpl>) getHibernateTemplate().find("from NpUserImpl where primaryEmail=?", primaryEmail);
        if (npUsers.size() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId)
    {
        List<NpCandidateEmployer> npCandidateEmployer = (List<NpCandidateEmployer>) getHibernateTemplate().find(
                "from NpCandidateEmployerImpl where CandidateID=? order by startDate", candidateId);

        return npCandidateEmployer;
    }
}
