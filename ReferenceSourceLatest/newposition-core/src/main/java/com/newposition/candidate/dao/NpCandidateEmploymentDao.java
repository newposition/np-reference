package com.newposition.candidate.dao;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.forms.NpCandidateEmploymentForm;
import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpTool;

public interface NpCandidateEmploymentDao
{

    public int getCompanyId(String companyName);

    public NpEmployer saveEmployer(NpEmployer npEmployer);

    public NpEmployer getNpEmployerById(int npEmployerId);

    public int saveEmployment(NpCandidateEmployer candidateEmployer);

    public void deleteEmployment(int candidateEmployerId);

    public void updateEmployment(NpCandidateEmploymentForm employmentForm);

    public List<NpSkill> getSkills(String id);

    public List<NpEmploymentRole> getRoles(String id);

    public List<NpTool> getTools(String id);

    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId);

    public int saveCandidateCompanySkill(String employerId, String skillId);

    public int saveCandidateCompanyRole(String employerId, String roleId);

    public int saveCandidateCompanyTool(String employerId, String toolId);

    public void deleteCandidateSkill(String employerId, String skillId, int candidateId);

    public void deleteCandidateRole(String employerId, String skillId, int candidateId);

    public void deleteCandidateTool(String employerId, String skillId, int candidateId);

    public void deleteCandidateCompanySkill(String employerId, String skillId);

    public void deleteCandidateCompanyRole(String employerId, String roleId);

    public void deleteCandidateCompanyTool(String employerId, String toolId);

    public boolean saveCandidateSkill(String skill, int candidateId, String employID, String domainType, boolean currentCompany);

    public boolean saveCandidateRole(String role, int candidateId, String employID, String domainType, boolean currentCompany);

    public boolean saveCandidateTool(String tool, int candidateId, String employID, String domainType, boolean currentCompany);

    public List<String> getEmployerNames();

    public void deleteCandidateTechnicalDetails(NpCandidate npCandidate);

    public List<NpCandidateSkills> getNpCandidateSkills(NpCandidate npCandidate);

    public List<NpCandidateTools> getNpCandidateTools(NpCandidate npCandidate);

    public List<NpCandidateEmpRoles> getNpCandidateRoles(NpCandidate npCandidate);

    public List<String> getListOfSkills();

    public List<String> getListOfRoles();

    public List<String> getListOfTools();

    public NpSkill getSkillBySkillName(String skillName);

    public NpEmploymentRole getRoleByRoleName(String roleName);

    public NpTool getToolByToolName(String toolName);
}
