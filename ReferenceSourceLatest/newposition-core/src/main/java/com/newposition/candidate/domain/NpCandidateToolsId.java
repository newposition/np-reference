package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * NpCandidateToolsId generated by hbm2java
 */
@Embeddable
public class NpCandidateToolsId implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = "ToolID", nullable = false)
    private int toolId;

    @Column(name = "CandidateID", nullable = false)
    private int candidateId;

    public int getToolId()
    {
        return this.toolId;
    }

    public void setToolId(int toolId)
    {
        this.toolId = toolId;
    }

    public int getCandidateId()
    {
        return this.candidateId;
    }

    public void setCandidateId(int candidateId)
    {
        this.candidateId = candidateId;
    }
}
