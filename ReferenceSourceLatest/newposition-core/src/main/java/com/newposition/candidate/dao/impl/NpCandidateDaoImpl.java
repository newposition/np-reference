package com.newposition.candidate.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.newposition.candidate.dao.NpCandidateDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.common.domain.NpUser;

public class NpCandidateDaoImpl extends HibernateDaoSupport implements NpCandidateDao
{

    @Override
    public NpCandidate save(NpCandidate npCandidate)
    {
        Serializable id = getHibernateTemplate().save(npCandidate);
        npCandidate.setId((Integer) id);
        return npCandidate;
    }

    @Override
    public void merge(NpCandidate entity)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(NpCandidate entity)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public List<NpCandidate> findAll()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpCandidate findByID(Integer id)
    {
        // TODO Auto-generated method stub
        List<NpCandidate> npCandidateList = (List<NpCandidate>) getHibernateTemplate().find("from NpCandidateImpl where id=?", id);
        return npCandidateList.get(0);
    }

    @Override
    public List<NpCandidateReference> getCandidateReferenceList(String referEmail)
    {
        // TODO Auto-generated method stub
        @SuppressWarnings("unchecked")
        List<NpCandidateReference> npCandidateReferences = (List<NpCandidateReference>) getHibernateTemplate().find("from NpCandidateReference where email=? and referenceVerified= 'waiting'", referEmail);

        return npCandidateReferences;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpCandidateReference getCurrentCandidateReference(String primaryEmail, Integer candidateId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateReference> npCandidateReferences = (List<NpCandidateReference>) getHibernateTemplate().find("from NpCandidateReference where email=? AND CandidateID = ?", primaryEmail, candidateId);
        if (npCandidateReferences.size() > 0)
        {
            return npCandidateReferences.get(0);
        }
        return null;
    }

    @Override
    public void varifyReference(NpCandidateReference npCandidateReference)
    {

        getHibernateTemplate().update(npCandidateReference);
    }
}
