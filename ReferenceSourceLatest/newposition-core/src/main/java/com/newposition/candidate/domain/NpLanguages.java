package com.newposition.candidate.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "np_languages", catalog = "npdbs")
public class NpLanguages implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String languageName;
    private String fluency;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", unique = true, nullable = false)
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Column(name = "LanguageName")
    public String getLanguageName()
    {
        return languageName;
    }

    public void setLanguageName(String languageName)
    {
        this.languageName = languageName;
    }

    @Column(name = "Fluency")
    public String getFluency()
    {
        return fluency;
    }

    public void setFluency(String fluency)
    {
        this.fluency = fluency;
    }
}
