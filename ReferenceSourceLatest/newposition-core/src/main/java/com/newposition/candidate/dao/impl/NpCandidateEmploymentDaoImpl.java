package com.newposition.candidate.dao.impl;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.candidate.dao.NpCandidateEmploymentDao;
import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateCurrentEmpRoles;
import com.newposition.candidate.domain.NpCandidateCurrentEmpRolesId;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateEmpRolesId;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateEmployerSkills;
import com.newposition.candidate.domain.NpCandidateEmployerSkillsId;
import com.newposition.candidate.domain.NpCandidateEmployerTools;
import com.newposition.candidate.domain.NpCandidateEmployerToolsId;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateSkillsId;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.domain.NpCandidateToolsId;
import com.newposition.candidate.domain.NpExpertLevel;
import com.newposition.candidate.forms.NpCandidateEmploymentForm;
import com.newposition.common.domain.NpDomain;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.common.domain.NpTool;
import com.newposition.common.domain.NpToolImpl;

public class NpCandidateEmploymentDaoImpl implements NpCandidateEmploymentDao
{

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpEmployer getNpEmployerById(int npEmployerId)
    {
        // TODO Auto-generated method stub

        List<NpEmployer> npEmployer = (List<NpEmployer>) hibernateTemplate.find("from NpEmployerImpl where id=?", npEmployerId);

        if (npEmployer.size() > 0)
        {
            return npEmployer.get(0);
        }

        return null;
    }

    @Transactional
    public NpEmployer saveEmployer(NpEmployer npEmployer)
    {
        Serializable Id = hibernateTemplate.save(npEmployer);
        NpEmployer npEmployers = hibernateTemplate.get(NpEmployerImpl.class, Id);
        return npEmployers;
    }

    @SuppressWarnings("unchecked")
    @Override
    public int getCompanyId(String companyName)
    {
        // TODO Auto-generated method stub

        List<NpEmployer> npEmployer = (List<NpEmployer>) hibernateTemplate.find("from NpEmployerImpl where name=?", companyName);
        if (npEmployer.size() > 0)
        {
            return npEmployer.get(0).getId();
        }
        else
        {
            return 0;
        }
    }

    @Override
    public int saveEmployment(NpCandidateEmployer candidateEmployer)
    {

        Serializable id = hibernateTemplate.save(candidateEmployer);

        return (Integer) id;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteEmployment(int candidateEmployerId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployer> npCandidateEmployers = (List<NpCandidateEmployer>) hibernateTemplate.find("from NpCandidateEmployerImpl where uid=?", candidateEmployerId);

        if (npCandidateEmployers.size() > 0)
        {

            hibernateTemplate.delete(npCandidateEmployers.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void updateEmployment(NpCandidateEmploymentForm employmentForm)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployer> npCandidateEmployers = (List<NpCandidateEmployer>) hibernateTemplate.find("from NpCandidateEmployerImpl where uid=?", Integer.parseInt(employmentForm.getEmployerId()));

        NpCandidateEmployer npCandidateEmployer = npCandidateEmployers.get(0);
        npCandidateEmployer.setJobTitle(employmentForm.getJobTitle());
        npCandidateEmployer.setAchievements(employmentForm.getAchievements());
        npCandidateEmployer.setCorporateTitle(employmentForm.getCorpTitle());
//        NpCandidateEmployerId npCandidateEmployerId=new NpCandidateEmployerId();
//        npCandidateEmployerId.setCandidateId(candidateId);
        int companyId;
        if (getCompanyId(employmentForm.getEmployerName()) > 0)
        {
            companyId = getCompanyId(employmentForm.getEmployerName());
            npCandidateEmployer.setNpEmployer(getNpEmployerById(companyId));
        }
        else
        {
            NpEmployer npEmployer = new NpEmployerImpl();
            npEmployer.setName(employmentForm.getEmployerName());
            npEmployer = saveEmployer(npEmployer);
            companyId = npEmployer.getId();
            npCandidateEmployer.setNpEmployer(npEmployer);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateInString = employmentForm.getStartDate();
        String dateInStringTo = employmentForm.getEndDate();
        Date dateFrom, dateTo;
        try
        {
            dateFrom = sdf.parse(dateInString);
            if (dateInStringTo.trim().equals("current") || dateInStringTo.trim().isEmpty() || dateInStringTo == null)
            {
                dateTo = null;
            }
            else
            {
                dateTo = sdf.parse(dateInStringTo);
            }
            npCandidateEmployer.setStartDate(dateFrom);
            npCandidateEmployer.setEnddate(dateTo);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

//        npCandidateEmployer.setId(npCandidateEmployerId);

        hibernateTemplate.update(npCandidateEmployer);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpSkill> getSkills(String id)
    {
        hibernateTemplate.setMaxResults(50);
        List<NpSkill> technicalCompitencies = (List<NpSkill>) hibernateTemplate.find("from NpSkillImpl where domainTypeId=? and status=1", id);
        return technicalCompitencies;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpEmploymentRole> getRoles(String id)
    {
        // TODO Auto-generated method stub
        hibernateTemplate.setMaxResults(50);
        List<NpEmploymentRole> roles = (List<NpEmploymentRole>) hibernateTemplate.find("from NpEmploymentRoleImpl where domainTypeId=? and status=1", id);
        return roles;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpTool> getTools(String id)
    {
        // TODO Auto-generated method stub
        hibernateTemplate.setMaxResults(50);
        List<NpTool> tools = (List<NpTool>) hibernateTemplate.find("from NpToolImpl where domainTypeId=? and status=1", id);
        return tools;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId)
    {
        List<NpCandidateEmployer> npCandidateEmployer = (List<NpCandidateEmployer>) hibernateTemplate.find(
                "from NpCandidateEmployerImpl where CandidateID=? ", candidateId);

        return npCandidateEmployer;
    }

    @SuppressWarnings("unchecked")
    @Override
    public int saveCandidateCompanySkill(String employerId, String skillId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployerSkills> npCandidateEmployerSavedSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate.find(
                "from NpCandidateEmployerSkills where CandidateEmployerId=? and SkillID=?", Integer.parseInt(employerId),
                Integer.parseInt(skillId));
        if (npCandidateEmployerSavedSkills.size() > 0)
        {
            return 0;
        }
        else
        {
            NpCandidateEmployerSkillsId npCandidateEmployerSkillsId = new NpCandidateEmployerSkillsId();

            npCandidateEmployerSkillsId.setCandidateEmployerId(Integer.parseInt(employerId));
            npCandidateEmployerSkillsId.setSkillId(Integer.parseInt(skillId));

            NpCandidateEmployerSkills npcCandidateEmployerSkills = new NpCandidateEmployerSkills();
            npcCandidateEmployerSkills.setNpCandidateEmployerSkillsId(npCandidateEmployerSkillsId);

            hibernateTemplate.save(npcCandidateEmployerSkills);
            return 1;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public int saveCandidateCompanyRole(String employerId, String roleId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateCurrentEmpRoles> npCandidateEmployerSavedRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate.find(
                "from NpCandidateCurrentEmpRoles where CandidateEmployerId=? and EmploymentRoleID=?", Integer.parseInt(employerId),
                Integer.parseInt(roleId));
        if (npCandidateEmployerSavedRoles.size() > 0)
        {
            return 0;
        }
        else
        {
            NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId = new NpCandidateCurrentEmpRolesId();

            npCandidateCurrentEmpRolesId.setCandidateEmployerId(Integer.parseInt(employerId));
            npCandidateCurrentEmpRolesId.setEmploymentRoleID(Integer.parseInt(roleId));

            NpCandidateCurrentEmpRoles npCandidateCurrentEmpRoles = new NpCandidateCurrentEmpRoles();
            npCandidateCurrentEmpRoles.setNpCandidateCurrentEmpRolesId(npCandidateCurrentEmpRolesId);

            hibernateTemplate.save(npCandidateCurrentEmpRoles);
            return 1;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public int saveCandidateCompanyTool(String employerId, String toolId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployerTools> npCandidateEmployerSavedTools = (List<NpCandidateEmployerTools>) hibernateTemplate.find(
                "from NpCandidateEmployerTools where CandidateEmployerId=? and ToolID=?", Integer.parseInt(employerId),
                Integer.parseInt(toolId));
        if (npCandidateEmployerSavedTools.size() > 0)
        {
            return 0;
        }
        else
        {
            NpCandidateEmployerToolsId npCandidateEmployerToolsId = new NpCandidateEmployerToolsId();

            npCandidateEmployerToolsId.setCandidateEmployerId(Integer.parseInt(employerId));
            npCandidateEmployerToolsId.setToolID(Integer.parseInt(toolId));

            NpCandidateEmployerTools npCandidateEmployerTools = new NpCandidateEmployerTools();
            npCandidateEmployerTools.setNpCandidateEmployerToolsId(npCandidateEmployerToolsId);

            hibernateTemplate.save(npCandidateEmployerTools);
            return 1;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateSkill(String employerId, String skillId, int candidateId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateSkills> npCandidateSkills = (List<NpCandidateSkills>) hibernateTemplate.find(
                "from NpCandidateSkills where CandidateID=? and SkillID=?", candidateId, Integer.parseInt(skillId));
        if (npCandidateSkills.size() > 0)
        {
            NpCandidateSkills npCandidateSkill = npCandidateSkills.get(0);
            hibernateTemplate.delete(npCandidateSkill);

            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", candidateId);

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateEmployerSkills> npCandidateEmployerSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate.find(
                            "from NpCandidateEmployerSkills where CandidateEmployerId=? and SkillId=?",
                            candidateEmployerId, Integer.parseInt(skillId));

                    if (npCandidateEmployerSkills.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateEmployerSkills.get(0));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateRole(String employerId, String roleId, int candidateId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmpRoles> npCandidateEmpRoles = (List<NpCandidateEmpRoles>) hibernateTemplate.find(
                "from NpCandidateEmpRoles where CandidateID=? and EmploymentRoleID=?", candidateId, Integer.parseInt(roleId));

        if (npCandidateEmpRoles.size() > 0)
        {
            hibernateTemplate.delete(npCandidateEmpRoles.get(0));

            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", candidateId);

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateCurrentEmpRoles> npCandidateCurrentEmpRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate
                            .find("from NpCandidateCurrentEmpRoles where CandidateEmployerId=? and EmploymentRoleID=?",
                                    candidateEmployerId, Integer.parseInt(roleId));

                    if (npCandidateCurrentEmpRoles.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateCurrentEmpRoles.get(0));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateTool(String employerId, String toolId, int candidateId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateTools> npCandidateTools = (List<NpCandidateTools>) hibernateTemplate.find(
                "from NpCandidateTools where CandidateID=? and ToolID=?", candidateId, Integer.parseInt(toolId));
        if (npCandidateTools.size() > 0)
        {
            hibernateTemplate.delete(npCandidateTools.get(0));

            List<Integer> candidateEmployerIds = (List<Integer>) hibernateTemplate.find(
                    "select uid from NpCandidateEmployerImpl where CandidateID=?", candidateId);

            if (candidateEmployerIds.size() > 0)
            {

                for (Integer candidateEmployerId : candidateEmployerIds)
                {
                    List<NpCandidateEmployerTools> npCandidateEmployerTools = (List<NpCandidateEmployerTools>) hibernateTemplate.find(
                            "from NpCandidateEmployerTools where CandidateEmployerId=? and ToolID=?",
                            candidateEmployerId, Integer.parseInt(toolId));

                    if (npCandidateEmployerTools.size() > 0)
                    {

                        hibernateTemplate.delete(npCandidateEmployerTools.get(0));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateCompanySkill(String employerId, String skillId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployerSkills> npCandidateEmployerSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate.find(
                "from NpCandidateEmployerSkills where CandidateEmployerId=? and SkillId=?",
                Integer.parseInt(employerId), Integer.parseInt(skillId));

        if (npCandidateEmployerSkills.size() > 0)
        {

            hibernateTemplate.delete(npCandidateEmployerSkills.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateCompanyRole(String employerId, String roleId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateCurrentEmpRoles> npCandidateCurrentEmpRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate
                .find("from NpCandidateCurrentEmpRoles where CandidateEmployerId=? and EmploymentRoleID=?",
                        Integer.parseInt(employerId), Integer.parseInt(roleId));

        if (npCandidateCurrentEmpRoles.size() > 0)
        {

            hibernateTemplate.delete(npCandidateCurrentEmpRoles.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteCandidateCompanyTool(String employerId, String toolId)
    {
        // TODO Auto-generated method stub
        List<NpCandidateEmployerTools> npCandidateEmployerTools = (List<NpCandidateEmployerTools>) hibernateTemplate.find(
                "from NpCandidateEmployerTools where CandidateEmployerId=? and ToolID=?",
                Integer.parseInt(employerId), Integer.parseInt(toolId));

        if (npCandidateEmployerTools.size() > 0)
        {

            hibernateTemplate.delete(npCandidateEmployerTools.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean saveCandidateSkill(String skill, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        // TODO Auto-generated method stub
        String[] skillItems = StringUtils.split(skill, ' ');
        if (skillItems != null && skillItems.length == 3)
        {

            List<NpExpertLevel> npExpertLevels = (List<NpExpertLevel>) hibernateTemplate
                    .find("from NpExpertLevel where ExpertLevelName=?", skillItems[2]);
            if (npExpertLevels.size() > 0)
            {
                List<NpSkill> npSkills = (List<NpSkill>) hibernateTemplate
                        .find("from NpSkillImpl where skillName=?", skillItems[0]);
                if (npSkills.size() > 0)
                {
                    List<NpCandidateSkills> npCandidateSkills = (List<NpCandidateSkills>) hibernateTemplate
                            .find("from NpCandidateSkills where SkilliD=? and CandidateID=?", npSkills.get(0).getId(), candidateId);

                    if (npCandidateSkills.size() > 0)
                    {
                        NpCandidateSkills npCandidateSkill = npCandidateSkills.get(0);
                        npCandidateSkill.setNpExpertLevel(npExpertLevels.get(0));
                        npCandidateSkill.setYearsOfExperience(Integer.parseInt(skillItems[1]));
                        hibernateTemplate.saveOrUpdate(npCandidateSkill);
                        if (currentCompany)
                        {
                            List<NpCandidateEmployerSkills> npCandidateEmployerSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate
                                    .find("from NpCandidateEmployerSkills where SkilliD=? and CandidateEmployerId=?", npSkills.get(0).getId(),
                                            Integer.parseInt(employID));
                            if (npCandidateEmployerSkills.size() == 0)
                            {
                                NpCandidateEmployerSkillsId npCandidateEmployerSkillsId = new NpCandidateEmployerSkillsId();
                                npCandidateEmployerSkillsId.setCandidateEmployerId(Integer.parseInt(employID));
                                npCandidateEmployerSkillsId.setSkillId(npSkills.get(0).getId());
                                NpCandidateEmployerSkills npCandidateEmployerSkill = new NpCandidateEmployerSkills();
                                npCandidateEmployerSkill.setNpCandidateEmployerSkillsId(npCandidateEmployerSkillsId);
                                // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                                hibernateTemplate.saveOrUpdate(npCandidateEmployerSkill);
                            }
                        }
                    }
                    else
                    {
                        // creating new candidate skill
                        NpCandidateSkills npCandidateSkill = new NpCandidateSkills();
                        NpCandidateSkillsId npCandidateSkillsId = new NpCandidateSkillsId();
                        npCandidateSkill.setNpSkill((NpSkillImpl) npSkills.get(0));
                        npCandidateSkill.setNpExpertLevel(npExpertLevels.get(0));
                        npCandidateSkill.setYearsOfExperience(Integer.parseInt(skillItems[1]));
                        npCandidateSkill.setPreferfutureSkill(0);
                        npCandidateSkillsId.setSkillId(npSkills.get(0).getId());
                        npCandidateSkillsId.setCandidateId(candidateId);
                        npCandidateSkill.setId(npCandidateSkillsId);
                        // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                        hibernateTemplate.saveOrUpdate(npCandidateSkill);

                        if (currentCompany)
                        {
                            // creating new candidate emp skill
                            List<NpCandidateEmployerSkills> npCandidateEmployerSkills = (List<NpCandidateEmployerSkills>) hibernateTemplate
                                    .find("from NpCandidateEmployerSkills where SkilliD=? and CandidateEmployerId=?", npSkills.get(0).getId(),
                                            Integer.parseInt(employID));
                            if (npCandidateEmployerSkills.size() == 0)
                            {
                                NpCandidateEmployerSkillsId npCandidateEmployerSkillsId = new NpCandidateEmployerSkillsId();
                                npCandidateEmployerSkillsId.setCandidateEmployerId(Integer.parseInt(employID));
                                npCandidateEmployerSkillsId.setSkillId(npSkills.get(0).getId());
                                NpCandidateEmployerSkills npCandidateEmployerSkill = new NpCandidateEmployerSkills();
                                npCandidateEmployerSkill.setNpCandidateEmployerSkillsId(npCandidateEmployerSkillsId);
                                // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                                hibernateTemplate.saveOrUpdate(npCandidateEmployerSkill);
                            }
                        }
                    }
                }
                else
                {
                    NpSkill npSkill = new NpSkillImpl();
                    NpDomain npDomainType = hibernateTemplate.get(NpDomainImpl.class, Integer.parseInt(domainType));
                    npSkill.setNpDomainTypes((NpDomainImpl) npDomainType);
                    npSkill.setSkillName(skillItems[0]);
                    npSkill.setStatus(true);
                    npSkill.setValid("Valid");
                    Serializable skillID = hibernateTemplate.save(npSkill);
                    // creating new candidate skill
                    NpCandidateSkills npCandidateSkill = new NpCandidateSkills();
                    NpCandidateSkillsId npCandidateSkillsId = new NpCandidateSkillsId();
                    npCandidateSkill.setNpSkill((NpSkillImpl) npSkill);
                    npCandidateSkill.setNpExpertLevel(npExpertLevels.get(0));
                    npCandidateSkill.setYearsOfExperience(Integer.parseInt(skillItems[1]));
                    npCandidateSkill.setPreferfutureSkill(0);
                    npCandidateSkillsId.setSkillId((Integer) skillID);
                    npCandidateSkillsId.setCandidateId(candidateId);
                    npCandidateSkill.setId(npCandidateSkillsId);
                    // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                    hibernateTemplate.saveOrUpdate(npCandidateSkill);

                    // creating new candidate emp skill
                    NpCandidateEmployerSkillsId npCandidateEmployerSkillsId = new NpCandidateEmployerSkillsId();
                    npCandidateEmployerSkillsId.setCandidateEmployerId(Integer.parseInt(employID));
                    npCandidateEmployerSkillsId.setSkillId((Integer) skillID);
                    NpCandidateEmployerSkills npCandidateEmployerSkill = new NpCandidateEmployerSkills();
                    npCandidateEmployerSkill.setNpCandidateEmployerSkillsId(npCandidateEmployerSkillsId);
                    // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                    hibernateTemplate.saveOrUpdate(npCandidateEmployerSkill);
                }
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean saveCandidateRole(String role, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        String[] roleItems = StringUtils.split(role, ' ');
        if (roleItems != null && roleItems.length == 2)
        {

            List<NpEmploymentRoleImpl> npEmploymentRoles = (List<NpEmploymentRoleImpl>) hibernateTemplate
                    .find("from NpEmploymentRoleImpl where roleName=?", roleItems[0]);

            if (npEmploymentRoles.size() > 0)
            {
                List<NpCandidateEmpRoles> npCandidateEmpRoles = (List<NpCandidateEmpRoles>) hibernateTemplate.find("from NpCandidateEmpRoles where EmploymentRoleID=? and CandidateID=?",
                        npEmploymentRoles.get(0).getId(), candidateId);

                if (npCandidateEmpRoles.size() > 0)
                {
                    NpCandidateEmpRoles npCandidateEmpRole = npCandidateEmpRoles.get(0);
                    npCandidateEmpRole.setYearsOfExperience(Integer.parseInt(roleItems[1]));
                    hibernateTemplate.saveOrUpdate(npCandidateEmpRole);
                    if (currentCompany)
                    {
                        List<NpCandidateCurrentEmpRoles> npCandidateCurrentEmpRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate
                                .find("from NpCandidateCurrentEmpRoles where EmploymentRoleID=? and CandidateEmployerId=?",
                                        npEmploymentRoles.get(0).getId(), Integer.parseInt(employID));
                        if (npCandidateCurrentEmpRoles.size() == 0)
                        {
                            NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId = new NpCandidateCurrentEmpRolesId();
                            npCandidateCurrentEmpRolesId.setCandidateEmployerId(Integer.parseInt(employID));
                            npCandidateCurrentEmpRolesId.setEmploymentRoleID(npEmploymentRoles.get(0).getId());
                            NpCandidateCurrentEmpRoles npCandidateCurrentEmpRole = new NpCandidateCurrentEmpRoles();
                            npCandidateCurrentEmpRole.setNpCandidateCurrentEmpRolesId(npCandidateCurrentEmpRolesId);
                            // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                            hibernateTemplate.saveOrUpdate(npCandidateCurrentEmpRole);
                        }
                    }
                }
                else
                {
                    // creating new candidate skill
                    NpCandidateEmpRoles npCandidateEmpRole = new NpCandidateEmpRoles();
                    NpCandidateEmpRolesId npCandidateEmpRolesId = new NpCandidateEmpRolesId();
                    npCandidateEmpRole.setNpEmploymentRoles(npEmploymentRoles.get(0));
                    npCandidateEmpRole.setYearsOfExperience(Integer.parseInt(roleItems[1]));
                    npCandidateEmpRole.setFuturePreferedRole(0);
                    npCandidateEmpRolesId.setEmploymentRoleId(npEmploymentRoles.get(0).getId());
                    npCandidateEmpRolesId.setCandidateId(candidateId);
                    npCandidateEmpRole.setId(npCandidateEmpRolesId);
                    // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                    hibernateTemplate.saveOrUpdate(npCandidateEmpRole);

                    // creating new candidate emp skill
                    if (currentCompany)
                    {
                        List<NpCandidateCurrentEmpRoles> npCandidateCurrentEmpRoles = (List<NpCandidateCurrentEmpRoles>) hibernateTemplate
                                .find("from NpCandidateCurrentEmpRoles where EmploymentRoleID=? and CandidateEmployerId=?", npEmploymentRoles.get(0)
                                        .getId(), Integer.parseInt(employID));
                        if (npCandidateCurrentEmpRoles.size() == 0)
                        {
                            NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId = new NpCandidateCurrentEmpRolesId();
                            npCandidateCurrentEmpRolesId.setCandidateEmployerId(Integer.parseInt(employID));
                            npCandidateCurrentEmpRolesId.setEmploymentRoleID(npEmploymentRoles.get(0).getId());
                            NpCandidateCurrentEmpRoles npCandidateCurrentEmpRole = new NpCandidateCurrentEmpRoles();
                            npCandidateCurrentEmpRole.setNpCandidateCurrentEmpRolesId(npCandidateCurrentEmpRolesId);
                            // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                            hibernateTemplate.saveOrUpdate(npCandidateCurrentEmpRole);
                        }
                    }
                }
            }
            else
            {
                NpEmploymentRole npEmploymentRole = new NpEmploymentRoleImpl();
                NpDomain npDomainType = hibernateTemplate.get(NpDomainImpl.class, Integer.parseInt(domainType));
                npEmploymentRole.setNpDomain((NpDomainImpl) npDomainType);
                npEmploymentRole.setRoleName(roleItems[0]);
                npEmploymentRole.setStatus(true);
                npEmploymentRole.setValid("Valid");
                Serializable roleID = hibernateTemplate.save(npEmploymentRole);
                // creating new candidate skill
                NpCandidateEmpRoles npCandidateEmpRole = new NpCandidateEmpRoles();
                NpCandidateEmpRolesId npCandidateEmpRolesId = new NpCandidateEmpRolesId();
                npCandidateEmpRole.setNpEmploymentRoles((NpEmploymentRoleImpl) npEmploymentRole);
                npCandidateEmpRole.setYearsOfExperience(Integer.parseInt(roleItems[1]));
                npCandidateEmpRole.setFuturePreferedRole(0);
                npCandidateEmpRolesId.setEmploymentRoleId((Integer) roleID);
                npCandidateEmpRolesId.setCandidateId(candidateId);
                npCandidateEmpRole.setId(npCandidateEmpRolesId);
                // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                hibernateTemplate.saveOrUpdate(npCandidateEmpRole);

                // creating new candidate emp skill
                NpCandidateCurrentEmpRolesId npCandidateCurrentEmpRolesId = new NpCandidateCurrentEmpRolesId();
                npCandidateCurrentEmpRolesId.setCandidateEmployerId(Integer.parseInt(employID));
                npCandidateCurrentEmpRolesId.setEmploymentRoleID((Integer) roleID);
                NpCandidateCurrentEmpRoles npCandidateCurrentEmpRole = new NpCandidateCurrentEmpRoles();
                npCandidateCurrentEmpRole.setNpCandidateCurrentEmpRolesId(npCandidateCurrentEmpRolesId);
                // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                hibernateTemplate.saveOrUpdate(npCandidateCurrentEmpRole);
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean saveCandidateTool(String tool, int candidateId, String employID, String domainType, boolean currentCompany)
    {
        // TODO Auto-generated method stub
        String[] toolItems = StringUtils.split(tool, ' ');
        if (toolItems != null && toolItems.length == 3)
        {
            List<NpExpertLevel> npExpertLevels = (List<NpExpertLevel>) hibernateTemplate
                    .find("from NpExpertLevel where ExpertLevelName=?", toolItems[2]);
            if (npExpertLevels.size() > 0)
            {
                List<NpToolImpl> npTools = (List<NpToolImpl>) hibernateTemplate.find("from NpToolImpl where toolName=?", toolItems[0]);

                if (npTools.size() > 0)
                {
                    List<NpCandidateTools> npCandidateTools = (List<NpCandidateTools>) hibernateTemplate
                            .find("from NpCandidateTools where ToolID=? and CandidateID=?", npTools.get(0).getId(), candidateId);
                    if (npCandidateTools.size() > 0)
                    {
                        NpCandidateTools npCandidateTool = npCandidateTools.get(0);
                        npCandidateTool.setNpExpertLevel(npExpertLevels.get(0));
                        npCandidateTool.setYearsOfExperience(Integer.parseInt(toolItems[1]));
                        hibernateTemplate.saveOrUpdate(npCandidateTool);

                        if (currentCompany)
                        {
                            List<NpCandidateEmployerTools> npCandidateEmployerTools = (List<NpCandidateEmployerTools>) hibernateTemplate
                                    .find("from NpCandidateEmployerTools where ToolID=? and CandidateEmployerId=?", npTools.get(0).getId(),
                                            Integer.parseInt(employID));
                            if (npCandidateEmployerTools.size() == 0)
                            {
                                NpCandidateEmployerToolsId npCandidateEmployerToolsId = new NpCandidateEmployerToolsId();
                                npCandidateEmployerToolsId.setCandidateEmployerId(Integer.parseInt(employID));
                                npCandidateEmployerToolsId.setToolID(npTools.get(0).getId());
                                NpCandidateEmployerTools npCandidateEmployerTool = new NpCandidateEmployerTools();
                                npCandidateEmployerTool.setNpCandidateEmployerToolsId(npCandidateEmployerToolsId);
                                // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                                hibernateTemplate.saveOrUpdate(npCandidateEmployerTool);
                            }
                        }
                    }
                    else
                    {
                        // creating new candidate skill
                        NpCandidateTools npCandidateTool = new NpCandidateTools();
                        NpCandidateToolsId npCandidateToolsId = new NpCandidateToolsId();
                        npCandidateTool.setNpTools(npTools.get(0));
                        npCandidateTool.setNpExpertLevel(npExpertLevels.get(0));
                        npCandidateTool.setYearsOfExperience(Integer.parseInt(toolItems[1]));
                        npCandidateTool.setFuturePreferedTool(0);
                        npCandidateToolsId.setToolId(npTools.get(0).getId());
                        npCandidateToolsId.setCandidateId(candidateId);
                        npCandidateTool.setId(npCandidateToolsId);
                        // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                        hibernateTemplate.saveOrUpdate(npCandidateTool);
                        // creating new candidate emp skill
                        if (currentCompany)
                        {
                            List<NpCandidateEmployerTools> npCandidateEmployerTools = (List<NpCandidateEmployerTools>) hibernateTemplate
                                    .find("from NpCandidateEmployerTools where ToolId=? and CandidateEmployerId=?", npTools.get(0).getId(),
                                            Integer.parseInt(employID));
                            if (npCandidateEmployerTools.size() == 0)
                            {
                                NpCandidateEmployerToolsId npCandidateEmployerToolsId = new NpCandidateEmployerToolsId();
                                npCandidateEmployerToolsId.setCandidateEmployerId(Integer.parseInt(employID));
                                npCandidateEmployerToolsId.setToolID(npTools.get(0).getId());
                                NpCandidateEmployerTools npCandidateEmployerTool = new NpCandidateEmployerTools();
                                npCandidateEmployerTool.setNpCandidateEmployerToolsId(npCandidateEmployerToolsId);
                                // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                                hibernateTemplate.saveOrUpdate(npCandidateEmployerTool);
                            }
                        }
                    }
                }
                else
                {
                    NpTool npTool = new NpToolImpl();
                    NpDomain npDomainType = hibernateTemplate.get(NpDomainImpl.class, Integer.parseInt(domainType));
                    npTool.setNpDomain((NpDomainImpl) npDomainType);
                    npTool.setToolName(toolItems[0]);
                    npTool.setStatus(true);
                    npTool.setValid("Valid");
                    Serializable toolID = hibernateTemplate.save(npTool);
                    // creating new candidate skill
                    NpCandidateTools npCandidateTool = new NpCandidateTools();
                    NpCandidateToolsId npCandidateToolsId = new NpCandidateToolsId();
                    npCandidateTool.setNpTools((NpToolImpl) npTool);
                    npCandidateTool.setNpExpertLevel(npExpertLevels.get(0));
                    npCandidateTool.setYearsOfExperience(Integer.parseInt(toolItems[1]));
                    npCandidateTool.setFuturePreferedTool(0);
                    npCandidateToolsId.setToolId((Integer) toolID);
                    npCandidateToolsId.setCandidateId(candidateId);
                    npCandidateTool.setId(npCandidateToolsId);
                    // hibernateTemplate.saveOrUpdate(npCandidateSkillsId);
                    hibernateTemplate.saveOrUpdate(npCandidateTool);

                    NpCandidateEmployerToolsId npCandidateEmployerToolsId = new NpCandidateEmployerToolsId();
                    npCandidateEmployerToolsId.setCandidateEmployerId(Integer.parseInt(employID));
                    npCandidateEmployerToolsId.setToolID((Integer) toolID);
                    NpCandidateEmployerTools npCandidateEmployerTool = new NpCandidateEmployerTools();
                    npCandidateEmployerTool.setNpCandidateEmployerToolsId(npCandidateEmployerToolsId);
                    // hibernateTemplate.saveOrUpdate(npCandidateEmployerSkillsId);
                    hibernateTemplate.saveOrUpdate(npCandidateEmployerTool);
                }
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getEmployerNames()
    {
        List<String> npEmployerNames = (List<String>) hibernateTemplate.find("select name from NpEmployerImpl");
        return npEmployerNames;
    }

    @Override
    public void deleteCandidateTechnicalDetails(NpCandidate npCandidate)
    {
        // TODO Auto-generated method stub
        List<NpCandidateSkills> npCandidateSkills = getNpCandidateSkills(npCandidate);
        List<NpCandidateEmpRoles> npCandidateEmpRoles = getNpCandidateRoles(npCandidate);
        List<NpCandidateTools> npCandidateTools = getNpCandidateTools(npCandidate);

        for (NpCandidateSkills npCandidateSkill : npCandidateSkills)
        {
            hibernateTemplate.delete(npCandidateSkill);
        }

        for (NpCandidateEmpRoles npCandidateEmpRole : npCandidateEmpRoles)
        {
            hibernateTemplate.delete(npCandidateEmpRole);
        }
        for (NpCandidateTools npCandidateTool : npCandidateTools)
        {
            hibernateTemplate.delete(npCandidateTool);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateSkills> getNpCandidateSkills(NpCandidate npCandidate)
    {

        List<NpCandidateSkills> futureSkillList = (List<NpCandidateSkills>) hibernateTemplate
                .find("from NpCandidateSkills where candidateId=? ",
                        npCandidate.getId());

        return futureSkillList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateTools> getNpCandidateTools(NpCandidate npCandidate)
    {

        List<NpCandidateTools> futureToolList = (List<NpCandidateTools>) hibernateTemplate
                .find("from NpCandidateTools where candidateId=? ", npCandidate.getId());

        return futureToolList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpCandidateEmpRoles> getNpCandidateRoles(NpCandidate npCandidate)
    {

        List<NpCandidateEmpRoles> futureRoleList = (List<NpCandidateEmpRoles>) hibernateTemplate
                .find("from NpCandidateEmpRoles where candidateId=? ", npCandidate.getId());

        return futureRoleList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListOfSkills()
    {
        List<String> lisOfSkills = (List<String>) hibernateTemplate.find("select skillName from NpSkillImpl where status=1");
        return lisOfSkills;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListOfRoles()
    {
        List<String> listOfRoles = (List<String>) hibernateTemplate.find("select roleName from NpEmploymentRoleImpl where status=1");
        return listOfRoles;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getListOfTools()
    {
        List<String> lisOfTools = (List<String>) hibernateTemplate.find("select toolName from NpToolImpl where status=1");
        return lisOfTools;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpSkill getSkillBySkillName(String skillName)
    {
        List<NpSkill> npSkills = (List<NpSkill>) hibernateTemplate.find("from NpSkillImpl where skillName=?", skillName);
        if (npSkills.size() > 0)
        {
            return npSkills.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpTool getToolByToolName(String toolName)
    {
        List<NpTool> npTools = (List<NpTool>) hibernateTemplate.find("from NpToolImpl where toolName=?", toolName);
        if (npTools.size() > 0)
        {
            return npTools.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public NpEmploymentRole getRoleByRoleName(String roleName)
    {
        List<NpEmploymentRole> employmentRoles = (List<NpEmploymentRole>) hibernateTemplate.find("from NpEmploymentRoleImpl where roleName=?", roleName);
        if (employmentRoles.size() > 0)
        {
            return employmentRoles.get(0);
        }
        return null;
    }
}
