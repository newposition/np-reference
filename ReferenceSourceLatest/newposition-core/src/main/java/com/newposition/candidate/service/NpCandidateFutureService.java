package com.newposition.candidate.service;

import java.util.List;
import java.util.Map;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateFutureEmployer;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.forms.NpCandidateFutureForm;

public interface NpCandidateFutureService
{

    public List<String> saveSkills(List<String> skillNames, NpCandidate npCandidate);

    public boolean removeSkill(String skillName, NpCandidate npCandidate);

    public List<String> saveRoles(List<String> roleNames, NpCandidate npCandidate);

    public boolean removeRole(String roleName, NpCandidate npCandidate);

    public boolean removeTool(String toolName, NpCandidate npCandidate);

    public List<String> addTools(List<String> toolNames, NpCandidate npCandidate);

    public List<String> addCountries(List<String> countryNames, NpCandidate npCandidate);

    public boolean removeCountry(String countryName, NpCandidate npCandidate);

    public String saveInterests(String name, int canId);

    public List<String> getListOfCountries();

    public List<String> saveFutureEmployer(List<String> companies, NpCandidate npCandidate);

    public void removeFutureEmployer(String company, NpCandidate npCandidate);

    public List<String> saveExcludeEmployer(List<String> companies, NpCandidate npCandidate);

    public Map<String, List<NpCandidateFutureEmployer>> getFutureEmployer(NpCandidate npCandidate);

    public void changeFuturePreferenceSkill(String name, int futurePreference, NpCandidate npCandidate);

    public void changeFuturePreferenceRole(String name, int futurePreference, NpCandidate npCandidate);

    public void changeFuturePreferenceTools(String name, int futurePreference, NpCandidate npCandidate);

    public List<NpCandidateSkills> getPreferedSkills(NpCandidate npCandidate);

    public List<String> getPreferCountries(NpCandidate npCandidate);

    public List<NpCandidateEmpRoles> getPreferedRoles(NpCandidate npCandidate);

    public List<NpCandidateTools> getPreferedTools(NpCandidate npCandidate);

    public NpCandidateFutureForm saveFutureDetails(NpCandidateFutureForm npCandidateFutureForm);

    public List<String> getListOfCountriesOnly();
}
