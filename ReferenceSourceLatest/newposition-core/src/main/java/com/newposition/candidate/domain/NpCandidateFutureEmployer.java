package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.newposition.common.domain.NpEmployerImpl;

/**
 * NpCandidateFutureEmployer generated by hbm2java
 */
@Entity
@Table(name = "np_candidate_prefercompany", catalog = "npdbs")
public class NpCandidateFutureEmployer implements java.io.Serializable
{

    private static final long serialVersionUID = 1L;

    private NpCandidateFutureEmployerId id;

    private NpCandidateImpl npCandidate;

    private NpEmployerImpl npEmployer;

    private Integer futurePreferedCompany;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "candidateId", column = @Column(name = "CandidateID", nullable = false)),
            @AttributeOverride(name = "employerId", column = @Column(name = "employerId", nullable = false))})
    public NpCandidateFutureEmployerId getId()
    {
        return this.id;
    }

    public void setId(NpCandidateFutureEmployerId id)
    {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CandidateID", nullable = false, insertable = false, updatable = false)
    public NpCandidateImpl getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidateImpl npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employerId", nullable = false, insertable = false, updatable = false)
    public NpEmployerImpl getNpEmployer()
    {
        return npEmployer;
    }

    public void setNpEmployer(NpEmployerImpl npEmployer)
    {
        this.npEmployer = npEmployer;
    }

    @Column(name = "futurePreferenceCompany")
    public Integer getFuturePreferedCompany()
    {
        return futurePreferedCompany;
    }

    public void setFuturePreferedCompany(Integer futurePreferedCompany)
    {
        this.futurePreferedCompany = futurePreferedCompany;
    }
}
