package com.newposition.candidate.service;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidatePreferenceForm;

public interface NpCandidateMyaccountService
{
    public void updateCandidatePreferences(NpCandidatePreferenceForm npCandidatePreferenceForm, NpCandidate npCandidate);

    public NpCandidatePreference getCandidatePreferences(NpCandidate npCandidate);

    public void updateCandidateDocument(NpCandidateDocument candidateDocument);

    public void saveCandidatePreference(NpCandidatePreference candidatePreference);

    public void updateCandidateEmployer(NpCandidateEmployer candidateEmployer);

    public void updateCandidate(NpCandidateImpl npCandidate);

    public boolean checkEmailExists(String primaryEmail);

    public List<NpCandidateEmployer> getNpCandidateEmployer(int candidateId);
}
