package com.newposition.candidate.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.Table;

@Entity
@Table(name = "np_candidate_region", catalog = "npdbs")
public class NpCandidateRegion implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private NpCandidateRegionId id;
    private NpCandidateImpl npCandidate;
    private Region region;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "npRegionId", column = @Column(name = "NpRegionId", nullable = false)),
            @AttributeOverride(name = "candidateID", column = @Column(name = "CandidateID", nullable = false))})
    public NpCandidateRegionId getId()
    {
        return this.id;
    }

    public void setId(NpCandidateRegionId id)
    {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NpRegionId", nullable = false, insertable = false, updatable = false)
    public Region getRegion()
    {
        return region;
    }

    public void setRegion(Region region)
    {
        this.region = region;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CandidateID", nullable = false, insertable = false, updatable = false)
    public NpCandidateImpl getNpCandidate()
    {
        return this.npCandidate;
    }

    public void setNpCandidate(NpCandidateImpl npCandidate)
    {
        this.npCandidate = npCandidate;
    }
}


