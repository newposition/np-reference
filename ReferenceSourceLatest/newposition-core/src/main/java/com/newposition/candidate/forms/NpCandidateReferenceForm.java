package com.newposition.candidate.forms;

import com.newposition.candidate.domain.NpCandidate;

public class NpCandidateReferenceForm implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    private NpCandidate npCandidate;

    private String firstName;

    private String surName;

    private String email;

    private String relationship;

    private String dateTo;

    private String dateFrom;

    private String coveringNote;

    public String getCoveringNote()
    {
        return coveringNote;
    }

    public void setCoveringNote(String coveringNote)
    {
        this.coveringNote = coveringNote;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public NpCandidate getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidate npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getSurName()
    {
        return surName;
    }

    public void setSurName(String surName)
    {
        this.surName = surName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(String dateTo)
    {
        this.dateTo = dateTo;
    }

    public String getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public String getRelationship()
    {
        return relationship;
    }

    public void setRelationship(String relationship)
    {
        this.relationship = relationship;
    }
}
