/**
 *
 */
package com.newposition.candidate.forms.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.newposition.candidate.forms.QuickRegistrationForm;

/*
 * @author ravi
 *
 *
 */
public class QuickRegistrationFormValidator implements Validator
{
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return clazz.equals(QuickRegistrationForm.class);
    }

    @SuppressWarnings("unused")
    @Override
    public void validate(Object target, Errors errors)
    {

        QuickRegistrationForm form = (QuickRegistrationForm) target;

        ValidationUtils.rejectIfEmpty(errors, "password", "password.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "candidate.primaryEmail", "email.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "candidate.firstName", "firstName.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "candidate.lastName", "lastName.required");

        String emailPattern = "^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\\.[a-zA-Z]{2,}$";

        String email = form.getCandidate().getPrimaryEmail();

        boolean isValidEmail = true;

        String confirmEmail = form.getConfirmPrimaryEmailAddress();

        boolean isMatchTel;

        if (email.length() > 0 && email.length() <= 50)
        {

            // Create a Pattern object
            Pattern pattern = Pattern.compile(emailPattern);

            // Now create matcher object.
            Matcher match = pattern.matcher(email.trim());

            if (!(isMatchTel = match.find()))
            {

                isValidEmail = false;
                errors.rejectValue("candidate.primaryEmail", "email.format");
            }
            else
            {

                if (email.contains("..") || email.contains("--") || email.contains("__"))
                {
                    isValidEmail = false;
                    errors.rejectValue("candidate.primaryEmail", "email.consecutive.periods");
                }
            }
        }
        else
        {

            isValidEmail = false;
        }

        if (isValidEmail && form.getCandidate().getPrimaryEmail() != null
                && !form.getCandidate().getPrimaryEmail().equals(form.getConfirmPrimaryEmailAddress()))
        {
            errors.rejectValue("confirmPrimaryEmailAddress", "confirm.email.nomatch");
        }

        if (form.getAgree() == null)
        {
            errors.rejectValue("agree", "termsandconditions.required");
        }

        boolean isValidPwd = true;
        if (StringUtils.isNotEmpty(form.getPassword()))
        {

            if (StringUtils.length(form.getPassword()) < 7)
            {
                isValidPwd = false;
                errors.rejectValue("password", "confirmpassword.required");
            }
            else if (!isValidPassword(form.getPassword()))
            {
                isValidPwd = false;
                errors.rejectValue("password", "confirmpassword.required");
            }
        }
        else
        {
            isValidPwd = false;
        }

        if (isValidPwd && (form.getConfirmPassword() == null || form.getConfirmPassword().length() == 0))
        {
            errors.rejectValue("confirmPassword", "confirmpassword.required");
        }
        else if (isValidPwd && form.getConfirmPassword().length() > 0)
        {

            if (!(form.getPassword()).equals(form.getConfirmPassword()))
            {
                errors.rejectValue("password", "confirm.pwd.nomatch");
            }
        }
        /*if (form.getCandidate().getPhone().length() != 0) {
			if (form.getCandidate().getPhone().matches("^[+]?\\d+(\\d+)?$")) {

				if (form.getCandidate().getPhone().length() < 10
						|| (form.getCandidate().getPhone().length() > 16)) {
					System.out.println("size error");
					errors.rejectValue("candidate.phone", "error.telephoneno.length");
				}
			} else {
				System.out.println("lenth issue in mobile");
				errors.rejectValue("candidate.phone", "telephone.required");
			}
		}*/
    }

    public boolean isValidPassword(String password)
    {
        Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$");
        Pattern pattern1 = Pattern.compile(".*[-~\\!\\@\\#\\$\\%\\^\\&\\.\\,\\*\\(\\)\\_\\+\\=\\{\\}\\|\\:\\\\'\\[\\]\\<\\>\\?\\/\\'\\^].*");
        Matcher match = pattern.matcher(password);
        Matcher match1 = pattern1.matcher(password);

        //boolean c1 = true, c2 = true;
        // LOG.info("@@  "+match.find()+" -- "+match1.find());
        boolean validCon_1 = match.find();
        boolean spclChar = match1.find();
        boolean validCon_2 = (spclChar && password.matches(".*[A-Z].*") && password.matches(".*[a-z].*"));
        // LOG.info("@@  "+fst+" -- "+scnd);
        if (validCon_1 && spclChar)
        {
            return false;
        }
        else if (validCon_1)
        {
            return true;
        }
        else if (validCon_2)
        {

            return true;
        }
        else
        {
            return false;
        }
    }
}
