/**
 *
 */
package com.newposition.candidate.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.newposition.common.domain.NpUserImpl;

/*
 * @author ravi
 *
 *
 */
@Entity
@Table(name = "np_candidate", catalog = "npdbs")
@PrimaryKeyJoinColumn(name = "ID")
public class NpCandidateImpl extends NpUserImpl implements NpCandidate
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationID")
    private NpLocations npLocations;

    @Column(name = "AvailableStatus", length = 20)
    private String availableStatus;

    @Column(name = "RegistrationStatus", length = 20)
    private String registrationStatus;

    @Column(name = "EligibilityCheckEU", length = 20)
    private String eligibilityCheckEu;

    @Column(name = "PhotoVisibility", length = 1)
    private Character photoVisibility;

    @Column(name = "personalStatement", length = 500)
    private String personalStatement;

    @Column(name = "CreatedBy", nullable = false, length = 50)
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreatedDate", nullable = false, length = 10)
    private Date createdDate;

    @Column(name = "ModifiedBy", nullable = false, length = 20)
    private String modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "ModifiedDate", nullable = false, length = 10)
    private Date modifiedDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateSkills> npCandidateSkills = new HashSet<NpCandidateSkills>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateTools> npCandidateTools = new HashSet<NpCandidateTools>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateEmpRoles> npCandidateEmpRoles = new HashSet<NpCandidateEmpRoles>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateEmployerImpl> npCandidateEmployers = new HashSet<NpCandidateEmployerImpl>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateReference> npCandidateReferences = new HashSet<NpCandidateReference>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateEducation> npCandidateEducations = new HashSet<NpCandidateEducation>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateCertificate> npCandidateCertificates = new HashSet<NpCandidateCertificate>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpPreferredLocation> npPreferredLocations = new HashSet<NpPreferredLocation>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateInterests> npCandidateInterests = new HashSet<NpCandidateInterests>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateLanguages> npCandidateLanguages = new HashSet<NpCandidateLanguages>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateFutureEmployer> npCandidateFutureEmployers = new HashSet<NpCandidateFutureEmployer>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateEligibility> npCandidateEligibilities = new HashSet<NpCandidateEligibility>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "npCandidate")
    private Set<NpCandidateRegion> npCandidateRegions = new HashSet<NpCandidateRegion>(0);

    @OneToOne(mappedBy = "npCandidate", fetch = FetchType.LAZY)
    private NpCandidateDocument npCandidateDocument;

    @OneToOne(mappedBy = "npCandidate", fetch = FetchType.LAZY)
    private NpCandidatePreference npCandidatePreference;

    @Transient
    private String jobTitle;

    @Transient
    private String currentCompany;

    @Override
    public String getAvailableStatus()
    {
        return this.availableStatus;
    }

    @Override
    public void setAvailableStatus(String availableStatus)
    {
        this.availableStatus = availableStatus;
    }

    @Override
    public NpLocations getNpLocations()
    {
        return this.npLocations;
    }

    @Override
    public void setNpLocations(NpLocations npLocations)
    {
        this.npLocations = npLocations;
    }

    @Override
    public String getRegistrationStatus()
    {
        return this.registrationStatus;
    }

    @Override
    public void setRegistrationStatus(String registrationStatus)
    {
        this.registrationStatus = registrationStatus;
    }

    @Override
    public String getEligibilityCheckEu()
    {
        return this.eligibilityCheckEu;
    }

    @Override
    public void setEligibilityCheckEu(String eligibilityCheckEu)
    {
        this.eligibilityCheckEu = eligibilityCheckEu;
    }

    @Override
    public Character getPhotoVisibility()
    {
        return this.photoVisibility;
    }

    @Override
    public void setPhotoVisibility(Character photoVisibility)
    {
        this.photoVisibility = photoVisibility;
    }

    @Override
    public String getJobTitle()
    {
        return jobTitle;
    }

    @Override
    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    @Override
    public String getCurrentCompany()
    {
        return currentCompany;
    }

    @Override
    public void setCurrentCompany(String currentCompany)
    {
        this.currentCompany = currentCompany;
    }

    @Override
    public Set<NpCandidateSkills> getNpCandidateSkills()
    {
        return this.npCandidateSkills;
    }

    @Override
    public void setNpCandidateSkills(Set<NpCandidateSkills> npCandidateSkills)
    {
        this.npCandidateSkills = npCandidateSkills;
    }

    @Override
    public Set<NpCandidateTools> getNpCandidateTools()
    {
        return this.npCandidateTools;
    }

    @Override
    public void setNpCandidateTools(Set<NpCandidateTools> npCandidateTools)
    {
        this.npCandidateTools = npCandidateTools;
    }

    @Override
    public Set<NpCandidateEmpRoles> getNpCandidateEmpRoles()
    {
        return this.npCandidateEmpRoles;
    }

    @Override
    public void setNpCandidateEmpRoles(Set<NpCandidateEmpRoles> npCandidateEmpRoles)
    {
        this.npCandidateEmpRoles = npCandidateEmpRoles;
    }

    @Override
    public String getCreatedBy()
    {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    @Override
    public String getModifiedBy()
    {
        return this.modifiedBy;
    }

    @Override
    public void setModifiedBy(String modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    @Override
    public Set<NpCandidateEmployerImpl> getNpCandidateEmployers()
    {
        return npCandidateEmployers;
    }

    @Override
    public void setNpCandidateEmployers(Set<NpCandidateEmployerImpl> npCandidateEmployers)
    {
        this.npCandidateEmployers = npCandidateEmployers;
    }

    @Override
    public String getPersonalStatement()
    {
        return personalStatement;
    }

    @Override
    public void setPersonalStatement(String personalStatement)
    {
        this.personalStatement = personalStatement;
    }

    @Override
    public Set<NpCandidateReference> getNpCandidateReferences()
    {
        return npCandidateReferences;
    }

    @Override
    public void setNpCandidateReferences(Set<NpCandidateReference> npCandidateReferences)
    {
        this.npCandidateReferences = npCandidateReferences;
    }

    @Override
    public Set<NpCandidateEducation> getNpCandidateEducations()
    {
        return npCandidateEducations;
    }

    @Override
    public void setNpCandidateEducations(Set<NpCandidateEducation> npCandidateEducations)
    {
        this.npCandidateEducations = npCandidateEducations;
    }

    @Override
    public Set<NpCandidateCertificate> getNpCandidateCertificates()
    {
        return npCandidateCertificates;
    }

    @Override
    public void setNpCandidateCertificates(Set<NpCandidateCertificate> npCandidateCertificates)
    {
        this.npCandidateCertificates = npCandidateCertificates;
    }

    @Override
    public Set<NpPreferredLocation> getNpPreferredLocations()
    {
        return npPreferredLocations;
    }

    @Override
    public void setNpPreferredLocations(Set<NpPreferredLocation> npPreferredLocations)
    {
        this.npPreferredLocations = npPreferredLocations;
    }

    @Override
    public Set<NpCandidateInterests> getNpCandidateInterests()
    {
        return npCandidateInterests;
    }

    @Override
    public void setNpCandidateInterests(Set<NpCandidateInterests> npCandidateInterests)
    {
        this.npCandidateInterests = npCandidateInterests;
    }

    @Override
    public Set<NpCandidateLanguages> getNpCandidateLanguages()
    {
        return npCandidateLanguages;
    }

    @Override
    public void setNpCandidateLanguages(Set<NpCandidateLanguages> npCandidateLanguages)
    {
        this.npCandidateLanguages = npCandidateLanguages;
    }

    @Override
    public Set<NpCandidateFutureEmployer> getNpCandidateFutureEmployers()
    {
        return npCandidateFutureEmployers;
    }

    @Override
    public void setNpCandidateFutureEmployers(Set<NpCandidateFutureEmployer> npCandidateFutureEmployers)
    {
        this.npCandidateFutureEmployers = npCandidateFutureEmployers;
    }

    public Set<NpCandidateEligibility> getNpCandidateEligibilities()
    {
        return npCandidateEligibilities;
    }

    @Override
    public void setNpCandidateEligibilities(Set<NpCandidateEligibility> npCandidateEligibilities)
    {
        this.npCandidateEligibilities = npCandidateEligibilities;
    }

    @Override
    public Set<NpCandidateRegion> getNpCandidateRegions()
    {
        return npCandidateRegions;
    }

    @Override
    public void setNpCandidateRegions(Set<NpCandidateRegion> npCandidateRegions)
    {
        this.npCandidateRegions = npCandidateRegions;
    }

    @Override
    public NpCandidateDocument getNpCandidateDocument()
    {
        return npCandidateDocument;
    }

    @Override
    public void setNpCandidateDocument(NpCandidateDocument npCandidateDocument)
    {
        this.npCandidateDocument = npCandidateDocument;
    }

    @Override
    public NpCandidatePreference getNpCandidatePreference()
    {
        return npCandidatePreference;
    }

    @Override
    public void setNpCandidatePreference(NpCandidatePreference npCandidatePreference)
    {
        this.npCandidatePreference = npCandidatePreference;
    }

    @Override
    public void setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;
    }
}
