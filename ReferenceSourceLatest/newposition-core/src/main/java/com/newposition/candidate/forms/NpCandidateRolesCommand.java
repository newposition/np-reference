/**
 *
 */
package com.newposition.candidate.forms;

import java.io.Serializable;
import java.util.List;

/*
 * @author ravi
 *
 *
 */
public class NpCandidateRolesCommand implements Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<NpCandidateRolesForm> roles;

    public List<NpCandidateRolesForm> getRoles()
    {
        return roles;
    }

    public void setRoles(List<NpCandidateRolesForm> roles)
    {
        this.roles = roles;
    }
}
