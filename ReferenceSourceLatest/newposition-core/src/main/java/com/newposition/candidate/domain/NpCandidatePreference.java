package com.newposition.candidate.domain;

/**
 * author :Sreenivas Arumilli
 */

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "np_candidate_preference", catalog = "npdbs")
public class NpCandidatePreference implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @Column(name = "type", nullable = true)
    private String type;

    @Column(name = "compensation", nullable = true)
    private Double compensation;

    @Column(name = "bonus", nullable = true)
    private Double bonus;

    @Column(name = "dayRate", nullable = true)
    private Double dayRate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "candidateID", nullable = false)
    private NpCandidateImpl npCandidate;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isSummaryReports;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isSendOpportunities;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isSendReports;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isJobAppearance;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isPermissionShortlist;

    @Column(columnDefinition = "TINYINT", length = 1)
    private boolean isAcceptPhoto;

    public NpCandidatePreference()
    {
        // TODO Auto-generated constructor stub
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public boolean isSummaryReports()
    {
        return isSummaryReports;
    }

    public void setSummaryReports(boolean isSummaryReports)
    {
        this.isSummaryReports = isSummaryReports;
    }

    @Column(columnDefinition = "TINYINT", length = 1)
    public boolean isSendOpportunities()
    {
        return isSendOpportunities;
    }

    public void setSendOpportunities(boolean isSendOpportunities)
    {
        this.isSendOpportunities = isSendOpportunities;
    }

    public boolean isSendReports()
    {
        return isSendReports;
    }

    public void setSendReports(boolean isSendReports)
    {
        this.isSendReports = isSendReports;
    }

    public boolean isJobAppearance()
    {
        return isJobAppearance;
    }

    public boolean isAcceptPhoto()
    {
        return isAcceptPhoto;
    }

    public void setJobAppearance(boolean isJobAppearance)
    {
        this.isJobAppearance = isJobAppearance;
    }

    public boolean isPermissionShortlist()
    {
        return isPermissionShortlist;
    }

    public void setPermissionShortlist(boolean isPermissionShortlist)
    {
        this.isPermissionShortlist = isPermissionShortlist;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Double getCompensation()
    {
        return compensation;
    }

    public void setCompensation(Double compensation)
    {
        this.compensation = compensation;
    }

    public Double getBonus()
    {
        return bonus;
    }

    public void setBonus(Double bonus)
    {
        this.bonus = bonus;
    }

    public Double getDayRate()
    {
        return dayRate;
    }

    public void setDayRate(Double dayRate)
    {
        this.dayRate = dayRate;
    }

    public NpCandidateImpl getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidateImpl npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    public void setAcceptPhoto(boolean isAcceptPhoto)
    {
        this.isAcceptPhoto = isAcceptPhoto;
    }
}
