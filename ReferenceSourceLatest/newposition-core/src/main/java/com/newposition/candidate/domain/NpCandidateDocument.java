package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * NpRoles generated by hbm2java
 */
@Entity
@Table(name = "np_candidate_document", catalog = "npdbs")
public class NpCandidateDocument implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "cv", unique = true, nullable = true)
    private String cv;

    @Column(name = "photo", unique = true, nullable = true)
    private byte[] photo;

    @Column(name = "coverletter", unique = true, nullable = true)
    private String coverLetter;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "candidateId")
    private NpCandidateImpl npCandidate;

    public NpCandidateDocument()
    {
    }

    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public NpCandidateImpl getNpCandidate()
    {
        return npCandidate;
    }

    public void setNpCandidate(NpCandidateImpl npCandidate)
    {
        this.npCandidate = npCandidate;
    }

    public String getCv()
    {
        return cv;
    }

    public void setCv(String cv)
    {
        this.cv = cv;
    }

    public byte[] getPhoto()
    {
        return photo;
    }

    public void setPhoto(byte[] photo)
    {
        this.photo = photo;
    }

    public String getCoverLetter()
    {
        return coverLetter;
    }

    public void setCoverLetter(String coverLetter)
    {
        this.coverLetter = coverLetter;
    }
}
