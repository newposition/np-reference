package com.newposition.candidate.service;

import java.util.List;

import com.newposition.candidate.forms.NpCandidateEducationForm;

public interface NpCandidateEducationRegisterService
{

    public void saveEducationHistory(NpCandidateEducationForm educationForm, int candidateId);

    public void saveQualification(NpCandidateEducationForm educationForm, int candidateId);

    public void saveCertifications(NpCandidateEducationForm educationForm, int candidateId);

    public List<String> getInstituteNames();

    public List<NpCandidateEducationForm> getEducationHistories(int candidateId);

    public List<NpCandidateEducationForm> getCandidateCertificates(int candidateId);

    public List<String> getLanguages();

    public List<String> saveLanguages(List<String> languageNames, List<String> languageFluencies, int candidateId);

    public void removeEducation(int educationId, int candidateId);

    public void removeCertificate(int certificateId, int candidateId);

    public List<String> saveInterests(List<String> interestNames, int candidateId);

    public List<NpCandidateEducationForm> getQualifications(int candidateId);

    public void removeQualification(int qualificationId, int candidateId);

    public void removeLanguage(String languageName, int candidateId);

    public void removeInterest(String interestName, int candidateId);

    public List<String> getCandidateLanguageNames(int candidateId);

    public List<String> getCandidateInterestNames(int candidateId);
}
