package com.newposition.candidate.domain;

// Generated Apr 5, 2015 12:37:03 PM by Hibernate Tools 4.0.0

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * NpCandidateFutureEmployerId
 */
@Embeddable
public class NpCandidateFutureEmployerId implements java.io.Serializable
{

    private static final long serialVersionUID = 1L;

    private int employerId;
    private int candidateId;

    @Column(name = "EmployerID", nullable = false)
    public int getEmployerId()
    {
        return this.employerId;
    }

    public void setEmployerId(int employerId)
    {
        this.employerId = employerId;
    }

    @Column(name = "CandidateID", nullable = false)
    public int getCandidateId()
    {
        return this.candidateId;
    }

    public void setCandidateId(int candidateId)
    {
        this.candidateId = candidateId;
    }

    public boolean equals(Object other)
    {
        if ((this == other))
        {
            return true;
        }
        if ((other == null))
        {
            return false;
        }
        if (!(other instanceof NpCandidateFutureEmployerId))
        {
            return false;
        }
        NpCandidateFutureEmployerId castOther = (NpCandidateFutureEmployerId) other;

        return (this.getEmployerId() == castOther.getEmployerId())
                && (this.getCandidateId() == castOther.getCandidateId());
    }

    public int hashCode()
    {
        int result = 17;

        result = 37 * result + this.getEmployerId();
        result = 37 * result + this.getCandidateId();

        return result;
    }
}
