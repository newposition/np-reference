package com.newposition.candidate.forms;

public class NpCandidatePreferenceForm
{

    private String primaryEmail;
    private String secondaryEmail;
    private String tertiaryEmail;
    private boolean summaryReports;
    private boolean sendOpportunities;
    private boolean sendReports;
    private boolean jobAppearance;
    private boolean permissionShortlist;
    private boolean acceptPhoto;

    public String getPrimaryEmail()
    {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail)
    {
        this.primaryEmail = primaryEmail;
    }

    public String getSecondaryEmail()
    {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail)
    {
        this.secondaryEmail = secondaryEmail;
    }

    public String getTertiaryEmail()
    {
        return tertiaryEmail;
    }

    public void setTertiaryEmail(String tertiaryEmail)
    {
        this.tertiaryEmail = tertiaryEmail;
    }

    public boolean isSummaryReports()
    {
        return summaryReports;
    }

    public void setSummaryReports(boolean summaryReports)
    {
        this.summaryReports = summaryReports;
    }

    public boolean isSendOpportunities()
    {
        return sendOpportunities;
    }

    public void setSendOpportunities(boolean sendOpportunities)
    {
        this.sendOpportunities = sendOpportunities;
    }

    public boolean isSendReports()
    {
        return sendReports;
    }

    public void setSendReports(boolean sendReports)
    {
        this.sendReports = sendReports;
    }

    public boolean isJobAppearance()
    {
        return jobAppearance;
    }

    public void setJobAppearance(boolean jobAppearance)
    {
        this.jobAppearance = jobAppearance;
    }

    public boolean isPermissionShortlist()
    {
        return permissionShortlist;
    }

    public void setPermissionShortlist(boolean permissionShortlist)
    {
        this.permissionShortlist = permissionShortlist;
    }

    public boolean isAcceptPhoto()
    {
        return acceptPhoto;
    }

    public void setAcceptPhoto(boolean acceptPhoto)
    {
        this.acceptPhoto = acceptPhoto;
    }
}
