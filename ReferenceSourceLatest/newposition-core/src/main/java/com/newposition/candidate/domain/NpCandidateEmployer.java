package com.newposition.candidate.domain;

import java.util.Date;
import java.util.Set;

import com.newposition.common.domain.NpEmployer;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.common.domain.NpToolImpl;

public interface NpCandidateEmployer
{

    public Set<NpToolImpl> getNpTools();

    public void setNpTools(Set<NpToolImpl> npTools);

    public Set<NpEmploymentRoleImpl> getNpEmploymentRoles();

    public void setNpEmploymentRoles(Set<NpEmploymentRoleImpl> npEmploymentRoles);

    public Set<NpSkillImpl> getNpSkills();

    public void setNpSkills(Set<NpSkillImpl> npSkills);

    public Date getStartDate();

    public void setStartDate(Date startDate);

    public Date getEnddate();

    public void setEnddate(Date enddate);

    public NpCandidate getNpCandidate();

    public void setNpCandidate(NpCandidate npCandidate);

    public NpEmployer getNpEmployer();

    public void setNpEmployer(NpEmployer npEmployer);

    public String getJobTitle();

    public void setJobTitle(String jobTitle);

    public Integer getSalaryCompensation();

    public void setSalaryCompensation(Integer salaryCompensation);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);

    public String getEmploymentType();

    public void setEmploymentType(String employmentType);

    public String getCorporateTitle();

    public void setCorporateTitle(String corporateTitle);

    public String getAchievements();

    public void setAchievements(String achievements);

    public int getUid();

    public void setUid(int uid);
}
