package com.newposition.candidate.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "np_candidate_employer_skills", catalog = "npdbs")
public class NpCandidateEmployerSkills implements java.io.Serializable
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "candidateEmployerId", column = @Column(name = "CandidateEmployerId", nullable = false)),
            @AttributeOverride(name = "skillId", column = @Column(name = "SkillId", nullable = false))})
    private NpCandidateEmployerSkillsId npCandidateEmployerSkillsId;

    public NpCandidateEmployerSkillsId getNpCandidateEmployerSkillsId()
    {
        return npCandidateEmployerSkillsId;
    }

    public void setNpCandidateEmployerSkillsId(NpCandidateEmployerSkillsId npCandidateEmployerSkillsId)
    {
        this.npCandidateEmployerSkillsId = npCandidateEmployerSkillsId;
    }
}