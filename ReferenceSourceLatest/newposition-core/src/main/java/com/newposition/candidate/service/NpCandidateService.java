package com.newposition.candidate.service;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.candidate.forms.QuickRegistrationForm;
import com.newposition.common.domain.NpUser;

public interface NpCandidateService
{

    public NpCandidate quickRegister(NpCandidate candidate, String password);

    public NpCandidate candidateRegister(NpCandidate candidate, String password);

    public NpCandidate findByID(Integer id);

    public List<NpCandidateReference> getCandidateReferenceList(String referEmail);

    public void sendRegistrationSucessMail(QuickRegistrationForm quickRegistrationForm, String[] codes, String subject);

    public NpCandidateReference getCurrentCandidateReference(String primaryEmail, Integer id);

    public void varifyReference(NpCandidateReference npCandidateReference);
}
