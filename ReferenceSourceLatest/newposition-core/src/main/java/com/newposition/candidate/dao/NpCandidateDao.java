package com.newposition.candidate.dao;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateReference;
import com.newposition.common.dao.GenericDao;
import com.newposition.common.domain.NpUser;

public interface NpCandidateDao extends GenericDao<NpCandidate, Integer>
{

    List<NpCandidateReference> getCandidateReferenceList(String referEmail);

    NpCandidateReference getCurrentCandidateReference(String primaryEmail, Integer candidateId);

    void varifyReference(NpCandidateReference npCandidateReference);
}
