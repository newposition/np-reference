package com.newposition.candidate.dao;

import java.util.List;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateEducationForm;

public interface NpCandidateEducationRegisterDao
{

    public void saveEducationHistory(NpCandidateEducationForm educationForm, NpCandidate npCandidate);

    public void saveQualification(NpCandidateEducationForm educationForm, NpCandidate npCandidate);

    public void saveCertifications(NpCandidateEducationForm educationForm, NpCandidate npCandidate);

    public List<String> getInstituteNames();

    public List<NpCandidateEducationForm> getEducationHistories(NpCandidate npCandidate);

    public List<NpCandidateEducationForm> getCandidateCertificates(NpCandidate npCandidate);

    public List<String> getLanguages();

    public List<String> saveLanguages(List<String> languageNames, List<String> languageFluencies, NpCandidate npCandidate);

    public void removeEducation(int educationId, int candidateId);

    public void removeCertificate(int certificateId, int candidateId);

    public List<String> saveInterests(List<String> interestNames, NpCandidate npCandidate);

    public List<NpCandidateEducationForm> getQualifications(NpCandidate npCandidate);

    public void removeQualification(int qualificationId, int candidateId);

    public void removeLanguage(String languageName, int candidateId);

    public void removeInterest(String interestName, int candidateId);

    public List<String> getCandidateLanguageNames(NpCandidate npCandidate);

    public List<String> getCandidateInterestNames(NpCandidate npCandidate);
}
