package com.newposition.infra.mail;

import java.util.List;

import org.thymeleaf.context.Context;

import com.newposition.common.domain.NpMailConfigProp;

public interface NpMailService
{

    public void sendMail(String information, String toAddress, Context context);

    public NpMailConfigProp getNpMailConfigProp();

    public List<String> getNpMailPropVariables();

    public NpMailConfigProp saveMailProp(String variable, String value);
}
