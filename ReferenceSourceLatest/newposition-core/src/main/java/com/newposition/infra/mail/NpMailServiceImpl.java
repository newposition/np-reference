/**
 *
 */
package com.newposition.infra.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.newposition.common.dao.NpMailDao;
import com.newposition.common.domain.JavaMailProperties;
import com.newposition.common.domain.NpMailConfigProp;

/*
 * @author ravi
 *
 *
 */
public class NpMailServiceImpl implements NpMailService
{

    //SMTP Properties declaration

    /*private Properties javaMailProperties;

    private String npMailUserName;

    private String npMailPassword;*/
    private NpMailDao npMailDao;

    private TemplateEngine templateEngine;

    public TemplateEngine getTemplateEngine()
    {
        return templateEngine;
    }

    public NpMailDao getNpMailDao()
    {
        return npMailDao;
    }

    public void setNpMailDao(NpMailDao npMailDao)
    {
        this.npMailDao = npMailDao;
    }

	

	/*public void setJavaMailProperties(Properties javaMailProperties) {
        this.javaMailProperties = javaMailProperties;
	}

	public void setNpMailUserName(String npMailUserName) {
		this.npMailUserName = npMailUserName;
	}

	public void setNpMailPassword(String npMailPassword) {
		this.npMailPassword = npMailPassword;
	}*/

    @Override
    public void sendMail(String information, String toAddress, Context context)
    {

        final NpMailConfigProp npMailConfigProp = npMailDao.getNpMailConfigProp();

        JavaMailProperties javaMailProperties = npMailConfigProp.getJavaMailProperties();
        System.out.println(" javamail properties host : " + javaMailProperties.getMailSmtpHost());
        Properties javaMailProperty = new Properties();
        javaMailProperty.put("mail.smtp.host", javaMailProperties.getMailSmtpHost());
        javaMailProperty.put("mail.smtp.socketFactory.port", javaMailProperties.getMailSmtpSocketFactoryPort());
        javaMailProperty.put("mail.smtp.socketFactory.class", javaMailProperties.getMailSmtpSocketFactoryClass());
        javaMailProperty.put("mail.smtp.auth", javaMailProperties.getMailSmtpAuth());
        javaMailProperty.put("mail.smtp.port", javaMailProperties.getMailSmtpPort());

        Session session = Session.getDefaultInstance(javaMailProperty, new javax.mail.Authenticator()
        {
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(npMailConfigProp.getNpMailUserName(), npMailConfigProp.getNpMailPassword());
            }
        });

        final String htmlContent = templateEngine.process(context.getVariables().get("template").toString(), context);

        String npMailUserName = npMailConfigProp.getNpMailUserName();
        // compose message
        try
        {
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(npMailUserName));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
            message.setSubject(information);
            message.setContent(htmlContent, "text/html");

            // send message
            Transport.send(message);
        }
        catch (MessagingException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setTemplateEngine(TemplateEngine templateEngine)
    {
        this.templateEngine = templateEngine;
    }

    @Override
    public NpMailConfigProp getNpMailConfigProp()
    {
        final NpMailConfigProp npMailConfigProp = npMailDao.getNpMailConfigProp();
        return npMailConfigProp;
    }

    @Override
    public List<String> getNpMailPropVariables()
    {
        List<String> mailPropVariableList = new ArrayList<String>();
        mailPropVariableList.add("npMailUserName");
        mailPropVariableList.add("npMailPassword");
        mailPropVariableList.add("mailSmtpHost");
        mailPropVariableList.add("mailSmtpSocketFactoryPort");
        mailPropVariableList.add("mailSmtpSocketFactoryClass");
        mailPropVariableList.add("mailSmtpAuth");
        mailPropVariableList.add("mailSmtpPort");
        return mailPropVariableList;
    }

    @Transactional
    @Override
    public NpMailConfigProp saveMailProp(String variable, String value)
    {
        System.out.println("in service");
        NpMailConfigProp npMailConfigProp = npMailDao.getNpMailConfigProp();
        if (variable.equals("npMailUserName"))
        {

            npMailConfigProp.setNpMailUserName(value);
        }
        else if (variable.equals("npMailPassword"))
        {

            npMailConfigProp.setNpMailPassword(value);
        }
        else if (variable.equals("mailSmtpHost"))
        {

            npMailConfigProp.getJavaMailProperties().setMailSmtpHost(value);
        }
        else if (variable.equals("mailSmtpSocketFactoryPort"))
        {

            npMailConfigProp.getJavaMailProperties().setMailSmtpSocketFactoryPort(value);
        }
        else if (variable.equals("mailSmtpSocketFactoryClass"))
        {

            npMailConfigProp.getJavaMailProperties().setMailSmtpSocketFactoryClass(value);
        }
        else if (variable.equals("mailSmtpAuth"))
        {

            npMailConfigProp.getJavaMailProperties().setMailSmtpAuth(value);
        }
        else if (variable.equals("mailSmtpPort"))
        {
            System.out.println("in mailSmtpPort");
            npMailConfigProp.getJavaMailProperties().setMailSmtpPort(value);
        }
        return npMailConfigProp;
    }
}
