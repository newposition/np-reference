package com.newposition.admin.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.RoleManagementDao;
import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;

@Repository
public class RoleManagementDaoImpl implements RoleManagementDao
{

    protected static final Logger LOG = Logger.getLogger(RoleManagementDaoImpl.class);

    private int numberOfReusltsPerPage;

    private HibernateTemplate hibernateTemplate;

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpEmploymentRoles(String sortBy,
                                             String roleNameFilter, String validFilter, String domainTypeIDFiter, String pageNumber)
    {
        List npEmploymentRoleImplResult = new ArrayList();

        String query = "from NpEmploymentRoleImpl";

        if (StringUtils.isNotBlank(roleNameFilter) || StringUtils.isNotBlank(validFilter) || StringUtils.isNotBlank(domainTypeIDFiter))
        {
            query = "from NpEmploymentRoleImpl where ";
            if (StringUtils.isNotBlank(roleNameFilter))
            {
                if (StringUtils.isBlank(validFilter) && StringUtils.isBlank(domainTypeIDFiter))
                {
                    query = query + "roleName='" + roleNameFilter + "'" + " ";
                }
                else
                {
                    query = query + "roleName='" + roleNameFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(domainTypeIDFiter))
                {
                    query = query + "valid='" + validFilter + "'";
                }
                else
                {
                    query = query + "valid='" + validFilter + "'" + " " + "and ";
                }
            }
            if (StringUtils.isNotBlank(domainTypeIDFiter))
            {
                query = query + "domainTypeID=" + domainTypeIDFiter;
            }

            query = query + " and status=true";
        }
        else
        {
            query = query + " where status=true";
        }

        if (StringUtils.isNotBlank(sortBy))
        {
            if (sortBy.equalsIgnoreCase("DomainTypeId"))
            {
                query = query + "  order by " + "npDomain.typeName";
            }
            else
            {
                query = query + "  order by " + sortBy;
            }
        }

        Long npEmploymentRoleCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npEmploymentRoleImplResult.add(getPaginatedresult(query, pageNo));
        npEmploymentRoleImplResult.add((int) npEmploymentRoleCount.longValue());

        return npEmploymentRoleImplResult;
    }

    @SuppressWarnings("unchecked")
    private List<NpEmploymentRoleImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpEmploymentRoleImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpDomainImpl> getDomainTypes()
    {

        List<NpDomainImpl> npDomainTypes = (List<NpDomainImpl>) getHibernateTemplate().find("from NpDomainImpl");
        return npDomainTypes;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getNpEmploymentRoleNames()
    {
        List<String> roleNames = (List<String>) getHibernateTemplate().find("select roleName from NpEmploymentRoleImpl where status=true");

        return roleNames;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Transactional
    @Override
    public void saveNpEmploymentRoles(AddNpEmploymentRoleForm addNpEmploymentRoleForm)
    {

        NpEmploymentRoleImpl npEmploymentRoleImpl = null;
        if (StringUtils.isNotBlank(addNpEmploymentRoleForm.getRoleId()))
        {
            npEmploymentRoleImpl = (NpEmploymentRoleImpl) getHibernateTemplate().find("from NpEmploymentRoleImpl where id=?", Integer.parseInt(addNpEmploymentRoleForm.getRoleId())).get(0);
            /*if(npEmploymentRoleImpl.getCoRoleAlias()!=null)
			{
				for(CoRoleAlias coAlias:npEmploymentRoleImpl.getCoRoleAlias())
				{
					getHibernateTemplate().delete(coAlias);
				}
			}*/

        }
        else
        {
            npEmploymentRoleImpl = new NpEmploymentRoleImpl();
        }

        npEmploymentRoleImpl.setRoleName(addNpEmploymentRoleForm.getRoleName());
        npEmploymentRoleImpl.setCreatedDate(new Date());
        npEmploymentRoleImpl.setStatus(true);
        NpDomainImpl npDomainTypes = (NpDomainImpl) getHibernateTemplate().find("from NpDomainImpl where id=?", Integer.parseInt(addNpEmploymentRoleForm.getDomainId())).get(0);
        if (npDomainTypes != null)
        {
            npEmploymentRoleImpl.setNpDomain(npDomainTypes);
        }
        npEmploymentRoleImpl.setValid(addNpEmploymentRoleForm.getValid());
        getHibernateTemplate().saveOrUpdate(npEmploymentRoleImpl);

        if (addNpEmploymentRoleForm.getCertificationAliases() != null)
        {
            for (CertificationAliases roleAliases : addNpEmploymentRoleForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(roleAliases.getAliasSource()))
                {
                    CoRoleAlias coRoleAlias = null;

                    if (StringUtils.isNotBlank(roleAliases.getAliasId()))
                    {
                        coRoleAlias = (CoRoleAlias) getHibernateTemplate().find("from CoRoleAlias where roleAliasID=?", Integer.parseInt(roleAliases.getAliasId())).get(0);
                    }
                    else
                    {
                        coRoleAlias = new CoRoleAlias();
                    }
                    coRoleAlias.setRoleAliasName(roleAliases.getAliasName());
                    CoSource coSource = (CoSource) getHibernateTemplate().find("from CoSource where sourceID=?", Integer.parseInt(roleAliases.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coRoleAlias.setCoSource(coSource);
                    }
                    coRoleAlias.setNpEmploymentRoleImpl(npEmploymentRoleImpl);
                    getHibernateTemplate().saveOrUpdate(coRoleAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getcoSources()
    {

        List<CoSource> coSources = (List<CoSource>) getHibernateTemplate().find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpEmploymentRoleImpl> getNpEmploymentRoleforAliasSearch(final String searchWord, String mergeId)
    {
        List<CoRoleAlias> coRoleAliasList = (List<CoRoleAlias>) getHibernateTemplate().find("from CoRoleAlias where roleAliasName like '" + searchWord + "%'");
        Set<NpEmploymentRoleImpl> npEmploymentRoleImplSet = new HashSet<NpEmploymentRoleImpl>();
        Integer merge = Integer.parseInt(mergeId);
        if (coRoleAliasList != null)
        {
            for (CoRoleAlias coRoleAlias : coRoleAliasList)
            {

                NpEmploymentRoleImpl npEmploymentRoleImpl = coRoleAlias.getNpEmploymentRoleImpl();
                if (npEmploymentRoleImpl.isStatus() && npEmploymentRoleImpl.getId().intValue() != merge.intValue())
                {
                    npEmploymentRoleImplSet.add(npEmploymentRoleImpl);
                }
            }
        }

        List<NpEmploymentRoleImpl> npEmpRolesList = (List<NpEmploymentRoleImpl>) hibernateTemplate.find("from NpEmploymentRoleImpl where roleName like '" + searchWord + "%'");
        if (npEmpRolesList != null)
        {
            for (NpEmploymentRoleImpl npEmploymentRoleImpl : npEmpRolesList)
            {
                if (npEmploymentRoleImpl.isStatus() && npEmploymentRoleImpl.getId().intValue() != merge.intValue())
                {
                    npEmploymentRoleImplSet.add(npEmploymentRoleImpl);
                }
            }
        }
        return npEmploymentRoleImplSet;
    }

    @Transactional
    @Override
    public void mergeNpEmploymentRole(AddNpEmploymentRoleForm addNpEmploymentRoleForm)
    {

        boolean isExists = true;
        NpEmploymentRoleImpl npEmploymentRoleImpl = (NpEmploymentRoleImpl) getHibernateTemplate().find("from NpEmploymentRoleImpl where id=?", Integer.parseInt(addNpEmploymentRoleForm.getMergeToId())).get(0);
        NpEmploymentRoleImpl npEmploymentRoleImplLatest = (NpEmploymentRoleImpl) getHibernateTemplate().find("from NpEmploymentRoleImpl where id=?", Integer.parseInt(addNpEmploymentRoleForm.getRoleId())).get(0);

        if (npEmploymentRoleImpl != null)
        {
            if (npEmploymentRoleImpl.getRoleName().equalsIgnoreCase(npEmploymentRoleImplLatest.getRoleName()))
            {
                isExists = false;
            }

            npEmploymentRoleImpl.setStatus(false);
            getHibernateTemplate().saveOrUpdate(npEmploymentRoleImpl);
            //updating references of npSkills in npCandidateSkills to npSkillsLatest
            Set<NpCandidateEmpRoles> npCandidateEmpRoleses = npEmploymentRoleImpl.getNpCandidateEmpRoleses();
            for (NpCandidateEmpRoles npCandidateEmpRole : npCandidateEmpRoleses)
            {
                npCandidateEmpRole.setNpEmploymentRoles(npEmploymentRoleImplLatest);
                getHibernateTemplate().saveOrUpdate(npCandidateEmpRole);
            }

            for (CoRoleAlias coRoleAlias : npEmploymentRoleImpl.getCoRoleAlias())
            {
                if (coRoleAlias.getRoleAliasName().equalsIgnoreCase(npEmploymentRoleImpl.getRoleName()))
                {
                    isExists = false;
                }

                for (CoRoleAlias coRoleAliasLatest : npEmploymentRoleImplLatest.getCoRoleAlias())
                {
                    if (coRoleAliasLatest.getRoleAliasName().equalsIgnoreCase(npEmploymentRoleImpl.getRoleName()))
                    {
                        isExists = false;
                    }
                    if (!coRoleAliasLatest.getRoleAliasName().equalsIgnoreCase(coRoleAlias.getRoleAliasName()))
                    {
                        coRoleAlias.setNpEmploymentRoleImpl(npEmploymentRoleImplLatest);
                        getHibernateTemplate().saveOrUpdate(coRoleAlias);
                    }
                }
            }
            if (isExists)
            {
                CoRoleAlias coRoleAlias = new CoRoleAlias();
                CoSource coSource = new CoSource();
                coSource.setSourceID(4);
                coRoleAlias.setCoSource(coSource);
                coRoleAlias.setRoleAliasName(npEmploymentRoleImpl.getRoleName());
                coRoleAlias.setNpEmploymentRoleImpl(npEmploymentRoleImplLatest);
                getHibernateTemplate().saveOrUpdate(coRoleAlias);
            }
        }
    }

    @Transactional
    @Override
    public void deletNpEmploymentRoles(String roleId)
    {

        NpEmploymentRoleImpl npEmploymentRoleImpl = (NpEmploymentRoleImpl) getHibernateTemplate().find("from NpEmploymentRoleImpl where id=?", Integer.parseInt(roleId)).get(0);

        if (npEmploymentRoleImpl != null)
        {
            npEmploymentRoleImpl.setStatus(false);
            getHibernateTemplate().saveOrUpdate(npEmploymentRoleImpl);
        }
    }

    @Transactional
    @Override
    public void deleteCoAliasRoles(String aliasId)
    {

        CoRoleAlias coRoleAlias = (CoRoleAlias) getHibernateTemplate().find("from CoRoleAlias where id=?", Integer.parseInt(aliasId)).get(0);

        if (coRoleAlias != null)
        {
            getHibernateTemplate().delete(coRoleAlias);
        }
    }

    @Override
    public Set<CoRoleAlias> getAliases(final String id)
    {

        NpEmploymentRoleImpl npEmploymentRoleImpl = getHibernateTemplate().get(NpEmploymentRoleImpl.class, Integer.parseInt(id));
        if (npEmploymentRoleImpl != null)
        {
            return npEmploymentRoleImpl.getCoRoleAlias();
        }
        return null;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }
}
