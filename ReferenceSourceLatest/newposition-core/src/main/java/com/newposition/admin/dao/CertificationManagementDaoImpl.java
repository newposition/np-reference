package com.newposition.admin.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.forms.AddNpCertificationForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCandidateCertificate;
import com.newposition.candidate.domain.NpCertifications;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoCertificationAlias;
import com.newposition.common.domain.CoCertificationAliasImpl;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpInstituteCertificate;
import com.newposition.common.domain.NpInstituteCertificateImpl;

@Repository
public class CertificationManagementDaoImpl implements CertificationManagementDao
{

    protected static final Logger LOG = Logger.getLogger(CertificationManagementDaoImpl.class);

    private HibernateTemplate hibernateTemplate;

    private int numberOfReusltsPerPage;

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpInstituteCertificate(String sortBy,
                                                  String certificationTypeFilter, String validFilter, String instituteFilter, String pageNumber)
    {
        List npInstituteCertificatesResult = new ArrayList();

        String query = "from com.newposition.common.domain.NpInstituteCertificate IC where IC.status=true ";

        if (StringUtils.isNotBlank(certificationTypeFilter) || StringUtils.isNotBlank(validFilter) || StringUtils.isNotBlank(instituteFilter))
        {
            query = "from com.newposition.common.domain.NpInstituteCertificate IC where IC.status=true and ";
            if (StringUtils.isNotBlank(certificationTypeFilter))
            {
                if (StringUtils.isBlank(validFilter) && StringUtils.isBlank(instituteFilter))
                {
                    query = query + "IC.npCertifications.certificateType='" + certificationTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "IC.npCertifications.certificateType='" + certificationTypeFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(instituteFilter))
                {
                    query = query + "IC.npCertifications.valid=" + validFilter;
                }
                else
                {
                    query = query + "IC.npCertifications.valid=" + validFilter + " " + "and ";
                }
            }
            if (StringUtils.isNotBlank(instituteFilter))
            {
                query = query + "IC.npInstitutions.id=" + instituteFilter;
            }
        }

        if (StringUtils.isNotBlank(sortBy) && !sortBy.equals("instituteName"))
        {
            query = query + "  order by IC.npCertifications." + sortBy;
        }
        else if (StringUtils.isNotBlank(sortBy) && sortBy.equals("instituteName"))
        {
            query = query + "  order by IC.npInstitutions." + sortBy;
        }

//		npInstituteCertificates=(List<NpInstituteCertificate>) getHibernateTemplate().find(query);

        Long npInstituteCertificatesCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npInstituteCertificatesResult.add(getPaginatedresult(query, pageNo));
        npInstituteCertificatesResult.add((int) npInstituteCertificatesCount.longValue());

        return npInstituteCertificatesResult;
    }

    @SuppressWarnings("unchecked")
    private List<NpInstituteCertificate> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpInstituteCertificate>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @Override
    public Set<CoCertificationAliasImpl> getAliases(final String id)
    {

        NpInstituteCertificateImpl npInstituteCertificate = (NpInstituteCertificateImpl) getHibernateTemplate().get(NpInstituteCertificateImpl.class, Integer.parseInt(id));
        if (npInstituteCertificate != null)
        {
            NpCertifications npCertifications = npInstituteCertificate.getNpCertifications();
            return npCertifications.getCoCertificationAlias();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpInstitutions> getNpInstitutions()
    {

        List<NpInstitutions> npInstitutions = (List<NpInstitutions>) getHibernateTemplate().find("from NpInstitutions");
        return npInstitutions;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCertificationTypes()
    {
        List<String> npCertificationTypes = (List<String>) getHibernateTemplate().find("select certificateType from NpCertifications where certificateType != null");
        return npCertificationTypes;
    }

    @Transactional
    @Override
    public void saveNpCertifications(AddNpCertificationForm addNpCertificationForm)
    {

        NpInstituteCertificate npInstituteCertificate = null;
        NpCertifications npCertifications = null;
        if (addNpCertificationForm.getNpInstituteCertificateId() != null && StringUtils.isNotBlank(addNpCertificationForm.getNpInstituteCertificateId().toString()))
        {
            npInstituteCertificate = (NpInstituteCertificate) getHibernateTemplate().find("from com.newposition.common.domain.NpInstituteCertificate where id=?", addNpCertificationForm.getNpInstituteCertificateId()).get(0);
            npCertifications = npInstituteCertificate.getNpCertifications();
       /*if(npCertifications.getCoCertificationAlias()!=null)
	   {
	    for(CoCertificationAlias coCertificationAlias:npCertifications.getCoCertificationAlias())
	    {
	     getHibernateTemplate().delete(coCertificationAlias);
	    }
	   }*/
        }
        else
        {
            npInstituteCertificate = new NpInstituteCertificateImpl();
            npCertifications = new NpCertifications();
        }

        npCertifications.setCertificateName(addNpCertificationForm.getCertificationName());
        npCertifications.setCreatedDate(new Date());
        NpInstitutions npInstitutions = (NpInstitutions) getHibernateTemplate().find("from NpInstitutions where id=?", addNpCertificationForm.getInstitutionId()).get(0);

        npCertifications.setValid(addNpCertificationForm.getValid());
        npCertifications.setCertificateType(addNpCertificationForm.getCertificationType());
        getHibernateTemplate().saveOrUpdate(npCertifications);
        if (npInstitutions != null)
        {
            npInstituteCertificate.setNpCertifications(npCertifications);
            npInstituteCertificate.setNpInstitutions(npInstitutions);
            npInstituteCertificate.setStatus(true);
        }
        getHibernateTemplate().saveOrUpdate(npInstituteCertificate);
        if (addNpCertificationForm.getCertificationAliases() != null)
        {
            for (CertificationAliases certificationAliases : addNpCertificationForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(certificationAliases.getAliasSource()))
                {
                    CoCertificationAlias coCertificationAlias = null;

                    if (StringUtils.isNotBlank(certificationAliases.getAliasId()))
                    {
                        coCertificationAlias = (CoCertificationAlias) getHibernateTemplate().find("from com.newposition.common.domain.CoCertificationAlias where certificateAliasID=?", Integer.parseInt(certificationAliases.getAliasId())).get(0);
                    }
                    else
                    {
                        coCertificationAlias = new CoCertificationAliasImpl();
                    }
                    coCertificationAlias.setCertificationAliasName(certificationAliases.getAliasName());
                    CoSource coSource = (CoSource) getHibernateTemplate().find("from CoSource where sourceID=?", Integer.parseInt(certificationAliases.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coCertificationAlias.setCoSource(coSource);
                    }
                    coCertificationAlias.setNpCertifications(npCertifications);
                    getHibernateTemplate().saveOrUpdate(coCertificationAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getCoSources()
    {

        List<CoSource> coSources = (List<CoSource>) getHibernateTemplate().find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpInstituteCertificateImpl> getNpInstituteCertificateforAliasSearch(final String searchWord, String mergeId)
    {
        Set<NpInstituteCertificateImpl> npInstituteCertificatesSet = new HashSet<NpInstituteCertificateImpl>();
        Integer merge = Integer.parseInt(mergeId);
        List<CoCertificationAlias> coCertificationAliasList = (List<CoCertificationAlias>) getHibernateTemplate().find("from com.newposition.common.domain.CoCertificationAlias where certificationAliasName like '" + searchWord + "%'");
        for (CoCertificationAlias coCertificationAlias : coCertificationAliasList)
        {
            List<NpCertifications> certificationsList = (List<NpCertifications>) getHibernateTemplate().find("from NpCertifications  where id=?", coCertificationAlias.getNpCertifications().getId());
            if (certificationsList != null)
            {
                for (NpCertifications npCertifications : certificationsList)
                {
                    Set<NpInstituteCertificateImpl> instituteCertificateList = npCertifications.getNpInstituteCertificates();
                    for (NpInstituteCertificateImpl npInstituteCertificateImpl : instituteCertificateList)
                    {
                        if (npInstituteCertificateImpl.getStatus() && npInstituteCertificateImpl.getId().intValue() != merge.intValue())
                        {
                            npInstituteCertificatesSet.add(npInstituteCertificateImpl);
                        }
                    }
                }
            }
        }
        List<NpCertifications> certificationsList = (List<NpCertifications>) getHibernateTemplate().find("from NpCertifications where certificateName like '" + searchWord + "%'");
        for (NpCertifications npCertifications : certificationsList)
        {
            if (npCertifications.getNpInstituteCertificates() != null)
            {
                Set<NpInstituteCertificateImpl> instituteCertificateList = npCertifications.getNpInstituteCertificates();
                for (NpInstituteCertificateImpl npInstituteCertificateImpl : instituteCertificateList)
                {
                    if (npInstituteCertificateImpl.getStatus() && npInstituteCertificateImpl.getId().intValue() != merge.intValue())
                    {
                        npInstituteCertificatesSet.add(npInstituteCertificateImpl);
                    }
                }
            }
        }
        return npInstituteCertificatesSet;
    }

    @Override
    public void removeCoCertificateAlias(final int coCertificationAliasId)
    {
        CoCertificationAlias coCertificationAlias = new CoCertificationAliasImpl();
        coCertificationAlias.setCertificationAliasID(coCertificationAliasId);
        getHibernateTemplate().delete(coCertificationAlias);
    }

    @Override
    public NpInstituteCertificate getNpInstituteCertificateById(final int npInstituteCertificateId)
    {
        NpInstituteCertificate npInstituteCertificateImpl = getHibernateTemplate().get(NpInstituteCertificateImpl.class, npInstituteCertificateId);
        return npInstituteCertificateImpl;
    }

    @Transactional
    @Override
    public void removeNpInstituteCertificateById(final int npInstituteCertificateId)
    {
        NpInstituteCertificateImpl instituteCertificate = new NpInstituteCertificateImpl();
        instituteCertificate.setId(npInstituteCertificateId);
        getHibernateTemplate().delete(instituteCertificate);
    }

    @Transactional
    @Override
    public void saveMergedNpInstituteCertificate(AddNpCertificationForm addNpCertificationForm)
    {
        boolean isExists = true;
        NpInstituteCertificate npInstituteCertificate = null;
        NpInstituteCertificate npInstituteCertificateMerge = getNpInstituteCertificateById(Integer.parseInt(addNpCertificationForm.getMergeId()));
        NpCertifications npCertifications = null;
        if (addNpCertificationForm.getNpInstituteCertificateId() != null && addNpCertificationForm.getNpInstituteCertificateId() > 0)
        {
            npInstituteCertificate = (NpInstituteCertificate) getHibernateTemplate().find("from com.newposition.common.domain.NpInstituteCertificate where id=?", addNpCertificationForm.getNpInstituteCertificateId()).get(0);
            npCertifications = npInstituteCertificate.getNpCertifications();
        }

        npCertifications.setCertificateName(addNpCertificationForm.getCertificationName());
        npCertifications.setCreatedDate(new Date());
        NpInstitutions npInstitutions = (NpInstitutions) getHibernateTemplate().find("from NpInstitutions where id=?", addNpCertificationForm.getInstitutionId()).get(0);

        npCertifications.setValid(addNpCertificationForm.getValid());
        npCertifications.setCertificateType(addNpCertificationForm.getCertificationType());
        getHibernateTemplate().saveOrUpdate(npCertifications);
        if (npInstitutions != null)
        {
            npInstituteCertificate.setNpCertifications(npCertifications);
            npInstituteCertificate.setNpInstitutions(npInstitutions);
        }
        npInstituteCertificate.setStatus(true);
        getHibernateTemplate().saveOrUpdate(npInstituteCertificate);
        if (npInstituteCertificateMerge.getNpCertifications().getCertificateName().equalsIgnoreCase(npInstituteCertificate.getNpCertifications().getCertificateName()))
        {
            isExists = false;
        }
		
		/*if(addNpCertificationForm.getCertificationAliases()!=null)
		{
			for(CertificationAliases certificationAliases: addNpCertificationForm.getCertificationAliases())
			{
				if(StringUtils.isNotBlank(certificationAliases.getAliasSource()))
				{
					CoCertificationAliasImpl coCertificationAlias=null;

					if(StringUtils.isNotBlank(certificationAliases.getAliasId()))
					{
						coCertificationAlias=(CoCertificationAliasImpl) getHibernateTemplate().find("from com.newposition.common.domain.CoCertificationAlias where certificateAliasID=?",Integer.parseInt(certificationAliases.getAliasId())).get(0);
					}
					else
					{
						coCertificationAlias=new CoCertificationAliasImpl();
					}
				coCertificationAlias.setCertificationAliasName(certificationAliases.getAliasName());
				CoSource coSource=(CoSource) getHibernateTemplate().find("from CoSource where sourceID=?",Integer.parseInt(certificationAliases.getAliasSource())).get(0);
				if(coSource!=null)
				{
					coCertificationAlias.setCoSource(coSource);
				}
				coCertificationAlias.setNpCertifications(npCertifications);
				getHibernateTemplate().saveOrUpdate(coCertificationAlias);
				}
			}
			
		}*/

        if (npInstituteCertificateMerge.getNpCertifications().getCoCertificationAlias() != null)
        {
            for (CoCertificationAliasImpl coCertificationAliasImpl : npInstituteCertificateMerge.getNpCertifications().getCoCertificationAlias())
            {

                if (coCertificationAliasImpl.getCertificationAliasName().equalsIgnoreCase(npInstituteCertificateMerge.getNpCertifications().getCertificateName()))
                {
                    isExists = false;
                }
                for (CoCertificationAliasImpl coCertificationAliasImplLatest : npCertifications.getCoCertificationAlias())
                {
                    if (coCertificationAliasImplLatest.getCertificationAliasName().equalsIgnoreCase(npInstituteCertificateMerge.getNpCertifications().getCertificateName()))
                    {
                        isExists = false;
                    }
                    if (!coCertificationAliasImplLatest.getCertificationAliasName().equalsIgnoreCase(coCertificationAliasImpl.getCertificationAliasName()))
                    {
                        coCertificationAliasImpl.setNpCertifications(npCertifications);

                        getHibernateTemplate().saveOrUpdate(coCertificationAliasImpl);
                    }
                }
            }
        }
        if (isExists)
        {
            CoCertificationAliasImpl coCertificationAliasImpl = new CoCertificationAliasImpl();
            CoSource coSource = new CoSource();
            coSource.setSourceID(4);
            coCertificationAliasImpl.setCoSource(coSource);
            coCertificationAliasImpl.setCertificationAliasName(npInstituteCertificateMerge.getNpCertifications().getCertificateName());
            coCertificationAliasImpl.setNpCertifications(npCertifications);
            hibernateTemplate.saveOrUpdate(coCertificationAliasImpl);
        }

        npInstituteCertificateMerge.setStatus(false);
        getHibernateTemplate().saveOrUpdate(npInstituteCertificateMerge);
        Set<NpCandidateCertificate> candidateCertficates = npInstituteCertificateMerge.getNpCertifications().getNpCandidateCertificates();
        for (NpCandidateCertificate npCandidateCertificate : candidateCertficates)
        {
            npCandidateCertificate.setNpCertifications(npCertifications);
            getHibernateTemplate().saveOrUpdate(npCandidateCertificate);
        }
    }
}
