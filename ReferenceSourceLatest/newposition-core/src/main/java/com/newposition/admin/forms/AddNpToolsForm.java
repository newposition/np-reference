package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Ravi
 */
public class AddNpToolsForm
{

    private String toolId;
    private String domainId;
    private String toolType;
    private String toolName;
    private String valid;
    private List<CertificationAliases> certificationAliases;
    private String mergeToId;
    private boolean deleteTool;

    public String getMergeToId()
    {
        return mergeToId;
    }

    public void setMergeToId(String mergeToId)
    {
        this.mergeToId = mergeToId;
    }

    public boolean isDeleteTool()
    {
        return deleteTool;
    }

    public void setDeleteTool(boolean deleteTool)
    {
        this.deleteTool = deleteTool;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public String getToolId()
    {
        return toolId;
    }

    public void setToolId(String toolId)
    {
        this.toolId = toolId;
    }

    public String getValid()
    {
        return valid;
    }

    public void setValid(String valid)
    {
        this.valid = valid;
    }

    public String getDomainId()
    {
        return domainId;
    }

    public void setDomainId(String domainId)
    {
        this.domainId = domainId;
    }

    public String getToolType()
    {
        return toolType;
    }

    public void setToolType(String toolType)
    {
        this.toolType = toolType;
    }

    public String getToolName()
    {
        return toolName;
    }

    public void setToolName(String toolName)
    {
        this.toolName = toolName;
    }
}
