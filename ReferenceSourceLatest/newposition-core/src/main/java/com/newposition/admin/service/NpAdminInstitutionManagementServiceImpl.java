package com.newposition.admin.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.NpAdminInstitutionManagementDao;
import com.newposition.admin.domain.CoInstituteAliasImpl;
import com.newposition.admin.forms.AddNpInstitutionForm;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoSource;

@Service
public class NpAdminInstitutionManagementServiceImpl implements NpAdminInstitutionManagementService
{

    private NpAdminInstitutionManagementDao institutionManagementDao;

    public NpAdminInstitutionManagementDao getInstitutionManagementDao()
    {
        return institutionManagementDao;
    }

    public void setInstitutionManagementDao(
            NpAdminInstitutionManagementDao institutionManagementDao)
    {
        this.institutionManagementDao = institutionManagementDao;
    }

    @Override
    public List getFilteredNpInstitutes(String sortBy,
                                        String instituteTypeFilter, String validFilter, String instituteFilter, String pageNumber)
    {

        return institutionManagementDao.getFilteredNpInstitutes(sortBy, instituteTypeFilter, validFilter, instituteFilter, pageNumber);
    }

    @Override
    public Set<String> getInstituteNames(List<NpInstitutions> npInstitutions)
    {

        List<String> instituteNamesList = new ArrayList<String>();
        for (NpInstitutions npInstitution : npInstitutions)
        {
            instituteNamesList.add(npInstitution.getInstituteName());
        }

//				institutionManagementDao.getInstituteNames();

        Set<String> instituteNames = new HashSet<String>(instituteNamesList);
        return instituteNames;
    }

    @Override
    public Set<String> getInstituteTypes(List<NpInstitutions> npInstitutions)
    {

        List<String> instituteTypesList = new ArrayList<String>();
//				institutionManagementDao.getInstituteTypes();
        for (NpInstitutions npInstitution : npInstitutions)
        {
            instituteTypesList.add(npInstitution.getInstituteType());
        }
        Set<String> instituteTypes = new HashSet<String>(instituteTypesList);
        return instituteTypes;
    }

    @Override
    public void saveNpInstitute(AddNpInstitutionForm addNpInstitutionForm)
    {
        institutionManagementDao.saveNpInstitute(addNpInstitutionForm);
    }

    @Override
    public Set<CoSource> getCoSources()
    {
        List<CoSource> coSourceList = institutionManagementDao.getCoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpInstitutions> getNpInstituteforAliasSearch(final String searchWord, String mergeId)
    {

        return institutionManagementDao.getNpInstituteforAliasSearch(searchWord, mergeId);
    }

    @Transactional
    @Override
    public void removeCoInstituteAlias(final int coInstituteAliasId)
    {
        institutionManagementDao.removeCoInstituteAlias(coInstituteAliasId);
    }

    @Override
    public NpInstitutions getNpInstituteById(final int npInstituteId)
    {

        return institutionManagementDao.getNpInstituteById(npInstituteId);
    }

    @Override
    public void saveMergedNpInstitute(AddNpInstitutionForm addNpInstitutionForm)
    {
        institutionManagementDao.saveMergedNpInstitute(addNpInstitutionForm);
    }

    @Override
    public void removeNpInstituteById(final int npInstituteId)
    {

        institutionManagementDao.removeNpInstituteById(npInstituteId);
    }

    public Set<CoInstituteAliasImpl> getAliases(final String id)
    {
        return institutionManagementDao.getAliases(id);
    }
}
