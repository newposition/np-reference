package com.newposition.admin.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.exceptions.DomainNotFoundException;

@Repository
public class SkillManagementDaoImpl implements SkillManagementDao
{

    protected static final Logger LOG = Logger.getLogger(SkillManagementDaoImpl.class);

    private int numberOfReusltsPerPage;
    private HibernateTemplate hibernateTemplate;

    @SuppressWarnings("unchecked")
    @Override
    public NpDomainImpl getDomainById(String id)
    {

        List<NpDomainImpl> npDomainImpl = (List<NpDomainImpl>) getHibernateTemplate().find("from NpDomainImpl where id=?", Integer.parseInt(id));
        if (npDomainImpl.size() > 0)
        {
            return npDomainImpl.get(0);
        }
        else
        {
            throw new DomainNotFoundException("Domain Not Found Exception");
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpSkills(String sortBy,
                                    String skillTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        List npSkillsSet = new ArrayList();

        String query = "from NpSkillImpl";

        if (StringUtils.isNotBlank(skillTypeFilter) || StringUtils.isNotBlank(validFilter) || StringUtils.isNotBlank(domainFiter))
        {
            query = "from NpSkillImpl where ";
            if (StringUtils.isNotBlank(skillTypeFilter))
            {
                if (StringUtils.isBlank(validFilter) && StringUtils.isBlank(domainFiter))
                {
                    query = query + "skillType='" + skillTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "skillType='" + skillTypeFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(domainFiter))
                {
                    query = query + "valid='" + validFilter + "'";
                }
                else
                {
                    query = query + "valid='" + validFilter + "'" + " " + "and ";
                }
            }
            if (StringUtils.isNotBlank(domainFiter))
            {
                query = query + "domainTypeid=" + domainFiter;
            }

            query = query + " and status=true";
        }
        else
        {
            query = query + " where status=true";
        }

        if (StringUtils.isNotBlank(sortBy))
        {
            if (sortBy.equalsIgnoreCase("DomainTypeId"))
            {
                query = query + "  order by " + "npDomain.typeName";
            }
            else
            {
                query = query + "  order by " + sortBy;
            }
        }

        Long npSkillCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npSkillsSet.add(getPaginatedresult(query, pageNo));
        npSkillsSet.add((int) npSkillCount.longValue());

        return npSkillsSet;
    }

    @SuppressWarnings("unchecked")
    private List<NpSkillImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpSkillImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpDomainImpl> getDomainTypes()
    {

        List<NpDomainImpl> npDomainTypes = (List<NpDomainImpl>) getHibernateTemplate().find("from NpDomainImpl");
        return npDomainTypes;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getSkillTypes()
    {
//		List<String> npSkillTypes=(List<String>) getHibernateTemplate().find("select skillType from NpSkills");
        List<String> npSkillTypes = (List<String>) getHibernateTemplate().find("select skillType from NpSkillImpl where status=true and skillType!=null");

        return npSkillTypes;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Transactional
    @Override
    public void saveNpSkills(AddNpSKillsForm addNpSKillsForm)
    {

        NpSkillImpl npSkills = null;
        if (StringUtils.isNotBlank(addNpSKillsForm.getSkillId()))
        {
            npSkills = (NpSkillImpl) getHibernateTemplate().find("from NpSkillImpl where id=?", Integer.parseInt(addNpSKillsForm.getSkillId())).get(0);
        }
        else
        {
            npSkills = new NpSkillImpl();
        }

        npSkills.setSkillName(addNpSKillsForm.getSkillName());
        npSkills.setCreatedDate(new Date());
        npSkills.setStatus(true);

        NpDomainImpl npDomainTypes = null;
        npDomainTypes = getDomainById(addNpSKillsForm.getDomainId());
        if (npDomainTypes != null)
        {
            npSkills.setNpDomainTypes(npDomainTypes);
        }
        npSkills.setValid(addNpSKillsForm.getValid());
        npSkills.setSkillType(addNpSKillsForm.getSkillType());
        getHibernateTemplate().saveOrUpdate(npSkills);

        if (addNpSKillsForm.getCertificationAliases() != null)
        {
            for (CertificationAliases skillAlias : addNpSKillsForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(skillAlias.getAliasSource()))
                {
                    CoSkillAlias coSkillAlias = null;

                    if (StringUtils.isNotBlank(skillAlias.getAliasId()))
                    {
                        coSkillAlias = (CoSkillAlias) getHibernateTemplate().find("from CoSkillAlias where skillAliasID=?", Integer.parseInt(skillAlias.getAliasId())).get(0);
                    }
                    else
                    {
                        coSkillAlias = new CoSkillAlias();
                    }
                    coSkillAlias.setSkillAliasName(skillAlias.getAliasName());
                    CoSource coSource = (CoSource) getHibernateTemplate().find("from CoSource where sourceID=?", Integer.parseInt(skillAlias.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coSkillAlias.setCoSource(coSource);
                    }
                    coSkillAlias.setNpSkillImpl(npSkills);
                    getHibernateTemplate().saveOrUpdate(coSkillAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getcoSources()
    {

        List<CoSource> coSources = (List<CoSource>) getHibernateTemplate().find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpSkillImpl> getNpSKillsforAliasSearch(final String searchWord, String mergeId)
    {
        List<CoSkillAlias> coSkillAliasList = (List<CoSkillAlias>) getHibernateTemplate().find("from CoSkillAlias where skillAliasName like '" + searchWord + "%'");
        Set<NpSkillImpl> npSkillSet = new HashSet<NpSkillImpl>();
        Integer merge = Integer.parseInt(mergeId);
        if (coSkillAliasList != null)
        {
            for (CoSkillAlias coSkillAlias : coSkillAliasList)
            {
                NpSkillImpl npSkillImpl = coSkillAlias.getNpSkillImpl();
                if (npSkillImpl.isStatus() && npSkillImpl.getId().intValue() != merge.intValue())
                {
                    npSkillSet.add(npSkillImpl);
                }
            }
        }

        List<NpSkillImpl> npSkillsList = (List<NpSkillImpl>) hibernateTemplate.find("from NpSkillImpl where skillName like '" + searchWord + "%'");
        for (NpSkillImpl npSkillImpl : npSkillsList)
        {
            if (npSkillImpl.isStatus() && npSkillImpl.getId().intValue() != merge.intValue())
            {
                npSkillSet.add(npSkillImpl);
            }
        }

        return npSkillSet;
    }

    @Transactional
    @Override
    public void mergeNpSkill(AddNpSKillsForm addNpSkillsForm)
    {
        boolean isExists = true;
        NpSkillImpl npSkills = (NpSkillImpl) getHibernateTemplate().find("from NpSkillImpl where id=?", Integer.parseInt(addNpSkillsForm.getMergeToId())).get(0);
        NpSkillImpl npSkillsLatest = (NpSkillImpl) getHibernateTemplate().find("from NpSkillImpl where id=?", Integer.parseInt(addNpSkillsForm.getSkillId())).get(0);

        if (npSkills != null)
        {
            if (npSkills.getSkillName().equalsIgnoreCase(npSkillsLatest.getSkillName()))
            {
                isExists = false;
            }

            npSkills.setStatus(false);
            getHibernateTemplate().saveOrUpdate(npSkills);
            //updating references of npSkills in npCandidateSkills to npSkillsLatest
            Set<NpCandidateSkills> npCandidateSkills = npSkills.getNpCandidateSkillses();
            for (NpCandidateSkills npCandidateSkill : npCandidateSkills)
            {
                npCandidateSkill.setNpSkill(npSkillsLatest);
                getHibernateTemplate().saveOrUpdate(npCandidateSkill);
            }

            for (CoSkillAlias coSkillAlias : npSkills.getCoSkillAlias())
            {
                if (coSkillAlias.getSkillAliasName().equalsIgnoreCase(npSkills.getSkillName()))
                {
                    isExists = false;
                }

                for (CoSkillAlias coSkillAliasLatest : npSkillsLatest.getCoSkillAlias())
                {
                    if (coSkillAliasLatest.getSkillAliasName().equalsIgnoreCase(npSkills.getSkillName()))
                    {
                        isExists = false;
                    }
                    if (!coSkillAliasLatest.getSkillAliasName().equalsIgnoreCase(coSkillAlias.getSkillAliasName()))
                    {
                        coSkillAlias.setNpSkillImpl(npSkillsLatest);
                        getHibernateTemplate().saveOrUpdate(coSkillAlias);
                    }
                }
            }
            if (isExists)
            {
                CoSkillAlias coSkillAlias = new CoSkillAlias();
                CoSource coSource = new CoSource();
                coSource.setSourceID(4);
                coSkillAlias.setCoSource(coSource);
                coSkillAlias.setSkillAliasName(npSkills.getSkillName());
                coSkillAlias.setNpSkillImpl(npSkillsLatest);
                getHibernateTemplate().saveOrUpdate(coSkillAlias);
            }
        }
    }

    @Transactional
    @Override
    public void deletNpSkills(String skillId)
    {

        NpSkillImpl npSkills = (NpSkillImpl) getHibernateTemplate().find("from NpSkillImpl where id=?", Integer.parseInt(skillId)).get(0);

        if (npSkills != null)
        {
            npSkills.setStatus(false);
            getHibernateTemplate().saveOrUpdate(npSkills);
        }
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @Transactional
    @Override
    public void deleteCoAliasSkills(String aliasId)
    {
        CoSkillAlias coSkillAlias = (CoSkillAlias) hibernateTemplate.find("from CoSkillAlias where id=?", Integer.parseInt(aliasId)).get(0);

        if (coSkillAlias != null)
        {
            hibernateTemplate.delete(coSkillAlias);
        }
    }

    @Override
    public Set<CoSkillAlias> getAliases(final String id)
    {

        NpSkillImpl npSkillImpl = getHibernateTemplate().get(NpSkillImpl.class, Integer.parseInt(id));
        if (npSkillImpl != null)
        {
            return npSkillImpl.getCoSkillAlias();
        }
        return null;
    }
}
