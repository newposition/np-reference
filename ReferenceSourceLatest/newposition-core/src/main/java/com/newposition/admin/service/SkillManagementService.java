package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpSkillImpl;

public interface SkillManagementService
{

    public Set<NpDomainImpl> getDomainTypes();

    public Set<String> getSkillTypes();

    public void saveNpSkills(AddNpSKillsForm addNpSKillsForm);

    public Set<CoSource> getcoSources();

    public Set<NpSkillImpl> getNpSKillsforAliasSearch(String searchWord, String mergeId);

    public void mergeNpSkill(AddNpSKillsForm addNpSkillsForm);

    public void deletNpSkills(String skillId);

    List getFilteredNpSkills(String sortBy, String skillTypeFilter,
                             String validFilter, String domainFiter, String pageNumber);

    public void deleteCoAliasSkills(String aliasId);

    public Set<CoSkillAlias> getAliases(final String id);
}
