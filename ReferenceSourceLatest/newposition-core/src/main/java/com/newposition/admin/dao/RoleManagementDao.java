package com.newposition.admin.dao;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;

public interface RoleManagementDao
{
    public List<NpDomainImpl> getDomainTypes();

    public List<CoSource> getcoSources();

    public List<String> getNpEmploymentRoleNames();

    public void saveNpEmploymentRoles(AddNpEmploymentRoleForm addNpEmploymentRoleForm);

    public Set<NpEmploymentRoleImpl> getNpEmploymentRoleforAliasSearch(
            String searchWord, String mergeId);

    public void mergeNpEmploymentRole(AddNpEmploymentRoleForm addNpEmploymentRoleForm);

    public void deletNpEmploymentRoles(String roleId);

    List getFilteredNpEmploymentRoles(String sortBy,
                                      String roleNameFilter, String validFilter,
                                      String domainTypeIDFiter, String pageNumber);

    public void deleteCoAliasRoles(String aliasId);

    public Set<CoRoleAlias> getAliases(final String id);
}
