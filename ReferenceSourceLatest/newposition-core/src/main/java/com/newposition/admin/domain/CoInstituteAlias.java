package com.newposition.admin.domain;

import java.util.Date;

import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoSource;

public interface CoInstituteAlias
{

    public NpInstitutions getNpInstitutions();

    public void setNpInstitutions(NpInstitutions npInstitutions);

    public String getInstituteAliasName();

    public void setInstituteAliasName(String instituteAliasName);

    public Integer getInstituteAliasID();

    public void setInstituteAliasID(Integer instituteAliasID);

    public CoSource getCoSource();

    public void setCoSource(CoSource coSource);

    public String getStatus();

    public void setStatus(String status);

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
