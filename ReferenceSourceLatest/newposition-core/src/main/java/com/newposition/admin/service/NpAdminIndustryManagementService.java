package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.domain.CoEmployerAliasImpl;
import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpSectorImpl;

public interface NpAdminIndustryManagementService
{

    public List<NpSectorImpl> findAllIndustries();

    public Set<String> getIndustryTypes();

    public void saveNpIndustries(AddNpIndustriesForm addNpIndustriesForm);

    public List<CoSource> getcoSources();

    public Set<NpEmployerImpl> getNpIndustriesforAliasSearch(String searchWord, String mergeId);

    public void mergeNpIndustry(AddNpIndustriesForm addNpIndustriesForm);

    public void deletNpIndustries(String industryId);

    public List getFilteredNpIndustries(String sortBy, String industryTypeFilter,
                                        String validFilter, String domainFiter, String pageNumber);

    public void deleteCoAliasIndustries(String aliasId);

    public Set<CoEmployerAliasImpl> getAliases(final String id);
}
