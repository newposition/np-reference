package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.domain.CoInstituteAliasImpl;
import com.newposition.admin.forms.AddNpInstitutionForm;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoSource;

public interface NpAdminInstitutionManagementService
{

    public Set<String> getInstituteNames(List<NpInstitutions> npInstitutions);

    public Set<String> getInstituteTypes(List<NpInstitutions> npInstitutions);

    public void saveNpInstitute(AddNpInstitutionForm addNpInstitutionForm);

    public Set<CoSource> getCoSources();

    public Set<NpInstitutions> getNpInstituteforAliasSearch(final String searchWord, String mergeId);

    public void removeCoInstituteAlias(final int coInstituteAliasId);

    public NpInstitutions getNpInstituteById(final int npInstituteId);

    public void saveMergedNpInstitute(AddNpInstitutionForm addNpInstitutionForm);

    public void removeNpInstituteById(final int npInstituteId);

    public List getFilteredNpInstitutes(String sortBy, String instituteTypeFilter,
                                        String validFilter, String instituteFilter, String pageNumber);

    public Set<CoInstituteAliasImpl> getAliases(final String id);
}
