package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;

public interface RoleManagementService
{

    public Set<NpDomainImpl> getDomainTypes();

    public Set<CoSource> getcoSources();

    public Set<String> getNpEmploymentRoleNames();

    public void saveNpEmploymentRoles(AddNpEmploymentRoleForm addNpEmploymentRoleForm);

    public Set<NpEmploymentRoleImpl> getNpEmploymentRoleforAliasSearch(String searcWord, String mergeId);

    public void mergeNpEmploymentRole(AddNpEmploymentRoleForm addNpEmploymentRoleForm);

    public void deletNpEmploymentRoles(String roleId);

    public List getFilteredNpEmploymentRoles(String sortBy,
                                             String skillTypeFilter, String validFilter, String domainFiter,
                                             String pageNumber);

    public void deleteCoAliasRoles(String aliasId);

    public Set<CoRoleAlias> getAliases(final String id);
}
