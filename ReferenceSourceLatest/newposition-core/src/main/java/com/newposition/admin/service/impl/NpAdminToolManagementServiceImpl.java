package com.newposition.admin.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import com.newposition.admin.dao.NpAdminToolManagementDao;
import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.admin.service.NpAdminToolManagementService;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpToolImpl;

public class NpAdminToolManagementServiceImpl implements NpAdminToolManagementService
{

    private NpAdminToolManagementDao npAdminToolManagementDao;

    public void setNpAdminToolManagementDao(
            NpAdminToolManagementDao npAdminToolManagementDao)
    {
        this.npAdminToolManagementDao = npAdminToolManagementDao;
    }

    @Override
    public List getFilteredNpTools(String sortBy, String skillTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {

        return npAdminToolManagementDao.getFilteredNpTools(sortBy, skillTypeFilter, validFilter, domainFiter, pageNumber);
    }

    @Override
    public Set<String> getToolTypes()
    {

        List<String> toolTypesList = npAdminToolManagementDao.getToolTypes();
        Set<String> toolTypes = new HashSet<String>(toolTypesList);
        return toolTypes;
    }

    @Transactional
    @Override
    public void saveNpTools(AddNpToolsForm addNpToolsForm)
    {

        npAdminToolManagementDao.saveNpTools(addNpToolsForm);
    }

    @Override
    public Set<CoSource> getcoSources()
    {
        List<CoSource> coSourceList = npAdminToolManagementDao.getcoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpToolImpl> getNpToolsforAliasSearch(String searchWord, String mergeId)
    {

        return npAdminToolManagementDao.getNpToolsforAliasSearch(searchWord, mergeId);
    }

    @Transactional
    @Override
    public void mergeNpTool(AddNpToolsForm addNpToolsForm)
    {

        npAdminToolManagementDao.mergeNpTool(addNpToolsForm);
    }

    @Transactional
    @Override
    public void deletNpTools(String toolId)
    {

        npAdminToolManagementDao.deletNpTools(toolId);
    }

    @Transactional
    @Override
    public void deleteCoAliasTools(String aliasId)
    {
        // TODO Auto-generated method stub
        npAdminToolManagementDao.deleteCoAliasTools(aliasId);
    }

    public Set<CoToolAlias> getAliases(final String id)
    {
        return npAdminToolManagementDao.getAliases(id);
    }
}
