package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Yamuna Vemula
 */
public class AddNpInstitutionForm
{

    public Integer instituteId;
    public String instituteType;
    public String instituteName;
    public boolean valid;
    public List<CertificationAliases> certificationAliases;
    public String mergeId;

    public Integer getInstituteId()
    {
        return instituteId;
    }

    public void setInstituteId(Integer instituteId)
    {
        this.instituteId = instituteId;
    }

    public String getInstituteType()
    {
        return instituteType;
    }

    public void setInstituteType(String instituteType)
    {
        this.instituteType = instituteType;
    }

    public String getInstituteName()
    {
        return instituteName;
    }

    public void setInstituteName(String instituteName)
    {
        this.instituteName = instituteName;
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(
            List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public String getMergeId()
    {
        return mergeId;
    }

    public void setMergeId(String mergeId)
    {
        this.mergeId = mergeId;
    }
}
