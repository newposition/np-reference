package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Trinadh
 */
public class AddNpEmploymentRoleForm
{

    public String roleId;
    public String domainId;
    public String roleName;
    public String valid;
    public List<CertificationAliases> certificationAliases;
    public String mergeToId;

    public boolean deleteRole;

    public String getRoleId()
    {
        return roleId;
    }

    public String getDomainId()
    {
        return domainId;
    }

    public String getRoleName()
    {
        return roleName;
    }

    public String getValid()
    {
        return valid;
    }

    public String getMergeToId()
    {
        return mergeToId;
    }

    public boolean isDeleteRole()
    {
        return deleteRole;
    }

    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }

    public void setDomainId(String domainId)
    {
        this.domainId = domainId;
    }

    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }

    public void setValid(String valid)
    {
        this.valid = valid;
    }

    public void setMergeToId(String mergeToId)
    {
        this.mergeToId = mergeToId;
    }

    public void setDeleteRole(boolean deleteRole)
    {
        this.deleteRole = deleteRole;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(
            List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }
}
