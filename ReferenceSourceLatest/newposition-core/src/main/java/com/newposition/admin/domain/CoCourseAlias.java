package com.newposition.admin.domain;

import java.util.Date;

import com.newposition.candidate.domain.NpCourse;
import com.newposition.common.domain.CoSource;

public interface CoCourseAlias
{

    public NpCourse getNpCourse();

    public void setNpCourse(NpCourse npCourse);

    public String getCourseAliasName();

    public void setCourseAliasName(String courseAliasName);

    public Integer getCourseAliasID();

    public void setCourseAliasID(Integer courseAliasID);

    public CoSource getCoSource();

    public void setCoSource(CoSource coSource);

    public String getStatus();

    public void setStatus(String status);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
