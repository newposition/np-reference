package com.newposition.admin.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.newposition.admin.dao.QualificationManagementDao;
import com.newposition.admin.domain.CoCourseAliasImpl;
import com.newposition.admin.forms.AddNpQualificationForm;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoSource;

@Service
public class QualificationManagementServiceImpl implements QualificationManagementService
{

    private QualificationManagementDao qualificationManagementDao;

    public QualificationManagementDao getQualificationManagementDao()
    {
        return qualificationManagementDao;
    }

    public void setQualificationManagementDao(
            QualificationManagementDao qualificationManagementDao)
    {
        this.qualificationManagementDao = qualificationManagementDao;
    }

    @Override
    public List getFilteredNpInstituteCourse(String sortBy,
                                             String courseTypeFilter, String validFilter, String instituteFilter, String pageNumber)
    {

        return qualificationManagementDao.getFilteredNpInstituteCourse(sortBy, courseTypeFilter, validFilter, instituteFilter, pageNumber);
    }

    @Override
    public Set<NpInstitutions> getNpInstitutions()
    {

        List<NpInstitutions> npInstitutionsList = qualificationManagementDao.getNpInstitutions();
        Set<NpInstitutions> npInstitutions = new HashSet<NpInstitutions>(npInstitutionsList);
        return npInstitutions;
    }

    @Override
    public Set<String> getCourseTypes()
    {

        List<String> courseTypesList = qualificationManagementDao.getCourseTypes();
        Set<String> courseTypes = new HashSet<String>(courseTypesList);
        return courseTypes;
    }

    @Override
    public void saveNpInstituteCourse(
            AddNpQualificationForm addNpQualificationForm)
    {
        qualificationManagementDao.saveNpInstituteCourse(addNpQualificationForm);
    }

    @Override
    public Set<CoSource> getCoSources()
    {
        List<CoSource> coSourceList = qualificationManagementDao.getCoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpInstituteCourse> getNpInstituteCourseforAliasSearch(
            String searchWord, String mergeId)
    {

        return qualificationManagementDao.getNpInstituteCourseforAliasSearch(searchWord, mergeId);
    }

    @Transactional
    @Override
    public void removeCoCourseAlias(int coCertificationAliasId)
    {
        qualificationManagementDao.removeCoCourseAlias(coCertificationAliasId);
    }

    @Override
    public NpInstituteCourse getNpInstituteCourseById(int npInstituteCourseId)
    {

        return qualificationManagementDao.getNpInstituteCourseById(npInstituteCourseId);
    }

    @Override
    public void saveMergedNpInstituteCourse(
            AddNpQualificationForm addNpQualificationForm)
    {
        qualificationManagementDao.saveMergedNpInstituteCourse(addNpQualificationForm);
    }

    @Override
    public void removeNpInstituteCourseById(int npInstituteCourseId)
    {

        qualificationManagementDao.removeNpInstituteCourseById(npInstituteCourseId);
    }

    public Set<CoCourseAliasImpl> getAliases(final String id)
    {
        return qualificationManagementDao.getAliases(id);
    }
}
