package com.newposition.admin.service.impl;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import com.newposition.admin.dao.NpAdminIndustryManagementDao;
import com.newposition.admin.domain.CoEmployerAliasImpl;
import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.admin.service.NpAdminIndustryManagementService;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpSectorImpl;

public class NpAdminIndustryManagementServiceImpl implements NpAdminIndustryManagementService
{

    private NpAdminIndustryManagementDao npAdminIndustryManagementDao;

    public void setNpAdminIndustryManagementDao(NpAdminIndustryManagementDao npAdminIndustryManagementDao)
    {
        this.npAdminIndustryManagementDao = npAdminIndustryManagementDao;
    }

    @Override
    public List<NpSectorImpl> findAllIndustries()
    {
        // TODO Auto-generated method stub
        return npAdminIndustryManagementDao.findAll();
    }

    @Override
    public List getFilteredNpIndustries(String sortBy,
                                        String industryTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        // TODO Auto-generated method stub
        return npAdminIndustryManagementDao.getFilteredNpEmployers(sortBy, industryTypeFilter, validFilter, domainFiter, pageNumber);
    }

    @Override
    public Set<String> getIndustryTypes()
    {
        // TODO Auto-generated method stub
        return npAdminIndustryManagementDao.getIndustryTypes();
    }

    @Override
    @Transactional
    public void saveNpIndustries(AddNpIndustriesForm addNpIndustriesForm)
    {
        // TODO Auto-generated method stub
        npAdminIndustryManagementDao.saveNpIndustries(addNpIndustriesForm);
    }

    @Override
    public List<CoSource> getcoSources()
    {
        // TODO Auto-generated method stub
        return npAdminIndustryManagementDao.getcoSources();
    }

    @Override
    public Set<NpEmployerImpl> getNpIndustriesforAliasSearch(String searchWord, String mergeId)
    {
        // TODO Auto-generated method stub
        return npAdminIndustryManagementDao.getNpEmployersforAliasSearch(searchWord, mergeId);
    }

    @Override
    @Transactional
    public void mergeNpIndustry(AddNpIndustriesForm addNpIndustriesForm)
    {
        // TODO Auto-generated method stub
        npAdminIndustryManagementDao.mergeNpEmployer(addNpIndustriesForm);
    }

    @Override
    @Transactional
    public void deletNpIndustries(String industryId)
    {
        // TODO Auto-generated method stub
        npAdminIndustryManagementDao.deleteNpEmployer(industryId);
    }

    @Transactional
    @Override
    public void deleteCoAliasIndustries(String aliasId)
    {

        npAdminIndustryManagementDao.deleteCoAliasEmployers(aliasId);
    }

    public Set<CoEmployerAliasImpl> getAliases(final String id)
    {
        return npAdminIndustryManagementDao.getAliases(id);
    }
}
