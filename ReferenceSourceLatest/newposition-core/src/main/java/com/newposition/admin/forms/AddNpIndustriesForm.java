package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Ravi
 */
public class AddNpIndustriesForm
{

    public String industryId;
    public String sectorId;
    public String industryName;
    public String industryType;
    private List<CertificationAliases> certificationAliases;
    private String mergeToId;
    private boolean deleteIndustry;
    public String valid;

    public String getIndustryId()
    {
        return industryId;
    }

    public void setIndustryId(String industryId)
    {
        this.industryId = industryId;
    }

    public String getIndustryName()
    {
        return industryName;
    }

    public void setIndustryName(String industryName)
    {
        this.industryName = industryName;
    }

    public String getSectorId()
    {
        return sectorId;
    }

    public void setSectorId(String sectorId)
    {
        this.sectorId = sectorId;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public String getMergeToId()
    {
        return mergeToId;
    }

    public void setMergeToId(String mergeToId)
    {
        this.mergeToId = mergeToId;
    }

    public boolean isDeleteIndustry()
    {
        return deleteIndustry;
    }

    public void setDeleteIndustry(boolean deleteIndustry)
    {
        this.deleteIndustry = deleteIndustry;
    }

    public String getValid()
    {
        return valid;
    }

    public void setValid(String valid)
    {
        this.valid = valid;
    }

    public String getIndustryType()
    {
        return industryType;
    }

    public void setIndustryType(String industryType)
    {
        this.industryType = industryType;
    }
}
