package com.newposition.admin.dao;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.common.dao.GenericDao;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpToolImpl;

public interface NpAdminToolManagementDao extends GenericDao<NpToolImpl, Integer>
{

    public List<String> getToolTypes();

    public void saveNpTools(AddNpToolsForm addNpToolsForm);

    public List<CoSource> getcoSources();

    public Set<NpToolImpl> getNpToolsforAliasSearch(String searchWord, String mergeId);

    public void mergeNpTool(AddNpToolsForm addNpToolsForm);

    public void deletNpTools(String toolId);

    public List getFilteredNpTools(String sortBy, String toolTypeFilter,
                                   String validFilter, String domainFiter, String pageNumber);

    public void deleteCoAliasTools(String aliasId);

    public Set<CoToolAlias> getAliases(final String id);
}
