package com.newposition.admin.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.newposition.admin.dao.RoleManagementDao;
import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.service.RoleManagementService;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;

@Service
public class RoleManagementServiceImpl implements RoleManagementService
{

    private RoleManagementDao roleManagementDao;

    public RoleManagementDao getRoleManagementDao()
    {
        return roleManagementDao;
    }

    public void setRoleManagementDao(RoleManagementDao roleManagementDao)
    {
        this.roleManagementDao = roleManagementDao;
    }

    @Override
    public List getFilteredNpEmploymentRoles(String sortBy,
                                             String skillTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        return getRoleManagementDao().getFilteredNpEmploymentRoles(sortBy, skillTypeFilter, validFilter, domainFiter, pageNumber);
    }

    @Override
    public Set<NpDomainImpl> getDomainTypes()
    {
        List<NpDomainImpl> npDomainTypesList = getRoleManagementDao().getDomainTypes();
        Set<NpDomainImpl> npDomainTypes = new HashSet<NpDomainImpl>(npDomainTypesList);
        return npDomainTypes;
    }

    @Override
    public Set<String> getNpEmploymentRoleNames()
    {
        List<String> roleNamesList = getRoleManagementDao().getNpEmploymentRoleNames();
        Set<String> roleNames = new HashSet<String>(roleNamesList);
        return roleNames;
    }

    @Override
    public void saveNpEmploymentRoles(AddNpEmploymentRoleForm addNpEmploymentRoleForm)
    {
        getRoleManagementDao().saveNpEmploymentRoles(addNpEmploymentRoleForm);
    }

    @Override
    public Set<CoSource> getcoSources()
    {
        List<CoSource> coSourceList = getRoleManagementDao().getcoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpEmploymentRoleImpl> getNpEmploymentRoleforAliasSearch(final String searcWord, String mergeId)
    {
        Set<NpEmploymentRoleImpl> npEmploymentRoleImpl = getRoleManagementDao().getNpEmploymentRoleforAliasSearch(searcWord, mergeId);
        return npEmploymentRoleImpl;
    }

    @Override
    public void mergeNpEmploymentRole(AddNpEmploymentRoleForm addNpEmploymentRoleForm)
    {
        getRoleManagementDao().mergeNpEmploymentRole(addNpEmploymentRoleForm);
    }

    @Override
    public void deletNpEmploymentRoles(String roleId)
    {
        getRoleManagementDao().deletNpEmploymentRoles(roleId);
    }

    @Override
    public void deleteCoAliasRoles(String aliasId)
    {
        getRoleManagementDao().deleteCoAliasRoles(aliasId);
    }

    public Set<CoRoleAlias> getAliases(final String id)
    {
        return getRoleManagementDao().getAliases(id);
    }
}
