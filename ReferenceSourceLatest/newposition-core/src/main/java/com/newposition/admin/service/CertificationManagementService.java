package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpCertificationForm;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoCertificationAliasImpl;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpInstituteCertificate;
import com.newposition.common.domain.NpInstituteCertificateImpl;

public interface CertificationManagementService
{

    public Set<NpInstitutions> getNpInstitutions();

    public Set<String> getCertificationTypes();

    public void saveNpCertifications(AddNpCertificationForm addNpCertificationForm);

    public Set<CoSource> getCoSources();

    public Set<NpInstituteCertificateImpl> getNpInstituteCertificateforAliasSearch(final String searchWord, String mergeId);

    public void removeCoCertificateAlias(final int coCertificationAliasId);

    public NpInstituteCertificate getNpInstituteCertificateById(final int npInstituteCertificateId);

    public void saveMergedNpInstituteCertificate(AddNpCertificationForm addNpCertificationForm);

    public void removeNpInstituteCertificateById(final int npInstituteCertificateId);

    public List getFilteredNpInstituteCertificate(String sortBy,
                                                  String certificationTypeFilter, String validFilter,
                                                  String domainFiter, String pageNumber);

    public Set<CoCertificationAliasImpl> getAliases(final String id);
}
