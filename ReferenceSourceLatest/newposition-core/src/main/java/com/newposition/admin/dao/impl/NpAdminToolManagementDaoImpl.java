package com.newposition.admin.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.admin.dao.NpAdminToolManagementDao;
import com.newposition.admin.domain.CoInstituteAlias;
import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.forms.ToolAliases;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpToolImpl;

public class NpAdminToolManagementDaoImpl implements NpAdminToolManagementDao
{

    private HibernateTemplate hibernateTemplate;

    private int numberOfReusltsPerPage;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public NpToolImpl save(NpToolImpl entity)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void merge(NpToolImpl entity)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(NpToolImpl entity)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public List<NpToolImpl> findAll()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public NpToolImpl findByID(Integer id)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpTools(String sortBy, String toolTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {

        List npToolsResult = new ArrayList();

        String query = "from NpToolImpl";

        if (StringUtils.isNotBlank(toolTypeFilter) || StringUtils.isNotBlank(validFilter) || (StringUtils.isNotBlank(domainFiter) && Integer.parseInt(domainFiter) != 0))
        {
            query = "from NpToolImpl where ";
            if (StringUtils.isNotBlank(toolTypeFilter))
            {
                if (StringUtils.isBlank(validFilter) || Integer.parseInt(domainFiter) == 0)
                {
                    query = query + "toolType='" + toolTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "toolType='" + toolTypeFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(domainFiter) || Integer.parseInt(domainFiter) == 0)
                {
                    query = query + "valid='" + validFilter + "'";
                }
                else
                {
                    query = query + "valid='" + validFilter + "'" + " " + "and ";
                }
            }
            if ((StringUtils.isNotBlank(domainFiter) && Integer.parseInt(domainFiter) != 0))
            {
                query = query + "domainTypeid=" + domainFiter;
            }
            query = query + " and status=true";
        }
        else
        {
            query = query + " where status=true";
        }
        if (StringUtils.isNotBlank(sortBy))
        {
            if (sortBy.equalsIgnoreCase("DomainTypeId"))
            {
                query = query + "  order by " + "npDomain.typeName";
            }
            else
            {
                query = query + "  order by " + sortBy;
            }
        }

//			npTools=(List<NpToolImpl>)hibernateTemplate.find(query);

        Long npToolsCount = (Long) hibernateTemplate.find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npToolsResult.add(getPaginatedresult(query, pageNo));
        npToolsResult.add((int) npToolsCount.longValue());

        return npToolsResult;
    }

    @SuppressWarnings("unchecked")
    private List<NpToolImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpToolImpl>) hibernateTemplate.execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getToolTypes()
    {
        List<String> nptoolTypes = (List<String>) hibernateTemplate.find("select toolType from NpToolImpl where toolType != null");
        return nptoolTypes;
    }

    @Override
    public void saveNpTools(AddNpToolsForm addNpToolsForm)
    {
        // TODO Auto-generated method stub
        NpToolImpl npTools = null;
        if (StringUtils.isNotBlank(addNpToolsForm.getToolId()))
        {
            npTools = (NpToolImpl) hibernateTemplate.find("from NpToolImpl where id=?", Integer.parseInt(addNpToolsForm.getToolId())).get(0);

			/*if(npTools.getCoToolAlias()!=null)
            {
				for(CoToolAlias coAlias:npTools.getCoToolAlias())
				{
					hibernateTemplate.delete(coAlias);
				}
			}*/
        }
        else
        {
            npTools = new NpToolImpl();
        }

        npTools.setToolName(addNpToolsForm.getToolName());
        npTools.setCreatedDate(new Date());
        NpDomainImpl npDomainTypes = (NpDomainImpl) hibernateTemplate.find("from NpDomainImpl where id=?", Integer.parseInt(addNpToolsForm.getDomainId())).get(0);
        if (npDomainTypes != null)
        {
            npTools.setNpDomain(npDomainTypes);
        }
        npTools.setValid(addNpToolsForm.getValid());

        npTools.setToolType(addNpToolsForm.getToolType());

        npTools.setStatus(true);

        hibernateTemplate.saveOrUpdate(npTools);

        if (addNpToolsForm.getCertificationAliases() != null)
        {
            for (CertificationAliases toolAlias : addNpToolsForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(toolAlias.getAliasSource()))
                {
                    CoToolAlias coToolAlias = null;

                    if (StringUtils.isNotBlank(toolAlias.getAliasId()))
                    {
                        coToolAlias = (CoToolAlias) hibernateTemplate.find("from CoToolAlias where toolAliasID=?", Integer.parseInt(toolAlias.getAliasId())).get(0);
                    }
                    else
                    {
                        coToolAlias = new CoToolAlias();
                    }
                    coToolAlias.setToolAliasName(toolAlias.getAliasName());
                    CoSource coSource = (CoSource) hibernateTemplate.find("from CoSource where sourceID=?", Integer.parseInt(toolAlias.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coToolAlias.setCoSource(coSource);
                    }
                    coToolAlias.setNpToolImpl(npTools);
                    hibernateTemplate.saveOrUpdate(coToolAlias);
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<CoSource> getcoSources()
    {

        List<CoSource> coSources = (List<CoSource>) hibernateTemplate.find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpToolImpl> getNpToolsforAliasSearch(String searchWord, String mergeId)
    {
        // TODO Auto-generated method stub
        List<CoToolAlias> coToolAliasList = (List<CoToolAlias>) hibernateTemplate.find("from CoToolAlias where toolAliasName like '" + searchWord + "%'");
        Set<NpToolImpl> npToolSet = new HashSet<NpToolImpl>();
        Integer merge = Integer.parseInt(mergeId);
        if (coToolAliasList != null)
        {
            for (CoToolAlias coToolAlias : coToolAliasList)
            {
                NpToolImpl npToolImpl = coToolAlias.getNpToolImpl();
                if (npToolImpl.isStatus() && npToolImpl.getId().intValue() != merge.intValue())
                {
                    npToolSet.add(npToolImpl);
                }
            }
        }
        List<NpToolImpl> npToolsList = (List<NpToolImpl>) hibernateTemplate.find("from NpToolImpl where toolName like '" + searchWord + "%'");
        if (npToolsList != null)
        {
            for (NpToolImpl npToolImpl : npToolsList)
            {
                if (npToolImpl.isStatus() && npToolImpl.getId().intValue() != merge.intValue())
                {
                    npToolSet.add(npToolImpl);
                }
            }
        }
        return npToolSet;
    }

    @Override
    public void mergeNpTool(AddNpToolsForm addNpToolsForm)
    {
        boolean isExists = true;
        NpToolImpl npTools = (NpToolImpl) hibernateTemplate.find("from NpToolImpl where id=?", Integer.parseInt(addNpToolsForm.getMergeToId())).get(0);
        NpToolImpl npToolsLatest = (NpToolImpl) hibernateTemplate.find("from NpToolImpl where id=?", Integer.parseInt(addNpToolsForm.getToolId())).get(0);

        if (npTools != null)
        {
            if (npTools.getToolName().equalsIgnoreCase(npToolsLatest.getToolName()))
            {
                isExists = false;
            }

            npTools.setStatus(false);

            hibernateTemplate.saveOrUpdate(npTools);
            //updating references of npSkills in npCandidateSkills to npSkillsLatest
            Set<NpCandidateTools> npCandidateTools = npTools.getNpCandidateTools();
            for (NpCandidateTools npCandidateTool : npCandidateTools)
            {
                System.out.println("npCandidateTool (" + npCandidateTool.getId() + ")" + "nptool id before change" + npCandidateTool.getNpTools().getId());

                npCandidateTool.setNpTools(npToolsLatest);
                hibernateTemplate.saveOrUpdate(npCandidateTool);
                System.out.println("npCandidateTool (" + npCandidateTool.getId() + ")" + "nptool id after#### change" + npCandidateTool.getNpTools().getId());
            }

            for (CoToolAlias coToolAlias : npTools.getCoToolAlias())
            {

                if (coToolAlias.getToolAliasName().equalsIgnoreCase(npTools.getToolName()))
                {
                    isExists = false;
                }
                for (CoToolAlias coToolAliasLatest : npToolsLatest.getCoToolAlias())
                {
                    if (coToolAliasLatest.getToolAliasName().equalsIgnoreCase(npTools.getToolName()))
                    {
                        isExists = false;
                    }
                    if (!coToolAliasLatest.getToolAliasName().equalsIgnoreCase(coToolAlias.getToolAliasName()))
                    {
                        coToolAlias.setNpToolImpl(npToolsLatest);
                        hibernateTemplate.saveOrUpdate(coToolAlias);
                    }
                }
            }
            if (isExists)
            {
                CoToolAlias coToolAlias = new CoToolAlias();
                CoSource coSource = new CoSource();
                coSource.setSourceID(4);
                coToolAlias.setCoSource(coSource);
                coToolAlias.setToolAliasName(npTools.getToolName());
                coToolAlias.setNpToolImpl(npToolsLatest);
                hibernateTemplate.saveOrUpdate(coToolAlias);
            }
        }
    }

    @Override
    public void deletNpTools(String toolId)
    {
        NpToolImpl npTools = (NpToolImpl) hibernateTemplate.find("from NpToolImpl where id=?", Integer.parseInt(toolId)).get(0);

        if (npTools != null)
        {
            npTools.setStatus(false);
            hibernateTemplate.saveOrUpdate(npTools);
        }
    }

    @Override
    public void deleteCoAliasTools(String aliasId)
    {

        CoToolAlias coRoleAlias = (CoToolAlias) hibernateTemplate.find("from CoToolAlias where id=?", Integer.parseInt(aliasId)).get(0);

        if (coRoleAlias != null)
        {
            hibernateTemplate.delete(coRoleAlias);
        }
    }

    @Override
    public Set<CoToolAlias> getAliases(final String id)
    {

        NpToolImpl npToolImpl = hibernateTemplate.get(NpToolImpl.class, Integer.parseInt(id));
        if (npToolImpl != null)
        {
            return npToolImpl.getCoToolAlias();
        }
        return null;
    }
}
