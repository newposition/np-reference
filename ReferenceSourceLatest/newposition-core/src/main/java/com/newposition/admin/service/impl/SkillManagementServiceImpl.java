package com.newposition.admin.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.newposition.admin.dao.SkillManagementDao;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpSkillImpl;

@Service
public class SkillManagementServiceImpl implements SkillManagementService
{

    private SkillManagementDao skillManagementDao;

    public SkillManagementDao getSkillManagementDao()
    {
        return skillManagementDao;
    }

    public void setSkillManagementDao(SkillManagementDao skillManagementDao)
    {
        this.skillManagementDao = skillManagementDao;
    }

    @Override
    public List getFilteredNpSkills(String sortBy,
                                    String skillTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        return getSkillManagementDao().getFilteredNpSkills(sortBy, skillTypeFilter, validFilter, domainFiter, pageNumber);
    }

    @Override
    public Set<NpDomainImpl> getDomainTypes()
    {
        List<NpDomainImpl> npDomainTypesList = getSkillManagementDao().getDomainTypes();
        Set<NpDomainImpl> npDomainTypes = new HashSet<NpDomainImpl>(npDomainTypesList);
        return npDomainTypes;
    }

    @Override
    public Set<String> getSkillTypes()
    {
        List<String> skillTypesList = getSkillManagementDao().getSkillTypes();
        Set<String> skillTypes = new HashSet<String>(skillTypesList);
        return skillTypes;
    }

    @Override
    public void saveNpSkills(AddNpSKillsForm addNpSKillsForm)
    {
        skillManagementDao.saveNpSkills(addNpSKillsForm);
    }

    @Override
    public Set<CoSource> getcoSources()
    {
        List<CoSource> coSourceList = getSkillManagementDao().getcoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpSkillImpl> getNpSKillsforAliasSearch(final String searcWord, String mergeId)
    {
        Set<NpSkillImpl> npSkills = skillManagementDao.getNpSKillsforAliasSearch(searcWord, mergeId);
        return npSkills;
    }

    @Override
    public void mergeNpSkill(AddNpSKillsForm addNpSkillsForm)
    {
        skillManagementDao.mergeNpSkill(addNpSkillsForm);
    }

    @Override
    public void deletNpSkills(String skillId)
    {
        skillManagementDao.deletNpSkills(skillId);
    }

    @Transactional
    @Override
    public void deleteCoAliasSkills(String aliasId)
    {

        skillManagementDao.deleteCoAliasSkills(aliasId);
    }

    public Set<CoSkillAlias> getAliases(final String id)
    {
        return skillManagementDao.getAliases(id);
    }
}
