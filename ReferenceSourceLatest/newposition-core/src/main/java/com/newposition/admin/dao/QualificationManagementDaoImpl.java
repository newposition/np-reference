package com.newposition.admin.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.domain.CoCourseAlias;
import com.newposition.admin.domain.CoCourseAliasImpl;
import com.newposition.admin.forms.AddNpQualificationForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCandidateEducation;
import com.newposition.candidate.domain.NpCertifications;
import com.newposition.candidate.domain.NpCourse;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoCertificationAliasImpl;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpInstituteCertificateImpl;

@Repository
public class QualificationManagementDaoImpl implements QualificationManagementDao
{

    protected static final Logger LOG = Logger.getLogger(QualificationManagementDaoImpl.class);

    private HibernateTemplate hibernateTemplate;
    private int numberOfReusltsPerPage;

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpInstituteCourse(String sortBy,
                                             String courseTypeFilter, String validFilter, String instituteFilter, String pageNumber)
    {
        List npInstituteCoursesResult = new ArrayList();

        String query = "from com.newposition.candidate.domain.NpInstituteCourse IC where IC.status=true  ";

        if (StringUtils.isNotBlank(courseTypeFilter) || StringUtils.isNotBlank(validFilter) || StringUtils.isNotBlank(instituteFilter))
        {
            query = "from com.newposition.candidate.domain.NpInstituteCourse IC where IC.status=true and ";
            if (StringUtils.isNotBlank(courseTypeFilter))
            {
                if (StringUtils.isBlank(validFilter) && StringUtils.isBlank(instituteFilter))
                {
                    query = query + "IC.npCourse.courseType='" + courseTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "IC.npCourse.courseType='" + courseTypeFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(instituteFilter))
                {
                    query = query + "IC.npCourse.valid=" + validFilter;
                }
                else
                {
                    query = query + "IC.npCourse.valid=" + validFilter + " " + "and ";
                }
            }
            if (StringUtils.isNotBlank(instituteFilter))
            {
                query = query + "IC.npInstitutions.id=" + instituteFilter;
            }
        }

        if (StringUtils.isNotBlank(sortBy) && !sortBy.equals("instituteName"))
        {
            query = query + "  order by IC.npCourse." + sortBy;
        }
        else if (StringUtils.isNotBlank(sortBy) && sortBy.equals("instituteName"))
        {
            query = query + "  order by IC.npInstitutions." + sortBy;
        }

//		npInstituteCourses=(List<NpInstituteCourse>) getHibernateTemplate().find(query);
        Long npInstituteCoursesCount = (Long) getHibernateTemplate().find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npInstituteCoursesResult.add(getPaginatedresult(query, pageNo));
        npInstituteCoursesResult.add((int) npInstituteCoursesCount.longValue());

        return npInstituteCoursesResult;
    }

    @SuppressWarnings("unchecked")
    private List<NpEmploymentRoleImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpEmploymentRoleImpl>) getHibernateTemplate().execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpInstitutions> getNpInstitutions()
    {

        List<NpInstitutions> npInstitutions = (List<NpInstitutions>) getHibernateTemplate().find("from NpInstitutions");
        return npInstitutions;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getCourseTypes()
    {
        List<String> npCourseTypes = (List<String>) getHibernateTemplate().find("select courseType from NpCourse where courseType != null");
        return npCourseTypes;
    }

    @Transactional
    @Override
    public void saveNpInstituteCourse(AddNpQualificationForm addNpQualificationForm)
    {

        NpInstituteCourse npInstituteCourse = null;
        NpCourse npCourse = null;
        if (addNpQualificationForm.getNpInstituteCourseId() != null && StringUtils.isNotBlank(addNpQualificationForm.getNpInstituteCourseId().toString()))
        {
            npInstituteCourse = (NpInstituteCourse) getHibernateTemplate().find("from com.newposition.candidate.domain.NpInstituteCourse where id=?", addNpQualificationForm.getNpInstituteCourseId()).get(0);
            npCourse = npInstituteCourse.getNpCourse();
        }
        else
        {
            npInstituteCourse = new NpInstituteCourse();
            npCourse = new NpCourse();
        }

        npCourse.setCourseName(addNpQualificationForm.getCourseName());
        npCourse.setCreatedDate(new Date());
        NpInstitutions npInstitutions = (NpInstitutions) getHibernateTemplate().find("from NpInstitutions where id=?", addNpQualificationForm.getInstitutionId()).get(0);

        npCourse.setValid(addNpQualificationForm.isValid());
        npCourse.setCourseType(addNpQualificationForm.getCourseType());
        getHibernateTemplate().saveOrUpdate(npCourse);
        if (npInstitutions != null)
        {
            npInstituteCourse.setNpCourse(npCourse);
            ;
            npInstituteCourse.setNpInstitutions(npInstitutions);
            npInstituteCourse.setStatus(true);
        }
        getHibernateTemplate().saveOrUpdate(npInstituteCourse);
        if (addNpQualificationForm.getCertificationAliases() != null)
        {
            for (CertificationAliases certificationAliases : addNpQualificationForm.getCertificationAliases())
            {
                Set<CoCourseAliasImpl> coCourseAliases = new HashSet<CoCourseAliasImpl>();
                if (StringUtils.isNotBlank(certificationAliases.getAliasSource()))
                {
                    CoCourseAliasImpl coCourseAlias = null;

                    if (StringUtils.isNotBlank(certificationAliases.getAliasId()))
                    {
                        coCourseAlias = (CoCourseAliasImpl) getHibernateTemplate().find("from com.newposition.admin.domain.CoCourseAlias where courseAliasID=?", Integer.parseInt(certificationAliases.getAliasId())).get(0);
                    }
                    else
                    {
                        coCourseAlias = new CoCourseAliasImpl();
                    }
                    coCourseAlias.setCourseAliasName(certificationAliases.getAliasName());
                    CoSource coSource = (CoSource) getHibernateTemplate().find("from CoSource where sourceID=?", Integer.parseInt(certificationAliases.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coCourseAlias.setCoSource(coSource);
                    }
                    coCourseAlias.setNpCourse(npCourse);
                    coCourseAliases.add(coCourseAlias);
                    getHibernateTemplate().saveOrUpdate(coCourseAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getCoSources()
    {

        List<CoSource> coSources = (List<CoSource>) getHibernateTemplate().find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpInstituteCourse> getNpInstituteCourseforAliasSearch(final String searchWord, String mergeId)
    {
        Set<NpInstituteCourse> npInstituteCourseSet = new HashSet<NpInstituteCourse>();
        Integer merge = Integer.parseInt(mergeId);
        List<CoCourseAlias> coCourseAliasList = (List<CoCourseAlias>) getHibernateTemplate().find("from com.newposition.admin.domain.CoCourseAlias where courseAliasName like '" + searchWord + "%'");
        for (CoCourseAlias coCourseAlias : coCourseAliasList)
        {
            List<NpCourse> courseList = (List<NpCourse>) getHibernateTemplate().find("from NpCourse  where id=?", coCourseAlias.getNpCourse().getId());
            if (courseList != null)
            {
                for (NpCourse npCourse : courseList)
                {
                    Set<NpInstituteCourse> npInstituteCourseList = npCourse.getNpInstituteCourses();
                    for (NpInstituteCourse npInstituteCourse : npInstituteCourseList)
                    {
                        if (npInstituteCourse.getStatus() && npInstituteCourse.getId().intValue() != merge.intValue())
                        {
                            npInstituteCourseSet.add(npInstituteCourse);
                        }
                    }
                }
            }
        }
        List<NpCourse> npCourseList = (List<NpCourse>) getHibernateTemplate().find("from NpCourse where courseName like '" + searchWord + "%'");
        for (NpCourse npCourse : npCourseList)
        {
            if (npCourse.getNpInstituteCourses() != null)
            {
                Set<NpInstituteCourse> npInstituteCourseList = npCourse.getNpInstituteCourses();
                for (NpInstituteCourse npInstituteCourse : npInstituteCourseList)
                {
                    if (npInstituteCourse.getStatus() && npInstituteCourse.getId().intValue() != merge.intValue())
                    {
                        npInstituteCourseSet.add(npInstituteCourse);
                    }
                }
            }
        }
        return npInstituteCourseSet;
    }

    @Transactional
    @Override
    public void removeCoCourseAlias(final int coCourseAliasId)
    {
        CoCourseAlias coCourseAlias = new CoCourseAliasImpl();
        coCourseAlias.setCourseAliasID(coCourseAliasId);
        getHibernateTemplate().delete(coCourseAlias);
    }

    @Override
    public NpInstituteCourse getNpInstituteCourseById(final int npInstituteCourseId)
    {
        NpInstituteCourse npInstituteCourse = getHibernateTemplate().get(NpInstituteCourse.class, npInstituteCourseId);
        return npInstituteCourse;
    }

    @Transactional
    @Override
    public void removeNpInstituteCourseById(final int npInstituteCourseId)
    {
        NpInstituteCourse instituteCourse = new NpInstituteCourse();
        instituteCourse.setId(npInstituteCourseId);
        getHibernateTemplate().delete(instituteCourse);
    }

    @Transactional
    @Override
    public void saveMergedNpInstituteCourse(AddNpQualificationForm addNpQualificationForm)
    {
        boolean isExists = true;
        NpInstituteCourse npInstituteCourse = null;
        NpInstituteCourse npInstituteCourseMerge = getNpInstituteCourseById(Integer.parseInt(addNpQualificationForm.getMergeId()));
        NpCourse npCourse = null;

        if (addNpQualificationForm.getNpInstituteCourseId() != null && addNpQualificationForm.getNpInstituteCourseId() > 0)
        {
            npInstituteCourse = getNpInstituteCourseById(addNpQualificationForm.getNpInstituteCourseId());
            npCourse = npInstituteCourse.getNpCourse();
        }

        npCourse.setCourseName(addNpQualificationForm.getCourseName());
        npCourse.setCreatedDate(new Date());
        NpInstitutions npInstitutions = (NpInstitutions) getHibernateTemplate().find("from NpInstitutions where id=?", addNpQualificationForm.getInstitutionId()).get(0);

        npCourse.setValid(addNpQualificationForm.isValid());
        npCourse.setCourseType(addNpQualificationForm.getCourseType());
        getHibernateTemplate().saveOrUpdate(npCourse);
        if (npInstitutions != null)
        {
            npInstituteCourse.setNpCourse(npCourse);
            npInstituteCourse.setNpInstitutions(npInstitutions);
        }
        npInstituteCourse.setStatus(true);
        getHibernateTemplate().saveOrUpdate(npInstituteCourse);
        if (npInstituteCourseMerge.getNpCourse().getCourseName().equalsIgnoreCase(npInstituteCourse.getNpCourse().getCourseName()))
        {
            isExists = false;
        }

		/*if(addNpCertificationForm.getCertificationAliases()!=null)
        {
			for(CertificationAliases certificationAliases: addNpCertificationForm.getCertificationAliases())
			{
				if(StringUtils.isNotBlank(certificationAliases.getAliasSource()))
				{
					CoCertificationAliasImpl coCertificationAlias=null;

					if(StringUtils.isNotBlank(certificationAliases.getAliasId()))
					{
						coCertificationAlias=(CoCertificationAliasImpl) getHibernateTemplate().find("from com.newposition.common.domain.CoCertificationAlias where certificateAliasID=?",Integer.parseInt(certificationAliases.getAliasId())).get(0);
					}
					else
					{
						coCertificationAlias=new CoCertificationAliasImpl();
					}
				coCertificationAlias.setCertificationAliasName(certificationAliases.getAliasName());
				CoSource coSource=(CoSource) getHibernateTemplate().find("from CoSource where sourceID=?",Integer.parseInt(certificationAliases.getAliasSource())).get(0);
				if(coSource!=null)
				{
					coCertificationAlias.setCoSource(coSource);
				}
				coCertificationAlias.setNpCertifications(npCertifications);
				getHibernateTemplate().saveOrUpdate(coCertificationAlias);
				}
			}
			
		}*/

        if (npInstituteCourseMerge.getNpCourse().getCoCourseAlias() != null)
        {
            for (CoCourseAliasImpl coCourseAliasImpl : npInstituteCourseMerge.getNpCourse().getCoCourseAlias())
            {

                if (coCourseAliasImpl.getCourseAliasName().equalsIgnoreCase(npInstituteCourseMerge.getNpCourse().getCourseName()))
                {
                    isExists = false;
                }
                for (CoCourseAliasImpl coCourseAliasImplLatest : npCourse.getCoCourseAlias())
                {
                    if (coCourseAliasImplLatest.getCourseAliasName().equalsIgnoreCase(npInstituteCourseMerge.getNpCourse().getCourseName()))
                    {
                        isExists = false;
                    }
                    if (!coCourseAliasImplLatest.getCourseAliasName().equalsIgnoreCase(coCourseAliasImpl.getCourseAliasName()))
                    {
                        coCourseAliasImpl.setNpCourse(npCourse);
                        getHibernateTemplate().saveOrUpdate(coCourseAliasImpl);
                    }
                }
            }
        }
        if (isExists)
        {
            CoCourseAliasImpl coCourseAliasImpl = new CoCourseAliasImpl();
            CoSource coSource = new CoSource();
            coSource.setSourceID(4);
            coCourseAliasImpl.setCoSource(coSource);
            coCourseAliasImpl.setCourseAliasName(npInstituteCourseMerge.getNpCourse().getCourseName());
            coCourseAliasImpl.setNpCourse(npCourse);
            hibernateTemplate.saveOrUpdate(coCourseAliasImpl);
        }

        npInstituteCourseMerge.setStatus(false);
        getHibernateTemplate().saveOrUpdate(npInstituteCourseMerge);
        Set<NpCandidateEducation> npCandidateEducations = npInstituteCourseMerge.getNpCandidateEducations();
        for (NpCandidateEducation npCandidateEducation : npCandidateEducations)
        {
            npCandidateEducation.setNpInstituteCourse(npInstituteCourse);
            getHibernateTemplate().saveOrUpdate(npCandidateEducation);
        }
    }

    @Override
    public Set<CoCourseAliasImpl> getAliases(final String id)
    {

        NpInstituteCourse npInstituteCourse = getHibernateTemplate().get(NpInstituteCourse.class, Integer.parseInt(id));
        if (npInstituteCourse != null)
        {
            NpCourse npCourse = npInstituteCourse.getNpCourse();
            return npCourse.getCoCourseAlias();
        }
        return null;
    }
}
