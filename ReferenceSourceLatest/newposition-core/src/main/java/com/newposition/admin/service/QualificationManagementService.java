package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.domain.CoCourseAliasImpl;
import com.newposition.admin.forms.AddNpQualificationForm;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoSource;

public interface QualificationManagementService
{

    public Set<NpInstitutions> getNpInstitutions();

    public Set<String> getCourseTypes();

    public void saveNpInstituteCourse(AddNpQualificationForm addNpQualificationForm);

    public Set<CoSource> getCoSources();

    public Set<NpInstituteCourse> getNpInstituteCourseforAliasSearch(final String searchWord, String mergeId);

    public void removeCoCourseAlias(final int coCertificationAliasId);

    public NpInstituteCourse getNpInstituteCourseById(final int npInstituteCourseId);

    public void saveMergedNpInstituteCourse(AddNpQualificationForm addNpQualificationForm);

    public void removeNpInstituteCourseById(final int npInstituteCourseId);

    public List getFilteredNpInstituteCourse(String sortBy, String courseTypeFilter,
                                             String validFilter, String instituteFilter, String pageNumber);

    public Set<CoCourseAliasImpl> getAliases(final String id);
}
