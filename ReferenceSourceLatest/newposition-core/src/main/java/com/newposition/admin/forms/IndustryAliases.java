package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Trinadh
 */
public class IndustryAliases
{

    public String aliasName;
    public String aliasSource;
    public String aliasId;

    public String getAliasId()
    {
        return aliasId;
    }

    public void setAliasId(String aliasId)
    {
        this.aliasId = aliasId;
    }

    public String getAliasSource()
    {
        return aliasSource;
    }

    public void setAliasSource(String aliasSource)
    {
        this.aliasSource = aliasSource;
    }

    public String getAliasName()
    {
        return aliasName;
    }

    public void setAliasName(String aliasName)
    {
        this.aliasName = aliasName;
    }
}
