package com.newposition.admin.domain;

import java.util.Date;

import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;

public interface CoEmployerAlias
{

    public NpEmployerImpl getNpEmployerImpl();

    public void setNpEmployerImpl(NpEmployerImpl npEmployerImpl);

    public String getEmployerAliasName();

    public void setEmployerAliasName(String industryAliasName);

    public CoSource getCoSource();

    public void setCoSource(CoSource coSource);

    public String getStatus();

    public void setStatus(String status);

    public Integer getEmployerAliasID();

    public void setEmployerAliasID(Integer industryAliasID);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    public String getModifiedBy();

    public void setModifiedBy(String modifiedBy);

    public Date getModifiedDate();

    public void setModifiedDate(Date modifiedDate);
}
