package com.newposition.admin.forms;

import java.util.List;

import com.newposition.admin.forms.SkillAliases;

/**
 * @author Trinadh
 */
public class AddNpSKillsForm
{

    public String skillId;
    public String domainId;
    public String skillType;
    public String skillName;
    public String valid;
    public List<CertificationAliases> certificationAliases;
    public String mergeToId;
    public boolean deleteSkill;

    public boolean isDeleteSkill()
    {
        return deleteSkill;
    }

    public void setDeleteSkill(boolean deleteSkill)
    {
        this.deleteSkill = deleteSkill;
    }

    public String getMergeToId()
    {
        return mergeToId;
    }

    public void setMergeToId(String mergeToId)
    {
        this.mergeToId = mergeToId;
    }

    public String getSkillId()
    {
        return skillId;
    }

    public void setSkillId(String skillId)
    {
        this.skillId = skillId;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public String getValid()
    {
        return valid;
    }

    public void setValid(String valid)
    {
        this.valid = valid;
    }

    public String getDomainId()
    {
        return domainId;
    }

    public void setDomainId(String domainId)
    {
        this.domainId = domainId;
    }

    public String getSkillType()
    {
        return skillType;
    }

    public void setSkillType(String skillType)
    {
        this.skillType = skillType;
    }

    public String getSkillName()
    {
        return skillName;
    }

    public void setSkillName(String skillName)
    {
        this.skillName = skillName;
    }
}
