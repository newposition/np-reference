package com.newposition.admin.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.newposition.admin.domain.CoEmployerAlias;
import com.newposition.admin.domain.CoEmployerAliasImpl;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;

import com.newposition.admin.dao.NpAdminIndustryManagementDao;
import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateEmployerImpl;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpSectorImpl;

public class NpAdminIndustryManagementDaoImpl implements NpAdminIndustryManagementDao
{
    private HibernateTemplate hibernateTemplate;
    private int numberOfReusltsPerPage;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    @Override
    public NpSectorImpl save(NpSectorImpl entity)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void merge(NpSectorImpl entity)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(NpSectorImpl entity)
    {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<NpSectorImpl> findAll()
    {
        // TODO Auto-generated method stub
        return (List<NpSectorImpl>) hibernateTemplate.find("from NpSectorImpl");
    }

    @Override
    public NpSectorImpl findByID(Integer id)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpEmployers(String sortBy, String industryTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        // TODO Auto-generated method stub
        List result = new ArrayList();

        String query = "from NpEmployerImpl";

        if (StringUtils.isNotBlank(industryTypeFilter)
                || StringUtils.isNotBlank(validFilter)
                || StringUtils.isNotBlank(domainFiter))
        {
            query = "from NpEmployerImpl where ";
            if (StringUtils.isNotBlank(industryTypeFilter))
            {
                if (StringUtils.isBlank(validFilter)
                        && StringUtils.isBlank(domainFiter))
                {
                    query = query + "industryType='" + industryTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "industryType='" + industryTypeFilter + "'"
                            + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(domainFiter))
                {
                    query = query + "valid='" + validFilter + "'";
                }
                else
                {
                    query = query + "valid='" + validFilter + "'" + " "
                            + "and ";
                }
            }
            if (StringUtils.isNotBlank(domainFiter))
            {
                query = query + "sectorId=" + domainFiter;
            }
            query = query + " and status=1";
        }
        else
        {
            query = query + " where status=1";
        }

        if (StringUtils.isNotBlank(sortBy))
        {
            query = query + " order by " + sortBy;
        }

//		npIndustries = (List<NpEmployerImpl>) hibernateTemplate.find(query);
        Long npIndustriesCount = (Long) hibernateTemplate.find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        result.add(getPaginatedresult(query, pageNo));
        result.add((int) npIndustriesCount.longValue());

        return result;
    }

    @SuppressWarnings("unchecked")
    private List<NpEmployerImpl> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpEmployerImpl>) hibernateTemplate.execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<String> getIndustryTypes()
    {
        // TODO Auto-generated method stub
        List<String> npIndsutryTypes = (List<String>) hibernateTemplate
                .find("select industryType from NpEmployerImpl where industryType != null");
        Set<String> npIndsutryTypesSet = new HashSet<String>(npIndsutryTypes);
        return npIndsutryTypesSet;
    }

    @Override
    public void saveNpIndustries(AddNpIndustriesForm addNpIndustriesForm)
    {
        // TODO Auto-generated method stub
        NpEmployerImpl npEmployers = null;
        if (StringUtils.isNotBlank(addNpIndustriesForm.getIndustryId()))
        {
            npEmployers = (NpEmployerImpl) hibernateTemplate.find(
                    "from NpEmployerImpl where id=?",
                    Integer.parseInt(addNpIndustriesForm.getIndustryId())).get(
                    0);
            /*if(npEmployers.getCoEmployerAlias()!=null)
			{
				for(CoEmployerAliasImpl coAlias:npEmployers.getCoEmployerAlias())
				{
					hibernateTemplate.delete(coAlias);
				}
			}*/
        }
        else
        {
            npEmployers = new NpEmployerImpl();
        }

        npEmployers.setName(addNpIndustriesForm.getIndustryName());
        npEmployers.setCreatedDate(new Date());
        npEmployers.setStatus(true);
        NpSectorImpl npSector = (NpSectorImpl) hibernateTemplate.find(
                "from NpSectorImpl where id=?",
                Integer.parseInt(addNpIndustriesForm.getSectorId())).get(0);
        if (npSector != null)
        {
            npEmployers.setNpSector(npSector);
        }
        npEmployers.setValid(addNpIndustriesForm.getValid());

        npEmployers.setIndustryType(addNpIndustriesForm.getIndustryType());

        // to allow testing, hack in values not passed in...
        npEmployers.setCreatedBy("who?");
        npEmployers.setIndustryType("indType?");
        npEmployers.setModifiedBy("who?");
        npEmployers.setModifiedDate(new Date());

        hibernateTemplate.saveOrUpdate(npEmployers);

        if (addNpIndustriesForm.getCertificationAliases() != null)
        {
            for (CertificationAliases certificationAliases : addNpIndustriesForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(certificationAliases.getAliasSource()))
                {
                    CoEmployerAlias coEmployerAlias = null;

                    if (StringUtils.isNotBlank(certificationAliases.getAliasId()))
                    {
                        coEmployerAlias = (CoEmployerAlias) hibernateTemplate
                                .find("from CoEmployerAliasImpl where employerAliasID=?",
                                        Integer.parseInt(certificationAliases
                                                .getAliasId())).get(0);
                    }
                    else
                    {
                        coEmployerAlias = new CoEmployerAliasImpl();
                    }
                    coEmployerAlias.setEmployerAliasName(certificationAliases
                            .getAliasName());
                    CoSource coSource = (CoSource) hibernateTemplate.find(
                            "from CoSource where sourceID=?",
                            Integer.parseInt(certificationAliases.getAliasSource()))
                            .get(0);
                    if (coSource != null)
                    {
                        coEmployerAlias.setCoSource(coSource);
                    }
                    coEmployerAlias.setNpEmployerImpl(npEmployers);
                    hibernateTemplate.saveOrUpdate(coEmployerAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getcoSources()
    {
        List<CoSource> coSources = (List<CoSource>) hibernateTemplate
                .find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpEmployerImpl> getNpEmployersforAliasSearch(String searchWord, String mergeId)
    {
        Integer merge = Integer.parseInt(mergeId);
        List<CoEmployerAlias> coEmployerAliasList = (List<CoEmployerAlias>) hibernateTemplate
                .find("from CoEmployerAliasImpl where employerAliasName like '" + searchWord + "%'");
        Set<NpEmployerImpl> npEmployerSet = new HashSet<NpEmployerImpl>();
        if (coEmployerAliasList != null)
        {
            for (CoEmployerAlias coEmployerAlias : coEmployerAliasList)
            {
                NpEmployerImpl npEmployer = coEmployerAlias.getNpEmployerImpl();
                if (npEmployer.isStatus() && npEmployer.getId().intValue() != merge.intValue())
                {
                    npEmployerSet.add(npEmployer);
                }
            }
        }
        List<NpEmployerImpl> npEmployerList = (List<NpEmployerImpl>) hibernateTemplate.find("from NpEmployerImpl where name like '" + searchWord + "%'");
        if (npEmployerList != null)
        {
            for (NpEmployerImpl npEmployerImpl : npEmployerList)
            {
                if (npEmployerImpl.isStatus() && npEmployerImpl.getId().intValue() != merge.intValue())
                {
                    npEmployerSet.add(npEmployerImpl);
                }
            }
        }
        return npEmployerSet;
    }

    @Override
    public void mergeNpEmployer(AddNpIndustriesForm addNpIndustriesForm)
    {
        // TODO Auto-generated method stub
        boolean isExists = true;
        NpEmployerImpl npEmployers = (NpEmployerImpl) hibernateTemplate.find("from NpEmployerImpl where id=?", Integer.parseInt(addNpIndustriesForm.getMergeToId())).get(0);

        NpEmployerImpl npEmployersLatest = (NpEmployerImpl) hibernateTemplate.find("from NpEmployerImpl where id=?", Integer.parseInt(addNpIndustriesForm.getIndustryId())).get(0);

        if (npEmployers != null)
        {
            if (npEmployers.getName().equalsIgnoreCase(npEmployersLatest.getName()))
            {
                isExists = false;
            }

            npEmployers.setStatus(false);

            hibernateTemplate.saveOrUpdate(npEmployers);

            Set<NpCandidateEmployerImpl> npCandidateEmployers = npEmployers.getNpCandidateEmployers();

            for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
            {

                npCandidateEmployer.setNpEmployer(npEmployersLatest);
                hibernateTemplate.saveOrUpdate(npCandidateEmployer);
            }

            for (CoEmployerAlias coEmployerAlias : npEmployers.getCoEmployerAlias())
            {

                if (coEmployerAlias.getEmployerAliasName().equalsIgnoreCase(npEmployers.getName()))
                {
                    isExists = false;
                }
                for (CoEmployerAlias coEmployerAliasLatest : npEmployersLatest.getCoEmployerAlias())
                {
                    if (coEmployerAliasLatest.getEmployerAliasName().equalsIgnoreCase(npEmployers.getName()))
                    {
                        isExists = false;
                    }
                    if (!coEmployerAliasLatest.getEmployerAliasName().equalsIgnoreCase(coEmployerAlias.getEmployerAliasName()))
                    {
                        coEmployerAlias.setNpEmployerImpl(npEmployersLatest);
                        hibernateTemplate.saveOrUpdate(coEmployerAlias);
                    }
                }
            }
            if (isExists)
            {
                CoEmployerAlias coEmployerAlias = new CoEmployerAliasImpl();
                CoSource coSource = new CoSource();
                coSource.setSourceID(4);
                coEmployerAlias.setCoSource(coSource);
                coEmployerAlias.setEmployerAliasName(npEmployers.getName());
                coEmployerAlias.setNpEmployerImpl(npEmployersLatest);
                hibernateTemplate.saveOrUpdate(coEmployerAlias);
            }
        }
    }

    @Override
    public void deleteNpEmployer(String industryId)
    {
        // TODO Auto-generated method stub
        NpEmployerImpl npIndustries = (NpEmployerImpl) hibernateTemplate.find("from NpEmployerImpl where id=?", Integer.parseInt(industryId)).get(0);

        if (npIndustries != null)
        {
            npIndustries.setStatus(false);
            hibernateTemplate.saveOrUpdate(npIndustries);
        }
    }

    @Override
    public void deleteCoAliasEmployers(String aliasId)
    {

        CoEmployerAliasImpl coEmployerAlias = (CoEmployerAliasImpl) hibernateTemplate.find("from CoEmployerAliasImpl where id=?", Integer.parseInt(aliasId)).get(0);

        if (coEmployerAlias != null)
        {
            hibernateTemplate.delete(coEmployerAlias);
        }
    }

    @Override
    public Set<CoEmployerAliasImpl> getAliases(final String id)
    {
        NpEmployerImpl npEmployerImpl = hibernateTemplate.get(NpEmployerImpl.class, Integer.parseInt(id));
        if (npEmployerImpl != null)
        {
            return npEmployerImpl.getCoEmployerAlias();
        }
        return null;
    }
}
