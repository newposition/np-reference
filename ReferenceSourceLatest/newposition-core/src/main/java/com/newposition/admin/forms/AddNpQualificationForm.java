package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Yamuna Vemula
 */
public class AddNpQualificationForm
{

    public Integer npInstituteCourseId;
    public Integer institutionId;
    public String courseType;
    public String courseName;
    public boolean valid;
    public List<CertificationAliases> certificationAliases;
    public String mergeId;

    public Integer getNpInstituteCourseId()
    {
        return npInstituteCourseId;
    }

    public void setNpInstituteCourseId(Integer npInstituteCourseId)
    {
        this.npInstituteCourseId = npInstituteCourseId;
    }

    public Integer getInstitutionId()
    {
        return institutionId;
    }

    public void setInstitutionId(Integer institutionId)
    {
        this.institutionId = institutionId;
    }

    public String getCourseType()
    {
        return courseType;
    }

    public void setCourseType(String courseType)
    {
        this.courseType = courseType;
    }

    public String getCourseName()
    {
        return courseName;
    }

    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(
            List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public String getMergeId()
    {
        return mergeId;
    }

    public void setMergeId(String mergeId)
    {
        this.mergeId = mergeId;
    }
}
