package com.newposition.admin.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.dao.CertificationManagementDao;
import com.newposition.admin.forms.AddNpCertificationForm;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoCertificationAliasImpl;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpInstituteCertificate;
import com.newposition.common.domain.NpInstituteCertificateImpl;

@Service
public class CertificationManagementServiceImpl implements CertificationManagementService
{

    private CertificationManagementDao certificationManagementDao;

    public CertificationManagementDao getCertificationManagementDao()
    {
        return certificationManagementDao;
    }

    public void setCertificationManagementDao(CertificationManagementDao certificationManagementDao)
    {
        this.certificationManagementDao = certificationManagementDao;
    }

    @Override
    public List getFilteredNpInstituteCertificate(String sortBy,
                                                  String certificationTypeFilter, String validFilter, String domainFiter, String pageNumber)
    {
        return getCertificationManagementDao().getFilteredNpInstituteCertificate(sortBy, certificationTypeFilter, validFilter, domainFiter, pageNumber);
    }

    @Override
    public Set<NpInstitutions> getNpInstitutions()
    {
        List<NpInstitutions> npInstitutionsList = getCertificationManagementDao().getNpInstitutions();
        Set<NpInstitutions> npInstitutions = new HashSet<NpInstitutions>(npInstitutionsList);
        return npInstitutions;
    }

    @Override
    public Set<String> getCertificationTypes()
    {
        List<String> certificationTypesList = getCertificationManagementDao().getCertificationTypes();
        Set<String> certificationTypes = new HashSet<String>(certificationTypesList);
        return certificationTypes;
    }

    @Override
    public void saveNpCertifications(AddNpCertificationForm addNpCertificationForm)
    {
        getCertificationManagementDao().saveNpCertifications(addNpCertificationForm);
    }

    @Override
    public Set<CoSource> getCoSources()
    {
        List<CoSource> coSourceList = getCertificationManagementDao().getCoSources();
        Set<CoSource> coSources = new HashSet<CoSource>(coSourceList);
        return coSources;
    }

    @Override
    public Set<NpInstituteCertificateImpl> getNpInstituteCertificateforAliasSearch(final String searchWord, String mergeId)
    {
        return getCertificationManagementDao().getNpInstituteCertificateforAliasSearch(searchWord, mergeId);
    }

    @Transactional
    @Override
    public void removeCoCertificateAlias(final int coCertificationAliasId)
    {
        getCertificationManagementDao().removeCoCertificateAlias(coCertificationAliasId);
    }

    @Override
    public NpInstituteCertificate getNpInstituteCertificateById(final int npInstituteCertificateId)
    {
        return getCertificationManagementDao().getNpInstituteCertificateById(npInstituteCertificateId);
    }

    @Override
    public void saveMergedNpInstituteCertificate(AddNpCertificationForm addNpCertificationForm)
    {
        getCertificationManagementDao().saveMergedNpInstituteCertificate(addNpCertificationForm);
    }

    @Override
    public void removeNpInstituteCertificateById(final int npInstituteCertificateId)
    {
        getCertificationManagementDao().removeNpInstituteCertificateById(npInstituteCertificateId);
    }

    @Transactional
    @Override
    public Set<CoCertificationAliasImpl> getAliases(final String id)
    {
        return getCertificationManagementDao().getAliases(id);
    }
}
