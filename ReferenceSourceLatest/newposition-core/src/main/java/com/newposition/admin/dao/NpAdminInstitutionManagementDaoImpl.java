package com.newposition.admin.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.newposition.admin.domain.CoInstituteAlias;
import com.newposition.admin.domain.CoInstituteAliasImpl;
import com.newposition.admin.forms.AddNpInstitutionForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.candidate.domain.NpCourse;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.candidate.domain.NpInstitutions;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpEmploymentRoleImpl;

@Repository
public class NpAdminInstitutionManagementDaoImpl implements NpAdminInstitutionManagementDao
{

    protected static final Logger LOG = Logger.getLogger(NpAdminInstitutionManagementDaoImpl.class);

    private HibernateTemplate hibernateTemplate;
    private int numberOfReusltsPerPage;

    public int getNumberOfReusltsPerPage()
    {
        return numberOfReusltsPerPage;
    }

    public void setNumberOfReusltsPerPage(int numberOfReusltsPerPage)
    {
        this.numberOfReusltsPerPage = numberOfReusltsPerPage;
    }

    public HibernateTemplate getHibernateTemplate()
    {
        return hibernateTemplate;
    }

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate)
    {
        this.hibernateTemplate = hibernateTemplate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List getFilteredNpInstitutes(String sortBy,
                                        String instituteTypeFilter, String validFilter, String instituteNameFilter, String pageNumber)
    {
        List npInstitutionsResult = new ArrayList();

        String query = "from com.newposition.candidate.domain.NpInstitutions I where I.status=true  ";

        if (StringUtils.isNotBlank(instituteTypeFilter) || StringUtils.isNotBlank(validFilter) || StringUtils.isNotBlank(instituteNameFilter))
        {
            query = "from com.newposition.candidate.domain.NpInstitutions I where I.status=true and ";
            if (StringUtils.isNotBlank(instituteTypeFilter))
            {
                if (StringUtils.isBlank(validFilter) && StringUtils.isBlank(instituteNameFilter))
                {
                    query = query + "I.instituteType='" + instituteTypeFilter + "'" + " ";
                }
                else
                {
                    query = query + "I.instituteType='" + instituteTypeFilter + "'" + " " + "and ";
                }
            }

            if (StringUtils.isNotBlank(validFilter))
            {
                if (StringUtils.isBlank(instituteNameFilter))
                {
                    query = query + "I.valid=" + validFilter;
                }
                else
                {
                    query = query + "I.valid=" + validFilter + " " + "and ";
                }
            }
            if (StringUtils.isNotBlank(instituteNameFilter))
            {
                query = query + "I.instituteName='" + instituteNameFilter + "'";
            }
        }

        if (StringUtils.isNotBlank(sortBy))
        {
            query = query + "  order by I." + sortBy;
        }

//		npInstitutions=(List<NpInstitutions>) getHibernateTemplate().find(query);

        Long npInstitutionsCount = (Long) hibernateTemplate.find("select count(*) " + query).get(0);

        int pageNo = 0;
        if (StringUtils.isNotBlank(pageNumber))
        {
            pageNo = Integer.parseInt(pageNumber);
        }

        npInstitutionsResult.add(getPaginatedresult(query, pageNo));
        npInstitutionsResult.add((int) npInstitutionsCount.longValue());

        return npInstitutionsResult;
    }

    @SuppressWarnings("unchecked")
    private List<NpInstitutions> getPaginatedresult(final String query, final int pageNo)
    {

        final String fquery = query;
        return (List<NpInstitutions>) hibernateTemplate.execute(new HibernateCallback()
        {
            public Object doInHibernate(Session session) throws HibernateException
            {
                Query queryh = session.createQuery(fquery);
                queryh.setMaxResults(getNumberOfReusltsPerPage());
                queryh.setFirstResult(getNumberOfReusltsPerPage() * pageNo);
                return queryh.list();
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getInstituteNames()
    {
        List<String> npInstituteNames = (List<String>) getHibernateTemplate().find("select instituteName from NpInstitutions");
        return npInstituteNames;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getInstituteTypes()
    {
        List<String> npInstituteTypes = (List<String>) getHibernateTemplate().find("select instituteType from NpInstitutions where instituteType != null");
        return npInstituteTypes;
    }

    @Transactional
    @Override
    public void saveNpInstitute(AddNpInstitutionForm addNpInstitutionForm)
    {

        NpInstitutions npInstitutions = null;
        if (addNpInstitutionForm.getInstituteId() != null && StringUtils.isNotBlank(addNpInstitutionForm.getInstituteId().toString()))
        {
            npInstitutions = (NpInstitutions) getHibernateTemplate().find("from com.newposition.candidate.domain.NpInstitutions where id=?", addNpInstitutionForm.getInstituteId()).get(0);
            /*if(npInstitutions.getCoInstituteAlias()!=null)
			{
				for(CoInstituteAlias coInstituteAlias:npInstitutions.getCoInstituteAlias())
				{
					getHibernateTemplate().delete(coInstituteAlias);
				}
			}*/
        }
        else
        {
            npInstitutions = new NpInstitutions();
        }

        npInstitutions.setInstituteName(addNpInstitutionForm.getInstituteName());
        npInstitutions.setCreatedDate(new Date());

        npInstitutions.setValid(addNpInstitutionForm.isValid());
        npInstitutions.setInstituteType(addNpInstitutionForm.getInstituteType());
        npInstitutions.setStatus(true);
        getHibernateTemplate().saveOrUpdate(npInstitutions);
        if (addNpInstitutionForm.getCertificationAliases() != null)
        {
            for (CertificationAliases certificationAliases : addNpInstitutionForm.getCertificationAliases())
            {
                if (StringUtils.isNotBlank(certificationAliases.getAliasSource()))
                {
                    CoInstituteAlias coInstituteAlias = null;

                    if (StringUtils.isNotBlank(certificationAliases.getAliasId()))
                    {
                        coInstituteAlias = (CoInstituteAliasImpl) getHibernateTemplate().find("from com.newposition.admin.domain.CoInstituteAliasImpl where instituteAliasID=?", Integer.parseInt(certificationAliases.getAliasId())).get(0);
                    }
                    else
                    {
                        coInstituteAlias = new CoInstituteAliasImpl();
                    }
                    coInstituteAlias.setInstituteAliasName(certificationAliases.getAliasName());
                    CoSource coSource = (CoSource) getHibernateTemplate().find("from CoSource where sourceID=?", Integer.parseInt(certificationAliases.getAliasSource())).get(0);
                    if (coSource != null)
                    {
                        coInstituteAlias.setCoSource(coSource);
                    }
                    coInstituteAlias.setNpInstitutions(npInstitutions);
                    getHibernateTemplate().saveOrUpdate(coInstituteAlias);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CoSource> getCoSources()
    {

        List<CoSource> coSources = (List<CoSource>) getHibernateTemplate().find("from CoSource");
        return coSources;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<NpInstitutions> getNpInstituteforAliasSearch(final String searchWord, String mergeId)
    {
        Set<NpInstitutions> npInstitutionsSet = new HashSet<NpInstitutions>();
        Integer merge = Integer.parseInt(mergeId);
        List<CoInstituteAlias> coInstituteAliasList = (List<CoInstituteAlias>) getHibernateTemplate().find("from com.newposition.admin.domain.CoInstituteAlias where instituteAliasName like '" + searchWord + "%'");
        for (CoInstituteAlias coInstituteAlias : coInstituteAliasList)
        {
            NpInstitutions npInstitutions = coInstituteAlias.getNpInstitutions();
            if (npInstitutions.isStatus() && npInstitutions.getId().intValue() != merge.intValue())
            {
                npInstitutionsSet.add(npInstitutions);
            }
        }
        List<NpInstitutions> npInstitutionsList = (List<NpInstitutions>) hibernateTemplate.find("from NpInstitutions where instituteName like '" + searchWord + "%'");
        if (npInstitutionsList != null)
        {
            for (NpInstitutions npInstitutions : npInstitutionsList)
            {
                if (npInstitutions.isStatus() && npInstitutions.getId().intValue() != merge.intValue())
                {
                    npInstitutionsSet.add(npInstitutions);
                }
            }
        }
        return npInstitutionsSet;
    }

    @Override
    public void removeCoInstituteAlias(final int coInstituteAliasId)
    {
        CoInstituteAlias coInstituteAlias = new CoInstituteAliasImpl();
        coInstituteAlias.setInstituteAliasID(coInstituteAliasId);
        getHibernateTemplate().delete(coInstituteAlias);
    }

    @Override
    public NpInstitutions getNpInstituteById(final int npInstituteId)
    {
        NpInstitutions npInstitutions = getHibernateTemplate().get(NpInstitutions.class, npInstituteId);
        return npInstitutions;
    }

    @Transactional
    @Override
    public void removeNpInstituteById(final int npInstituteId)
    {
        NpInstitutions npInstitutions = getNpInstituteById(npInstituteId);
        npInstitutions.setStatus(false);
        getHibernateTemplate().saveOrUpdate(npInstitutions);
    }

    @Transactional
    @Override
    public void saveMergedNpInstitute(AddNpInstitutionForm addNpInstitutionForm)
    {
        boolean isExists = true;
        NpInstitutions npInstitutions = null;
        NpInstitutions npInstitutionsMerge = getNpInstituteById(Integer.parseInt(addNpInstitutionForm.getMergeId()));
        if (addNpInstitutionForm.getInstituteId() != null && addNpInstitutionForm.getInstituteId() > 0)
        {
            npInstitutions = getNpInstituteById(addNpInstitutionForm.getInstituteId());
        }
        if (npInstitutions != null)
        {
            if (npInstitutions.getInstituteName().equalsIgnoreCase(npInstitutionsMerge.getInstituteName()))
            {
                isExists = false;
            }

            npInstitutions.setInstituteName(addNpInstitutionForm.getInstituteName());
            npInstitutions.setCreatedDate(new Date());

            npInstitutions.setValid(addNpInstitutionForm.isValid());
            npInstitutions.setInstituteType(addNpInstitutionForm.getInstituteType());
            getHibernateTemplate().saveOrUpdate(npInstitutions);
            npInstitutions.setStatus(true);
            getHibernateTemplate().saveOrUpdate(npInstitutions);

            if (npInstitutionsMerge.getCoInstituteAlias() != null)
            {
                for (CoInstituteAliasImpl coInstituteAliasImpl : npInstitutionsMerge.getCoInstituteAlias())
                {

                    if (coInstituteAliasImpl.getInstituteAliasName().equalsIgnoreCase(npInstitutionsMerge.getInstituteName()))
                    {
                        isExists = false;
                    }
                    for (CoInstituteAliasImpl coInstituteAliasImplLatest : npInstitutions.getCoInstituteAlias())
                    {
                        if (coInstituteAliasImplLatest.getInstituteAliasName().equalsIgnoreCase(npInstitutionsMerge.getInstituteName()))
                        {
                            isExists = false;
                        }
                        if (!coInstituteAliasImplLatest.getInstituteAliasName().equalsIgnoreCase(coInstituteAliasImpl.getInstituteAliasName()))
                        {
                            coInstituteAliasImpl.setNpInstitutions(npInstitutions);
                            getHibernateTemplate().saveOrUpdate(coInstituteAliasImpl);
                        }
                    }
                }
            }

            npInstitutionsMerge.setStatus(false);
            getHibernateTemplate().saveOrUpdate(npInstitutionsMerge);
            if (isExists)
            {
                CoInstituteAliasImpl coInstituteAliasImpl = new CoInstituteAliasImpl();
                CoSource coSource = new CoSource();
                coSource.setSourceID(4);
                coInstituteAliasImpl.setCoSource(coSource);
                coInstituteAliasImpl.setInstituteAliasName(npInstitutionsMerge.getInstituteName());
                coInstituteAliasImpl.setNpInstitutions(npInstitutions);
                getHibernateTemplate().saveOrUpdate(coInstituteAliasImpl);
            }
		/*Set<NpCandidateEducation> npCandidateEducations = npInstituteCourseMerge.getNpCandidateEducations();
		for (NpCandidateEducation npCandidateEducation : npCandidateEducations) {
			npCandidateEducation.setNpInstituteCourse(npInstituteCourse);
			getHibernateTemplate().saveOrUpdate(npCandidateEducation);
		}*/
        }
    }

    @Override
    public Set<CoInstituteAliasImpl> getAliases(final String id)
    {

        NpInstitutions npInstitutions = getHibernateTemplate().get(NpInstitutions.class, Integer.parseInt(id));
        if (npInstitutions != null)
        {
            return npInstitutions.getCoInstituteAlias();
        }
        return null;
    }
}
