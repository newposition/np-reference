package com.newposition.admin.service;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpToolImpl;

public interface NpAdminToolManagementService
{

    public Set<String> getToolTypes();

    public void saveNpTools(AddNpToolsForm addNpToolsForm);

    public Set<CoSource> getcoSources();

    public Set<NpToolImpl> getNpToolsforAliasSearch(String searchWord, String mergeId);

    public void mergeNpTool(AddNpToolsForm addNpToolsForm);

    public void deletNpTools(String toolId);

    List getFilteredNpTools(String sortBy, String skillTypeFilter,
                            String validFilter, String domainFiter, String pageNumber);

    public void deleteCoAliasTools(String aliasId);

    public Set<CoToolAlias> getAliases(final String id);
}
