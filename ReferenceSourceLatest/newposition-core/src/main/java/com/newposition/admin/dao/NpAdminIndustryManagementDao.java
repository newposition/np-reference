package com.newposition.admin.dao;

import java.util.List;
import java.util.Set;

import com.newposition.admin.domain.CoEmployerAliasImpl;
import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.common.dao.GenericDao;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpEmployerImpl;
import com.newposition.common.domain.NpSectorImpl;

public interface NpAdminIndustryManagementDao extends GenericDao<NpSectorImpl, Integer>
{

    public Set<String> getIndustryTypes();

    public void saveNpIndustries(AddNpIndustriesForm addNpIndustriesForm);

    public List<CoSource> getcoSources();

    public Set<NpEmployerImpl> getNpEmployersforAliasSearch(String searchWord, String mergeId);

    public void mergeNpEmployer(AddNpIndustriesForm addNpIndustriesForm);

    public void deleteNpEmployer(String industryId);

    List getFilteredNpEmployers(String sortBy,
                                String industryTypeFilter, String validFilter, String domainFiter,
                                String pageNumber);

    public void deleteCoAliasEmployers(String aliasId);

    public Set<CoEmployerAliasImpl> getAliases(final String id);
}
