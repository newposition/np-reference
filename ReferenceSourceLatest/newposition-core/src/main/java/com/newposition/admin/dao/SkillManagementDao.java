package com.newposition.admin.dao;

import java.util.List;
import java.util.Set;

import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoSource;
import com.newposition.common.domain.NpDomainImpl;
import com.newposition.common.domain.NpSkillImpl;
import com.newposition.exceptions.DomainNotFoundException;

public interface SkillManagementDao
{

    public List<NpDomainImpl> getDomainTypes();

    public List<String> getSkillTypes();

    public void saveNpSkills(AddNpSKillsForm addNpSKillsForm);

    public List<CoSource> getcoSources();

    public Set<NpSkillImpl> getNpSKillsforAliasSearch(String searchWord, String mergeId);

    public void mergeNpSkill(AddNpSKillsForm addNpSkillsForm);

    public void deletNpSkills(String skillId);

    List getFilteredNpSkills(String sortBy,
                             String skillTypeFilter, String validFilter, String domainFiter,
                             String pageNumber);

    public void deleteCoAliasSkills(String aliasId);

    public Set<CoSkillAlias> getAliases(final String id);

    public NpDomainImpl getDomainById(String id) throws DomainNotFoundException;
}
