package com.newposition.admin.forms;

import java.util.List;

/**
 * @author Yamuna Vemula
 */
public class AddNpCertificationForm
{

    public Integer npInstituteCertificateId;
    public Integer institutionId;
    public String certificationType;
    public String certificationName;
    public boolean valid;
    public List<CertificationAliases> certificationAliases;
    public String mergeId;

    public Integer getNpInstituteCertificateId()
    {
        return npInstituteCertificateId;
    }

    public void setNpInstituteCertificateId(Integer npInstituteCertificateId)
    {
        this.npInstituteCertificateId = npInstituteCertificateId;
    }

    public List<CertificationAliases> getCertificationAliases()
    {
        return certificationAliases;
    }

    public void setCertificationAliases(List<CertificationAliases> certificationAliases)
    {
        this.certificationAliases = certificationAliases;
    }

    public boolean getValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public Integer getInstitutionId()
    {
        return institutionId;
    }

    public void setInstitutionId(Integer institutionId)
    {
        this.institutionId = institutionId;
    }

    public String getCertificationType()
    {
        return certificationType;
    }

    public void setCertificationType(String certificationType)
    {
        this.certificationType = certificationType;
    }

    public String getCertificationName()
    {
        return certificationName;
    }

    public void setCertificationName(String certificationName)
    {
        this.certificationName = certificationName;
    }

    public String getMergeId()
    {
        return mergeId;
    }

    public void setMergeId(String mergeId)
    {
        this.mergeId = mergeId;
    }
}
