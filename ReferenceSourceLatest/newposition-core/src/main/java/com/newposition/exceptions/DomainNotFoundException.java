package com.newposition.exceptions;

public class DomainNotFoundException extends RuntimeException
{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DomainNotFoundException(String s)
    {
        super(s);
    }
}
