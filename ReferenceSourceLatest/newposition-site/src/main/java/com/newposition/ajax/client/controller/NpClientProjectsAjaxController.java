package com.newposition.ajax.client.controller;

import info.magnolia.context.MgnlContext;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.service.NpClientService;
import com.newposition.client.service.NpProjectService;
import com.newposition.common.domain.NpProgram;
import com.newposition.common.domain.NpUser;

@RestController
public class NpClientProjectsAjaxController
{

    @Autowired
    NpClientService npClientService;

    @Autowired
    NpProjectService npProjectService;

    @RequestMapping(value = "/getAllProjects", method = RequestMethod.POST)
    public
    @ResponseBody
    List<NpClientProject> getAllProjects(HttpSession session)
    {
        System.out.println("in all list ajaxs");
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        System.out.println("in all list ajaxs");
        List<NpClientProject> projects = getLightWeightPramsList(npProjectService.getAllClientProjects(npClient));
        System.out.println("projects=" + projects.size());
        return projects;
    }

    @RequestMapping(value = "/getObjectById", method = RequestMethod.GET)
    public
    @ResponseBody
    NpClientProject getProjectObject(@RequestParam("id") int id, Model model)
    {
        System.out.println("in ajaxs get Object id=" + id);
        NpClientProject npProject = getLightWeightProject(npProjectService.getNpProjectById(id));
        System.out.println("after ajaxs==" + npProject);
        return npProject;
    }

    @RequestMapping(value = "/deleteProject", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientProject> deleteRow(@RequestParam("id") int id, Model model, HttpSession session)
    {
        System.out.println("in ajaxs id=" + id);
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        System.out.println("in ajaxs npClient=" + npClient);
        String result = npProjectService.deleteProject(id);
        List<NpClientProject> projects = getLightWeightPramsList(npProjectService.getAllClientProjects(npClient));
        System.out.println("in ajaxs projects=" + projects.size());

        return projects;
    }

    @RequestMapping(value = "/getStringList", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getListOfStr(@RequestParam("str") String str, HttpSession session)
    {
        System.out.println("in str ajax controller");
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        System.out.println("in str ajax controlle npClient=" + npClient);
        List<NpClientProject> projects = npProjectService.getAllClientProjects(npClient);
        System.out.println("in str ajax controller projects=" + projects.size());

        List<String> list = npProjectService.getStringList(str, projects);
        System.out.println("in str ajax controller list=" + list.size());
        return list;
    }

    @RequestMapping(value = "/filterData", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientProject> getProjectsByFilters(HttpSession session, @RequestParam("str1") String str1, @RequestParam("str2") String str2)
    {
        System.out.println("in all filter ajaxs STR1=" + str1 + "   str2=" + str2);
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpClientProject> projects = npProjectService.getAllClientProjects(npClient);
        List<NpClientProject> projects1 = getLightWeightPramsList(npProjectService.getClientProjectsByFilter(str1, str2, projects));
        System.out.println("projects1=" + projects1.size());
        return projects1;
    }

    @RequestMapping(value = "/sortData")
    public
    @ResponseBody
    List<NpClientProject> getProjectsBySorting(HttpSession session, @RequestParam("str") String str, @RequestBody ArrayList<NpClientProject> projectlist)
    {
        System.out.println("in all sorting ajaxs STR=" + str);
        System.out.println("projectlist=" + projectlist.size());
        List<NpClientProject> projects = getLightWeightPramsList(npProjectService.getProjectsBySort(projectlist, str));
        System.out.println("project");
        return projects;
    }

    @RequestMapping(value = "/saveData")
    public
    @ResponseBody
    NpClientProject saveNpClientProject(HttpSession session, @RequestBody NpClientProject npClientProject, @RequestParam("npClientCostCentreImpl.costCentreID") int costCentreID)
    {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        NpClientCostCentreImpl costCenter = new NpClientCostCentreImpl();
        costCenter.setCostCentreId(new Long(costCentreID));
        npClientProject.setNpClient(npClient);
        try
        {
            System.out.println("$$$$$$$$$$$$$$$$$$$video convert");
            File video = MgnlContext.getPostedForm().getDocument("video").getFile();
            System.out.println("^^^^^^^^^^^^^^^^^^^  " + video);
//			proDoc.setDocument(npProgrammeService.getVideo(video));
            if (video != null)
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  file uploaded  "
                        + video.getName());
            }
            else
            {
                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    file not available");
            }
        }
        catch (NullPointerException npe)
        {
            System.out.println("Getting File or Video as Null");
        }
        npClientProject.setNpClientCostCentreImpl(costCenter);
        NpClientProject npClientProject1 = getLightWeightProject(npProjectService.saveOrUpdate(npClientProject));
        return npClientProject1;
    }

    @RequestMapping(value = "/checkCode", method = RequestMethod.GET)
    public
    @ResponseBody
    String checkProjectCode(@RequestParam("str") String str, HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        String result = npProjectService.checkProjectCode(str, npClient);
        return result;
    }

    public List<NpClientProject> getLightWeightPramsList(List<NpClientProject> npClientProjectList)
    {
        List<NpClientProject> projects1 = new ArrayList<NpClientProject>();
        for (int i = 0; i < npClientProjectList.size(); i++)
        {
            NpClientProject npClientProject = getLightWeightProject(npClientProjectList.get(i));
            projects1.add(npClientProject);
        }
        return projects1;
    }

    public NpClientProject getLightWeightProject(NpClientProject npClientProject)
    {
        NpClientProject npClientProject1 = new NpClientProject();
        npClientProject1.setClientProjectID(npClientProject.getClientProjectID());
        npClientProject1.setProjectCode(npClientProject.getProjectCode());
        npClientProject1.setProjectDescription(npClientProject.getProjectDescription());
        npClientProject1.setProjectOverview(npClientProject.getProjectOverview());
        npClientProject1.setProjectOwner(npClientProject.getProjectOwner());
        npClientProject1.setVideoUrl(npClientProject.getVideoUrl());

        NpClientCostCentreImpl npClientCostCentreImpl = new NpClientCostCentreImpl();
        npClientCostCentreImpl.setCostCentreId(npClientProject.getNpClientCostCentreImpl().getCostCentreId());
        npClientCostCentreImpl.setCostCentreCode(npClientProject.getNpClientCostCentreImpl().getCostCentreCode());
        npClientCostCentreImpl.setCostCentreName(npClientProject.getNpClientCostCentreImpl().getCostCentreName());
        npClientCostCentreImpl.setCostCentreLocation(npClientProject.getNpClientCostCentreImpl().getCostCentreLocation());
        npClientCostCentreImpl.setCostCentreOwner(npClientProject.getNpClientCostCentreImpl().getCostCentreOwner());
        npClientProject1.setNpClientCostCentreImpl(npClientCostCentreImpl);
        return npClientProject1;
    }
}
