/**
 *
 */
package com.newposition.ajax.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.newposition.client.service.NpClientSupportAddEditUserService;

@Controller
public class NpAdminUserAjaxController
{
    @Autowired
    private NpClientSupportAddEditUserService npClientSupportAddEditUserService;

    @RequestMapping("getAdminRoles")
    public
    @ResponseBody
    List<String> getRoles(HttpServletRequest request)
    {
        List<String> jsonOfRoles = npClientSupportAddEditUserService.getNpRoles();
        return jsonOfRoles;
    }

    @RequestMapping("getClientRoles")
    public
    @ResponseBody
    List<String> getClientRoles(HttpServletRequest request)
    {
        List<String> jsonOfRoles = npClientSupportAddEditUserService.getNpClientRoles();
        return jsonOfRoles;
    }
}
