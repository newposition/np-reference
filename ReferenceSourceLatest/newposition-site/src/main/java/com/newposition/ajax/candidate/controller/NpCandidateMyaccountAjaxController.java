package com.newposition.ajax.candidate.controller;

import info.magnolia.cms.beans.runtime.Document;
import info.magnolia.context.MgnlContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateDocument;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateEmployerImpl;
import com.newposition.candidate.domain.NpCandidateImpl;
import com.newposition.candidate.domain.NpCandidatePreference;
import com.newposition.candidate.forms.NpCandidateCompensationForm;
import com.newposition.candidate.forms.NpCandidateReferenceForm;
import com.newposition.candidate.service.NpCandidateMyaccountService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.candidate.service.NpCandidateSponsorService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.domain.NpUserImpl;
import com.newposition.common.service.NpUserService;

@Controller
public class NpCandidateMyaccountAjaxController
{
    protected static final Logger LOG = Logger
            .getLogger(NpCandidateMyaccountAjaxController.class);

    @Autowired
    NpCandidateSponsorService npCandidateSponsorService;

	/*@Autowired
    NpCandidateReferenceFormValidator candidateReferenceFormValidator;*/

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpCandidateMyaccountService npCandidateMyaccountService;

    @RequestMapping(value = "/mySponsor", method = RequestMethod.POST)
    @ResponseBody
    public String rendering(NpCandidateReferenceForm npCandidateReferenceModel,
                            HttpServletRequest request, ModelMap model, String sponserId, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

//		LOG.info("POST method getting NpCandidateObject: "+npCandidate);

        // if user edits data
        if (StringUtils.isNotEmpty(sponserId))
        {

            npCandidateReferenceModel.setId(Integer.parseInt(sponserId));
        }
        else if (npCandidateSponsorService.isSponsorEmailExist(npCandidateReferenceModel.getEmail(), npCandidate))
        {
            return "false";
        }
        npCandidateReferenceModel.setNpCandidate(npCandidate);

        if (npCandidateReferenceModel != null)
        {

            npCandidateSponsorService.saveMySponsorDetail(npCandidateReferenceModel);
        }

        return "true";
    }

    public boolean notValidDate(String fromDate, String toDate)
    {
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        Date from = null, to = null;
        try
        {
            from = formater.parse(fromDate);

            if (from.after(new Date()))
            {

                return true;
            }
            if (StringUtils.isNotEmpty(toDate) && !"Current".equalsIgnoreCase(toDate))
            {

                to = formater.parse(toDate);

                if (to.before(from))
                {

                    return true;
                }
            }
            LOG.info("from " + from + " to - " + to);
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            LOG.error("Problem in Parsing From/To date:  " + e.getStackTrace() + toDate);

            return true;
        }

        return false;
    }

    @RequestMapping(value = "/uploadCV", method = RequestMethod.POST)
    public String uploadCV(HttpServletRequest request, ModelMap model, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        Map<String, Document> listOfFile = MgnlContext.getPostedForm().getDocuments();
        String rootPath = request.getContextPath();
        String fileNameWithExtension = null;
        String fileSavedPath = null;
        File cv = null;
        if (listOfFile.get("cvFile") != null)
        {
            cv = listOfFile.get("cvFile").getFile();
        }
        else
        {
            LOG.error("CV is not Entered");
        }
        if (cv != null)
        {
            fileNameWithExtension = npCandidate.getFirstName() + npCandidate.getId() + "_cv." + listOfFile.get("cvFile").getExtension();
            fileSavedPath = saveFile(rootPath, cv, fileNameWithExtension);
            if (!StringUtils.isEmpty(fileSavedPath))
            {
                LOG.info("cv- " + fileSavedPath);
                NpCandidateDocument candidateDocument = null;
                if (npCandidate.getNpCandidateDocument() != null)
                {
                    candidateDocument = npCandidate.getNpCandidateDocument();
                }
                else
                {
                    candidateDocument = new NpCandidateDocument();
                    candidateDocument.setNpCandidate((NpCandidateImpl) npCandidate);
                }
                candidateDocument.setCv(fileSavedPath);
                npCandidateMyaccountService.updateCandidateDocument(candidateDocument);
            }
        }
        return "redirect:/myaccount";
    }

    @SuppressWarnings("resource")
    public String saveFile(String rootPath, File file, String fileNameWithExtension)
    {

        // Creating the directory to store file

        File dir = new File(rootPath + File.separator + "DocFiles");
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        // Create the file on server

        File serverFile = new File(dir.getAbsolutePath() + File.separator);
        LOG.info("Server File Location=" + serverFile.getAbsolutePath());

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try
        {
            inputStream = new FileInputStream(file.getAbsolutePath());
            outputStream = new FileOutputStream(serverFile + "\\" + fileNameWithExtension);

            Integer c;
            //continue reading till the end of the file
            while ((c = inputStream.read()) != -1)
            {
                //writes to the output Stream
                outputStream.write(c);
            }
            LOG.info("Photo is saved in : " + serverFile);
        }
        catch (FileNotFoundException e)
        {
            // TODO Auto-generated catch block
            LOG.error("File Not Exist " + e);
        }
        catch (IOException e)
        {
            LOG.error("Error Occur while saving the file" + e);
        }
        LOG.info("path returned: " + serverFile.getAbsolutePath() + "\\" + fileNameWithExtension);
        return serverFile.getAbsolutePath() + "\\" + fileNameWithExtension;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void updateNpUsers(NpUserImpl npUsers, HttpServletRequest request, Authentication authentication)
    {
        NpUser user = npUserService.findByUserName(authentication.getName());

        if (user != null)
        {
            user.setFirstName(npUsers.getFirstName());
            user.setLastName(npUsers.getLastName());
            user.setPhone(npUsers.getPhone());
            npUserService.updateNpUser(user);
        }
    }

    @RequestMapping(value = "saveAchievements", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void saveAchievements(String achievements, int uid, HttpServletRequest request)
    {
//	    	NpCandidate npCandidate(NpCandidate)request.getSession().getAttribute("npCandidate")).getId();
        NpCandidateEmployer candidateEmployer = new NpCandidateEmployerImpl();
        candidateEmployer.setUid(uid);
        candidateEmployer.setAchievements(achievements);
        npCandidateMyaccountService.updateCandidateEmployer(candidateEmployer);
    }

    @RequestMapping(value = "savePersonalStatement", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void savePersonalStatement(String personalStatement, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());
        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        npCandidate.setPersonalStatement(personalStatement);
        npCandidateMyaccountService.updateCandidate((NpCandidateImpl) npCandidate);
    }

    @RequestMapping(value = "saveCandidateCompensation", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void saveCandidateCompensation(NpCandidateCompensationForm candidateCompensationForm, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        NpCandidatePreference candidatePreference = new NpCandidatePreference();
        candidatePreference.setType(candidateCompensationForm.getCompensationType());
        candidatePreference.setDayRate(candidateCompensationForm.getDayrate());
        candidatePreference.setBonus(candidateCompensationForm.getBonus());
        candidatePreference.setCompensation(candidateCompensationForm.getSalary());
        candidatePreference.setNpCandidate((NpCandidateImpl) npCandidate);
        npCandidateMyaccountService.saveCandidatePreference(candidatePreference);
    }

    @RequestMapping(value = "myStatus", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void saveAvailiabilityStatus(String status, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        npCandidate.setAvailableStatus(status);
        npCandidateMyaccountService.updateCandidate((NpCandidateImpl) npCandidate);
    }
}
