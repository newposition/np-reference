package com.newposition.ajax.admin.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.CertificationManagementService;
import com.newposition.common.domain.CoCertificationAliasImpl;
import com.newposition.common.domain.NpInstituteCertificateImpl;

@Controller
public class CertificationManagementAjaxController
{

    protected static final Logger LOG = Logger
            .getLogger(CertificationManagementAjaxController.class);

    @Autowired
    private CertificationManagementService certificationManagementService;

    @RequestMapping(value = "/certificateManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpInstituteCertificates(HttpServletRequest request, String mergeId)
    {
        final String searchCode = request.getParameter("searchCode");
        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpInstituteCertificateImpl> npInstituteCertificates = certificationManagementService.getNpInstituteCertificateforAliasSearch(searchCode, mergeId);

            request.setAttribute("npInstituteCertificates", npInstituteCertificates);
            request.setAttribute("npInstitutions", certificationManagementService.getNpInstitutions());
            request.setAttribute("npCertificationTypes", certificationManagementService.getCertificationTypes());
            request.setAttribute("coSources", certificationManagementService.getCoSources());
            request.setAttribute("mergeId", mergeId);
        }
        return "aliasSearchNpInstituteCertificateResult";
    }

    @RequestMapping(value = "/certificateManagement/deleteAlias", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeCoCertificateAlias(String id)
    {
        if (StringUtils.isNotBlank(id))
        {
            certificationManagementService.removeCoCertificateAlias(Integer.parseInt(id));
        }
    }

    @RequestMapping(value = "/certificateManagement/removeNpInstituteCertificate", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeNpInstituteCertificate(String npInstituteCertificateId)
    {
        if (StringUtils.isNotBlank(npInstituteCertificateId))
        {
            LOG.info("Removing the npInstituteCertificate with " + npInstituteCertificateId);
            certificationManagementService.removeNpInstituteCertificateById(Integer.parseInt(npInstituteCertificateId));
        }
    }

    @RequestMapping(value = "/certificateManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoCertificationAliasImpl> coCertificationAliasSet = certificationManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoCertificationAliasImpl coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getCertificationAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getCertificationAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", certificationManagementService.getCoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
