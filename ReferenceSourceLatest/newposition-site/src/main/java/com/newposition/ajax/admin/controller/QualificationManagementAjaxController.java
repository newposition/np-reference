package com.newposition.ajax.admin.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.domain.CoCourseAliasImpl;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.QualificationManagementService;
import com.newposition.candidate.domain.NpInstituteCourse;
import com.newposition.common.domain.CoCertificationAliasImpl;

@Controller
public class QualificationManagementAjaxController
{

    protected static final Logger LOG = Logger
            .getLogger(QualificationManagementAjaxController.class);

    @Autowired
    private QualificationManagementService qualificationManagementService;

    @RequestMapping(value = "/qualificationManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpInstituteCourses(HttpServletRequest request, String mergeId)
    {
        final String searchCode = request.getParameter("searchCode");
        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpInstituteCourse> npInstituteCourses = qualificationManagementService.getNpInstituteCourseforAliasSearch(searchCode, mergeId);

            request.setAttribute("npInstituteCourses", npInstituteCourses);
            request.setAttribute("npInstitutions", qualificationManagementService.getNpInstitutions());
            request.setAttribute("npCourseTypes", qualificationManagementService.getCourseTypes());
            request.setAttribute("coSources", qualificationManagementService.getCoSources());
            request.setAttribute("mergeId", mergeId);
        }
        return "aliasSearchNpInstituteCourseResult";
    }

    /* @RequestMapping(value = "saveMergedNpInstituteCourses", method = RequestMethod.POST)
     @ResponseStatus(value=HttpStatus.OK)
     public void mergeNpInstituteCourses(HttpServletRequest request,String targetId,String sourceId) {
           NpInstituteCourse npInstituteCourseTarget;
           NpInstituteCourse npInstituteCourseSource ;
         if(StringUtils.isNotBlank(targetId) && StringUtils.isNotBlank(sourceId))
         {
               npInstituteCourseTarget = qualificationManagementService.getNpInstituteCourseById(Integer.parseInt(targetId));


               npInstituteCourseSource = qualificationManagementService.getNpInstituteCourseById(Integer.parseInt(sourceId));

               npInstituteCourseTarget.getNpCourse().setCourseName(npInstituteCourseSource.getNpCertifications().getCourseName());

               npInstituteCourseTarget.getNpCertifications().setCourseType(npInstituteCourseSource.getNpCertifications().getCourseType());

               npInstituteCourseTarget.setNpInstitutions(npInstituteCourseSource.getNpInstitutions());
               Set<CoCertificationAliasImpl> coCertificationAlias=npInstituteCourseTarget.getNpCertifications().getCoCertificationAlias();
               coCertificationAlias.addAll(npInstituteCourseSource.getNpCertifications().getCoCertificationAlias());
               npInstituteCourseTarget.getNpCertifications().setCoCertificationAlias(coCertificationAlias);
               npInstituteCourseTarget.getNpCertifications().getCoCertificationAlias().addAll(npInstituteCourseSource.getNpCertifications().getCoCertificationAlias());
               npInstituteCourseTarget.getNpCertifications().setValid(npInstituteCourseSource.getNpCertifications().getValid());

               AddNpCertificationForm addNpCertificationForm=new AddNpCertificationForm();
               addNpCertificationForm.setNpInstituteCourseId(npInstituteCourseTarget.getId());
               addNpCertificationForm.setCertificationName(npInstituteCourseTarget.getNpCertifications().getCourseName());
               addNpCertificationForm.setCertificationType( npInstituteCourseTarget.getNpCertifications().getCourseType());
               addNpCertificationForm.setCertificationAliases(certificationAliases);
               qualificationManagementService.saveMergedNpInstituteCourse(npInstituteCourseTarget);
         }



     }
*/
    @RequestMapping(value = "/qualificationManagement/removeCoCourse", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeCoCourseAlias(String id)
    {
        if (StringUtils.isNotBlank(id))
        {
            qualificationManagementService.removeCoCourseAlias(Integer.parseInt(id));
        }
    }

    @RequestMapping(value = "/qualificationManagement/removeNpInstituteCourse", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeNpInstituteCourse(String npInstituteCourseId)
    {
        if (StringUtils.isNotBlank(npInstituteCourseId))
        {
            LOG.info("Removing the npInstituteCourse with " + npInstituteCourseId);
            qualificationManagementService.removeNpInstituteCourseById(Integer.parseInt(npInstituteCourseId));
        }
    }

    @RequestMapping(value = "/qualificationManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoCourseAliasImpl> coCertificationAliasSet = qualificationManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoCourseAliasImpl coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getCourseAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getCourseAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", qualificationManagementService.getCoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
