package com.newposition.ajax.admin.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.newposition.admin.domain.CoEmployerAliasImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.forms.AddNpIndustriesForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.forms.IndustryAliases;
import com.newposition.admin.service.NpAdminIndustryManagementService;
import com.newposition.common.domain.NpEmployerImpl;

@Controller
public class NpAdminIndustryManagementAjaxController
{

    @Autowired
    private NpAdminIndustryManagementService npAdminIndustryManagementService;

    @RequestMapping(value = "/industryManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpTools(HttpServletRequest request)
    {
        final String searchCode = request.getParameter("searchCode");
        final String mergeTo = request.getParameter("mergeToId");

        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpEmployerImpl> npEmployersSet = npAdminIndustryManagementService.getNpIndustriesforAliasSearch(searchCode, mergeTo);
            List<NpEmployerImpl> npEmployers = new ArrayList<NpEmployerImpl>(npEmployersSet);
            request.setAttribute("npEmployers", npEmployers);
            request.setAttribute("addNpIndustriesForm", new AddNpIndustriesForm());

            request.setAttribute("npIndustryTypes", npAdminIndustryManagementService.getIndustryTypes());
            request.setAttribute("coSources", npAdminIndustryManagementService.getcoSources());

            request.setAttribute("industryAliases", new IndustryAliases());
            request.setAttribute("mergeTo", mergeTo);
        }
        return "aliasSearchNpIndustriesResult";
    }

    @RequestMapping(value = "/industryManagement/deleteAlias", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeAliasRow(HttpServletRequest request)
    {

        final String aliasId = request.getParameter("id");
        if (StringUtils.isNotBlank(aliasId))
        {
            npAdminIndustryManagementService.deleteCoAliasIndustries(aliasId);
        }
    }

    @RequestMapping(value = "/industryManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoEmployerAliasImpl> coCertificationAliasSet = npAdminIndustryManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoEmployerAliasImpl coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getEmployerAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getEmployerAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", npAdminIndustryManagementService.getcoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
