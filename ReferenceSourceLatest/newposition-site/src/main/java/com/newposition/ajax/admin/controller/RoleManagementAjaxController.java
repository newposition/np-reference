package com.newposition.ajax.admin.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.forms.AddNpEmploymentRoleForm;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.forms.RoleAliases;
import com.newposition.admin.forms.SkillAliases;
import com.newposition.admin.service.RoleManagementService;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.CoRoleAlias;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.NpEmploymentRoleImpl;
import com.newposition.common.domain.NpSkillImpl;

@Controller
public class RoleManagementAjaxController
{
    protected static final Logger LOG = Logger
            .getLogger(RoleManagementAjaxController.class);

    @Autowired
    private RoleManagementService roleManagementService;

    @RequestMapping(value = "/roleManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpSKills(HttpServletRequest request)
    {
        final String searchCode = request.getParameter("searchCode");
        final String mergeTo = request.getParameter("mergeToId");

        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpEmploymentRoleImpl> npEmploymentRoleSet = roleManagementService.getNpEmploymentRoleforAliasSearch(searchCode, mergeTo);
            List<NpEmploymentRoleImpl> npEmploymentRoles = new ArrayList<NpEmploymentRoleImpl>(npEmploymentRoleSet);
            request.setAttribute("npEmploymentRoles", npEmploymentRoles);
            request.setAttribute("addNpEmploymentRoleForm", new AddNpEmploymentRoleForm());

            request.setAttribute("npDomainTypes", roleManagementService.getDomainTypes());
            request.setAttribute("npRoleNames", roleManagementService.getNpEmploymentRoleNames());
            request.setAttribute("coSources", roleManagementService.getcoSources());

            request.setAttribute("roleAliases", new RoleAliases());
            request.setAttribute("mergeTo", mergeTo);
        }
        return "aliasSearchNpEmploymentRolesResult";
    }

    @RequestMapping(value = "/roleManagement/addAalias", method = RequestMethod.POST)
    public String addAliasRow(HttpServletRequest request)
    {

        final int index = Integer.parseInt(request.getParameter("index"));
        final String type = request.getParameter("type");
        final String addOrEdit = request.getParameter("addOrEdit");
        request.setAttribute("index", index);
        request.setAttribute("type", type);
        request.setAttribute("addOrEdit", addOrEdit);
        request.setAttribute("coSources", roleManagementService.getcoSources());
        return "addAlias";
    }

    @RequestMapping(value = "/roleManagement/deleteAlias", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeAliasRow(HttpServletRequest request)
    {

        final String aliasId = request.getParameter("id");
        if (StringUtils.isNotBlank(aliasId))
        {
            roleManagementService.deleteCoAliasRoles(aliasId);
        }
    }

    @RequestMapping(value = "/roleManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoRoleAlias> coCertificationAliasSet = roleManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoRoleAlias coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getRoleAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getRoleAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", roleManagementService.getcoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
