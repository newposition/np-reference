/**
 *
 */
package com.newposition.ajax.candidate.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.domain.NpCandidateEmpRoles;
import com.newposition.candidate.domain.NpCandidateEmployer;
import com.newposition.candidate.domain.NpCandidateEmployerImpl;
import com.newposition.candidate.domain.NpCandidateSkills;
import com.newposition.candidate.domain.NpCandidateTools;
import com.newposition.candidate.forms.NpCandidateEmploymentForm;
import com.newposition.candidate.forms.NpCandidateRolesCommand;
import com.newposition.candidate.forms.NpCandidateRolesForm;
import com.newposition.candidate.forms.NpCandidateSkillForm;
import com.newposition.candidate.forms.NpCandidateSkillsCommand;
import com.newposition.candidate.forms.NpCandidateToolsCommand;
import com.newposition.candidate.forms.NpCandidateToolsForm;
import com.newposition.candidate.service.NpCandidateEmploymentService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpEmploymentRole;
import com.newposition.common.domain.NpSkill;
import com.newposition.common.domain.NpTool;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
public class NpCandidateEmploymentAjaxController
{

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpCandidateEmploymentService npCandidateEmploymentService;

    protected static final Logger LOG = Logger.getLogger(NpCandidateEmploymentAjaxController.class);

    @ResponseBody
    @RequestMapping("skills")
    public String getSkills(HttpServletRequest request)
    {

        List<NpSkill> listOfNpSkills = npCandidateEmploymentService.getSkills(request.getParameter("id"));

        List<NpCandidateSkillForm> npCandidateSkillForms = new ArrayList<NpCandidateSkillForm>();

        for (NpSkill npSkills : listOfNpSkills)
        {

            NpCandidateSkillForm npCandidateSkillForm = new NpCandidateSkillForm();
            npCandidateSkillForm.setId(npSkills.getId());
            npCandidateSkillForm.setSkill(npSkills.getSkillName());
            npCandidateSkillForms.add(npCandidateSkillForm);
        }
        String json = "";

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            json = mapper.writeValueAsString(npCandidateSkillForms);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return json;
    }

    @ResponseBody
    @RequestMapping("roles")
    public String getRoles(HttpServletRequest request)
    {

        List<NpEmploymentRole> listOfNpEmploymentRoles = npCandidateEmploymentService.getRoles(request.getParameter("id"));

        List<NpCandidateRolesForm> npCandidateRolesForms = new ArrayList<NpCandidateRolesForm>();

        for (NpEmploymentRole npEmploymentRoles : listOfNpEmploymentRoles)
        {

            NpCandidateRolesForm npCandidateRolesForm = new NpCandidateRolesForm();
            npCandidateRolesForm.setId(npEmploymentRoles.getId());
            npCandidateRolesForm.setRoleName(npEmploymentRoles.getRoleName());
            npCandidateRolesForms.add(npCandidateRolesForm);
        }
        String json = "";

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            json = mapper.writeValueAsString(npCandidateRolesForms);
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return json;
    }

    @ResponseBody
    @RequestMapping("tools")
    public String getTools(String id)
    {

        List<NpTool> listOfNpTools = npCandidateEmploymentService.getTools(id);

        List<NpCandidateToolsForm> npCandidateToolsForms = new ArrayList<NpCandidateToolsForm>();

        for (NpTool npTools : listOfNpTools)
        {

            NpCandidateToolsForm npCandidateToolsForm = new NpCandidateToolsForm();
            npCandidateToolsForm.setId(npTools.getId());
            npCandidateToolsForm.setToolName(npTools.getToolName());
            npCandidateToolsForms.add(npCandidateToolsForm);
        }
        String json = "";

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            json = mapper.writeValueAsString(npCandidateToolsForms);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return json;
    }

    @RequestMapping("saveCompany")
    @ResponseBody
    public ModelAndView saveCompany(NpCandidateEmploymentForm npCandidateEmploymentForm, HttpServletRequest request,
                                    Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateEmploymentService.saveEmployment(convertEmploymentForm(request), npCandidate);

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npUser.getId().intValue());

        request.getSession().setAttribute("npCandidateEmployers", npCandidateEmployers);

        ModelAndView modelAndView = new ModelAndView();

        if (npCandidateEmployers.size() == 1)
        {
            modelAndView.addObject("notShowNext", true);
            modelAndView.addObject("notShowPrev", true);
        }

        if (npCandidateEmployers.size() > 1)
        {
            modelAndView.addObject("notShowNext", true);
            modelAndView.addObject("notShowPrev", false);
            modelAndView.addObject("prevIds", npCandidateEmployers.size() - 2);
        }

        modelAndView.addObject("employer", npCandidateEmployers.get(npCandidateEmployers.size() - 1));
        modelAndView.setViewName("nextEmployment");
        return modelAndView;
    }

    public NpCandidateEmploymentForm convertEmploymentForm(HttpServletRequest request)
    {

        NpCandidateEmploymentForm employmentForm = new NpCandidateEmploymentForm();

        employmentForm.setEmployerName(request.getParameter("company"));
        employmentForm.setJobTitle(request.getParameter("jobtitle"));
        employmentForm.setStartDate(request.getParameter("df"));
        employmentForm.setEndDate(request.getParameter("dt"));
        employmentForm.setAchievements(request.getParameter("achievements"));
        employmentForm.setCorpTitle(request.getParameter("corptitle"));
        employmentForm.setEmployerId(request.getParameter("employerId"));

        return employmentForm;
    }

    @RequestMapping("deleteCompany")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteCompany(String employerId, HttpServletRequest request, Authentication authentication)
    {

        npCandidateEmploymentService.deleteEmployment(Integer.parseInt(employerId));

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate.getNpCandidateEmployers().size() == 0)
        {
            npCandidateEmploymentService.deleteCandidateTechnicalDetails(npCandidate);
        }
    }

    @RequestMapping("updateCompany")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public void updateCompany(HttpServletRequest request)
    {

        npCandidateEmploymentService.updateEmployment(convertEmploymentForm(request));
    }

    @RequestMapping("skillsModalPopup")
    public String test(HttpServletRequest request, String candidateEmployerId,
                       @RequestParam(value = "skills[]", required = false) String[] skills,
                       @RequestParam(value = "companyName", required = false) String companyName,
                       @RequestParam(value = "skillIds[]", required = false) String[] skillIds,
                       @RequestParam(value = "domaintype", required = false) String domainType)
    {

        List<NpCandidateSkillForm> npCandidateSkillForms = new ArrayList<NpCandidateSkillForm>();

        NpCandidateSkillForm npCandidateSkillForm = null;

        int iterator = 0;
        for (String string : skills)
        {
            npCandidateSkillForm = new NpCandidateSkillForm();
            npCandidateSkillForm.setSkill(string);
            npCandidateSkillForm.setCompanyName(companyName);
            npCandidateSkillForm.setCandidateEmployerId(candidateEmployerId);
            npCandidateSkillForm.setId(Integer.parseInt(skillIds[iterator]));
            npCandidateSkillForm.setDomainType(domainType);
            npCandidateSkillForms.add(npCandidateSkillForm);

            iterator++;
        }

        NpCandidateSkillsCommand npCandidateSkillsCommand = new NpCandidateSkillsCommand();

        npCandidateSkillsCommand.setSkills(npCandidateSkillForms);

        request.setAttribute("skillCombinedCommand", npCandidateSkillsCommand);
        request.setAttribute("companyName", companyName);

        return "skillsForm";
    }

    @RequestMapping(value = "skillsSubmit", produces = "application/json")
    public String skillsSubmit(NpCandidateSkillsCommand npCandidateSkillsCommand, HttpServletRequest request, Authentication authentication)
    {

        List<NpCandidateSkillForm> npCandidateSkillForms = npCandidateSkillsCommand.getSkills();

        List<NpCandidateSkillForm> companySkills = new ArrayList<NpCandidateSkillForm>();

        for (NpCandidateSkillForm npCandidateSkillForm : npCandidateSkillForms)
        {

            if (npCandidateSkillForm.isInCurrentCompany())
            {
                companySkills.add(npCandidateSkillForm);
            }
        }

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        for (NpCandidateSkillForm npCandidateSkillForm : npCandidateSkillForms)
        {
            String skill = npCandidateSkillForm.getSkill() + " " + npCandidateSkillForm.getExperienceLevel() + " " + npCandidateSkillForm.getExpertLevel();
            npCandidateEmploymentService.saveCandidateSkill(skill, npCandidate.getId().intValue(), npCandidateSkillForm.getCandidateEmployerId(),
                    npCandidateSkillForm.getDomainType(), npCandidateSkillForm.isInCurrentCompany());
        }

        // employmentService.saveCandidateSkills(skillCombinedCommand,npCandidate.getId());

        npCandidate = npCandidateService.findByID(npUser.getId());

        Set<NpCandidateSkills> candidateSkills = npCandidate.getNpCandidateSkills();

        List<NpCandidateSkillForm> npCandidateSkillsForm = new ArrayList<NpCandidateSkillForm>();

        for (NpCandidateSkills npCandidateSkill : candidateSkills)
        {
            NpCandidateSkillForm npCandidateSkillForm = new NpCandidateSkillForm();
            npCandidateSkillForm.setId(npCandidateSkill.getId().getSkillId());
            npCandidateSkillForm.setExpertLevel(npCandidateSkill.getNpExpertLevel().getExpertLevelName());
            npCandidateSkillForm.setSkill(npCandidateSkill.getNpSkill().getSkillName());
            npCandidateSkillForm.setExperienceLevel(Integer.toString(npCandidateSkill.getYearsOfExperience()));
            npCandidateSkillsForm.add(npCandidateSkillForm);
        }

        Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandidate.getNpCandidateEmployers();

        for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
        {

            if (Integer.parseInt(npCandidateSkillsCommand.getSkills().get(0).getCandidateEmployerId()) == (npCandidateEmployer.getUid()))
            {

                request.setAttribute("companySkills", npCandidateEmployer.getNpSkills());
            }
        }

        request.setAttribute("skills", npCandidateSkillsForm);
        request.setAttribute("validData", true);

        return "skillBasket";
    }

    @RequestMapping(value = "rolesSubmit", produces = "application/json")
    public String rolesSubmit(NpCandidateRolesCommand rolesCombinedCommand, HttpServletRequest request, Authentication authentication)
    {

        List<NpCandidateRolesForm> roles = rolesCombinedCommand.getRoles();

        List<NpCandidateRolesForm> companyRoles = new ArrayList<NpCandidateRolesForm>();

        for (NpCandidateRolesForm npCandidateRolesForm : roles)
        {

            if (npCandidateRolesForm.isInCurrentCompany())
            {
                companyRoles.add(npCandidateRolesForm);
            }
        }
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        for (NpCandidateRolesForm roleModel : roles)
        {
            String role = roleModel.getRoleName() + " " + roleModel.getExperienceLevel();
            npCandidateEmploymentService.saveCandidateRole(role, npCandidate.getId().intValue(), roleModel.getCandidateEmployerId(),
                    roleModel.getDomainType(), roleModel.isInCurrentCompany());
        }
        // employmentService.saveCandidateRoles(rolesCombinedCommand,
        // npCandidate.getId());

        npCandidate = npCandidateService.findByID(npUser.getId());

        Set<NpCandidateEmpRoles> candidateRoles = npCandidate.getNpCandidateEmpRoles();

        List<NpCandidateRolesForm> npCandidateRolesForms = new ArrayList<NpCandidateRolesForm>();

        for (NpCandidateEmpRoles npCandidateRole : candidateRoles)
        {
            NpCandidateRolesForm npCandidateRolesForm = new NpCandidateRolesForm();
            npCandidateRolesForm.setId(npCandidateRole.getId().getEmploymentRoleId());
            // rolesModel.setExpertLevel(npCandidateRole.getNpExpertLevel().getExpertLevelName());
            npCandidateRolesForm.setRoleName(npCandidateRole.getNpEmploymentRoles().getRoleName());
            npCandidateRolesForm.setExperienceLevel(Integer.toString(npCandidateRole.getYearsOfExperience()));
            npCandidateRolesForms.add(npCandidateRolesForm);
        }

        Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandidate.getNpCandidateEmployers();

        for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
        {

            if (Integer.parseInt(roles.get(0).getCandidateEmployerId()) == (npCandidateEmployer.getUid()))
            {

                request.setAttribute("companyRoles", npCandidateEmployer.getNpEmploymentRoles());
            }
        }

        request.setAttribute("roles", npCandidateRolesForms);
        request.setAttribute("validData", true);

        return "roleBasket";
    }

    @RequestMapping(value = "toolsSubmit", produces = "application/json")
    public String toolsSubmit(NpCandidateToolsCommand toolCombinedCommand, HttpServletRequest request, Authentication authentication)
    {

        List<NpCandidateToolsForm> tools = toolCombinedCommand.getTools();

        List<NpCandidateToolsForm> companyTools = new ArrayList<NpCandidateToolsForm>();

        System.out.println("tools has to add : ");
        for (NpCandidateToolsForm npCandidateToolsForm : tools)
        {

            if (npCandidateToolsForm.isInCurrentCompany())
            {
                companyTools.add(npCandidateToolsForm);
            }
        }
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        // employmentService.saveCandidateTools(toolCombinedCommand,
        // npCandidate.getId());
        for (NpCandidateToolsForm toolsModel : tools)
        {
            String tool = toolsModel.getToolName() + " " + toolsModel.getExperienceLevel() + " " + toolsModel.getExpertLevel();
            npCandidateEmploymentService.saveCandidateTool(tool, npCandidate.getId().intValue(), toolsModel.getCandidateEmployerId(),
                    toolsModel.getDomainType(), toolsModel.isInCurrentCompany());
        }
        NpCandidate npCandiate = npCandidate = npCandidateService.findByID(npUser.getId());

        Set<NpCandidateTools> candidateTools = npCandiate.getNpCandidateTools();

        List<NpCandidateToolsForm> npCandidateToolsForms = new ArrayList<NpCandidateToolsForm>();

        for (NpCandidateTools npCandidateTool : candidateTools)
        {
            NpCandidateToolsForm npCandidateToolsForm = new NpCandidateToolsForm();
            npCandidateToolsForm.setId(npCandidateTool.getId().getToolId());
            npCandidateToolsForm.setExpertLevel(npCandidateTool.getNpExpertLevel().getExpertLevelName());
            npCandidateToolsForm.setToolName(npCandidateTool.getNpTools().getToolName());
            npCandidateToolsForm.setExperienceLevel(Integer.toString(npCandidateTool.getYearsOfExperience()));
            npCandidateToolsForms.add(npCandidateToolsForm);
        }

        Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandidate.getNpCandidateEmployers();

        for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
        {

            if (Integer.parseInt(tools.get(0).getCandidateEmployerId()) == (npCandidateEmployer.getUid()))
            {

                request.setAttribute("companyTools", npCandidateEmployer.getNpTools());
            }
        }

        // request.setAttribute("companyTools", companyTools);

        request.setAttribute("tools", npCandidateToolsForms);
        request.setAttribute("validData", true);

        return "toolBasket";
    }

    @RequestMapping("rolesModalPopup")
    public String roles(HttpServletRequest request, String candidateEmployerId,
                        @RequestParam(value = "objectValues[]", required = false) String[] objectValues,
                        @RequestParam(value = "companyName", required = false) String companyName,
                        @RequestParam(value = "rolesIds[]", required = false) String[] rolesIds,
                        @RequestParam(value = "domainType", required = false) String domainType)
    {

        List<NpCandidateRolesForm> roles = new ArrayList<NpCandidateRolesForm>();

        NpCandidateRolesForm npCandidateRolesForm = null;
        int iterator = 0;
        for (String string : objectValues)
        {

            npCandidateRolesForm = new NpCandidateRolesForm();
            npCandidateRolesForm.setRoleName(string);
            npCandidateRolesForm.setCompanyName(companyName);
            npCandidateRolesForm.setCandidateEmployerId(candidateEmployerId);
            npCandidateRolesForm.setDomainType(domainType);
            npCandidateRolesForm.setId(Integer.parseInt(rolesIds[iterator]));
            roles.add(npCandidateRolesForm);

            iterator++;
        }

        NpCandidateRolesCommand roleForm = new NpCandidateRolesCommand();

        roleForm.setRoles(roles);

        request.setAttribute("roleForm", roleForm);
        request.setAttribute("companyName", companyName);

        return "rolesForm";
    }

    @RequestMapping("toolsModalPopup")
    public String tools(HttpServletRequest request, String candidateEmployerId,
                        @RequestParam(value = "objectValues[]", required = false) String[] objectValues,
                        @RequestParam(value = "companyName", required = false) String companyName,
                        @RequestParam(value = "toolsIds[]", required = false) String[] toolsIds,
                        @RequestParam(value = "domainType", required = false) String domainType)
    {

        List<NpCandidateToolsForm> tools = new ArrayList<NpCandidateToolsForm>();

        NpCandidateToolsForm npCandidateToolsForm = null;

        int iterator = 0;
        for (String string : objectValues)
        {

            npCandidateToolsForm = new NpCandidateToolsForm();
            npCandidateToolsForm.setToolName(string);
            npCandidateToolsForm.setCompanyName(companyName);
            npCandidateToolsForm.setCandidateEmployerId(candidateEmployerId);
            npCandidateToolsForm.setDomainType(domainType);
            npCandidateToolsForm.setId(Integer.parseInt(toolsIds[iterator]));
            tools.add(npCandidateToolsForm);

            iterator++;
        }

        NpCandidateToolsCommand toolsForm = new NpCandidateToolsCommand();

        toolsForm.setTools(tools);

        request.setAttribute("toolsForm", toolsForm);
        request.setAttribute("companyName", companyName);

        return "toolsForm";
    }

    @RequestMapping("nextCandidateEmployer")
    public ModelAndView getNpCandidateEmployer(HttpServletRequest request, String nextVal, String previousVal, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npCandidate.getId().intValue());
        ModelAndView modelAndView = new ModelAndView();

        if (npCandidateEmployers != null)
        {
            if (previousVal != null)
            {
                int prevVal = Integer.parseInt(previousVal);
                if (prevVal == 0)
                {
                    modelAndView.addObject("notShowPrev", true);
                }
                modelAndView.addObject("employer", npCandidateEmployers.get(prevVal));
                modelAndView.addObject("prevIds", prevVal - 1);
                modelAndView.addObject("nextIds", prevVal + 1);
                modelAndView.setViewName("nextEmployment");
                return modelAndView;
            }
            else
            {
                int nextValInt = Integer.parseInt(nextVal);
                if (npCandidateEmployers.size() == nextValInt + 1)
                {
                    modelAndView.addObject("notShowNext", true);
                }
                modelAndView.addObject("employer", npCandidateEmployers.get(nextValInt));
                modelAndView.addObject("nextIds", nextValInt + 1);
                modelAndView.addObject("prevIds", nextValInt - 1);
                modelAndView.setViewName("nextEmployment");
                return modelAndView;
            }
        }

        return null;
    }

    @RequestMapping("currentCompanySkill")
    @ResponseBody
    public int saveCurrentCompanySkill(String employerId, String skillId)
    {
        return npCandidateEmploymentService.saveCandidateCompanySkill(employerId, skillId);
    }

    @RequestMapping("currentCompanyRole")
    @ResponseBody
    public int saveCurrentCompanyRole(String employerId, String roleId)
    {
        return npCandidateEmploymentService.saveCandidateCompanyRole(employerId, roleId);
    }

    @RequestMapping("currentCompanyTool")
    @ResponseBody
    public int saveCurrentCompanyTool(String employerId, String toolId)
    {
        return npCandidateEmploymentService.saveCandidateCompanyTool(employerId, toolId);
    }

    @RequestMapping("deleteCandidateSkill")
    public
    @ResponseBody
    String deleteCandidateSkill(String employerId, String skillId, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateEmploymentService.deleteCandidateSkill(employerId, skillId, npCandidate.getId().intValue());

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npCandidate.getId().intValue());

        request.getSession().setAttribute("npCandidateEmployers", npCandidateEmployers);

        return "deletedSucessfully";
    }

    @RequestMapping("deleteCandidateRole")
    public
    @ResponseBody
    String deleteCandidateRole(String employerId, String roleId, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateEmploymentService.deleteCandidateRole(employerId, roleId, npCandidate.getId().intValue());

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npCandidate.getId().intValue());

        request.getSession().setAttribute("npCandidateEmployers", npCandidateEmployers);

        return "deletedSucessfully";
    }

    @RequestMapping("deleteCandidateTool")
    public
    @ResponseBody
    String deleteCandidateTool(String employerId, String toolId, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateEmploymentService.deleteCandidateTool(employerId, toolId, npCandidate.getId().intValue());

        List<NpCandidateEmployer> npCandidateEmployers = npCandidateEmploymentService.getNpCandidateEmployer(npCandidate.getId().intValue());

        request.getSession().setAttribute("npCandidateEmployers", npCandidateEmployers);

        return "deletedSucessfully";
    }

    @RequestMapping("deleteCandidateCompanySkill")
    public
    @ResponseBody
    String deleteCandidateCompanySkill(String employerId, String skillId)
    {
        npCandidateEmploymentService.deleteCandidateCompanySkill(employerId, skillId);
        return "deletedSucessfully";
    }

    @RequestMapping("deleteCandidateCompanyRole")
    public
    @ResponseBody
    String deleteCandidateCompanyRole(String employerId, String roleId)
    {
        npCandidateEmploymentService.deleteCandidateCompanyRole(employerId, roleId);
        return "deletedSucessfully";
    }

    @RequestMapping("deleteCandidateCompanyTool")
    public
    @ResponseBody
    String deleteCandidateCompanyTool(String employerId, String toolId)
    {
        npCandidateEmploymentService.deleteCandidateCompanyTool(employerId, toolId);
        return "deletedSucessfully";
    }

    @RequestMapping(value = "addskill", produces = "application/json")
    public String addSkill(String addskill, String employerID, String domainType, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String[] listOfSkills = addskill.split(",");

        LOG.info("@@@ get skils  :" + listOfSkills.length);
        boolean validData = false;
        for (int i = 0; i < listOfSkills.length; i++)
        {

            validData = validateSkillData(listOfSkills[i].trim(), employerID, domainType);
            LOG.info("Token skils  :" + listOfSkills[i] + " v= " + validData);

            if (validData)
            {
                validData = npCandidateEmploymentService.saveCandidateSkill(listOfSkills[i], npCandidate.getId().intValue(), employerID, domainType, true);
            }
        }

        NpCandidate npCandiate = npCandidateEmploymentService.getNpCandidateById(npCandidate.getId().intValue());

        Set<NpCandidateSkills> candidateSkills = npCandiate.getNpCandidateSkills();

        List<NpCandidateSkillForm> npCandidateSkillForms = new ArrayList<NpCandidateSkillForm>();

        for (NpCandidateSkills npCandidateSkill : candidateSkills)
        {
            if (npCandidateSkill.getPreferfutureSkill() != 1)
            {
                NpCandidateSkillForm npCandidateSkillForm = new NpCandidateSkillForm();
                npCandidateSkillForm.setId(npCandidateSkill.getId().getSkillId());

                npCandidateSkillForm.setExpertLevel(npCandidateSkill.getNpExpertLevel().getExpertLevelName());
                npCandidateSkillForm.setSkill(npCandidateSkill.getNpSkill().getSkillName());
                npCandidateSkillForm.setExperienceLevel(Integer.toString(npCandidateSkill.getYearsOfExperience()));
                npCandidateSkillForms.add(npCandidateSkillForm);
            }
        }

        Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandiate.getNpCandidateEmployers();

        for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
        {

            if (Integer.parseInt(employerID) == (npCandidateEmployer.getUid()))
            {

                request.setAttribute("companySkills", npCandidateEmployer.getNpSkills());
            }
        }
        request.setAttribute("skills", npCandidateSkillForms);
        request.setAttribute("validData", validData);
        return "skillBasket";
    }

    @SuppressWarnings("unused")
    public boolean validateSkillData(String addskill, String employerID, String domainType)
    {

        if (StringUtils.countMatches(addskill, " ") != 2)
        {
            return false;
        }
        if (StringUtils.isEmpty(employerID))
        {
            return false;
        }
        if (StringUtils.isEmpty(domainType))
        {
            return false;
        }
        String[] skillItems = StringUtils.split(addskill, ' ');
        if (skillItems != null && skillItems.length == 2)
        {
            try
            {
                int yearofExp = Integer.parseInt(skillItems[1]);
            }
            catch (NumberFormatException exception)
            {
                return false;
            }
        }

        return true;
    }

    @SuppressWarnings("unused")
    public boolean validateRoleData(String addskill, String employerID, String domainType)
    {

        if (StringUtils.countMatches(addskill, " ") != 1)
        {
            return false;
        }
        if (StringUtils.isEmpty(employerID))
        {
            return false;
        }
        if (StringUtils.isEmpty(domainType))
        {
            return false;
        }
        String[] skillItems = StringUtils.split(addskill, ' ');
        if (skillItems != null && skillItems.length == 1)
        {
            try
            {
                int yearofExp = Integer.parseInt(skillItems[1]);
            }
            catch (NumberFormatException exception)
            {
                return false;
            }
        }

        return true;
    }

    @RequestMapping(value = "getEmployerNames", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getEmployerNames()
    {
        List<String> employerNames = npCandidateEmploymentService.getEmployerNames();

        return employerNames;
    }

    @RequestMapping(value = "addrole", produces = "application/json")
    public String addRole(String addRole, String employerID, String domainType, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String[] listOfRoles = addRole.split(",");

        LOG.info("@@@ get Role  :" + listOfRoles.length);
        boolean validData = false;
        for (int i = 0; i < listOfRoles.length; i++)
        {

            LOG.info("Token Roles  :" + listOfRoles[i] + " v= " + validData);

            validData = validateRoleData(listOfRoles[i].trim(), employerID, domainType);

            if (validData)
            {
                validData = npCandidateEmploymentService.saveCandidateRole(listOfRoles[i].trim(), npCandidate.getId().intValue(), employerID, domainType, true);
            }

            NpCandidate npCandiate = npCandidateEmploymentService.getNpCandidateById(npCandidate.getId().intValue());

            Set<NpCandidateEmpRoles> candidateRoles = npCandiate.getNpCandidateEmpRoles();

            List<NpCandidateRolesForm> rolesModels = new ArrayList<NpCandidateRolesForm>();

            for (NpCandidateEmpRoles npCandidateRole : candidateRoles)
            {
                if (npCandidateRole.getFuturePreferedRole() != 1)
                {
                    NpCandidateRolesForm rolesModel = new NpCandidateRolesForm();
                    rolesModel.setId(npCandidateRole.getId().getEmploymentRoleId());
                    // rolesModel.setExpertLevel(npCandidateRole.getNpExpertLevel().getExpertLevelName());
                    rolesModel.setRoleName(npCandidateRole.getNpEmploymentRoles().getRoleName());
                    rolesModel.setExperienceLevel(Integer.toString(npCandidateRole.getYearsOfExperience()));
                    rolesModels.add(rolesModel);
                }
            }

            Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandiate.getNpCandidateEmployers();

            for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
            {

                if (Integer.parseInt(employerID) == (npCandidateEmployer.getUid()))
                {

                    request.setAttribute("companyRoles", npCandidateEmployer.getNpEmploymentRoles());
                }
            }

            request.setAttribute("roles", rolesModels);
            request.setAttribute("validData", validData);
        }
        return "roleBasket";
    }

    @RequestMapping(value = "addtool", produces = "application/json")
    public String addtool(String addtool, String employerID, String domainType, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String[] listOfTools = addtool.split(",");

        LOG.info("@@@ get Tool  :" + listOfTools.length);
        boolean validData = false;
        for (int i = 0; i < listOfTools.length; i++)
        {

            LOG.info("Token Tool  :" + listOfTools[i] + " v= " + validData);
            validData = validateSkillData(listOfTools[i].trim(), employerID, domainType);

            if (validData)
            {
                validData = npCandidateEmploymentService.saveCandidateTool(listOfTools[i].trim(), npCandidate.getId().intValue(), employerID, domainType, true);
            }

            NpCandidate npCandiate = npCandidateEmploymentService.getNpCandidateById(npCandidate.getId().intValue());

            Set<NpCandidateTools> candidateTools = npCandiate.getNpCandidateTools();

            List<NpCandidateToolsForm> toolsModels = new ArrayList<NpCandidateToolsForm>();

            for (NpCandidateTools npCandidateTool : candidateTools)
            {
                if (npCandidateTool.getFuturePreferedTool() != 1)
                {
                    NpCandidateToolsForm npCandidateToolsForm = new NpCandidateToolsForm();
                    npCandidateToolsForm.setId(npCandidateTool.getId().getToolId());
                    npCandidateToolsForm.setExpertLevel(npCandidateTool.getNpExpertLevel().getExpertLevelName());
                    npCandidateToolsForm.setToolName(npCandidateTool.getNpTools().getToolName());
                    npCandidateToolsForm.setExperienceLevel(Integer.toString(npCandidateTool.getYearsOfExperience()));
                    toolsModels.add(npCandidateToolsForm);
                }
            }

            Set<NpCandidateEmployerImpl> npCandidateEmployers = npCandiate.getNpCandidateEmployers();

            for (NpCandidateEmployer npCandidateEmployer : npCandidateEmployers)
            {

                if (Integer.parseInt(employerID) == (npCandidateEmployer.getUid()))
                {

                    request.setAttribute("companyTools", npCandidateEmployer.getNpTools());
                }
            }

            request.setAttribute("tools", toolsModels);
            request.setAttribute("validData", validData);
        }
        return "toolBasket";
    }

    @RequestMapping("getSkills")
    public
    @ResponseBody
    List<String> getNpSkills(HttpServletRequest request)
    {

        LOG.info("Getting List of Skills");

        List<String> jsonOfSkills = npCandidateEmploymentService.getListOfSkills();

        return jsonOfSkills;
    }

    @RequestMapping("getTools")
    public
    @ResponseBody
    List<String> getNpTools(HttpServletRequest request)
    {

        LOG.info("Getting List of Tools");

        List<String> jsonOfTools = npCandidateEmploymentService.getListOfTools();

        return jsonOfTools;
    }

    @RequestMapping("getRoles")
    public
    @ResponseBody
    List<String> getNpRoles(HttpServletRequest request)
    {

        LOG.info("Getting List of Tools");

        List<String> jsonOfRoles = npCandidateEmploymentService.getListOfRoles();

        return jsonOfRoles;
    }

    @RequestMapping(value = "addMyaccountTool")
    @ResponseBody
    public Map<Integer, String> addMyaccountTool(String addtool, String employerID, HttpServletRequest request, Authentication authentication)
    {

        String[] listOfTools = addtool.split(",");
        Map<Integer, String> addedTools = new HashMap<Integer, String>();
        LOG.info("@@@ get Tool  :" + listOfTools.length);
        boolean validData = false;
        for (int i = 0; i < listOfTools.length; i++)
        {

            LOG.info("Token Tool  :" + listOfTools[i] + " v= " + validData);
            String[] toolList = listOfTools[i].split(" ");
            NpTool npTools = npCandidateEmploymentService.getToolByToolName(toolList[0]);
            int domainType = 0;
            validData = validateSkillData(listOfTools[i].trim(), employerID, domainType + "");

            if (validData)
            {
                NpUser npUser = npUserService.findByUserName(authentication.getName());
                validData = npCandidateEmploymentService.saveCandidateTool(listOfTools[i].trim(), npUser.getId(), employerID, domainType + "", true);
                if (validData)
                {
                    addedTools.put(npTools.getId(), npTools.getToolName());
                }
            }
        }
        return addedTools;
    }

    @RequestMapping(value = "addMyaccountRole")
    @ResponseBody
    public Map<Integer, String> addMyaccountRole(String addRole, String employerID, HttpServletRequest request, Authentication authentication)
    {

        String[] listOfRoles = addRole.split(",");
        Map<Integer, String> addedroles = new HashMap<Integer, String>();
        LOG.info("@@@ get Role  :" + listOfRoles.length);
        boolean validData = false;
        for (int i = 0; i < listOfRoles.length; i++)
        {

            String[] roleList = listOfRoles[i].split(" ");
            NpEmploymentRole npEmploymentRoles = npCandidateEmploymentService.getRoleByRoleName(roleList[0]);
            int domainType = 0;
            if (npEmploymentRoles.getNpDomain() != null)
            {
                domainType = npEmploymentRoles.getNpDomain().getId();
            }
            if (domainType > 0)
            {
                validData = validateRoleData(listOfRoles[i].trim(), employerID, domainType + "");

                LOG.info("Token Roles  :" + listOfRoles[i] + " v= " + validData);
                if (validData)
                {
                    NpUser npUser = npUserService.findByUserName(authentication.getName());
                    validData = npCandidateEmploymentService.saveCandidateRole(listOfRoles[i].trim(), npUser.getId(), employerID, domainType + "", true);
                    if (validData)
                    {
                        addedroles.put(npEmploymentRoles.getId(), npEmploymentRoles.getRoleName());
                    }
                }
            }
        }
        return addedroles;
    }

    @RequestMapping(value = "addMyaccountSkill")
    @ResponseBody
    public Map<Integer, String> addMyaccountSkill(String addskill, String employerID, HttpServletRequest request, Authentication authentication)
    {

        String[] listOfSkills = addskill.split(",");
        Map<Integer, String> addedskills = new HashMap<Integer, String>();
        LOG.info("@@@ get skils  :" + listOfSkills.length);
        boolean validData = false;
        for (int i = 0; i < listOfSkills.length; i++)
        {
            String[] skillList = listOfSkills[i].split(" ");
            int domainType = 0;
            NpSkill npSkills = npCandidateEmploymentService.getSkillBySkillName(skillList[0]);
            if (npSkills.getNpDomain() != null)
            {
                domainType = npSkills.getNpDomain().getId();
            }

            if (domainType > 0)
            {
                validData = validateSkillData(listOfSkills[i].trim(), employerID, domainType + "");
                LOG.info("Token skils  :" + listOfSkills[i] + " v= " + validData);

                if (validData)
                {
                    NpUser npUser = npUserService.findByUserName(authentication.getName());

                    validData = npCandidateEmploymentService.saveCandidateSkill(listOfSkills[i], npUser.getId(), employerID, domainType + "", true);
                    if (validData)
                    {
                        addedskills.put(npSkills.getId(), npSkills.getSkillName());
                    }
                }
            }
        }

        return addedskills;
    }
}
