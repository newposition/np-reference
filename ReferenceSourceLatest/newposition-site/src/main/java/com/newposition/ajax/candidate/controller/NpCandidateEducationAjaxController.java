package com.newposition.ajax.candidate.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateEducationForm;
import com.newposition.candidate.service.NpCandidateEducationRegisterService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
public class NpCandidateEducationAjaxController
{

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    public NpCandidateEducationRegisterService npCandidateEducationRegisterService;

    @RequestMapping(value = "saveEducation", method = RequestMethod.POST)
    @ResponseBody
    public String saveEducationHistory(NpCandidateEducationForm npCandidateEducationForm, HttpServletRequest request, Authentication authentication) throws Exception
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String json = "";
        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.saveEducationHistory(npCandidateEducationForm, npCandidate.getId());

            List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterService.getEducationHistories(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(npCandidateEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "saveQualification", method = RequestMethod.POST)
    @ResponseBody
    public String saveQualification(NpCandidateEducationForm npCandidateEducationForm, HttpServletRequest request, Authentication authentication) throws Exception
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String json = "";
        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.saveQualification(npCandidateEducationForm, npCandidate.getId());

            List<NpCandidateEducationForm> myEducationForms = npCandidateEducationRegisterService.getQualifications(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(myEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "getInstitutes", method = RequestMethod.GET)
    @ResponseBody
    public String getInstituteNames(String name) throws Exception
    {
        List<String> institutions = npCandidateEducationRegisterService.getInstituteNames();
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        json = mapper.writeValueAsString(institutions);
        return json;
    }

    @RequestMapping(value = "getLanguages", method = RequestMethod.GET)
    @ResponseBody
    public String getLanguages() throws Exception
    {
        List<String> languages = npCandidateEducationRegisterService.getLanguages();
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        json = mapper.writeValueAsString(languages);
        return json;
    }

    @RequestMapping(value = "saveLanguage", method = RequestMethod.GET)
    @ResponseBody
    public List<String> saveLanguages(String names, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> languageNames = new ArrayList<String>();
        List<String> languageFluencies = new ArrayList<String>();
        String[] splitLanguage = {};

        if (npCandidate != null && names != null)
        {
            String[] result = names.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String language = result[i];
                if (language.trim().length() > 0 && validateLanguagePattern(language))
                {
                    splitLanguage = language.split(" ");
                    languageNames.add(splitLanguage[0].trim());
                    languageFluencies.add(splitLanguage[1].trim());
                }
            }
            languageNames = npCandidateEducationRegisterService.saveLanguages(languageNames, languageFluencies, npCandidate.getId());
        }
        return languageNames;
    }

    public boolean validateLanguagePattern(String language)
    {

        String[] lang = language.split(" ");
        if (lang.length == 2)
        {
            if (lang[1].length() >= 1 && (lang[1].equalsIgnoreCase("Basic") || lang[1].equalsIgnoreCase("Conversation") || lang[1].equalsIgnoreCase("Written") || lang[1].equalsIgnoreCase("Fluent")))
            {
                return true;
            }
        }

        return false;
    }

    @RequestMapping(value = "saveInterests", method = RequestMethod.GET)
    @ResponseBody
    public List<String> saveInterests(String names, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> interestNames = new ArrayList<String>();
        if (npCandidate != null && names != null)
        {
            String[] result = names.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String interestName = result[i];
                if (interestName.trim().length() > 0)
                {
                    interestNames.add(result[i]);
                }
            }
            interestNames = npCandidateEducationRegisterService.saveInterests(interestNames, npCandidate.getId());
        }
        return interestNames;
    }

    @RequestMapping(value = "saveCertificate", method = RequestMethod.POST)
    @ResponseBody
    public String saveCertificate(NpCandidateEducationForm npCandidateEducationForm, HttpServletRequest request, Authentication authentication
    ) throws Exception
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        String json = "";

        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.saveCertifications(npCandidateEducationForm, npCandidate.getId());

            List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterService.getCandidateCertificates(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(npCandidateEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "removeEducation", method = RequestMethod.GET)
    @ResponseBody
    public String removeEducation(int id, HttpServletRequest request, Authentication authentication) throws JsonGenerationException, JsonMappingException, IOException
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String json = "";
        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.removeEducation(id, npCandidate.getId());
            List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterService.getEducationHistories(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(npCandidateEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "removeCertificate", method = RequestMethod.GET)
    @ResponseBody
    public String removeCertificate(int id, HttpServletRequest request, Authentication authentication) throws JsonGenerationException, JsonMappingException, IOException
    {
        String json = "";
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.removeCertificate(id, npCandidate.getId());
            List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterService.getCandidateCertificates(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(npCandidateEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "removeQualification", method = RequestMethod.GET)
    @ResponseBody
    public String removeQualification(int id, HttpServletRequest request, Authentication authentication) throws JsonGenerationException, JsonMappingException, IOException
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        String json = "";
        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.removeQualification(id, npCandidate.getId());
            ;
            List<NpCandidateEducationForm> npCandidateEducationForms = npCandidateEducationRegisterService.getQualifications(npCandidate.getId());

            ObjectMapper mapper = new ObjectMapper();

            json = mapper.writeValueAsString(npCandidateEducationForms);
        }
        return json;
    }

    @RequestMapping(value = "removeLanguage", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeLanguage(String languageName, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateEducationRegisterService.removeLanguage(languageName, npCandidate.getId());
    }

    @RequestMapping(value = "removeInterest", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeInterest(String interestName, HttpServletRequest request, Authentication authentication)
    {
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        if (npCandidate != null)
        {
            npCandidateEducationRegisterService.removeInterest(interestName, npCandidate.getId());
        }
    }
}
