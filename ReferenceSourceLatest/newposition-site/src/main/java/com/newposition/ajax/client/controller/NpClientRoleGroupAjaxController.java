package com.newposition.ajax.client.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.client.domain.NpClientUser;
import com.newposition.client.service.NpClientRoleManagementService;
import com.newposition.client.service.NpClientSupportAddEditUserService;
import com.newposition.common.domain.CoGroups;
import com.newposition.common.domain.CoRoleGroups;

@Controller
public class NpClientRoleGroupAjaxController
{

    @Autowired
    private NpClientSupportAddEditUserService npClientSupportAddEditUserService;

    @Autowired
    private NpClientRoleManagementService npClientRoleManagementService;

    @RequestMapping("getClientRolesAndGroups")
    public
    @ResponseBody
    List<String> getClientRoles(HttpServletRequest request, Authentication authentication)
    {

        List<String> jsonOfRoles = npClientSupportAddEditUserService.getNpClientRoles();

        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        List<CoGroups> coGroups = npClientRoleManagementService.getListOfClientGroupsById(clientUser.getOrganization().getId(), "");

        for (CoGroups coGroup : coGroups)
        {

            jsonOfRoles.add(coGroup.getGroupName());
        }

        return jsonOfRoles;
    }

    @RequestMapping("deleteGroupById")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteGroup(String id)
    {
        npClientRoleManagementService.deleteGroupById(Integer.parseInt(id));
    }

    @RequestMapping("getClientRolesInGroups")
    @ResponseBody
    public List<String> getClientRolesInGroups(HttpServletRequest request, Authentication authentication, String groupName)
    {

        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        CoGroups coGroup = npClientRoleManagementService.getListOfRolesInClientGroups(clientUser.getOrganization().getId(), groupName);

        List<String> jsonOfRoles = new ArrayList<>();
        if (coGroup != null)
        {
            Set<CoRoleGroups> set = coGroup.getCoRoleGroups();
            if (set.size() > 0)
            {
                for (CoRoleGroups coRoleGroups : set)
                {
                    jsonOfRoles.add(coRoleGroups.getNpRole().getRoleName());
                }
            }
        }
        return jsonOfRoles;
    }

    @RequestMapping("checkGroupName")
    @ResponseBody
    public String checkGroupName(String name, Authentication authentication)
    {

        NpClientUser clientUser = npClientSupportAddEditUserService.getCompanyOfCurrentUser(authentication.getName());

        return npClientRoleManagementService.checkClientGroupExistsOrNot(clientUser.getOrganization().getId(), name);
    }
}
