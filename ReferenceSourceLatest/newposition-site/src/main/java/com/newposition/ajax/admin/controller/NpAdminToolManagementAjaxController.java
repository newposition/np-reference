package com.newposition.ajax.admin.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.forms.AddNpToolsForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.forms.ToolAliases;
import com.newposition.admin.service.NpAdminToolManagementService;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.CoToolAlias;
import com.newposition.common.domain.NpToolImpl;

@Controller
public class NpAdminToolManagementAjaxController
{

    @Autowired
    private NpAdminToolManagementService toolManagementService;

    @RequestMapping(value = "/toolManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpTools(HttpServletRequest request)
    {
        final String searchCode = request.getParameter("searchCode");
        final String mergeTo = request.getParameter("mergeToId");

        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpToolImpl> npToolsSet = toolManagementService.getNpToolsforAliasSearch(searchCode, mergeTo);
            List<NpToolImpl> npTools = new ArrayList<NpToolImpl>(npToolsSet);
            request.setAttribute("npTools", npTools);
            request.setAttribute("addNpToolsForm", new AddNpToolsForm());

            request.setAttribute("npToolTypes", toolManagementService.getToolTypes());
            request.setAttribute("coSources", toolManagementService.getcoSources());

            request.setAttribute("toolAliases", new ToolAliases());
            request.setAttribute("mergeTo", mergeTo);
        }
        return "aliasSearchNpToolsResult";
    }

    @RequestMapping(value = "/toolManagement/deleteAlias", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeAliasRow(HttpServletRequest request)
    {

        final String aliasId = request.getParameter("id");
        if (StringUtils.isNotBlank(aliasId))
        {
            toolManagementService.deleteCoAliasTools(aliasId);
        }
    }

    @RequestMapping(value = "/toolManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoToolAlias> coCertificationAliasSet = toolManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoToolAlias coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getToolAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getToolAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", toolManagementService.getcoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
