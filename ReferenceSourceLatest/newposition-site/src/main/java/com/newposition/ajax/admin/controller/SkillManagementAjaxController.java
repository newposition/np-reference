package com.newposition.ajax.admin.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.domain.CoCourseAliasImpl;
import com.newposition.admin.forms.AddNpSKillsForm;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.forms.SkillAliases;
import com.newposition.admin.service.SkillManagementService;
import com.newposition.common.domain.CoSkillAlias;
import com.newposition.common.domain.NpSkillImpl;

@Controller
public class SkillManagementAjaxController
{
    protected static final Logger LOG = Logger
            .getLogger(SkillManagementAjaxController.class);

    @Autowired
    private SkillManagementService skillManagementService;

    @RequestMapping(value = "/skillManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpSKills(HttpServletRequest request)
    {
        final String searchCode = request.getParameter("searchCode");
        final String mergeTo = request.getParameter("mergeToId");

        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpSkillImpl> npSkillsSet = skillManagementService.getNpSKillsforAliasSearch(searchCode, mergeTo);
            List<NpSkillImpl> npSkills = new ArrayList<NpSkillImpl>(npSkillsSet);
            request.setAttribute("npSkills", npSkills);
            request.setAttribute("addNpSkillsForm", new AddNpSKillsForm());

            request.setAttribute("npDomainTypes", skillManagementService.getDomainTypes());
            request.setAttribute("npSkillTypes", skillManagementService.getSkillTypes());
            request.setAttribute("coSources", skillManagementService.getcoSources());

            request.setAttribute("skillAliases", new SkillAliases());
            request.setAttribute("mergeTo", mergeTo);
        }
        return "aliasSearchNpSkillsResult";
    }

    @RequestMapping(value = "/skillManagement/deleteAlias", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeAliasRow(HttpServletRequest request)
    {

        final String aliasId = request.getParameter("id");
        if (StringUtils.isNotBlank(aliasId))
        {
            skillManagementService.deleteCoAliasSkills(aliasId);
        }
    }

    @RequestMapping(value = "/skillManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoSkillAlias> coCertificationAliasSet = skillManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoSkillAlias coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getSkillAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getSkillAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", skillManagementService.getcoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
