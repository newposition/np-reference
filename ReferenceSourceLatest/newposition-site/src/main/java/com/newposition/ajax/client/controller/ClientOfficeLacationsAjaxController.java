package com.newposition.ajax.client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientOfficeLocation;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.service.NpClientOfficeLocationService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.CoCities;
import com.newposition.common.domain.CoCountry;
import com.newposition.common.domain.CoCounty;
import com.newposition.common.domain.CoLocationState;
import com.newposition.common.domain.CoLocations;
import com.newposition.common.domain.CoZip;
import com.newposition.common.domain.CommonBean;
import com.newposition.common.domain.NpUser;

@RestController
public class ClientOfficeLacationsAjaxController
{

    @Autowired
    NpClientService npClientService;

    @Autowired
    NpClientOfficeLocationService npClientOfficeLocationService;

    @RequestMapping(value = "/getAllOfficeLocations", method = RequestMethod.POST)
    public
    @ResponseBody
    List<NpClientOfficeLocation> getAllClientOfficeLocations(HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpClientOfficeLocation> officeLocations = getLightWeightList(npClientOfficeLocationService.getAllClientOfficeLocations(npClient));
        return officeLocations;
    }

    @RequestMapping(value = "/getObjectById2", method = RequestMethod.GET)
    public
    @ResponseBody
    NpClientOfficeLocation getNpClientOfficeLocationbyId(@RequestParam("id") int id, Model model)
    {
        NpClientOfficeLocation npClientOfficeLocation = getLightWieghtNpClientOfficeLocation(npClientOfficeLocationService.getnpClientOfficeLocationServiceById(id));
        return npClientOfficeLocation;
    }

    @RequestMapping(value = "/getCommonBeanList", method = RequestMethod.GET)
    public
    @ResponseBody
    List<CommonBean> getCommonBeanList(@RequestParam("str1") String str1, @RequestParam("str2") String str2)
    {
        List<CommonBean> commonBeanList = npClientOfficeLocationService.getCommonBeanList(str1, str2);
        return commonBeanList;
    }

    @RequestMapping(value = "/getZipList", method = RequestMethod.GET)
    public
    @ResponseBody
    List<CoZip> getZipList(@RequestParam("str1") String str1, @RequestParam("str2") String str2)
    {
        List<CoZip> zipList = getLightWeightCoZipList(npClientOfficeLocationService.getZipList(str1, str2));
        return zipList;
    }

    @RequestMapping(value = "/deleteOfficeLocation", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientOfficeLocation> deleteOfficeLocation(@RequestParam("id") int id, HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpClientOfficeLocation> officeLocations = getLightWeightList(npClientOfficeLocationService.deleteOfficeLocation(id, npClient));
        return officeLocations;
    }

    @RequestMapping(value = "/sortData2", method = RequestMethod.POST)
    public
    @ResponseBody
    List<NpClientOfficeLocation> sortTheList(HttpSession session, @RequestParam("str") String str, @RequestBody ArrayList<NpClientOfficeLocation> officeLocations)
    {
        List<NpClientOfficeLocation> officeLocations1 = getLightWeightList(npClientOfficeLocationService.getSortedList(officeLocations, str));
        return officeLocations1;
    }

    @RequestMapping(value = "/getStateListByCountryName", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getStateListByCountryName(@RequestParam("str") String str)
    {
        List<String> statesNames = npClientOfficeLocationService.getStateListByCountryName(str);
        return statesNames;
    }

    @RequestMapping(value = "/getLocationListByCountryName", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientOfficeLocation> getLocationListByCountryName(@RequestParam("str") String str)
    {
        List<NpClientOfficeLocation> list = getLightWeightList(npClientOfficeLocationService.getLocationListByCountryName(str));
        return list;
    }

    @RequestMapping(value = "/getLocationListByStateAndCountry", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientOfficeLocation> getLocationListByStateAndCountry(@RequestParam("str1") String str1, @RequestParam("str2") String str2)
    {
        List<NpClientOfficeLocation> list = getLightWeightList(npClientOfficeLocationService.getLocationListByStateAndCountry(str1, str2));
        return list;
    }

    @RequestMapping(value = "/getCitesByStateAndCountry", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getCitesByStateAndCountry(@RequestParam("str1") String str1, @RequestParam("str2") String str2)
    {
        List<String> cities = npClientOfficeLocationService.getCitesByStateAndCountry(str1, str2);
        return cities;
    }

    @RequestMapping(value = "/getLocationsByStateAndCountryAndCity", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientOfficeLocation> getLocationsByStateAndCountryAndCity(@RequestParam("str1") String str1, @RequestParam("str2") String str2, @RequestParam("str3") String str3, HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpClientOfficeLocation> locations = getLightWeightList(npClientOfficeLocationService.getLocationsByStateAndCountryAndCity(str1, str2, str3, npClient));
        return locations;
    }

    public CoZip getLightWeightZipBean(CoZip coZip)
    {
        CoLocationState coLocationState = coZip.getCoLocationState();
        CoZip coZip1 = new CoZip();
        CoLocationState coLocationState1 = new CoLocationState();
        coLocationState1.setStateID(coLocationState.getStateID());
        coLocationState1.setStateDescription(coLocationState.getStateDescription());
        coZip1.setzIP(coZip.getzIP());
        coZip1.setCoLocationState(coLocationState1);
        return coZip1;
    }

    public List<CoZip> getLightWeightCoZipList(List<CoZip> coZipList)
    {
        List<CoZip> coZipList1 = new ArrayList<CoZip>();
        for (int i = 0; i < coZipList.size(); i++)
        {
            CoZip coZip = coZipList.get(i);
            CoZip coZip1 = getLightWeightZipBean(coZip);
            coZipList1.add(coZip1);
        }
        return coZipList1;
    }

    public NpClientOfficeLocation getLightWieghtNpClientOfficeLocation(NpClientOfficeLocation npClientOfficeLocation1)
    {
        NpClientOfficeLocation npClientOfficeLocation = new NpClientOfficeLocation();
        CoLocations coLocations = new CoLocations();
        CoCities coCities = new CoCities();
        CoCountry coCountry = new CoCountry();
        CoCounty coCounty = new CoCounty();
        CoZip coZip = new CoZip();
        CoLocationState coLocationState = new CoLocationState();
        coCities.setCityID(npClientOfficeLocation1.getCoLocations().getCoCities().getCityID());
        coCities.setCityDescription(npClientOfficeLocation1.getCoLocations().getCoCities().getCityDescription());
        coCountry.setCountryID(npClientOfficeLocation1.getCoLocations().getCoCountry().getCountryID());
        coCountry.setCountryDescription(npClientOfficeLocation1.getCoLocations().getCoCountry().getCountryDescription());
        coCounty.setCountyID(npClientOfficeLocation1.getCoLocations().getCoCounty().getCountyID());
        coCounty.setCountyDescription(npClientOfficeLocation1.getCoLocations().getCoCounty().getCountyDescription());
        coLocationState.setStateID(npClientOfficeLocation1.getCoLocations().getCoZip().getCoLocationState().getStateID());
        coLocationState.setStateDescription(npClientOfficeLocation1.getCoLocations().getCoZip().getCoLocationState().getStateDescription());
        coZip.setzIP(npClientOfficeLocation1.getCoLocations().getCoZip().getzIP());
        coZip.setCoLocationState(coLocationState);
        coLocations.setLocationID(npClientOfficeLocation1.getCoLocations().getLocationID());
        coLocations.setAddress1(npClientOfficeLocation1.getCoLocations().getAddress1());
        coLocations.setAddress2(npClientOfficeLocation1.getCoLocations().getAddress2());
        coLocations.setAddress3(npClientOfficeLocation1.getCoLocations().getAddress3());
        coLocations.setLocationXML(npClientOfficeLocation1.getCoLocations().getLocationXML());
        coLocations.setCoCities(coCities);
        coLocations.setCoCountry(coCountry);
        coLocations.setCoCounty(coCounty);
        coLocations.setCoZip(coZip);
        npClientOfficeLocation.setClientOfficeLocID(npClientOfficeLocation1.getClientOfficeLocID());
        npClientOfficeLocation.setCoLocations(coLocations);
        return npClientOfficeLocation;
    }

    public List<NpClientOfficeLocation> getLightWeightList(List<NpClientOfficeLocation> list1)
    {
        List<NpClientOfficeLocation> list = new ArrayList<NpClientOfficeLocation>();
        for (int i = 0; i < list1.size(); i++)
        {
            NpClientOfficeLocation npClientOfficeLocation = list1.get(i);
            NpClientOfficeLocation npClientOfficeLocation1 = getLightWieghtNpClientOfficeLocation(npClientOfficeLocation);
            list.add(npClientOfficeLocation1);
        }
        return list;
    }
}
