package com.newposition.ajax.client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.newposition.client.domain.NpClientCostCentre;
import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.domain.NpClientProject;
import com.newposition.client.service.NpClientCostCentreService;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.NpUser;

@RestController
public class NpClientCostCentreAjaxController
{

    protected static final Logger LOG = Logger.getLogger(NpClientCostCentreAjaxController.class);

    @Autowired
    private NpClientService npClientService;

    @Autowired
    private NpClientCostCentreService npClientCostCentreService;

    @RequestMapping(value = "/deleteCostCentre", method = RequestMethod.GET)
    public List<NpClientCostCentreImpl> deleteRow(@RequestParam("id") String id, Model model, HttpSession session, Authentication authentication)
    {
        LOG.info("in ajaxs id=" + id);

        npClientCostCentreService.deleteCostCentre(id);
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreService.findAllCostCentreOfCurrentClient(authentication.getName());
        List<NpClientCostCentreImpl> costCentres_1 = npClientCostCentreService.getLightList(costCentres);
        LOG.info("in ajaxs costCentre=" + costCentres.size());

        return costCentres_1;
    }

    @RequestMapping(value = "/checkCostCentreCode", method = RequestMethod.GET)
    public String checkCostCentreObject(@RequestParam("id") String str, Model model, HttpSession session)
    {
        LOG.info("in ajaxs get Object id=" + str);

        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        String result = npClientCostCentreService.checkCostCentreCode(str, npClient);
        return result;
    }

    @RequestMapping(value = "/getAllCostCentre", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientCostCentreImpl> getAllCostCentre(HttpSession session)
    {
        LOG.info("in all list ajaxs");
        NpUser user = (NpUser) session.getAttribute("user");
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreService.findAllCostCentreOfCurrentClient(user.getPrimaryEmail());

        List<NpClientCostCentreImpl> costCentres_1 = npClientCostCentreService.getLightList(costCentres);
        LOG.info("costCentre=" + costCentres.size());

        return costCentres_1;
    }

    @RequestMapping(value = "/sortCostCentreData")
    public
    @ResponseBody
    List<NpClientCostCentreImpl> getProjectsBySorting(HttpSession session, @RequestParam("str") String str, @RequestBody ArrayList<NpClientCostCentreImpl> costCentreList)
    {
        LOG.info("in all sorting ajaxs STR=" + str);
        LOG.info("projectlist=" + costCentreList.size());
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreService.getSortedListOfCostCentre(costCentreList, str);
        LOG.info("cost centre : " + costCentres.size());
        return costCentres;
    }

    @RequestMapping(value = "/getCostCentreById", method = RequestMethod.GET)
    public NpClientCostCentre getCostCentreObject(@RequestParam("id") String str, Model model, HttpSession session)
    {
        LOG.info("in ajaxs get Object id=" + str);
        NpClientCostCentre costCentre = npClientCostCentreService.findCostCentreById(str);
        NpClientCostCentre _costCentre = npClientCostCentreService.getCostCentreLightWeightData((NpClientCostCentreImpl) costCentre);
        LOG.info("after ajaxs==" + costCentre);
        return _costCentre;
    }

    @RequestMapping(value = "/getFilterStringList", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getListOfStr(@RequestParam("str") String str, HttpSession session, Authentication authentication)
    {
        LOG.info("in str ajax controller Filter String List @@@@@@@");
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreService.findAllCostCentreOfCurrentClient(authentication.getName());
        LOG.info("in str ajax controller projects=" + costCentres.size());

        List<String> list = npClientCostCentreService.getStringList(str, costCentres);
        LOG.info("in str ajax controller list=" + list.size());
        return list;
    }

    @RequestMapping(value = "/filterCCData", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpClientCostCentreImpl> getProjectsByFilters(HttpSession session, @RequestParam("str1") String str1, @RequestParam("str2") String str2, Authentication authentication)
    {
        LOG.info("in all filter ajaxs STR1=" + str1 + "   str2=" + str2);
        List<NpClientCostCentreImpl> costCentres = npClientCostCentreService.findAllCostCentreOfCurrentClient(authentication.getName());
        List<NpClientCostCentreImpl> costCentre1 = npClientCostCentreService.getLightList(npClientCostCentreService.getClientCostCentreByFilter(str1, str2, costCentres));
        LOG.info("CostCentre1 = " + costCentre1.size());
        return costCentre1;
    }
}
