package com.newposition.ajax.common.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.newposition.client.domain.NpClientCostCentreImpl;
import com.newposition.client.domain.NpClientImpl;
import com.newposition.client.service.NpClientService;
import com.newposition.common.domain.CommonBean;
import com.newposition.common.domain.NpProgram;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpProgrammeService;

@RestController
public class NpProgrammeAjaxController
{

    @Autowired
    NpProgrammeService npProgrammeService;

    @Autowired
    NpClientService npClientService;

    @RequestMapping(value = "/getAllProgramme", method = RequestMethod.POST)
    public
    @ResponseBody
    List<NpProgram> getAllProjects(HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        System.out.println("in all list ajaxs");
        List<NpProgram> programmes = getLightWeightPramsList(npProgrammeService.getAllClientProgramme(npClient));
        System.out.println("programmes=" + programmes.size());
        return programmes;
    }

    @RequestMapping(value = "/getObjectById1", method = RequestMethod.GET)
    public
    @ResponseBody
    NpProgram getProgrammeObject(@RequestParam("id") long id, Model model)
    {
        System.out.println("in ajaxs get Object id=" + id);
        NpProgram npProgramme = getLightWeightProgram(npProgrammeService.getNpProgrammeById(id));
        System.out.println("after ajaxs==" + npProgramme.getVideoUrl());
        return npProgramme;
    }

    @RequestMapping(value = "/deleteProgramee", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpProgram> deleteRow(@RequestParam("id") int id, Model model, HttpSession session)
    {
        System.out.println("in ajaxs id=" + id);
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        System.out.println("in ajaxs npClient=" + npClient);
        String result = npProgrammeService.deleteProject(id);
        List<NpProgram> programmes = getLightWeightPramsList(npProgrammeService.getAllClientProgramme(npClient));
        System.out.println("in ajaxs projects=" + programmes.size());
        return programmes;
    }

    @RequestMapping(value = "/getStringList1", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getListOfStr(@RequestParam("str") String str, HttpSession session)
    {
        System.out.println("in str ajax controller");
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpProgram> programmes = npProgrammeService.getAllClientProgramme(npClient);
        System.out.println("in str ajax controller programmes=" + programmes.size());
        List<String> list = npProgrammeService.getStringList(str, programmes);
        System.out.println("in str ajax controller list=" + list.size());
        return list;
    }

    @RequestMapping(value = "/filterData1", method = RequestMethod.GET)
    public
    @ResponseBody
    List<NpProgram> getProjectsByFilters(@RequestParam("str1") String str1, @RequestParam("str2") String str2, HttpSession session)
    {
        System.out.println("in all filter ajaxs STR1=" + str1 + "   str2=" + str2);

        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        List<NpProgram> programmes = npProgrammeService.getAllClientProgramme(npClient);

        List<NpProgram> programmes1 = getLightWeightPramsList(npProgrammeService.getClientProjectsByFilter(str1, str2, programmes));
        System.out.println("projects1=" + programmes1.size());
        return programmes1;
    }

    @RequestMapping(value = "/sortData1")
    public
    @ResponseBody
    List<NpProgram> getProjectsBySorting(HttpSession session, @RequestParam("str") String str, @RequestBody ArrayList<NpProgram> programelist)
    {
        System.out.println("in all sorting ajaxs STR=" + str);
        System.out.println("programelist=" + programelist.size());
        List<NpProgram> programmes = getLightWeightPramsList(npProgrammeService.getProjectsBySort(programelist, str));
        System.out.println("project");
        return programmes;
    }

    @RequestMapping(value = "/saveData1")
    public
    @ResponseBody
    NpProgram saveNpProgram(HttpSession session, @RequestBody NpProgram npProgram, @RequestParam("npClientCostCentreImpl.costCentreID") int costCentreID)
    {

        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        NpClientCostCentreImpl costCenter = new NpClientCostCentreImpl();
        costCenter.setCostCentreId(new Long(costCentreID));
        npProgram.setNpClientCostCentreImpl(costCenter);
        npProgram.setNpClient(npClient);
        NpProgram npProgram1 = getLightWeightProgram(npProgrammeService.saveOrUpdate(npProgram));

        return npProgram1;
    }

    @RequestMapping(value = "/getCostCenter", method = RequestMethod.GET)
    public
    @ResponseBody
    List<CommonBean> getCostCenter(@RequestParam("str") String str)
    {
        System.out.println("in controller str=" + str);
        List<CommonBean> commonlist = npProgrammeService.getCostCentercommonlist(str);
        return commonlist;
    }

    @RequestMapping(value = "/checkCode1", method = RequestMethod.GET)
    public
    @ResponseBody
    String checkCode(@RequestParam("str") String str, HttpSession session)
    {
        NpUser user = (NpUser) session.getAttribute("user");
        NpClientImpl npClient = npClientService.getNpClientImpl(user.getId());
        String result = npProgrammeService.checkProgramCode(str, npClient);

        return result;
    }

    public List<NpProgram> getLightWeightPramsList(List<NpProgram> programs)
    {
        List<NpProgram> programs1 = new ArrayList<>();
        for (int i = 0; i < programs.size(); i++)
        {
            NpProgram npProgram = getLightWeightProgram(programs.get(i));
            programs1.add(npProgram);
        }
        return programs1;
    }

    public NpProgram getLightWeightProgram(NpProgram npProgram)
    {
        NpProgram npProgram1 = new NpProgram();
        npProgram1.setClientProgramID(npProgram.getClientProgramID());
        npProgram1.setProgramCode(npProgram.getProgramCode());
        npProgram1.setProgramDescription(npProgram.getProgramDescription());
        npProgram1.setProgramOverview(npProgram.getProgramOverview());
        npProgram1.setProgramOwner(npProgram.getProgramOwner());
        npProgram1.setProgramSponsor(npProgram.getProgramSponsor());
        npProgram1.setVideoUrl(npProgram.getVideoUrl());
        System.out.println(npProgram1.getVideoUrl() + " #######");
        NpClientCostCentreImpl npClientCostCentreImpl = new NpClientCostCentreImpl();
        npClientCostCentreImpl.setCostCentreId(npProgram.getNpClientCostCentreImpl().getCostCentreId());
        npClientCostCentreImpl.setCostCentreCode(npProgram.getNpClientCostCentreImpl().getCostCentreCode());
        npClientCostCentreImpl.setCostCentreName(npProgram.getNpClientCostCentreImpl().getCostCentreName());
        npClientCostCentreImpl.setCostCentreLocation(npProgram.getNpClientCostCentreImpl().getCostCentreLocation());
        npClientCostCentreImpl.setCostCentreOwner(npProgram.getNpClientCostCentreImpl().getCostCentreOwner());
        npProgram1.setNpClientCostCentreImpl(npClientCostCentreImpl);
        return npProgram1;
    }
}
