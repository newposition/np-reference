/**
 *
 */
package com.newposition.ajax.candidate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.candidate.domain.NpCandidate;
import com.newposition.candidate.forms.NpCandidateReferenceForm;
import com.newposition.candidate.service.NpCandidateEmploymentService;
import com.newposition.candidate.service.NpCandidateFutureService;
import com.newposition.candidate.service.NpCandidateService;
import com.newposition.candidate.service.NpCandidateSponsorService;
import com.newposition.common.domain.NpUser;
import com.newposition.common.service.NpUserService;

@Controller
public class NpCandidateFutureAjaxController
{

    protected static final Logger LOG = Logger.getLogger(NpCandidateFutureAjaxController.class);

    @Autowired
    private NpCandidateFutureService npCandidateFutureService;

    @Autowired
    private NpCandidateSponsorService npCandidateSponsorService;

    @Autowired
    private NpUserService npUserService;

    @Autowired
    private NpCandidateService npCandidateService;

    @Autowired
    private NpCandidateEmploymentService npCandidateEmploymentService;

    @SuppressWarnings("unchecked")
    @RequestMapping("inviteAll")
    public String inviteAllSponsor(HttpServletRequest request, Authentication authentication)
    {

        // invite all functionality

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<NpCandidateReferenceForm> referenceList = (List<NpCandidateReferenceForm>) request.getSession().getAttribute("referenceList");

        LOG.info("Sending email to all the sponsor tha recently added");

        if (referenceList.size() > 0)
        {
            npCandidateSponsorService.sendEmailToAll(referenceList, npCandidate);
            request.setAttribute("sucesMsg",
                    "All sponsors have been sent an email");
        }
        else
        {
            request.setAttribute("errMsg", "You have no sponsor currently");
        }

        return "redirect:/myFuture";
    }

    @RequestMapping("addSkill")
    public
    @ResponseBody
    List<String> addSkill(String skills, HttpServletRequest request, Authentication authentication)
    {

        LOG.info("Skill Name added:  " + skills);

//          getting npCandidateObject
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> skillNames = new ArrayList<String>();
        if (npCandidate != null && skills != null)
        {
            String[] result = skills.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String skillName = result[i];
                if (skillName.trim().length() > 0)
                {
                    skillNames.add(skillName);
                }
            }
            skillNames = npCandidateFutureService.saveSkills(skillNames, npCandidate);
        }
//        if (!StringUtils.isEmpty(skillName)) {
//            LOG.info("Skill added");
//            return skillName;
//        }
//        LOG.info("already exist");
        return skillNames;
    }

    @RequestMapping("removeSkill")
    public
    @ResponseBody
    String removeSkill(HttpServletRequest request, Authentication authentication)
    {

        String skillName = request.getParameter("skillName");
        LOG.info("Skill to be added:  " + skillName);
        /*
         * 
         * getting npCandidateObject
         * 
         * request.getSession().getAttribute("npCandidate");
         */
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidateFutureService.removeSkill(skillName, npCandidate))
        {
            return skillName;
        }
        LOG.info("already exist");
        return "";
    }

    @RequestMapping("addRole")
    public
    @ResponseBody
    List<String> addRole(String roles, HttpServletRequest request, Authentication authentication)
    {

//        LOG.info("Role to be added:  " + roleName);
        /*
         * 
         * getting npCandidateObject
         * 
         * request.getSession().getAttribute("npCandidate");
         */
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> roleNames = new ArrayList<String>();
        if (npCandidate != null && roles != null)
        {
            String[] result = roles.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String roleName = result[i];
                if (roleName.trim().length() > 0)
                {
                    roleNames.add(roleName);
                }
            }
            roleNames = npCandidateFutureService.saveRoles(roleNames, npCandidate);
        }
        return roleNames;
    }

    @RequestMapping("removeRole")
    public
    @ResponseBody
    String removeRole(HttpServletRequest request, Authentication authentication)
    {

        String roleName = request.getParameter("roleName");
        LOG.info("Role to be deleted:  " + roleName);
        /*
         * 
         * getting npCandidateObject
         * 
         * request.getSession().getAttribute("npCandidate");
         */
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidateFutureService.removeRole(roleName, npCandidate))
        {
            return roleName;
        }
        return "";
    }

    @RequestMapping("addTool")
    public
    @ResponseBody
    List<String> addTool(String tools, HttpServletRequest request, Authentication authentication)
    {

        /*
         * 
         * getting npCandidateObject
         * 
         * request.getSession().getAttribute("npCandidate");
         */
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> toolNames = new ArrayList<String>();
        if (npCandidate != null && tools != null)
        {
            String[] result = tools.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String toolName = result[i];
                if (toolName.trim().length() > 0)
                {
                    toolNames.add(toolName);
                }
            }
            toolNames = npCandidateFutureService.addTools(toolNames, npCandidate);
        }
        return toolNames;
    }

    @RequestMapping("removeTool")
    public
    @ResponseBody
    String removeTool(HttpServletRequest request, Authentication authentication)
    {

        String toolName = request.getParameter("toolName");
        LOG.info("Tool to be deleted:  " + toolName);
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidateFutureService.removeTool(toolName, npCandidate))
        {
            LOG.info("Role removed");
            return toolName;
        }
        return "";
    }

    @RequestMapping("addCountry")
    public
    @ResponseBody
    List<String> addCountry(String countries, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        List<String> countryNames = new ArrayList<String>();
        if (npCandidate != null && countries != null)
        {
            String[] result = countries.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String countryName = result[i];
                if (countryName.trim().length() > 0)
                {
                    if (countryName.contains("-"))
                    {
                        String[] names = countryName.split("-");
                        countryName = names[0];
                    }
                    countryNames.add(countryName);
                }
            }
            countryNames = npCandidateFutureService.addCountries(countryNames, npCandidate);
        }
        return countryNames;
    }

    @RequestMapping("removeCountry")
    public
    @ResponseBody
    String removeCountry(HttpServletRequest request, Authentication authentication)
    {

        String countryName = request.getParameter("countryName");
        LOG.info("Country Name to be deleted :  " + countryName);

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        if (npCandidateFutureService.removeCountry(countryName, npCandidate))
        {
            LOG.info("Country removed");
            return countryName;
        }
        return "";
    }

    @RequestMapping("getCountries")
    public
    @ResponseBody
    List<String> getCountries(HttpServletRequest request)
    {

        LOG.info("Getting List of Countries");

        List<String> countries = npCandidateFutureService.getListOfCountries();

        return countries;
    }

    @RequestMapping("getListOfSkills")
    public
    @ResponseBody
    List<String> getSkills(HttpServletRequest request)
    {

        LOG.info("Getting List of Skills");

        List<String> jsonOfSkills = npCandidateEmploymentService.getListOfSkills();

        return jsonOfSkills;
    }

    @RequestMapping("getListOfTools")
    public
    @ResponseBody
    List<String> getTools(HttpServletRequest request)
    {

        LOG.info("Getting List of Tools");

        List<String> jsonOfTools = npCandidateEmploymentService.getListOfTools();

        return jsonOfTools;
    }

    @RequestMapping("getListOfRoles")
    public
    @ResponseBody
    List<String> getRoles(HttpServletRequest request)
    {

        LOG.info("Getting List of Tools");

        List<String> jsonOfRoles = npCandidateEmploymentService.getListOfRoles();

        return jsonOfRoles;
    }

    @RequestMapping("addEmployer")
    public
    @ResponseBody
    List<String> saveFutureEmployer(String futureCompanies, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> companyNames = new ArrayList<String>();
        if (npCandidate != null && futureCompanies != null)
        {
            String[] result = futureCompanies.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String company = result[i];
                if (company.trim().length() > 0)
                {
                    companyNames.add(company);
                }
            }
            companyNames = npCandidateFutureService.saveFutureEmployer(companyNames, npCandidate);
        }
        return companyNames;
    }

    @RequestMapping("addExculudeEmployer")
    public
    @ResponseBody
    List<String> saveExcludeEmployer(String excludeCompanies, HttpServletRequest request, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        List<String> companyNames = new ArrayList<String>();
        if (npCandidate != null && excludeCompanies != null)
        {
            String[] result = excludeCompanies.split(",");
            for (int i = 0; i < result.length; i++)
            {
                String company = result[i];
                if (company.trim().length() > 0)
                {
                    companyNames.add(company);
                }
            }
            companyNames = npCandidateFutureService.saveExcludeEmployer(companyNames, npCandidate);
        }
        return companyNames;
    }

    @RequestMapping("removeFutureEmployer")
    public
    @ResponseBody
    String removeFutureEmployer(HttpServletRequest request, Authentication authentication)
    {

        String company = request.getParameter("companyName");
        LOG.info("@@@@Removing Comapny name " + company);
        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());

        npCandidateFutureService.removeFutureEmployer(company, npCandidate);

        return "ok";
    }

    @RequestMapping("changeFuturePreference")
    @ResponseStatus(value = HttpStatus.OK)
    public void changeFuturePreference(String type, String name, int futurePreference, Authentication authentication)
    {

        NpUser npUser = npUserService.findByUserName(authentication.getName());

        NpCandidate npCandidate = npCandidateService.findByID(npUser.getId());
        if (type.equals("skill"))
        {
            npCandidateFutureService.changeFuturePreferenceSkill(name, futurePreference, npCandidate);
        }
        else if (type.equals("role"))
        {
            npCandidateFutureService.changeFuturePreferenceRole(name, futurePreference, npCandidate);
        }
        else if (type.equals("tool"))
        {
            npCandidateFutureService.changeFuturePreferenceTools(name, futurePreference, npCandidate);
        }
    }

    @RequestMapping("getCountriesForAddClient")
    public
    @ResponseBody
    List<String> getCountriesForClient(HttpServletRequest request)
    {

        LOG.info("Getting List of Countries");

        List<String> countries = npCandidateFutureService.getListOfCountriesOnly();

        return countries;
    }
}
