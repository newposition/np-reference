package com.newposition.ajax.admin.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.newposition.admin.domain.CoInstituteAliasImpl;
import com.newposition.admin.forms.CertificationAliases;
import com.newposition.admin.service.NpAdminInstitutionManagementService;
import com.newposition.candidate.domain.NpInstitutions;

@Controller
public class InstitutionManagementAjaxController
{

    protected static final Logger LOG = Logger
            .getLogger(InstitutionManagementAjaxController.class);

    @Autowired
    private NpAdminInstitutionManagementService institutionManagementService;

    @RequestMapping(value = "/institutionManagement/aliasSearch", method = RequestMethod.POST)
    public String searchNpInstitute(HttpServletRequest request, String mergeId)
    {
        final String searchCode = request.getParameter("searchCode");
        if (StringUtils.isNotBlank(searchCode))
        {
            Set<NpInstitutions> npInstitutions = institutionManagementService.getNpInstituteforAliasSearch(searchCode, mergeId);
            List<NpInstitutions> listNpInstitutes = new ArrayList<NpInstitutions>(npInstitutions);
            request.setAttribute("npInstitutions", npInstitutions);
            request.setAttribute("npInstituteNames", institutionManagementService.getInstituteNames(listNpInstitutes));
            request.setAttribute("npInstituteTypes", institutionManagementService.getInstituteTypes(listNpInstitutes));
            request.setAttribute("coSources", institutionManagementService.getCoSources());
            request.setAttribute("mergeId", mergeId);
        }
        return "aliasSearchNpInstituteResult";
    }

    /* @RequestMapping(value = "saveMergedNpInstituteCourses", method = RequestMethod.POST)
     @ResponseStatus(value=HttpStatus.OK)
     public void mergeNpInstituteCourses(HttpServletRequest request,String targetId,String sourceId) {
           NpInstituteCourse npInstituteCourseTarget;
           NpInstituteCourse npInstituteCourseSource ;
         if(StringUtils.isNotBlank(targetId) && StringUtils.isNotBlank(sourceId))
         {
               npInstituteCourseTarget = qualificationManagementService.getNpInstituteCourseById(Integer.parseInt(targetId));


               npInstituteCourseSource = qualificationManagementService.getNpInstituteCourseById(Integer.parseInt(sourceId));

               npInstituteCourseTarget.getNpCourse().setCourseName(npInstituteCourseSource.getNpCertifications().getCourseName());

               npInstituteCourseTarget.getNpCertifications().setCourseType(npInstituteCourseSource.getNpCertifications().getCourseType());

               npInstituteCourseTarget.setNpInstitutions(npInstituteCourseSource.getNpInstitutions());
               Set<CoCertificationAliasImpl> coCertificationAlias=npInstituteCourseTarget.getNpCertifications().getCoCertificationAlias();
               coCertificationAlias.addAll(npInstituteCourseSource.getNpCertifications().getCoCertificationAlias());
               npInstituteCourseTarget.getNpCertifications().setCoCertificationAlias(coCertificationAlias);
               npInstituteCourseTarget.getNpCertifications().getCoCertificationAlias().addAll(npInstituteCourseSource.getNpCertifications().getCoCertificationAlias());
               npInstituteCourseTarget.getNpCertifications().setValid(npInstituteCourseSource.getNpCertifications().getValid());

               AddNpCertificationForm addNpCertificationForm=new AddNpCertificationForm();
               addNpCertificationForm.setNpInstituteCourseId(npInstituteCourseTarget.getId());
               addNpCertificationForm.setCertificationName(npInstituteCourseTarget.getNpCertifications().getCourseName());
               addNpCertificationForm.setCertificationType( npInstituteCourseTarget.getNpCertifications().getCourseType());
               addNpCertificationForm.setCertificationAliases(certificationAliases);
               qualificationManagementService.saveMergedNpInstituteCourse(npInstituteCourseTarget);
         }



     }
*/
    @RequestMapping(value = "/institutionManagement/removeCoInstitute", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeCoInstituteAlias(HttpServletRequest request)
    {
        final String aliasId = request.getParameter("id");
        if (StringUtils.isNotBlank(aliasId))
        {
            institutionManagementService.removeCoInstituteAlias(Integer.parseInt(aliasId));
        }
    }

    @RequestMapping(value = "/institutionManagement/removeNpInstitute", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeNpInstitute(String npInstituteId)
    {
        if (StringUtils.isNotBlank(npInstituteId))
        {
            LOG.info("Removing the npInstituteCourse with " + npInstituteId);
            institutionManagementService.removeNpInstituteById(Integer.parseInt(npInstituteId));
        }
    }

    @RequestMapping(value = "/institutionManagement/editAlias", method = RequestMethod.POST)
    public String getAliases(String id, HttpServletRequest request)
    {
        if (StringUtils.isNotBlank(id))
        {
            Set<CoInstituteAliasImpl> coCertificationAliasSet = institutionManagementService.getAliases(id);
            Set<CertificationAliases> certificationAliasesSet = new HashSet<CertificationAliases>();
            for (CoInstituteAliasImpl coCertificationAliasImpl : coCertificationAliasSet)
            {
                CertificationAliases certificationAliases = new CertificationAliases();
                certificationAliases.setAliasId(coCertificationAliasImpl.getInstituteAliasID().toString());
                certificationAliases.setAliasName(coCertificationAliasImpl.getInstituteAliasName());
                certificationAliases.setAliasSource(coCertificationAliasImpl.getCoSource().getSourceID().toString());
                certificationAliasesSet.add(certificationAliases);
            }
            request.setAttribute("coSources", institutionManagementService.getCoSources());
            request.setAttribute("certificationAliases", certificationAliasesSet);
        }
        return "editAliases";
    }
}
