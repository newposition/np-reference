package com.newposition.common.presentation.tag;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.context.ApplicationContext;

import com.newposition.common.dao.NpSectorDao;
import com.newposition.common.dao.impl.NpSectorDaoImpl;
import com.newposition.common.domain.NpSector;
import com.newposition.common.service.ApplicationContextProviderService;
import com.newposition.common.service.ProviderService;

public class ListSectorsTag extends SimpleTagSupport
{

    private int defaultSelect = 0;

    private String defaultValue;

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public int getDefaultSelect()
    {
        return defaultSelect;
    }

    public void setDefaultSelect(int defaultSelect)
    {
        this.defaultSelect = defaultSelect;
    }

    @Override
    public void doTag() throws JspException
    {

        try
        {

            ProviderService<ApplicationContext> provider = ApplicationContextProviderService.getInstance();

            ApplicationContext context = provider.get();

            NpSectorDao dao = context.getBean(NpSectorDaoImpl.class);

            List<NpSector> sectors = dao.findAll();

            PageContext pageContext = (PageContext) getJspContext();

            JspWriter out = pageContext.getOut();

            StringBuilder builder = new StringBuilder();

            builder.append(" <option value=\"\">").append(getDefaultValue()).append("</option>\n");

            for (NpSector sector : sectors)
            {
                if (sector.getId() == getDefaultSelect())
                {
                    builder.append(" <option selected=\"selected\" value=\"").append(sector.getId()).append("\">")
                            .append(sector.getSectorName()).append("</option>\n");
                }
                else
                {
                    builder.append(" <option value=\"").append(sector.getId()).append("\">").append(sector.getSectorName())
                            .append("</option>\n");
                }
            }

            out.print(builder.toString());
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
