package com.newposition.common.presentation.tag;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.context.ApplicationContext;

import com.newposition.client.dao.NpClientDao;
import com.newposition.client.dao.impl.NpClientDaoImpl;
import com.newposition.client.domain.NpClientStatus;
import com.newposition.common.service.ApplicationContextProviderService;
import com.newposition.common.service.ProviderService;

public class ListStatusOptionsTag extends SimpleTagSupport
{

    private int defaultSelect = 0;

    public int getDefaultSelect()
    {
        return defaultSelect;
    }

    public void setDefaultSelect(int defaultSelect)
    {
        this.defaultSelect = defaultSelect;
    }

    @Override
    public void doTag() throws JspException
    {

        try
        {

            ProviderService<ApplicationContext> provider = ApplicationContextProviderService.getInstance();

            ApplicationContext context = provider.get();

            NpClientDao dao = context.getBean(NpClientDaoImpl.class);

            List<NpClientStatus> statusList = dao.fetchAllClientStatus();

            PageContext pageContext = (PageContext) getJspContext();

            JspWriter out = pageContext.getOut();

            StringBuilder builder = new StringBuilder();

            builder.append(" <option value=\"0\">Select</option>\n");

            for (NpClientStatus status : statusList)
            {
                if (status.getId() == getDefaultSelect())
                {
                    builder.append(" <option selected=\"selected\" value=\"").append(status.getId()).append("\">")
                            .append(status.getStatus()).append("</option>\n");
                }
                else
                {
                    builder.append(" <option value=\"").append(status.getId()).append("\">").append(status.getStatus())
                            .append("</option>\n");
                }
            }

            out.print(builder.toString());
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
