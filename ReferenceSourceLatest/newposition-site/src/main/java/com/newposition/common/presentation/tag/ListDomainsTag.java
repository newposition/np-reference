package com.newposition.common.presentation.tag;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.springframework.context.ApplicationContext;

import com.newposition.common.dao.NpDomainDao;
import com.newposition.common.dao.impl.NpDomainDaoImpl;
import com.newposition.common.domain.NpDomain;
import com.newposition.common.service.ApplicationContextProviderService;
import com.newposition.common.service.ProviderService;

public class ListDomainsTag extends SimpleTagSupport
{

    private int defaultSelect = 0;

    public int getDefaultSelect()
    {
        return defaultSelect;
    }

    public void setDefaultSelect(int defaultSelect)
    {
        this.defaultSelect = defaultSelect;
    }

    @Override
    public void doTag() throws JspException
    {

        try
        {

            ProviderService<ApplicationContext> provider = ApplicationContextProviderService.getInstance();

            ApplicationContext context = provider.get();

            NpDomainDao dao = context.getBean(NpDomainDaoImpl.class);

            List<NpDomain> npDomains = dao.findAll();

            PageContext pageContext = (PageContext) getJspContext();

            JspWriter out = pageContext.getOut();

            StringBuilder builder = new StringBuilder();

            builder.append(" <option value=\'' style='display:none;''>Select Domain</option>\n");

            for (NpDomain npDomain : npDomains)
            {
                if (npDomain.getId() == getDefaultSelect())
                {
                    builder.append(" <option selected=\"selected\" value=\"").append(npDomain.getId()).append("\">")
                            .append(npDomain.getTypeName()).append("</option>\n");
                }
                else
                {
                    builder.append(" <option value=\"").append(npDomain.getId()).append("\">").append(npDomain.getTypeName())
                            .append("</option>\n");
                }
            }

            out.print(builder.toString());
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
