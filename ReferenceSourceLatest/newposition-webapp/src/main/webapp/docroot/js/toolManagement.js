$(document).ready(
		function() {
			
			$("#domainFilter").on('change',
					function(event) {
						var domainFilter = $(this).val();
						var sortBy=$('#sortNpTools').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						if (domainFilter.length != 0) {
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter,"_self");
						}
					});
			
			$("#toolTypeFilter").on(
					'change',
					function(event) {
						var toolTypeFilter = $(this).val();
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var	validFilter=$('#validFilter').val();
						if (toolTypeFilter.length != 0) {
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter,"_self");
						}
					});
			
			$("#validFilter").on(
					'change',
					function(event) {
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$(this).val();
						if (validFilter.length != 0) {
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter,"_self");
						}
					});
			
			$("#sortNpTools").on(
					'change',
					function(event) {
						var sortBy = $(this).val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						if(sortBy!='Sort by')
							{
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter,"_self");
							}
					});
			
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpTools').val();
						var domainFilter=$('#domainFilter').val();
						var toolTypeFilter=$('#toolTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
							window.open('toolManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&toolTypeFilter="+toolTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
//			$('.fa-search').on('click',	function(event) {
//				alert('hi');
//				event.preventDefault();
//				var searchWord=	$('#qSearch').val();
//
//				alert(searchWord);
////					if (event.keyCode == 13) {
//						$.ajax({
//							type : "POST",
//							url : "ajax/skillManagement/aliasSearch?searchCode="+searchWord,
//							success : function(data) {
//								alert('success');
//								$('#searchResults').html(data.html);
//							},
//							error : function() {
//								alert('error');
//							}
//						});
////					}
//				});
            function getSearchData(searchCode,targetId,currentModal) {
				 $.ajax({
                     type : "POST",
                     url : "ajax/toolManagement/aliasSearch?searchCode="+searchCode+"&mergeToId="+targetId,
                     success : function(response) {
                    	 if(response!= undefined)
                  	   {
                         $('.searchResults1').html(response);
                         $(".skillDelete").css("bottom", "55.9%");
                  	   }
                     },
                     complete : function(){
                         //code to update the height of modal backdrop -start
                          var $modal        = currentModal.parents(".modal");
                           var $backdrop     = $('.modal-backdrop');
                           var $modalContent = $modal.find('.modal-dialog');
                           var $modalHeight  = $modalContent.height();
                                $backdrop.css({
                                   height: $modalHeight * 1.1
                                   })
                         //code to update the height of modal backdrop - end       
                    },
                     error : function() {
                     }
                 });
			}
			$('.quickEntryFilter input').keyup(function(event){
	            event.preventDefault();
	                if (event.keyCode == 13) {
	                 var searchCode=$(this).val();
	                 var targetModal=$(this).parents(".modal").attr("id");
	                 var targetId=$("#"+targetModal+" #addNpToolsForm #toolIds").val();
	                 if(searchCode.length)
	                 {
                         getSearchData(searchCode,targetId,$(this)); 
	            	 }
	                }
	            });
			
			$('.quickEntryFilter label').on("click",function(event){
	             event.preventDefault();
	             var searchCode=$(this).parent().find("input#qSearch").val();
	             var targetModal=$(this).parents(".modal").attr("id");
	             var targetId=$("#"+targetModal+" #addNpToolsForm #toolIds").val();
	             if(searchCode.length)
	            	 {
	            	 	getSearchData(searchCode,targetId,$(this)); 
	            	 }
	        });
			
			$("button#toolsFormDeleteBtn").on("click",function(event){
				var isSure = confirm("Are you sure you want to delete the Tool !!!");
				if (isSure) {
				 var targetModal=$(this).parents(".modal").attr("id");
				$("#"+targetModal+" #addNpToolsFormDelete").submit();
				}
				else
					{
					event.preventDefault();
					}
			});
			
			 $("form#addNpToolsForm").on("submit",function(event){
                 var toolName = $(this).find("input[name=toolName]").val();
                 var toolType = $(this).find("select[name=toolType]").val();
                 var domainId = $(this).find("select[name=domainId]").val();
                 $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
                 var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
                 for(var i=0;i<rows;i++)
              	   {
              	   var aliasSource=$(this).find(".aliasSource"+i).val();
              	  var aliasName=$(this).find(".aliasName"+i).val();
              	   if(aliasName.length > 0 && aliasSource == "")
              	   {
              	  $("span#errPop").text("Please choose Alias Source").show();
              	 event.preventDefault();
              	   }
              	   else if(aliasName.length == 0 && aliasSource > 0)
             	   {
             	  $("span#errPop").text("Please enter Alias Name").show();
             	 event.preventDefault();
             	   }
              	   }
                 if (toolName.length == 0) {
                     $("span#errPop").text("Please enter Tool Name").show();
                     event.preventDefault();
                 }
                 else if(toolType.length== 0) {
                     $("span#errPop").text("Please choose Tool type").show();
                     event.preventDefault();
                 }
                 else if(domainId == 0) {
                     $("span#errPop").text("Please choose domain").show();
                     event.preventDefault();
                 }
                 
             });
			 
			 
			 $(".edit-icon").on("click",function(event){
                 var modal=$(this).attr("data-target");
                 var id=$(this).attr("id");
                 var form=$(modal+" input[name=toolId]").val();
                 if(form != '' && form != undefined)
                   {
                       $.ajax({
                           type : "POST",
                           url : "ajax/toolManagement/editAlias?id="+form,
                           success : function(response) {
                               if(response != undefined && response!= "")
                                   {
                               $(modal+" .coAliasSection").html(response);
                                   }
                           },
                           error : function() {
                           }
                           
                       });
                   }
                 $(modal +" select[name='domainId']").val($("#domain"+id).val().trim());
                 $(modal +" select[name='toolType']").val($("#toolType"+id).val());
                 $(modal +" input[name='toolName']").val($("#toolName"+id).val());
                var valid=$("#valid"+id).val();
                 if(valid.trim() == "Valid")
                     {
                 $(modal + " #radio1").prop('checked', true);
                 $(modal + " #radio2").prop('checked', false);
                     }
                 if(valid.trim() == "Invalid")
                     {
                     $(modal + " #radio2").prop('checked', true);
                     $(modal + " #radio1").prop('checked', false);
                     }
               });
			 
			 $(".btn-rd-cor").on("click",function(event){
           $("#myModalAdd input[name='toolName']").val("");
           $("#myModalAdd select[name='toolType']").val($("#myModalAdd select[name='toolType'] option[val='']").val());
           $("#myModalAdd select[name='domainId']").val(0);
           $("#myModalAdd #radio1").prop('checked', true);
           $("#myModalAdd #radio2").prop('checked', false);
           
           $.ajax({
					type : "POST",
					url : "ajax/roleManagement/addAalias?index="+0,
					success : function(data) {
						
						$("#myModalAdd .coAliasSection").html($(data).html());
					},
					error : function() {
					}
					
				});
              });
		});