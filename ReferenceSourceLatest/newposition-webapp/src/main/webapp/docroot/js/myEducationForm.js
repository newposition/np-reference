 $(document).ready(function(){
	 $(function() {
		  $( ".datepicker-12" ).datepicker({
			  dateFormat: 'dd/mm/yy',
				  changeMonth: true,
				    changeYear: true,
				    yearRange     : "1900:+1",
					     onClose: function(dateText, inst) { 
					            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					        }
		  });
		});
	 function validateDate(dtValue)
	 {
		 var date=dtValue.split('/');
		 if( (date.length < 3)){return false;}
	    // YEAR CHECK
		if( (date[2].length != 4)  || date[2] == 0000){return false;}
		// MONTH CHECK			
		if( (date[1].length == 0 || date[1].length > 2) ||date[1] < 1 || date[1] > 12){return  false;}
		// DAY CHECK
		if( (date[0].trim().length == 0 || date[0].trim().length > 2) || date[0] < 1 || date[0] > 31){return false;}
		
		return  true;
	 }
	 function convertDate(date) {
		 var f_date=date.split('/');
		 var date1 = new Date(f_date[2],f_date[1]-1,f_date[0].trim());
		 return date1;
		 
	 }
	 function isFutureDate(date) {
		 
		 if (date > new Date()) {
			 return true;	 
		 }else {
			 return false;
		 }
		 
	 }
	 $('.pp_text').focus(function(){
		 if($(this).hasClass('errorTag'))
		 {
		 $(this).removeClass('errorTag');
		 }
	 });
	 //myaccount
	 $('.form-control').focus(function(){
		 if($(this).hasClass('errorTag'))
			 {
		 $(this).removeClass('errorTag');
			 }
	 });
	 
	 $('.datepicker-12').focus(function(){
		 $('span[class="errorMessage"]').remove();
	 });
		 $(document).delegate("#saveEducation", "click", function(event){
			 var validation=true;
			 var error=0;
		 var educationId=$('#educationForm #id').val();
		 var Institution=$("#educationForm #Institution").val();
		 if(Institution.length == 0)
			 {
			 error++;
			 $("#educationForm #Institution").addClass("errorTag");
			 validation=false;
			 }
		 var awardType=$("#educationForm #awardType").val();
		 if(awardType.length == 0)
		 {
			 error++;
			 $("#educationForm #awardType").addClass("errorTag");
			 validation=false;
		 }
		 var awardSubject=$("#educationForm #awardSubject").val();
		 if(awardSubject.length == 0)
		 {
			 error++;
			 $("#educationForm #awardSubject").addClass("errorTag");
			 validation=false;
		 }
		 var educationStartDate=$("#educationForm #educationStartDate").val();
		 var educationEndDate=$("#educationForm #educationEndDate").val();
		 var fromDate=convertDate(educationStartDate.replace(/-/g, " "));
		 var toDate= convertDate(educationEndDate.replace(/-/g, " "));
		 
		 if(educationStartDate.length == 0)
		 {
			 error++;
			 $("#educationForm #educationStartDate").addClass("errorTag");
			 validation=false;
		 }
		 
		 else if(!validateDate(educationStartDate))
		 {
			 error=6;
			 $("#educationForm #educationStartDate").addClass("errorTag");
			 validation=false;
				
		 }
		 else if(fromDate >= toDate )
			 {
			 error=7;
			 $("#educationForm  #educationEndDate").addClass("errorTag");
			 validation=false;
			 }
		 
		 if((educationEndDate.length >0 && educationEndDate !="current") && !validateDate(educationEndDate))
		 {
			 error=6;
			 $("#educationForm #educationEndDate").addClass("errorTag");
			 validation=false;
				
		 } 
		 if(validation && isFutureDate(fromDate))
		 {
		 error=10;
		 $("#educationForm  #educationStartDate").addClass("errorTag");
		 validation=false;
		 }
		 else if (validation && isFutureDate(toDate) ) {
			 $("#educationForm #educationEndDate").addClass("errorTag");
			 error=11;
			 validation=false;
		 }
			if(error >0 && error < 6)
				{
			$('.educationHistory .errorText').text('All Fields are mandatory');
				}
			else if(error == 6 )
				{
				$('.educationHistory .errorText').text('Invalid date format (provide: dd/mm/yyyy)');
				}
			else if(error == 7 )
			{
			$('.educationHistory .errorText').text('To Date must be greater than From Date');
			}
			else if(error == 10 )
			{
			$('.educationHistory .errorText').text('From Date must be less than Todays Date');
			}
			else if (error == 11)
			{
				$('.educationHistory .errorText').text('To Date should not be a future date');
			}
			
		 if(!$.isNumeric(educationId))
			 {
			 $('#educationForm #id').val('0');
			 }
			var form=$("#educationForm");
			var param=$("#educationForm").attr("name");
			if(validation)
				{
				$('.educationHistory .errorText').text('');
	        $.ajax({
	        	url:"ajax/saveEducation?param="+param,
	        	type : 'POST',
	        	async :false,
	        	data:form.serialize(),
	        	success:function(response){
	        		if(param!=null && param.trim()=="myaccount")
	        			{
	        			window.location="myaccount";
	        			}
	        		var result = jQuery.parseJSON(response);
	        		getHistories(result);
	        		clearEducationForm();
	        		event.preventDefault();
	        	},
	        	error:function(error){
	        	}
	   	 });
				}
			else
				{
				 $(function() {
					  $( ".datepicker-12" ).datepicker({
						  dateFormat: 'dd/mm/yy',
							  changeMonth: true,
							    changeYear: true,
							    yearRange     : "1900:+1",
								     onClose: function(dateText, inst) { 
								            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
								            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
								        }
					  });
					});
				}
			event.preventDefault();
	 });
		 $(document).delegate(".educationHistory .cancel", "click", function(event){
			  clearEducationForm();
			 event.preventDefault();
		 });
	  function clearEducationForm()
	  {
		  $('.educationHistory .errorText').text('');
		  $('.pp_text').removeClass('errorTag');
		     $('.educationHistory .em_box_logo-a').html('');
		     $("#educationForm #id").val('');
			 $("#educationForm #Institution").val('');
			 $("#educationForm #awardType").val('');
			 $("#educationForm #awardSubject").val('');
			 $("#educationForm #level").val('');
			 $("#educationForm #educationStartDate").val('');
			 $("#educationForm #educationEndDate").val('');
	  }
	 
	  function clearQualificationForm()
	  {
		  $('.qualifications .errorText').text('');
		     $('.pp_text').removeClass('errorTag');
		     $('.qualifications .em_box_logo-a').html('');
		     $("#qualificationForm #id").val('');
			 $("#qualificationForm #Institution").val('');
			 $("#qualificationForm #awardSubject").val('');
			 $("#qualificationForm #level").val('');
			 $("#qualificationForm #qualificationStartDate").val('');
			 $("#qualificationForm #qualificationEndDate").val('');
	  }
	 $(document).delegate(".certifications .cancel", "click", function(event){
		 clearCertificationForm();
		 event.preventDefault();
	 });
	 function clearCertificationForm()
	  {
		 $('.certifications .errorText').text('');
		 $('.pp_text').removeClass('errorTag');
		 $('.certifications .em_box_logo-a').html('');
		 $("#certificationForm #id").val('');
		 $("#certificationForm #Institution").val('');
		 $("#certificationForm #awardSubject").val('');
		 $("#certificationForm #level").val('');
		 $("#certificationForm #startDate").val('');
		 $("#certificationForm #endDate").val('');
	  }
	 $(document).delegate(".qualifications .cancel", "click", function(event){
		 clearQualificationForm();
		 event.preventDefault();
	 });
	 $(document).delegate("#saveCertification", "click", function(event){
		 var validation=true;
		 var id=$('#certificationForm #id').val();
		 var error=0;
		 var Institution=$("#certificationForm #Institution").val();
		 if(Institution.length == 0)
			 {
			 error++;
			 $("#certificationForm #Institution").addClass("errorTag");
			 validation=false;
			 }
		 var awardSubject=$("#certificationForm #awardSubject").val();
		 if(awardSubject.length == 0)
		 {
			 error++;
			 $("#certificationForm #awardSubject").addClass("errorTag");
			 validation=false;
		 }
		 var startDate=$("#certificationForm #startDate").val();
		 if(startDate.length == 0 )
		 {
			 error++;
			 $("#certificationForm #startDate").addClass("errorTag");
			 validation=false;
		 }
		 else if(!$.isNumeric(startDate)|| startDate.length > 4 || startDate.length < 4)
		 {
			 error=5;
			 $("#certificationForm #startDate").addClass("errorTag");
			 validation=false;
		 }
			if(error >=1 && error < 4)
				{
			$('.certifications .errorText').text('All Fields are mandatory');
				}
			else if(error == 5 )
				{
				$('.certifications .errorText').text('Invalid year (provide: yyyy)');
				}
			
		 
		 if(!$.isNumeric(id))
			 {
			 $('#certificationForm #id').val('0');
			 }
				var certificationForm=$("#certificationForm");
				var param=$("#certificationForm").attr("name");
				if(validation){
					$('.certifications .errorText').text('');
		        $.ajax({
		        	url:"ajax/saveCertificate?param="+param,
		        	type : 'POST',
		        	async :false,
		        	data:certificationForm.serialize(),
		        	success:function(response){
		        		if(param!=null && param.trim()=="myaccount")
	        			{
	        			window.location="myaccount";
	        			}
		        		var result = jQuery.parseJSON(response);
		        		getCertificates(result);
		        	 	clearCertificationForm();
		        		event.preventDefault();
		        		
		        		
		        	},
		        	error:function(error){
		        	}
		   		 
		   	 });
		        
				}
				else
					{
					 $(function() {
						  $( ".datepicker-12" ).datepicker({
							  dateFormat: 'yy',
								    changeYear: true,
								    yearRange     : "1900:+1",
									     onClose: function(dateText, inst) { 
									            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
									        }
						  });
						});
					}
				event.preventDefault();
			
		 });
	 $(document).delegate("#saveQualification", "click", function(event){
		 var validation=true;
		 var error=0;
		 var id=$('#qualificationForm #id').val();
		 var Institution=$("#qualificationForm #Institution").val();
		 if(Institution.length == 0)
			 {
			 $("#qualificationForm #Institution").addClass("errorTag");
			 validation=false;
			 error++;
			 }
		 var awardSubject=$("#qualificationForm #awardSubject").val();
		 if(awardSubject.length == 0)
		 {
			 $("#qualificationForm  #awardSubject").addClass("errorTag");
			 validation=false;
			 error++;
		 }
		 var qualificationStartDate=$("#qualificationForm #qualificationStartDate").val();
		 var qualificationEndDate=$("#qualificationForm #qualificationEndDate").val();
		 var fromDate=convertDate(qualificationStartDate.replace(/-/g, " "));
		 var toDate=convertDate(qualificationEndDate.replace(/-/g, " "))
		 if(qualificationStartDate.length == 0)
		 {
			 $("#qualificationForm #qualificationStartDate").addClass("errorTag");
			 validation=false;
			 error++;
		 }
		 else if(validation && !validateDate(qualificationStartDate))
		 {
			 $("#qualificationForm #qualificationStartDate").addClass("errorTag");
			 validation=false;
			 error=6;
				
		 }
		 
		 else if(validation &&  fromDate >= toDate)
			 {
			 error=7;
			 $("#qualificationForm  #qualificationEndDate").addClass("errorTag");
			 validation=false;
			 }
		
		 if(validation && (qualificationEndDate.length >0 && qualificationEndDate!="current") && !validateDate(qualificationEndDate))
		 {
			 $("#qualificationForm #qualificationEndDate").addClass("errorTag");
			 validation=false;
			error=7;
		 }
		 if(validation && isFutureDate(fromDate) )
		 {
		 error=10;
		 $("#qualificationForm  #qualificationStartDate").addClass("errorTag");
		 validation=false;
		 } 
		 else if (validation && isFutureDate(toDate))
		 {
			 error=11;
			 $("#qualificationForm  #qualificationEndDate").addClass("errorTag");
			 validation=false;
		 }
		 
			if(error >0 && error < 6)
				{
			$('.qualifications .errorText').text('All Fields are mandatory');
				}
			else if(error == 6 )
				{
				$('.qualifications .errorText').text('Invalid date format (provide: dd/mm/yyyy)');
				}
			else if(error == 7 )
			{
			$('.qualifications .errorText').text('To Date must be greater than From Date');
			}
			else if(error == 10 )
			{
			$('.qualifications .errorText').text('From Date must be less than Todays Date');
			}
			else if (error == 11 )
			{
				$('.qualifications .errorText').text('To Date should not be any future Date');
			}
		 if(!$.isNumeric(id))
			 {
			 $('#qualificationForm #id').val('0');
			 }
				var qualificationForm=$("#qualificationForm");
				var param=$("#qualificationForm").attr("name");
				if(validation)
					{
					$('.qualifications .errorText').text('');
		        $.ajax({
		        	url:"ajax/saveQualification?param="+param,
		        	type : 'POST',
		        	async :false,
		        	data:qualificationForm.serialize(),
		        	success:function(response){
		        		if(param!=null && param.trim()=="myaccount")
	        			{
	        			window.location="myaccount";
	        			}
		        		var result = jQuery.parseJSON(response);
		        		getQualifications(result);
		        		clearQualificationForm();
		        		event.preventDefault();
		        		
		        		
		        	},
		        	error:function(error){
		        	}
		   		 
		   	 });
					}
				else
					{
					 $(function() {
						  $( ".datepicker-12" ).datepicker({
							  dateFormat: 'dd/mm/yy',
								  changeMonth: true,
								    changeYear: true,
								    yearRange     : "1900:+1",
									     onClose: function(dateText, inst) { 
									            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
									            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
									        }
						  });
						});
					}
				event.preventDefault();
});
	 function getHistories(result){
		 var form="";
		 $.each(result, function(i, obj) {

     		form=form+"<li><div class='em_box apc'>";
     		
     		var logo=obj.instituteLogo;
     		
     		if(logo != null)
     			{
     		form=form +"<div class='em_box_logo'><img alt='' src='./docroot/images/"+logo+"'/>                  ";
     		form=form+ "<input type='hidden' id='instituteLogo"+obj.id+"' value='./docroot/images/"+logo+"'/></div>";
            
     			}
     		else
     			{
     			form=form +"<div class='em_box_logo'>                </div>";
     			}
     		form=form +" <div class='em_box_details'>";
     		form=form +" <div class='line1"+obj.id+"'>"+obj.institution+"</div>";
     		form=form  +"<div class='line2"+obj.id+"'>"+obj.awardType+"</div>";
     		form=form   +"<div class='line3"+obj.id+"'>"+obj.awardSubject+"</div>";
     		form=form   +"<div class='line4"+obj.id+"'>"+obj.level+"</div>";
     		form=form   +" <span class='line5"+obj.id+"'>"+obj.startDate+"</span>&nbsp;-&nbsp;";
     		form=form   +"<span class='line6"+obj.id+"'>"+obj.endDate+"</span>";
     		form=form   +"</div>";
     		form=form   +" <button id='"+obj.id+"' class='edit edit_btn_es'></button>";
     		form=form   +" <button id='"+obj.id+"' class='trash trash_btn_es'></button>";
     		form=form   +" </div></li>";
     		});
		 $('.educationHistory').find('li').slice(1).remove();
 		$('.educationHistory li').after(form);
		 
	 }
	 var Institutes=[];
	 $.ajax({
   	   url:"ajax/getInstitutes",
   	  type : 'GET',
   	   success:function(response){
   		Institutes = jQuery.parseJSON(response);
   		$( ".educationHistory #Institution" ).autocomplete({
   		 source:Institutes,
   	 });
   		$( " .certifications #Institution" ).autocomplete({
      		 source:Institutes,
      	 });
   		$( ".qualifications #Institution" ).autocomplete({
      		 source:Institutes,
      	 });
   	      },
   	   error:function(error){
   	  }
	 });
	 
	 
	 var Languages=[];
	 $.ajax({
   	   url:"ajax/getLanguages",
   	  type : 'GET',
   	   success:function(response){
   		Languages = jQuery.parseJSON(response);
   		$( "#Languages" ).autocomplete({
   		 source:Languages,
   	 });
   	      },
   	   error:function(error){
   	  }
	 });
	 
	 $("#Languages").keydown(function(event){
		    if(event.keyCode == 13 || event.keyCode == 9){
		 var list=$('#Languages').val().split(",");
		 
//		alert(list);
		 var url="ajax/saveLanguage?names="+list;
		 var cls='.language_tog .links_jsonlyclick';
		 addLanguages(list,cls,url);
		 $('#Languages').val('');
		    }
	 });
	 
	 function addLanguages(list,cls,url)
	 {
//		 var existLang = $('.language_tog').find('a').text().toLowerCase();
		 
		 $.ajax({
		   	   url:url,
		   	  type : 'GET',
		   	   success:function(response){
		   		
		   		$.each(response,function(id,obj){
		   			var lang = obj.split('-');
		   			$('.language_tog a').filter(function(){ 
		   				if ($(this).text().toLowerCase().indexOf(lang[0].toLowerCase()) > -1) {
		   					$(this).remove();
		   				}
		   				
		   			});
		   			var existLang = $('.language_tog').find('a').text().toLowerCase();
		   			if (existLang.indexOf(lang[0]) == -1) {
		   				$(cls).append("<a class='blue_link' href='javascript:void(0)'>"+obj+"<i class='fa fa-times'></i></a>");
		   			}
		   			});
		   	      },
		   	   error:function(error){
//		   		   alert("error");
		   	  }
			 });
	 }
	 function addInterest(list,cls,url)
	 {
		 var existInterest = $('.interest_tog').find('a').text().toLowerCase();
		 
		 $.ajax({
		   	   url:url,
		   	  type : 'GET',
		   	   success:function(response){
		   		$.each(response,function(id,obj){
		   			if (existInterest.indexOf(obj.toLowerCase()) == -1) {
		   				$(cls).append("<a class='blue_link' href='javascript:void(0)'>"+obj+"<i class='fa fa-times'></i></a>");
		   			}
		   			});
		   	      },
		   	   error:function(error){
//		   		   alert("error");
		   	  }
			 });
	 }
	 $("#interests").keydown(function(event){
		 if(event.keyCode == 13 || event.keyCode == 9){
		    	 var list=$('#interests').val().split(",");
		 		
				 var url="ajax/saveInterests?names="+list;
				 var cls='.interest_tog .links_jsonlyclick';
				 addInterest(list,cls,url);
				 $('#interests').val('');
		    }
		});
	 $(document).delegate(".educationHistory .edit", "click", function(event){
		 //remove validation
		 $('.pp_text').removeClass('errorTag');
		 //remove error message
		 $('.educationHistory .errorText').text('');
		 var id=$(this).attr('id');
		 var img=$('#instituteLogo'+id ).val();
		 if(img != undefined )	 {
		 $('.educationHistory .em_box_logo-a').html('<img alt="" src="'+img+'"/>');
			 }
		 else
			 {
			 $('.educationHistory .em_box_logo-a').html('');
			 }
		 $("#educationForm #id").val(id);
		 $("#educationForm #Institution").val($('.line1'+id).text());
		 $("#educationForm #awardType").val($('.line2'+id).text());
		 $("#educationForm #awardSubject").val($('.line3'+id).text());
		 $("#educationForm #level").val($('.line4'+id).text());
		 $("#educationForm #educationStartDate").val($('.line5'+id).text());
		 $("#educationForm #educationEndDate").val($('.line6'+id).text());
		 $("#educationForm .edit").attr('id',id);
		 $('.scroll').scrollLeft('0');
	 });
	 $(document).delegate(".qualifications .edit", "click", function(event){
		 $('.pp_text').removeClass('errorTag');
		 $('.qualifications .errorText').text('');
		 var id=$(this).attr('id');
		 $("#qualificationForm #id").val(id);
		 var img=$('#instituteLogo'+id ).val();
		 if(img != undefined )	 {
		 $('.qualifications .em_box_logo-a').html('<img alt="" src="'+img+'"/>');
			 }
		 else
		 {
		 $('.qualifications .em_box_logo-a').html('');
		 }
		 $("#qualificationForm #Institution").val($('.line1'+id).text());
		 $("#qualificationForm #awardType").val($('.line2'+id).text());
		 $("#qualificationForm #awardSubject").val($('.line3'+id).text());
		 $("#qualificationForm #level").val($('.line4'+id).text());
		 $("#qualificationForm #qualificationStartDate").val($('.line5'+id).text());
		 $("#qualificationForm #qualificationEndDate").val($('.line6'+id).text());
		 $("#qualificationForm .edit").attr('id',id);
		 $('.scroll').scrollLeft('0');
	 });
	 
	 $(document).delegate(".certifications .edit", "click", function(event){
		 $('.pp_text').removeClass('errorTag');
		 var id=$(this).attr('id');
		 var img=$('#instituteLogo'+id ).val();
		 $('.certifications .errorText').text('');
		 if(img != undefined ){
		 $('.certifications .em_box_logo-a').html('<img alt="" src="'+img+'"/>');
		 }
		 else
		 {
		 $('.certifications .em_box_logo-a').html('');
		 }
		 $("#certificationForm #id").val(id);
		 $("#certificationForm #Institution").val($('.line1'+id).text());
		 $("#certificationForm #awardSubject").val($('.line3'+id).text());
		 $("#certificationForm #level").val($('.line4'+id).text());
		 $("#certificationForm #startDate").val($('.line5'+id).text());
		 $("#certificationForm .edit").attr('id',id);
		 $('.scroll').scrollLeft('0');
	 });
	 $(document).delegate(".educationHistory .trash", "click", function(event){
		 var id=$(this).attr('id');
		 $.ajax({
	        	url:"ajax/removeEducation?id="+id,
	        	type : 'GET',
	        	async :false,
	        	success:function(response){
	        		var result = jQuery.parseJSON(response);
	        		getHistories(result);
	        		clearEducationForm();
	        		event.preventDefault();
	        	},
	        	error:function(error){
	        	}
	   	 });
	 });
	 $(document).delegate(".certifications .trash", "click", function(event){
		 var id=$(this).attr('id');
		 $.ajax({
	        	url:"ajax/removeCertificate?id="+id,
	        	type : 'GET',
	        	async :false,
	        	success:function(response){
	        		var result = jQuery.parseJSON(response);
	        		getCertificates(result);
	        		clearCertificationForm();
	        		event.preventDefault();
	        	},
	        	error:function(error){
	        	}
	   	 });
	 });
	 
	 function getCertificates(result)
	 {
		 var form="";
   		 $.each(result, function(i, obj) {

        		form=form+"<li><div class='em_box apc'>";
        		var logo=obj.instituteLogo;
         		if(logo != null)
         			{
         			form=form +"<div class='em_box_logo'><img alt='' src='./docroot/images/"+logo+"'/>                  ";
             		form=form+ "<input type='hidden' id='instituteLogo"+obj.id+"' value='./docroot/images/"+logo+"'/></div>";
                 			}
         		else
         			{
         			form=form +"<div class='em_box_logo'>                </div>";
         			}
        		form=form +" <div class='em_box_details'>";
        		form=form +" <div class='line1"+obj.id+"'>"+obj.institution+"</div>";
        		form=form   +"<div class='line3"+obj.id+"'>"+obj.awardSubject+"</div>";
        		form=form   +"<div class='line4"+obj.id+"'>"+obj.level+"</div>";
        		form=form   +" <div class='line5"+obj.id+"'>"+obj.startDate+"</div>";
        		form=form   +"</div>";
        		form=form   +" <button id='"+obj.id+"' class='edit edit_btn_es'></button>";
        		form=form   +" <button id='"+obj.id+"' class='trash trash_btn_es'></button>";
        		form=form   +" </div></li>";
        		});
   		 
    		 $('.certifications').find('li').slice(1).remove();
    	 		$('.certifications li').after(form);
	 }
	 $(document).delegate(".qualifications .trash", "click", function(event){
			 var id=$(this).attr('id');
			 $.ajax({
		        	url:"ajax/removeQualification?id="+id,
		        	type : 'GET',
		        	async :false,
		        	success:function(response){
		        		var result = jQuery.parseJSON(response);
		        		getQualifications(result);
		        		clearQualificationForm();
		        		event.preventDefault();
		        	},
		        	error:function(error){
		        	}
		   	 });
		 });
	 
	 function getQualifications(result)
	 {
			var form="";
      		 $.each(result, function(i, obj) {

           		form=form+"<li><div class='em_box apc'>";
           		var logo=obj.instituteLogo;
         		if(logo != null)
         			{
         			form=form +"<div class='em_box_logo'><img alt='' src='./docroot/images/"+logo+"'/>                  ";
             		form=form+ "<input type='hidden' id='instituteLogo"+obj.id+"' value='./docroot/images/"+logo+"' /></div>";
                    	}
         		else
         			{
         			form=form +"<div class='em_box_logo'>                </div>";
         			}
           		form=form +" <div class='em_box_details'>";
           		form=form +" <div class='line1"+obj.id+"'>"+obj.institution+"</div>";
           		form=form   +"<div class='line3"+obj.id+"'>"+obj.awardSubject+"</div>";
           		form=form   +"<div class='line4"+obj.id+"'>"+obj.level+"</div>";
           		form=form   +" <span class='line5"+obj.id+"'>"+obj.startDate+"</span>&nbsp;-&nbsp;";
           		form=form   +"<span class='line6"+obj.id+"'>"+obj.endDate+"</span>";
           		form=form   +"</div>";
           		form=form   +" <button id='"+obj.id+"' class='edit edit_btn_es'></button>";
           		form=form   +" <button id='"+obj.id+"' class='trash trash_btn_es'></button>";
           		form=form   +" </div></li>";
           		});
      		 $('.qualifications').find('li').slice(1).remove();
 	 		$('.qualifications li').after(form);
      		 
	 }
	 $(document).delegate(".educationHistory .trash", "click", function(event){
		 var id=$(this).attr('id');
		 
		 $.ajax({
	        	url:"ajax/removeEducation?id="+id,
	        	type : 'GET',
	        	async :false,
	        	success:function(response){
	        		var result = jQuery.parseJSON(response);
	        		getHistories(result);
	        		event.preventDefault();
	        	},
	        	error:function(error){
	        	}
	   	 });
	 });
	 $(document).delegate(".interest_tog .links_jsonlyclick a", "click", function(event){
		 var interestName = $(this).text().trim();
			

		 var request= $.ajax({
	        	url:"ajax/removeInterest?interestName="+interestName,
	        	type : 'GET',
	        	async :false,
	   	 });
		 $(this).remove();
	 });
	 $(document).delegate(".language_tog .links_jsonlyclick a", "click", function(event){
		 var text = $(this).text().trim().split('-');
		 var languageName= text[0];
		 var request= $.ajax({
	        	url:"ajax/removeLanguage?languageName="+languageName,
	        	type : 'GET',
	        	async :false,
	   	 });
		 $(this).remove();
	 });
	
	 $(".row .education_history_head").click(function(){
			$('.Education_history_row').toggle();
			$(this).toggleClass("hpe");
			$(this).toggleClass("hpe_plus");
			});
	 $(".row .personal_qualification_head").click(function(){
			$('.personal_qualification_row').toggle();
			$(this).toggleClass("hpe");
			$(this).toggleClass("hpe_plus");
			});
	 $(".row .certification_head").click(function(){
			$('.certification_row').toggle();
			$(this).toggleClass("hpe");
			$(this).toggleClass("hpe_plus");
			});
	 $(".row .languages_head").click(function(){
			$('.language_tog').toggle();
			$(this).toggleClass("hpe");
			$(this).toggleClass("hpe_plus");
			});
	 $(".row .interest_head").click(function(){
			$('.interest_tog').toggle();
			$(this).toggleClass("hpe");
			$(this).toggleClass("hpe_plus");
			});
 });
