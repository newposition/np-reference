var referenceForm = function() {

	return {
		init : function() {
			// on page loading code should present here
			
			$( ".datepicker-12" ).datepicker({
				  dateFormat: 'dd/mm/yy',
					  changeMonth: true,
					    changeYear: true,
					    yearRange     : "1900:+1",
						     onClose: function(dateText, inst) { 
						            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						        }
			  });
			
			$("#btn-send").on("click",submitReference);
			$("#Button1").on("click",preferNotGiveReference);
		},


	};
		
}();

var submitReference = function(event) {
	var status = $("input[name=referenceVerified]").val();
		$("input[name=referenceVerified]").val("Accepted");
	$("#form-contact").submit();
};
var preferNotGiveReference = function(event) {
	var status = $("input[name=referenceVerified]").val();
		$("input[name=referenceVerified]").val("Rejected");
	$("#form-contact").submit();
};

$(document).ready(function() {
	
	referenceForm.init();
	
});