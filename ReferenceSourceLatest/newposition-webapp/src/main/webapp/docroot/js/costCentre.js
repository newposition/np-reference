$(document).ready(function() {
	
	var listOfDataCostcentre = null;
	var noOfRows=0;
	var noOfButtons=0;
	var noOfRowsToDisplay=getNoOfResultPerPage();
	$('span#msg').delay(5000).fadeOut();
	function getNoOfResultPerPage(){
		
		noOfRowsToDisplay = $("#perPageResult").val();
	//	alert(noOfRowsToDisplay);
		return noOfRowsToDisplay;
	}
	
	$.ajax({
		url : "ajax/getAllCostCentre",
		type : "GET",
		success : function(data) {
			getListOfCostCentre(data);

		},
		error : function(err) {
		}
	});
	
	
	function getListOfCostCentre(list) {
		listOfDataCostcentre = list;
	//	alert(list.length);
		getListOfCostCentre1(list);
		
	}
	
	function getListOfCostCentre1(listOfDataCostcentre){
	    noOfRows=listOfDataCostcentre.length;
	    $("#noOfRows").html(noOfRows);
		var num=0;
		var num1=noOfRows;
		num=parseInt(noOfRows/noOfRowsToDisplay);
		
		if(noOfRows>noOfRowsToDisplay){
		for(var i=0;i<num;i++){
			num1=num1-noOfRowsToDisplay;
		}
		if(num1==0){
			noOfButtons=num;

		}else{
			noOfButtons=num+1;
		}
		}else{
			noOfButtons=1;
		}
	//	alert("num: "+num+" noBtn: "+noOfButtons);
		for(var i=0;i<listOfDataCostcentre.length;i++){
				var obj=listOfDataCostcentre[i];
				$("#table1Div").append("<div class='row no-pad' id='div"+(i+1)+"'><div class='bds_panel'><div class='row no-pad'><div class='col-md-10'><div class='dtl_list'><div class='col-md-12 dtl_ct'><h3 class='dtl_h3'>"+obj.costCentreCode+"</h3><p>"+obj.costCentreName+"<br>"+obj.costCentreLocation+"<br>"+obj.description+"<br>"+obj.costCentreSystem+"<br>"+"</p></div></div></div><div class='col-md-2'><div class='dtl_tab-b'><a class='detail' href='#' id="+obj.costCentreId+" data-toggle='modal' data-target='#myModalEdit'><i class='fa fa-list-alt'></i>Details</a></div><div class='dtl_tab-b'><a class='deleteBtn' href='javascript:void(0)' id = '"+obj.costCentreId+"'><i class='fa fa-trash'></i>Delete</a></div></div></div></div></div>");
	    }

		for(var i=0;i<noOfButtons;i++){
			$("#indexId2").append("<li id='li"+(i+1)+"'><a id='hr"+(i+1)+"' href='#0' class = 'navNo' >"+(i+1)+"</a></li>");
		}
			if(parseInt(noOfButtons)>0){
				$("#hr1").attr("class","current navNo");
			}
			var starting=1;
			var ending=getNoOfResultPerPage();
			var count=0;
			for(var i=0;i<listOfDataCostcentre.length;i++){
				count++;
				if(count>=starting && count<=ending){
					$("#div"+(i+1)).show();
				}else{
					$("#div"+(i+1)).hide();
				}
			}
	}

	function deleteRows(listOfDataCostcentre){
	    var size=listOfDataCostcentre.length;
	    for(var i=0;i<size;i++){
	    listOfDataCostcentre.splice(0,1);
	    $("#div"+(i+1)).remove();
	    }
	    noOfRows=0;
	    deleteButton(noOfButtons);
	}
	function deleteButton(noOfButtons){
//		alert("in delete button");
		for(var i=0;i<noOfButtons;i++){
			$("#li"+(i+1)).remove();
			}
		noOfButtons=0;
	}
	

$(document).delegate(".deleteBtn","click",function(){
		var ccId = $(this).attr("id");
		if (confirm('Are you sure you want to the cost centre !!!!!')) {
		   	$.ajax({
				url :  "ajax/deleteCostCentre?id="+ccId,
				type : "Get",
				async : false,
			    dataType:"json",
				success : function(data) {
				        var count = $('span.count').text();
						var _count =  parseInt(count);
						$('span.count').text(_count-1);
						$('span#msg').text("CostCentre deleted successfully").delay(5000).fadeOut();
						deleteRows(listOfDataCostcentre);
				    	listOfDataCostcentre=data;
				    	addRows(listOfDataCostcentre);
				}
			});
		} else {
		   return false;
		}
	});

	


	function addRows(onLoadProjectList){
	     noOfRows=onLoadProjectList.length;
	    	
	    for(var i=0;i<onLoadProjectList.length;i++){
				var obj=onLoadProjectList[i];
				$("#table1Div").append("<div class='row no-pad' id='div"+(i+1)+"'><div class='bds_panel'><div class='row no-pad'><div class='col-md-10'><div class='dtl_list'><div class='col-md-12 dtl_ct'><h3 class='dtl_h3'>"+obj.costCentreCode+"</h3><p>"+obj.costCentreName+"<br>"+obj.costCentreLocation+"<br>"+obj.description+"<br>"+obj.costCentreSystem+"<br>"+"</p></div></div></div><div class='col-md-2'><div class='dtl_tab-b'><a class='detail' id="+obj.costCentreId+" href='#' data-toggle='modal' data-target='#myModalEdit'><i class='fa fa-list-alt'></i>Details</a></div><div class='dtl_tab-b'><a class='deleteBtn' id="+obj.costCentreId+" href='javascript:void(0)' ><i class='fa fa-trash'></i>Delete</a></div></div></div></div></div>");
			}							
		var num=0;
		var num1=noOfRows;
		num=parseInt(noOfRows/noOfRowsToDisplay);

		if(noOfRows>noOfRowsToDisplay){
		for(var i=0;i<num;i++){
			num1=num1-noOfRowsToDisplay;
		}
		if(num1==0){
			noOfButtons=num;

		}else{
			noOfButtons=num+1;
		}
		}else{
			noOfButtons=1;
		}
		//alert("num: "+num+" noBtn: "+noOfButtons);
			for(var i=0;i<noOfButtons;i++){
				$("#indexId2").append("<li id='li"+(i+1)+"'><a id='hr"+(i+1)+"' href='#0' class = 'navNo' >"+(i+1)+"</a></li>");
			}
			if(parseInt(noOfButtons)>0){
				$("#hr1").attr("class","current navNo");
			}
			var starting=1;
			var ending=getNoOfResultPerPage();
			var count=0;
			for(var i=0;i<onLoadProjectList.length;i++){
				count++;
				if(count>=starting && count<=ending){
					$("#div"+(i+1)).show();
				}else{
					$("#div"+(i+1)).hide();
				}
			}
	}	

	$("select#select3").change(function() {
		var str=$(this).val();
		if(str==""){
			return false;
		}
		$.ajax({
			url :  "ajax/sortCostCentreData?str="+str,
			type : "Post",
			async : false,
			data  : JSON.stringify(listOfDataCostcentre),
		    headers: {
	                       "Content-Type": "application/json;charset=UTF-8"
	                   },
			success : function(data) {
			   deleteRows(listOfDataCostcentre);
			   listOfDataCostcentre=data;
			    	addRows(data);
			    	
			}
		});
	});
	
	$(document).delegate("#btn-edit-pp","click",function() {
	   
		var dd= $(this).parents("form");
		dd.find('input[type=text]').removeProp("readonly");
		dd.find('input[type=file]').removeProp("disabled");
		dd.find('textarea').removeProp("readonly");
		//var inputReadonly = dd.find('input[type=text][readonly=readonly]').length;
	//	var textReadonly = dd.find('textarea[readonly=readonly]').length;
		
	});
	
	$(document).delegate("#btn-confirm-submit","click",function() {
		var dd= $(this).parents("form");
		
		var inputReadonly = dd.find('input[type=text][readonly=readonly]').length;
		var textReadonly = dd.find('textarea[readonly=readonly]').length;
		if (inputReadonly == 0 && textReadonly==0) {
			dd.submit();
		} else {
			return false;
		}
		
	});
	

	var buttonPressed=1;
	//$(document).undelegate(".navNo",'click');
	$(document).delegate(".navNo",'click',function (){
		var num = $(this).text();
		if(parseInt(num)>1){
			$("#ancher1").removeAttr("class");
			$("#ancher2").removeAttr("class");
		}
		if(parseInt(num)==1){
			$("#ancher1").attr("class","disabled");
			$("#ancher2").attr("class","disabled");
			$("#ancher3").removeAttr("class");
			$("#ancher4").removeAttr("class");
		}
		if(parseInt(num)==noOfButtons){
			$("#ancher1").removeAttr("class");
			$("#ancher2").removeAttr("class");
			$("#ancher3").attr("class","disabled");
			$("#ancher4").attr("class","disabled");

		}
	//	alert(buttonPressed);
		$("#hr"+buttonPressed).removeAttr("class");
		$("#hr"+buttonPressed).attr("class","navNo");
		$("#hr"+num).attr("class","current navNo");
		buttonPressed=num;
		displaySelected(buttonPressed);
	});


	// move to first
	$(document).delegate("#ancher1",'click',function (){
		$("#hr"+buttonPressed).removeAttr("class");
		$("#hr"+buttonPressed).attr("class","navNo");
		buttonPressed=1;
		$("#hr"+buttonPressed).attr("class","current navNo");
		displaySelected(buttonPressed);
		$("#ancher1").attr("class","disabled");
		$("#ancher2").attr("class","disabled");
		$("#ancher3").removeAttr("class");
		$("#ancher4").removeAttr("class");
	});

	//move one step back
	$(document).delegate("#ancher2",'click',function (){
		if(buttonPressed>1){
			$("#hr"+buttonPressed).removeAttr("class");
			$("#hr"+buttonPressed).attr("class","navNo");
			
			buttonPressed=buttonPressed-1;
			$("#hr"+buttonPressed).attr("class","current navNo");
			displaySelected(buttonPressed);
		}
		if(buttonPressed==1){
			$("#ancher1").attr("class","disabled");
			$("#ancher2").attr("class","disabled");
			$("#ancher3").removeAttr("class");
			$("#ancher4").removeAttr("class");
		}
	});

	// move one step right
	$(document).delegate("#ancher3",'click',function (){
		if(buttonPressed<noOfButtons){
		$("#hr"+buttonPressed).removeAttr("class");
		$("#hr"+buttonPressed).attr("class","navNo");
		
		buttonPressed=buttonPressed+1;
		$("#hr"+buttonPressed).attr("class","current");
		displaySelected(buttonPressed);
		}
		if(buttonPressed==noOfButtons){
			$("#ancher1").removeAttr("class");
			$("#ancher2").removeAttr("class");
			$("#ancher3").attr("class","disabled");
			$("#ancher4").attr("class","disabled");
		}
	});

	// move to last
	$(document).delegate("#ancher4",'click',function (){
		$("#hr"+buttonPressed).removeAttr("class");
		$("#hr"+buttonPressed).attr("class","navNo");
		
		buttonPressed=noOfButtons;
		$("#hr"+buttonPressed).attr("class","current navNo");
		
		displaySelected(buttonPressed);
		
		$("#ancher1").removeAttr("class");
		$("#ancher2").removeAttr("class");
		$("#ancher3").attr("class","disabled");
		$("#ancher4").attr("class","disabled");
	});

	function displaySelected(buttonPressed){
		
		var starting=buttonPressed*noOfRowsToDisplay-(noOfRowsToDisplay-1);
		var ending=buttonPressed*noOfRowsToDisplay;
		var count=0;
		for(var i=0;i<listOfDataCostcentre.length;i++){
			count++;
			if(count>=starting && count<=ending){
				$("#div"+(i+1)).show();
			}else{
				$("#div"+(i+1)).hide();
			}
		}
		
	}
	
	$(document).delegate("#Text6","keyup",function submitFunction(id){
		var code=$(this).val();
		var curr = $(this);
		var is = true;
		$.ajax({
			url :  "ajax/checkCostCentreCode?id="+code,
			type : "GET",
			async : false,
		    dataType:"json",
			success : function(data) {
				if(data!="OK"){
					is = false;
				}
			}
		});
		if (!is) {
			curr.parent().append("<input type='hidden'value='exist' id='isCodeExist' />");
		}
	});
	
	$("#form-cost-centre-add").on("submit","form",function(event){
		var flag = $(this).find('input#isCodeExist').length;
		if (flag > 0 ) {
			event.preventDefault();
			return false;
		}
	});
	
	
	$(document).delegate("#editFile","change",function showVideoFile(event){
		var vid = $(this).parent().find("Video");
		if (this.files && this.files[0]) {
			
			 var filerdr = new FileReader();
			 filerdr.onload = function(event) {
				 vid.attr('src', event.target.result);
			 },
			 filerdr.readAsDataURL(this.files[0]);
			 }
		
	});
	$(".modal").on("shown.bs.modal", function () { 
        var id =$(this).attr("id");
       
        if(id == 'myModalAdd') {
           // alert("sdf");
        }else {
    	var inputReadonly = $(this).find('form').find('input[type=text][readonly=readonly]').length;
		var textReadonly = $(this).find('form').find('textarea[readonly=readonly]').length;
		//alert(inputReadonly+" "+textReadonly)
		if(inputReadonly <= 1 && textReadonly==0) {
            $(this).find('form').find('input[type=text]').prop('readonly','readonly');
            $(this).find('form').find('textarea').prop('readonly','readonly');
            $(this).find('form').find('input[type=file]').prop('disabled','disabled');
		}
        }
    });
    $(".modal").on("hide.bs.modal", function () { 
        
        $(".has-error").removeClass("has-error");
        $(".error").removeProp("style");
        $(".error").removeClass("error");
        $(".form-error").remove();
        $(this).find('form').find('input[type=text]').prop("value","");
        $(this).find('form').find('input[type=hidden]').prop("value","");
         $(this).find('form').find('textarea').prop("value","");
         var control = $(this).find('form').find('input[type=file]');
             control.replaceWith( control = control.clone( true ) );
             $(this).find('form').find('video').prop("src","");
    });
    
    $(document).delegate("#select1","change",function(){
		var str=$(this).val();
		$('#select2')
		    .find('option')
		    .remove()
		    .end()
		    .append('<option value="" style="display:none">Select '+str+'</option>');
		    
		if(str==""){
			$.ajax({
				url : "ajax/getAllCostCentre",
				type : "GET",
				success : function(data) {
					getListOfCostCentre(data);
                    $('span.count').text(data.length);
				},
				error : function(err) {
				}
			});
		}else{
		$.ajax({
			url :  "ajax/getFilterStringList?str="+str,
			type : "Get",
			async : false,
		    dataType:"json",
			success : function(data) {
				$.each(data, function(i, obj) { 
					var div_data = "<option value='"+obj+"'>" + obj + "</option>";
					$('#select2').append(div_data);
				});
			}
		});
		}
	});
	$(document).delegate("#select2","change",function(){
		var str1=$("#select1").val();	
		var str2=$(this).val();

		if(str1!=""&&str2!=""){
		$.ajax({
			url :  "ajax/filterCCData?str1="+str1+"&str2="+str2,
			type : "Get",
			async : false,
		    dataType:"json",
			success : function(data) {
				deleteRows(listOfDataCostcentre);
				listOfDataCostcentre=data;
			    	addRows(listOfDataCostcentre);
			    	$('span.count').text(data.length);
			    	
			}
		});
		}else{
		    $.ajax({
		url :  "ajax/getAllCostCentre",
		type : "Post",
		async : false,
	    dataType:"json",
		success : function(data) {
			deleteRows(listOfDataCostcentre);
			listOfDataCostcentre=data;
		    	addRows(listOfDataCostcentre);
		    	$('span.count').text(data.length);
			    
		}
	});
		}
	});
	
	
	// details getting on click
	$(document).delegate("a.detail","click",function(){
	   var ccid = $(this).attr("id");
	   //alert("clicked "+ccid);
	   
   $.ajax({
		url :  "ajax/getCostCentreById?id="+ccid,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
		//	alert(data.costCentreId);
			setCosrCentreDetails(data);
		}
	});
   });
	
	function setCosrCentreDetails(costCentre) {
	    var detailsPop = $("#myModalEdit").find("form");
	    detailsPop.find('input[name=costCentreId]').val(costCentre.costCentreId);
	    detailsPop.find('input[name=costCentreCode]').val(costCentre.costCentreCode);
	    detailsPop.find('input[name=costCentreName]').val(costCentre.costCentreName);
	    detailsPop.find('input[name=location]').val(costCentre.costCentreLocation);
	    detailsPop.find('input[name=costCentreOwner]').val(costCentre.costCentreOwner);
	    detailsPop.find('input[name=costCentreSystem]').val(costCentre.costCentreSystem);
	    detailsPop.find('input[name=description]').val(costCentre.description);
	    detailsPop.find('video').attr("src",$("input#url"+costCentre.costCentreId).val());
	    detailsPop.find('textarea[name=overview]').val(costCentre.costCentreDoc.comments);
	    
	    
	}
	
});



 