 $(document).ready(function(){
	 $("form").mouseover(function(){
		 var id = $(this).attr('id');
		 $('#'+id+' #coveringNoteText .note-toolbar').css('display','block');
         $('#'+id+' #coveringNoteText .note-editor .note-statusbar').css('display','block');
         $('#'+id+' #coveringNoteText .note-editor .note-editable').css('height','139');
         $('#'+id+' #coveringNoteText .note-editor .note-style').css('display','none');
 		$('#'+id+' #coveringNoteText .note-editor .note-height').css('display','none');
 		$('#'+id+' #coveringNoteText .note-editor .note-table ').css('display','none');
 		$('#'+id+' #coveringNoteText .note-editor .note-insert').css('display','none');
 		$('#'+id+' #coveringNoteText .note-editor .note-view').css('display','none');
 		$('#'+id+' #coveringNoteText .note-editor .note-help').css('display','none');
	 });
	 
	 $("form").mouseout(function(e){
		 var id = $(this).attr('id');
		 $('#'+id+' #coveringNoteText .note-toolbar').css('display','none');
         $('#'+id+' #coveringNoteText .note-editor .note-statusbar').css('display','none');
         $('#'+id+' #coveringNoteText .note-editor .note-editable').css('height','188');
     });
	 
	 $("form").submit(function(event){
			$(".errorMessage").hide();
			var id = $(this).attr('id');
			 	if (id != 'invite') {
							if ($(this).attr('id') == 'sponsorForm') {
									var fname = $(this).find("input[name='firstName']").val();
							        var lname = $(this).find("input[name='surName']").val();
							        var relationship = $(this).find("select[name='relationship']").val();
							        var email = $(this).find("input[name='email']").val();
							        var dateFrom = $(this).find("input[name='dateFrom']").val();
							        var statement = $(this).find(".note-editable").text();
							        var isEmpty = false;
							          if (fname == "" ){
							        	  $(this).find("#info_fname" ).text( "Please enter the First name" ).show();
							              isEmpty=true;
							          }
							           if (lname == "" ){
							              $(this).find( "#info_sname" ).text( "Please enter the Surname" ).show();
							              isEmpty=true;
							          }
							          
						               if (email == ""){
						
							              $(this).find( "#info_email" ).text( "Please enter a valid email" ).show();
							              isEmpty=true;
							          }
						               if (email.length > 45){
						           		
								              $(this).find( "#info_email" ).text( "Entered email is too long " ).show();
								              isEmpty=true;
								          }
							           if (relationship == "" ){
								              $(this).find( "#info_relationship" ).text( "Please select a relationship" ).show();
								              isEmpty=true;
								          }
							           if (statement.length > 150){
							           		
								              $(this).find( "#info_stmt" ).text( "Entered comment contains more than 150 characters" ).show();
								              isEmpty=true;
								          }
							           
							           if (dateFrom == "" ){
								              $(this).find( "#info_fromdate" ).text( "Please enter From date" ).show();
								              isEmpty=true;
								          }
							           if(isEmpty){
							        	   event.preventDefault();
							           }
						
					}
					else {  // if the sponsor details are edited
//						alert("edit"+$(this).find('input[type=text](:disabled)').length);
						if ($(this).find('input.pp_text:not(:disabled)').length > 0) { 
							var fname = $(this).find("input[name='_firstName']").val();
					        var lname = $(this).find("input[name='_surName']").val();
					        var relationship = $(this).find("select[name='_relationship']").val();
					        var email = $(this).find("input[name='_email']").val();
					        var dateFrom = $(this).find("input[name='_dateFrom']").val();
					        var statement = $(this).find(".note-editable").text();
					        var isEmpty = false;
					          if (fname == "" ){
					        	  $(this).find("#info_fname" ).text( "Please enter the first name" ).show();
					              isEmpty=true;
					          }
					           if (lname == "" ){
					              $(this).find( "#info_sname" ).text( "Please enter the surname" ).show();
					              isEmpty=true;
					          }
					          
				               if (email == ""){
				
					              $(this).find( "#info_email" ).text( "Please enter a valid email" ).show();
					              isEmpty=true;
					          }
				               if (email.length > 45){
				           		
						              $(this).find( "#info_email" ).text( "Entered email is too long please " ).show();
						              
						              isEmpty=true;
						          }
					           if (relationship == "" ){
						              $(this).find( "#info_relationship" ).text( "Please select a relationship" ).show();
						              isEmpty=true;
						          }
					           if (statement.length > 150){
					           		
						              $(this).find( "#info_stmt" ).text( "Entered comment contains more than 150 characters" ).show();
						              isEmpty=true;
						          }
					           
					           if (dateFrom == "" ){
						              $(this).find( "#info_fromdate" ).text( "Please enter From date" ).show();
						              isEmpty=true;
						          }
					           if(isEmpty){
					        	   event.preventDefault();
					           }
				
						} else{
							$('#'+id+' #coveringNoteText .note-editor .note-editable').attr('contenteditable',true);
					    	$(this).find('input').prop('disabled',false);
					    	$(this).find('textarea').prop('disabled',false);
					    	$(this).find('select').prop('disabled',false);
							 event.preventDefault();
						}
					}
			 	}
		 });
    
     $( '.pp_textarea' ).summernote({
         height: 168
       });
     $('.note-toolbar').css('display','none');
     $('.note-editor .note-statusbar').css('display','none');
	 
	 
	 $( ".datepicker-12" ).datepicker({
		
		 yearRange     : "-30:+1",
		 changeMonth: true,
	     changeYear: true,
	     dateFormat: 'dd/mm/yy',
	     onClose: function(dateText, inst) { 
	            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        }
		 
	 });
 });
