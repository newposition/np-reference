var myFuturePage = function() {

	return {
		init : function() {

			// on page loading code should present here

			$('#myFutureDetails input').keydown(function(e) {
				if (e.keyCode == 13) {
					// $('#form').submit();
					e.preventDefault();
				}
			});

			$('#myFutureDetails #personalTextStatement').mouseover(function(e){
				$('#personalTextStatement .note-toolbar').css('display','block');
				
				$('#personalTextStatement .note-editor .note-style').css('display','none');
				$('#personalTextStatement .note-editor .note-height').css('display','none');
				$('#personalTextStatement .note-editor .note-table ').css('display','none');
				$('#personalTextStatement .note-editor .note-insert').css('display','none');
				$('#personalTextStatement .note-editor .note-view').css('display','none');
				$('#personalTextStatement .note-editor .note-help').css('display','none');
				$('#personalTextStatement .note-editor .note-editable').css('height','125');
				$('#personalTextStatement .note-editor .note-statusbar').css('display','block');
				
			});
			$('#myFutureDetails #personalTextStatement').mouseout(function(e){

				$('#personalTextStatement .note-toolbar').css('display','none');
				$('#personalTextStatement  .note-editor .note-statusbar').css('display','none');
				$('#personalTextStatement .note-editor .note-editable').css('height','173');
				
			});
			$('#myFutureDetails #coveringLetterText').mouseover(function(e){
				$('#coveringLetterText .note-toolbar').css('display','block');
				
				$('#coveringLetterText .note-editor .note-style').css('display','none');
				$('#coveringLetterText .note-editor .note-height').css('display','none');
				$('#coveringLetterText .note-editor .note-table ').css('display','none');
				$('#coveringLetterText .note-editor .note-insert').css('display','none');
				$('#coveringLetterText .note-editor .note-view').css('display','none');
				$('#coveringLetterText .note-editor .note-help').css('display','none');
				$('#coveringLetterText .note-editor .note-editable').css('height','134');
				$('#coveringLetterText .note-editor .note-statusbar').css('display','block');
				
			});
			$('#myFutureDetails #coveringLetterText').mouseout(function(e){

				$('#coveringLetterText .note-toolbar').css('display','none');
				$('#coveringLetterText .note-editor .note-statusbar').css('display','none');
				$('#coveringLetterText .note-editor .note-editable').css('height','185');
				
			});
			
			myFuturePage.getDATA("#skills_input", "ajax/getListOfSkills");

			myFuturePage.getDATA("#Eligblity_input", "ajax/getCountries");

			myFuturePage.getDATA("#tool_input", "ajax/getListOfTools");
			
			myFuturePage.getDATA("#role_input", "ajax/getListOfRoles");

			$('#skills_input').keyup( myFuturePage.addNewSkill);

			$('#role_input').keyup( myFuturePage.addNewRole);

			$('#tool_input').keyup( myFuturePage.addNewTool);

			$('#input_FC').keyup(myFuturePage.addFutureCompany);
			
			$('#input_ExC').keyup(myFuturePage.addExculudeEmployer);

			$('#Eligblity_input').keyup( myFuturePage.addCountry);

			$("#Permanent_select").change(myFuturePage.changeTypeValue);


			$(" .list_skills   ").on('click', 'i',
					myFuturePage.removeSkill);
			$(" .list_skills  ").on('click','a',
					myFuturePage.statusSkill);
			$(" .list_roles  ").on('click','a',
					myFuturePage.statusRole);
			$(" .list_tools ").on('click','a',
					myFuturePage.statusTool);

			$(".links_jsonlyclick.list_roles ").on('click', 'i',
					myFuturePage.removeRoles);

			$(".links_jsonlyclick.list_tools ").on('click', 'i',
					myFuturePage.removeTools);

			$(".links_jsonlyclick.future_comp ").on('click', 'a',myFuturePage.removeFutureEmployer);

			$(".links_jsonlyclick.exclude_comp ").on('click', 'a',
					myFuturePage.removeFutureEmployer);

			$(".links_js.links_jsonlyclick.list_countries ").on('click', 'a',
					myFuturePage.removeCountry);

			$('input[id=lefile]').change(function() {
				$('#photoCover').val($(this).val());
			});
            $('#finish').click(myFuturePage.checkValidation);
            
            $('#Permanent_select').change(function(){
            	$("#dayRate_error" ).hide();
            	$("#bonus_error" ).hide();
            	$("#compensation_error" ).hide();
            	$("#errorMsg" ).hide();
            });
            
		},

		changeTypeValue : function() {

			$("select option:selected").each(function() {
				if ($(this).attr("value") == "Permanent") {
					$(".permanent_row").show();
					$(".contract_row").hide();
					$(".contract_row input[type='text']").val('');
				}
				if ($(this).attr("value") == "Contract") {
					$(".contract_row").show();
					$(".permanent_row").hide();
					$(".permanent_row input[type='text']").val('');
				}
				if ($(this).attr("value") == "Donotmind") {
					$(".permanent_row,.contract_row").show();
				}
			});

		},

		
		removeSkill : function() {

			var text = $(this).closest('a').text().trim();
			var request = $.ajax({
				url : "ajax/removeSkill",
				method : "POST",
				data : {
					skillName : text
				},
			});
			$(this).closest('a').remove();

			request.fail(function(msg) {
//				window.alert("bad request");
			});

		},
		addNewSkill : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
			if (text.length) {
				var request = $.ajax({
					url : "ajax/addSkill",
					method : "POST",
					data : {
						skills : text
					},
				});

				request
						.done(function(msg) {
							
//							if (msg != "") {
							$.each(msg,function(i,obj){
								$('.list_skills')
										.append(
												"<a class='blue_link' href='javascript:void(0)'>"
														+ obj
														+ " <i class='fa fa-times'> </>");
							});
								$(".errSkill").text("").hide();
//							} else {
//								$(".errSkill")
//										.text(
//												"This Skill is already preasent in your prefer list")
//										.show();
//							}
							$('#skills_input').val("");
						});

				request.fail(function(msg) {
//					window.alert("bad request");
				});
			} else {
				$(".errSkill").text("").hide();
			}
			}

		},

		addNewRole : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
			if (text.length) {
				var request = $.ajax({
					url : "ajax/addRole",
					method : "POST",
					data : {
						roles : text
					},
				});

				request
						.done(function(msg) {
//							if (msg != "") {
							$.each(msg,function(i,obj){
								$('.list_roles')
										.append(
												"<a class='blue_link' href='javascript:void(0)'>"
														+ obj
														+ " <i class='fa fa-times'> </>");
							});
								$(".errRole").text("").hide();
//							} else {
//								$(".errRole")
//										.text(
//												"This Role is already preasent in your prefer list")
//										.show();
//							}
							$('#role_input').val("");
						});

				request.fail(function(msg) {
//					window.alert("bad request");
				});
			} else {
				$(".errRole").text("").hide();
			}
		}

		},

		addNewTool : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
			if (text.length) {
				var request = $.ajax({
					url : "ajax/addTool",
					method : "POST",
					data : {
						tools : text
					},
				});

				request
						.done(function(msg) {
//							if (msg != "") {
							$.each(msg,function(i,obj){
								$('.list_tools')
										.append(
												"<a class='blue_link' href='javascript:void(0)'>"
														+ obj
														+ " <i class='fa fa-times'> </>");
							});
								$(".errTool").text("").hide();
//							} else {
//								$(".errTool")
//										.text(
//												"This Tool is already preasent in your prefer list")
//										.show();
//							}
							$('#tool_input').val("");
						});

				request.fail(function(msg) {
//					window.alert("bad request");
				});
			} else {
				$(".errTool").text("").hide();
			}
		}

		},

		addCountry : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
			if (text.length) {
				var request = $.ajax({
					url : "ajax/addCountry",
					method : "POST",
					data : {
						countries : text
					},
				});

				request
						.done(function(msg) {
//							if (msg != "") {
							$.each(msg,function(i,obj){
								$('.list_countries')
										.append(
												"<a class='blue_link' href='javascript:void(0)'>"
														+ obj
														+ " <i class='fa fa-times'> </>");
							});
								$(".errEligibility").text("").hide();
//							} else {
//								$(".errEligibility")
//										.text(
//												"This Country is already preasent in your prefer list")
//										.show();
//							}
							$('#Eligblity_input').val("");
						});

				request.fail(function(msg) {
//					window.alert("bad request");
				});
			} else {
				$(".errEligibility").text("").hide();
			}
			}

		},

		removeRoles : function() {

			$(this).toggleClass("blue_link");
			var text = $(this).closest('a').text().trim();

			var request = $.ajax({
				url : "ajax/removeRole",
				method : "POST",
				data : {
					roleName : text
				},
			});
			$(this).closest('a').remove();

			request.fail(function(msg) {
//				window.alert("bad request");
			});

		},

		removeTools : function(e) {

			$(this).toggleClass("blue_link");
			var text = $(this).closest('a').text().trim();

			var request = $.ajax({
				url : "ajax/removeTool",
				method : "POST",
				data : {
					toolName : text
				},
			});
			$(this).closest('a').remove();

			request.fail(function(msg) {
//				window.alert("bad request");
			});

		},

		removeCountry : function() {

			$(this).toggleClass("blue_link");
			var text = $(this).text().trim();
			var request = $.ajax({
				url : "ajax/removeCountry",
				method : "POST",
				data : {
					countryName : text
				},
			});
			$(this).remove();

			request.fail(function(msg) {
//				window.alert("bad request");
			});

		},

		getDATA : function(id, ajaxUrl) {

			var listOfData = [];

			$.ajax({
				url : ajaxUrl,
				type : "GET",
				success : function(data) {

					listOfData = data;
					$(id).autocomplete({
						source : listOfData
					});

				},
				error : function(err) {
//					alert("err: " + err);
				}
			});

		},
		addFutureCompany : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
				if (text.length) {
				$.ajax({
							url : "ajax/addEmployer",
							type : 'POST',
							data : {
								futureCompanies : text
							},
							success : function(response) {
//								var status = response;
//								if (status == 1) {
								$.each(response,function(i,obj){
									$('.future_comp')
											.append(
													"<a class='blue_link' href='javascript:void(0)'>"
															+ obj
															+ "<i class='fa fa-times'></i></a>");
								});
									$('#input_FC').val('');
									$(".errFuture_comp").text("").hide();
//								} else if (status == 0) {
//									$(".errFuture_comp")
//											.text(
//													"This Company you have excluded already")
//											.show();
//								} else {
//									$(".errFuture_comp")
//											.text(
//													"This Company you already have in preferred list")
//											.show();
//								}
							},
							error : function(error) {
//								alert("error occur during saving company");
							}
						});
				}
			}

		},
		
		addExculudeEmployer : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
				if (text.length) {
				$.ajax({
							url : "ajax/addExculudeEmployer",
							type : 'POST',
							data : {
								excludeCompanies : text
							},
							success : function(response) {
//								var status = response;
//								if (status == 1) {
								$.each(response,function(i,obj){
									$('.exclude_comp')
											.append(
													"<a class='blue_link' href='javascript:void(0)'>"
															+ obj
															+ "<i class='fa fa-times'></i></a>");
								});
									$('#input_ExC').val('');
									$(".errExcld_comp").text("").hide();
//								} else if (status == 0) {
//									$(".errExcld_comp")
//											.text(
//													"This Company you have in preferred list")
//											.show();
//								} else {
//									$(".errExcld_comp")
//											.text(
//													"This Company you already have in Excluded list")
//											.show();
//								}
							},
							error : function(error) {
//								alert("error occur during saving company");
							}
						});
				}
			}

		},
		
		removeFutureEmployer : function() {

			var cls = $(this).parent().attr("class");
			
			
			var text = $(this).text().trim();
			var request = $.ajax({
				url : "ajax/removeFutureEmployer",
				method : "POST",
				data : {
					companyName : text
				},
			});
			$(this).remove();

			request.fail(function(msg) {
//				window.alert("error occur during removing preferred company");
			});

		},
		checkValidation: function(){
			$('span.errorMessage').hide();
			$("#errorMsg" ).hide();
			var type=$('#Permanent_select').val();
			var compensation= $('#compensation').val();
			var bonus= $('#bonus').val();
			var dayRate= $('#dayRate').val();
			var pstmt = $('#personalTextStatement').find(".note-editable").text();
			
			var cletter = $('#coveringLetterText').find(".note-editable").text();
			
			var validation=true;
		if (type == "Donotmind") {
			if(compensation.length == 0)
				{
				validation=false;
				$("#compensation_error" ).text( "Please enter the Compensation" ).show();
				}
			else if(!$.isNumeric(compensation))
			{
			validation=false;
			$("#compensation_error" ).text( "Please enter the Valid Compensation" ).show();
			}
			 if(bonus.length == 0)
			{
			validation=false;
			$("#bonus_error" ).text( "Please enter the Bonus" ).show();
			}
			 else if(!$.isNumeric(bonus))
				{
				validation=false;
				$("#bonus_error" ).text( "Please enter the Valid bonus" ).show();
				}
			if(dayRate.length == 0)
			{
			validation=false;
			$("#dayRate_error" ).text( "Please enter the DayRate" ).show();
			}
			else if(!$.isNumeric(dayRate))
			{
			validation=false;
			$("#dayRate_error" ).text( "Please enter the Valid DayRate" ).show();
			}
		}
		else if(type == "Contract")
			{
			  if(dayRate.length == 0)
		  	  {
			   validation=false;
			   $("#dayRate_error" ).text( "Please enter the DayRate" ).show();
			  }
			  else if(!$.isNumeric(dayRate))
				{
				validation=false;
				$("#dayRate_error" ).text( "Please enter the Valid DayRate " ).show();
				}
			}
		else if (type == "Permanent")
			{
		     	if(compensation.length == 0)
		    	{
		    	validation=false;
		    	$("#compensation_error" ).text( "Please enter the Compensation" ).show();
		     	}
		     	else if(!$.isNumeric(compensation))
				{
				validation=false;
				$("#compensation_error" ).text( "Please enter the Valid Compensation" ).show();
				}
		        if(bonus.length == 0)
		       {
		      validation=false;
		      $("#bonus_error" ).text( "Please enter the Bonus" ).show();
		        }
		        else if(!$.isNumeric(bonus))
				{
				validation=false;
				$("#bonus_error" ).text( "Please enter the Valid bonus" ).show();
				}
	 		}
		if(!validation)
			{
			$("#errorMsg" ).text( "Please select the Next Role to be Permanent or Contract." ).show();
			var url=window.location.href;
            var index=url.indexOf('#');

			if(index == -1)
				{
			window.open(url+"#","_self");
				}
			else
				{
				window.open(url,"_self");
				}
			}
		if(pstmt.length > 45 ) {
			$("#pstmt_error" ).text( "Personal statement is too long and contains more than 45 characters" ).show();
			validation=false;
		}
		if(cletter.length > 100 ) {
			$("#cletter_error" ).text( "Coverng Letter is too long and contains more than 100 characters" ).show();
			validation=false;
		}
		if(validation)
			{
			 var form=$('.myFuturePage form');
			 $(form).submit();
			}
		
		},
		
		statusSkill:function(){
			var cls=$(this).hasClass('blue_link');
			var futurePreference;
			if(cls)
				{
				futurePreference=0;
				$(this).removeClass('blue_link');
				}
			else
			{
			futurePreference=1;
			$(this).addClass('blue_link');
			}
			var text = $(this).text().trim();
			var request = $.ajax({
				url : "ajax/changeFuturePreference",
				method : "POST",
				data : {
					name : text,futurePreference:futurePreference,type:"skill"
				}
			});
		},
		statusTool:function(){
			var cls=$(this).hasClass('blue_link');
			var futurePreference;
			if(cls)
				{
				futurePreference=0;
				$(this).removeClass('blue_link');
				}
			else
			{
			futurePreference=1;
			$(this).addClass('blue_link');
			}
			var text = $(this).text().trim();
			var request = $.ajax({
				url : "ajax/changeFuturePreference",
				method : "POST",
				data : {
					name : text,futurePreference:futurePreference,type:"tool"
				}
			});
		},
		statusRole:function(){
			var cls=$(this).hasClass('blue_link');
			var futurePreference;
			if(cls)
				{
				futurePreference=0;
				$(this).removeClass('blue_link');
				}
			else
			{
			futurePreference=1;
			$(this).addClass('blue_link');
			}
			var text = $(this).text().trim();
			var request = $.ajax({
				url : "ajax/changeFuturePreference",
				method : "POST",
				data : {
					name : text,futurePreference:futurePreference,type:"role"
				}
			});
		}
		
	};
}();

$(document).ready(function() {
	
	 $.ajax({
	        url : "ajax/getEmployerNames",
	        type : "GET",
	        success : function(data) {
	           var  listOfEmployers = data;
	            $('#input_FC').autocomplete({
	                source : listOfEmployers
	            });
	            $('#input_ExC').autocomplete({
	                source : listOfEmployers
	            });
	        },
	        error : function(err) {
	        }
	    });


	myFuturePage.init();
	
	$( '.pp_textarea' ).summernote({
	    height: 158
	  });
	$('.note-toolbar').css('display','none');
	$('.note-editor .note-statusbar').css('display','none');
	
	 $("canvas").click(function(){
		    var value_data = $("#current_value").val()
		    if (value_data >= 0 && value_data < 25) {
		    $(".rotate_value").text("Not available")
		    }
		    else if (value_data >= 25 && value_data < 50)
		    {
		    $(".rotate_value").text("Available from")
		    }
		    else if (value_data >= 50 && value_data < 75){
		    $(".rotate_value").text("Interested")  
		    }
		    else if (value_data >= 75 && value_data <= 100){
		    $(".rotate_value").text("Available Immediately")   
		    }
		  });
	 

});