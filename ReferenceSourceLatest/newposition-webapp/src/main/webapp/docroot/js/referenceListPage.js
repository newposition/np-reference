var referenceList = function() {

	return {
		init : function() {
			// alert("alert");
			// on page loading code should present here
			$("a#verifyDetailsBtn").on("click", referenceList.submitReferedId);
			$("select.filByName").on(
					'change',
					function(event) {
						var sortBy = $(this).val();
						if(sortBy != "" || sortBy != null) {
						window.open('referenceList?sortBy='+sortBy,"_self");
						}
					});
		},

		submitReferedId : function(event) {
			var form = $(this).parent();
			form.attr("action", "referenceList");
			form.submit();

		},

	};

}();

$(document).ready(function() {

	referenceList.init();

});