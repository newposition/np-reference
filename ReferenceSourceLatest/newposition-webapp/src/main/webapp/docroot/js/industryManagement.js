$(document).ready(
		function() {
			
			$("#sectorFilter").on('change',
					function(event) {
						var sectorFilter = $(this).val();
						var sortBy=$('#sortNpIndustries').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						if (domainFilter.length != 0) {
							window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter,"_self");
						}
						});
			
			$("#industryTypeFilter").on(
					'change',
					function(event) {
						var industryTypeFilter = $(this).val();
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var	validFilter=$('#validFilter').val();
						if (industryTypeFilter.length != 0) {
							window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter,"_self");
						}
					});
			
			$("#validFilter").on(
					'change',
					function(event) {
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$(this).val();
						if (validFilter.length != 0) {
							window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter,"_self");
						}
						});
			
			$("#sortNpIndustries").on(
					'change',
					function(event) {
						var sortBy = $(this).val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						if(sortBy!='Sort by')
							{
							window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter,"_self");
							}
					});
			
			

			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
						window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
						window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
						window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
						window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpIndustries').val();
						var sectorFilter=$('#sectorFilter').val();
						var industryTypeFilter=$('#industryTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						
						window.open('industryManagement?sortBy='+sortBy+"&sectorFilter="+sectorFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
//			$('.fa-search').on('click',	function(event) {
//				alert('hi');
//				event.preventDefault();
//				var searchWord=	$('#qSearch').val();
//
//				alert(searchWord);
////					if (event.keyCode == 13) {
//						$.ajax({
//							type : "POST",
//							url : "ajax/skillManagement/aliasSearch?searchCode="+searchWord,
//							success : function(data) {
//								alert('success');
//								$('#searchResults').html(data.html);
//							},
//							error : function() {
//								alert('error');
//							}
//						});
////					}
//				});
			
            function getSearchData(searchCode,targetId,currentModal) {
				$.ajax({
                    type : "POST",
                    url : "ajax/industryManagement/aliasSearch?searchCode="+searchCode+"&mergeToId="+targetId,
                    success : function(response) {
                    	 if(response!= undefined)
                  	   {
                        $('.searchResults1').html(response);
                        $(".industryDelete").css("bottom", "55.9%");
                  	   }
                    },
                    complete : function(){
                        //code to update the height of modal backdrop -start
                         var $modal        = currentModal.parents(".modal");
                          var $backdrop     = $('.modal-backdrop');
                          var $modalContent = $modal.find('.modal-dialog');
                          var $modalHeight  = $modalContent.height();
                               $backdrop.css({
                                  height: $modalHeight * 1.1
                                  })
                        //code to update the height of modal backdrop - end       
                   },
                    error : function() {
                    }
                });
			}
			$('.quickEntryFilter input').keyup(function(event){
	            event.preventDefault();
	                if (event.keyCode == 13) {
	                 var searchCode=$(this).val();
	                 var targetModal=$(this).parents(".modal").attr("id");
	                 var targetId=$("#"+targetModal+" #addNpIndustriesForm #industryIds").val();
	                 if(searchCode.length)
	   					{
                         getSearchData(searchCode,targetId,$(this));
	   					}   
	                }
	            });
			
			$('.quickEntryFilter label').on("click",function(event){
	             event.preventDefault();
	             var searchCode=$(this).parent().find("input#qSearch").val();
                var targetModal=$(this).parents(".modal").attr("id");
                var targetId=$("#"+targetModal+" #addNpIndustriesForm #industryIds").val();
                if(searchCode.length)
                	{
            	 	getSearchData(searchCode,targetId,$(this));
                	}    
            });
			
			$("button#industryFormDeleteBtn").on("click",function(event){
				var isSure = confirm("Are you sure you want to delete the Industry !!!");
				if (isSure) {
					var targetModal=$(this).parents(".modal").attr("id");
				$("#"+targetModal+" #addNpIndustriesFormDelete").submit();
				}
				else
					{
					 event.preventDefault();
					}
			});
			
			
			 $("form#addNpIndustriesForm").on("submit",function(event){
                 var industryName = $(this).find("input[name=industryName]").val();
                 var sectorId = $(this).find("select[name=sectorId]").val();
                 $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
                 var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
                 for(var i=0;i<rows;i++)
              	   {
              	   var aliasSource=$(this).find(".aliasSource"+i).val();
              	  var aliasName=$(this).find(".aliasName"+i).val();
              	   if(aliasName.length > 0 && aliasSource == "")
              	   {
              	  $("span#errPop").text("Please choose Alias Source").show();
              	 event.preventDefault();
              	   }
              	   else if(aliasName.length == 0 && aliasSource > 0)
             	   {
             	  $("span#errPop").text("Please enter Alias Name").show();
             	 event.preventDefault();
             	   }
              	   }
                 if (industryName.length == 0) {
                     $("span#errPop").text("Please enter Industry Name").show();
                     event.preventDefault();
                 } 
                 else if(sectorId.length== 0) {
                     $("span#errPop").text("Please choose Parent Industry").show();
                     event.preventDefault();
                 }
                 
             });
			 
			 $(".edit-icon").on("click",function(event){
                 var modal=$(this).attr("data-target");
                 var id=$(this).attr("id");
                 var form=$(modal+" input[name=industryId]").val();
                 if(form != '' && form != undefined)
                   {
                       $.ajax({
                           type : "POST",
                           url : "ajax/industryManagement/editAlias?id="+form,
                           success : function(response) {
                               if(response != undefined && response!= "")
                                   {
                               $(modal+" .coAliasSection").html(response);
                                   }
                           },
                           error : function() {
                           }
                           
                       });
                   }
                 $(modal +" select[name='sectorId']").val($("#sector"+id).val());
                 $(modal +" input[name='industryName']").val($("#name"+id).val());
                var valid=$("#valid"+id).val();
                 if(valid.trim() == "Valid")
                     {
                 $(modal + " #radio1").prop('checked', true);
                 $(modal + " #radio2").prop('checked', false);
                     }
                 if(valid.trim() == "Invalid")
                     {
                     $(modal + " #radio2").prop('checked', true);
                     $(modal + " #radio1").prop('checked', false);
                     }
               });

             $(".btn-rd-cor").on("click",function(event){
//                 $("#myModalAdd .coAliasSection").html("");
             $("#myModalAdd input[name='industryName']").val("");
             $("#myModalAdd select[name='sectorId']").val($("#myModalAdd select[name='sectorId'] option[val='']").val());
             $("#myModalAdd #radio1").prop('checked', true);
             $("#myModalAdd #radio2").prop('checked', false);
             
             $.ajax({
					type : "POST",
					url : "ajax/roleManagement/addAalias?index="+0,
					success : function(data) {
						
						$("#myModalAdd .coAliasSection").html($(data).html());
					},
					error : function() {
					}
					
				});
                });
		});