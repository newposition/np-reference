$(document).ready(function() {
	$("#instituteFilter").on(
			'change',
			function(event) {
				var instituteFilter = $(this).val();
				var sortBy=$('#sortNpCertifications').val();
				var certificationTypeFilter=$('#certificationTypeFilter').val();
				var	validFilter=$('#validFilter').val();
//					window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter,"_self");
				if (instituteFilter.length != 0) {
				window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter,"_self");
				}
			});
	
	$("#certificationTypeFilter").on(
			'change',
			function(event) {
				var certificationTypeFilter = $(this).val();
				var sortBy=$('#sortNpCertifications').val();
				var instituteFilter=$('#instituteFilter').val();
				var	validFilter=$('#validFilter').val();
				if (certificationTypeFilter.length != 0) {
					window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter,"_self");
				}
			});
	
	$("#validFilter").on(
			'change',
			function(event) {
				var sortBy=$('#sortNpCertifications').val();
				var instituteFilter=$('#instituteFilter').val();
				var certificationTypeFilter=$('#certificationTypeFilter').val();
				var	validFilter=$(this).val();
				if (validFilter.length != 0) {
					window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter,"_self");
				}
			});
	
	$("#sortNpCertifications").on(
			'change',
			function(event) {
				var sortBy = $(this).val();
				var instituteFilter=$('#instituteFilter').val();
				var certificationTypeFilter=$('#certificationTypeFilter').val();
				var	validFilter=$('#validFilter').val();
				if(sortBy!='Sort by')
					{
					window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter,"_self");
					}
			});
	
    function getSearchData(searchCode,targetId,currentModal) {
		$.ajax({
            type : "POST",
            url : "ajax/certificateManagement/aliasSearch?searchCode="+searchCode+"&mergeId="+targetId,
            success : function(response) {
               if(response!= undefined)
            	   {
                $("div#searchResults").html(response);
            	   }
            },
            complete : function(){
                //code to update the height of modal backdrop -start
                 var $modal        = currentModal.parents(".modal");
                  var $backdrop     = $('.modal-backdrop');
                  var $modalContent = $modal.find('.modal-dialog');
                  var $modalHeight  = $modalContent.height();
                       $backdrop.css({
                          height: $modalHeight * 1.1
                          })
                //code to update the height of modal backdrop - end       
           },
            error : function() {
            }
        });
		
	}
		 $('.quickEntryFilter input').keyup(function(event){
           
                if (event.keyCode == 13) {
                	var searchCode=$(this).val();
                	var targetModal=$(this).parents(".modal").attr("id");
//                	var search=$("#"+targetModal+" #searchResults");
                	var targetId=$("#"+targetModal+" #addNpCertificationsForm #npInstituteCertificateId").val();
                	if(searchCode.length)
                		{
                        getSearchData(searchCode,targetId,$(this));
                		}
                }
                event.preventDefault();
            });
		 
		 $('.quickEntryFilter label').on("click",function(event){
             
             var searchCode=$(this).parent().find("input#qSearch").val();
            var targetModal=$(this).parents(".modal").attr("id");
            var targetId=$("#"+targetModal+" #addNpCertificationsForm #npInstituteCertificateId").val();
            if(searchCode.length)
    		{
            getSearchData(searchCode,targetId,$(this));   
    		}
            event.preventDefault();
        });
		 
		 
		 $(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpCertifications').val();
						var instituteFilter=$('#instituteFilter').val();
						var certificationTypeFilter=$('#certificationTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpCertifications').val();
						var instituteFilter=$('#instituteFilter').val();
						var certificationTypeFilter=$('#certificationTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpCertifications').val();
						var instituteFilter=$('#instituteFilter').val();
						var certificationTypeFilter=$('#certificationTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpCertifications').val();
						var instituteFilter=$('#instituteFilter').val();
						var certificationTypeFilter=$('#certificationTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpCertifications').val();
						var instituteFilter=$('#instituteFilter').val();
						var certificationTypeFilter=$('#certificationTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('certificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&certificationTypeFilter="+certificationTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
		 
		 $('.btn-edt').on('click', '#editCoCertificate', function(event){
//			  alert($(this).parents('.form-group').attr("class"));
			  var parents= $(this).parents('.form-group'); // find the parent row
//			  alert(parents.find("input.pp_text:not(:disabled)").length)
			  $(this).parents('.form-group').children("input").removeAttr('disabled');// find text elements in that row
			           
			      parents.find(".aliasSource") // find select elements in that row
		       .attr('disabled',false);
			   event.preventDefault();

		});
       
       
       $(".removeId").on(
   			'click',
   			function(event) {
   			 var isSure = confirm("Are you sure you want to delete the Certificate !!!");
				if (isSure) {
   				var targetModal=$(this).parents(".modal").attr("id");
   		   		var targetId=$("#"+targetModal+" #addNpCertificationsForm #npInstituteCertificateId").val();
   				$.ajax({
   		         type : "POST",
   		         url : "ajax/certificateManagement/removeNpInstituteCertificate?npInstituteCertificateId="+targetId,
   		         success : function(response) {
   		            
   		        	 window.location='certificationManagement?showDelMsg=true';
   		         },
   		         error : function() {
//   		             alert('error');
   		         }
   		     });
				}
				else
					{
					event.preventDefault();
					}
   				
   			});
       
       $("form#addNpCertificationsForm").on("submit",function(event){
           var certificationName = $(this).find("input[name=certificationName]").val();
           var certificationType = $(this).find("select[name=certificationType]").val();
           var institutionId = $(this).find("select[name=institutionId]").val();
           $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
//           event.preventDefault();
           var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
           for(var i=0;i<rows;i++)
        	   {
        	   var aliasSource=$(this).find(".aliasSource"+i).val();
        	  var aliasName=$(this).find(".aliasName"+i).val();
        	   if(aliasName.length > 0 && aliasSource == "")
        	   {
        	  $("span#errPop").text("Please choose Alias Source").show();
        	 event.preventDefault();
        	   }
        	   else if(aliasName.length == 0 && aliasSource > 0)
       	   {
       	  $("span#errPop").text("Please enter Alias Name").show();
       	 event.preventDefault();
       	   }
        	   }
           if (certificationName.length == 0) {
               $("span#errPop").text("Please enter Certification Name").show();
               event.preventDefault();
           } else if(certificationType.length == 0) {
               $("span#errPop").text("Please enter Certificate type").show();
               event.preventDefault();
           }
           else if(institutionId.length== 0) {
               $("span#errPop").text("Please choose Institute").show();
               event.preventDefault();
           }
           
       });
       
       
       $(".edit-icon").on("click",function(event){
           var modal=$(this).attr("data-target");
           var id=$(this).attr("id");
           var form=$(modal+" input[name=npInstituteCertificateId]").val();
           if(form != '' && form != undefined)
             {
                 $.ajax({
                     type : "POST",
                     url : "ajax/certificateManagement/editAlias?id="+form,
                     success : function(response) {
                         if(response != undefined && response!= "")
                             {
                         $(modal+" .coAliasSection").html(response);
                             }
                     },
                     error : function() {
                     }
                     
                 });
             }
           $(modal +" select[name='certificationType']").val($("#certificateType"+id).val());
           $(modal +" select[name='institutionId']").val($("#instituteName"+id).val());
           $(modal +" input[name='certificationName']").val($("#certificateName"+id).val());
          var valid=$("#valid"+id).val();
           if(valid.trim() == "true")
               {
           $(modal + " #radio1").prop('checked', true);
           $(modal + " #radio2").prop('checked', false);
               }
           if(valid.trim() == "false")
               {
               $(modal + " #radio2").prop('checked', true);
               $(modal + " #radio1").prop('checked', false);
               }
         });
       
       $(".btn-rd-cor").on("click",function(event){
       $("#myModalAdd input[name='certificationName']").val("");
       $("#myModalAdd select[name='certificationType']").val($("#myModalAdd select[name='certificationType'] option[val='']").val());
       $("#myModalAdd select[name='institutionId']").val($("#myModalAdd select[name='institutionId'] option[val='']").val());
       $("#myModalAdd #radio1").prop('checked', true);
       $("#myModalAdd #radio2").prop('checked', false);
       
       $.ajax({
			type : "POST",
			url : "ajax/roleManagement/addAalias?index="+0,
			success : function(data) {
				
				$("#myModalAdd .coAliasSection").html($(data).html());
			},
			error : function() {
			}
			
		});
          });
});


 