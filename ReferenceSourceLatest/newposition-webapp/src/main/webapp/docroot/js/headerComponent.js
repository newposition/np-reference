$(document).ready(function() {
	$(document).delegate(".search_icon",'click',
		    function(event) {
		  $('.search_bar').toggleClass("collapsed pressed"); //you can list several class names
		   e.preventDefault();
	});
	

	$(document).delegate("body",'click',
		    function(event) {
			if($(event.target).closest('.login_icon').length) {
				$('.login_drop').toggleClass("collapsed pressed");
		    }
			else
			{
				if($('.login_drop').hasClass("collapsed pressed") && !$(event.target).closest('.login_drop').length)
				{
			$('.login_drop').removeClass("collapsed pressed"); // you can list
				}
			}
			
		});

	$(document).delegate(".online_icon",'click',
		    function(event) {
			if (is_visible == false) {
				is_visible = true;
			} else if (is_visible == true) {
				is_visible = false;
			}
			$('.online_drop').toggleClass("collapsed pressed");
			e.preventDefault();
		});
		
		deleteRow();
		
		$(document).delegate(".addAlias",'click',
			    function(event) {
					event.preventDefault();
					var index=$(this).parent(".pull-right").parent(".coAliasDiv").find(".form-group").size();
					var div=$(this).parent(".pull-right").parent(".coAliasDiv").find(".coAliasSection");
					var type=$("#type").val();
					$.ajax({
						type : "POST",
						url : "ajax/roleManagement/addAalias?index="+index+"&type="+type,
						success : function(data) {
							
							div.append($(data).html());
							deleteRow();
						},
						error : function() {
						}
						
					});
				});
		
		$(document).delegate(".editAlias",'click',
			    function(event) {
		
					event.preventDefault();
					var index=$(this).parent(".pull-right").parent(".coAliasDiv").find(".form-group").size();
					var div=$(this).parent(".pull-right").parent(".coAliasDiv").find(".coAliasSection");
					var type=$("#type").val();
					$.ajax({
						type : "POST",
						url : "ajax/roleManagement/addAalias?index="+index+"&type="+type+"&addOrEdit=edit",
						success : function(data) {
							
							div.append($(data).html());
							deleteRow();
						},
						error : function() {
						}
						
					});
				});
		
		
		
		function deleteRow()
		{
			$(document).undelegate(".deleteAliasRow" , "click");
			$(document).delegate(".deleteAliasRow",'click',
				    function(event) {
					event.preventDefault();
						var index=$(this).attr("id");
						
						var aliasId=$(this).closest('.row').find('.coAliasId').val();
							var isSure = confirm("Are you sure you want to delete the Alias!!!");
						       if (isSure) {
						var url=$('#del-url').val();
						if(aliasId != '' && typeof(aliasId) != 'undefined')
						{
							$.ajax({
								type : "POST",
								url : "ajax"+url+"?id="+aliasId,
								success : function() {
								},
								error : function() {
								}
								
							});
						}	
					
						/*var form=$(this).closest('form').find(":nth-child(1)").val();
						var coAliasDiv=$(this).parents().find('.coAliasDiv');
//						var id=$(form).next("input").val();
						var editUrl=$('#edit-url').val();
						var targetModal=$(this).parents(".modal").attr("id");
						if(form != '' && form != undefined)
						{
							$.ajax({
								type : "POST",
								url : "ajax"+editUrl+"?id="+form,
								success : function(response) {
									$("#"+targetModal+" .coAliasSection").html(response);
								},
								error : function() {
								}
								
							});
						}	*/
						var aliasCounter=$(this).closest('.coAliasDiv').find(".form-group").size();
						var aliasesDiv=$(this).closest('.coAliasDiv');
						var aliasFormRow=$(this).closest('.form-group');

						var count=0;
						 var rowCount=parseInt(index);
						 for(var i=0;i<aliasCounter;i++){
						  count=count+1;
						  if(rowCount==count || rowCount==0){
							  aliasFormRow.remove();
						  }
						  if(count>rowCount && count<=aliasCounter){
							  
							  var aliasid="certificationAliases\["+parseInt(count-1)+"\].aliasId";
							  var aliasName="certificationAliases\["+parseInt(count-1)+"\].aliasName";
							  var aliasSource="certificationAliases\["+parseInt(count-1)+"\]\.aliasSource";
							
							  aliasesDiv.find(".aliasId"+count).attr("name",aliasid);
							  aliasesDiv.find(".aliasName"+count).attr("name",aliasName);
							  aliasesDiv.find(".aliasSource"+count).attr("name",aliasSource);
						
						  }
						  
						 }
						 
						 
						var aliasCounters=aliasesDiv.find(".form-group").size();
						 
						 for(var i=rowCount;i<aliasCounters;i++){
							 
							 var aliasid="certificationAliases\["+parseInt(i)+"\].aliasId";
							 var aliasName="certificationAliases\["+parseInt(i)+"\].aliasName";
							 var aliasSource="certificationAliases\["+parseInt(i)+"]\.aliasSource";
							 var eachRowId =aliasesDiv.find('input[name="'+aliasid+'"]');
							 var eachRowName =aliasesDiv.find('input[name="'+aliasName+'"]');
							 var eachRowSource =aliasesDiv.find('select[name="'+aliasSource+'"]');
							 

							 for(var l=rowCount;l<aliasCounters+1;l++)
							 {
								 eachRowId.removeClass('aliasId'+l);
								 eachRowName.removeClass('aliasName'+l);
								 eachRowSource.removeClass('aliasSource'+l);
							 }
							
							  var deleteButtonDiv= eachRowId.closest('.row').find(".deleteAliasRow");
								 deleteButtonDiv.attr('id',i);
								 
							 eachRowId.addClass('aliasId'+i);
							 eachRowName.addClass('aliasName'+i);
							 eachRowSource.addClass('aliasSource'+i);
							 
							 }
						 
							}
						else
							{
							event.preventDefault();
							}
					});
		}
		
		$(document).delegate(".editAliasRow",'click',
			    function(event) {
		
					event.preventDefault();
					var rowDiv=$(this).closest(".row");
					rowDiv.find(".pp_text").removeAttr('readonly');
					rowDiv.find(".selectbox").removeAttr('disabled');

				});
        $(".modal").on('hidden.bs.modal', function () {
            $("div#searchResults").html("");
            $("div.searchResults1").html("");
            $("span#errPop").text("");
            $(".quickEntryFilter #qSearch").val("");
            
        });

});
