$(document).ready(function() {
		 				var listOfData = [];

		 				$.ajax({
		 					url : "ajax/getCountriesForAddClient",
		 					type : "GET",
		 					success : function(data) {

		 						listOfData = data;
		 						$("#country").autocomplete({
		 							source : listOfData
		 						});

		 					},
		 					error : function(err) {
		 					}
		 				});

			


			$("#addClient").validate({ 
				rules: {
					companyLogo: {
						accept:"image/jpeg,image/jpg,image/png,image/gif,jpg|png|gif"
					    },
					companyName: {
						required: true	,
						minlength:2,
						maxlength:50
					},
					sectorId: {
						required: true,
						min:1
					},
					firstName: {
						required: true	,
						minlength:2,
						maxlength:20
					},
					lastName: {
						required: true	,
						minlength:2,
						maxlength:20
					},
					principleContactEmail:{
						 required: true,
						 email: true,
						 pattern: /[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\.[a-zA-Z]{2,}/
					},
					teleNumber: {
						required: true,
						number: true,
						minlength:10,
						maxlength:16,				
					},
					mobileNumber: {
						required: true,
						number: true,
						minlength:10,
						maxlength:16
					},
					address1: {
						required: true,
						maxlength:20
					},
					address2: {
						maxlength:20
					},
					town: {
						required: true
					},
					country: {
						required: true
					},
					adminFirstName: {
						required: true,
						minlength:2,
						maxlength:20
					},
					adminLastName: {
						required: true,
						minlength:2,
						maxlength:20
					},
					adminEmail: {
						required: true,
						email: true,
						pattern: /[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\.[a-zA-Z]{2,}/
					},
					adminPassword: {
						required: true,
						minlength:8,
						maxlength:20,
						pattern: /^(((?=.*[a-z])(?=.*[A-Z])(?=.*[\d]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\W]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W]))).{8,}$/
					},
					adminConfirmPassword: {
						equalTo:"#adminPassword"
					},
					clientStatusId: {
//						min: 1
					}
					
				},
				
				 messages: {
					 companyLogo:{
						 accept:"provide .jpg or.png or .gif"
					 },
					 companyName: {
						required: "Company Name is mandatory"	,
						minlength:"Company Name should be a minimum of length 2",
						maxlength:"Company Name exceeds the max length of 50"
					},
					sectorId: {
						min: "Please select a valid sector"
					},
					firstName: {
						required: "First Name is mandatory"	,
						minlength:"First Name should be a minimum of length 2",
						maxlength:"First Name exceeds the max length of 20"
					},
					lastName: {
						required: "Last Name is mandatory"	,
						minlength:"Last Name should be a minimum of length 2",
						maxlength:"Last Name exceeds the max length of 20"
					},
					principleContactEmail:{
						 required: "Principle Contact Email field is mandatory",
						 email: "Please enter a valid email address"
					},
					teleNumber: {
						required: "Telephone Number field is mandatory",
						number: "Enter a valid telephone number",
						minlength:"Telephone Number should be greater than 10 and less than 16 numbers",
						maxlength:"Telephone Number should be greater than 10 and less than 16 numbers",				
					},
					mobileNumber: {
						required: "Mobile Number field is mandatory",
						number: "Enter a valid Mobile number",
						minlength:"Mobile Number should be greater than 10 and less than 16 numbers",
						maxlength:"Mobile Number should be greater than 10 and less than 16 numbers",
					},
					address1: {
						required: "Address1 field is mandatory",
						maxlength: "Address1 exceeds the max length of 20"
					},
					address2: {
						maxlength: "Address2 exceeds the max length of 20"
					},
					town: {
						required: "Town field is mandatory"
					},
					country: {
						required: "Country is mandatory"
					},
					adminFirstName: {
						required: "First Name is mandatory"	,
						minlength:"First Name should be a minimum of length 2",
						maxlength:"First Name exceeds the max length of 20"
					},
					adminLastName: {
						required: "Last Name is mandatory",
						minlength:"Last Name should be a minimum of length 2",
						maxlength:"Last Name exceeds the max length of 20"
					},
					adminEmail: {
						required: "Email Address is mandatory",
						email: "Please enter a valid email address"
					},
					adminPassword: {
						required: "Password is mandatory",
						minlength:"Passwords must be a minimum of 8 characters including one Uppercase and one lowercase letter, and any number or symbol",
						maxlength:"Password exceeds the max length of 20"
					},
					adminConfirmPassword: {
						equalTo:"Confirm password should be same as password"
					},
					clientStatusId: {
						min: "Please select a valid status"
					}
				},
				
				errorClass: "errorMessage",
				/*errorPlacement: function (error, element) {
		            error.insertBefore(element);
		        },*/
					 showErrors: function (errorMap, errorList) {
				            var msg = "Oops! not all of the fields have been filled in correctly. correct the ones highlighted to continue."
				            /*$.each(errorMap, function(key, value) {
				                msg += key + ": " + value + "<br/>";
				            });*/
				            $("#errormessages").html("<label class='errorMessage'>"+msg+'</label>');
				            
				            this.defaultShowErrors();  // default labels from errorPlacement
				            if (this.numberOfInvalids() > 0) {
				                $("#errormessages").show();
				            } else {
				                $("#errormessages").hide();
				            }
				        }
			});
			
			
			//for displaying Image before upload
			/*$("#companyLogo").change ( function(event) {
			    if (this.files && this.files[0]) {
			     var filerdr = new FileReader();
			     filerdr.onload = function(event) {
			    	 $('.add_logo').addClass('em_box_logo');
			     $('.add_logo').prepend("<img src='"+event.target.result+"'/>");
//			    	 $('.add_logo').css('background-image', 'url(' + event.target.result + ')');
			     }
			     filerdr.readAsDataURL(this.files[0]);
			     }
			  });*/
			$(".myimg #uploadBtn1").change ( function(event) {
				 if (this.files && this.files[0]) {
					 var filerdr = new FileReader();
					 filerdr.onload = function(event) {
					 $('.myimg .img-responsive').attr('src', event.target.result);
					 }
					 filerdr.readAsDataURL(this.files[0]);
					 }
			});
			
			
			
			
			
});


 