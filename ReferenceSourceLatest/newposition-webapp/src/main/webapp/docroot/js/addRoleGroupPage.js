$(document).ready(
       
		function(event) {
			var listOfData = [];
			if (event.keyCode == 13){
				event.preventDefault();
			}
			$("#btn-send").on("click",function(){
			    var val=$("#uniqueGroupName").text();
			    if(val.indexOf("GroupName Already Exists")==-1){
				$("form#npClientAddRoleGroupForm").submit();
			    }
			    else{
			        $('#groupname').focus();
			    }
			});
			
			function returnRoles(roles) {
				listOfData= roles;
			}
				
			$.ajax({
				url : "ajax/getClientRolesAndGroups",
				type : "GET",
				success : function(data) {

					listOfData = data;
					
					$('#currentRole').autocomplete({
						source : listOfData
					});
					returnRoles(listOfData);
				},
				error : function(err) {
				}
			});

			//			Roles set to the current role field on page load
			var existRoles='';
			$(".links_jss a").each(function() {

				existRoles = existRoles+$(this).text()+','; 
			    
			});
			$('#_currentRole').val(existRoles);
			
			$('#currentRole').keyup( function(event) {
				var text = $(this).val().trim();
				
				var thisId = this;
				var msg = text.split(",");
				
				if (event.keyCode == '13') {
					var roles = $('#_currentRole').val();
				if (text.length > 0) {
					var addedRole = $('#_currentRole').val();
					var isCandidate = $("input#roleCan").val();
					if(text.indexOf("ROLE_CLIENT_ADMINISTRATOR")== -1 && text.indexOf("ROLE_CLIENT_INTERVIEWER")== -1 && text.indexOf("ROLE_CLIENT_MANAGER")== -1 
							&& text.indexOf("ROLE_CLIENT_SUPPORT")== -1 
							&& text.indexOf("ROLE_CLIENT_EMPLOYEE")== -1){
						
						$.ajax({
							url : "ajax/getClientRolesInGroups?groupName="+text,
							type : "GET",
							success : function(data) {
							    if(data.length>0){
								$.each(data,function(i,obj){
									if (obj.trim().length != 0 && addedRole.indexOf(obj)== -1) {
									$('.links_jss.links_jsonlyclick.links_bd').append("<a  href='javascript:void(0)'>"
															+ obj.trim()+ " <i class='fa fa-times'> </>");
									roles = roles+obj.trim()+',';
									}
									$(thisId).val("");
									
									addedRole = addedRole+obj+',';
								});
								$('#_currentRole').val(roles);
								$(thisId).val("");
							    }
							    else{
							        alert("Please Enter A valid Role or RoleGroup");
							        $(thisId).val("");
							    }
							
							},
							error : function(err) {
							}
						});
						
					}
					else {
					if (listOfData.indexOf(text) > -1 && isCandidate!='candidate') {
					$.each(msg,function(i,obj){
						if (obj.trim().length != 0 && addedRole.indexOf(obj)== -1) {
						$('.links_jss.links_jsonlyclick.links_bd').append("<a  href='javascript:void(0)'>"
												+ obj.trim()+ " <i class='fa fa-times'> </>");
						roles = roles+obj.trim()+',';
						}
						$(thisId).val("");
						
						addedRole = addedRole+obj+',';
					});
//					alert(roles);
					$('#_currentRole').val(roles);
				}// if entered role is invalid role
					else {
						$(this).val("");
					}
					}
				} else {
					$(".errSkill").text("").hide();
				}
				}
				
				event.preventDefault();
			});
			
			$(".links_jss").on("click",'a' ,function(){
				var deletingRole = $(this).text().trim();
				var roles = $('#_currentRole').val();
				roles = roles.replace(deletingRole+',','');
				$('#_currentRole').val(roles);
//				$('#currentRole').val(roles);
				$(this).remove();
			});
			
			$(".deleteUser").on('click',(function(event){
			    if (confirm("Are you sure?")){
			    var id=$(this).attr('id');
             $.ajax({
                 url: "ajax/deleteGroupById?id="+id, 
                 success: function(result){
                  window.open('clientRoleGroupManagement?delete=Delete',"_self");
                  }
               });
			    }
			}));
			
			
				$("#groupname").blur(function(){
				  $.ajax({url: 'ajax/checkGroupName?name='+$("#groupname").val(), type: 'GET'})
				  .done(function(response){
				      if(response.indexOf("exists")>-1){
					  $("#uniqueGroupName").text("GroupName Already Exists");
				      }
				      else{
				           $("#uniqueGroupName").text("");
				      }
				  })
				});
				
			
			$("#sortNpClientGroups").on(
						'change',
						function(event) {
							event.preventDefault();
							var sortBy = $(this).val();
							
							window.open('clientRoleGroupManagement?sortBy='+sortBy,"_self");
						
						});
			
		/*	$(".editUser").on(
					'click',
					function(event) {
						var id = $(this).parent().parent().parent().find(
								'.tbl-user span').text();
						window.open('edit-user?id='+id,"_self");
					});
			
			$(".deleteUser").on(
					'click',
					function(event) {
						var id = $(this).parent().parent().parent().find(
						'.tbl-user span').text();
				$('input[name="userId"]').val(id);
				 var name=$(this).parent().parent().parent().find(
					'.tbl-name.fullName').text();
				 var email=$(this).parent().parent().parent().find(
					'.emailId').text();

					$('.modal-header span#user-Name').text(name).show();
					});
			
			$(".suspendUser").on(
					'click',
					function(event) {
						var id = $(this).parent().parent().parent().find(
								'.tbl-user span').text();
						$('input[name="userId"]').val(id);
						var name=$(this).parent().parent().parent().find(
						'.tbl-name.fullName').text();
						$('.modal-body select[name="statusId"]').val($(this).attr("id"));
					 var email=$(this).parent().parent().parent().find(
						'.emailId').text();
						$('.modal-header span#user-Name').text(name).show();
					});
			
			$("#sortNpUsers").on(
					'change',
					function(event) {
						var sortBy = $(this).val();
						window.open('clientSupportUserManagement?sortBy='+sortBy,"_self");
					});
			$('#currentRole').keyup( function(event) {
				var text = $(this).val().trim();
				
				var thisId = this;
				var msg = text.split(",");
				if (event.keyCode == '13') {
					var roles = $('#_currentRole').val();
				if (text.length > 0) {
					var addedRole = $('#_currentRole').val();
					var isCandidate = $("input#roleCan").val();
					if (listOfData.indexOf(text) > -1 && isCandidate!='candidate') {
					$.each(msg,function(i,obj){
						if (obj.trim().length != 0 && addedRole.indexOf(obj)== -1) {
						$('.links_jss.links_jsonlyclick.links_bd')
								.append(
										"<a  href='javascript:void(0)'>"
												+ obj.trim()
												+ " <i class='fa fa-times'> </>");
						roles = roles+obj.trim()+',';
						}
						$(thisId).val("");
						
						addedRole = addedRole+obj+',';
					});
//					alert(roles);
					$('#_currentRole').val(roles);
				}// if entered role is invalid role
					else {
						$(this).val("");
					}
					
				} else {
					$(".errSkill").text("").hide();
				}
				}
				
				event.preventDefault();
			});
			$(".links_jss").on("click",'a' ,function(){
				var deletingRole = $(this).text().trim();
				var roles = $('#_currentRole').val();
				roles = roles.replace(deletingRole+',','');
				$('#_currentRole').val(roles);
//				$('#currentRole').val(roles);
				$(this).remove();
			});
			
			
			*//**
			 * 
			 *  pagination
			 *  
			 * *//*
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpUsers').val();
						var domainFilter=$('#domainFilter').val();
						var skillTypeFilter=$('#skillTypeFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('clientSupportUserManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&skillTypeFilter="+skillTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpUsers').val();
						window.open('clientSupportUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpUsers').val();
						window.open('clientSupportUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpUsers').val();
						window.open('clientSupportUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpUsers').val();
						window.open('clientSupportUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
						
					});*/
		});
			
			
			
			