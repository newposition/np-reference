//var str=  "<div class='row no-pad'><div class='bds_panel'><div class='row no-pad'><div class='col-md-10'><div class='dtl_list'><div class='col-md-12 dtl_ct'><h3 class='dtl_h3'>"+obj.projectId+"</h3><p>"+obj.projectName+"<br>"+obj.projectLocation+"<br>"+obj.projectDescription+"<br>"+obj.programmeName+"<br>"+obj.costCenter+"</p></div></div></div><div class='col-md-2'><div class='dtl_tab-b'><a href='#' data-toggle='modal' data-target='#myModal' onclick='showInDetail("+obj.projectId+")'><i class='fa fa-list-alt'></i>Details</a></div><div class='dtl_tab-b'><a href='#' onclick='deleteProject("+obj.projectId+")'><i class='fa fa-trash'></i>Delete</a></div></div></div></div></div>";
var onLoadProjectList=null;
var noOfRows=0
var noOfButtons=0;
var noOfRowsToDisplay=4;
$.ajax({
	url :  "ajax/getAllProjects",
	type : "Post",
	async : false,
    dataType:"json",
	success : function(data) {
		onLoadProjectList=data;
	
	}
});

$(function(){

	$('span#msg').delay(5000).fadeOut();
    noOfRows=onLoadProjectList.length;
    $("#noOfRows").html(noOfRows);
  
	var num=0;
	var num1=noOfRows;
	num=parseInt(noOfRows/noOfRowsToDisplay);

	if(noOfRows>noOfRowsToDisplay){
	for(var i=0;i<num;i++){
		num1=num1-noOfRowsToDisplay;
	}
	if(num1==0){
		noOfButtons=num;

	}else{
		noOfButtons=num+1;
	}
	}else{
		noOfButtons=1;
	}
    	$('#select2').append("<option value=''>" + 'select' + "</option>");
    for(var i=0;i<onLoadProjectList.length;i++){
			var obj=onLoadProjectList[i];
			$("#table1Div").append("<div class='row no-pad' id='div"+(i+1)+"'><div class='bds_panel'><div class='row no-pad'><div class='col-md-10'><div class='dtl_list'><div class='col-md-12 dtl_ct'><h3 class='dtl_h3'>"+obj.projectCode+"</h3><p>"+obj.projectDescription+"<br>"+obj.projectOwner+"<br>"+obj.npClientCostCentreImpl.costCentreCode+"<br>"+obj.npClientCostCentreImpl.costCentreName+"</p></div></div></div><div class='col-md-2'><div class='dtl_tab-b'><a href='#' data-toggle='modal' data-target='#myModal' onclick='showInDetail("+obj.clientProjectID+")'><i class='fa fa-list-alt'></i>Details</a></div><div class='dtl_tab-b'><a href='#' onclick='deleteProject("+obj.clientProjectID+")'><i class='fa fa-trash'></i>Delete</a></div></div></div></div></div>");
    }

	for(var i=0;i<noOfButtons;i++){
		$("#indexId2").append("<li id='li"+(i+1)+"'><a id='hr"+(i+1)+"' href='#0' onclick='showButtonRealatedRows("+(i+1)+")'>"+(i+1)+"</a></li>");
	}
	if(noOfButtons==1){
		$("#ancher3").attr("class","disabled");
		$("#ancher4").attr("class","disabled");
	}
		if(parseInt(noOfButtons)>0){
		
			$("#hr1").attr("class","current");
		}
		var starting=1;
		var ending=4;
		var count=0;
		for(var i=0;i<onLoadProjectList.length;i++){
			count++;
			if(count>=starting && count<=ending){
				$("#div"+(i+1)).show();
			}else{
				$("#div"+(i+1)).hide();
			}
		}
});




function addRows(onLoadProjectList){
	
     noOfRows=onLoadProjectList.length;
    $("#noOfRows").html(noOfRows);

    	
    for(var i=0;i<onLoadProjectList.length;i++){
			var obj=onLoadProjectList[i];
			$("#table1Div").append("<div class='row no-pad' id='div"+(i+1)+"'><div class='bds_panel'><div class='row no-pad'><div class='col-md-10'><div class='dtl_list'><div class='col-md-12 dtl_ct'><h3 class='dtl_h3'>"+obj.projectCode+"</h3><p>"+obj.projectDescription+"<br>"+obj.projectOwner+"<br>"+obj.npClientCostCentreImpl.costCentreCode+"<br>"+obj.npClientCostCentreImpl.costCentreName+"</p></div></div></div><div class='col-md-2'><div class='dtl_tab-b'><a href='#' data-toggle='modal' data-target='#myModal' onclick='showInDetail("+obj.clientProjectID+")'><i class='fa fa-list-alt'></i>Details</a></div><div class='dtl_tab-b'><a href='#' onclick='deleteProject("+obj.clientProjectID+")'><i class='fa fa-trash'></i>Delete</a></div></div></div></div></div>");
		}
   
	var num=0;
	var num1=noOfRows;
	num=parseInt(noOfRows/noOfRowsToDisplay);

	if(noOfRows>noOfRowsToDisplay){
	for(var i=0;i<num;i++){
		num1=num1-noOfRowsToDisplay;
	}
	if(num1==0){
		noOfButtons=num;

	}else{
		noOfButtons=num+1;
	}
	}else{
		noOfButtons=1;
	}
	
		for(var i=0;i<noOfButtons;i++){
			$("#indexId2").append("<li id='li"+(i+1)+"'><a id='hr"+(i+1)+"' href='#0' onclick='showButtonRealatedRows("+(i+1)+")'>"+(i+1)+"</a></li>");
		}
		if(noOfButtons==1){
			$("#ancher3").attr("class","disabled");
			$("#ancher4").attr("class","disabled");
		}
		if(parseInt(noOfButtons)>0){
	
			$("#hr1").attr("class","current");
		}
		var starting=1;
		var ending=4;
		var count=0;
		for(var i=0;i<onLoadProjectList.length;i++){
			count++;
			if(count>=starting && count<=ending){
				$("#div"+(i+1)).show();
			}else{
				$("#div"+(i+1)).hide();
			}
		}
}

function deleteRows(onLoadProjectList){
    var size=onLoadProjectList.length;
    for(var i=0;i<size;i++){
    onLoadProjectList.splice(0,1);
    $("#div"+(i+1)).remove();
    }
    noOfRows=0;
    deleteButton(noOfButtons);
}

function filterFunction1(){
	var str=$("#select1").val();
	$('#select2')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value="">select</option>');
	    
	if(str==""){
	  $.ajax({
	url :  "ajax/getAllProjects",
	type : "Post",
	async : false,
    dataType:"json",
	success : function(data) {
	 deleteRows(onLoadProjectList);
		    	onLoadProjectList=data;
		    	addRows(onLoadProjectList);
		    	
	}
});
	}else{
	$.ajax({
		url :  "ajax/getStringList?str="+str,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
			$.each(data, function(i, obj) { 
				var div_data = "<option value='"+obj+"'>" + obj + "</option>";
				$('#select2').append(div_data);
			});
		}
	});
	}
}

function filterFunction2(){
	var str1=$("#select1").val();	
	var str2=$("#select2").val();

	if(str1!=""&&str2!=""){
	$.ajax({
		url :  "ajax/filterData?str1="+str1+"&str2="+str2,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
		   deleteRows(onLoadProjectList);
		    	onLoadProjectList=data;
		    	addRows(onLoadProjectList);
		    	
		}
	});
	}else{
	    $.ajax({
	url :  "ajax/getAllProjects",
	type : "Post",
	async : false,
    dataType:"json",
	success : function(data) {
	 deleteRows(onLoadProjectList);
		    	onLoadProjectList=data;
		    	addRows(onLoadProjectList);
		    
	}
});
	}
}

function showInDetail(id){
   
   	$.ajax({
		url :  "ajax/getObjectById?id="+id,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
			$("#programID").val(data.clientProjectID);
		   $("#Text1").val(data.projectCode);
		   $("#Text4").val(data.projectOwner);
		   $("#Text5").val(data.npClientCostCentreImpl.costCentreCode);
		   $("#Textarea").val(data.projectOverview);
		   $("#Text2").val(data.projectDescription);
		   $("#Text13").val(data.npClientCostCentreImpl.costCentreId);
		   $('video#editVideo').attr('src',data.videoUrl);
		   $("#editProjectModal span").remove();
           $("#editProjectModal input").removeAttr("style");
           $("#Textarea").removeAttr("style");
		}
	});
	makeReadOnly();
}
function makeReadOnly(){
	 $("#Text1").attr("readonly","readonly");
	 $("#Text2").attr("readonly","readonly");
	 $("#Text3").attr("readonly","readonly");
	 $("#Text4").attr("readonly","readonly");
	 $("#Text5").attr("readonly","readonly");
	 $("#Text6").attr("readonly","readonly");
	 $("#Text7").attr("readonly","readonly");
	 $("#Textarea").attr("readonly","readonly");
	 $('#editFile').attr("disabled","disabled");
	}
function deleteProject(id){
    if (confirm('Are you sure you want to delete row')) {
   	$.ajax({
		url :  "ajax/deleteProject?id="+id,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
			var count = $('span.count').text();
		      var _count =  parseInt(count);
		      $('span.count').text(_count-1);
		      $('span#msg').text("CostCentre deleted successfully").delay(5000).fadeOut();
		   deleteRows(onLoadProjectList);
		    	onLoadProjectList=data;
		    	addRows(onLoadProjectList);
		}
	});
} else {
   return false;
}

}

function sortByFunction(){
	var str=$("#select3").val();
	if(str==""){
		return false;
	}
	$.ajax({
		url :  "ajax/sortData?str="+str,
		type : "Post",
		async : false,
		data  : JSON.stringify(onLoadProjectList),
	    headers: {
                       "Content-Type": "application/json;charset=UTF-8"
                   },
		success : function(data) {
		   deleteRows(onLoadProjectList);
		    	onLoadProjectList=data;
		    	addRows(onLoadProjectList);
		    	
		}
	});
	
}

var buttonPressed=1;

function showButtonRealatedRows(num){
	if(parseInt(num)>1){
		$("#ancher1").removeAttr("class");
		$("#ancher2").removeAttr("class");
	}
	if(parseInt(num)==1){
		$("#ancher1").attr("class","disabled");
		$("#ancher2").attr("class","disabled");
		$("#ancher3").removeAttr("class");
		$("#ancher4").removeAttr("class");
	}
	if(parseInt(num)==noOfButtons){
		$("#ancher1").removeAttr("class");
		$("#ancher2").removeAttr("class");
		$("#ancher3").attr("class","disabled");
		$("#ancher4").attr("class","disabled");

	}
	$("#hr"+buttonPressed).removeAttr("class");
	$("#hr"+num).attr("class","current");
	buttonPressed=num;
	displaySelected(buttonPressed);
}

function moveToFirst(){
	$("#hr"+buttonPressed).removeAttr("class");
	buttonPressed=1;
	$("#hr"+buttonPressed).attr("class","current");
	displaySelected(buttonPressed);
	$("#ancher1").attr("class","disabled");
	$("#ancher2").attr("class","disabled");
	$("#ancher3").removeAttr("class");
	$("#ancher4").removeAttr("class");
}

function moveOneStepLeft(){
	if(buttonPressed>1){
		$("#hr"+buttonPressed).removeAttr("class");
		buttonPressed=buttonPressed-1;
		$("#hr"+buttonPressed).attr("class","current");
		displaySelected(buttonPressed);
	}
	if(buttonPressed==1){
		$("#ancher1").attr("class","disabled");
		$("#ancher2").attr("class","disabled");
		$("#ancher3").removeAttr("class");
		$("#ancher4").removeAttr("class");
	}
}

function moveOneStepRight(){
	if(buttonPressed<noOfButtons){
	$("#hr"+buttonPressed).removeAttr("class");
	buttonPressed=buttonPressed+1;
	$("#hr"+buttonPressed).attr("class","current");
	displaySelected(buttonPressed);
	}
	if(buttonPressed==noOfButtons){
		$("#ancher1").removeAttr("class");
		$("#ancher2").removeAttr("class");
		$("#ancher3").attr("class","disabled");
		$("#ancher4").attr("class","disabled");
	}
}

function moveToLast(){
	$("#hr"+buttonPressed).removeAttr("class");
	buttonPressed=noOfButtons;
	$("#hr"+buttonPressed).attr("class","current");
	displaySelected(buttonPressed);
	$("#ancher1").removeAttr("class");
	$("#ancher2").removeAttr("class");
	$("#ancher3").attr("class","disabled");
	$("#ancher4").attr("class","disabled");
}

function displaySelected(buttonPressed){
	
	var starting=buttonPressed*noOfRowsToDisplay-(noOfRowsToDisplay-1);
	var ending=buttonPressed*noOfRowsToDisplay;
	var count=0;
	for(var i=0;i<onLoadProjectList.length;i++){
		count++;
		if(count>=starting && count<=ending){
			$("#div"+(i+1)).show();
		}else{
			$("#div"+(i+1)).hide();
		}
	}
	
}


function noOfButtons(){
	var num=0;
	var num1=noOfRows;
	num=parseInt(noOfRows/noOfRowsToDisplay);

	if(noOfRows>noOfRowsToDisplay){
	for(var i=0;i<num;i++){
		num1=num1-noOfRowsToDisplay;
	}
	if(num1==0){
		noOfButtons=num;

	}else{
		noOfButtons=num+1;
	}
	}else{
		noOfButtons=1;
	}
}

function addButtons(noOfButtons){
 
	for(var i=0;i<noOfButtons;i++){
		$("#indexId2").append("<li id='li"+(i+1)+"'><a id='hr"+(i+1)+"' href='#0' onclick='showButtonRealatedRows('"+(i+1)+"')'>"+(i+1)+"</a></li>");
	}
}

function deleteButton(noOfButtons){
	
	for(var i=0;i<noOfButtons;i++){
		$("#li"+(i+1)).remove();
		}
	noOfButtons=0;
}

var refCount=0;

function editFunction(){
        refCount++;
       
        var dd=  $('#myModal').find('form');
  dd.find('input[type=text]').removeProp("readonly");
  dd.find('input[type=file]').removeProp("disabled");
  dd.find('textarea').removeProp("readonly");
  dd.find('input#Text1').prop('readonly','readonly');
}
function saveEdited(){
   if($('#Text1').val()==""||$('#Text2').val()==""||$('#Text4').val()==""||$('#Textarea').val()==""){
	   alert("Please Fill All Fields");
	   return;
   }
   if($("#Text5").val()==""||$("#Text13").val()==""){
	   alert("Select CostCenter");
	   return;
   }
    if(refCount!=0){
    	var costCentreID=$("#Text13").val();
	
	/*var obj={
			
			clientProjectID:$('#programID').val(),
			projectCode:$('#Text1').val(),
			projectDescription:$('#Text2').val(),
			projectOwner:$('#Text4').val(),
			projectOverview:$('#Textarea').val()
			
		   
	};
	
	var oMyForm = new FormData();
    	oMyForm.append("video", file2.files[0]);
    	oMyForm.append("clientProjectID",$('#programID').val());
    	oMyForm.append("projectCode",$('#Text1').val());
    	oMyForm.append("projectDescription",$('#Text2').val());
    	oMyForm.append("projectOwner",$('#Text4').val());
    	oMyForm.append("projectOverview",$('#Textarea').val());
	   
	$.ajax({
		url :  "ajax/saveData?npClientCostCentreImpl.costCentreID="+costCentreID,
		type : "Post",
		async : false,
		data  : oMyForm,
	    headers: {
                       "Content-Type": "multipart/form-data;charset=UTF-8"
                   },
		success : function(data) {
		  alert("success");
		}
	});*/
	
	
    }else{
        alert("Edit And Save");
    }
}

function getCostCenter(str,text1,text2){
	
	var myData=null;
	if(str.length>=2){
	$.ajax({
		url :  "ajax/getCostCenter?str="+str,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
		
			myData=data;
		}
	});
	
	$("#"+text1).autocomplete({
        source: myData, // The source of the AJAX results
        minLength: 2, // The minimum amount of characters that must be typed before the autocomplete is triggered
        focus: function( event, ui ) { // What happens when an autocomplete result is focused on
            $("#"+text1).val( ui.item.value );
            return false;
      },
      select: function ( event, ui ) { // What happens when an autocomplete result is selected
          $("#"+text1).val( ui.item.value );
          $('#'+text2).val( ui.item.id );
      }
  });
	}
}

function submitFunction(id){
	var code=$("#"+id).val();
	$.ajax({
		url :  "ajax/checkCode?str="+code,
		type : "Get",
		async : false,
	    dataType:"json",
		success : function(data) {
			if(data!="OK"){
				$("#"+id).val("");
				$("#"+id).focus();
			alert("Code Already Exists");
			}
		}
	});
	
	$(document).delegate("#editFile","change",function showVideoFile(event){
		var vid = $(this).parent().find("Video");
		if (this.files && this.files[0]) {
			
			 var filerdr = new FileReader();
			 filerdr.onload = function(event) {
				 vid.attr('src', event.target.result);
			 },
			 filerdr.readAsDataURL(this.files[0]);
			 }
		
	});
	
}
function clearAll(){
	 $("#Text6").val("");
	 $("#Text8").val("");
	 $("#Text9").val("");
	 $("#Text10").val("");
	 $("#Text11").val("");
	 $("#Text12").val("");
	 $("#Textarea1").val("");
	 $("#addProjectModal span").remove();
	 $("#addProjectModal input").removeAttr("style");
	  $("#Textarea1").removeAttr("style");
	}
$(document).ready(function() {
	$(".modal").on("hide.bs.modal", function () { 
	        $(".has-error").removeClass("has-error");
	        $(".error").removeProp("style");
	        $(".error").removeClass("error");
	        $(".form-error").remove();
	    });
	    
	});
	