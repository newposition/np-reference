 $(document).ready(function(){
	 $( '.sumeditor' ).summernote({
		    height: 94
		  });
		  $( '.acheditor' ).summernote({
		    height: 145
		  });
		  $( '.peseditor' ).summernote({
		    height: 120
		  });

		 
		  
	 $('.editorr .note-toolbar').css('display','none');
     $('.editorr .note-editor .note-statusbar').css('display','none'); 
     $('.note-editor .note-editable').attr('contenteditable','false');
     
	 $(document).delegate("#update ","click",function(event){
		
		 var validation=true;
	 var sponserId=$('#Ref_Modal #spid').val();
	 var firstName=$("#Ref_Modal #firstName").val();
	 if(firstName.length == 0)
		 {
		 $("#Ref_Modal .errorText").text("All Fields are mandatory");
		 $("#Ref_Modal #firstName").addClass("errorTag");
		 validation=false;
		 }
	 var surName=$("#Ref_Modal #surName").val();
	 if(surName.length == 0)
	 {
		 $("#Ref_Modal .errorText").text("All Fields are mandatory");
		 $("#Ref_Modal #surName").addClass("errorTag");
		 validation=false;
	 }
	 var relationship=$("#Ref_Modal #relationship").val();
	 if(relationship.length == 0)
	 {
		 $("#Ref_Modal .errorText").text("All Fields are mandatory");
		 $("#Ref_Modal #relationship").addClass("errorTag");
		 validation=false;
	 }
	 var email=$("#Ref_Modal #email").val();
//	 var pattern="/^[a-zA-Z0-9]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]*[a-zA-Z]+\\.[a-zA-Z]{2,}$/";
	 if(email.length == 0)
	 {
		 $("#Ref_Modal .errorText").text("All Fields are mandatory");
		 $("#Ref_Modal #email").addClass("errorTag");
		 validation=false;
	 }
	 var userMail=$("#dvemail1").text();
	 if(userMail == email.trim())
		 {
		 $("#Ref_Modal .errorText").text("You cannot add yourself as sponsor. Please enter valid sponsor");
		 $("#Ref_Modal #email").addClass("errorTag");
		 validation=false;
		 }
//	 else if (! /^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]+\.[a-zA-Z]{2,}$/.test(email) ){
	 else if (! validEmail(email)) {
		 $("#Ref_Modal .errorText").text("Invalid Email");
		 $("#Ref_Modal #email").addClass("errorTag");
		 validation=false;
		 }
	 var dateFrom=$("#Ref_Modal #dateFrom").val();
	 var dateTo=$("#Ref_Modal #dateTo").val();
	 var fromDate=convertDate(dateFrom.replace(/-/g, " "));
	 var toDate= convertDate(dateTo.replace(/-/g, " "));
	 if(dateFrom.length == 0)
	 {
		 $("#Ref_Modal .errorText").text("All Fields are mandatory");
		 $("#Ref_Modal #dateFrom").addClass("errorTag");
		 validation=false;
	 }
	 
	 else if(!validateDate(dateFrom))
	 {
		 $("#Ref_Modal .errorText").text("Invalid date format (provide: dd/mm/yyyy)");
		 $("#Ref_Modal #dateFrom").addClass("errorTag");
		 validation=false;
			
	 }
	 else if(fromDate >= toDate)
		 {
		 $("#Ref_Modal .errorText").text("To Date must be greater than From Date");
		 $("#Ref_Modal #dateTo").addClass("errorTag");
		 validation=false;
		 }
	 if((dateTo.length >0 && dateTo!="current") && !validateDate(dateTo))
	 {
		 $("#Ref_Modal .errorText").text("Invalid date format (provide: dd/mm/yyyy)");
		 $("#Ref_Modal #dateTo").addClass("errorTag");
		 validation=false;
			
	 } 
	 if(validation && isFutureDate(fromDate))
	 {
		 $("#Ref_Modal .errorText").text("From Date should not be future date");
		 $("#Ref_Modal #dateFrom").addClass("errorTag");
	 validation=false;
	 }
	
		if(validation)
			{
			var form = $("#Ref_Modal #form-contact");
		 $.ajax({
	        	url:"ajax/mySponsor?sponserId="+sponserId.trim(),
	        	type : 'POST',
	        	async :false,
	        	data:form.serialize(),
	        	success:function(response){
	        		if(response == "false")
	        			{
	        			 $("#Ref_Modal .errorText").text("You have already added this sponsor. Please enter valid sponsor");
	        			event.preventDefault();
	        			}
	        		else
	        			{
	        			window.location="myaccount";
	        			}
	        	},
	        	error:function(error){
	        	}
	        	
	   	 });
			}
		else
			{
			$(function() {
				  $( ".datepicker-12" ).datepicker({
					  dateFormat: 'dd/mm/yy',
						  changeMonth: true,
						    changeYear: true,
						    yearRange     : "1900:+1",
							     onClose: function(dateText, inst) { 
							            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
							            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
							        }
				  });
				});
			}
		 event.preventDefault();
	 });
	 $('.edit-save').click(function(event){
		    $(this).toggleClass('my_save_pencil');
		  });
	 $(document).delegate("#myaccount_achievements", "click", function(event){
		 var edit=$(this).hasClass("my_save_pencil");
		 if(!edit)
			 {
			 var companyachievements=$(this).parent().parent().parent();
	       var achievements= $(companyachievements).find(".note-editor .note-editable").html();
	 var target = $(event.currentTarget);

		var parent = target.parents("div.blue_border");
		
		var id=$(parent).find('input[type="hidden"]');
		 var uid=$(id).val();
		 var validation=true;
		 if(!$.isNumeric(uid))
			 {
			 validation=false;
			 }
		 if(achievements.length ==0)
			 {
			 validation=false;
			 }
		 if( validation)
			 {
			 $(companyachievements).find('   .note-toolbar').css('display','none');
			 $(companyachievements).find('   .note-editor .note-editable').css('height','150');
			 $(companyachievements).find('   .note-editor .note-style').css('display','none');
			 $(companyachievements).find('   .note-editor .note-editable').attr('contenteditable','false');
	 $.ajax({
        	url:"ajax/saveAchievements",
        	type : 'POST',
        	async :false,
        	data:{'achievements':achievements,'uid':uid},
        	success:function(response){
        		window.location="myaccount";
        		event.preventDefault();
        	},
        	error:function(error){
        	}
        	
   	 });
			 }
			 }
		 else
			 {
			 var target = $(event.currentTarget);

				var parent = target.parents("div.blue_border");
				
				
				 var companyachievements=$(this).parent().parent().parent();
			  $(companyachievements).find('.note-toolbar').css('display','block');
				 $(companyachievements).find(' .note-editor .note-style').css('display','none');
				 $(companyachievements).find(' .note-editor .note-height').css('display','none');
				 $(companyachievements).find(' .note-editor .note-table ').css('display','none');
				 $(companyachievements).find(' .note-editor .note-insert').css('display','none');
				 $(companyachievements).find('  .note-editor .note-view').css('display','none');
				 $(companyachievements).find('  .note-editor .note-help').css('display','none');
				 $(companyachievements).find('  .note-editor .note-editable').css('height','122');
				 $(companyachievements).find(' .note-editor .note-statusbar').remove();
				 $(companyachievements).find(' .note-editor .note-editable').attr('contenteditable','true');
			 }
	 event.preventDefault();
			 
	 });
	 
	
	 $("#myaccount_personalStatement").click(function(event){
		 var edit=$(this).hasClass("my_save_pencil");
		    if(!edit)
			   {
	            var personalStatement=$(".row_padding_left_elementd2  .note-editable").html();
	               if(personalStatement.length > 0)
	             	 {
	            	   //remove toolbar and disable for personal statement
		                  $('.row_padding_left_elementd2  .note-toolbar').css('display','none');
		                  $('.row_padding_left_elementd2  .note-editor .note-editable').css('height','150');
		                  $('.row_padding_left_elementd2  .note-editor .note-style').css('display','none');
		                 $('.row_padding_left_elementd2 .note-editor .note-editable').attr('contenteditable','false');
		                 $.ajax({
		                	 url:"ajax/savePersonalStatement",
		                	 type : 'POST',
		                	 async :false,
		                	 data:{personalStatement:personalStatement},
		                	 success:function(response){
		                		 window.location="myaccount";
		                		 event.preventDefault();
		                	 },
		                	 error:function(error){
		                	 }
        	
		                 });
	             	 }
			 }
		 else
		 {
			 //enable toolbar  for personal statement
			 $('.row_padding_left_elementd2  .note-toolbar').css('display','block');
			 $('.row_padding_left_elementd2 .note-editor .note-statusbar').css('display','block'); 
			 $('.row_padding_left_elementd2  .note-editor .note-style').css('display','none');
			 $('.row_padding_left_elementd2 .note-editor .note-height').css('display','none');
			$('.row_padding_left_elementd2  .note-editor .note-table ').css('display','none');
			$('.row_padding_left_elementd2  .note-editor .note-insert').css('display','none');
			$('.row_padding_left_elementd2  .note-editor .note-view').css('display','none');
			$('.row_padding_left_elementd2 .note-editor .note-help').css('display','none');
			$('.row_padding_left_elementd2  .note-editor .note-editable').css('height','175');
			$('.row_padding_left_elementd2 .note-editor .note-statusbar').remove();
			 $('.row_padding_left_elementd2 .note-editor .note-editable').attr('contenteditable','true');
		 }
	 event.preventDefault();
 });
	 
	 $(function() {
		  $( ".datepicker-12" ).datepicker({
			  dateFormat: 'dd/mm/yy',
				  changeMonth: true,
				    changeYear: true,
				    yearRange     : "-30:+30",
					     onClose: function(dateText, inst) { 
					            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
					            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
					        }
		  });
		});
	 function validateDate(dtValue)
	 {
		 var date=dtValue.split('/');
		 if( (date.length < 3)){return false;}
	    // YEAR CHECK
		if( (date[2].length != 4)  || date[2] == 0000){return false;}
		// MONTH CHECK			
		if( (date[1].length == 0 || date[1].length > 2) ||date[1] < 1 || date[1] > 12){return  false;}
		// DAY CHECK
		if( (date[0].trim().length == 0 || date[0].trim().length > 2) || date[0] < 1 || date[0] > 31){return false;}
		
		return  true;
	 }
	 function convertDate(date) {
		 var f_date=date.split('/');
		 var date1 = new Date(f_date[2],f_date[1]-1,f_date[0].trim());
		 return date1;
		 
	 }
	 function isFutureDate(date) {
		 
		 if (date > new Date()) {
			 return true;	 
		 }else {
			 return false;
		 }
		 
	 }
	 function validEmail(email) {
		var valid =  /^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+[a-zA-Z]+\.[a-zA-Z]{2,}$/.test(email);
		if (valid) {
			if (email.indexOf('..')!= -1 || email.indexOf('--')!= -1 || email.indexOf('__')!= -1) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	 }
	 
	 
	// toggle permanent contract either
	 $('#compToggle').click(function(){
	         $(".pstate").prop('disabled', false);
	     });
	 $(document).delegate("#updateUser ","click",function(event){
		 
		 var validation=true;
	 var firstName=$('#mfirstName').val();
	 if(firstName.trim().length == 0)
		 {
		 $('#Con_Det_Modal .errorText').text('All Fields are mandatory');
		 $("#Con_Det_Modal  #mfirstName").addClass("errorTag");
		 validation=false;
		 }
	 var lastName=$("#mlastName").val();
	 if(lastName.trim().length == 0)
	 {
		 $('#Con_Det_Modal .errorText').text('All Fields are mandatory');
		 $("#Con_Det_Modal  #mlastName").addClass("errorTag");
		 validation=false;
	 }
	 var telephone=$("#mtel").val();
	  if(telephone.length > 0 && (!$.isNumeric(telephone.trim()) || telephone.trim().length >16 || telephone.trim().length <10))
		 {
		 $('#Con_Det_Modal .errorText').text('please provide telephone number');
		 $("#Con_Det_Modal  #mtel").addClass("errorTag");
		 validation=false;
		 }
	 if(validation)
		 {
		 var form=$("#userForm");
		  $.ajax({
	        	url:"ajax/updateUser",
	        	type : 'POST',
	        	async :false,
	        	data:form.serialize(),
	        	success:function(response){
	        		window.location="myaccount";
	        		event.preventDefault();
	        	},
	        	error:function(error){
	        	}
	        	
	   	 });
		 }
		  event.preventDefault();
      });
	 
	 //compensation
	 $(document).delegate("#npCompensationButton","click",function(event){
		if($("input[name=bonus]").prop('disabled'))
		{
			$("input[name=salary]").removeAttr('disabled');
			$("input[name=bonus]").removeAttr('disabled');
			$("input[name=dayrate]").removeAttr('disabled');
			$("#npCompensationButton").addClass("my_save_pencil");
		}
		else
			{
			
			var type=$('input[name=compensationType]').val();
			var salary= $('input[name=salary]').val();
			var bonus= $('input[name=bonus]').val();
			var dayRate= $('input[name=dayrate]').val();
			
			var validation=true;
			if (type == "Donotmind") {
				if(salary.trim().length == 0)
					{
					validation=false;
					$("#salary_error" ).text( "Please enter the Compensation" ).show();
					}
				else if(!$.isNumeric(salary))
				{
				validation=false;
				$("#compensation_error" ).text( "Please enter the Valid Compensation" ).show();
				}
				 if(bonus.trim().length == 0)
				{
				validation=false;
				$("#bonus_error" ).text( "Please enter the Bonus" ).show();
				}
				 else if(!$.isNumeric(bonus))
					{
					validation=false;
					$("#bonus_error" ).text( "Please enter the Valid bonus" ).show();
					}
				if(dayRate.trim().length == 0)
				{
				validation=false;
				$("#dayrate_error" ).text( "Please enter the DayRate" ).show();
				}
				else if(!$.isNumeric(dayRate))
				{
				validation=false;
				$("#dayrate_error" ).text( "Please enter the Valid DayRate" ).show();
				}
			}
			else if(type.trim() == "Contract")
				{
				  if(dayRate.trim().length == 0)
			  	  {
				   validation=false;
				   $("#dayrate_error" ).text( "Please enter the DayRate" ).show();
				  }
				  else if(!$.isNumeric(dayRate))
					{
					validation=false;
					$("#dayrate_error" ).text( "Please enter the Valid DayRate " ).show();
					}
				}
			else if (type == "Permanent")
				{
			     	if(salary.trim().length == 0)
			    	{
			    	validation=false;
			    	$("#salary_error" ).text( "Please enter the salary" ).show();
			     	}
			     	else if(!$.isNumeric(salary))
					{
					validation=false;
					$("#compensation_error" ).text( "Please enter the Valid salary" ).show();
					}
			        if(bonus.length == 0)
			       {
			      validation=false;
			      $("#bonus_error" ).text( "Please enter the Bonus" ).show();
			        }
			        else if(!$.isNumeric(bonus))
					{
					validation=false;
					$("#bonus_error" ).text( "Please enter the Valid bonus" ).show();
					}
				}
			if(validation)
				{
				$("input[name=salary]").attr('disabled', 'disabled');
				$("input[name=bonus]").attr('disabled', 'disabled');
				$("input[name=dayrate]").attr('disabled', 'disabled');
			  $.ajax({
		        	url:"ajax/saveCandidateCompensation",
		        	type : 'POST',
		        	async :false,
		        	data:{compensationType:type,salary:salary,bonus:bonus,dayrate:dayRate},
		        	success:function(response){
		        		event.preventDefault();
		        	},
		        	error:function(error){
		        	}
		        	
		   	 });
				}
			
			}
		event.preventDefault();
	 });
	 
	 
	 $(".myimg #uploadBtn1").change ( function(event) {
			 if (this.files && this.files[0]) {
				 var filerdr = new FileReader();
				 filerdr.onload = function(event) {
				 $('.myimg img').attr('src', event.target.result);
				 }
				 filerdr.readAsDataURL(this.files[0]);
				 }
		});
	$(".addNewTool").keydown(function(event) {
		var text = $(this).val().trim();
		var employer=$(this).attr("id");
		var list=$(this).parents(".list_myaccount_tools");
		var employerId = employer.substr(employer.indexOf("_") + 1);
		if (event.keyCode == 13) {
		if (text.length) {
			$(this).val("");
			var request = $.ajax({
				url : "ajax/addMyaccountTool",
				type : 'POST',
				data : {
					addtool : text,
					employerID:employerId,
				}
			
			});

			request
					.done(function(response) {
						$.each(response,function(id,obj){
							var existTool = $(list).find('a').text().toLowerCase();
							if(($(list).find("a:last").html() != undefined) )
							{
								if (existTool.indexOf(obj.toLowerCase()) == -1) {
							$(list).find("a:last").after(
									"<a  id='"+id+"' name='"+obj+"' href='javascript:void(0)'>"
									+ obj
									+ " <i class='fa fa-times'></i> </a>"); 
								}
						}
							else
							{
							//if tools are not there
							$(list).find(".clearfix").before("<a  id='"+id+"' name='"+obj+"' href='javascript:void(0)'>"
									+ obj
									+ " <i class='fa fa-times'></i> </a>");
							}
						});
							$(".errTool").text("").hide();
					});

		} else {
			$(".errTool").text("").hide();
		}
	}
	});
	
	$(".addNewSkill").keydown(function(event) {
		var text = $(this).val().trim();
		var employer=$(this).attr("id");
		var list=$(this).parents(".list_myaccount_skills");
		var employerId = employer.substr(employer.indexOf("_") + 1);
		if (event.keyCode == 13) {
		if (text.length) {
			$(this).val("");
			var request = $.ajax({
				url : "ajax/addMyaccountSkill",
				type : 'POST',
				data : {
					addskill : text,
					employerID:employerId,
				}
			});
			request
					.done(function(response) {
						var existSkill = $(list).find("a").text().toLowerCase();
						$.each(response,function (id,obj) {
							
							if($(list).find("a:last").html() != undefined)
							{
								if (existSkill.indexOf(obj.toLowerCase())== -1) {
							$(list).find("a:last").after(
											"<a  id='"+id+"' name='"+obj+"' href='javascript:void(0)'>"
													+ obj
													+ " <i class='fa fa-times'></i> </a>");
							}
							}
							else
							{
							//if roles are not there
							$(list).find(".clearfix").before("<a id='"+id+"'  name='"+obj+"' href='javascript:void(0)'>"
									+ obj
									+ " <i class='fa fa-times'> </i></a>");
							}
						});
							$(".errTool").text("").hide();
						
					});

		} else {
			$(".errTool").text("").hide();
		}
	}
	});
	
	$(".addNewRole").keydown(function(event) {
		var text = $(this).val().trim();
		var employer=$(this).attr("id");
		var list=$(this).parent().parent();
		
		var employerId = employer.substr(employer.indexOf("_") + 1);
		if (event.keyCode == 13) {
		if (text.length) {
			$(this).val("");
			
			var request = $.ajax({
				url : "ajax/addMyaccountRole",
				type : 'POST',
				data : {
					addRole : text,
					employerID:employerId,
				}
			
			});
			request.done(function(response) {
				var existRoles = $(list).find("a").text().toLowerCase();
						$.each(response,function(id,obj){
							if($(list).find("a:last").html() != undefined)
								{
								if (existRoles.indexOf(obj.toLowerCase())== -1){
								//if roles are there
							$(list).find("a:last").after("<a  id='"+id+"' name='"+obj+"' href='javascript:void(0)'>"
									+ obj
									+ " <i class='fa fa-times'></i> </a>");
								}
								}
							else
								{
								//if roles are not there
								$(list).find(".clearfix").before("<a  id='"+id+"' name='"+obj+"' href='javascript:void(0)'>"
										+ obj
										+ " <i class='fa fa-times'></i> </a>");
								}
						});
						
							$(".errTool").text("").hide();
						
					});

		} else {
			$(".errTool").text("").hide();
		}
	}
	});
	
	$(".list_myaccount_skills ").on("click",'a',function(e){
		
		
			var skillId=$(this).attr('id');
			var employerId=$(this).parents(".blue_border").find("input[type=hidden]").val();
			var skillName=$(this).attr('name');
			var removeSkill=$(this);
			e.preventDefault();
			  if($(e.target).is('.fa-times'))
				  {
		$.ajax({
			type : "POST",
			url:"ajax/deleteCandidateCompanySkill?skillId="+skillId+"&employerId="+employerId,
			datatype:'json',
			success : function(response) {
				removeSkill.remove();
			},
			error : function() {
			}
		});
				  }
	});
	$(".list_myaccount_roles").on("click",'a',function(e){
		
		
		var roleId=$(this).attr('id');
		var employerId=$(this).parents(".blue_border").find("input[type=hidden]").val();
		var roleName=$(this).attr('name');
		var removeRole=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
			  {
	$.ajax({
		type : "POST",
		url:"ajax/deleteCandidateCompanyRole?roleId="+roleId+"&employerId="+employerId,
		datatype:'json',
		success : function(response) {
			removeRole.remove();
		},
		error : function() {
		}
	});
			  }
});
	$(".list_myaccount_tools").on("click",'a',function(e){
		
		
		var toolId=$(this).attr('id');
		var employerId=$(this).parents(".blue_border").find("input[type=hidden]").val();
		var toolName=$(this).attr('name');
		var removeTool=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
			  {
	$.ajax({
		type : "POST",
		url:"ajax/deleteCandidateCompanyTool?toolId="+toolId+"&employerId="+employerId,
		datatype:'json',
		success : function(response) {
			removeTool.remove();
		},
		error : function() {
		}
	});
			  }
});
	
	$(".sk-edit").click(function(){
    	var inputBox=$(this).parents('.border_bottom_right_one');
        $(inputBox).find(".sk-qeb").slideToggle();
    });
	$(".rl-edit").click(function(){
		var inputBox=$(this).parents('.border_bottom_right_one');
        $(inputBox).find(".rl-qeb").slideToggle();
    });
	$(".tas-edit").click(function(){
		var inputBox=$(this).parents('.border_bottom_right_one');
        $(inputBox).find(".tas-qeb").slideToggle();
    });
 });
