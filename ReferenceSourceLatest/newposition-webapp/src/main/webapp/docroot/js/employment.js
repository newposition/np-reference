var myEmploymentHistory = function() {

	return {

		init : function() {
            var listOfData = [];
            var listOfSkills =[];
            var listOfRoles = [];
            var listOfTools = [];

            $('.companyNameValue').text($('.companyNameValue').val()).hide();
           
            
            
            $(".links_skills").on("click",'a',function(){
            	
            	if(myEmploymentHistory.returnCompanyName('').length){
            		$(this).toggleClass("blue_link");
            	}
            });
            
            $(".roles").on("click",'a',function(){
            	if(myEmploymentHistory.returnCompanyName('').length){
            		$(this).toggleClass("blue_link");
            	}
        	});
            $(".tools").on("click",'a',function(){
            	if(myEmploymentHistory.returnCompanyName('').length){
            		$(this).toggleClass("blue_link");
            	}
        	});
            

//            $(el).on('click', dismiss, this.close)
			$(".em_box .edit_btn").click(myEmploymentHistory.editCompany);
			$(".em_box .plus_btn").click(myEmploymentHistory.addCompany);
			$(".em_box .update_btn").click(myEmploymentHistory.updateCompany);
			$(".em_box .cancel_btn").click(myEmploymentHistory.cancelCompany);
			$(".em_box .save_btn").click(myEmploymentHistory.saveCompany);
			$(".em_box .trash_btn").click(myEmploymentHistory.deleteCompany);
			$("#selectbox1").change(myEmploymentHistory.getSkillsList);
			$("#selectbox2").change(myEmploymentHistory.getRolesList);
			$("#selectbox3").change(myEmploymentHistory.getToolsList);
			$("#skilBtn").on('click', myEmploymentHistory.getModalPopup);
			$("#roleBtn").click(myEmploymentHistory.getRolesModalPopup);
			$("#toolBtn").click(myEmploymentHistory.getToolsModalPopup);
			$('.next').click(myEmploymentHistory.getNextEmployer);
			$('.prev').click(myEmploymentHistory.getPreviousEmployer);
			$('#addskillBasket').keyup(myEmploymentHistory.addSkill);
			$('#addroleBasket').keyup(myEmploymentHistory.addRole);
			$('#addtoolBasket').keyup(myEmploymentHistory.addTool);
			
		},
		
		returnCompanyName : function (data) 
		{
			var company = $('.employeRecords .em_box_details').find('.line1').text();
			return company; 
		},
		
		validateDate:function (dtValue)
		 {
			 var date=dtValue.split('/');
			 if( (date.length < 3)){return false;}
		    // YEAR CHECK
			if( (date[2].length != 4 ) || date[2] == 0000){return false;}
			// MONTH CHECK			
			if( (date[1].length == 0 || date[1].length > 2) ||date[1] < 1 || date[1] > 12){return  false;}
			// DAY CHECK
			if( (date[0].length == 0 || date[0].length > 2) || date[0] < 1 || date[0] > 31){return false;}
			return  true;
		 },
		 convertDate: function (date) {
			 var f_date=date.split('/');
			 var date1 = new Date(f_date[2],f_date[1]-1,f_date[0]);
			 return date1;
			 
		 },
		

		updateCompany:function(event){
			var id = $("#employerId").val();
			var uc = $(".line1 input[type=text]").val();
			
			var uc2a = $(".line2ip1").val();
			var uc2b = $(".line2ip2").val();

			var from=myEmploymentHistory.convertDate(uc2a.replace(/-/g, " "));
			var to=myEmploymentHistory.convertDate(uc2b.replace(/-/g, " "));
			var uc3 = $(".line3 input[type=text]").val();

			var uc4 = $(".line4 input[type=text]").val();
			
			var uc5 = $("#achievements .note-editable").html();
			
			
			var validation=true;
			var error=0;
			

			if(uc.length==0){
				error++;
				$(".line1 input[type=text]").addClass("errorTag");
				validation=false;
			}
			else{
				$(".line1 input[type=text]").removeClass("errorTag");
			}
			if(uc2a.length==0){
				error++;
				$(".line2ip1").addClass("errorTag");
				validation=false;
			}
			 else if(!myEmploymentHistory.validateDate(uc2a))
			 {
				 error=6;
				 $(".line2ip1").addClass("errorTag");
				 validation=false;
			 }
			 else if(from > new Date())
			 {
			 error=10;
			 $(".line2ip1").addClass("errorTag");
			 validation=false;
			 }
			else
				{
				$(".line2ip1").removeClass("errorTag");
				}
			
			 
			 if((uc2b.length >0 && uc2b.trim()!="current") && !myEmploymentHistory.validateDate(uc2b))
			 {
				 validation=false;
				 error=6;
				 $(".line2ip2").addClass("errorTag");
			 }
			 else if(uc2b.length >0 && (from >= to ))
			 {
				 error=7;
			    $(".line2ip2").addClass("errorTag");
			    validation=false;
			 }
			 else
				{
				$(".line2ip2").removeClass("errorTag");
				}
			if(uc3.length==0){
				error++;
				 $(".line3 input[type=text]").addClass("errorTag");
				validation=false;
			}
			else{
				 $(".line3 input[type=text]").removeClass("errorTag");

			}
			if(uc4.length==0){
				error++;
				 $(".line4 input[type=text]").addClass("errorTag");
				validation=false;
			}
			else{
				 $(".line4 input[type=text]").removeClass("errorTag");

			}
			if(error >0 && error < 6)
			{	
		           $('.errorText').text('All Fields are mandatory');
			}
	       	else if(error == 6 )
			{
			    $('.errorText').text('Invalid date format (provide: dd/mm/yyyy)');
			}
	    	else if(error == 7 )
		   {
		      $('.errorText').text('To Date must be greater than From Date');
		   }
	    	else if(error == 10 )
			{
			$('.errorText').text('From Date must be less than Todays Date');
			}
			if(validation)
				{
				$('.errorText').text('');
			$.ajax({
				type : "POST",
				url : "ajax/updateCompany",
				data : {
					company : uc,
					df : uc2a,
					dt : uc2b,
					jobtitle : uc3,
					corptitle : uc4,
					achievements : uc5,
					employerId : id
				},
				success : function() {
					var param=$("#Emp_hist_Modal #form-contact").attr("name");
					if(param!=null && param.trim() =="myaccount")
						{
					window.location="myaccount";
						}
					$(".line1").text(uc);
					if(uc2b.length == 0)
						{
						uc2b="current";
						}
					$(".line2").html("<span class='line2a'>" + uc2a	+ "</span> To <span class='line2b'>" + uc2b
							+ "</span>");
					$(".line3").text(uc3);
					$(".line4").text(uc4);
					
					$(".plus_btn").css("display","block");
					$(".edit_btn").css("display","block");
					$(".save").removeClass("update_btn");
					$(".save").addClass("save_btn");
					$(".save_btn").css("display","none");
				    $(".trash_btn").css("display","block");
				},
				error : function() {
				}
				
			});
				}
			else
				{
				$( ".datepicker-12" ).datepicker({
					yearRange     : "1900:+1",
					 changeMonth: true,
				     changeYear: true,
				     dateFormat: 'dd/mm/yy',
				     onClose: function(dateText, inst) { 
				            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				        }
				  });
				}
			event.preventDefault();
			
		},
		editCompany : function() {
			var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
			
			if (isCompany != '') {
				
			$( ".save").unbind( "click" );
			$(".save").removeClass('save_btn');
			$(".save").addClass('update_btn');
			$(".update_btn").css("display","inline-block");
			$(".update_btn").css("left","0px");
			$(".em_box .update_btn").click(myEmploymentHistory.updateCompany);
			$(".em_box_edit_logo").show();
			$(this).hide();
			$(".plus_btn , .trash_btn").hide();
			$(".save_btn").css({"left" : "0"}).show();
			var line_te_one = $(".line1").text();
			$(".line1").html("<input type='text' class='pp_text' value='" + line_te_one	+ "' />");

			var line_te_one2a = $("span.line2a").text();
			var line_te_one2b = $("span.line2b").text();
			$(".line2").html("<input type='text' class='pp_text line2ip1 datepicker-12' value='"
									+ line_te_one2a
									+ "' style='width:47.5%;' /><input type='text' class='pp_text line2ip2 datepicker-12' value='"+ line_te_one2b.trim()+ "'  style='width:47.5%;'/>");

			var line_te_one3 = $(".line3").text();
			$(".line3").html("<input type='text' class='pp_text' value='" + line_te_one3+ "' />");

			var line_te_one4 = $(".line4").text();
			$(".line4").html(
					"<input type='text' class='pp_text' value='" + line_te_one4
							+ "' />");
			 $( ".datepicker-12" ).datepicker({
				 yearRange     : "1900:+1",
				 changeMonth: true,
			     changeYear: true,
			     dateFormat: 'dd/mm/yy',
			     onClose: function(dateText, inst) { 
			            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			        }
			  });
			 
            $('.em_box_details .line1 .pp_text').autocomplete({
                source : listOfData
            });
            
		}

		},

		addCompany : function() {
			//disable all selected skill/roles/tools
			$('.links_skills a').removeClass('blue_link');
			$('.links_jsonlyclick.roles a').removeClass('blue_link');
			$('.links_jsonlyclick.tools a').removeClass('blue_link');
			
			$('#companyNo').hide();
			$("#skillsWithCurrentCompany").html('');
			$("#rolesWithCurrentCompany").html('');
			$("#toolsWithCurrentCompany").html('');
			$("span.companyNameSkills").text(' (Please select skills you used at your time with)');
			$("span.companyNameRoles").text(' (Please select skills you used at your time with)');
			$("span.companyNameTools").text(' (Please select skills you used at your time with)');
			
			$(".cancel_btn").click(myEmploymentHistory.cancelCompany);

			$(".em_box_edit_logo").html("<a href='javascript:void(0)'></a>").show();
			$(this).hide();
			$(".plus_btn , .trash_btn , .edit_btn").hide();
			$(".save_btn").css({"left" : "0"}).show();
			$(".cancel_btn").css({"left" : "23px"}).show();
			$(".line1").html("<input type='text' value='' name='companyName' placeholder='Enter Company Name' class='pp_text' required />");
			$(".line2").html("<input type='text' value='' name='fromDate' placeholder='Date From' class='datepicker-12 line2ip1 pp_text' style='width:47.5%;' required /><input type='text' value='' name='toDate' placeholder='Date To' class='datepicker-12 line2ip2 pp_text' style='width:47.5%;' required />");
			$(".line3").html("<input type='text' value='' name='corpTitle' placeholder='Enter Corp Title' class='pp_text' required />");
			$(".line4").html("<input type='text' value='' name='jobTitle' placeholder='Enter Job Title' class='pp_text' required />");
            $('.em_box_details .line1 .pp_text').autocomplete({
                source : listOfData
            });

			$(".note-editable").text('');
			  $( ".datepicker-12" ).datepicker({
				  yearRange     : "1900:+1",
					 changeMonth: true,
				     changeYear: true,
				     dateFormat: 'dd/mm/yy',
				     onClose: function(dateText, inst) { 
				            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				        }
			  });
		},

		cancelCompany : function() {
			/*$(".save_btn").hide();
			$(this).hide();
			$(".plus_btn , .trash_btn , .edit_btn").show();
			$(".em_box_edit_logo").hide();

			$(".line1").html("");
			$(".line2").html("");
			$(".line3").html("");
			$(".line4").html("");*/
			
			location.reload();

		},

		saveCompany : function() {
			$('#companyNo').hide();
			$(".em_box_edit_logo").hide();
			$(this).hide();
			$(".cancel_btn").hide();
			$(".plus_btn, .trash_btn, .edit_btn").show();
			var uc = $(".line1 input[type=text]").val();
			var uc2a = $(".line2ip1").val();
			var uc2b = $(".line2ip2").val();
			var uc3 = $(".line3 input[type=text]").val();
			var uc4 = $(".line4 input[type=text]").val();
			var uc5 = $(".note-editable").html();
			var from=myEmploymentHistory.convertDate(uc2a);
			var to=myEmploymentHistory.convertDate(uc2b); 
			var validation=true;
			var error=0;
			
			if(uc.length==0){
				$(".line1 input[type=text]").addClass("errorTag");
				error++;
				validation=false;
			}
			else{
				$(".line1 input[type=text]").removeClass("errorTag");
			}
			if(uc2a.length==0){
				error++;
				$(".line2ip1").addClass("errorTag");
				validation=false;
			}
			 else if(!myEmploymentHistory.validateDate(uc2a))
			 {
				 error=6;
				 $(".line2ip1").addClass("errorTag");
				 validation=false;
			 }
			 else if(from > new Date())
			 {
			 error=10;
			 $(".line2ip1").addClass("errorTag");
			 validation=false;
			 }
			else
				{
				$(".line2ip1").removeClass("errorTag");
				}
			
			 
			 if((uc2b.length >0 && uc2b.trim() !="current") && !myEmploymentHistory.validateDate(uc2b))
			 {
				 error=6;
				 validation=false;
				 $(".line2ip2").addClass("errorTag");
			 }
			 else if(uc2b.length >0 && (uc2a == uc2b || from > to))
			 {
				 error=7;
			    $(".line2ip2").addClass("errorTag");
			    validation=false;
			 }
			 else{
				 $(".line2ip2").removeClass("errorTag");

			}
			if(uc3.length==0){
				error++;
				 $(".line3 input[type=text]").addClass("errorTag");
				validation=false;
			}
			else{
				 $(".line3 input[type=text]").removeClass("errorTag");

			}
			if(uc4.length==0){
				error++;
				 $(".line4 input[type=text]").addClass("errorTag");
				validation=false;
			}
			else{
				 $(".line4 input[type=text]").removeClass("errorTag");

			}
			if(error >0 && error < 6)
				{
			$('.errorText').text('All Fields are mandatory');
				}
			else if(error == 6 )
				{
				$('.errorText').text('Invalid date format (provide: dd/mm/yyyy)');
				}
			else if(error == 7 )
			{
			$('.errorText').text('To Date must be greater than From Date');
			}
			else if(error == 10 )
			{
			$('.errorText').text('From Date must be less than Todays Date');
			}
			if(validation) {
			
			$(".companyNameSkillsLabel").text("Skills with "+uc);
			$(".companyNameRolesLabel").text("Roles with "+uc);
			$(".companyNameToolsLabel").text("Tools & Systems with "+uc);
			$(".companyNameValue").text(uc);

			$.ajax({
				type : "POST",
				url : "ajax/saveCompany",
				data : {
					company : uc,
					df : uc2a,
					dt : uc2b,
					jobtitle : uc3,
					corptitle : uc4,
					achievements : uc5
				},
				success : function(response)
				{
					$('.browser_next input').val(1);
					var json=$.parseJSON(response);
					$(".employeRecords").html(json.company);
					$("#skillsWithCurrentCompany").html(json.currentCompanySkills);
					$("#rolesWithCurrentCompany").html(json.currentCompanyRoles);
					$("#toolsWithCurrentCompany").html(json.currentCompanyTools);
					$(".companyNameSkills").show();
					$(".companyNameRoles").show();
					$(".companyNameTools").show();
					$(".companyNameSkills").text("(Please select skills you used at your time with "+json.currentCompanyName+')');
					$(".companyNameRoles").text("(Please select roles you used at your time with "+json.currentCompanyName+')');
					$(".companyNameTools").text("(Please select tools & systems you used at your time with "+json.currentCompanyName+')');
					$('.prev').click(myEmploymentHistory.getPreviousEmployer);	
					$('.next').click(myEmploymentHistory.getNextEmployer);
					$(".em_box .edit_btn").click(myEmploymentHistory.editCompany);
					$(".em_box .plus_btn").click(myEmploymentHistory.addCompany);
					$(".em_box .trash_btn").click(myEmploymentHistory.deleteCompany);
					$(".em_box .save_btn").click(myEmploymentHistory.saveCompany);
				},
				error : function() {
				}
			});
	}else{
		$( ".datepicker-12" ).datepicker({
			yearRange     : "1900:+1",
			 changeMonth: true,
		     changeYear: true,
		     dateFormat: 'dd/mm/yy',
		     onClose: function(dateText, inst) { 
		            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		        }
		  });
		$(".plus_btn , .trash_btn , .edit_btn").hide();
		$(".save_btn").css({"left" : "0"}).show();
		$(".cancel_btn").css({"left" : "23px"}).show();
	}
		},

		getSkillsList : function() {
			
			
			var id = $(this).val();

			$.ajax({
						type : "GET",
						url : "ajax/skills?id=" + id,
						async : false,
						success : function(response) {
                        
							var htm = "";
							
							var result = jQuery.parseJSON(response);

							$.each(result,function(index, value) {
												htm = htm+ "<a href='javascript:void(0)' class='get-blue get-blue-skill' name="+value.id+">"
														+ value.skill + "</a>";
											});

					$(".links_skills").html(htm);
				},
				error : function() {
//					alert('Error occured');
				}
			});
			

		},
			getRolesList : function() {
				
				var id = $(this).val();

				$.ajax({
							type : "GET",
							url : "ajax/roles?id=" + id,
							async : false,
							success : function(response) {
	                        
								var htm = "";
								
								var result = jQuery.parseJSON(response);

								$.each(result,function(index, value) {
													htm = htm+ "<a href='javascript:void(0)' class='get-blue get-blue-role' name="+value.id+">"
															+ value.roleName + "</a>";
												});

						$(".roles").html(htm);
					},
					error : function() {
//						alert('Error occured');
					}
				});
			

		},
		getToolsList : function() {
			
			var id = $(this).val();

			$.ajax({
						type : "GET",
						url : "ajax/tools?id=" + id,
						async : false,
						success : function(response) {
                        
							var htm = "";
							
							var result = jQuery.parseJSON(response);

							$.each(result,function(index, value) {
												htm = htm+ "<a href='javascript:void(0)' class='get-blue get-blue-tool' name="+value.id+">"
														+ value.toolName + "</a>";
											});

					$(".tools").html(htm);
				},
				error : function() {
//					alert('Error occured');
				}
			});

	},

		deleteCompany : function() {
			
			
			var employerId=$('#employerId').val();
			if (employerId.length) {
			var isSure = confirm("Are you sure you want to delete the company!!!");
			if (isSure == true) {
	        
			$.ajax({
				type : "POST",
				url : "ajax/deleteCompany",
				data : {
					employerId : employerId
				},
				success : function() {
					location.reload();
				},
				error : function() {
				}
			});
			}
		}
		},
		
		getModalPopup:function(){
			// make error message empty while modal appers
			$('span.skill-Modal.errorMessage').text('');
			var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
			
			if (isCompany != '') {
			
			var skills=[];
			var skillIds=[];
			$(".links_skills a").each(function() {
				
				var skillClass=$(this).attr('class');
				
				 if(skillClass.indexOf("blue_link")!=-1){
					 
					 skills.push($(this).text());
					 skillIds.push($(this).attr('name'));
				 }
			});
             var companyName=$('.companyNameValue').text();
             var employerId = $("#employerId").val();
             var validation=true;
             if(skills.length ==0 )
            	 {
            	 $(this).parent().find('.errorMessage').text('Please select some skills from above Skills').show();
            	 validation=false;
            	 }
             if(validation)
            	 {
            	 $(this).parent().find('.errorMessage').text('');
			$.ajax({
				type : "GET",
				url : "ajax/skillsModalPopup",
				data:{'skills' : skills ,
					'companyName' : companyName,
					'candidateEmployerId' : employerId ,
					'skillIds' : skillIds ,
					'domaintype' :$('#selectbox1').val() 
					},
				datatype:'json',
				success : function(response) {
					$(".modal-body").html(response);
				},
				error : function() {
				}
			});
			
			$(".model_popup").modal();
			
			$(document).undelegate("#frm_details" , "submit");
			$(document).delegate("#frm_details","submit",function(event){
//				var domainType=$('#selectbox1').val();
				var  values = [];
				values = $(this).find('select');
				var isSelectAll = true;
//				alert(values.length);
				for (var i = 0; i < values.length; ++ i) {
				     if($(values[i]).val() == 'o' || $(values[i]).val() == '0'){
				    	 isSelectAll = false;
				    	 break;
				     }
				  }
				if (isSelectAll){
		        $.ajax({
		            url: "ajax/skillsSubmit",
		            type: "post",
		            data: $(this).serialize(),
		            success: function(response) {
					        
		            	var json = $.parseJSON(response);
		                $(".SkillBasket").html(json.overAllSkills);
						$(".skillsWithCurrentCompany").html(json.companySkills);
						$('#addskillBasket').keyup(myEmploymentHistory.addSkill);
						 
		            }
		        });
		        $(".model_popup").modal('hide');
		        $('.links_skills a').removeClass('blue_link');
		        event.preventDefault();
				} else {
					$('span.skill-Modal.errorMessage').text('Please select experience and Level ').show();
				}
				event.preventDefault();
			    });
            	 }
			} else {
				$(this).parent().find('.errorMessage').text('Please enter company details').show();
			}
			
		},

getRolesModalPopup:function(){
	// make error message empty while modal appers
	$('span.skill-Modal.errorMessage').text('');
	var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
	
	if (isCompany != '') {
		
			var roles=[];
			var rolesIds=[];
			$(".roles a").each(function() {
				
				var roleClass=$(this).attr('class');
				
				 if(roleClass.indexOf("blue_link")!=-1){
					 
					 roles.push($(this).text());
					 rolesIds.push($(this).attr('name'));
				 }
			});
             var companyName=$('.companyNameValue').text();
             var employerId = $("#employerId").val();
             var domainType=$('#selectbox1').val();
             var validation=true;
             if(roles.length ==0 )
            	 {
            	 $(this).parent().find('.errorMessage').text('Please select any roles from above roles').show();
            	 validation=false;
            	 }
             if(validation)
            	 {
            	 $(this).parent().find('.errorMessage').text('');
			$.ajax({
				type : "GET",
				url : "ajax/rolesModalPopup",
				data:{'objectValues' : roles ,
					'companyName':companyName,
					'candidateEmployerId' : employerId,
					'rolesIds' : rolesIds,
					'domainType' : domainType
					},
				datatype:'json',
				success : function(response) {
					$(".modal-body").html(response);
				},
				error : function() {
				}
			});
			
			$(".model_popup").modal();
			
			$(document).undelegate("#frm_roles_details" , "submit");
			$(document).delegate("#frm_roles_details","submit",function(event){
				var  values = [];
				values = $(this).find('select');
				var isSelectAll = true;
				for (var i = 0; i < values.length; ++ i) {
				     if($(values[i]).val() == 'o' || $(values[i]).val() == '0'){
				    	 isSelectAll = false;
				    	 break;
				     }
				  }
				if (isSelectAll){
		        $.ajax({
		            url: "ajax/rolesSubmit",
		            type: "post",
		            data: $(this).serialize(),
		            success: function(response) {
	                 
			             var json = $.parseJSON(response);
					               
		                $(".rolesBask").html(json.overAllRoles);
						$(".rolesWithCurrentCompany").html(json.companyRoles);
						$('#addroleBasket').keyup(myEmploymentHistory.addRole);
		            }
		        });
		        
		        $(".model_popup").modal('hide');
		        $('.links_jsonlyclick.roles a').removeClass('blue_link');
		        event.preventDefault();
				}  else {
					$('span.skill-Modal.errorMessage').text('Please select experience and level ').show();
					
				}
				event.preventDefault();
			    });
            	 }
	} else {
		$(this).parent().find('.errorMessage').text('Please enter company details').show();
	}
			
		},
		
getToolsModalPopup:function(){
	// make error message empty while modal appers
	$('span.skill-Modal.errorMessage').text('');
var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
	
	if (isCompany != '') {
			var tools=[];
			var toolsIds=[];
			$(".tools a").each(function() {
				
				var toolClass=$(this).attr('class');
				
				 if(toolClass.indexOf("blue_link")!=-1){
					 
					 tools.push($(this).text());
					 toolsIds.push($(this).attr('name'));
				 }
			});
             var companyName=$('.companyNameValue').text();
             var employerId = $("#employerId").val();
             var domainType=$('#selectbox1').val();
             var validation=true;
             if(tools.length ==0 )
            	 {
            	 $(this).parent().find('.errorMessage').text('Please select any tools from above Tools').show();
            	 validation=false;
            	 }
            
             if(validation)
            	 {
            	 $(this).parent().find('.errorMessage').text('');
			$.ajax({
				type : "GET",
				url : "ajax/toolsModalPopup",
				data:{'objectValues' : tools ,
					'companyName':companyName,
					'candidateEmployerId' : employerId,
					'toolsIds' : toolsIds,
					'domainType' : domainType
					},
				datatype:'json',
				success : function(response) {
					$(".modal-body").html(response);
				},
				error : function() {
				}
			});
			
			$(".model_popup").modal();
			$(document).undelegate("#frm_tools_details" , "submit");
			$(document).delegate("#frm_tools_details","submit",function(event){
				// check any of select exp or level is not selected ?
				var  values = [];
				values = $(this).find('select');
				var isSelectAll = true;
				for (var i = 0; i < values.length; ++ i) {
				     if($(values[i]).val() == 'o' || $(values[i]).val() == '0'){
				    	 isSelectAll = false;
				    	 break;
				     }
				  }
				if (isSelectAll){
					
		        $.ajax({
		            url: "ajax/toolsSubmit",
		            type: "post",
		            data: $(this).serialize(),
		            success: function(response) {
	                  
					      var json = $.parseJSON(response);
					  
		                $(".toolsBask").html(json.overAllTools);
						$(".toolsWithCurrentCompany").html(json.companyTools);
						$('#addtoolBasket').keyup(myEmploymentHistory.addTool);
		            }
		        });
		        
		        $(".model_popup").modal('hide');
		        $('.links_jsonlyclick.tools a').removeClass('blue_link');
		        
		        event.preventDefault();
				} else {
					
					$('span.skill-Modal.errorMessage').text('Please select experience and Level ').show();
					
				}
				event.preventDefault();
			    });
            	 }
	} else {
		$(this).parent().find('.errorMessage').text('Please enter company details').show();
	}
			
		},
		getNextEmployer:function(){
			// disable blue links buttons
			/*$("#selectbox1").val("0");
			$("#selectbox2").val("0");
			$("#selectbox3").val("0");
			$(".links_skills").html("");
			$(".roles").html("");
			$(".tools").html("");*/
			
			$('.links_skills a').removeClass('blue_link');
			$('.links_jsonlyclick.roles a').removeClass('blue_link');
			$('.links_jsonlyclick.tools a').removeClass('blue_link');
			
			var next=$(this).attr('id');

			var nextVal=parseInt(next);
			$.ajax({
				type : "GET",
				url : "ajax/nextCandidateEmployer?nextVal="+nextVal,
				success : function(response) {
					   var json=$.parseJSON(response);
						$(".employeRecords").html(json.company);
						$(".note-editable").html(json.achievements);
						$("#skillsWithCurrentCompany").html(json.currentCompanySkills);
						$("#rolesWithCurrentCompany").html(json.currentCompanyRoles);
						$("#toolsWithCurrentCompany").html(json.currentCompanyTools);
						$('.companyNameValue').html(json.currentCompanyName);
						$(".companyNameSkills").show();
						$(".companyNameRoles").show();
						$(".companyNameTools").show();
						$(".companyNameSkills").text("(Please select skills you used at your time with "+json.currentCompanyName+')');
						$(".companyNameRoles").text("(Please select roles you used at your time with "+json.currentCompanyName+')');
						$(".companyNameTools").text("(Please select tools & systems you used at your time with "+json.currentCompanyName+')');
						$('.prev').click(myEmploymentHistory.getPreviousEmployer);
						$('.next').click(myEmploymentHistory.getNextEmployer);
						$(".em_box .edit_btn").click(myEmploymentHistory.editCompany);
						$(".em_box .plus_btn").click(myEmploymentHistory.addCompany);
						$(".em_box .trash_btn").click(myEmploymentHistory.deleteCompany);
						$(".em_box .save_btn").click(myEmploymentHistory.saveCompany);


				},
				error : function() {
				}
			});
			
		},
		
		addSkill : function(event) {
			$('.links_but.text-right .errorMessage').hide();
			
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
				
				$('span.skill-Modal.errorMessage').text('');
				var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
				
				if (isCompany != '') {
					var category = $('#selectbox1').val();
						// checking catagory selected or not
						if (category != 0) {
								
								if (text.length) {
									$(this).parent().find('.errorMessage').text('Please enter company details').hide()
										$.ajax({
											url : "ajax/addskill",
											type : 'POST',
											data : {
												addskill : text,
												employerID:$('#employerId').val(),
												domainType:$('#selectbox1').val()
											},
											success : function(response) {
												
													 var json = $.parseJSON(response);
										               
										                $(".SkillBasket").html(json.overAllSkills);
										                $('#addskillBasket').keyup(myEmploymentHistory.addSkill);
										                $(".skillsWithCurrentCompany").html(json.companySkills);
													    $('#addskillBasket').val('');
													   
													    $('#addskillBasket').autocomplete({
													    	source : listOfSkills
													    });
													    
											},
											error : function(error) {
				//								alert("error occur during saving company");
											}
										});
								}
						} else {
							$(this).parent().find('.errorMessage').text('Please enter skill category').show();
						}// eo catagory check
		
				} else {
					$(this).parent().find('.errorMessage').text('Please enter company details').show();
				}//eo company check
				
			}// eo key()
		},
		
		addRole : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
				$('span.skill-Modal.errorMessage').text('');
				var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
				
				if (isCompany != '') {
						var category = $('#selectbox2').val();
						
						// checking catagory selected or not
						if (category != 0) {
						
							if (text.length) {
												$(this).parent().find('.errorMessage').text('Please enter company details').hide()
											$.ajax({
														url : "ajax/addrole",
														type : 'POST',
														data : {
															addRole : text,
															employerID:$('#employerId').val(),
															domainType:$('#selectbox1').val()
														},
														success : function(response) {
															
																 var json = $.parseJSON(response);
													               
													                $(".rolesBask").html(json.overAllRoles);
													                $('#addroleBasket').keyup(myEmploymentHistory.addRole);
													                $(".rolesWithCurrentCompany").html(json.companyRoles);
																	
																    $('#addroleBasket').val('');
																    $('#addroleBasket').autocomplete({
																    	source : listOfRoles
																    });
														},
														error : function(error) {
							//								alert("error occur during saving company");
														}
													});
											}
					}else {
						$(this).parent().find('.errorMessage').text('Please enter roles category').show();
					}
					}else {
						$(this).parent().find('.errorMessage').text('Please enter company details').show();
					}
		}

		},
		
		addTool : function(event) {
			var text = $(this).val().trim();
			if (event.keyCode == 13) {
					$('span.skill-Modal.errorMessage').text('');
					var isCompany = $('.employeRecords .em_box_details').find('.line1').text();
					
					if (isCompany != '') {
						var category = $('#selectbox3').val();
						
						// checking catagory selected or not
							if (category != 0) {
								if (text.length) {
									$(this).parent().find('.errorMessage').text('Please enter company details').hide();
										$.ajax({
													url : "ajax/addtool",
													type : 'POST',
													data : {
														addtool : text,
														employerID:$('#employerId').val(),
														domainType:$('#selectbox1').val()
													},
													success : function(response) {
														
															 var json = $.parseJSON(response);
												               
												                $(".toolsBask").html(json.overAllTools);
												                $(".toolsWithCurrentCompany").html(json.companyTools);
												                $('#addtoolBasket').keyup(myEmploymentHistory.addTool);
																
															    $('#addtoolBasket').val('');
															    
															    $('#addtoolBasket').autocomplete({
															    	source : listOfTools
															    });
													},
													error : function(error) {
						//								alert("error occur during saving company");
													}
											});
								}
						}else {
							$(this).parent().find('.errorMessage').text('Please enter Tools category').show();
						}
					}else {
						$(this).parent().find('.errorMessage').text('Please enter company details').show();
					}
		}

		},

		
		getPreviousEmployer:function(){
			//disable the blue link buttons 
			/*$("#selectbox1").val("0");
			$("#selectbox2").val("0");
			$("#selectbox3").val("0");
			$(".links_skills").html("");
			$(".roles").html("");
			$(".tools").html("");*/
			$('.links_skills a').removeClass('blue_link');
			$('.links_jsonlyclick.roles a').removeClass('blue_link');
			$('.links_jsonlyclick.tools a').removeClass('blue_link');
			
			var prev=$('.prev').attr('id');
			$.ajax({
				type : "GET",
				url : "ajax/nextCandidateEmployer?previousVal="+prev,
				success : function(response) {
					    var json=$.parseJSON(response);
						$(".employeRecords").html(json.company);
						$(".note-editable").html(json.achievements);
						$("#skillsWithCurrentCompany").html(json.currentCompanySkills);
						$("#rolesWithCurrentCompany").html(json.currentCompanyRoles);
						$("#toolsWithCurrentCompany").html(json.currentCompanyTools);
						$('.companyNameValue').html(json.currentCompanyName);
						$(".companyNameSkills").show();
						$(".companyNameRoles").show();
						$(".companyNameTools").show();
						$(".companyNameSkills").text("(Please select skills you used at your time with "+json.currentCompanyName+')');
						$(".companyNameRoles").text("(Please select roles you used at your time with "+json.currentCompanyName+')');
						$(".companyNameTools").text("(Please select tools & systems you used at your time with "+json.currentCompanyName+')');
						$('.prev').click(myEmploymentHistory.getPreviousEmployer);	
						$('.next').click(myEmploymentHistory.getNextEmployer);
						$(".em_box .edit_btn").click(myEmploymentHistory.editCompany);
						$(".em_box .plus_btn").click(myEmploymentHistory.addCompany);
						$(".em_box .trash_btn").click(myEmploymentHistory.deleteCompany);
						$(".em_box .save_btn").click(myEmploymentHistory.saveCompany);


				},
				error : function() {
				}
			});
			
		},
    getDATA : function(id, ajaxUrl) {

        var list = [];

        $.ajax({
            url : ajaxUrl,
            type : "GET",
            success : function(data) {

            	list = data;
                $(id).autocomplete({
                    source : list
                });
               

            },
            error : function(err) {
//                alert("err: " + err);
            }
        });
        return list;

    }
		
		
		
	};

}();


$(document).ready(function() {

	myEmploymentHistory.init();
	
	$( '#editor1').summernote({
	    height: 207
	  });
	
	 $('#achievements .note-editor .note-style').css('display','none');
     $('#achievements .note-editor .note-height').css('display','none');
     $('#achievements .note-editor .note-table ').css('display','none');
     $('#achievements .note-editor .note-insert').css('display','none');
     $('#achievements .note-editor .note-view').css('display','none');
     $('#achievements .note-editor .note-help').css('display','none');
     $('#achievements .note-editor .note-statusbar').css('display','block');
     
     $('.note-toolbar').css('display','none');
     $('.note-editor .note-statusbar').css('display','none'); 
     
     $(' #achievements').mouseover(function(e){
         $('#achievements .note-toolbar').css('display','block');
         
         $('#achievements .note-editor .note-style').css('display','none');
         $('#achievements .note-editor .note-height').css('display','none');
         $('#achievements .note-editor .note-table ').css('display','none');
         $('#achievements .note-editor .note-insert').css('display','none');
         $('#achievements .note-editor .note-view').css('display','none');
         $('#achievements .note-editor .note-help').css('display','none');
         $('#achievements .note-editor .note-editable').css('height','180');
         $('#achievements .note-editor .note-statusbar').css('display','block');
         
     });
     $('#achievements').mouseout(function(e){

         $('#achievements .note-toolbar').css('display','none');
         $('#achievements .note-editor .note-statusbar').css('display','none');
         $('#achievements .note-editor .note-editable').css('height','227');
         
     });
     
    /* if (isCompany != '') {
			var category = $('#selectbox3').val();
     $(".links_skills").on("click",'a',function(){
		$(this).toggleClass("blue_link");
	});
     
     $(".roles").on("click",'a',function(){
 		$(this).toggleClass("blue_link");
 	});
     $(".tools").on("click",'a',function(){
 		$(this).toggleClass("blue_link");
 	});*/
	
	$( ".datepicker-12" ).datepicker({
		yearRange     : "1900:+1",
		 changeMonth: true,
	     changeYear: true,
	     dateFormat: 'dd/mm/yy',
	     onClose: function(dateText, inst) { 
	            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        }
	  });
	
	$(".SkillBasket").on("click","a",function(e){
		if(myEmploymentHistory.returnCompanyName('').length){
		var skillId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var skillName=$(this).attr('name');
		var removeSkill=$(this);
		
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateSkill?skillId="+skillId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeSkill.remove();
						$(".skillsWithCurrentCompany a").each(function() {
							 if($(this).attr('name').indexOf(skillName)!=-1){
								 $(this).remove();
							 }
						});
					},
					error : function() {
					}
				});
		  }
		  
		  else{
				$.ajax({
					type : "POST",
					url : "ajax/currentCompanySkill?skillId="+skillId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						var result=response;
						if(result==1){
						$('.skillsWithCurrentCompany').append("<a href='javascript:void(0)' name='"+skillName+"'id='"+skillId+"'>"+skillName+"<i class='fa fa-times'></i></a>");
					
						}
						},
					error : function() {
					}
				});
		  }
	}
		  });
	$(".rolesBask").on("click","a",function(e){
		if(myEmploymentHistory.returnCompanyName('').length){
		var roleId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var roleName=$(this).attr('name');
		var removeRole=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateRole?roleId="+roleId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeRole.remove();
						$(".rolesWithCurrentCompany a").each(function() {
							 if($(this).attr('name').indexOf(roleName)!=-1){
								 $(this).remove();
							 }
						});
					},
					error : function() {
					}
				});
			  
		  }
		  else{
			$.ajax({
				type : "POST",
				url : "ajax/currentCompanyRole?roleId="+roleId+"&employerId="+employerId,
				datatype:'json',
				success : function(response) {
					var result=response;
					if(result==1){
					  $('.rolesWithCurrentCompany').append("<a href='javascript:void(0)' name='"+roleName+"'id='"+roleId+"'>"+roleName+"<i class='fa fa-times'></i></a>");
					}
					},
				error : function() {
				}
			});
	    }
	}
	});
	
	$(".toolsBask").on("click","a",function(e){
		if(myEmploymentHistory.returnCompanyName('').length){
		var toolId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var toolName=$(this).attr('name');
		var removeTool=$(this);
		
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateTool?toolId="+toolId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeTool.remove();
						$(".toolsWithCurrentCompany a").each(function() {
							 if($(this).attr('name').indexOf(toolName)!=-1){
								 $(this).remove();
							 }
						});
					},
					error : function() {
					}
				});
			  
		  }else{
			  
			$.ajax({
				type : "POST",
				url : "ajax/currentCompanyTool?toolId="+toolId+"&employerId="+employerId,
				datatype:'json',
				success : function(response) {
					var result=response;
					if(result==1){
					$('.toolsWithCurrentCompany').append("<a href='javascript:void(0)' name='"+toolName+"'id='"+toolId+"'>"+toolName+"<i class='fa fa-times'></i></a>");
					}
					},
				error : function() {
				}
		  });
		 }
	}
	});
	$("#skillsWithCurrentCompany").on("click","a",function(e){
		var skillId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var removeCompanySkill=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateCompanySkill?skillId="+skillId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeCompanySkill.remove();
					},
					error : function() {
					}
				});
		  }
	});
	
	$("#rolesWithCurrentCompany").on("click","a",function(e){
		var roleId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var removeCompanyRole=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateCompanyRole?roleId="+roleId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeCompanyRole.remove();
					},
					error : function() {
					}
				});
		  }
	});

	$("#toolsWithCurrentCompany").on("click","a",function(e){
		var toolId=$(this).attr('id');
		var employerId=$('#employerId').val();
		var removeCompanyTool=$(this);
		e.preventDefault();
		  if($(e.target).is('.fa-times'))
		  {
			  $.ajax({
					type : "POST",
					url : "ajax/deleteCandidateCompanyTool?toolId="+toolId+"&employerId="+employerId,
					datatype:'json',
					success : function(response) {
						removeCompanyTool.remove();
					},
					error : function() {
					}
				});
		  }
	});
	$(".skills_box .hear").click(function(){
		$(this).toggleClass("hear_plus");
		$(this).parent().toggleClass("skills_boxCL")
		});
	
    $.ajax({
        url : "ajax/getEmployerNames",
        type : "GET",
        success : function(data) {
            listOfData = data;
            $('.em_box_details .line1 .pp_text').autocomplete({
                source : listOfData
            });
            /*$('#input_FC').autocomplete({
                source : listOfData
            });
            $('#input_ExC').autocomplete({
                source : listOfData
            });*/
        },
        error : function(err) {
        }
    });
//  var listOfRoles = myEmploymentHistory.getDATA("#addroleBasket", "ajax/getTools");
    
//  var listOfTools = myEmploymentHistory.getDATA("#addtoolBasket", "ajax/getRoles");
    $.ajax({
        url : "ajax/getSkills",
        type : "GET",
        success : function(data) {
        	listOfSkills = data;
            $('#addskillBasket').autocomplete({
                source : listOfSkills
            });
            $('.addNewSkill').autocomplete({
                source : listOfSkills
            });

        },
        error : function(err) {
        }
    });
    
    $.ajax({
        url : "ajax/getRoles",
        type : "GET",
        success : function(data) {
        	listOfRoles = data;
            $('#addroleBasket').autocomplete({
                source : listOfRoles
            });
            $('.addNewRole').autocomplete({
                source : listOfRoles
            });

        },
        error : function(err) {
        }
    });
    
    $.ajax({
        url : "ajax/getTools",
        type : "GET",
        success : function(data) {
        	listOfTools = data;
            $('#addtoolBasket').autocomplete({
                source : listOfTools
            });
            $('.addNewTool').autocomplete({
                source : listOfTools
            });

        },
        error : function(err) {
        }
    });
    
	$(".browser_next").on("click","a",function(event){
		 
		var isCompany = $('.browser_next input').val();
		if (isCompany > 0) {
			
		}
		else{
			$('#companyNo').text('Please enter company details').show();
			event.preventDefault();
		}
    });
    

});






