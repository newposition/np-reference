$(document).ready(function() {
			
				$("select#sortNpClients").on('change',function(event) {
							var sortBy = $(this).val();
							window.open('clientUserManagement?sortBy='+sortBy,"_self");
						});

				$(".suspendClient").on(
						'click',
						function(event) {
							var id = $(this).parent().parent().parent().find(
									'.tbl-user span').text();
							$('input[name="userId"]').val(id);
							$('.modal-body select[name="statusId"]').val($(this).attr("id"));
							var name = $(this).parent().parent().parent().find(
							'.tbl-user.clientUserName').text();
							$('.modal-header span#clientName').text(name).show(); 
						});

				$(".deleteClient").on(
						'click',
						function(event) {
							var id = $(this).parent().parent().parent().find(
									'.tbl-user span').text();
							$('input[name="userId"]').val(id);

						});
	$(".pagination").on(
			'click',
			function(event) {
				event.preventDefault();
				var pageNumber = $(this).text()-1;
				var sortBy=$('#sortNpClients').val();
				var domainFilter=$('#domainFilter').val();
				var skillTypeFilter=$('#skillTypeFilter').val();
				var	validFilter=$('#validFilter').val();
				window.open('clientUserManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&skillTypeFilter="+skillTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
				
			});
	
	$(".prev").on(
			'click',
			function(event) {
				event.preventDefault();
				var pageNumber = $('.currentPageNo').text()-2;
				var sortBy=$('#sortNpClients').val();
				window.open('clientUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
				
			});
	
	$(".next").on(
			'click',
			function(event) {
				event.preventDefault();
				var pageNumber = $('.currentPageNo').text();
				var sortBy=$('#sortNpClients').val();
				window.open('clientUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
				
			});
	
	$(".fristPagination").on(
			'click',
			function(event) {
				event.preventDefault();
				var pageNumber = 0;
				var sortBy=$('#sortNpClients').val();
				window.open('clientUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
				
			});
	
	$(".lastPagination").on(
			'click',
			function(event) {
				event.preventDefault();
				var pageNumber = $(this).attr("href");
				var sortBy=$('#sortNpClients').val();
				window.open('clientUserManagement?sortBy='+sortBy+"&pageNumber="+pageNumber,"_self");
				
			});
	
	$("form#form-contact").submit(function(event){
		var status = $(this).find("select#statusId").val();
		if (status == 0) {
			event.preventDefault();
			$(this).find("select#statusId").css("background-color", "#e8a7a7"); 
		}
		
	});
	$("#myModalSuspend.modal").on('hidden.bs.modal', function (event) {
		event.preventDefault();
		
	});
			
});


 