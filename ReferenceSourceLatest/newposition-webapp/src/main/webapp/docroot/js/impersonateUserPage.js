var impersonateUser = function() {

	return {
		init : function() {
			$("#sortBy").on(
					'change',
					function(event) {
						var filterBy=$('#filterBy').val();
						var pageNumber=0;
						var sortBy = $(this).val();
						if (sortBy.indexOf("Sort") == -1) {
							window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self", "_self");
						}
					});
			$(".btn.btn-rd-cor").on( 'click', function(event) {
				
				var fullName = $(this).parents(".tbl_panel").find("input[name=userName]").val();
				var email = $(this).parents(".tbl_panel").find("input[name=userEmail]").val();
				var userId = $(this).parents(".tbl_panel").find("input[name=userId]").val();
//				alert(fullName+' '+email+' '+userId);
				$("span#modal_username").text(fullName);
				$("span#modal_useremail").text(email);
				$(".modal-body input[name=userId]").val(userId);
				
			});
			$(".linkIm").click(function(){
				$('#impersonateLink').attr("href", "switchUser?j_username="+$("span#modal_useremail").text());
			});
			
			/**
			 * 
			 *  pagination
			 *  
			 * */
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortBy').val();
						var filterBy=$('#filterBy').val();
						window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						
					});
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortBy').val();
						var filterBy=$('#filterBy').val();
						window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortBy').val();
						var filterBy=$('#filterBy').val();
						window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortBy').val();
						var filterBy=$('#filterBy').val();
						window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortBy').val();
						var filterBy=$('#filterBy').val();
						window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						
					});
			
			$("#filterBy").on(
					'change',
					function(event) {
						var sortBy = $('#sortBy').val();
						var pageNumber = 0;
						var filterBy = $(this).val();
						if (filterBy != "") {
							window.open('impersonate_user?sortBy='+sortBy+"&pageNumber="+pageNumber+"&filterBy="+filterBy,"_self");
						}
					});
		}
	};
}();

$(document).ready(function() {

	impersonateUser.init();

});