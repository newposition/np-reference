$(document).ready(function() {
	$("#instituteFilter").on(
			'change',
			function(event) {
				var instituteFilter = $(this).val();
				var sortBy=$('#sortNpInstitutes').val();
				var instituteTypeFilter=$('#instituteTypeFilter').val();
				var	validFilter=$('#validFilter').val();
//					window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&institutionTypeFilter="+institutionTypeFilter+"&validFilter="+validFilter,"_self");
				if (instituteFilter.length != 0) {
				window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter,"_self");
				}
			});
	
	$("#instituteTypeFilter").on(
			'change',
			function(event) {
				var instituteTypeFilter = $(this).val();
				var sortBy=$('#sortNpInstitutes').val();
				var instituteFilter=$('#instituteFilter').val();
				var	validFilter=$('#validFilter').val();
				if (instituteTypeFilter.length != 0) {
					window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter,"_self");
				}
				});
	
	$("#validFilter").on(
			'change',
			function(event) {
				var sortBy=$('#sortNpInstitutes').val();
				var instituteFilter=$('#instituteFilter').val();
				var instituteTypeFilter=$('#instituteTypeFilter').val();
				var	validFilter=$(this).val();
				if (validFilter.length != 0) {
					window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter,"_self");
				}
			});
	
	$("#sortNpInstitutes").on(
			'change',
			function(event) {
				var sortBy = $(this).val();
				var instituteFilter=$('#instituteFilter').val();
				var instituteTypeFilter=$('#instituteTypeFilter').val();
				var	validFilter=$('#validFilter').val();
				if(sortBy!='Sort by')
					{
					window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter,"_self");
					}
			});
	
    function getSearchData(searchCode,targetId,currentModal) {
		$.ajax({
	         type : "POST",
           url : "ajax/institutionManagement/aliasSearch?searchCode="+searchCode+"&mergeId="+targetId,
           success : function(response) {
              if(response!= undefined)
           	   {
            	  $("div#searchResults").html(response);
           	   }
           }, 
           complete : function(){
               //code to update the height of modal backdrop -start
               var $modal        = currentModal.parents(".modal");
                var $backdrop     = $('.modal-backdrop');
                var $modalContent = $modal.find('.modal-dialog');
                var $modalHeight  = $modalContent.height();
                     $backdrop.css({
                        height: $modalHeight * 1.1
                        })
              //code to update the height of modal backdrop - end       
         },
           error : function() {
           }
       });
		
	}
		 $('.quickEntryFilter input').keyup(function(event){
           
                if (event.keyCode == 13) {
                	var searchCode=$(this).val();
//                	var search=$(this).parent().parent();
                	var targetModal=$(this).parents(".modal").attr("id");
                	var targetId=$("#"+targetModal+" #addNpinstitutionsForm #instituteId").val();
                	if(searchCode.length)
   						{
                        getSearchData(searchCode,targetId,$(this));
   						}  
                }
            });
		 
		 $('.quickEntryFilter label').on("click",function(event){
             event.preventDefault();
             var searchCode=$(this).parent().find("input#qSearch").val();
             var targetModal=$(this).parents(".modal").attr("id");
             var targetId=$("#"+targetModal+" #addNpinstitutionsForm #instituteId").val();
             if(searchCode.length)
             		{
             			getSearchData(searchCode,targetId,$(this));
             		}     
        });
		 
		 
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpInstitutes').val();
						var instituteFilter=$('#instituteFilter').val();
						var instituteTypeFilter=$('#instituteTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpInstitutes').val();
						var instituteFilter=$('#instituteFilter').val();
						var instituteTypeFilter=$('#instituteTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpInstitutes').val();
						var instituteFilter=$('#instituteFilter').val();
						var instituteTypeFilter=$('#instituteTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpInstitutes').val();
						var instituteFilter=$('#instituteFilter').val();
						var instituteTypeFilter=$('#instituteTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpInstitutes').val();
						var instituteFilter=$('#instituteFilter').val();
						var instituteTypeFilter=$('#instituteTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('institutionManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&instituteTypeFilter="+instituteTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
		 /*$(".aliasSource").on(
					'change',
					function(event) {
						var targetModal=$(this).parents(".modal").attr("id");
				   		var index=$("#"+targetModal+" #addNpinstitutionsForm #coInstituteAliasIndex").val();
//		var index= $(this).parents().find("#coCertificationAliasIndex").val();
		//alert(index);
//		$(this).parent().append("<input type='text' name='certificationAliases["+index+"].aliasName' id='Text20'  placeholder='Alias Certification Name' class='pp_text' />");
			
		 var form;
		 form="<div class='form-group'>";
         form=form+"<div class='row'>";
         form=form+"<div class='col-md-4 col-xs-6'>";
         form=form+"<input type='hidden'  name='certificationAliases["+index+"].aliasId'>";
         form=form+"<input type='text' class='pp_text'  placeholder='Alias Certification Name' name='certificationAliases["+index+"].aliasName' id='Text20'>";

         form=form+"   </div>";
         form=form+"    <div class='col-md-5'>";
         form=form+"      <select class='form-control selectbox aliasSource' name='certificationAliases["+index+"].aliasSource' id='Select22'>";
         form=form+"       <option value=''>Select CertificationType</option>";
         form=form+"         <option value='1' selected='selected'>sourcename1</option>";
         form=form+"		 </select>";
         form=form+"    </div>";
         form=form+"   <div class='col-md-3 col-xs-6'>";
         form=form+"        <button class='btn btn-edt' name='btn-send' id='Button1'><span class='fa fa-pencil-square-o'></span></button>";
         form=form+"       <button class='btn btn-edt pull-right' name='btn-send' id='Button2'><span class='fa fa-trash'></span></button>";
         form=form+"    </div>";
         form=form+"</div>";
         form=form+"</div>";
         $(this).parent().parent().parent().parent().append(form);
         event.preventDefault();
		 });
		 */
		 $('.btn-edt').on('click', '#editCoInstitute', function(event){
			  //alert($(this).parents('.form-group').attr("class"));
			  var parents= $(this).parents('.form-group'); // find the parent row
//			  //alert(parents.find("input.pp_text:not(:disabled)").length)
			  $(this).parents('.form-group').children("input").removeAttr('disabled');// find text elements in that row
			           
			      parents.find(".aliasSource") // find select elements in that row
		       .attr('disabled',false);
			   event.preventDefault();

		});
       
  
       
       $(".removeId").on(
   			'click',
   			function(event) {
   				var isSure = confirm("Are you sure you want to delete the Institution !!!");
				if (isSure) {
   				var targetModal=$(this).parents(".modal").attr("id");
   		   		var targetId=$("#"+targetModal+" #addNpinstitutionsForm #instituteId").val();
   				$.ajax({
   		         type : "POST",
   		         url : "ajax/institutionManagement/removeNpInstitute?npInstituteId="+targetId,
   		         success : function(response) {
   		            
   		        	 window.location='institutionManagement?showDelMsg=true';
   		         },
   		         error : function() {
   		         }
   		     });
				}
				else
					{
					 event.preventDefault();
					}
   			});
       
       $(".pull-right").on(
      			'click','#removeCoInstitute',
      			function(event) {
//      				var targetModal=$(this).parents(".modal").attr("id");
      		   		var targetId=$(this).parent().attr("id");
      				//alert(targetId);
      				$.ajax({
      		         type : "POST",
      		         url : "ajax/institutionManagement/removeCoInstitute?coInstituteAliasId="+targetId,
      		         success : function(response) {
      		            
      		        	 window.location='institutionManagement';
      		         },
      		         error : function() {
      		             //alert('error');
      		         }
      		     });
      			});
       
       $("form#addNpInstitutionsForm").on("submit",function(event){
           var instituteName = $(this).find("input[name=instituteName]").val();
           var instituteType = $(this).find("select[name=instituteType]").val();
           $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
           var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
           for(var i=0;i<rows;i++)
        	   {
        	   var aliasSource=$(this).find(".aliasSource"+i).val();
        	  var aliasName=$(this).find(".aliasName"+i).val();
        	   if(aliasName.length > 0 && aliasSource == "")
        	   {
        	  $("span#errPop").text("Please choose Alias Source").show();
        	 event.preventDefault();
        	   }
        	   else if(aliasName.length == 0 && aliasSource > 0)
       	   {
       	  $("span#errPop").text("Please enter Alias Name").show();
       	 event.preventDefault();
       	   }
        	   }
           if (instituteName.length == 0) {
               $("span#errPop").text("Please enter Institute Name").show();
               event.preventDefault();
           }
           else if(instituteType.length== 0) {
               $("span#errPop").text("Please choose Institute type").show();
               event.preventDefault();
           }
       });
       
       $(".edit-icon").on("click",function(event){
           var modal=$(this).attr("data-target");
           var id=$(this).attr("id");
           var form=$(modal+" input[name=instituteId]").val();
           if(form != '' && form != undefined)
             {
                 $.ajax({
                     type : "POST",
                     url : "ajax/institutionManagement/editAlias?id="+form,
                     success : function(response) {
                         if(response != undefined && response!= "")
                             {
                         $(modal+" .coAliasSection").html(response);
                             }
                     },
                     error : function() {
                     }
                     
                 });
             }
           $(modal +" select[name='instituteType']").val($("#instituteType"+id).val());
           $(modal +" input[name='instituteName']").val($("#instituteName"+id).val());
          var valid=$("#valid"+id).val();
           if(valid.trim() == "true")
               {
           $(modal + " #radio1").prop('checked', true);
           $(modal + " #radio2").prop('checked', false);
               }
           if(valid.trim() == "false")
               {
               $(modal + " #radio2").prop('checked', true);
               $(modal + " #radio1").prop('checked', false);
               }
         });
       
       $(".btn-rd-cor").on("click",function(event){
       $("#myModalAdd input[name='instituteName']").val("");
       $("#myModalAdd select[name='instituteType']").val($("#myModalAdd select[name='instituteType'] option[val='']").val());
       $("#myModalAdd #radio1").prop('checked', true);
       $("#myModalAdd #radio2").prop('checked', false);
       $.ajax({
			type : "POST",
			url : "ajax/roleManagement/addAalias?index="+0,
			success : function(data) {
				
				$("#myModalAdd .coAliasSection").html($(data).html());
			},
			error : function() {
			}
			
		});
          });

});


 