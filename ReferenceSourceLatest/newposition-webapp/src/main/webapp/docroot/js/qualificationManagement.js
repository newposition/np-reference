$(document).ready(function() {
	$("#instituteFilter").on(
			'change',
			function(event) {
				var instituteFilter = $(this).val();
				var sortBy=$('#sortNpCourses').val();
				var courseTypeFilter=$('#courseTypeFilter').val();
				var	validFilter=$('#validFilter').val();
//					window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&qualificationTypeFilter="+qualificationTypeFilter+"&validFilter="+validFilter,"_self");
				if (instituteFilter.length != 0) {
				window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter,"_self");
				}
			});
	
	$("#courseTypeFilter").on(
			'change',
			function(event) {
				var courseTypeFilter = $(this).val();
				var sortBy=$('#sortNpCourses').val();
				var instituteFilter=$('#instituteFilter').val();
				var	validFilter=$('#validFilter').val();
				if (courseTypeFilter.length != 0) {
					window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter,"_self");
				}
			});
	
	$("#validFilter").on(
			'change',
			function(event) {
				var sortBy=$('#sortNpCourses').val();
				var instituteFilter=$('#instituteFilter').val();
				var courseTypeFilter=$('#courseTypeFilter').val();
				var	validFilter=$(this).val();
				if (validFilter.length != 0) {
					window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter,"_self");
				}
				});
	
	$("#sortNpCourses").on(
			'change',
			function(event) {
				var sortBy = $(this).val();
				var instituteFilter=$('#instituteFilter').val();
				var courseTypeFilter=$('#courseTypeFilter').val();
				var	validFilter=$('#validFilter').val();
				if(sortBy!='Sort by')
					{
					window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter,"_self");
					}
			});
	
	
    function getSearchData(searchCode,targetId,currentModal) {
		$.ajax({
            type : "POST",
            url : "ajax/qualificationManagement/aliasSearch?searchCode="+searchCode+"&mergeId="+targetId,
            success : function(response) {
               if(response!= undefined)
            	   {
            	   $("div#searchResults").html(response);
            	   }
            },
            complete : function(){
                //code to update the height of modal backdrop -start
                 var $modal        = currentModal.parents(".modal");
                  var $backdrop     = $('.modal-backdrop');
                  var $modalContent = $modal.find('.modal-dialog');
                  var $modalHeight  = $modalContent.height();
                       $backdrop.css({
                          height: $modalHeight * 1.1
                          })
                //code to update the height of modal backdrop - end       
           },
            error : function() {
            }
        });
	}
		 $('.quickEntryFilter input').keyup(function(event){
           
                if (event.keyCode == 13) {
                	var searchCode=$(this).val();
                	var search=$(this).parent().parent();
                	var targetModal=$(this).parents(".modal").attr("id");
                	var targetId=$("#"+targetModal+" #addNpQualificationsForm #npInstituteCourseId").val();
                	if(searchCode.length)
                		{
                        getSearchData(searchCode,targetId,$(this));
                		}
                }
                event.preventDefault();
            });
		 
		 $('.quickEntryFilter label').on("click",function(event){
             event.preventDefault();
             var searchCode=$(this).parent().find("input#qSearch").val();
             var targetModal=$(this).parents(".modal").attr("id");
             var targetId=$("#"+targetModal+" #addNpQualificationsForm #npInstituteCourseId").val();
             if(searchCode.length)
     			{
     	       getSearchData(searchCode,targetId,$(this));
     			}
        });
		 
		 
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpCourses').val();
						var instituteFilter=$('#instituteFilter').val();
						var courseTypeFilter=$('#courseTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpCourses').val();
						var instituteFilter=$('#instituteFilter').val();
						var courseTypeFilter=$('#courseTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpCourses').val();
						var instituteFilter=$('#instituteFilter').val();
						var courseTypeFilter=$('#courseTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpCourses').val();
						var instituteFilter=$('#instituteFilter').val();
						var courseTypeFilter=$('#courseTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpCourses').val();
						var instituteFilter=$('#instituteFilter').val();
						var courseTypeFilter=$('#courseTypeFilter').val();
						var	validFilter=$('#validFilter').val();
							window.open('qualificationManagement?sortBy='+sortBy+"&instituteFilter="+instituteFilter+"&courseTypeFilter="+courseTypeFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					
					});
		 /*$(".aliasSource").on(
					'change',
					function(event) {
						var targetModal=$(this).parents(".modal").attr("id");
				   		var index=$("#"+targetModal+" #addNpQualificationsForm #coCourseAliasIndex").val();
//		var index= $(this).parents().find("#coCertificationAliasIndex").val();
		alert(index);
//		$(this).parent().append("<input type='text' name='certificationAliases["+index+"].aliasName' id='Text20'  placeholder='Alias Certification Name' class='pp_text' />");
			
		 var form;
		 form="<div class='form-group'>";
         form=form+"<div class='row'>";
         form=form+"<div class='col-md-4 col-xs-6'>";
         form=form+"<input type='hidden'  name='certificationAliases["+index+"].aliasId'>";
         form=form+"<input type='text' class='pp_text'  placeholder='Alias Certification Name' name='certificationAliases["+index+"].aliasName' id='Text20'>";

         form=form+"   </div>";
         form=form+"    <div class='col-md-5'>";
         form=form+"      <select class='form-control selectbox aliasSource' name='certificationAliases["+index+"].aliasSource' id='Select22'>";
         form=form+"       <option value=''>Select CertificationType</option>";
         form=form+"         <option value='1' selected='selected'>sourcename1</option>";
         form=form+"		 </select>";
         form=form+"    </div>";
         form=form+"   <div class='col-md-3 col-xs-6'>";
         form=form+"        <button class='btn btn-edt' name='btn-send' id='Button1'><span class='fa fa-pencil-square-o'></span></button>";
         form=form+"       <button class='btn btn-edt pull-right' name='btn-send' id='Button2'><span class='fa fa-trash'></span></button>";
         form=form+"    </div>";
         form=form+"</div>";
         form=form+"</div>";
         $(this).parent().parent().parent().parent().append(form);
         event.preventDefault();
		 });
		 */
		 $('.btn-edt').on('click', '#editCoCourse', function(event){
//			  alert($(this).parents('.form-group').attr("class"));
			  var parents= $(this).parents('.form-group'); // find the parent row
//			  alert(parents.find("input.pp_text:not(:disabled)").length)
			  $(this).parents('.form-group').children("input").removeAttr('disabled');// find text elements in that row
			           
			      parents.find(".aliasSource") // find select elements in that row
		       .attr('disabled',false);
			   event.preventDefault();

		});
       
  
       
       $(".removeId").on(
   			'click',
   			function(event) {
   			 var isSure = confirm("Are you sure you want to delete the Qualification !!!");
				if (isSure) {
   				var targetModal=$(this).parents(".modal").attr("id");
   		   		var targetId=$("#"+targetModal+" #addNpQualificationsForm #npInstituteCourseId").val();
   				$.ajax({
   		         type : "POST",
   		         url : "ajax/qualificationManagement/removeNpInstituteCourse?npInstituteCourseId="+targetId,
   		         success : function(response) {
   		            
   		        	 window.location='qualificationManagement?showDelMsg=true';
   		         },
   		         error : function() {
   		         }
   		     });
				}
				else
					{
					 event.preventDefault();
					}
   				
   			});
       
       $(".pull-right").on(
      			'click','#removeCoCourse',
      			function(event) {
//      				var targetModal=$(this).parents(".modal").attr("id");
      		   		var targetId=$(this).parent().attr("id");
//      				alert(targetId);
      				$.ajax({
      		         type : "POST",
      		         url : "ajax/qualificationManagement/removeCoCourse?coCourseAliasId="+targetId,
      		         success : function(response) {
      		            
      		        	 window.location='qualificationManagement';
      		         },
      		         error : function() {
//      		             alert('error');
      		         }
      		     });
      			});
       
       $("form#addNpQualificationsForm").on("submit",function(event){
           var courseName = $(this).find("input[name=courseName]").val();
           var courseType = $(this).find("select[name=courseType]").val();
           var institutionId = $(this).find("select[name=institutionId]").val();
           $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
           var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
           for(var i=0;i<rows;i++)
        	   {
        	   var aliasSource=$(this).find(".aliasSource"+i).val();
        	  var aliasName=$(this).find(".aliasName"+i).val();
        	   if(aliasName.length > 0 && aliasSource == "")
        	   {
        	  $("span#errPop").text("Please choose Alias Source").show();
        	 event.preventDefault();
        	   }
        	   else if(aliasName.length == 0 && aliasSource > 0)
       	   {
       	  $("span#errPop").text("Please enter Alias Name").show();
       	 event.preventDefault();
       	   }
        	   }
           if (courseName.length == 0) {
               $("span#errPop").text("Please enter Course Name").show();
               event.preventDefault();
           } else if(courseType.length == 0) {
               $("span#errPop").text("Please enter Course type").show();
               event.preventDefault();
           }
           else if(institutionId.length== 0) {
               $("span#errPop").text("Please choose Institute").show();
               event.preventDefault();
           }
           
       });
       
       $(".edit-icon").on("click",function(event){
    	     var modal=$(this).attr("data-target");
    	     var id=$(this).attr("id");
    	     var form=$(modal+" input[name=npInstituteCourseId]").val();
    	     if(form != '' && form != undefined)
    	    {
    	     $.ajax({
    	      type : "POST",
    	      url : "ajax/qualificationManagement/editAlias?id="+form,
    	      success : function(response) {
    	       if(response != undefined && response!= "")
    	        {
    	       $(modal+" .coAliasSection").html(response);
    	        }
    	      },
    	      error : function() {
    	      }
    	      
    	     });
    	    }
    	     $(modal +" select[name='institutionId']").val($("input#instituteName"+id).val());
    	     $(modal +" select[name='courseType']").val($("#courseType"+id).val());
    	     $(modal +" input[name='courseName']").val($("#courseName"+id).val());
    	    var valid=$("#valid"+id).val();
    	     if(valid.trim() == "true")
    	      {
    	     $(modal + " #radio1").prop('checked', true);
    	     $(modal + " #radio2").prop('checked', false);
    	      }
    	     if(valid.trim() == "false")
    	      {
    	      $(modal + " #radio2").prop('checked', true);
    	      $(modal + " #radio1").prop('checked', false);
    	      }
    	   });
       
       $(".btn-rd-cor").on("click",function(event){
           $("#myModalAdd input[name='courseName']").val("");
           $("#myModalAdd select[name='courseType']").val($("#myModalAdd select[name='courseType'] option[val='']").val());
           $("#myModalAdd select[name='institutionId']").val($("#myModalAdd select[name='institutionId'] option[val='']").val());
           $("#myModalAdd #radio1").prop('checked', true);
           $("#myModalAdd #radio2").prop('checked', false);
           
           $.ajax({
    			type : "POST",
    			url : "ajax/roleManagement/addAalias?index="+0,
    			success : function(data) {
    				
    				$("#myModalAdd .coAliasSection").html($(data).html());
    			},
    			error : function() {
    			}
    			
    		});
              });
});


 