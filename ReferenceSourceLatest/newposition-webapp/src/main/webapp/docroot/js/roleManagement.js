$(document).ready(
		function() {
			
			$("#domainFilter").on(
					'change',
					function(event) {
						event.preventDefault();
						var domainFilter = $(this).val();
						var sortBy=$('#sortNpEmploymentRoles').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						var pageNumber=0;
						if (domainFilter.length != 0) {
					    window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						}
					});
			
			$("#roleNameFilter").on(
					'change',
					function(event) {
						event.preventDefault();
						var roleNameFilter = $(this).val();
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var	validFilter=$('#validFilter').val();
						var pageNumber=0;
						if (roleNameFilter.length != 0) {
					    window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						}
						});
			
			$("#validFilter").on(
					'change',
					function(event) {
						event.preventDefault();
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$(this).val();
						var pageNumber=0;
						if (validFilter.length != 0) {
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						}
						});
			
			$("#sortNpEmploymentRoles").on(
					'change',
					function(event) {
						event.preventDefault();
						var sortBy = $(this).val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						var pageNumber=0;
							window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
					});
			
			$(".pagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).text()-1;
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			
			$(".prev").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text()-2;
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".next").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $('.currentPageNo').text();
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".fristPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = 0;
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
			$(".lastPagination").on(
					'click',
					function(event) {
						event.preventDefault();
						var pageNumber = $(this).attr("href");
						var sortBy=$('#sortNpEmploymentRoles').val();
						var domainFilter=$('#domainFilter').val();
						var roleNameFilter=$('#roleNameFilter').val();
						var	validFilter=$('#validFilter').val();
						window.open('roleManagement?sortBy='+sortBy+"&domainFiter="+domainFilter+"&roleNameFilter="+roleNameFilter+"&validFilter="+validFilter+"&pageNumber="+pageNumber,"_self");
						
					});
			
            function getSearchData(searchCode,targetId,currentModal) {
				$.ajax({
                    type : "POST",
                    url : "ajax/roleManagement/aliasSearch?searchCode="+searchCode+"&mergeToId="+targetId,
                    success : function(response) {
                       
                        if(response != undefined)
                        	{
                        	 $('.searchResults1').html(response);
                            	$(".skillDelete").css("bottom", "55.9%");
                            	$(".modal-backdrop").css("height", "100% !important");
                        	}
                    },
                    complete : function(){
                        //code to update the height of modal backdrop -start
                         var $modal        = currentModal.parents(".modal");
                          var $backdrop     = $('.modal-backdrop');
                          var $modalContent = $modal.find('.modal-dialog');
                          var $modalHeight  = $modalContent.height();
                               $backdrop.css({
                                  height: $modalHeight * 1.1
                                  })
                        //code to update the height of modal backdrop - end       
                   },
                    error : function() {
                    }
                });
			   };
			
			$('.quickEntryFilter input').keyup(function(event){
	            event.preventDefault();
	                if (event.keyCode == 13) {
	                	
	                 var searchCode=$(this).val();
	                 var targetModal=$(this).parents(".modal").attr("id");
	                 var targetId=$("#"+targetModal+" #addNpEmploymentRoleForm #roleIds").val();
	                 if(searchCode.length)
	      				{
                         getSearchData(searchCode,targetId,$(this));
	      				} 
	                }
	            });
			$('.quickEntryFilter label').on("click",function(event){
		             event.preventDefault();
		             var searchCode=$(this).parent().find("input#qSearch").val();
	                 var targetModal=$(this).parents(".modal").attr("id");
	                 var targetId=$("#"+targetModal+" #addNpEmploymentRoleForm #roleIds").val();
	                 if(searchCode.length)
	      				{
	                	 	getSearchData(searchCode,targetId,$(this));
	      				}   
	             });
			
			$("button#roleFormDeleteBtn").on("click",function(event){
				var isSure = confirm("Are you sure you want to delete the Role !!!");
				if (isSure) {
					 var targetModal=$(this).parents(".modal").attr("id");
				$("#"+targetModal+" form#addNpEmploymentRoleFormDelete").submit();
				}
				else
					{
					 event.preventDefault();
					}
			});
			
			
			$("form#addNpEmploymentRoleForm").on("submit",function(event){
                var roleName = $(this).find("input[name=roleName]").val();
                var domainId = $(this).find("select[name=domainId]").val();
                $(this).find("fieldset.coAliasSection").find("select").attr("disabled",false);
                var rows= $(this).find("fieldset.coAliasSection").find(".form-group").size();
                for(var i=0;i<rows;i++)
             	   {
             	   var aliasSource=$(this).find(".aliasSource"+i).val();
             	  var aliasName=$(this).find(".aliasName"+i).val();
             	   if(aliasName.length > 0 && aliasSource == "")
             	   {
             	  $("span#errPop").text("Please choose Alias Source").show();
             	 event.preventDefault();
             	   }
             	   else if(aliasName.length == 0 && aliasSource > 0)
            	   {
            	  $("span#errPop").text("Please enter Alias Name").show();
            	 event.preventDefault();
            	   }
             	   }
                if (roleName.length == 0) {
                    $("span#errPop").text("Please enter Role Name").show();
                    event.preventDefault();
                }
                else if(domainId.length== 0) {
                    $("span#errPop").text("Please choose domain").show();
                    event.preventDefault();
                }
                
              
                
            });
			
			$(".edit-icon").on("click",function(event){
                var modal=$(this).attr("data-target");
                var id=$(this).attr("id");
                var form=$(modal+" input[name=roleId]").val();
                if(form != '' && form != undefined)
                  {
                      $.ajax({
                          type : "POST",
                          url : "ajax/roleManagement/editAlias?id="+form,
                          success : function(response) {
                              if(response != undefined && response!= "")
                                  {
                              $(modal+" .coAliasSection").html(response);
                                  }
                          },
                          error : function() {
                          }
                          
                      });
                  }
//                alert()
                $(modal+" select[name='domainId']").val($("#domain"+id).val());
                $(modal +" input[name='roleName']").val($("#roleName"+id).val());
               var valid=$("#valid"+id).val();
                if(valid.trim() == "Valid")
                    {
                $(modal + " #radio1").prop('checked', true);
                $(modal + " #radio2").prop('checked', false);
                    }
                if(valid.trim() == "Invalid")
                    {
                    $(modal + " #radio2").prop('checked', true);
                    $(modal + " #radio1").prop('checked', false);
                    }
              });

            $(".btn-rd-cor").on("click",function(event){
            $("#myModalAdd input[name='roleName']").val("");
            $("#myModalAdd select[name='domainId']").val($("#myModalAdd select[name='domainId'] option[val='']").val());
            $("#myModalAdd #radio1").prop('checked', true);
            $("#myModalAdd #radio2").prop('checked', false);
            
            $.ajax({
					type : "POST",
					url : "ajax/roleManagement/addAalias?index="+0,
					success : function(data) {
						
						$("#myModalAdd .coAliasSection").html($(data).html());
					},
					error : function() {
					}
					
				});
               });
			
		});