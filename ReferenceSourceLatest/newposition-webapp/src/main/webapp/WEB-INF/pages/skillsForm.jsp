<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<form:form id="frm_details" method="post" action="ajax/skillsSubmit" commandName="skillCombinedCommand">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Skills</th>
            <th>Experience</th>
            <th>Level</th>
            <th>In ${companyName}</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${skillCombinedCommand.skills}" var="skill" varStatus="status">

            <tr>
                <td>
                        ${skill.skill}
                    <form:hidden path="skills[${status.index}].skill" value="${skill.skill}"/>
                    <form:hidden path="skills[${status.index}].companyName" value="${skill.companyName}"/>
                    <form:hidden path="skills[${status.index}].candidateEmployerId"
                                 value="${skill.candidateEmployerId}"/>
                </td>

                <td>
                    <form:select path="skills[${status.index}].experienceLevel">
                        <form:option id="0" value="0">--select--</form:option>
                        <form:option id="1" value="1">1</form:option>
                        <form:option id="2" value="2">2</form:option>
                        <form:option id="3" value="3">3</form:option>
                        <form:option id="4" value="4">4</form:option>
                        <form:option id="5" value="5">5</form:option>
                        <form:option id="6" value="6">6</form:option>
                    </form:select>
                </td>
                <td>
                    <form:select path="skills[${status.index}].expertLevel">
                        <form:option id="0" value="o">--select--</form:option>
                        <form:option id="1" value="beginner">Beginner</form:option>
                        <form:option id="2" value="intermediate">Intermediate</form:option>
                        <form:option id="3" value="expert">Expert</form:option>
                    </form:select>
                </td>
                <td><form:checkbox path="skills[${status.index}].inCurrentCompany"
                                   value="skills[${status.index}].company"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="row text-center"><input id="modalBtn" type="submit" value="Submit" class="pp_button test"></div>
</form:form>
                
