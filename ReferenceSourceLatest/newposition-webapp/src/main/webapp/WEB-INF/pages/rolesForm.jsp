<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<form:form id="frm_roles_details" method="post" action="" commandName="roleForm">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Roles</th>
            <th>Experience</th>
            <th>Level</th>
            <th>In ${companyName}</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${roleForm.roles}" var="skill" varStatus="status">

            <tr>
                <td>
                        ${skill.roleName}
                    <form:hidden path="roles[${status.index}].roleName" value="${skill.roleName}"/>
                    <form:hidden path="roles[${status.index}].companyName" value="${skill.companyName}"/>
                    <form:hidden path="roles[${status.index}].candidateEmployerId"
                                 value="${skill.candidateEmployerId}"/>

                </td>

                <td>
                    <form:select path="roles[${status.index}].experienceLevel">
                        <form:option id="0" value="0">--select--</form:option>
                        <form:option id="0" value="1">1</form:option>
                        <form:option id="0" value="2">2</form:option>
                        <form:option id="0" value="3">3</form:option>
                        <form:option id="0" value="4">4</form:option>
                        <form:option id="0" value="5">5</form:option>
                        <form:option id="0" value="6">6</form:option>
                    </form:select>
                </td>
                <td>
                    <form:select path="roles[${status.index}].expertLevel">
                        <form:option id="0" value="o">--select--</form:option>
                        <form:option id="0" value="beginner">Beginner</form:option>
                        <form:option id="0" value="intermediate">Intermediate</form:option>
                        <form:option id="0" value="expert">Expert</form:option>
                    </form:select>
                </td>
                <td><form:checkbox path="roles[${status.index}].inCurrentCompany"
                                   value="roles[${status.index}].company"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="row text-center"><input id="modalBtn" type="submit" value="Submit" class="pp_button test"></div>
</form:form>
                
