<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>


<json:object>
    <json:property name="overAllSkills" escapeXml="false">
        <div class="links_but">
            <label class="pp_label">Skill Basket</label>
        </div>
        <div class="links_js links_jsonlyclick ">
            <c:forEach items="${skills}" var="skill">

                <a href="javascript:void(0)" id="${skill.id}"
                   name="${skill.skill}">${skill.skill} ${skill.experienceLevel}
                    <c:choose>
                        <c:when test="${skill.experienceLevel eq 1}">Yr</c:when>
                        <c:otherwise>Yrs</c:otherwise>
                    </c:choose>
                        ${skill.expertLevel}<i class="fa fa-times"></i></a>

            </c:forEach>
        </div>
        <div class="clear"></div>

        <div class="links_but text-right">
            <input type="text" id="addskillBasket" placeholder="Java 1 Beginner" class="pp_text">
            <c:if test="${!validData}">
                <span class="errorMessage">Please Enter Valid Format(EX:Java 1 Beginner) </span>
            </c:if>
        </div>


    </json:property>

    <json:property name="companySkills" escapeXml="false">
        <c:forEach items="${companySkills}" var="companySkill">
            <a href="javascript:void(0)" id="${companySkill.id}"
               name="${companySkill.skillName}">${companySkill.skillName}<i
                    class="fa fa-times"></i></a>
        </c:forEach>
    </json:property>
    <json:property name="success" escapeXml="false">

    </json:property>
</json:object>


 
 
