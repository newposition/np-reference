<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<form:form id="frm_tools_details" method="post" action="" commandName="toolsForm">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Tools & Systems</th>
            <th>Experience</th>
            <th>Level</th>
            <th>In ${companyName}</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${toolsForm.tools}" var="tool" varStatus="status">

            <tr>
                <td>
                        ${tool.toolName}
                    <form:hidden path="tools[${status.index}].toolName" value="${tool.toolName}"/>
                    <form:hidden path="tools[${status.index}].companyName" value="${tool.companyName}"/>
                    <form:hidden path="tools[${status.index}].candidateEmployerId" value="${tool.candidateEmployerId}"/>
                </td>

                <td>
                    <form:select path="tools[${status.index}].experienceLevel">
                        <form:option id="0" value="0">--select--</form:option>
                        <form:option id="1" value="1">1</form:option>
                        <form:option id="2" value="2">2</form:option>
                        <form:option id="3" value="3">3</form:option>
                        <form:option id="4" value="4">4</form:option>
                        <form:option id="5" value="5">5</form:option>
                        <form:option id="6" value="6">6</form:option>
                    </form:select>
                </td>
                <td>
                    <form:select path="tools[${status.index}].expertLevel">
                        <form:option id="0" value="o">--select--</form:option>
                        <form:option id="1" value="beginner">Beginner</form:option>
                        <form:option id="2" value="intermediate">Intermediate</form:option>
                        <form:option id="3" value="expert">Expert</form:option>
                    </form:select>
                </td>
                <td><form:checkbox path="tools[${status.index}].inCurrentCompany"
                                   value="tools[${status.index}].company"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="row text-center"><input id="modalBtn" type="submit" value="Submit" class="pp_button test"></div>
</form:form>
                
