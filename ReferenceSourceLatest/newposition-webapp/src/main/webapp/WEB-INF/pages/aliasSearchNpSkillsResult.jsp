<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script>
    $(".merge").on(
            'click',
            function (event) {
                var targetModal = $(this).parents(".modal").attr("id");
                var isSure = confirm("Are you sure you want to Merge !!!");
                if (!isSure) {
                    event.preventDefault();
                }
            });
</script>
<c:forEach items="${npSkills}" var="npSkill" varStatus="index">
    <form:form id="addNpSkillsForm" action="?"
               modelAttribute="addNpSkillsForm" method="POST">
        <form:input type="hidden" path="skillId" value="${npSkill.id}"/>
        <input type="hidden" class="mergeTo" name="mergeToId" value="${mergeTo}"/>

        <div class="col-md-5 col-xs-12">
            <div class="bd-blue-panel">
                <div class="skl_btx">
                    <button class="merge" type="submit">
                        <!-- 							<span class="fa fa-plus"></span> -->
                    </button>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <form:select path="domainId" class="form-control selectbox "
                                                 id="Select19">
                                        <option value="" style="display:none;">Select Domain</option>
                                        <c:forEach items="${npDomainTypes}" var="npDomainType">
                                            <c:choose>
                                                <c:when
                                                        test="${npSkill.npDomain.id eq npDomainType.id}">
                                                    <form:option value="${npDomainType.id}"
                                                                 selected="selected">${npDomainType.typeName}</form:option>
                                                </c:when>
                                                <c:otherwise>
                                                    <form:option
                                                            value="${npDomainType.id}">${npDomainType.typeName}</form:option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <form:select path="skillType" class="form-control selectbox "
                                                 id="Select20">
                                        <option value="" style="display:none;">Select SkillType</option>
                                        <c:forEach items="${npSkillTypes}" var="npSkillType">
                                            <c:choose>
                                                <c:when test="${npSkill.skillType eq npSkillType}">
                                                    <form:option value="${npSkillType}"
                                                                 selected="selected">${npSkillType}</form:option>
                                                </c:when>
                                                <c:otherwise>
                                                    <form:option value="${npSkillType}">${npSkillType}</form:option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>

                                    </form:select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <form:input type="text" path="skillName"
                                                placeholder="Skill Name" class="pp_text"
                                                value="${npSkill.skillName}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="radio-inline f-label" for="radios-0"> <input
                                            type="radio" name="valid" id="radio1" value="Valid"
                                            <c:if test="${npSkill.valid eq 'Valid'}">checked="checked"</c:if>>
                                        Valid
                                    </label> <label class="radio-inline f-label" for="radios-1"> <input
                                        type="radio" name="valid" id="radio2" value="Invalid"
                                        <c:if test="${npSkill.valid eq 'Invalid'}">checked="checked"</c:if>>
                                    Invalid
                                </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group1">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="f-label"> No. of Active Candidate :
                                            ${fn:length(npSkill.npCandidateSkillses)} </label> <label
                                        class="f-label"> No. of Active Positions : 20 </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>


        <div class="col-md-7 col-xs-12">
            <div class="bd-blue-panel-scroll">
                <div class="skl_btx">

                    <fieldset>
                        <c:forEach items="${npSkill.coSkillAlias}" var="coSkillAlias"
                                   varStatus="index">


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6">
                                        <input type="hidden" class="aliasId${index.index} coAliasId"
                                               name="certificationAliases[${index.index}].aliasId"
                                               value="${coSkillAlias.skillAliasID}"
                                               class="aliasId${index.index} coAliasId"/>
                                        <input type="text"
                                               name="certificationAliases[${index.index}].aliasName" id="Text20"
                                               value="${coSkillAlias.skillAliasName}"
                                               placeholder="Alias Skill Name" class="pp_text" readonly="true"/>

                                    </div>
                                    <div class="col-md-5">
                                        <select name="certificationAliases[${index.index}].aliasSource"
                                                class="form-control selectbox " id="Select22" readonly="true">
                                            <option value="" style="display:none;">Select Alias Source</option>
                                            <c:forEach items="${coSources}" var="coSource">
                                                <c:choose>
                                                    <c:when
                                                            test="${coSkillAlias.coSource.sourceID eq coSource.sourceID}">
                                                        <option value="${coSource.sourceID}" selected>
                                                                ${coSource.sourceName}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${coSource.sourceID}">${coSource.sourceName}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                            <%-- 											<button id="${index.index}" name="btn-send" class="btn btn-edt editAliasRow" ><span class="fa fa-pencil-square-o"></span></button> --%>
                                            <%-- 	                                        <button id="${index.index}" name="btn-send" class="btn btn-edt pull-right deleteAliasRow"><span class="fa fa-trash"></span></button> --%>
                                    </div>
                                </div>
                            </div>


                            <%-- 							<input type="text" name="certificationAliases[${index.index}].aliasName" id="Text20" placeholder="Alias Skill Name" class="pp_text" /> --%>


                        </c:forEach>
                    </fieldset>
                </div>
            </div>
        </div>

    </form:form>
</c:forEach>