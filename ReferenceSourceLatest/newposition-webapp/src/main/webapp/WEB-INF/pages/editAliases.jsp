<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${certificationAliases}" var="certificationAlias" varStatus="index">
    <div class="form-group">
        <div class="row">
            <div class="col-md-4 col-xs-6">

                <input type="hidden" class="aliasId${index.index} coAliasId"
                       name="certificationAliases[${index.index}].aliasId" value="${certificationAlias.aliasId}"/>
                <input type="hidden" id="url" value="/certificateManagement/deleteAlias" class="del-url">
                <input type="text" name="certificationAliases[${index.index}].aliasName" readonly="true" id="Text20"
                       value="${certificationAlias.aliasName}" placeholder="Alias  Name"
                       class="aliasName${index.index} pp_text"/>

            </div>
            <div class="col-md-5">
                <select name="certificationAliases[${index.index}].aliasSource" disabled="disabled"
                        class="form-control selectbox aliasSource${index.index}" id="Select22">
                    <option value="">Select Alias Source</option>
                    <c:forEach items="${coSources}" var="coSource">
                        <fmt:parseNumber var="Source" value="${certificationAlias.aliasSource}"/>
                        <c:choose>

                            <c:when test="${Source eq coSource.sourceID}">
                                <option value="${coSource.sourceID}" selected="selected">${coSource.sourceName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${coSource.sourceID}">${coSource.sourceName}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-3 col-xs-6">
                <button id="${index.index}" name="btn-send" class="btn btn-edt editAliasRow"><span
                        id="editCoCertificate" class="fa fa-pencil-square-o"></span></button>
                <button id="${index.index}" name="btn-send" class="btn btn-edt pull-right deleteAliasRow"><span
                        class="fa fa-trash"></span></button>
            </div>
        </div>
    </div>
</c:forEach>
