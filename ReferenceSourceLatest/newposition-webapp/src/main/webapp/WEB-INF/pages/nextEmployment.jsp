<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<json:object>
    <json:property name="company" escapeXml="false">
        <label class="pp_label">Employment History</label>
        <div class="em_box">
            <div class="em_box_logo">
                <div class="em_box_edit_logo"><a href="javascript:void(0)">Edit Logo</a></div>
            </div>
            <div class="em_box_details">
                <div class="errorText"></div>
                <input type="hidden" id="employerId" name="employerId" value="${employer.uid}"/>
                <div class="line1">${employer.npEmployer.name}</div>
                <div class="line2">
                    <span class="line2a"><fmt:formatDate pattern="dd/MM/yyyy" value="${employer.startDate}"/></span>
                    to
                <span class="line2b">
                <c:if test="${employer.enddate eq null}">
                    current
                </c:if>
                <c:if test="${employer.enddate ne null}">
                    <fmt:formatDate pattern="dd/MM/yyyy" value="${employer.enddate}"/>
                </c:if>
                </span>
                </div>
                <div class="line3">${employer.jobTitle}</div>
                <div class="line4">${employer.corporateTitle}</div>
            </div>
            <button class="plusbox plus_btn"></button>
            <button class="edit edit_btn"></button>
            <button class="save save_btn"></button>
            <button class="trash trash_btn"></button>
            <button class="cancel cancel_btn"></button>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <c:if test="${notShowPrev ne true}">

                    <label id="${prevIds}" class="pn_label prev"><< Prev</label>
                </c:if>

            </div>
            <c:if test="${notShowNext ne true}">
                <div class="col-xs-6 col-md-6 text-right">
                    <label id="${nextIds}" class="pn_label next">Next >></label>
                </div>
            </c:if>
        </div>
    </json:property>

    <json:property name="achievements" escapeXml="false">
        ${employer.achievements}
    </json:property>

    <json:property name="currentCompanySkills" escapeXml="false">
        <div class="col-md-12">
            <label class="pp_label companyNameSkillsLabel">Skills With ${employer.npEmployer.name}</label>
            <div class="links_js links_jsonlyclick skillsWithCurrentCompany">
                <c:forEach items="${employer.npSkills}" var="skill" varStatus="status">
                    <a href="javascript:void(0)" id="${skill.id}" name="${skill.skillName}">${skill.skillName}<i
                            class="fa fa-times"></i></a>
                </c:forEach>
            </div>
        </div>
    </json:property>

    <json:property name="currentCompanyRoles" escapeXml="false">
        <div class="col-md-12">
            <label class="pp_label companyNameRolesLabel">Roles With ${employer.npEmployer.name}</label>
            <div class="links_js links_jsonlyclick rolesWithCurrentCompany">
                <c:forEach items="${employer.npEmploymentRoles}" var="role" varStatus="status">
                    <a href="javascript:void(0)" id="${role.id}" name="${role.roleName}">${role.roleName}<i
                            class="fa fa-times"></i></a>
                </c:forEach>
            </div>
        </div>
    </json:property>
    <json:property name="currentCompanyTools" escapeXml="false">
        <div class="col-md-12">
            <label class="pp_label companyNameToolsLabel">Tools With ${employer.npEmployer.name}</label>
            <div class="links_js links_jsonlyclick toolsWithCurrentCompany">
                <c:forEach items="${employer.npTools}" var="tool" varStatus="status">
                    <a href="javascript:void(0)" id="${tool.id}" name="${tool.toolName}">${tool.toolName}<i
                            class="fa fa-times"></i></a>
                </c:forEach>
            </div>
        </div>
    </json:property>
    <json:property name="currentCompanyName" escapeXml="false">
        ${employer.npEmployer.name}
    </json:property>
</json:object>