<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>


<json:object>
    <json:property name="overAllTools" escapeXml="false">
        <div class="links_but">
            <label class="pp_label">Tools & Systems Basket</label>
        </div>
        <div class="links_js links_jsonlyclick toolsBask">
            <c:forEach items="${tools}" var="tool">
                <a href="javascript:void(0)" id="${tool.id}"
                   name="${tool.toolName}">${tool.toolName} ${tool.experienceLevel}
                    <c:choose>
                        <c:when test="${tool.experienceLevel eq 1}">Yr</c:when>
                        <c:otherwise>Yrs</c:otherwise>
                    </c:choose>
                        ${tool.expertLevel}<i class="fa fa-times"></i></a>
            </c:forEach>
        </div>
        <div class="clear"></div>
        <div class="links_but text-right">
            <input type="text" id="addtoolBasket" placeholder="Jenkins 1 Beginner" class="pp_text" id="tools_input">
            <c:if test="${!validData}">
                <span class="errorMessage">Please Enter Valid Format(EX:Jenkins 1 Beginner) </span>
            </c:if>
        </div>
    </json:property>

    <json:property name="companyTools" escapeXml="false">
        <c:forEach items="${companyTools}" var="companyTool">
            <a href="javascript:void(0)" id="${companyTool.id}" name="${companyTool.toolName}">${companyTool.toolName}<i
                    class="fa fa-times"></i></a>
        </c:forEach>
    </json:property>
</json:object>

