<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>


<json:object>
    <json:property name="overAllRoles" escapeXml="false">
        <div class="links_but">
            <label class="pp_label">Roles Basket</label>
        </div>
        <div class="links_js links_jsonlyclick rolesBask">
            <c:forEach items="${roles}" var="role">
                <a href="javascript:void(0)" id="${role.id}"
                   name="${role.roleName}">${role.roleName} ${role.experienceLevel} <c:choose>
                    <c:when test="${role.experienceLevel eq 1}">Yr</c:when>
                    <c:otherwise>Yrs</c:otherwise>
                </c:choose><i class="fa fa-times"></i></a>
            </c:forEach>
        </div>
        <div class="clear"></div>
        <div class="links_but text-right">
            <input type="text" id="addroleBasket" placeholder="SoftwareEnginner 1" class="pp_text" id="roles_input">
            <c:if test="${!validData}">
                <span class="errorMessage">Please Enter Valid Format(EX:SoftwareEnginner 1) </span>
            </c:if>
        </div>

    </json:property>

    <json:property name="companyRoles" escapeXml="false">
        <c:forEach items="${companyRoles}" var="companyRole">
            <a href="javascript:void(0)" id="${companyRole.id}" name="${companyRole.roleName}">${companyRole.roleName}<i
                    class="fa fa-times"></i></a>
        </c:forEach>
    </json:property>
</json:object>

