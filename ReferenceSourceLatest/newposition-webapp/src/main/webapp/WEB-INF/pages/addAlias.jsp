<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div>
    <div class="form-group ">
        <div class="row ">
            <div class="col-md-4 col-xs-6">
                <input type="hidden" class="aliasId${index} coAliasId" name="certificationAliases[${index}].aliasId"
                       value=""/>
                <input type="text" name="certificationAliases[${index}].aliasName" id="Text20" placeholder="Alias  Name"
                       class="pp_text aliasName${index}"/>
            </div>
            <div class="col-md-5">
                <select name="certificationAliases[${index}].aliasSource"
                        class="form-control selectbox aliasSource${index} " id="Select22">
                    <option value="">Select Alias Source</option>
                    <c:forEach items="${coSources}" var="coSource">
                        <option value="${coSource.sourceID}">${coSource.sourceName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-md-3 col-xs-6">
                <button id="${index}" name="btn-send" class="btn btn-edt pull-right deleteAliasRow"><span
                        class="fa fa-trash"></span></button>
            </div>
        </div>
    </div>
</div>
