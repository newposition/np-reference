<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="np" uri="newpositionTags" %>
<script>
    $(".merge").on(
            'click',
            function (event) {
                var targetModal = $(this).parents(".modal").attr("id");
                var isSure = confirm("Are you sure you want to Merge !!!");
                if (!isSure) {
                    event.preventDefault();
                }
            });
</script>
<c:forEach items="${npEmployers}" var="npEmployer" varStatus="index">
    <form:form id="addNpIndustriesForm" action="?"
               modelAttribute="addNpIndustriesForm" method="POST">
        <form:input type="hidden" path="industryId" value="${npEmployer.id}"/>
        <input type="hidden" class="mergeTo" name="mergeToId" value="${mergeTo}"/>

        <div class="col-md-5 col-xs-12">
            <div class="bd-blue-panel">
                <div class="skl_btx">
                    <button class="merge" type="submit">
                        <!-- 							<span class="fa fa-plus"></span> -->
                    </button>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <form:select path="sectorId" class="form-control selectbox "
                                                 id="Select19">
                                        <np:listSectorOptions defaultSelect="${npEmployer.npSector.id}"
                                                              defaultValue="Select Parent Industry"/>
                                    </form:select>
                                </div>
                            </div>
                        </div>
                            <%-- <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form:select path="industryType" class="form-control selectbox "
                                            id="Select20">
                                            <option value="">Select IndustrysType</option>
                                            <c:forEach items="${npIndustryTypes}" var="npIndustryType">
                                                <c:choose>
                                                    <c:when test="${npIndustry.industryType eq industryType}">
                                                        <form:option value="${industryType}" selected="selected">${npIndustryType}</form:option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <form:option value="${npIndustryType}">${npIndustryType}</form:option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>

                                        </form:select>
                                    </div>
                                </div>
                            </div> --%>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <form:input type="text" path="industryName"
                                                placeholder="Industry Name" class="pp_text"
                                                value="${npEmployer.name}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="radio-inline f-label" for="radios-0"> <input
                                            type="radio" name="valid" id="radio1" value="Valid"
                                            <c:if test="${npEmployer.valid eq 'Valid'}">checked="checked"</c:if>>
                                        Valid
                                    </label> <label class="radio-inline f-label" for="radios-1"> <input
                                        type="radio" name="valid" id="radio2" value="Invalid"
                                        <c:if test="${npEmployer.valid eq 'Invalid'}">checked="checked"</c:if>>
                                    Invalid
                                </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group1">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="f-label"> No. of Active Candidate :
                                            ${fn:length(npEmployer.npCandidateEmployers)} </label> <label
                                        class="f-label"> No. of Active Positions : 20 </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>


        <div class="col-md-7 col-xs-12">
            <div class="bd-blue-panel-scroll">
                <div class="skl_btx">

                    <fieldset>
                        <c:forEach items="${npEmployer.coEmployerAlias}" var="coEmployerAlias"
                                   varStatus="index">


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6">
                                        <input type="hidden" class="aliasId${index.index} coAliasId"
                                               name="certificationAliases[${index.index}].aliasId"
                                               value="${coEmployerAlias.employerAliasID}"/>
                                        <input type="text"
                                               name="certificationAliases[${index.index}].aliasName" id="Text20"
                                               value="${coEmployerAlias.employerAliasName}"
                                               placeholder="Alias Industry Name" class="pp_text" readonly="true"/>

                                    </div>
                                    <div class="col-md-5">
                                        <select name="certificationAliases[${index.index}].aliasSource"
                                                class="form-control selectbox " id="Select22" readonly="true">
                                            <option value="" style="display:none;">Alias Source</option>
                                            <c:forEach items="${coSources}" var="coSource">
                                                <c:choose>
                                                    <c:when
                                                            test="${coEmployerAlias.coSource.sourceID eq coSource.sourceID}">
                                                        <option value="${coSource.sourceID}" selected>
                                                                ${coSource.sourceName}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${coSource.sourceID}">${coSource.sourceName}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <!-- 											<button id="Button1" name="btn-send" class="btn btn-edt editAliasRow"><span class="fa fa-pencil-square-o"></span></button> -->
                                            <%--                                             <button id="${index.index}" name="btn-send" class="btn btn-edt pull-right deleteAliasRow"><span class="fa fa-trash"></span></button> --%>
                                    </div>
                                </div>
                            </div>


                            <%-- 							<input type="text" name="toolAliases[${index.index}].aliasName" id="Text20" placeholder="Alias Tool Name" class="pp_text" /> --%>


                        </c:forEach>
                    </fieldset>
                </div>
            </div>
        </div>

    </form:form>
</c:forEach>