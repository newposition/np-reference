<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cms" prefix="cms" %>
<%@ taglib uri="http://magnolia-cms.com/taglib/templating-components/cmsfn" prefix="cmsfn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script>
    $(".merge").on(
            'click',
            function (event) {
                var targetModal = $(this).parents(".modal").attr("id");
                var isSure = confirm("Are you sure you want to Merge !!!");
                if (!isSure) {
                    event.preventDefault();
                }
            });
</script>
<c:forEach items="${npInstituteCertificates}" var="npInstituteCertificate" varStatus="index">
    <form id="addNpCertificationsForm" action="?" method="POST">
        <input type="hidden" name="npInstituteCertificateId" value="${npInstituteCertificate.id}"/>
        <input type="hidden" name="mergeId" value="${mergeId}"/>
        <div class="col-md-5 col-xs-12">
            <div class="bd-blue-panel">
                <div class="skl_btx">
                    <button class="merge"></button>
                    <fieldset>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                        <%-- <a class="skl-btl" href="">
                                            ${npInstituteCertificate.npInstitutions.instituteName}
                                         </a> --%>
                                    <select name="institutionId" class="form-control selectbox " id="Select19">
                                        <option value="" style="display:none;">Select Institution</option>
                                        <c:forEach items="${npInstitutions}" var="npInstitution">
                                            <c:choose>
                                                <c:when test="${npInstituteCertificate.npInstitutions.id eq npInstitution.id}">
                                                    <option value="${npInstitution.id}"
                                                            selected="selected">${npInstitution.instituteName}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${npInstitution.id}">${npInstitution.instituteName}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                        <%--  <a class="skl-btl" href="">
                                                 ${npInstituteCertificate.npCertifications.certificateType}
                                              </a> --%>
                                    <select name="certificationType" class="form-control selectbox " id="Select20">
                                        <option value="" style="display:none;">Select CertificationType</option>
                                        <c:forEach items="${npCertificationTypes}" var="npCertificationType">
                                            <c:choose>
                                                <c:when test="${npInstituteCertificate.npCertifications.certificateType eq npCertificationType}">
                                                    <option value="${npCertificationType}"
                                                            selected="selected">${npCertificationType}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${npCertificationType}">${npCertificationType}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                        <%--                                                                 <a class="skl-btl" href="">${npInstituteCertificate.npCertifications.certificateName}</a> --%>
                                    <!--                                                             <select name="" id="" class="form-control selectbox "> -->
                                    <!--                                                                 <option value="1">Certification Name</option> -->
                                    <!--                                                             </select> -->
                                    <input type="text" name="certificationName" placeholder="Certification Name"
                                           class="pp_text"
                                           value="${npInstituteCertificate.npCertifications.certificateName}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="radio-inline f-label" for="radios-0">
                                        <input type="radio" name="valid" id="radio1" value="true"
                                               <c:if test="${npInstituteCertificate.npCertifications.valid eq 'true'}">checked="checked"</c:if>>
                                        Valid
                                    </label>
                                    <label class="radio-inline f-label" for="radios-1">
                                        <input type="radio" name="valid" id="radio2" value="false"
                                               <c:if test="${npInstituteCertificate.npCertifications.valid eq 'false'}">checked="checked"</c:if>>
                                        Invalid
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group1">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="f-label">
                                        No. of Active Candidate
                                        : ${fn:length(npInstituteCertificate.npCertifications.npCandidateCertificates)}
                                    </label>
                                    <label class="f-label">
                                        No. of Active Positions : 20
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <!--                                             <input type="submit" value="Add certification" id="addCertification" class="pp_button"> -->
                        <%--                                         </form> --%>
                </div>
            </div>
        </div>


        <div class="col-md-7 col-xs-12">
            <div class="bd-blue-panel-scroll">
                <div class="skl_btx">
                    <!--                                           <input type="submit" value="Add certification" id="addCertification" class="pp_button"> -->
                        <%--                                         <form action=""> --%>
                    <fieldset>
                        <input type="hidden" id="coCertificationAliasIndex"
                               value="${fn:length(npInstituteCertificate.npCertifications.coCertificationAlias) }">
                        <c:forEach items="${npInstituteCertificate.npCertifications.coCertificationAlias}"
                                   var="coCertificationAlias" varStatus="index">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6">
                                        <input type="hidden" name="certificationAliases[${index.index}].aliasId"
                                               value="${coCertificationAlias.certificationAliasID}"
                                               class="aliasId${index.index} coAliasId"/>
                                        <input type="text" name="certificationAliases[${index.index}].aliasName"
                                               id="Text20" value="${coCertificationAlias.certificationAliasName}"
                                               placeholder="Alias Name" class="pp_text" readonly="true"/>

                                    </div>
                                    <div class="col-md-5">
                                        <select name="certificationAliases[${index.index}].aliasSource"
                                                class="form-control selectbox aliasSource" id="Select22"
                                                readonly="true">
                                            <option value="" style="display:none;">Select Alias Source</option>
                                            <c:forEach items="${coSources}" var="coSource">
                                                <c:choose>
                                                    <c:when test="${coCertificationAlias.coSource.sourceID eq coSource.sourceID}">
                                                        <option value="${coSource.sourceID}"
                                                                selected="selected">${coSource.sourceName}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${coSource.sourceID}">${coSource.sourceName}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                            <%--                                                             <button id="${index.index}" name="btn-send" class="btn btn-edt editAliasRow"><span id="editCoCertificate" class="fa fa-pencil-square-o"></span></button> --%>
                                            <%--                                                              <button id="${index.index}" name="btn-send" class="btn btn-edt pull-right deleteAliasRow"><span class="fa fa-trash"></span></button> --%>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </fieldset>
                </div>
            </div>
        </div>

    </form>

</c:forEach>